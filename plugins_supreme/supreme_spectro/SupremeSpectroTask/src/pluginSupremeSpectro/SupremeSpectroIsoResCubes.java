package pluginSupremeSpectro;

import SupremeSpectroExport.*;

import com.mathworks.toolbox.javabuilder.*;

import java.util.ArrayList;
import java.util.List;

import herschel.ia.task.Task;
import herschel.ia.task.TaskParameter;
import herschel.ia.dataset.MetaData;
import herschel.ia.dataset.StringParameter;
import herschel.ia.dataset.spectrum.SpectralSimpleCube;
import herschel.ia.numeric.*;
import herschel.share.interpreter.InterpreterUtil;
import herschel.share.unit.FluxDensity;
import herschel.share.unit.SolidAngle;

public class SupremeSpectroIsoResCubes extends Task {
	
	public SupremeSpectroIsoResCubes() {
		
		super("supremeSpectroIsoResCubes");
        setDescription("Function that calculate IsoResolution cubes for a given wave number");
                
        //SimpleSpectralCube SSW
        TaskParameter parameter = new TaskParameter("cubeSSW", SpectralSimpleCube.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Full Resolution SSW cube");
        addTaskParameter(parameter);
        
        //SimpleSpectralCube SLW
        parameter = new TaskParameter("cubeSLW", SpectralSimpleCube.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Full Resolution SLW cube");
        addTaskParameter(parameter);
        
        //fEqBeamSSW
        parameter = new TaskParameter("fEqBeamSSW", Double3d.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Fourier domain beam corresponding to SSW cube");
        addTaskParameter(parameter);
        
        //fEqBeamSSW
        parameter = new TaskParameter("fEqBeamSLW", Double3d.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Fourier domain beam corresponding to SLW cube");
        addTaskParameter(parameter);
        
        //IsoWave
        parameter = new TaskParameter("isoWave", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Wave number for which the Iso resolved cubes are calculated");
        addTaskParameter(parameter);
        
        //outList
        parameter = new TaskParameter("outList", ArrayList.class);
        parameter.setType(TaskParameter.OUT);
        parameter.setMandatory(false);
        parameter.setDescription("List of Product: Iso Resolutions cubes. SSW and SLW");
        addTaskParameter(parameter);
		
	}
	
	public void execute() {
		
		SpectralSimpleCube cubeSSW_HIPE = new SpectralSimpleCube((SpectralSimpleCube) getParameter("cubeSSW").getValue());
		SpectralSimpleCube cubeSLW_HIPE = new SpectralSimpleCube((SpectralSimpleCube) getParameter("cubeSLW").getValue());
		Double3d fEqBeamSSW_HIPE = (Double3d) getParameter("fEqBeamSSW").getValue();
		Double3d fEqBeamSLW_HIPE = (Double3d) getParameter("fEqBeamSLW").getValue();
		Double isoWave = (Double) getParameter("isoWave").getValue();
		
		//Check isoWave Integrity
		if (isoWave<0) {throw new IllegalArgumentException("AlphaStep should be positive.");}
		        
        List<Object> outList = new ArrayList<Object>();
        boolean verbose = true;
        
        try {
        
        	System.out.println("SupremeSpectroIsoResCube");
        	System.out.println("Reading SSW and SLW data ...");
        	
	    	double[][][] cubeSSW_image = ((Double3d) (cubeSSW_HIPE.getImage())).getArray();
	    	MWNumericArray cubeSSW = new MWNumericArray(cubeSSW_image, MWClassID.DOUBLE);
	    	if (verbose){System.out.print("#cubeSSW ");}
	    	
	    	double[] cubeSSW_wave = ((Double1d) (cubeSSW_HIPE.getWave())).getArray();
	    	MWNumericArray waveSSW = new MWNumericArray(cubeSSW_wave, MWClassID.DOUBLE);
	    	if (verbose){System.out.print("#waveSSW ");}
	    	
	    	double[][][] cubeSLW_image = ((Double3d) (cubeSLW_HIPE.getImage())).getArray();
	    	MWNumericArray cubeSLW = new MWNumericArray(cubeSLW_image, MWClassID.DOUBLE);
	    	if (verbose){System.out.print("#cubeSLW ");}
	    	
	    	double[] cubeSLW_wave = ((Double1d) (cubeSLW_HIPE.getWave())).getArray();
	    	MWNumericArray waveSLW = new MWNumericArray(cubeSLW_wave, MWClassID.DOUBLE);
	    	if (verbose){System.out.print("#waveSLW ");}
	    	
	    	double[][][] fEqBeamSSW_tmp = fEqBeamSSW_HIPE.getArray();
	    	MWNumericArray fEqBeamSSW = new MWNumericArray(fEqBeamSSW_tmp, MWClassID.DOUBLE);
	    	if (verbose){System.out.print("#fEqBeamSSW ");}
	    	
	    	double[][][] fEqBeamSLW_tmp = fEqBeamSLW_HIPE.getArray();
	    	MWNumericArray fEqBeamSLW = new MWNumericArray(fEqBeamSLW_tmp, MWClassID.DOUBLE);
	    	if (verbose){System.out.println("#fEqBeamSLW ");}
	    	
	    	
	    	System.out.println("Starting SupremeSpectroIsoResCubes Core Task...");
			InterpreterUtil.println("Starting SupremeSpectroIsoResCubes Core Task...");
	    	SupremeSpectroJAVA x = new SupremeSpectroJAVA();
			Object[] outSupreme = x.SupremeSpectro_isoResCubes(2,cubeSSW,cubeSLW,fEqBeamSSW,fEqBeamSLW,waveSSW,waveSLW,isoWave);
			//[SSW SLW]
			System.out.println("SupremeSpectroIsoResCubes Task Completed...");
			InterpreterUtil.println("SupremeSpectroIsoResCubes Task Completed...");
			
			
			//SSW
			System.out.print("Extract Cube SSW... ");
			InterpreterUtil.println("Extract Cube SSW... ");
			MWNumericArray SSW = (MWNumericArray)outSupreme[0]; 
			double[][][] SSW_HIPE = (double[][][]) SSW.toDoubleArray();
			
			//Creating HIPE SSW Cube
			System.out.print("Creating Spectral Simple SSW Cube for HIPE... ");
			InterpreterUtil.println("Creating Spectral Simple SSW Cube for HIPE... ");
			SpectralSimpleCube outCubeSSW = new SpectralSimpleCube();
			outCubeSSW.setMeta(cubeSSW_HIPE.getMeta());
			outCubeSSW.setDescription("IsoResolution SSW cube for the specified wave number (IsoWave)");
			outCubeSSW.setUnit(FluxDensity.WATTS_PER_SQUARE_METER_PER_HERTZ.divide(SolidAngle.STERADIANS));
			System.out.print("Image... ");
			InterpreterUtil.println("Image... ");
			outCubeSSW.setImage(new Double3d(SSW_HIPE));
			System.out.print("WCS... ");
			InterpreterUtil.println("WCS... ");
			outCubeSSW.setWcs(cubeSSW_HIPE.getWcs());
			System.out.println("Done !... ");
			InterpreterUtil.println("Done !... ");
			
			//SLW
			System.out.print("Extract Cube SLW... ");
			InterpreterUtil.println("Extract Cube SLW... ");
			MWNumericArray SLW = (MWNumericArray)outSupreme[1]; 
			double[][][] SLW_HIPE = (double[][][]) SLW.toDoubleArray();
			
			//Creating HIPE SSW Cube
			System.out.print("Creating Spectral Simple SLW Cube for HIPE... ");
			InterpreterUtil.println("Creating Spectral Simple SLW Cube for HIPE... ");
			SpectralSimpleCube outCubeSLW = new SpectralSimpleCube();
			outCubeSLW.setMeta(cubeSLW_HIPE.getMeta());
			outCubeSLW.setDescription("IsoResolution SLW cube for the specified wave number (IsoWave)");
			outCubeSLW.setUnit(FluxDensity.WATTS_PER_SQUARE_METER_PER_HERTZ.divide(SolidAngle.STERADIANS));
			System.out.print("Image... ");
			InterpreterUtil.println("Image... ");
			outCubeSLW.setImage(new Double3d(SLW_HIPE));
			System.out.print("WCS... ");
			InterpreterUtil.println("WCS... ");
			outCubeSLW.setWcs(cubeSLW_HIPE.getWcs());
			System.out.println("Done ! ");
			InterpreterUtil.println("Done !");
			
			//METADATA
			MetaData metaCube = new MetaData();
			metaCube.set("creator", new StringParameter("SUPREME Spectro"));
			metaCube.set("formatVersion", new StringParameter("Version 1.5"));
			metaCube.set("instrument", new StringParameter("SPIRE Spectro"));
			metaCube.set("IsoWave", new StringParameter(isoWave.toString()));
			
			outCubeSSW.setMeta(metaCube);
			outCubeSLW.setMeta(metaCube);
			
			System.out.println(" & Adding them to outList");
			outList.add(outCubeSSW);
			outList.add(outCubeSLW);
		
		} catch (MWException e) {
			
			System.out.println("Supreme Spectro Error");
			e.printStackTrace();
		}
		
		
        setValue("outList", outList);
        
	}

}
