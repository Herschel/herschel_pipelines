To generate the SupremeSpectro Plugin for HIPE:

- Create the matlab jar file that will contain all the matlab routines needed for SupremeSpectro to work:
    Create a top routine above all that will call all the others.
    We do that to only call one fonction from the matlab code package in the Java SupremeSpectroCubeMakingTask and SupremeSpectroIsoResCubes tasks.
    Once done, from your working directory, launch matlab.
    In the console, type deploytool.
    Select Library Compiler.
    In the TYPE section, select Java Package.
    Fill in the library information like SupremeSpectroMatlab2Java for library name, your name, email and institut.
    In the EXPORTED FUNCTION section, add all the matlab files needed.
        These files can be found in /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/
    Then change the Class Name from Class1 to SupremeSpectroMatlab
    Then click on the PACKAGE check icon.
    This will create a SupremeSpectroMatlab2Java folder and a SupremeSpectroMatlab2Java.prj file in you working directory.
    Copy the SupremeSpectroMatlab2Java.jar to the jars directory:
        cp SupremeSpectroMatlab2Java/for_redistribution_files_only/SupremeSpectroMatlab2Java.jar jars/

- Prepare in Eclipse or Netbeans the Java SupremeSpectroCubeMakingTask and SupremeSpectroIsoResCubestask tasks that will call the matlab code:
    First read the documentation at http://hipecommunity.wikispaces.com/Creating+your+first+Java+task
    Follow the "Installing and configuring Eclipse" instructions.
    Follow the "Writing the task in Eclipse" instructions but use:
        SupremeSpectroTask as the name project.
        supremeSpectroPlugin as the package name.
        SupremeSpectroMapMakingTask as the class name created.
        SupremeSpectroIsoResCubestask as the class name for the second class.
    Once done, you can copy the content of vault/SupremeSpectroCubeMakingTask.java and vault/SupremeSpectroIsoResCubes in the classes created.
    Make sure to import:
        import SupremeSpectroMatlab2Java.*;
        import com.mathworks.toolbox.javabuilder.*;
    
    Finally, Export the project to a Java / Jar file in the folder jars as SupremeSpectroTask.jar
    
At this point we have created two jar files in the folder jars, SupremeSpectroMatlab2Java.jar and SupremeSpectroTask.jar
The folder already contains a javabuilder.jar file that is needed in the java task (import com.mathworks.toolbox.javabuilder.*;) to use dataypes that will be sent to the Core Matlab code. 

- Now we need to make the Jython SupremeSpectroGetDataFromObs and SupremeSpectroGetDataFromFits tasks that will prepare the data to be sent to the Java tasks.
    See plugin.py in the working directory.
    In this script we define the SupremeSpectroGetDataFromObs and SupremeSpectroGetDataFromFits classes.
    
    Then we register the four tasks (Java and Jython):
    
    toolRegistry = TaskToolRegistry.getInstance()

    print "Installing SupremeSpectroGetDataFromObs...",
    toolRegistry.register(SupremeSpectroGetDataFromObs(),[Category.SPIRE])

    print "SupremeSpectroGetDataFromFits...",
    toolRegistry.register(SupremeSpectroGetDataFromFits(),[Category.SPIRE])

    print "SupremeSpectroCubeMakingTask...",
    from pluginSupremeSpectro import SupremeSpectroCubeMakingTask
    toolRegistry.register(SupremeSpectroCubeMakingTask(), [Category.SPIRE])

    print "SupremeSpectroIsoResCubes...",
    from pluginSupremeSpectro import SupremeSpectroIsoResCubes
    toolRegistry.register(SupremeSpectroIsoResCubes(), [Category.SPIRE])

    del(toolRegistry)
    print " Done !"

- Finally, from the working directory, create the HIPE plugin jar with:
    jar cf name_of_plugin.jar jars plugin.py plugin.xml

