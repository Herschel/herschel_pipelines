############ SupremeSpecto Jython and Java Tasks ############

from herschel.share.unit import Angle
from herschel.ia.obs.auxiliary.pointing import PointingUtils
from herschel.share.fltdyn.time import FineTime
from herschel.spire.ia.util import SpireMask
from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.toolbox.util import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from herschel.spire.ia.pipeline.phot.fluxconv import *
from herschel.ia.obs.util import ObsParameter
from java.util import List

from java.lang import *
from java.util import *
import time


print "Installing SupremeSpectroGetDataFromObs...",
class SupremeSpectroGetDataFromObs(Task):

	def __init__(self,name = "supremeSpectroGetDataFromObs"):
	
		version = Configuration.getProjectInfo().getVersion().split('.')
		if int(version[0]) > 5:
			self.setDescription("This task gets all the data and information from a list of observation IDs (in the local "+\
			"pool or from the HSA) that will be used in the SupremeSpecto task. The list of observation IDs and the band to process must be given."+\
			"You can use either a poolname or useHSA to retrieve the observations.")
		
		#Input
		p = TaskParameter("obsids",valueType = List, mandatory = 1, type = IN,\
				description = "Obsids of the observations to process. List")
		self.addTaskParameter(p)
		p = TaskParameter("bandName",valueType = String, mandatory = 1, type = IN,\
				description = "Band to process.")
		self.addTaskParameter(p)
		p = TaskParameter("poolNames",valueType = List, mandatory = 0, type = IN,\
				defaultValue=[], \
				description = "List of pool names from where the observations are loaded.")
		self.addTaskParameter(p)
		p = TaskParameter("useHsa",valueType = Boolean, mandatory = 0, type = IN,\
				defaultValue=True, \
				description = "UseHSA if you want to get the observations from Herschel Science Archive.")
		self.addTaskParameter(p)
		p = TaskParameter("apodized",valueType = Boolean, mandatory = 0, type = IN,\
				defaultValue=False, \
				description = "Choose apodized_spectrum or unapodized_spectrum.")
		self.addTaskParameter(p)
		p = TaskParameter("resolution",valueType = String, mandatory = 0, type = IN,\
				defaultValue="HR", \
				description = "Choose spectral resolution in case of H+LR observation.")
		self.addTaskParameter(p)
		p = TaskParameter("saveSpectrum2d",valueType = String, mandatory = 0, type = IN,\
				defaultValue="None", \
				description = "Choose folder where to eventualy save the spectrum2d.")
		self.addTaskParameter(p)
		p = TaskParameter("spectrumPrefix",valueType = String, mandatory = 0, type = IN,\
				defaultValue="None", \
				description = "Apply a specific prefix to spectrum2d to ensure the same name is present for the same field.")
		self.addTaskParameter(p)
		
		#Output
		p = TaskParameter("data",valueType = ListContext,type = OUT,\
				description = "ListContext containing all the data needed by SupremeSpectro.")
		self.addTaskParameter(p)
		__all__=['SupremeSpectroGetDataFromObs']
	
	def execute(self):
	
		#Reading data
		obsids = self.obsids
		bandName = self.bandName
		useHsa = self.useHsa
		poolNames = self.poolNames
		apodized = self.apodized
		resolution = self.resolution
		spectrum2dDir = self.saveSpectrum2d
		spectrumPrefix = self.spectrumPrefix
		
		if (len(poolNames)!=0) and (len(obsids)!=len(poolNames)):
			raise Error("The obsids and poolNames Lists don't have the same size.\n You need to give corresponding poolNames and obsids.")
		
		BAND_ALL = ""
		APOD_ALL = ""
		RES_ALL = ""
		
		#Creating Listcontext
		dataObsList = ListContext()
		for i in range(len(obsids)):
		
			obsid = obsids[i]
			
			print 5*"#"+" Get data from obsid "+str(obsid)
			if useHsa: 
				obs = getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
			else: 
				poolName = poolNames[i]
				obs = getObservation(obsid=obsid,poolName=poolName,instrument="SPIRE")
			
			metaObs = obs.meta
			if metaObs["mapSampling"].value == "sparse": 
				if len(obsids)==1:
					raise Error("This observation ("+str(obsid)+") is a Sparse Sampling observation.\nThe Supreme Spectro plugin only works with Intermediate and Full Sampling observations.")
				else:
					print "This observation ("+str(obsid)+") will be ignored."
					continue
			
#			if apodized: productName = 'apodized_spectrum'
#			else: productName = 'unapodized_spectrum'
			
			res = metaObs["commandedResolution"].value
			object = metaObs["object"].value
			apodName = {True:"apodized",False:"unapodized"}
			
			NbJiggId = obs.refs["level1"].product.getRefs().size()
			if res == "H+LR":
				NbJiggId = NbJiggId/2
				res = resolution
				if resolution != "HR" or resolution != "LR": res = "HR"
			
			if i==0:
				RES_ALL = res
			else:
				if res != RES_ALL: raise Error("The data from "+str(obsid)+"don't have the same processed spectral resolution as the first obsid.")
			
#			sdsList = SpireMapContext()
#			for jiggId in range(0,NbJiggId):
#				sdslist_temp = obs.refs["level1"].product.refs["Point_0_Jiggle_"+str(jiggId)+"_"+res].product.refs[productName].product
#				sdsList.setProduct("%d"%(sdsList.refs.size()), sdslist_temp)
#			del sdslist_temp
#			
#			preCube = spirePreprocessCube(context=sdsList, arrayType=bandName, unvignetted=True)
#			preCubeAsSpectrum2d = preCube.toSpectrum2d()
#			del preCube, sdsList, obs
			
			if apodized: preCubeAsSpectrum2d = obs.refs["level2"].product.refs["_".join([res,bandName,"spectrum2d_apod"])].product["spectrum2d"]
			else: preCubeAsSpectrum2d = obs.refs["level2"].product.refs["_".join([res,bandName,"spectrum2d"])].product["spectrum2d"]
			
			#data
			prod = Product(creator="HIPE", instrument="SPIRE", type="Product for SupremeSpectro", description="Data for SupremeSpectro - obsid "+str(obsid))
			prod["flux"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("flux").getData())
			prod["error"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("error").getData())
			prod["wave"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("wave").getData())
			prod["longitude"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("longitude").getData())
			prod["latitude"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("latitude").getData())
			prod["detector"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("detector").getData())
			prod["mask"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("mask").getData())
			
			metaMain = metaObs.copy()
			metaMain["wavelength"]=StringParameter(str(bandName),"Wavelength")
			
			metaSpectrum = preCubeAsSpectrum2d.meta.copy()
			metaSpectrum_Keys = metaSpectrum.keySet()
			for key in metaSpectrum_Keys:
				if metaMain.containsKey(key)==False: metaMain[key]=metaSpectrum[key]
				#else: print key, "already in metaMain"
			
			#Save Spectrum2d fits
			if spectrum2dDir!="None":
				prodSpectumFits=Product()
				prodSpectumFits["spectrum"]=preCubeAsSpectrum2d.copy()
				prodSpectumFits.meta=metaMain.copy()
				try: 
					os.mkdir(spectrum2dDir)
				except: 
					print "Can not create ",spectrum2dDir
				object_tmp = object.replace(" ", "_")
				if (spectrumPrefix != "None"):
					spectrum2dFile = spectrum2dDir+spectrumPrefix+"_spectrum2d.fits"
					else:
						spectrum2dFile = spectrum2dDir+str(obsid)+"_"+object_tmp+"_"+bandName+"_"+res+"_"+apodName[apodized]+"_spectrum2d.fits"
				try: 
					simpleFitsWriter(prodSpectumFits, spectrum2dFile)
				except: 
					print spectrum2dFile, "is not valid !"
				del prodSpectumFits
			
			prod.meta = metaMain.copy()
			dataObsList.refs.add(ProductRef(prod))
			
			#Building meta for the combined obsids
			if i==0:
				metaMultipleObs = metaMain.copy()
			else:
				for key in metaMain.keySet():
					if metaMultipleObs.containsKey(key)==True and metaMultipleObs[key].value!=metaMain[key].value:
						description = metaMain[key].description
						value = str(metaMultipleObs[key].value)+" & "+str(metaMain[key].value)
						try:
							unit = metaMain[key].unit
							metaMultipleObs[key] = StringParameter(value, description, unit)
						except:
							metaMultipleObs[key] = StringParameter(value, description)
			
			print 5*"#"+str(obsid)+" Retrieving Data for SupremeSpectro Done !"
		
		dataObsList.meta = metaMultipleObs.copy()
		self.data = dataObsList

# Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremeSpectroGetDataFromObs(),[Category.SPIRE])
del(toolRegistry)
print " Done !"

print "Installing SupremeSpectroGetDataFromFits...",
class SupremeSpectroGetDataFromFits(Task):

	def __init__(self,name = "supremeSpectroGetDataFromFits"):
	
		version = Configuration.getProjectInfo().getVersion().split('.')
		if int(version[0]) > 5:
			self.setDescription("This task gets all the data and information from a list of Spectrum2d fits file (previously created with" +\
			" the SupremeSpectroGetDataFromObs task) that will be used in the SupremeSpecto task.")
		
		#Input
		p = TaskParameter("fitsfiles",valueType = List, mandatory = 1, type = IN,\
				description = "Spectrum2d fits files to process. List")
		self.addTaskParameter(p)
		
		#Output
		p = TaskParameter("data",valueType = ListContext,type = OUT,\
				description = "ListContext containing all the data needed by SupremeSpectro.")
		self.addTaskParameter(p)
		__all__=['SupremeSpectroGetDataFromFits']
	
	def execute(self):
	
		#Reading data
		fitsfiles = self.fitsfiles
		
		dataObsList = ListContext()
		
		BAND_ALL = ""
		APOD_ALL = ""
		RES_ALL = ""
		
		for i in range(len(fitsfiles)):
		
			fits = fitsReader(fitsfiles[i])
			metaFits = fits.getMeta()
			metaSpectrum = fits["spectrum"].getMeta()
			
			try: band = metaFits["wavelength"]
			except: band = metaSpectrum["wavelength"]
			try: apod = metaFits["apodName"]
			except: apod = metaSpectrum["apodName"]
			try: res = metaFits["processResolution"]
			except: res = metaSpectrum["processResolution"]
			try: obsid = metaFits["obsid"]
			except: obsid = metaSpectrum["obsid"]
			
			#Is not a sparse sampling observation
			if metaFits["mapSampling"].value == "sparse":
				if len(fitsfiles)==1:
					raise Error("This observation ("+str(obsid)+") is a Sparse Sampling observation.\nThe Supreme Spectro plugin only works with Intermediate and Full Sampling observations.")
				else:
					print str(obsid)+" will be ignored."
					continue
			
			if i==0:
				BAND_ALL = band
				APOD_ALL = apod
				RES_ALL = res
			else:
				if band != BAND_ALL: raise Error("The data from "+str(fitsfile)+"don't have the same band as the first fitsfile.")
				if apod != APOD_ALL: raise Error("The data from "+str(fitsfile)+"don't have the same apodisation as the first fitsfile.")
				if res != RES_ALL: raise Error("The data from "+str(fitsfile)+"don't have the same processed spectral resolution as the first fitsfile.")
			
			preCubeAsSpectrum2d = fits["spectrum"]
			prod = Product(creator="HIPE", instrument="SPIRE", type="Product for SupremeSpectro", description="Data for SupremeSpectro - obsid "+str(obsid))
			prod["flux"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("flux").getData())
			prod["error"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("error").getData())
			prod["wave"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("wave").getData())
			prod["longitude"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("longitude").getData())
			prod["latitude"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("latitude").getData())
			prod["detector"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("detector").getData())
			prod["mask"] = ArrayDataset(preCubeAsSpectrum2d.getColumn("mask").getData())
			
			
			metaMain = metaFits.copy()
			metaSpectrum_Keys = metaSpectrum.keySet()
			
			#Building meta for each obsids
			for key in metaSpectrum_Keys:
				if metaMain.containsKey(key)==False: metaMain[key]=metaSpectrum[key]
				#else: print key, "already in metaMain"
			
			prod.meta = metaMain.copy()
			dataObsList.refs.add(ProductRef(prod))
			
			#Building meta for the combined obsids
			if i==0:
				metaMultipleObs = metaMain.copy()
			else:
				for key in metaMain.keySet():
					if metaMultipleObs.containsKey(key)==True and metaMultipleObs[key].value!=metaMain[key].value:
						description = metaMain[key].description
						value = str(metaMultipleObs[key].value)+" "+str(metaMain[key].value)
						try:
							unit = metaMain[key].unit
							metaMultipleObs[key] = StringParameter(value, description, unit)
						except:
							metaMultipleObs[key] = StringParameter(value, description)
		
		dataObsList.meta = metaMultipleObs.copy()
		self.data = dataObsList

# Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremeSpectroGetDataFromFits(),[Category.SPIRE])
del(toolRegistry)
print " Done !"

print "Installing SupremeSpectroCubeMakingTask...",
from pluginSupremeSpectro import SupremeSpectroCubeMakingTask
# Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremeSpectroCubeMakingTask(), [Category.SPIRE])
del(toolRegistry)
print " Done !"

print "Installing SupremeSpectroIsoResCubes...",
from pluginSupremeSpectro import SupremeSpectroIsoResCubes
# Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremeSpectroIsoResCubes(), [Category.SPIRE])
del(toolRegistry)
print " Done !"
