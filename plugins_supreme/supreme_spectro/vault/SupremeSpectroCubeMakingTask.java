package pluginSupremeSpectro;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import SupremeSpectroMatlab2Java.*;
import com.mathworks.toolbox.javabuilder.*;

import herschel.ia.task.Task;
import herschel.ia.task.TaskParameter;
import herschel.ia.pal.ListContext;
import herschel.ia.dataset.*;
import herschel.ia.numeric.*;
import herschel.ia.dataset.image.SimpleCube;
import herschel.ia.dataset.spectrum.SpectralSimpleCube;
import herschel.share.unit.FluxDensity;
import herschel.share.unit.SolidAngle;
import herschel.ia.dataset.image.wcs.Wcs;
import herschel.share.interpreter.InterpreterUtil;


public class SupremeSpectroCubeMakingTask extends Task {

	public SupremeSpectroCubeMakingTask() {
    	
        super("supremeSpectroCubeMaking");
        setDescription("Super Resolution algorithm for L1 SPIRE FTS Data");
                
        //data
        TaskParameter parameter = new TaskParameter("data", ListContext.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("ListContext containing all the Product data");
        addTaskParameter(parameter);
        
        //alphaStep
        parameter = new TaskParameter("AlphaStep", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Alpha step angle in arcsec");
        addTaskParameter(parameter);
        
        //RhosOp
        parameter = new TaskParameter("RhosOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("GetRhos"));
        parameter.setDescription("'GetRhos' or 'SetRhos'");
        addTaskParameter(parameter);
        
        //RhosVal
        parameter = new TaskParameter("RhosVal", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Double(0));
        parameter.setDescription("Rhos Value");
        addTaskParameter(parameter);
        
        //RhonOp
        parameter = new TaskParameter("RhonOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("GetRhonFull"));
        parameter.setDescription("'GetRhonFull' 'GetRhonPartial' 'GetRhonIso' or 'SetRhon'");
        addTaskParameter(parameter);
        
        //RhonVal
        parameter = new TaskParameter("RhonVal", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Double(0));
        parameter.setDescription("Rhon Value");
        addTaskParameter(parameter);
        
        //PointingOp
        parameter = new TaskParameter("PointingOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("Bilinear"));
        parameter.setDescription("'Naive' or 'Bilinear'");
        addTaskParameter(parameter);
        
        //StopMode
        parameter = new TaskParameter("StopMode", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("Manual"));
        parameter.setDescription("'Automatic' (based on free energy evolution) or 'Manual'");
        addTaskParameter(parameter);
        
        //StopValue
        parameter = new TaskParameter("StopValue", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Determine the stoping value according to stopping mode");
        addTaskParameter(parameter);
        
        //WCS
        parameter = new TaskParameter("inputWCS", Wcs.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDescription("WCS of a processed SSW SupremeSpectro cube");
        addTaskParameter(parameter);
        
        //display
        parameter = new TaskParameter("display", Integer.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Integer(0));
        parameter.setDescription("Dislay the evolution of the computation or not");
        addTaskParameter(parameter);
        
        //outList
        parameter = new TaskParameter("outList", ArrayList.class);
        parameter.setType(TaskParameter.OUT);
        parameter.setMandatory(false);
        parameter.setDescription("List of Product: Output Cube, Estimated standard deviation Cube, Extra");
        addTaskParameter(parameter);

        
    }
    
	public void execute() {
    	        
    	ListContext data = (ListContext) getParameter("data").getValue();
		Double AlphaStep = (Double) getParameter("AlphaStep").getValue();
		String RhosOp = (String) getParameter("RhosOp").getValue();
		Double RhosVal = (Double) getParameter("RhosVal").getValue();
		String RhonOp = (String) getParameter("RhonOp").getValue();
		Double RhonVal = (Double) getParameter("RhonVal").getValue();
		String PointingOp = (String) getParameter("PointingOp").getValue();
		String StopMode = (String) getParameter("StopMode").getValue();
		Double StopValue = (Double) getParameter("StopValue").getValue();
    	Integer display = (Integer) getParameter("display").getValue();
    	Wcs WCS_SSW = (Wcs) getParameter("inputWCS").getValue();

    	//Input integrity Check
    	
    	if (data==null) {throw new NullPointerException("ListContext data is Null");}
    	
    	if (AlphaStep<0) {throw new IllegalArgumentException("AlphaStep should be positive.");}
    	
    	List<String> RhosOpList = new ArrayList<String>();
        String RhosOp_Values[]={"GetRhos","SetRhos"};
        for (int i = 0; i < RhosOp_Values.length; i++) {RhosOpList.add(RhosOp_Values[i]);}
        if(!RhosOpList.contains(RhosOp)) {throw new IllegalArgumentException("RhosOp - "+RhosOp+" is not a valid RhosOp option.");}
        
    	if (RhosVal<0) {throw new IllegalArgumentException("RhosVal should be positive.");}
        
        List<String> RhonOpList = new ArrayList<String>();
        String RhonOp_Values[]={"GetRhonFull","GetRhonPartial","GetRhonIso","SetRhon"};
        for (int i = 0; i < RhonOp_Values.length; i++) {RhonOpList.add(RhonOp_Values[i]);}
        if(!RhonOpList.contains(RhonOp)) {throw new IllegalArgumentException("RhonOp - "+RhonOp+" is not a valid RhonOp option.");}
        
    	if (RhonVal<0) {throw new IllegalArgumentException("RhonVal should be positive.");}
    	
        List<String> PointingOpList = new ArrayList<String>();
        String PointingOp_Values[]={"Naive","Bilinear"};
        for (int i = 0; i < PointingOp_Values.length; i++) {PointingOpList.add(PointingOp_Values[i]);}
        if(!PointingOpList.contains(PointingOp)) {throw new IllegalArgumentException("PointingOp - "+PointingOp+" is not a valid PointingOp option.");}
        
        List<String> StopModeList = new ArrayList<String>();
        String StopMode_Values[]={"Automatic","Manual"};
        for (int i = 0; i < StopMode_Values.length; i++) {StopModeList.add(StopMode_Values[i]);}
        if(!StopModeList.contains(StopMode)) {throw new IllegalArgumentException("StopMode - "+StopMode+" is not a valid StopMode option.");}

    	if (StopValue<0) {throw new IllegalArgumentException("StopValue should be positive.");}
    	
    	boolean forceWCS = false;
    	try {
    		if (WCS_SSW.isValid()){forceWCS=true;}
    	} catch (Exception e) {
    		System.out.println("WCS to be calculated... ");
        	InterpreterUtil.println("WCS to be calculated... ");;
		}
    	
    	//Input integrity Check Done !!
    	
        boolean verbose = true;
        
        List<Object> outList = new ArrayList<Object>();
        String obsids = new String("");
        String objectName = new String();
                
        try {
        	 
        	Integer nbrObs = data.getAllRefs().size();
            int[] dim = {10,nbrObs};
            MWCellArray Data = new MWCellArray(dim);
            objectName = (String)(data.getRefs().get(0).getProduct().getMeta().get("object").getValue());
            MetaData metaDataObs = data.getMeta();
            
            for(int i=0;i<nbrObs;i++){

            	Product dataObs = new Product();
            	dataObs = data.getRefs().get(i).getProduct();
            	
            	Long obsid = (Long)(dataObs.getMeta().get("obsid").getValue());
            	
            	obsids = obsids + obsid.toString() + " ";
            	
            	System.out.println("Preparing Data for SupremePhoto from obsid "+obsid+" ... ");
            	InterpreterUtil.println("Preparing Data for SupremePhoto from obsid "+obsid+" ... ");
            	
            	double[][] sigObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("flux")).getData()).getArray();
            	MWNumericArray sigMat = new MWNumericArray(sigObs, MWClassID.DOUBLE);
            	Data.set(new int[] {1, i+1}, sigMat);
            	if (verbose){System.out.print("#flux ");}
            	            	
            	double[][] errorObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("error")).getData()).getArray();
            	MWNumericArray errorMat = new MWNumericArray(errorObs, MWClassID.DOUBLE);
            	Data.set(new int[] {2, i+1}, errorMat);
            	if (verbose){System.out.print("#error ");}
            	
            	double[][] waveObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("wave")).getData()).getArray();
            	MWNumericArray waveMat = new MWNumericArray(waveObs, MWClassID.DOUBLE);
            	Data.set(new int[] {3, i+1}, waveMat);
            	if (verbose){System.out.print("#wave ");}
            	
            	double[] lonObs = ((Double1d) ((ArrayDataset) dataObs.getSets().get("longitude")).getData()).getArray();
            	MWNumericArray lonMat = new MWNumericArray(lonObs, MWClassID.DOUBLE);
            	Data.set(new int[] {4, i+1}, lonMat);
            	if (verbose){System.out.print("#lon ");}
            	
            	double[] latObs = ((Double1d) ((ArrayDataset) dataObs.getSets().get("latitude")).getData()).getArray();
            	MWNumericArray latMat = new MWNumericArray(latObs, MWClassID.DOUBLE);
            	Data.set(new int[] {5, i+1}, latMat);
            	if (verbose){System.out.print("#lat ");}
            	
            	String[] detObs = ((String1d) ((ArrayDataset) dataObs.getSets().get("detector")).getData()).getArray();
            	MWCharArray detMat = new MWCharArray(detObs);
            	Data.set(new int[] {6, i+1}, detMat);
            	if (verbose){System.out.print("#detector ");}
            	
            	int[][] mskObs = ((Int2d) ((ArrayDataset) dataObs.getSets().get("mask")).getData()).getArray();
            	MWNumericArray mskMat = new MWNumericArray(mskObs, MWClassID.INT8);
            	Data.set(new int[] {7, i+1}, mskMat);
            	if (verbose){System.out.print("#mask ");}
            	
            	double ra_nomObs = (Double)(dataObs.getMeta().get("raNominal").getValue());
            	MWNumericArray ra_nomMat = new MWNumericArray(ra_nomObs, MWClassID.DOUBLE);
            	Data.set(new int[] {8, i+1}, ra_nomMat);
            	if (verbose){System.out.print("#ranom ");}
            	
            	double dec_nomObs = (Double)(dataObs.getMeta().get("decNominal").getValue());
            	MWNumericArray dec_nomMat = new MWNumericArray(dec_nomObs, MWClassID.DOUBLE);
            	Data.set(new int[] {9, i+1}, dec_nomMat);
            	if (verbose){System.out.print("#decnom ");}
            	
            	
            	String[] WCStructFields = {"NAXIS1", "NAXIS2", "NAXIS3", "CDELT1", "CDELT2", "CDELT3", "CRVAL1", "CRVAL2", "CRVAL3", "CRPIX1", "CRPIX2", "CRPIX3"};
            	MWStructArray WCS_toMatlab = new MWStructArray(1, 1, WCStructFields);
            	if (forceWCS) {
            		            		
            		int naxis1 = WCS_SSW.getNaxis1();
            		int naxis2 = WCS_SSW.getNaxis2();
            		int naxis3 = WCS_SSW.getNaxis3();
    				
            		double cdelt1 = WCS_SSW.getCdelt1();
            		double cdelt2 = WCS_SSW.getCdelt2();
            		double cdelt3 = WCS_SSW.getCdelt3();
    				
            		double crval1 = WCS_SSW.getCrval1();
            		double crval2 = WCS_SSW.getCrval2();
            		double crval3 = WCS_SSW.getCrval3();
    				
            		double crpix1 = WCS_SSW.getCrpix1();
            		double crpix2 = WCS_SSW.getCrpix2();
            		double crpix3 = WCS_SSW.getCrpix3();
            		
            		WCS_toMatlab.set("NAXIS1", 1, naxis1);
            		WCS_toMatlab.set("NAXIS2", 1, naxis2);
            		WCS_toMatlab.set("NAXIS3", 1, naxis3);
            		WCS_toMatlab.set("CDELT1", 1, cdelt1);
            		WCS_toMatlab.set("CDELT2", 1, cdelt2);
            		WCS_toMatlab.set("CDELT3", 1, cdelt3);
            		WCS_toMatlab.set("CRVAL1", 1, crval1);
            		WCS_toMatlab.set("CRVAL2", 1, crval2);
            		WCS_toMatlab.set("CRVAL3", 1, crval3);
            		WCS_toMatlab.set("CRPIX1", 1, crpix1);
            		WCS_toMatlab.set("CRPIX2", 1, crpix2);
            		WCS_toMatlab.set("CRPIX3", 1, crpix3);
            		
            		Data.set(new int[] {10, i+1}, WCS_toMatlab);
            		if (verbose){System.out.print("#WCS ");}
            		
            	}
            	
            	else {
            		
            		MWNumericArray zero = new MWNumericArray(0, MWClassID.DOUBLE);
            		WCS_toMatlab.set("NAXIS1", 1, zero);
            		WCS_toMatlab.set("NAXIS2", 1, zero);
            		WCS_toMatlab.set("NAXIS3", 1, zero);
            		WCS_toMatlab.set("CDELT1", 1, zero);
            		WCS_toMatlab.set("CDELT2", 1, zero);
            		WCS_toMatlab.set("CDELT3", 1, zero);
            		WCS_toMatlab.set("CRVAL1", 1, zero);
            		WCS_toMatlab.set("CRVAL2", 1, zero);
            		WCS_toMatlab.set("CRVAL3", 1, zero);
            		WCS_toMatlab.set("CRPIX1", 1, zero);
            		WCS_toMatlab.set("CRPIX2", 1, zero);
            		WCS_toMatlab.set("CRPIX3", 1, zero);
            		
            		Data.set(new int[] {10, i+1}, WCS_toMatlab);
            		if (verbose){System.out.print("#NoWcs ");}
            	}
            	
            	       	
            	System.out.println("Done!");
            	InterpreterUtil.println("Done!");
            	
    		}
        	
			System.out.println("Starting SupremeSpectroCubeMaking Core Task...");
			InterpreterUtil.println("Starting SupremeSpectroCubeMaking Core Task...");
			SupremeSpectroMatlab x = new SupremeSpectroMatlab();
			int fitsMatlab = 0;
			Object[] outSupreme = x.SupremeSpectro_cubeMaking(8,objectName,nbrObs,Data,AlphaStep,RhosOp,RhosVal,RhonOp,RhonVal,PointingOp,StopMode,StopValue,display,fitsMatlab);
			//[Xh Xstd XerrCube Xcoadd Wave Extra WCS isGood]
			
			MWNumericArray isGood = (MWNumericArray)outSupreme[7];
			if (isGood.getInt()==1){
				
				//Extract Cube
				System.out.print("Extract Cube... ");
				InterpreterUtil.println("Extract Cube... ");
				MWNumericArray Xh = (MWNumericArray)outSupreme[0]; 
				double[][][] Xh_cube = (double[][][]) Xh.toDoubleArray();		
				
				//Extract Cube Error
				System.out.print("CubeError... ");
				InterpreterUtil.println("CubeError... ");
				MWNumericArray Xerr = (MWNumericArray)outSupreme[2]; 
				double[][][] Xerr_cube = (double[][][]) Xerr.toDoubleArray();
				
				//Extract Cube Coadd
				System.out.print("CubeCoadd... ");
				InterpreterUtil.println("CubeCoadd... ");
				MWNumericArray Xcoadd = (MWNumericArray)outSupreme[3]; 
				double[][][] Xcoadd_cube = (double[][][]) Xcoadd.toDoubleArray();
				
				//Extract Wave
				/*System.out.print("Wave... ");
				InterpreterUtil.println("Wave... ");
				MWNumericArray Wave = (MWNumericArray)outSupreme[4]; 
				double[][] Wave_array = (double[][]) Wave.toDoubleArray();
				*/
				
				//Extract and Create WCS
				System.out.println("Extract & Create WCS for the Cube... ");
				InterpreterUtil.println("Extract & Create WCS for the Cube... ");
				MWStructArray WCS = (MWStructArray)outSupreme[6];
				//System.out.println(WCS);
				//InterpreterUtil.println(""+WCS);
				
				MWNumericArray cdelt1 = (MWNumericArray)WCS.getField("CDELT1", 1);
				MWNumericArray cdelt2 = (MWNumericArray)WCS.getField("CDELT2", 1);
				MWNumericArray cdelt3 = (MWNumericArray)WCS.getField("CDELT3", 1);
				
				MWNumericArray crval1 = (MWNumericArray)WCS.getField("CRVAL1", 1);
				MWNumericArray crval2 = (MWNumericArray)WCS.getField("CRVAL2", 1);
				MWNumericArray crval3 = (MWNumericArray)WCS.getField("CRVAL3", 1);
				
				MWNumericArray crpix1 = (MWNumericArray)WCS.getField("CRPIX1", 1);
				MWNumericArray crpix2 = (MWNumericArray)WCS.getField("CRPIX2", 1);
				MWNumericArray crpix3 = (MWNumericArray)WCS.getField("CRPIX3", 1);
				
				MWNumericArray naxis1 = (MWNumericArray)WCS.getField("NAXIS1", 1);
				MWNumericArray naxis2 = (MWNumericArray)WCS.getField("NAXIS2", 1);
				MWNumericArray naxis3 = (MWNumericArray)WCS.getField("NAXIS3", 1);
				
				Wcs wcs = new Wcs(3);
				
				wcs.setNAxis(3);
				wcs.setNaxis1(naxis1.getInt());
				wcs.setNaxis2(naxis2.getInt());
				wcs.setNaxis3(naxis3.getInt());
				
				wcs.setCdelt1(cdelt1.getDouble());
				wcs.setCdelt2(cdelt2.getDouble());
				wcs.setCdelt3(cdelt3.getDouble());
				
				wcs.setCrval1(crval1.getDouble());
				wcs.setCrval2(crval2.getDouble());
				wcs.setCrval3(crval3.getDouble());
				
				wcs.setCrpix1(crpix1.getDouble());
				wcs.setCrpix2(crpix2.getDouble());
				wcs.setCrpix3(crpix3.getDouble());
				
				wcs.setCunit1("deg");
				wcs.setCunit2("deg");
				wcs.setCunit3("GHz");
				
				wcs.setCtype1("RA---TAN");
				wcs.setCtype2("DEC--TAN");
				wcs.setCtype3("wave");
				
				wcs.setCrota2(0);
				wcs.setEpoch(2000.0);
				wcs.setEquinox(2000.0);
				
				wcs.setRadesys("Equatorial");
	
				//wcs.setImageIndex(new Double1d(Wave_array[0]), Frequency.GIGAHERTZ);
				
				InterpreterUtil.println(""+wcs);
				
				if (wcs.isValid()){InterpreterUtil.println("Valid WCS... ");}
				if (wcs.isCompleteWcs()){InterpreterUtil.println("Complete WCS... ");}
				if (wcs.hasImageIndex()){InterpreterUtil.println("Valid ImageIndex... ");}
				
				//Creating Main Cube
				System.out.print("Creating Spectral Simple Cube for HIPE... ");
				InterpreterUtil.println("Creating Spectral Simple Cube  for HIPE... ");
				SpectralSimpleCube outCube = new SpectralSimpleCube();
				outCube.setDescription("Output Cube from SupremeSpectro");
				//outCube.setType()
				//outCube.setCreator("SUPREME Spectro Version 1.5");
				//outCube.setInstrument("SPIRE");
				//outCube.setFormatVersion("2.4");
				outCube.setUnit(FluxDensity.WATTS_PER_SQUARE_METER_PER_HERTZ.divide(SolidAngle.STERADIANS));
				InterpreterUtil.println("SpectralSimpleCube Image... ");
				outCube.setImage(new Double3d(Xh_cube));
				InterpreterUtil.println("SpectralSimpleCube Error... ");
				outCube.setError(new Double3d(Xerr_cube));
				InterpreterUtil.println("SpectralSimpleCube Coverage... ");
				outCube.setCoverage(new Double3d(Xcoadd_cube));
				InterpreterUtil.println("SpectralSimpleCube WCS... ");
				outCube.setWcs(wcs);
				//InterpreterUtil.println("SpectralSimpleCube Wave... ");
				//outCube.setWave(new Double1d(Wave_array[0]));
				//outCube.setWaveUnit(Frequency.GIGAHERTZ);
				
				//METADATA
				//MetaData metaCube = new MetaData();
				metaDataObs.set("creator", new StringParameter("SUPREME Spectro Plugin"));
				metaDataObs.set("supreme_version", new StringParameter("Version 1.5"));
				//metaCube.set("instrument", new StringParameter("SPIRE Spectro"));
				metaDataObs.set("obsids", new StringParameter(obsids));
				metaDataObs.set("supreme_AlphaStep", new StringParameter(AlphaStep.toString()));
				metaDataObs.set("supreme_RhonOp", new StringParameter(RhonOp));
				metaDataObs.set("supreme_RhonVal", new StringParameter(RhonVal.toString()));
				metaDataObs.set("supreme_RhosOp", new StringParameter(RhosOp));
				metaDataObs.set("supreme_RhosVal", new StringParameter(RhosVal.toString()));
				metaDataObs.set("supreme_PointingOp", new StringParameter(PointingOp));
				metaDataObs.set("supreme_StopMode", new StringParameter(StopMode));
				metaDataObs.set("supreme_StopValue", new StringParameter(StopValue.toString()));
				outCube.setMeta(metaDataObs);
								
				System.out.println(" & Adding it to outList");
				outList.add(outCube);
				
				//Extract Standard Deviation Cube
				System.out.println("Extract Standard Deviation Cube");
				InterpreterUtil.println("Extract Standard Deviation Cube");
				MWNumericArray Xstd = (MWNumericArray)outSupreme[1]; 
				double[][][] Xstd_cube = (double[][][]) Xstd.toDoubleArray();
							
				//Creating STD Cube
				System.out.print("Creating Standard Deviation Cube for HIPE... ");
				InterpreterUtil.println("Creating Standard Deviation Cube for HIPE... ");
				SimpleCube outCubeSTD = new SimpleCube();
				outCubeSTD.setDescription("Output Standard Deviation Cube from SupremePhoto");
				outCubeSTD.setUnit(FluxDensity.WATTS_PER_SQUARE_METER_PER_HERTZ.divide(SolidAngle.STERADIANS));
				outCubeSTD.setImage(new Double3d(Xstd_cube));
				outCubeSTD.setWcs(wcs);
				
				System.out.println(" & Adding it to outList");
				outList.add(outCubeSTD);
				
				//Extra information
				System.out.print("Extract Extra information... ");
				InterpreterUtil.println("Extract Extra information... ");
				MWStructArray Extra = (MWStructArray)outSupreme[5];
				//MWNumericArray Coadd = (MWNumericArray)Extra.getField("Coadd", 1);
				MWNumericArray FreeEnergy = (MWNumericArray)Extra.getField("FreeEnergy", 1);
				MWNumericArray Rhon = (MWNumericArray)Extra.getField("Rhon", 1);
				MWNumericArray Rhos = (MWNumericArray)Extra.getField("Rhos", 1);
				MWNumericArray VarRhon = (MWNumericArray)Extra.getField("VarRhon", 1);
				MWNumericArray VarRhos = (MWNumericArray)Extra.getField("VarRhos", 1);
				MWNumericArray EqBeam = (MWNumericArray)Extra.getField("EqBeam", 1);
				MWNumericArray fEqBeam = (MWNumericArray)Extra.getField("fEqBeam", 1);
				
	
				Product ExtraHIPE = new Product();
				ExtraHIPE.setDescription("Extra information from SupremeSpectro.");
				//ExtraHIPE.setCreator("SUPREME Spectro Version 1.5");
				//ExtraHIPE.setInstrument("SPIRE");
				//ExtraHIPE.setFormatVersion("2.4");
	
				System.out.print("Rhon... ");
				InterpreterUtil.println("Rhon... ");
				Double3d Rhon_D = new Double3d((double[][][]) Rhon.toDoubleArray());
				ExtraHIPE.getSets().put("Rhon",new ArrayDataset(Rhon_D));
				
				System.out.print("VarRhon... ");
				InterpreterUtil.println("VarRhon... ");
				Double3d VarRhon_D = new Double3d((double[][][]) VarRhon.toDoubleArray());
				ExtraHIPE.getSets().put("VarRhon",new ArrayDataset(VarRhon_D));
				
				System.out.print("Rhos... ");
				InterpreterUtil.println("Rhos... ");
				Double3d Rhos_D = new Double3d((double[][][]) Rhos.toDoubleArray());
				ExtraHIPE.getSets().put("Rhos",new ArrayDataset(Rhos_D));
				
				System.out.print("VarRhos... ");
				InterpreterUtil.println("VarRhos... ");
				Double3d VarRhos_D = new Double3d((double[][][]) VarRhos.toDoubleArray());
				ExtraHIPE.getSets().put("VarRhos",new ArrayDataset(VarRhos_D));
				
				/*
				System.out.print("Coadd... ");
				InterpreterUtil.println("Coadd... ");
				Double3d Coadd_D = new Double3d((double[][][]) Coadd.toDoubleArray());
				ExtraHIPE.getSets().put("Coadd",new ArrayDataset(Coadd_D));
				*/
				
				System.out.print("FreeEnergy... ");
				InterpreterUtil.println("FreeEnergy... ");
				Double2d FreeEnergy_D = new Double2d((double[][]) FreeEnergy.toDoubleArray());
				ExtraHIPE.getSets().put("FreeEnergy",new ArrayDataset(FreeEnergy_D));
				
				System.out.print("EqBeam... ");
				InterpreterUtil.println("EqBeam... ");
				Double3d EqBeam_D = new Double3d((double[][][]) EqBeam.toDoubleArray());
				ExtraHIPE.getSets().put("EqBeam",new ArrayDataset(EqBeam_D));
				
				System.out.println("fEqBeam... ");
				InterpreterUtil.println("fEqBeam... ");
				Double3d fEqBeam_D = new Double3d((double[][][]) fEqBeam.toDoubleArray());
				ExtraHIPE.getSets().put("fEqBeam",new ArrayDataset(fEqBeam_D));
								
				MetaData metaEx = new MetaData();
				metaEx.set("creator", new StringParameter("SUPREME Spectro"));
				metaEx.set("formatVersion", new StringParameter("Version 1.5"));
				metaEx.set("instrument", new StringParameter("SPIRE"));
				metaEx.set("Rhon", new StringParameter(Rhon.toString()));
				metaEx.set("VarRhon", new StringParameter(VarRhon.toString()));
				metaEx.set("Rhos", new StringParameter(Rhos.toString()));
				metaEx.set("VarRhos", new StringParameter(VarRhos.toString()));
				ExtraHIPE.setMeta(metaEx);
				
				System.out.println("Adding Extra Information to outList");
				outList.add(ExtraHIPE);
				
				System.out.println("Final Cubes Product to HIPE !");
				InterpreterUtil.println("Final Cubes Product to HIPE !");
			
			}
			
			else {
				System.out.println("Free Energy has not been calculated. No cubes have been created.");
				InterpreterUtil.println("Free Energy has not been calculated. No cubes have been created.");
			}
			
			
		} catch (MWException e) {
			
			System.out.println("Supreme Spectro Error");
			e.printStackTrace();
			
		} catch (IOException e1) {
			
			System.out.println("IO Exception !!");
			e1.printStackTrace();
			
		} catch (GeneralSecurityException e2) {
			
			System.out.println("General Security Exception !!");
			e2.printStackTrace();
			
		}
           
        setValue("outList", outList);

	}
    	
}

