/*
 * MATLAB Compiler: 6.1 (R2015b)
 * Date: Thu Dec 24 13:05:46 2015
 * Arguments: "-B" "macro_default" "-W" "java:SupremePhotoMatlab2Java,SupremePhotoMatlab" 
 * "-T" "link:lib" "-d" 
 * "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/SupremePhotoMatlab2Java/for_testing" 
 * "class{SupremePhotoMatlab:/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/AppendWave.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Cube2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_info.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_read.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_write.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fitsreadme.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetBeamSize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetFitsL1.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/getFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIndex.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIsoResolutionCubes.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetLegInd.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2D.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/GetSpectrum2D_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2DLE.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetWCS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Map2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/MyFitsRead.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImResize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImRotate.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/PrepareData_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/ProjectPointing.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/setFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremePhoto.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremePhoto_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremeSpectro.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_cubeMaking.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_isoResCubes.m}" 
 */

package SupremePhotoMatlab2Java;

import com.mathworks.toolbox.javabuilder.*;
import com.mathworks.toolbox.javabuilder.internal.*;
import java.util.*;

/**
 * The <code>SupremePhotoMatlab</code> class provides a Java interface to the M-functions
 * from the files:
 * <pre>
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/AppendWave.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Cube2FITS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_info.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_read.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_write.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fitsreadme.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeam.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeamFTS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetBeamSize.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeam.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeamFTS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetFitsL1.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/getFitsValue.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIndex.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIsoResolutionCubes.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetLegInd.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2D.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/GetSpectrum2D_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2DLE.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetWCS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrew.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrownies.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrew.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrownies.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Map2FITS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/MyFitsRead.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImResize.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImRotate.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/PrepareData_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/ProjectPointing.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/setFitsValue.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremePhoto.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremePhoto_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremeSpectro.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_cubeMaking.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_isoResCubes.m
 * </pre>
 * The {@link #dispose} method <b>must</b> be called on a <code>SupremePhotoMatlab</code> 
 * instance when it is no longer needed to ensure that native resources allocated by this 
 * class are properly freed.
 * @version 0.0
 */
public class SupremePhotoMatlab extends MWComponentInstance<SupremePhotoMatlab>
{
    /**
     * Tracks all instances of this class to ensure their dispose method is
     * called on shutdown.
     */
    private static final Set<Disposable> sInstances = new HashSet<Disposable>();

    /**
     * Maintains information used in calling the <code>AppendWave</code> M-function.
     */
    private static final MWFunctionSignature sAppendWaveSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "AppendWave",
                                /* max inputs = */ 2,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>Cube2FITS</code> M-function.
     */
    private static final MWFunctionSignature sCube2FITSSignature =
        new MWFunctionSignature(/* max outputs = */ 0,
                                /* has varargout = */ false,
                                /* function name = */ "Cube2FITS",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>fits_info</code> M-function.
     */
    private static final MWFunctionSignature sFits_infoSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "fits_info",
                                /* max inputs = */ 1,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>fits_read</code> M-function.
     */
    private static final MWFunctionSignature sFits_readSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "fits_read",
                                /* max inputs = */ 1,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>fits_write</code> M-function.
     */
    private static final MWFunctionSignature sFits_writeSignature =
        new MWFunctionSignature(/* max outputs = */ 0,
                                /* has varargout = */ false,
                                /* function name = */ "fits_write",
                                /* max inputs = */ 3,
                                /* has varargin = */ true);
    /**
     * Maintains information used in calling the <code>fitsreadme</code> M-function.
     */
    private static final MWFunctionSignature sFitsreadmeSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "fitsreadme",
                                /* max inputs = */ 1,
                                /* has varargin = */ true);
    /**
     * Maintains information used in calling the <code>GetBeam</code> M-function.
     */
    private static final MWFunctionSignature sGetBeamSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetBeam",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetBeamFTS</code> M-function.
     */
    private static final MWFunctionSignature sGetBeamFTSSignature =
        new MWFunctionSignature(/* max outputs = */ 4,
                                /* has varargout = */ false,
                                /* function name = */ "GetBeamFTS",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetBeamSize</code> M-function.
     */
    private static final MWFunctionSignature sGetBeamSizeSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetBeamSize",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetEqBeam</code> M-function.
     */
    private static final MWFunctionSignature sGetEqBeamSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetEqBeam",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetEqBeamFTS</code> M-function.
     */
    private static final MWFunctionSignature sGetEqBeamFTSSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetEqBeamFTS",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetFitsL1</code> M-function.
     */
    private static final MWFunctionSignature sGetFitsL1Signature =
        new MWFunctionSignature(/* max outputs = */ 8,
                                /* has varargout = */ false,
                                /* function name = */ "GetFitsL1",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>getFitsValue</code> M-function.
     */
    private static final MWFunctionSignature sGetFitsValueSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "getFitsValue",
                                /* max inputs = */ 2,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetIndex</code> M-function.
     */
    private static final MWFunctionSignature sGetIndexSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetIndex",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetIsoResolutionCubes</code> 
     *M-function.
     */
    private static final MWFunctionSignature sGetIsoResolutionCubesSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "GetIsoResolutionCubes",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetLegInd</code> M-function.
     */
    private static final MWFunctionSignature sGetLegIndSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "GetLegInd",
                                /* max inputs = */ 2,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetSpectrum2D</code> M-function.
     */
    private static final MWFunctionSignature sGetSpectrum2DSignature =
        new MWFunctionSignature(/* max outputs = */ 7,
                                /* has varargout = */ false,
                                /* function name = */ "GetSpectrum2D",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetSpectrum2D_plugin</code> 
     *M-function.
     */
    private static final MWFunctionSignature sGetSpectrum2D_pluginSignature =
        new MWFunctionSignature(/* max outputs = */ 6,
                                /* has varargout = */ false,
                                /* function name = */ "GetSpectrum2D_plugin",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetSpectrum2DLE</code> M-function.
     */
    private static final MWFunctionSignature sGetSpectrum2DLESignature =
        new MWFunctionSignature(/* max outputs = */ 7,
                                /* has varargout = */ false,
                                /* function name = */ "GetSpectrum2DLE",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>GetWCS</code> M-function.
     */
    private static final MWFunctionSignature sGetWCSSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "GetWCS",
                                /* max inputs = */ 5,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>InitJavaBrew</code> M-function.
     */
    private static final MWFunctionSignature sInitJavaBrewSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "InitJavaBrew",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>InitJavaBrownies</code> M-function.
     */
    private static final MWFunctionSignature sInitJavaBrowniesSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "InitJavaBrownies",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>JavaBrew</code> M-function.
     */
    private static final MWFunctionSignature sJavaBrewSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "JavaBrew",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>JavaBrownies</code> M-function.
     */
    private static final MWFunctionSignature sJavaBrowniesSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "JavaBrownies",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>Map2FITS</code> M-function.
     */
    private static final MWFunctionSignature sMap2FITSSignature =
        new MWFunctionSignature(/* max outputs = */ 0,
                                /* has varargout = */ false,
                                /* function name = */ "Map2FITS",
                                /* max inputs = */ 4,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>MyFitsRead</code> M-function.
     */
    private static final MWFunctionSignature sMyFitsReadSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "MyFitsRead",
                                /* max inputs = */ 1,
                                /* has varargin = */ true);
    /**
     * Maintains information used in calling the <code>MyImResize</code> M-function.
     */
    private static final MWFunctionSignature sMyImResizeSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "MyImResize",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>MyImRotate</code> M-function.
     */
    private static final MWFunctionSignature sMyImRotateSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "MyImRotate",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>PrepareData_plugin</code> 
     *M-function.
     */
    private static final MWFunctionSignature sPrepareData_pluginSignature =
        new MWFunctionSignature(/* max outputs = */ 7,
                                /* has varargout = */ false,
                                /* function name = */ "PrepareData_plugin",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>ProjectPointing</code> M-function.
     */
    private static final MWFunctionSignature sProjectPointingSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "ProjectPointing",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>setFitsValue</code> M-function.
     */
    private static final MWFunctionSignature sSetFitsValueSignature =
        new MWFunctionSignature(/* max outputs = */ 1,
                                /* has varargout = */ false,
                                /* function name = */ "setFitsValue",
                                /* max inputs = */ 3,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>SupremePhoto</code> M-function.
     */
    private static final MWFunctionSignature sSupremePhotoSignature =
        new MWFunctionSignature(/* max outputs = */ 4,
                                /* has varargout = */ false,
                                /* function name = */ "SupremePhoto",
                                /* max inputs = */ 15,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>SupremePhoto_plugin</code> 
     *M-function.
     */
    private static final MWFunctionSignature sSupremePhoto_pluginSignature =
        new MWFunctionSignature(/* max outputs = */ 5,
                                /* has varargout = */ false,
                                /* function name = */ "SupremePhoto_plugin",
                                /* max inputs = */ 16,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>SupremeSpectro</code> M-function.
     */
    private static final MWFunctionSignature sSupremeSpectroSignature =
        new MWFunctionSignature(/* max outputs = */ 4,
                                /* has varargout = */ false,
                                /* function name = */ "SupremeSpectro",
                                /* max inputs = */ 13,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>SupremeSpectro_cubeMaking</code> 
     *M-function.
     */
    private static final MWFunctionSignature sSupremeSpectro_cubeMakingSignature =
        new MWFunctionSignature(/* max outputs = */ 8,
                                /* has varargout = */ false,
                                /* function name = */ "SupremeSpectro_cubeMaking",
                                /* max inputs = */ 13,
                                /* has varargin = */ false);
    /**
     * Maintains information used in calling the <code>SupremeSpectro_isoResCubes</code> 
     *M-function.
     */
    private static final MWFunctionSignature sSupremeSpectro_isoResCubesSignature =
        new MWFunctionSignature(/* max outputs = */ 2,
                                /* has varargout = */ false,
                                /* function name = */ "SupremeSpectro_isoResCubes",
                                /* max inputs = */ 7,
                                /* has varargin = */ false);

    /**
     * Shared initialization implementation - private
     */
    private SupremePhotoMatlab (final MWMCR mcr) throws MWException
    {
        super(mcr);
        // add this to sInstances
        synchronized(SupremePhotoMatlab.class) {
            sInstances.add(this);
        }
    }

    /**
     * Constructs a new instance of the <code>SupremePhotoMatlab</code> class.
     */
    public SupremePhotoMatlab() throws MWException
    {
        this(SupremePhotoMatlab2JavaMCRFactory.newInstance());
    }
    
    private static MWComponentOptions getPathToComponentOptions(String path)
    {
        MWComponentOptions options = new MWComponentOptions(new MWCtfExtractLocation(path),
                                                            new MWCtfDirectorySource(path));
        return options;
    }
    
    /**
     * @deprecated Please use the constructor {@link #SupremePhotoMatlab(MWComponentOptions componentOptions)}.
     * The <code>com.mathworks.toolbox.javabuilder.MWComponentOptions</code> class provides API to set the
     * path to the component.
     * @param pathToComponent Path to component directory.
     */
    public SupremePhotoMatlab(String pathToComponent) throws MWException
    {
        this(SupremePhotoMatlab2JavaMCRFactory.newInstance(getPathToComponentOptions(pathToComponent)));
    }
    
    /**
     * Constructs a new instance of the <code>SupremePhotoMatlab</code> class. Use this 
     * constructor to specify the options required to instantiate this component.  The 
     * options will be specific to the instance of this component being created.
     * @param componentOptions Options specific to the component.
     */
    public SupremePhotoMatlab(MWComponentOptions componentOptions) throws MWException
    {
        this(SupremePhotoMatlab2JavaMCRFactory.newInstance(componentOptions));
    }
    
    /** Frees native resources associated with this object */
    public void dispose()
    {
        try {
            super.dispose();
        } finally {
            synchronized(SupremePhotoMatlab.class) {
                sInstances.remove(this);
            }
        }
    }
  
    /**
     * Invokes the first m-function specified by MCC, with any arguments given on
     * the command line, and prints the result.
     */
    public static void main (String[] args)
    {
        try {
            MWMCR mcr = SupremePhotoMatlab2JavaMCRFactory.newInstance();
            mcr.runMain( sAppendWaveSignature, args);
            mcr.dispose();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /**
     * Calls dispose method for each outstanding instance of this class.
     */
    public static void disposeAllInstances()
    {
        synchronized(SupremePhotoMatlab.class) {
            for (Disposable i : sInstances) i.dispose();
            sInstances.clear();
        }
    }

    /**
     * Provides the interface for calling the <code>AppendWave</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void AppendWave(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sAppendWaveSignature);
    }

    /**
     * Provides the interface for calling the <code>AppendWave</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void AppendWave(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sAppendWaveSignature);
    }

    /**
     * Provides the standard interface for calling the <code>AppendWave</code>
     * M-function with 2 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] AppendWave(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sAppendWaveSignature), 
                    sAppendWaveSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>Cube2FITS</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 3D FTS Cube to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % Cube: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void Cube2FITS(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sCube2FITSSignature);
    }

    /**
     * Provides the interface for calling the <code>Cube2FITS</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 3D FTS Cube to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % Cube: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void Cube2FITS(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sCube2FITSSignature);
    }

    /**
     * Provides the standard interface for calling the <code>Cube2FITS</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 3D FTS Cube to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % Cube: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] Cube2FITS(Object... rhs) throws MWException
    {
        Object[] lhs = new Object[0];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sCube2FITSSignature), 
                    sCube2FITSSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>fits_info</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsinfo that returns the PrimaryData.Keywords
     * % part of the fits file as a MATLAB structure.  Only Keyword/value pairs
     * % are retained in the structure.  Comments are stripped.
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_info(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sFits_infoSignature);
    }

    /**
     * Provides the interface for calling the <code>fits_info</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsinfo that returns the PrimaryData.Keywords
     * % part of the fits file as a MATLAB structure.  Only Keyword/value pairs
     * % are retained in the structure.  Comments are stripped.
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_info(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sFits_infoSignature);
    }

    /**
     * Provides the standard interface for calling the <code>fits_info</code>
     * M-function with 1 input argument.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsinfo that returns the PrimaryData.Keywords
     * % part of the fits file as a MATLAB structure.  Only Keyword/value pairs
     * % are retained in the structure.  Comments are stripped.
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] fits_info(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sFits_infoSignature), 
                    sFits_infoSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>fits_read</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsread.
     * % (Just intended for consistent usage with the other fits functions.)
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_read(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sFits_readSignature);
    }

    /**
     * Provides the interface for calling the <code>fits_read</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsread.
     * % (Just intended for consistent usage with the other fits functions.)
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_read(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sFits_readSignature);
    }

    /**
     * Provides the standard interface for calling the <code>fits_read</code>
     * M-function with 1 input argument.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsread.
     * % (Just intended for consistent usage with the other fits functions.)
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] fits_read(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sFits_readSignature), 
                    sFits_readSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>fits_write</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Usage:
     * % -----
     * % 1st argument: filename: string containing the name of the file to write.  
     * '.fits' will
     * % be appended if the file name does not end in '.fits' or '.FITS'.
     * %
     * % 2nd argument: data: an N-dimensional array, usually of 32-bit floats or 64-bit 
     * doubles.
     * % Complex data types are not currently supported, but this may be added in
     * % the the near future.
     * %
     * % 3rd argument: header (optional): A fits header can be optionally supplied
     * % in one of 3 formats.
     * %
     * % Header format #1: The header is a structure loaded directly from MATLAB's
     * % fitsinfo command: header=fitsinfo('myFITS.fits');  In this case, the
     * % keyword/values are held in a cell array: header.PrimaryData.Keywords.  This
     * % cell array is extracted and header format #2 is used.  For example, if
     * % 'myFITS.fits' is a fits file, then the following commands read it in and
     * % re-write identical data with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header);
     * %
     * % Header format #2: The header may be a cell array, in the same format
     * % as the headers returned by MATLAB's fitsinfo command in the
     * % header.PrimaryData.Keywords field.  For example, if 'myFITS.fits' is a fits
     * % file, then the following commands read it in and re-write identical data
     * % with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header.PrimaryData.Keywords);
     * %
     * % This format preserves all bare FITS keywords (with no value field) and
     * % all comments.
     * %
     * % Header format #3: The header may be a MATLAB structure in the format
     * % header.keyword1=value1;
     * % header.keyword2=value2;
     * % header.keyword3=value3;
     * % 
     * % This format is compatible with the MFITSIO package.  Note that it does
     * % not permit bare keywords or comment fields.
     * %
     * % Certain mandatory FITS fields may be added of they are not present in
     * % the supplied header.  Please see the function 'requiredKeywords' in this
     * % file for details.  These required keywords will be used to generate a
     * % minimal FITS header if the optional header field is not supplied.
     * %
     * % ==============================
     * % Modify these lines to suit your data and machine architecture.
     * %
     * % FITS file BITPIX field.  Common choices are -32 for single precision
     * % data, and -64 for double precision.
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_write(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sFits_writeSignature);
    }

    /**
     * Provides the interface for calling the <code>fits_write</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Usage:
     * % -----
     * % 1st argument: filename: string containing the name of the file to write.  
     * '.fits' will
     * % be appended if the file name does not end in '.fits' or '.FITS'.
     * %
     * % 2nd argument: data: an N-dimensional array, usually of 32-bit floats or 64-bit 
     * doubles.
     * % Complex data types are not currently supported, but this may be added in
     * % the the near future.
     * %
     * % 3rd argument: header (optional): A fits header can be optionally supplied
     * % in one of 3 formats.
     * %
     * % Header format #1: The header is a structure loaded directly from MATLAB's
     * % fitsinfo command: header=fitsinfo('myFITS.fits');  In this case, the
     * % keyword/values are held in a cell array: header.PrimaryData.Keywords.  This
     * % cell array is extracted and header format #2 is used.  For example, if
     * % 'myFITS.fits' is a fits file, then the following commands read it in and
     * % re-write identical data with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header);
     * %
     * % Header format #2: The header may be a cell array, in the same format
     * % as the headers returned by MATLAB's fitsinfo command in the
     * % header.PrimaryData.Keywords field.  For example, if 'myFITS.fits' is a fits
     * % file, then the following commands read it in and re-write identical data
     * % with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header.PrimaryData.Keywords);
     * %
     * % This format preserves all bare FITS keywords (with no value field) and
     * % all comments.
     * %
     * % Header format #3: The header may be a MATLAB structure in the format
     * % header.keyword1=value1;
     * % header.keyword2=value2;
     * % header.keyword3=value3;
     * % 
     * % This format is compatible with the MFITSIO package.  Note that it does
     * % not permit bare keywords or comment fields.
     * %
     * % Certain mandatory FITS fields may be added of they are not present in
     * % the supplied header.  Please see the function 'requiredKeywords' in this
     * % file for details.  These required keywords will be used to generate a
     * % minimal FITS header if the optional header field is not supplied.
     * %
     * % ==============================
     * % Modify these lines to suit your data and machine architecture.
     * %
     * % FITS file BITPIX field.  Common choices are -32 for single precision
     * % data, and -64 for double precision.
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fits_write(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sFits_writeSignature);
    }

    /**
     * Provides the standard interface for calling the <code>fits_write</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Usage:
     * % -----
     * % 1st argument: filename: string containing the name of the file to write.  
     * '.fits' will
     * % be appended if the file name does not end in '.fits' or '.FITS'.
     * %
     * % 2nd argument: data: an N-dimensional array, usually of 32-bit floats or 64-bit 
     * doubles.
     * % Complex data types are not currently supported, but this may be added in
     * % the the near future.
     * %
     * % 3rd argument: header (optional): A fits header can be optionally supplied
     * % in one of 3 formats.
     * %
     * % Header format #1: The header is a structure loaded directly from MATLAB's
     * % fitsinfo command: header=fitsinfo('myFITS.fits');  In this case, the
     * % keyword/values are held in a cell array: header.PrimaryData.Keywords.  This
     * % cell array is extracted and header format #2 is used.  For example, if
     * % 'myFITS.fits' is a fits file, then the following commands read it in and
     * % re-write identical data with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header);
     * %
     * % Header format #2: The header may be a cell array, in the same format
     * % as the headers returned by MATLAB's fitsinfo command in the
     * % header.PrimaryData.Keywords field.  For example, if 'myFITS.fits' is a fits
     * % file, then the following commands read it in and re-write identical data
     * % with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header.PrimaryData.Keywords);
     * %
     * % This format preserves all bare FITS keywords (with no value field) and
     * % all comments.
     * %
     * % Header format #3: The header may be a MATLAB structure in the format
     * % header.keyword1=value1;
     * % header.keyword2=value2;
     * % header.keyword3=value3;
     * % 
     * % This format is compatible with the MFITSIO package.  Note that it does
     * % not permit bare keywords or comment fields.
     * %
     * % Certain mandatory FITS fields may be added of they are not present in
     * % the supplied header.  Please see the function 'requiredKeywords' in this
     * % file for details.  These required keywords will be used to generate a
     * % minimal FITS header if the optional header field is not supplied.
     * %
     * % ==============================
     * % Modify these lines to suit your data and machine architecture.
     * %
     * % FITS file BITPIX field.  Common choices are -32 for single precision
     * % data, and -64 for double precision.
     * </pre>
     * </p>
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] fits_write(Object... rhs) throws MWException
    {
        Object[] lhs = new Object[0];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sFits_writeSignature), 
                    sFits_writeSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>fitsreadme</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according to
     * %   the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'Primary', 'Table', 'BinTable', 
     * %                'Image', or 'Unknown' for reading data from the primary 
     * %                data array, ASCII table extension, Binary table extension, 
     * %                Image extension or an unknown extension respectively. Only  
     * %                one extension should be supplied. DATA for ASCII and  
     * %                Binary table extensions is a 1-D cell array. The contents 
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be
     * %                the same class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing 
     * %                the output of FITSINFO with the 'Info' parameter helps 
     * %                FITSREAD locate the data in the file more quickly. 
     * %                 
     * %   Example:
     * %
     * %       data = fitsread('tst0012.fits');
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fitsreadme(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sFitsreadmeSignature);
    }

    /**
     * Provides the interface for calling the <code>fitsreadme</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according to
     * %   the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'Primary', 'Table', 'BinTable', 
     * %                'Image', or 'Unknown' for reading data from the primary 
     * %                data array, ASCII table extension, Binary table extension, 
     * %                Image extension or an unknown extension respectively. Only  
     * %                one extension should be supplied. DATA for ASCII and  
     * %                Binary table extensions is a 1-D cell array. The contents 
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be
     * %                the same class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing 
     * %                the output of FITSINFO with the 'Info' parameter helps 
     * %                FITSREAD locate the data in the file more quickly. 
     * %                 
     * %   Example:
     * %
     * %       data = fitsread('tst0012.fits');
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void fitsreadme(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sFitsreadmeSignature);
    }

    /**
     * Provides the standard interface for calling the <code>fitsreadme</code>
     * M-function with 1 input argument.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according to
     * %   the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'Primary', 'Table', 'BinTable', 
     * %                'Image', or 'Unknown' for reading data from the primary 
     * %                data array, ASCII table extension, Binary table extension, 
     * %                Image extension or an unknown extension respectively. Only  
     * %                one extension should be supplied. DATA for ASCII and  
     * %                Binary table extensions is a 1-D cell array. The contents 
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be
     * %                the same class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing 
     * %                the output of FITSINFO with the 'Info' parameter helps 
     * %                FITSREAD locate the data in the file more quickly. 
     * %                 
     * %   Example:
     * %
     * %       data = fitsread('tst0012.fits');
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] fitsreadme(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sFitsreadmeSignature), 
                    sFitsreadmeSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetBeam</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [PSF OTF] = GetPSF(FileName,WCS,Type)
     * % 
     * %
     * % Utility function to calculate the theoretical or emperical PSF given by
     * % the instrument beams model on a given grid and for certain observation
     * % angle. 
     * % 
     * % input:
     * % --------
     * % FileName : source file name
     * % WVS: a coordinate system (see SupremePhoto)
     * % Type: Beam type.  it can be 'Simulated' or 'Empiric'
     * % 
     * % output:
     * % -------
     * % PSF: the space domain tranfer function 
     * % OTF : the Fourier domain transfer function
     * % 
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * %
     * % Version 2.5
     * % 
     * % New: Remove dependency of Image processing toolbox 
     * % 
     * % Version 2.0
     * % 
     * % New: Antialiasing treatement for beams
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeam(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetBeamSignature);
    }

    /**
     * Provides the interface for calling the <code>GetBeam</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [PSF OTF] = GetPSF(FileName,WCS,Type)
     * % 
     * %
     * % Utility function to calculate the theoretical or emperical PSF given by
     * % the instrument beams model on a given grid and for certain observation
     * % angle. 
     * % 
     * % input:
     * % --------
     * % FileName : source file name
     * % WVS: a coordinate system (see SupremePhoto)
     * % Type: Beam type.  it can be 'Simulated' or 'Empiric'
     * % 
     * % output:
     * % -------
     * % PSF: the space domain tranfer function 
     * % OTF : the Fourier domain transfer function
     * % 
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * %
     * % Version 2.5
     * % 
     * % New: Remove dependency of Image processing toolbox 
     * % 
     * % Version 2.0
     * % 
     * % New: Antialiasing treatement for beams
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeam(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetBeamSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetBeam</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [PSF OTF] = GetPSF(FileName,WCS,Type)
     * % 
     * %
     * % Utility function to calculate the theoretical or emperical PSF given by
     * % the instrument beams model on a given grid and for certain observation
     * % angle. 
     * % 
     * % input:
     * % --------
     * % FileName : source file name
     * % WVS: a coordinate system (see SupremePhoto)
     * % Type: Beam type.  it can be 'Simulated' or 'Empiric'
     * % 
     * % output:
     * % -------
     * % PSF: the space domain tranfer function 
     * % OTF : the Fourier domain transfer function
     * % 
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * %
     * % Version 2.5
     * % 
     * % New: Remove dependency of Image processing toolbox 
     * % 
     * % Version 2.0
     * % 
     * % New: Antialiasing treatement for beams
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetBeam(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetBeamSignature), 
                    sGetBeamSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetBeamFTS</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Beam, OTF, COTF ] = GetBeamFTS(BeamFile,WCS,Wave)
     * %  
     * % Generates Beam cube for SPIRE FTS instrument using coefficient provided
     * % in File. The MAT File should contain a cell array 'Profile', it first item 
     * % contains SSW profile parameters (c_1=c_2=0) and the second element
     * % contains SLW parameters. Each profile is stuctured as of 
     * % 
     * % Wn(cm^-1)   |   w_0   |    c_0   |    c_1   |    c_2
     * %
     * % Input: 
     * % ------
     * % BeamFile: Beam MAT file name
     * % WCS : a structure containing the coordinate of the cube
     * % Wave : a vector containing the wavenumbers
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeamFTS(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetBeamFTSSignature);
    }

    /**
     * Provides the interface for calling the <code>GetBeamFTS</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Beam, OTF, COTF ] = GetBeamFTS(BeamFile,WCS,Wave)
     * %  
     * % Generates Beam cube for SPIRE FTS instrument using coefficient provided
     * % in File. The MAT File should contain a cell array 'Profile', it first item 
     * % contains SSW profile parameters (c_1=c_2=0) and the second element
     * % contains SLW parameters. Each profile is stuctured as of 
     * % 
     * % Wn(cm^-1)   |   w_0   |    c_0   |    c_1   |    c_2
     * %
     * % Input: 
     * % ------
     * % BeamFile: Beam MAT file name
     * % WCS : a structure containing the coordinate of the cube
     * % Wave : a vector containing the wavenumbers
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeamFTS(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetBeamFTSSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetBeamFTS</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Beam, OTF, COTF ] = GetBeamFTS(BeamFile,WCS,Wave)
     * %  
     * % Generates Beam cube for SPIRE FTS instrument using coefficient provided
     * % in File. The MAT File should contain a cell array 'Profile', it first item 
     * % contains SSW profile parameters (c_1=c_2=0) and the second element
     * % contains SLW parameters. Each profile is stuctured as of 
     * % 
     * % Wn(cm^-1)   |   w_0   |    c_0   |    c_1   |    c_2
     * %
     * % Input: 
     * % ------
     * % BeamFile: Beam MAT file name
     * % WCS : a structure containing the coordinate of the cube
     * % Wave : a vector containing the wavenumbers
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetBeamFTS(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetBeamFTSSignature), 
                    sGetBeamFTSSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetBeamSize</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [BeamSizea, BeamSizeb]  = GetBeamSize(Beams,PixelSize,Method)
     * %
     * % A simple function to calculate a beam size by energy critera
     * % The beam width is given by the bandwith to -3db
     * %
     * % Inputs:
     * % Beams : a Cell array of beams in Fourier domain
     * % PixelSize (Default=1): the pixel size in arcsec
     * % Method={'FWHP' or 'SCF'} (Default='SCF') : Defines calculation method The
     * % default Spatial Cutt-off Frequency (SCF) which more accurate the second
     * % is based on the Full Width at Half Power (FWHP)
     * % Copyrights: Hacheme AYASSO, GIPSA-LAB 2014
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeamSize(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetBeamSizeSignature);
    }

    /**
     * Provides the interface for calling the <code>GetBeamSize</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [BeamSizea, BeamSizeb]  = GetBeamSize(Beams,PixelSize,Method)
     * %
     * % A simple function to calculate a beam size by energy critera
     * % The beam width is given by the bandwith to -3db
     * %
     * % Inputs:
     * % Beams : a Cell array of beams in Fourier domain
     * % PixelSize (Default=1): the pixel size in arcsec
     * % Method={'FWHP' or 'SCF'} (Default='SCF') : Defines calculation method The
     * % default Spatial Cutt-off Frequency (SCF) which more accurate the second
     * % is based on the Full Width at Half Power (FWHP)
     * % Copyrights: Hacheme AYASSO, GIPSA-LAB 2014
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetBeamSize(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetBeamSizeSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetBeamSize</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [BeamSizea, BeamSizeb]  = GetBeamSize(Beams,PixelSize,Method)
     * %
     * % A simple function to calculate a beam size by energy critera
     * % The beam width is given by the bandwith to -3db
     * %
     * % Inputs:
     * % Beams : a Cell array of beams in Fourier domain
     * % PixelSize (Default=1): the pixel size in arcsec
     * % Method={'FWHP' or 'SCF'} (Default='SCF') : Defines calculation method The
     * % default Spatial Cutt-off Frequency (SCF) which more accurate the second
     * % is based on the Full Width at Half Power (FWHP)
     * % Copyrights: Hacheme AYASSO, GIPSA-LAB 2014
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetBeamSize(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetBeamSizeSignature), 
                    sGetBeamSizeSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetEqBeam</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeam(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method maps
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetEqBeam(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetEqBeamSignature);
    }

    /**
     * Provides the interface for calling the <code>GetEqBeam</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeam(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method maps
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetEqBeam(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetEqBeamSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetEqBeam</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeam(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method maps
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetEqBeam(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetEqBeamSignature), 
                    sGetEqBeamSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetEqBeamFTS</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeamFTS(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method Cubes
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetEqBeamFTS(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetEqBeamFTSSignature);
    }

    /**
     * Provides the interface for calling the <code>GetEqBeamFTS</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeamFTS(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method Cubes
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetEqBeamFTS(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetEqBeamFTSSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetEqBeamFTS</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeamFTS(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method Cubes
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetEqBeamFTS(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetEqBeamFTSSignature), 
                    sGetEqBeamFTSSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetFitsL1</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Data,Pointing,ProjPoint,Mask,Time,TrOvMs,Center,posangle,Objname] = 
     * GetFitsL1(FilesName,Band)
     * %
     * % Important Version 3 capable of concatinating several scans (attention file name 
     * becomes cell array)
     * %
     * % Use this function to import data from Level 1 fits file provided by HIPE.
     * % Attention: Certain hypothesis are made on the structure of the FITS file
     * %  According to version 5 structure. However, the function try to
     * %  identifies structure errors using informations supplied by the FITS
     * %  file.
     * % 
     * % Inputs: 
     * % --------
     * % FilesName : the name of the FTIS files to be imported (with extension)
     * % Band (default= {1,2,3}): chooses the Band which data will be loaded 
     * %   1 == PSW, 2== PMW, 3= PLW. Band can be a cell array, so the output can
     * %   be a cell array with the corresponding Band assignement.
     * % 
     * % Outputs: 
     * % ---------
     * % data : the time lines of the different bolometers for a given Band
     * % pointing: the position of each bolometer in degrees (dec/ra)
     * % Mask: Logical array which gives information about the validity of the data at a 
     * given
     * % position. One signfies that data is not usable (glitched, error, overflow...)
     * % Time : sampling instents (good for speed calculation)
     * % 
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA LAB 2011 & 2012
     * % 
     * % Version  4.0
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetFitsL1(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetFitsL1Signature);
    }

    /**
     * Provides the interface for calling the <code>GetFitsL1</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Data,Pointing,ProjPoint,Mask,Time,TrOvMs,Center,posangle,Objname] = 
     * GetFitsL1(FilesName,Band)
     * %
     * % Important Version 3 capable of concatinating several scans (attention file name 
     * becomes cell array)
     * %
     * % Use this function to import data from Level 1 fits file provided by HIPE.
     * % Attention: Certain hypothesis are made on the structure of the FITS file
     * %  According to version 5 structure. However, the function try to
     * %  identifies structure errors using informations supplied by the FITS
     * %  file.
     * % 
     * % Inputs: 
     * % --------
     * % FilesName : the name of the FTIS files to be imported (with extension)
     * % Band (default= {1,2,3}): chooses the Band which data will be loaded 
     * %   1 == PSW, 2== PMW, 3= PLW. Band can be a cell array, so the output can
     * %   be a cell array with the corresponding Band assignement.
     * % 
     * % Outputs: 
     * % ---------
     * % data : the time lines of the different bolometers for a given Band
     * % pointing: the position of each bolometer in degrees (dec/ra)
     * % Mask: Logical array which gives information about the validity of the data at a 
     * given
     * % position. One signfies that data is not usable (glitched, error, overflow...)
     * % Time : sampling instents (good for speed calculation)
     * % 
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA LAB 2011 & 2012
     * % 
     * % Version  4.0
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetFitsL1(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetFitsL1Signature);
    }

    /**
     * Provides the standard interface for calling the <code>GetFitsL1</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Data,Pointing,ProjPoint,Mask,Time,TrOvMs,Center,posangle,Objname] = 
     * GetFitsL1(FilesName,Band)
     * %
     * % Important Version 3 capable of concatinating several scans (attention file name 
     * becomes cell array)
     * %
     * % Use this function to import data from Level 1 fits file provided by HIPE.
     * % Attention: Certain hypothesis are made on the structure of the FITS file
     * %  According to version 5 structure. However, the function try to
     * %  identifies structure errors using informations supplied by the FITS
     * %  file.
     * % 
     * % Inputs: 
     * % --------
     * % FilesName : the name of the FTIS files to be imported (with extension)
     * % Band (default= {1,2,3}): chooses the Band which data will be loaded 
     * %   1 == PSW, 2== PMW, 3= PLW. Band can be a cell array, so the output can
     * %   be a cell array with the corresponding Band assignement.
     * % 
     * % Outputs: 
     * % ---------
     * % data : the time lines of the different bolometers for a given Band
     * % pointing: the position of each bolometer in degrees (dec/ra)
     * % Mask: Logical array which gives information about the validity of the data at a 
     * given
     * % position. One signfies that data is not usable (glitched, error, overflow...)
     * % Time : sampling instents (good for speed calculation)
     * % 
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA LAB 2011 & 2012
     * % 
     * % Version  4.0
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetFitsL1(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetFitsL1Signature), 
                    sGetFitsL1Signature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>getFitsValue</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void getFitsValue(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetFitsValueSignature);
    }

    /**
     * Provides the interface for calling the <code>getFitsValue</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void getFitsValue(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetFitsValueSignature);
    }

    /**
     * Provides the standard interface for calling the <code>getFitsValue</code>
     * M-function with 2 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] getFitsValue(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetFitsValueSignature), 
                    sGetFitsValueSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetIndex</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [NPa,NPb,Na,Nb]=GetIndex(Pa,Pb,WCS)
     * % 
     * % Generate indexes for the projection data
     * % 
     * % Inputs:
     * %--------
     * %
     * % Pa: projected pointing axis 1 (Alpha)
     * % Pb: projected pointinng axis 2 (Beta)
     * % WCS: Coordinate system (see SupremePhoto)
     * %
     * % outputs:
     * %---------
     * % NPa : Normalised pointing oon axis 1 (1<=NPa)
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetIndex(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetIndexSignature);
    }

    /**
     * Provides the interface for calling the <code>GetIndex</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [NPa,NPb,Na,Nb]=GetIndex(Pa,Pb,WCS)
     * % 
     * % Generate indexes for the projection data
     * % 
     * % Inputs:
     * %--------
     * %
     * % Pa: projected pointing axis 1 (Alpha)
     * % Pb: projected pointinng axis 2 (Beta)
     * % WCS: Coordinate system (see SupremePhoto)
     * %
     * % outputs:
     * %---------
     * % NPa : Normalised pointing oon axis 1 (1<=NPa)
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetIndex(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetIndexSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetIndex</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [NPa,NPb,Na,Nb]=GetIndex(Pa,Pb,WCS)
     * % 
     * % Generate indexes for the projection data
     * % 
     * % Inputs:
     * %--------
     * %
     * % Pa: projected pointing axis 1 (Alpha)
     * % Pb: projected pointinng axis 2 (Beta)
     * % WCS: Coordinate system (see SupremePhoto)
     * %
     * % outputs:
     * %---------
     * % NPa : Normalised pointing oon axis 1 (1<=NPa)
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetIndex(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetIndexSignature), 
                    sGetIndexSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetIsoResolutionCubes</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function IsoCubes = GetIsoResolutionCubes(Cubes,fBeams,Waves,IsoWave)
     * %
     * % A utility fuction used to calculated IsoResolution cubes for a given
     * % wave number
     * % 
     * % Inputs:
     * %---------
     * % Cubes (cell array 1xNcubes): Full resolution cubes 
     * % fBeams (cell array 1xNcubes): fourier domain beams corresponding to cubes
     * % Waves (cell array 1xNcubes): Wave number corresponding to cubes
     * % IsoWave (scalar): wave number for which the Iso resolved cubes are
     * % calculated
     * %
     * % Output:
     * %---------
     * % IsoCubes (cell array 1xNcubes): Cubes with the same resolution as the
     * % cube given by wave IsoWave
     * %
     * % 
     * % Copyrights Hacheme AYASSO , GIPSA-LAB, 2013
     * %
     * % ver 0.5
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetIsoResolutionCubes(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetIsoResolutionCubesSignature);
    }

    /**
     * Provides the interface for calling the <code>GetIsoResolutionCubes</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function IsoCubes = GetIsoResolutionCubes(Cubes,fBeams,Waves,IsoWave)
     * %
     * % A utility fuction used to calculated IsoResolution cubes for a given
     * % wave number
     * % 
     * % Inputs:
     * %---------
     * % Cubes (cell array 1xNcubes): Full resolution cubes 
     * % fBeams (cell array 1xNcubes): fourier domain beams corresponding to cubes
     * % Waves (cell array 1xNcubes): Wave number corresponding to cubes
     * % IsoWave (scalar): wave number for which the Iso resolved cubes are
     * % calculated
     * %
     * % Output:
     * %---------
     * % IsoCubes (cell array 1xNcubes): Cubes with the same resolution as the
     * % cube given by wave IsoWave
     * %
     * % 
     * % Copyrights Hacheme AYASSO , GIPSA-LAB, 2013
     * %
     * % ver 0.5
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetIsoResolutionCubes(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetIsoResolutionCubesSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetIsoResolutionCubes</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function IsoCubes = GetIsoResolutionCubes(Cubes,fBeams,Waves,IsoWave)
     * %
     * % A utility fuction used to calculated IsoResolution cubes for a given
     * % wave number
     * % 
     * % Inputs:
     * %---------
     * % Cubes (cell array 1xNcubes): Full resolution cubes 
     * % fBeams (cell array 1xNcubes): fourier domain beams corresponding to cubes
     * % Waves (cell array 1xNcubes): Wave number corresponding to cubes
     * % IsoWave (scalar): wave number for which the Iso resolved cubes are
     * % calculated
     * %
     * % Output:
     * %---------
     * % IsoCubes (cell array 1xNcubes): Cubes with the same resolution as the
     * % cube given by wave IsoWave
     * %
     * % 
     * % Copyrights Hacheme AYASSO , GIPSA-LAB, 2013
     * %
     * % ver 0.5
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetIsoResolutionCubes(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetIsoResolutionCubesSignature), 
                    sGetIsoResolutionCubesSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetLegInd</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [LegInd DatRed] = GetLegInd(Time)
     * %
     * % Utility function to help getting Leg index for block data containing
     * % total scan. 
     * %
     * % Copyright Hacheme AYASSO,  IAS 2011
     * % 
     * % ver 1.5
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetLegInd(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetLegIndSignature);
    }

    /**
     * Provides the interface for calling the <code>GetLegInd</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [LegInd DatRed] = GetLegInd(Time)
     * %
     * % Utility function to help getting Leg index for block data containing
     * % total scan. 
     * %
     * % Copyright Hacheme AYASSO,  IAS 2011
     * % 
     * % ver 1.5
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetLegInd(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetLegIndSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetLegInd</code>
     * M-function with 2 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [LegInd DatRed] = GetLegInd(Time)
     * %
     * % Utility function to help getting Leg index for block data containing
     * % total scan. 
     * %
     * % Copyright Hacheme AYASSO,  IAS 2011
     * % 
     * % ver 1.5
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetLegInd(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetLegIndSignature), 
                    sGetLegIndSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetSpectrum2D</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2D(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetSpectrum2DSignature);
    }

    /**
     * Provides the interface for calling the <code>GetSpectrum2D</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2D(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetSpectrum2DSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetSpectrum2D</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetSpectrum2D(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetSpectrum2DSignature), 
                    sGetSpectrum2DSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetSpectrum2D_plugin</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2D_plugin(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetSpectrum2D_pluginSignature);
    }

    /**
     * Provides the interface for calling the <code>GetSpectrum2D_plugin</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2D_plugin(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetSpectrum2D_pluginSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetSpectrum2D_plugin</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetSpectrum2D_plugin(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetSpectrum2D_pluginSignature), 
                    sGetSpectrum2D_pluginSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetSpectrum2DLE</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2DLE(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2DLE(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetSpectrum2DLESignature);
    }

    /**
     * Provides the interface for calling the <code>GetSpectrum2DLE</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2DLE(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetSpectrum2DLE(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetSpectrum2DLESignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetSpectrum2DLE</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2DLE(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetSpectrum2DLE(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetSpectrum2DLESignature), 
                    sGetSpectrum2DLESignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>GetWCS</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetWCS(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sGetWCSSignature);
    }

    /**
     * Provides the interface for calling the <code>GetWCS</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void GetWCS(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sGetWCSSignature);
    }

    /**
     * Provides the standard interface for calling the <code>GetWCS</code>
     * M-function with 5 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] GetWCS(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sGetWCSSignature), 
                    sGetWCSSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>InitJavaBrew</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void InitJavaBrew(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sInitJavaBrewSignature);
    }

    /**
     * Provides the interface for calling the <code>InitJavaBrew</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void InitJavaBrew(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sInitJavaBrewSignature);
    }

    /**
     * Provides the standard interface for calling the <code>InitJavaBrew</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] InitJavaBrew(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sInitJavaBrewSignature), 
                    sInitJavaBrewSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>InitJavaBrownies</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void InitJavaBrownies(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sInitJavaBrowniesSignature);
    }

    /**
     * Provides the interface for calling the <code>InitJavaBrownies</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void InitJavaBrownies(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sInitJavaBrowniesSignature);
    }

    /**
     * Provides the standard interface for calling the <code>InitJavaBrownies</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] InitJavaBrownies(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sInitJavaBrowniesSignature), 
                    sInitJavaBrowniesSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>JavaBrew</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrew(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Extended emission and additive White noise  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void JavaBrew(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sJavaBrewSignature);
    }

    /**
     * Provides the interface for calling the <code>JavaBrew</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrew(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Extended emission and additive White noise  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void JavaBrew(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sJavaBrewSignature);
    }

    /**
     * Provides the standard interface for calling the <code>JavaBrew</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrew(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Extended emission and additive White noise  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] JavaBrew(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sJavaBrewSignature), 
                    sJavaBrewSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>JavaBrownies</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrownies(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Offseted White NoIsE and Smooth fields  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void JavaBrownies(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sJavaBrowniesSignature);
    }

    /**
     * Provides the interface for calling the <code>JavaBrownies</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrownies(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Offseted White NoIsE and Smooth fields  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void JavaBrownies(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sJavaBrowniesSignature);
    }

    /**
     * Provides the standard interface for calling the <code>JavaBrownies</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrownies(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Offseted White NoIsE and Smooth fields  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] JavaBrownies(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sJavaBrowniesSignature), 
                    sJavaBrowniesSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>Map2FITS</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 2D map to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % MAP: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void Map2FITS(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sMap2FITSSignature);
    }

    /**
     * Provides the interface for calling the <code>Map2FITS</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 2D map to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % MAP: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void Map2FITS(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sMap2FITSSignature);
    }

    /**
     * Provides the standard interface for calling the <code>Map2FITS</code>
     * M-function with 4 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 2D map to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % MAP: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     * </p>
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] Map2FITS(Object... rhs) throws MWException
    {
        Object[] lhs = new Object[0];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sMap2FITSSignature), 
                    sMap2FITSSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>MyFitsRead</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according
     * %   to the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'primary', 'asciitable', 'binarytable',
     * %                'image', or 'unknown' for reading data from the primary
     * %                data array, ASCII table extension, Binary table extension,
     * %                Image extension or an unknown extension respectively. Only
     * %                one extension should be supplied. DATA for ASCII and
     * %                Binary table extensions is a 1-D cell array. The contents
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be the same
     * %                class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing
     * %                the output of FITSINFO with the 'Info' parameter helps
     * %                FITSREAD locate the data in the file more quickly.
     * %
     * %   'PixelRegion',{ROWS, COLS, ..., N_DIM}  
     * %                FITSREAD returns the sub-image specified by the boundaries
     * %                for an N dimensional image. ROWS, COLS, ..., N_DIM are
     * %                each vectors of 1-based indices given either as START,
     * %                [START STOP] or [START INCREMENT STOP] selecting the
     * %                sub-image region for the corresponding dimension. This
     * %                parameter is valid only for primary or image extensions.
     * %
     * %   'TableColumns',COLUMNS
     * %                COLUMNS is a vector with 1-based indices selecting the
     * %                columns to read from the ASCII or Binary table extension.
     * %                This vector should contain unique and valid indices into
     * %                the table data specified in increasing order. This
     * %                parameter is valid only for ASCII or Binary extensions.
     * %
     * %   'TableRows',ROWS
     * %                ROWS is a vector with 1-based indices selecting the rows
     * %                to read from the ASCII or Binary table extension. This
     * %                vector should contain unique and valid indices into the
     * %                table data specified in increasing order. This parameter
     * %                is valid only for ASCII or Binary extensions.
     * %                
     * %            
     * %   Example: Read primary data from file.
     * %      data = fitsread('tst0012.fits');
     * %
     * %   Example: Inspect available extensions, read 'image' extension using the
     * %   EXTNAME option.
     * %      info      = fitsinfo('tst0012.fits');
     * %      % List of contents, includes any extensions if present.
     * %      disp(info.Contents);
     * %      imageData = fitsread('tst0012.fits','image');
     * %
     * %   Example: Subsample the fifth plane of 'image' extension by 2.
     * %      info        = fitsinfo('tst0012.fits');
     * %      rowend      = info.Image.Size(1);
     * %      colend      = info.Image.Size(2);
     * %      primaryData = fitsread('tst0012.fits','image',...
     * %                     'Info', info,...
     * %                     'PixelRegion',{[1 2 rowend], [1 2 colend], 5 });
     * %
     * %   Example: Read every other row from a ASCII table data.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.AsciiTable.Rows; 
     * %      tableData = fitsread('tst0012.fits','asciitable',...
     * %                   'Info',info,...
     * %                   'TableRows',[1:2:rowend]);
     * %
     * %   Example: Read all data for the first, second and fifth column of the
     * %   Binary table.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.BinaryTable.Rows;       
     * %      tableData = fitsread('tst0012.fits','binarytable',...
     * %                   'Info',info,...
     * %                   'TableColumns',[1 2 5]);
     * %
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyFitsRead(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sMyFitsReadSignature);
    }

    /**
     * Provides the interface for calling the <code>MyFitsRead</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according
     * %   to the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'primary', 'asciitable', 'binarytable',
     * %                'image', or 'unknown' for reading data from the primary
     * %                data array, ASCII table extension, Binary table extension,
     * %                Image extension or an unknown extension respectively. Only
     * %                one extension should be supplied. DATA for ASCII and
     * %                Binary table extensions is a 1-D cell array. The contents
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be the same
     * %                class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing
     * %                the output of FITSINFO with the 'Info' parameter helps
     * %                FITSREAD locate the data in the file more quickly.
     * %
     * %   'PixelRegion',{ROWS, COLS, ..., N_DIM}  
     * %                FITSREAD returns the sub-image specified by the boundaries
     * %                for an N dimensional image. ROWS, COLS, ..., N_DIM are
     * %                each vectors of 1-based indices given either as START,
     * %                [START STOP] or [START INCREMENT STOP] selecting the
     * %                sub-image region for the corresponding dimension. This
     * %                parameter is valid only for primary or image extensions.
     * %
     * %   'TableColumns',COLUMNS
     * %                COLUMNS is a vector with 1-based indices selecting the
     * %                columns to read from the ASCII or Binary table extension.
     * %                This vector should contain unique and valid indices into
     * %                the table data specified in increasing order. This
     * %                parameter is valid only for ASCII or Binary extensions.
     * %
     * %   'TableRows',ROWS
     * %                ROWS is a vector with 1-based indices selecting the rows
     * %                to read from the ASCII or Binary table extension. This
     * %                vector should contain unique and valid indices into the
     * %                table data specified in increasing order. This parameter
     * %                is valid only for ASCII or Binary extensions.
     * %                
     * %            
     * %   Example: Read primary data from file.
     * %      data = fitsread('tst0012.fits');
     * %
     * %   Example: Inspect available extensions, read 'image' extension using the
     * %   EXTNAME option.
     * %      info      = fitsinfo('tst0012.fits');
     * %      % List of contents, includes any extensions if present.
     * %      disp(info.Contents);
     * %      imageData = fitsread('tst0012.fits','image');
     * %
     * %   Example: Subsample the fifth plane of 'image' extension by 2.
     * %      info        = fitsinfo('tst0012.fits');
     * %      rowend      = info.Image.Size(1);
     * %      colend      = info.Image.Size(2);
     * %      primaryData = fitsread('tst0012.fits','image',...
     * %                     'Info', info,...
     * %                     'PixelRegion',{[1 2 rowend], [1 2 colend], 5 });
     * %
     * %   Example: Read every other row from a ASCII table data.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.AsciiTable.Rows; 
     * %      tableData = fitsread('tst0012.fits','asciitable',...
     * %                   'Info',info,...
     * %                   'TableRows',[1:2:rowend]);
     * %
     * %   Example: Read all data for the first, second and fifth column of the
     * %   Binary table.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.BinaryTable.Rows;       
     * %      tableData = fitsread('tst0012.fits','binarytable',...
     * %                   'Info',info,...
     * %                   'TableColumns',[1 2 5]);
     * %
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyFitsRead(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sMyFitsReadSignature);
    }

    /**
     * Provides the standard interface for calling the <code>MyFitsRead</code>
     * M-function with 1 input argument.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according
     * %   to the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'primary', 'asciitable', 'binarytable',
     * %                'image', or 'unknown' for reading data from the primary
     * %                data array, ASCII table extension, Binary table extension,
     * %                Image extension or an unknown extension respectively. Only
     * %                one extension should be supplied. DATA for ASCII and
     * %                Binary table extensions is a 1-D cell array. The contents
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be the same
     * %                class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing
     * %                the output of FITSINFO with the 'Info' parameter helps
     * %                FITSREAD locate the data in the file more quickly.
     * %
     * %   'PixelRegion',{ROWS, COLS, ..., N_DIM}  
     * %                FITSREAD returns the sub-image specified by the boundaries
     * %                for an N dimensional image. ROWS, COLS, ..., N_DIM are
     * %                each vectors of 1-based indices given either as START,
     * %                [START STOP] or [START INCREMENT STOP] selecting the
     * %                sub-image region for the corresponding dimension. This
     * %                parameter is valid only for primary or image extensions.
     * %
     * %   'TableColumns',COLUMNS
     * %                COLUMNS is a vector with 1-based indices selecting the
     * %                columns to read from the ASCII or Binary table extension.
     * %                This vector should contain unique and valid indices into
     * %                the table data specified in increasing order. This
     * %                parameter is valid only for ASCII or Binary extensions.
     * %
     * %   'TableRows',ROWS
     * %                ROWS is a vector with 1-based indices selecting the rows
     * %                to read from the ASCII or Binary table extension. This
     * %                vector should contain unique and valid indices into the
     * %                table data specified in increasing order. This parameter
     * %                is valid only for ASCII or Binary extensions.
     * %                
     * %            
     * %   Example: Read primary data from file.
     * %      data = fitsread('tst0012.fits');
     * %
     * %   Example: Inspect available extensions, read 'image' extension using the
     * %   EXTNAME option.
     * %      info      = fitsinfo('tst0012.fits');
     * %      % List of contents, includes any extensions if present.
     * %      disp(info.Contents);
     * %      imageData = fitsread('tst0012.fits','image');
     * %
     * %   Example: Subsample the fifth plane of 'image' extension by 2.
     * %      info        = fitsinfo('tst0012.fits');
     * %      rowend      = info.Image.Size(1);
     * %      colend      = info.Image.Size(2);
     * %      primaryData = fitsread('tst0012.fits','image',...
     * %                     'Info', info,...
     * %                     'PixelRegion',{[1 2 rowend], [1 2 colend], 5 });
     * %
     * %   Example: Read every other row from a ASCII table data.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.AsciiTable.Rows; 
     * %      tableData = fitsread('tst0012.fits','asciitable',...
     * %                   'Info',info,...
     * %                   'TableRows',[1:2:rowend]);
     * %
     * %   Example: Read all data for the first, second and fifth column of the
     * %   Binary table.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.BinaryTable.Rows;       
     * %      tableData = fitsread('tst0012.fits','binarytable',...
     * %                   'Info',info,...
     * %                   'TableColumns',[1 2 5]);
     * %
     * %
     * %   See also FITSINFO.
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] MyFitsRead(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sMyFitsReadSignature), 
                    sMyFitsReadSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>MyImResize</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImResize(Im,Fctr,InType)
     * % 
     * % A utility function to resize an image (Im) by a factor (Fctr) using (InType)
     * % Interpolation method. (This function simply to avoid using imresize of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyImResize(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sMyImResizeSignature);
    }

    /**
     * Provides the interface for calling the <code>MyImResize</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImResize(Im,Fctr,InType)
     * % 
     * % A utility function to resize an image (Im) by a factor (Fctr) using (InType)
     * % Interpolation method. (This function simply to avoid using imresize of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyImResize(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sMyImResizeSignature);
    }

    /**
     * Provides the standard interface for calling the <code>MyImResize</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImResize(Im,Fctr,InType)
     * % 
     * % A utility function to resize an image (Im) by a factor (Fctr) using (InType)
     * % Interpolation method. (This function simply to avoid using imresize of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] MyImResize(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sMyImResizeSignature), 
                    sMyImResizeSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>MyImRotate</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImRotate(Im,Ngl,InType)
     * % 
     * % A utility function to rotate an image (Im) by a angle (Ngl, given in degrees) 
     * using (InType)
     * % Interpolation method. (This function simply to avoid using imrotate of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyImRotate(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sMyImRotateSignature);
    }

    /**
     * Provides the interface for calling the <code>MyImRotate</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImRotate(Im,Ngl,InType)
     * % 
     * % A utility function to rotate an image (Im) by a angle (Ngl, given in degrees) 
     * using (InType)
     * % Interpolation method. (This function simply to avoid using imrotate of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void MyImRotate(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sMyImRotateSignature);
    }

    /**
     * Provides the standard interface for calling the <code>MyImRotate</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImRotate(Im,Ngl,InType)
     * % 
     * % A utility function to rotate an image (Im) by a angle (Ngl, given in degrees) 
     * using (InType)
     * % Interpolation method. (This function simply to avoid using imrotate of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] MyImRotate(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sMyImRotateSignature), 
                    sMyImRotateSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>PrepareData_plugin</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % data = cell(NOBS);
     * % ra = cell(NOBS);
     * % dec = cell(NOBS);
     * % mask = cell(NOBS);
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void PrepareData_plugin(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sPrepareData_pluginSignature);
    }

    /**
     * Provides the interface for calling the <code>PrepareData_plugin</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % data = cell(NOBS);
     * % ra = cell(NOBS);
     * % dec = cell(NOBS);
     * % mask = cell(NOBS);
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void PrepareData_plugin(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sPrepareData_pluginSignature);
    }

    /**
     * Provides the standard interface for calling the <code>PrepareData_plugin</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % data = cell(NOBS);
     * % ra = cell(NOBS);
     * % dec = cell(NOBS);
     * % mask = cell(NOBS);
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] PrepareData_plugin(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sPrepareData_pluginSignature), 
                    sPrepareData_pluginSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>ProjectPointing</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function [NPA NPB] = ProjectPointing(RA,DEC,WCS)
     * %
     * %   Project angular pointing (RA,DEC) on a planar grid (PA,PB) around the
     * %   center. The function returns a normalised projected pointing 
     * % 
     * % 
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void ProjectPointing(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sProjectPointingSignature);
    }

    /**
     * Provides the interface for calling the <code>ProjectPointing</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function [NPA NPB] = ProjectPointing(RA,DEC,WCS)
     * %
     * %   Project angular pointing (RA,DEC) on a planar grid (PA,PB) around the
     * %   center. The function returns a normalised projected pointing 
     * % 
     * % 
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void ProjectPointing(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sProjectPointingSignature);
    }

    /**
     * Provides the standard interface for calling the <code>ProjectPointing</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %   function [NPA NPB] = ProjectPointing(RA,DEC,WCS)
     * %
     * %   Project angular pointing (RA,DEC) on a planar grid (PA,PB) around the
     * %   center. The function returns a normalised projected pointing 
     * % 
     * % 
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] ProjectPointing(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sProjectPointingSignature), 
                    sProjectPointingSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>setFitsValue</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void setFitsValue(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSetFitsValueSignature);
    }

    /**
     * Provides the interface for calling the <code>setFitsValue</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void setFitsValue(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSetFitsValueSignature);
    }

    /**
     * Provides the standard interface for calling the <code>setFitsValue</code>
     * M-function with 3 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] setFitsValue(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSetFitsValueSignature), 
                    sSetFitsValueSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>SupremePhoto</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremePhoto(Data,RA,DEC,Mask,Time,WCS,RhosOp,RhosVal,RhonOp,RhonVal,OffsetOp,BeamOp,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A mapmaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Nmes x Nbolo): is a 2D matrix where each ligne contains
     * % measurements for all bolometers at a given sampling instant. Attention
     * % The dimesion of this matrix is used to determine Band and the PSF
     * %   88<Nbolo<139 --> PSW
     * %   43<Nbolo<=88 --> PMW
     * %   0<Nbolo<=43  --> PLW
     * % 
     * % RA,DEC (Nmes x Nbolo): are 2 matrixs containing the RA and DEC of the
     * % detecor positions.
     * %   
     * % Mask (Nmes x Nbolo): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Time (Nmes) : a column vector containing time instant for each sample
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  WCS.POSANGLE = Observation angle (for BEAM rotation)
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhon' or 'SetRhon'): see RhosOp
     * %
     * % RhonVal (real positive): see above
     * %
     * % OffsetOp (String='GetOffsetLeg', 'GetOffsetAll' or 'ZeroOffset'): 'GetOffsetLeg' 
     * or will activate
     * % joint estimation of detectors offsets either per scan leg or in a global
     * % way for whole observation. 'ZeroOffset' will deactivate offset estimation
     * % and supposes that the offset was aleardy substracted from the data.
     * %
     * % BeamOp (String='Simulated' or 'Empiric'): Determine which beam to use.
     * % Two beam models are possible: simulated or emperic.
     * %
     * % PointingOp (String= 'Nearest' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Map: Output map
     * % STD: Standard deviation map from the estimator
     * % ErrMap: a projected map of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.Offset: Estimated Offsets
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.VarOffset: the variance of Offsets estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the map.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 2.4.2
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremePhoto(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSupremePhotoSignature);
    }

    /**
     * Provides the interface for calling the <code>SupremePhoto</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremePhoto(Data,RA,DEC,Mask,Time,WCS,RhosOp,RhosVal,RhonOp,RhonVal,OffsetOp,BeamOp,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A mapmaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Nmes x Nbolo): is a 2D matrix where each ligne contains
     * % measurements for all bolometers at a given sampling instant. Attention
     * % The dimesion of this matrix is used to determine Band and the PSF
     * %   88<Nbolo<139 --> PSW
     * %   43<Nbolo<=88 --> PMW
     * %   0<Nbolo<=43  --> PLW
     * % 
     * % RA,DEC (Nmes x Nbolo): are 2 matrixs containing the RA and DEC of the
     * % detecor positions.
     * %   
     * % Mask (Nmes x Nbolo): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Time (Nmes) : a column vector containing time instant for each sample
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  WCS.POSANGLE = Observation angle (for BEAM rotation)
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhon' or 'SetRhon'): see RhosOp
     * %
     * % RhonVal (real positive): see above
     * %
     * % OffsetOp (String='GetOffsetLeg', 'GetOffsetAll' or 'ZeroOffset'): 'GetOffsetLeg' 
     * or will activate
     * % joint estimation of detectors offsets either per scan leg or in a global
     * % way for whole observation. 'ZeroOffset' will deactivate offset estimation
     * % and supposes that the offset was aleardy substracted from the data.
     * %
     * % BeamOp (String='Simulated' or 'Empiric'): Determine which beam to use.
     * % Two beam models are possible: simulated or emperic.
     * %
     * % PointingOp (String= 'Nearest' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Map: Output map
     * % STD: Standard deviation map from the estimator
     * % ErrMap: a projected map of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.Offset: Estimated Offsets
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.VarOffset: the variance of Offsets estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the map.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 2.4.2
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremePhoto(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSupremePhotoSignature);
    }

    /**
     * Provides the standard interface for calling the <code>SupremePhoto</code>
     * M-function with 15 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremePhoto(Data,RA,DEC,Mask,Time,WCS,RhosOp,RhosVal,RhonOp,RhonVal,OffsetOp,BeamOp,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A mapmaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Nmes x Nbolo): is a 2D matrix where each ligne contains
     * % measurements for all bolometers at a given sampling instant. Attention
     * % The dimesion of this matrix is used to determine Band and the PSF
     * %   88<Nbolo<139 --> PSW
     * %   43<Nbolo<=88 --> PMW
     * %   0<Nbolo<=43  --> PLW
     * % 
     * % RA,DEC (Nmes x Nbolo): are 2 matrixs containing the RA and DEC of the
     * % detecor positions.
     * %   
     * % Mask (Nmes x Nbolo): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Time (Nmes) : a column vector containing time instant for each sample
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  WCS.POSANGLE = Observation angle (for BEAM rotation)
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhon' or 'SetRhon'): see RhosOp
     * %
     * % RhonVal (real positive): see above
     * %
     * % OffsetOp (String='GetOffsetLeg', 'GetOffsetAll' or 'ZeroOffset'): 'GetOffsetLeg' 
     * or will activate
     * % joint estimation of detectors offsets either per scan leg or in a global
     * % way for whole observation. 'ZeroOffset' will deactivate offset estimation
     * % and supposes that the offset was aleardy substracted from the data.
     * %
     * % BeamOp (String='Simulated' or 'Empiric'): Determine which beam to use.
     * % Two beam models are possible: simulated or emperic.
     * %
     * % PointingOp (String= 'Nearest' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Map: Output map
     * % STD: Standard deviation map from the estimator
     * % ErrMap: a projected map of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.Offset: Estimated Offsets
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.VarOffset: the variance of Offsets estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the map.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 2.4.2
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] SupremePhoto(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSupremePhotoSignature), 
                    sSupremePhotoSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>SupremePhoto_plugin</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremePhoto_plugin(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSupremePhoto_pluginSignature);
    }

    /**
     * Provides the interface for calling the <code>SupremePhoto_plugin</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremePhoto_plugin(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSupremePhoto_pluginSignature);
    }

    /**
     * Provides the standard interface for calling the <code>SupremePhoto_plugin</code>
     * M-function with 16 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] SupremePhoto_plugin(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSupremePhoto_pluginSignature), 
                    sSupremePhoto_pluginSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>SupremeSpectro</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremeSpectro(Data,NPa,NPb,Mask,Wave,WCS,RhosOp,RhosVal,RhonOp,RhonVal,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A CubeMaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Njig x Nbolo x Nwave) : is a 3D matrix where each ligne contains
     * % measurements for all bolometers at a given Jiggle postion. The thrid dimension
     * % 
     * % NPa,NPb (Nmes x Nbolo): are 2 matrixs containing the normalised pointing
     * % of the detectors (obtained using ProjectPointing)
     * %   
     * % Mask (Njig x Nbolo x Nwave): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Wave (Nwave) : a line vector containing wave numbers
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhonFull', 'GetRhonPartial', 'GetRhonIso'  or 'SetRhon'): 
     * %  Selects the noise variance estimation mode, the 'full' option will
     * %  estimate one variance per wavelength, the 'partial' estimates a variance 
     * %  for a subset of wavelengths (one variance for 25 wavelengths), with 'Iso'
     * % the same variance is supposed 
     * % RhonVal (real positive): see above
     * %
     * %
     * % PointingOp (String= 'Naive' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Cube: Output cube
     * % STD: Standard deviation cube from the estimator
     * % ErrMap: a projected cube of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the cube.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 1.5
     * %
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSupremeSpectroSignature);
    }

    /**
     * Provides the interface for calling the <code>SupremeSpectro</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremeSpectro(Data,NPa,NPb,Mask,Wave,WCS,RhosOp,RhosVal,RhonOp,RhonVal,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A CubeMaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Njig x Nbolo x Nwave) : is a 3D matrix where each ligne contains
     * % measurements for all bolometers at a given Jiggle postion. The thrid dimension
     * % 
     * % NPa,NPb (Nmes x Nbolo): are 2 matrixs containing the normalised pointing
     * % of the detectors (obtained using ProjectPointing)
     * %   
     * % Mask (Njig x Nbolo x Nwave): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Wave (Nwave) : a line vector containing wave numbers
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhonFull', 'GetRhonPartial', 'GetRhonIso'  or 'SetRhon'): 
     * %  Selects the noise variance estimation mode, the 'full' option will
     * %  estimate one variance per wavelength, the 'partial' estimates a variance 
     * %  for a subset of wavelengths (one variance for 25 wavelengths), with 'Iso'
     * % the same variance is supposed 
     * % RhonVal (real positive): see above
     * %
     * %
     * % PointingOp (String= 'Naive' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Cube: Output cube
     * % STD: Standard deviation cube from the estimator
     * % ErrMap: a projected cube of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the cube.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 1.5
     * %
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSupremeSpectroSignature);
    }

    /**
     * Provides the standard interface for calling the <code>SupremeSpectro</code>
     * M-function with 13 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremeSpectro(Data,NPa,NPb,Mask,Wave,WCS,RhosOp,RhosVal,RhonOp,RhonVal,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A CubeMaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Njig x Nbolo x Nwave) : is a 3D matrix where each ligne contains
     * % measurements for all bolometers at a given Jiggle postion. The thrid dimension
     * % 
     * % NPa,NPb (Nmes x Nbolo): are 2 matrixs containing the normalised pointing
     * % of the detectors (obtained using ProjectPointing)
     * %   
     * % Mask (Njig x Nbolo x Nwave): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Wave (Nwave) : a line vector containing wave numbers
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhonFull', 'GetRhonPartial', 'GetRhonIso'  or 'SetRhon'): 
     * %  Selects the noise variance estimation mode, the 'full' option will
     * %  estimate one variance per wavelength, the 'partial' estimates a variance 
     * %  for a subset of wavelengths (one variance for 25 wavelengths), with 'Iso'
     * % the same variance is supposed 
     * % RhonVal (real positive): see above
     * %
     * %
     * % PointingOp (String= 'Naive' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Cube: Output cube
     * % STD: Standard deviation cube from the estimator
     * % ErrMap: a projected cube of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the cube.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 1.5
     * %
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] SupremeSpectro(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSupremeSpectroSignature), 
                    sSupremeSpectroSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>SupremeSpectro_cubeMaking</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro_cubeMaking(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSupremeSpectro_cubeMakingSignature);
    }

    /**
     * Provides the interface for calling the <code>SupremeSpectro_cubeMaking</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro_cubeMaking(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSupremeSpectro_cubeMakingSignature);
    }

    /**
     * Provides the standard interface for calling the <code>SupremeSpectro_cubeMaking</code>
     * M-function with 13 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] SupremeSpectro_cubeMaking(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSupremeSpectro_cubeMakingSignature), 
                    sSupremeSpectro_cubeMakingSignature);
        return lhs;
    }
    /**
     * Provides the interface for calling the <code>SupremeSpectro_isoResCubes</code> M-function 
     * where the first input, an instance of List, receives the output of the M-function and
     * the second input, also an instance of List, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * 
     * %save('/Users/boualam/Work/SUPREME_SPECTRO/SupremeSpectro_Keep/spectroData','SSW','SLW','fEqBeamSSW','fEqBeamSLW','WaveSSW','WaveSLW')
     * </pre>
     * </p>
     * @param lhs List in which to return outputs. Number of outputs (nargout) is
     * determined by allocated size of this List. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs List containing inputs. Number of inputs (nargin) is determined
     * by the allocated size of this List. Input arguments may be passed as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or
     * as arrays of any supported Java type. Arguments passed as Java types are
     * converted to MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro_isoResCubes(List lhs, List rhs) throws MWException
    {
        fMCR.invoke(lhs, rhs, sSupremeSpectro_isoResCubesSignature);
    }

    /**
     * Provides the interface for calling the <code>SupremeSpectro_isoResCubes</code> M-function 
     * where the first input, an Object array, receives the output of the M-function and
     * the second input, also an Object array, provides the input to the M-function.
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * 
     * %save('/Users/boualam/Work/SUPREME_SPECTRO/SupremeSpectro_Keep/spectroData','SSW','SLW','fEqBeamSSW','fEqBeamSLW','WaveSSW','WaveSLW')
     * </pre>
     * </p>
     * @param lhs array in which to return outputs. Number of outputs (nargout)
     * is determined by allocated size of this array. Outputs are returned as
     * sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>.
     * Each output array should be freed by calling its <code>dispose()</code>
     * method.
     *
     * @param rhs array containing inputs. Number of inputs (nargin) is
     * determined by the allocated size of this array. Input arguments may be
     * passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     * @throws MWException An error has occurred during the function call.
     */
    public void SupremeSpectro_isoResCubes(Object[] lhs, Object[] rhs) throws MWException
    {
        fMCR.invoke(Arrays.asList(lhs), Arrays.asList(rhs), sSupremeSpectro_isoResCubesSignature);
    }

    /**
     * Provides the standard interface for calling the <code>SupremeSpectro_isoResCubes</code>
     * M-function with 7 input arguments.
     * Input arguments may be passed as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of
     * any supported Java type. Arguments passed as Java types are converted to
     * MATLAB arrays according to default conversion rules.
     *
     * <p>M-documentation as provided by the author of the M function:
     * <pre>
     * 
     * %save('/Users/boualam/Work/SUPREME_SPECTRO/SupremeSpectro_Keep/spectroData','SSW','SLW','fEqBeamSSW','fEqBeamSLW','WaveSSW','WaveSLW')
     * </pre>
     * </p>
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     * @return Array of length nargout containing the function outputs. Outputs
     * are returned as sub-classes of
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>. Each output array
     * should be freed by calling its <code>dispose()</code> method.
     * @throws MWException An error has occurred during the function call.
     */
    public Object[] SupremeSpectro_isoResCubes(int nargout, Object... rhs) throws MWException
    {
        Object[] lhs = new Object[nargout];
        fMCR.invoke(Arrays.asList(lhs), 
                    MWMCR.getRhsCompat(rhs, sSupremeSpectro_isoResCubesSignature), 
                    sSupremeSpectro_isoResCubesSignature);
        return lhs;
    }
}
