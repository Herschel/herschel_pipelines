/*
 * MATLAB Compiler: 6.1 (R2015b)
 * Date: Thu Dec 24 13:05:46 2015
 * Arguments: "-B" "macro_default" "-W" "java:SupremePhotoMatlab2Java,SupremePhotoMatlab" 
 * "-T" "link:lib" "-d" 
 * "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/SupremePhotoMatlab2Java/for_testing" 
 * "class{SupremePhotoMatlab:/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/AppendWave.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Cube2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_info.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_read.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_write.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fitsreadme.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetBeamSize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetFitsL1.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/getFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIndex.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIsoResolutionCubes.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetLegInd.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2D.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/GetSpectrum2D_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2DLE.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetWCS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Map2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/MyFitsRead.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImResize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImRotate.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/PrepareData_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/ProjectPointing.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/setFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremePhoto.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremePhoto_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremeSpectro.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_cubeMaking.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_isoResCubes.m}" 
 */

package SupremePhotoMatlab2Java;

import com.mathworks.toolbox.javabuilder.*;
import com.mathworks.toolbox.javabuilder.internal.*;

/**
 * <i>INTERNAL USE ONLY</i>
 */
public class SupremePhotoMatlab2JavaMCRFactory
{
   
    
    /** Component's uuid */
    private static final String sComponentId = "SupremePhoto_403CFB163778456E40CA6844D4DCF0B0";
    
    /** Component name */
    private static final String sComponentName = "SupremePhotoMatlab2Java";
    
   
    /** Pointer to default component options */
    private static final MWComponentOptions sDefaultComponentOptions = 
        new MWComponentOptions(
            MWCtfExtractLocation.EXTRACT_TO_CACHE, 
            new MWCtfClassLoaderSource(SupremePhotoMatlab2JavaMCRFactory.class)
        );
    
    
    private SupremePhotoMatlab2JavaMCRFactory()
    {
        // Never called.
    }
    
    public static MWMCR newInstance(MWComponentOptions componentOptions) throws MWException
    {
        if (null == componentOptions.getCtfSource()) {
            componentOptions = new MWComponentOptions(componentOptions);
            componentOptions.setCtfSource(sDefaultComponentOptions.getCtfSource());
        }
        return MWMCR.newInstance(
            componentOptions, 
            SupremePhotoMatlab2JavaMCRFactory.class, 
            sComponentName, 
            sComponentId,
            new int[]{9,0,0}
        );
    }
    
    public static MWMCR newInstance() throws MWException
    {
        return newInstance(sDefaultComponentOptions);
    }
}
