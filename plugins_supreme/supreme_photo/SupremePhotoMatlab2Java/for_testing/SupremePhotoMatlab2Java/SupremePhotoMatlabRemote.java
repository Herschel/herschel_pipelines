/*
 * MATLAB Compiler: 6.1 (R2015b)
 * Date: Thu Dec 24 13:05:46 2015
 * Arguments: "-B" "macro_default" "-W" "java:SupremePhotoMatlab2Java,SupremePhotoMatlab" 
 * "-T" "link:lib" "-d" 
 * "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/SupremePhotoMatlab2Java/for_testing" 
 * "class{SupremePhotoMatlab:/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/AppendWave.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Cube2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_info.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_read.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_write.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fitsreadme.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetBeamSize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeam.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeamFTS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetFitsL1.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/getFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIndex.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIsoResolutionCubes.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetLegInd.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2D.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/GetSpectrum2D_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2DLE.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetWCS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrew.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrownies.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Map2FITS.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/MyFitsRead.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImResize.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImRotate.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/PrepareData_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/ProjectPointing.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/setFitsValue.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremePhoto.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremePhoto_plugin.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremeSpectro.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_cubeMaking.m,/data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_isoResCubes.m}" 
 */

package SupremePhotoMatlab2Java;

import com.mathworks.toolbox.javabuilder.pooling.Poolable;
import java.util.List;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The <code>SupremePhotoMatlabRemote</code> class provides a Java RMI-compliant 
 * interface to the M-functions from the files:
 * <pre>
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/AppendWave.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Cube2FITS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_info.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_read.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fits_write.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/fitsreadme.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeam.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetBeamFTS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetBeamSize.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeam.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetEqBeamFTS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetFitsL1.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/getFitsValue.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIndex.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetIsoResolutionCubes.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetLegInd.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2D.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/GetSpectrum2D_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/GetSpectrum2DLE.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/GetWCS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrew.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/InitJavaBrownies.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrew.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/JavaBrownies.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Map2FITS.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/MyFitsRead.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImResize.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/MyImRotate.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/PrepareData_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/lib/ProjectPointing.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/setFitsValue.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremePhoto.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremePhoto_plugin.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/SupremeSpectro.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_cubeMaking.m
 *  /data/glx-herschel/data1/herschel/SUPREME/SupremePhotoSpectro/plugin/git/herschel/util/Plugin/SupremeSpectro_isoResCubes.m
 * </pre>
 * The {@link #dispose} method <b>must</b> be called on a 
 * <code>SupremePhotoMatlabRemote</code> instance when it is no longer needed to ensure 
 * that native resources allocated by this class are properly freed, and the server-side 
 * proxy is unexported.  (Failure to call dispose may result in server-side threads not 
 * being properly shut down, which often appears as a hang.)  
 *
 * This interface is designed to be used together with 
 * <code>com.mathworks.toolbox.javabuilder.remoting.RemoteProxy</code> to automatically 
 * generate RMI server proxy objects for instances of 
 * SupremePhotoMatlab2Java.SupremePhotoMatlab.
 */
public interface SupremePhotoMatlabRemote extends Poolable
{
    /**
     * Provides the standard interface for calling the <code>AppendWave</code> M-function 
     * with 2 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * No usage documentation is available for this function.  (To fix this, the function 
     * author should insert a help comment at the beginning of their M code.  See the 
     * MATLAB documentation for more details.)
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] AppendWave(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>Cube2FITS</code> M-function 
     * with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 3D FTS Cube to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % Cube: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     *
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] Cube2FITS(Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>fits_info</code> M-function 
     * with 1 input argument.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsinfo that returns the PrimaryData.Keywords
     * % part of the fits file as a MATLAB structure.  Only Keyword/value pairs
     * % are retained in the structure.  Comments are stripped.
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] fits_info(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>fits_read</code> M-function 
     * with 1 input argument.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Just a wrapper for fitsread.
     * % (Just intended for consistent usage with the other fits functions.)
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] fits_read(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>fits_write</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * % Usage:
     * % -----
     * % 1st argument: filename: string containing the name of the file to write.  
     * '.fits' will
     * % be appended if the file name does not end in '.fits' or '.FITS'.
     * %
     * % 2nd argument: data: an N-dimensional array, usually of 32-bit floats or 64-bit 
     * doubles.
     * % Complex data types are not currently supported, but this may be added in
     * % the the near future.
     * %
     * % 3rd argument: header (optional): A fits header can be optionally supplied
     * % in one of 3 formats.
     * %
     * % Header format #1: The header is a structure loaded directly from MATLAB's
     * % fitsinfo command: header=fitsinfo('myFITS.fits');  In this case, the
     * % keyword/values are held in a cell array: header.PrimaryData.Keywords.  This
     * % cell array is extracted and header format #2 is used.  For example, if
     * % 'myFITS.fits' is a fits file, then the following commands read it in and
     * % re-write identical data with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header);
     * %
     * % Header format #2: The header may be a cell array, in the same format
     * % as the headers returned by MATLAB's fitsinfo command in the
     * % header.PrimaryData.Keywords field.  For example, if 'myFITS.fits' is a fits
     * % file, then the following commands read it in and re-write identical data
     * % with a copy of the same header:
     * %
     * % data=fitsread('myFITS.fits');
     * % header=fitsinfo('myFITS.fits');
     * % fitswrite('myFITS_copy.fits', data, header.PrimaryData.Keywords);
     * %
     * % This format preserves all bare FITS keywords (with no value field) and
     * % all comments.
     * %
     * % Header format #3: The header may be a MATLAB structure in the format
     * % header.keyword1=value1;
     * % header.keyword2=value2;
     * % header.keyword3=value3;
     * % 
     * % This format is compatible with the MFITSIO package.  Note that it does
     * % not permit bare keywords or comment fields.
     * %
     * % Certain mandatory FITS fields may be added of they are not present in
     * % the supplied header.  Please see the function 'requiredKeywords' in this
     * % file for details.  These required keywords will be used to generate a
     * % minimal FITS header if the optional header field is not supplied.
     * %
     * % ==============================
     * % Modify these lines to suit your data and machine architecture.
     * %
     * % FITS file BITPIX field.  Common choices are -32 for single precision
     * % data, and -64 for double precision.
     * </pre>
     *
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] fits_write(Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>fitsreadme</code> M-function 
     * with 1 input argument.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according to
     * %   the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'Primary', 'Table', 'BinTable', 
     * %                'Image', or 'Unknown' for reading data from the primary 
     * %                data array, ASCII table extension, Binary table extension, 
     * %                Image extension or an unknown extension respectively. Only  
     * %                one extension should be supplied. DATA for ASCII and  
     * %                Binary table extensions is a 1-D cell array. The contents 
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be
     * %                the same class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing 
     * %                the output of FITSINFO with the 'Info' parameter helps 
     * %                FITSREAD locate the data in the file more quickly. 
     * %                 
     * %   Example:
     * %
     * %       data = fitsread('tst0012.fits');
     * %
     * %   See also FITSINFO.
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] fitsreadme(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetBeam</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [PSF OTF] = GetPSF(FileName,WCS,Type)
     * % 
     * %
     * % Utility function to calculate the theoretical or emperical PSF given by
     * % the instrument beams model on a given grid and for certain observation
     * % angle. 
     * % 
     * % input:
     * % --------
     * % FileName : source file name
     * % WVS: a coordinate system (see SupremePhoto)
     * % Type: Beam type.  it can be 'Simulated' or 'Empiric'
     * % 
     * % output:
     * % -------
     * % PSF: the space domain tranfer function 
     * % OTF : the Fourier domain transfer function
     * % 
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * %
     * % Version 2.5
     * % 
     * % New: Remove dependency of Image processing toolbox 
     * % 
     * % Version 2.0
     * % 
     * % New: Antialiasing treatement for beams
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetBeam(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetBeamFTS</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Beam, OTF, COTF ] = GetBeamFTS(BeamFile,WCS,Wave)
     * %  
     * % Generates Beam cube for SPIRE FTS instrument using coefficient provided
     * % in File. The MAT File should contain a cell array 'Profile', it first item 
     * % contains SSW profile parameters (c_1=c_2=0) and the second element
     * % contains SLW parameters. Each profile is stuctured as of 
     * % 
     * % Wn(cm^-1)   |   w_0   |    c_0   |    c_1   |    c_2
     * %
     * % Input: 
     * % ------
     * % BeamFile: Beam MAT file name
     * % WCS : a structure containing the coordinate of the cube
     * % Wave : a vector containing the wavenumbers
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetBeamFTS(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetBeamSize</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % function [BeamSizea, BeamSizeb]  = GetBeamSize(Beams,PixelSize,Method)
     * %
     * % A simple function to calculate a beam size by energy critera
     * % The beam width is given by the bandwith to -3db
     * %
     * % Inputs:
     * % Beams : a Cell array of beams in Fourier domain
     * % PixelSize (Default=1): the pixel size in arcsec
     * % Method={'FWHP' or 'SCF'} (Default='SCF') : Defines calculation method The
     * % default Spatial Cutt-off Frequency (SCF) which more accurate the second
     * % is based on the Full Width at Half Power (FWHP)
     * % Copyrights: Hacheme AYASSO, GIPSA-LAB 2014
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetBeamSize(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetEqBeam</code> M-function 
     * with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeam(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method maps
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetEqBeam(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetEqBeamFTS</code> 
     * M-function with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % function EqBeam = GetEqBeamFTS(Prior,OP,Rhon,Rhos)
     * %
     * % Calculate the equivalent beam for the SUPREME method Cubes
     * %
     * % Inputs: 
     * % --------
     * % Prior : The prior stucture
     * % OP: The operateur structure
     * % Rhon: The noise precesion 
     * % Rhos: The source precesion
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetEqBeamFTS(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetFitsL1</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [Data,Pointing,ProjPoint,Mask,Time,TrOvMs,Center,posangle,Objname] = 
     * GetFitsL1(FilesName,Band)
     * %
     * % Important Version 3 capable of concatinating several scans (attention file name 
     * becomes cell array)
     * %
     * % Use this function to import data from Level 1 fits file provided by HIPE.
     * % Attention: Certain hypothesis are made on the structure of the FITS file
     * %  According to version 5 structure. However, the function try to
     * %  identifies structure errors using informations supplied by the FITS
     * %  file.
     * % 
     * % Inputs: 
     * % --------
     * % FilesName : the name of the FTIS files to be imported (with extension)
     * % Band (default= {1,2,3}): chooses the Band which data will be loaded 
     * %   1 == PSW, 2== PMW, 3= PLW. Band can be a cell array, so the output can
     * %   be a cell array with the corresponding Band assignement.
     * % 
     * % Outputs: 
     * % ---------
     * % data : the time lines of the different bolometers for a given Band
     * % pointing: the position of each bolometer in degrees (dec/ra)
     * % Mask: Logical array which gives information about the validity of the data at a 
     * given
     * % position. One signfies that data is not usable (glitched, error, overflow...)
     * % Time : sampling instents (good for speed calculation)
     * % 
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA LAB 2011 & 2012
     * % 
     * % Version  4.0
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetFitsL1(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>getFitsValue</code> 
     * M-function with 2 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] getFitsValue(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetIndex</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [NPa,NPb,Na,Nb]=GetIndex(Pa,Pb,WCS)
     * % 
     * % Generate indexes for the projection data
     * % 
     * % Inputs:
     * %--------
     * %
     * % Pa: projected pointing axis 1 (Alpha)
     * % Pb: projected pointinng axis 2 (Beta)
     * % WCS: Coordinate system (see SupremePhoto)
     * %
     * % outputs:
     * %---------
     * % NPa : Normalised pointing oon axis 1 (1<=NPa)
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetIndex(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetIsoResolutionCubes</code> 
     * M-function with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function IsoCubes = GetIsoResolutionCubes(Cubes,fBeams,Waves,IsoWave)
     * %
     * % A utility fuction used to calculated IsoResolution cubes for a given
     * % wave number
     * % 
     * % Inputs:
     * %---------
     * % Cubes (cell array 1xNcubes): Full resolution cubes 
     * % fBeams (cell array 1xNcubes): fourier domain beams corresponding to cubes
     * % Waves (cell array 1xNcubes): Wave number corresponding to cubes
     * % IsoWave (scalar): wave number for which the Iso resolved cubes are
     * % calculated
     * %
     * % Output:
     * %---------
     * % IsoCubes (cell array 1xNcubes): Cubes with the same resolution as the
     * % cube given by wave IsoWave
     * %
     * % 
     * % Copyrights Hacheme AYASSO , GIPSA-LAB, 2013
     * %
     * % ver 0.5
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetIsoResolutionCubes(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetLegInd</code> M-function 
     * with 2 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [LegInd DatRed] = GetLegInd(Time)
     * %
     * % Utility function to help getting Leg index for block data containing
     * % total scan. 
     * %
     * % Copyright Hacheme AYASSO,  IAS 2011
     * % 
     * % ver 1.5
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetLegInd(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetSpectrum2D</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetSpectrum2D(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetSpectrum2D_plugin</code> 
     * M-function with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2D(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetSpectrum2D_plugin(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetSpectrum2DLE</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * %   function [Datas Errs wave Poss Masks ObjNames 
     * Centers]=GetSpectrum2DLE(FileName,Factor,UseMask)
     * % 
     * %   Import level 1 data for SPIRE FTS instrument even with the existance of
     * %   missing jiggles.
     * %   
     * %   New in Version 2:
     * %   -----------------
     * %   Several observation importation
     * %   Possibility to account for HIPE Mask or not (in case the mask is not valid)
     * % 
     * %   New in Version LE: 
     * %   ------------------
     * %   Import L1 files with missing jiggle positions
     * % 
     * %   Inputs:
     * %    -------
     * %   FileName : should be a cell array (even for one observation) that contains the 
     * name of L1 files
     * %   Factor (>=1, Default=1): Reduction factor for wavelength domain (used only for 
     * debug to run the method faster)
     * %   UseMask (1 or 0, Default=1): option to account for Mask information
     * %   present in the L1 FITS file. 
     * %
     * %   Copyrights Hacheme AYASSO 2012, GIPSA & IAS
     * % 
     * %   Version 2.0
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetSpectrum2DLE(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>GetWCS</code> M-function 
     * with 5 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * No usage documentation is available for this function.  (To fix this, the function 
     * author should insert a help comment at the beginning of their M code.  See the 
     * MATLAB documentation for more details.)
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] GetWCS(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>InitJavaBrew</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * No usage documentation is available for this function.  (To fix this, the function 
     * author should insert a help comment at the beginning of their M code.  See the 
     * MATLAB documentation for more details.)
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] InitJavaBrew(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>InitJavaBrownies</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * No usage documentation is available for this function.  (To fix this, the function 
     * author should insert a help comment at the beginning of their M code.  See the 
     * MATLAB documentation for more details.)
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] InitJavaBrownies(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>JavaBrew</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrew(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Extended emission and additive White noise  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] JavaBrew(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>JavaBrownies</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % function [Posterior]=JavaBrownies(y,OP,Prior)
     * %
     * % JavaBrownies = Joint Automatic VAriational Bayesian Reconstruction for
     * % Offseted White NoIsE and Smooth fields  
     * % 
     * % 
     * % Inputs: 
     * % --------
     * % y : the data.
     * % OP: Structure containing the operators 
     * % Prior: structure containing the prior informations (metahyperparameters value)
     * % 
     * % Output:
     * % --------
     * % Posterior: structure containing the whole posterior values of the model 
     * %  for a deeper analysis 
     * % 
     * % Copyright Hacheme AYASSO, IAS & GIPSA-LAB 2011-2012
     * % 
     * % Version 2.0
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] JavaBrownies(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>Map2FITS</code> M-function 
     * with 4 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %   function []=Map2FITS(FileName,MAP,WCS,Extra)
     * %
     * %   A utility function to export 2D map to FITS format
     * %
     * % Inputs:
     * % --------
     * % FileName : FITS filename
     * % MAP: the map to export
     * % WCS: Coordinate system
     * % Extra: extra parameters
     * %
     * %
     * </pre>
     *
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] Map2FITS(Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>MyFitsRead</code> M-function 
     * with 1 input argument.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %FITSREAD Read data from FITS file
     * %
     * %   DATA = FITSREAD(FILENAME) reads data from the primary data of the FITS
     * %   (Flexible Image Transport System) file FILENAME.  Undefined data values
     * %   will be replaced by NaN.  Numeric data will be scaled by the slope and
     * %   intercept values and is always returned in double precision.
     * %
     * %   DATA = FITSREAD(FILENAME,OPTIONS) reads data from a FITS file according
     * %   to the options specified in OPTIONS.  Valid options are:
     * %
     * %   EXTNAME      EXTNAME can be either 'primary', 'asciitable', 'binarytable',
     * %                'image', or 'unknown' for reading data from the primary
     * %                data array, ASCII table extension, Binary table extension,
     * %                Image extension or an unknown extension respectively. Only
     * %                one extension should be supplied. DATA for ASCII and
     * %                Binary table extensions is a 1-D cell array. The contents
     * %                of a FITS file can be located in the Contents field of the
     * %                structure returned by FITSINFO.
     * %
     * %   EXTNAME,IDX  Same as EXTNAME except if there is more than one of the
     * %                specified extension type, the IDX'th one is read.
     * %
     * %   'Raw'        DATA read from the file will not be scaled and undefined
     * %                values will not be replaced by NaN.  DATA will be the same
     * %                class as it is stored in the file.
     * %
     * %   'Info',INFO  When reading from a FITS file multiple times, passing
     * %                the output of FITSINFO with the 'Info' parameter helps
     * %                FITSREAD locate the data in the file more quickly.
     * %
     * %   'PixelRegion',{ROWS, COLS, ..., N_DIM}  
     * %                FITSREAD returns the sub-image specified by the boundaries
     * %                for an N dimensional image. ROWS, COLS, ..., N_DIM are
     * %                each vectors of 1-based indices given either as START,
     * %                [START STOP] or [START INCREMENT STOP] selecting the
     * %                sub-image region for the corresponding dimension. This
     * %                parameter is valid only for primary or image extensions.
     * %
     * %   'TableColumns',COLUMNS
     * %                COLUMNS is a vector with 1-based indices selecting the
     * %                columns to read from the ASCII or Binary table extension.
     * %                This vector should contain unique and valid indices into
     * %                the table data specified in increasing order. This
     * %                parameter is valid only for ASCII or Binary extensions.
     * %
     * %   'TableRows',ROWS
     * %                ROWS is a vector with 1-based indices selecting the rows
     * %                to read from the ASCII or Binary table extension. This
     * %                vector should contain unique and valid indices into the
     * %                table data specified in increasing order. This parameter
     * %                is valid only for ASCII or Binary extensions.
     * %                
     * %            
     * %   Example: Read primary data from file.
     * %      data = fitsread('tst0012.fits');
     * %
     * %   Example: Inspect available extensions, read 'image' extension using the
     * %   EXTNAME option.
     * %      info      = fitsinfo('tst0012.fits');
     * %      % List of contents, includes any extensions if present.
     * %      disp(info.Contents);
     * %      imageData = fitsread('tst0012.fits','image');
     * %
     * %   Example: Subsample the fifth plane of 'image' extension by 2.
     * %      info        = fitsinfo('tst0012.fits');
     * %      rowend      = info.Image.Size(1);
     * %      colend      = info.Image.Size(2);
     * %      primaryData = fitsread('tst0012.fits','image',...
     * %                     'Info', info,...
     * %                     'PixelRegion',{[1 2 rowend], [1 2 colend], 5 });
     * %
     * %   Example: Read every other row from a ASCII table data.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.AsciiTable.Rows; 
     * %      tableData = fitsread('tst0012.fits','asciitable',...
     * %                   'Info',info,...
     * %                   'TableRows',[1:2:rowend]);
     * %
     * %   Example: Read all data for the first, second and fifth column of the
     * %   Binary table.
     * %      info      = fitsinfo('tst0012.fits');
     * %      rowend    = info.BinaryTable.Rows;       
     * %      tableData = fitsread('tst0012.fits','binarytable',...
     * %                   'Info',info,...
     * %                   'TableColumns',[1 2 5]);
     * %
     * %
     * %   See also FITSINFO.
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] MyFitsRead(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>MyImResize</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImResize(Im,Fctr,InType)
     * % 
     * % A utility function to resize an image (Im) by a factor (Fctr) using (InType)
     * % Interpolation method. (This function simply to avoid using imresize of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] MyImResize(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>MyImRotate</code> M-function 
     * with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % function [RotIm] = MyImRotate(Im,Ngl,InType)
     * % 
     * % A utility function to rotate an image (Im) by a angle (Ngl, given in degrees) 
     * using (InType)
     * % Interpolation method. (This function simply to avoid using imrotate of image 
     * processing toolbox)
     * % 
     * % Copyrights Hacheme AYASSO, GIPSA-LAB 2012
     * % 
     * % V1.0
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] MyImRotate(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>PrepareData_plugin</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % data = cell(NOBS);
     * % ra = cell(NOBS);
     * % dec = cell(NOBS);
     * % mask = cell(NOBS);
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] PrepareData_plugin(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>ProjectPointing</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %   function [NPA NPB] = ProjectPointing(RA,DEC,WCS)
     * %
     * %   Project angular pointing (RA,DEC) on a planar grid (PA,PB) around the
     * %   center. The function returns a normalised projected pointing 
     * % 
     * % 
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] ProjectPointing(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>setFitsValue</code> 
     * M-function with 3 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %
     * % mfits: A simple fits package fo MATLAB.
     * %
     * % Author: Jason D. Fiege, University of Manitoba, 2010
     * % fiege@physics.umanitoba.ca
     * %
     * % The main component is a FITS writer fits_write to complement MATLAB's
     * % built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
     * % returns the header information in a more convenient form than the
     * % built-in fitsinfo function.  Additional functionality is provides by
     * % getFitsValue and setFitsValue to get and set a single header field.
     * %
     * % -------------------------
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] setFitsValue(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>SupremePhoto</code> 
     * M-function with 15 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremePhoto(Data,RA,DEC,Mask,Time,WCS,RhosOp,RhosVal,RhonOp,RhonVal,OffsetOp,BeamOp,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A mapmaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Nmes x Nbolo): is a 2D matrix where each ligne contains
     * % measurements for all bolometers at a given sampling instant. Attention
     * % The dimesion of this matrix is used to determine Band and the PSF
     * %   88<Nbolo<139 --> PSW
     * %   43<Nbolo<=88 --> PMW
     * %   0<Nbolo<=43  --> PLW
     * % 
     * % RA,DEC (Nmes x Nbolo): are 2 matrixs containing the RA and DEC of the
     * % detecor positions.
     * %   
     * % Mask (Nmes x Nbolo): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Time (Nmes) : a column vector containing time instant for each sample
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  WCS.POSANGLE = Observation angle (for BEAM rotation)
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhon' or 'SetRhon'): see RhosOp
     * %
     * % RhonVal (real positive): see above
     * %
     * % OffsetOp (String='GetOffsetLeg', 'GetOffsetAll' or 'ZeroOffset'): 'GetOffsetLeg' 
     * or will activate
     * % joint estimation of detectors offsets either per scan leg or in a global
     * % way for whole observation. 'ZeroOffset' will deactivate offset estimation
     * % and supposes that the offset was aleardy substracted from the data.
     * %
     * % BeamOp (String='Simulated' or 'Empiric'): Determine which beam to use.
     * % Two beam models are possible: simulated or emperic.
     * %
     * % PointingOp (String= 'Nearest' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Map: Output map
     * % STD: Standard deviation map from the estimator
     * % ErrMap: a projected map of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.Offset: Estimated Offsets
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.VarOffset: the variance of Offsets estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the map.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 2.4.2
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] SupremePhoto(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>SupremePhoto_plugin</code> 
     * M-function with 16 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] SupremePhoto_plugin(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the <code>SupremeSpectro</code> 
     * M-function with 13 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * % 
     * % function [Map,STD,ErrMap,Extra]= 
     * SupremeSpectro(Data,NPa,NPb,Mask,Wave,WCS,RhosOp,RhosVal,RhonOp,RhonVal,PointingOp,StopMode,StopValue)
     * % 
     * %
     * %  A CubeMaking method for SPIRE/Herschel Data based on JavaBrownies solver.
     * %
     * % Inputs: 
     * % --------
     * % Data (Njig x Nbolo x Nwave) : is a 3D matrix where each ligne contains
     * % measurements for all bolometers at a given Jiggle postion. The thrid dimension
     * % 
     * % NPa,NPb (Nmes x Nbolo): are 2 matrixs containing the normalised pointing
     * % of the detectors (obtained using ProjectPointing)
     * %   
     * % Mask (Njig x Nbolo x Nwave): a logical matrix indicating if the data should be
     * % masked (True indicate masked data). Masked data are not used. 
     * % 
     * % Wave (Nwave) : a line vector containing wave numbers
     * % 
     * % WCS (Structure): WCS.CDELT1 = step size in degrees for 1st Axis     
     * %                  WCS.CDELT2 = step size in degrees for 2nd Axis
     * %                  WCS.NAXIS1 = number of points in 1st Axis
     * %                  WCS.NAXIS2 = number of points in 2nd Axis
     * %                  WCS.CRPIX1 = center pixel 1st coordinate 
     * %                  WCS.CRPIX2 = center pixel 2nd coordinate
     * %                  WCS.CRVAL1 = center pixel value
     * %                  WCS.CRVAL2 = center pixel value
     * %                  
     * %                  
     * % RhosOp (String='GetRhos' or 'SetRhos' ): 'GetRhos' will activate rhos 
     * %            estimation jointly with the map in this case the RhosVal is
     * %            ignored. Otherwise, the Rhos is fixed to RhosVal
     * %
     * % RhosVal (real positive): See above
     * %
     * % RhonOp (String='GetRhonFull', 'GetRhonPartial', 'GetRhonIso'  or 'SetRhon'): 
     * %  Selects the noise variance estimation mode, the 'full' option will
     * %  estimate one variance per wavelength, the 'partial' estimates a variance 
     * %  for a subset of wavelengths (one variance for 25 wavelengths), with 'Iso'
     * % the same variance is supposed 
     * % RhonVal (real positive): see above
     * %
     * %
     * % PointingOp (String= 'Naive' or 'Bilinear'): determine the pointing
     * % model. Two models are available: nearest and bilinear 
     * %
     * % StopMode (String= 'Automatic' or 'Manual'): determine stoping mode. Two
     * % modes are available: Automatic (based on free energy evolution) or
     * % manual.
     * % 
     * % StopValue (real positive): determine the stoping value according to stopping 
     * mode.
     * %  If autmatic mode is set the stop value correspond to tolerance in
     * %  Free energy variation. In manual mode StopValue correspond to the number of 
     * iterations 
     * %
     * % ----------------------------
     * % 
     * % Outputs:
     * % ---------
     * % Cube: Output cube
     * % STD: Standard deviation cube from the estimator
     * % ErrMap: a projected cube of error
     * % Extra (stucture): Extra.Coadd : a coadded map using Bilinear projecteur 
     * %                   Extra.Rhon :  Estimated noise  
     * %                   Extra.Rhos: Estimated source 
     * %                   Extra.VarRhon: the variance of Rhon estimator
     * %                   Extra.VarRhos: the variance of Rhos estimator
     * %                   Extra.FreeEnergy: The log of free energy evolution
     * %                   during iterations
     * %                   Extra.EqBeam :Equivelent beam for the cube.
     * %
     * % ------------------------------
     * %
     * % Copyrights Hacheme AYASSO, IAS & GIPSA 2012
     * % 
     * % Ver, internal 1.0
     * %
     * % Version 1.5
     * %
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] SupremeSpectro(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the 
     * <code>SupremeSpectro_cubeMaking</code> M-function with 13 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * %save('dataHipeCell','dataHIPE')
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] SupremeSpectro_cubeMaking(int nargout, Object... rhs) throws RemoteException;
    /**
     * Provides the standard interface for calling the 
     * <code>SupremeSpectro_isoResCubes</code> M-function with 7 input arguments.  
     *
     * Input arguments to standard interface methods may be passed as sub-classes of 
     * <code>com.mathworks.toolbox.javabuilder.MWArray</code>, or as arrays of any 
     * supported Java type (i.e. scalars and multidimensional arrays of any numeric, 
     * boolean, or character type, or String). Arguments passed as Java types are 
     * converted to MATLAB arrays according to default conversion rules.
     *
     * All inputs to this method must implement either Serializable (pass-by-value) or 
     * Remote (pass-by-reference) as per the RMI specification.
     *
     * M-documentation as provided by the author of the M function:
     * <pre>
     * 
     * %save('/Users/boualam/Work/SUPREME_SPECTRO/SupremeSpectro_Keep/spectroData','SSW','SLW','fEqBeamSSW','fEqBeamSLW','WaveSSW','WaveSLW')
     * </pre>
     *
     * @param nargout Number of outputs to return.
     * @param rhs The inputs to the M function.
     *
     * @return Array of length nargout containing the function outputs. Outputs are 
     * returned as sub-classes of <code>com.mathworks.toolbox.javabuilder.MWArray</code>. 
     * Each output array should be freed by calling its <code>dispose()</code> method.
     *
     * @throws java.jmi.RemoteException An error has occurred during the function call or 
     * in communication with the server.
     */
    public Object[] SupremeSpectro_isoResCubes(int nargout, Object... rhs) throws RemoteException;
  
    /** Frees native resources associated with the remote server object */
    void dispose() throws RemoteException;
}
