package pluginSupremePhoto;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import SupremePhotoMatlab2Java.*;
import com.mathworks.toolbox.javabuilder.*;

import herschel.ia.task.Task;
import herschel.ia.task.TaskParameter;
import herschel.ia.pal.ListContext;
import herschel.ia.dataset.*;
import herschel.ia.numeric.*;
import herschel.ia.dataset.image.SimpleImage;
import herschel.share.unit.FluxDensity;
import herschel.share.unit.Scalar;
import herschel.ia.dataset.image.wcs.Wcs;
import herschel.share.interpreter.InterpreterUtil;


public class SupremePhotoMapMakingTask extends Task {
	
    public SupremePhotoMapMakingTask() {
    	
        super("supremePhotoMapMaking");
        setDescription("Super Resolution algorithm for L1 Spire Photo Data");
        
        // Appel HIPE: ##outSupreme = supremePhoto(dirPhoto,data,NumItr,NumBand,AlphaStep,turnOver)
        
        //data
        TaskParameter parameter = new TaskParameter("data", ListContext.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("ListContext containing all the Product data");
        addTaskParameter(parameter);
        
        //turnover
        parameter = new TaskParameter("turnover", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("NoTurnOver"));
        parameter.setDescription("TurnOver or NoTurnOver");
        addTaskParameter(parameter);
        
        //alphaStep
        parameter = new TaskParameter("AlphaStep", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Alpha step angle in arcsec");
        addTaskParameter(parameter);
        
        //RhosOp
        parameter = new TaskParameter("RhosOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("GetRhos"));
        parameter.setDescription("'GetRhos' or 'SetRhos'");
        addTaskParameter(parameter);
        
        //RhosVal
        parameter = new TaskParameter("RhosVal", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Double(0));
        parameter.setDescription("Rhos Value");
        addTaskParameter(parameter);
        
        //RhonOp
        parameter = new TaskParameter("RhonOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("GetRhon"));
        parameter.setDescription("'GetRhon' or 'SetRhon'");
        addTaskParameter(parameter);
        
        //RhonVal
        parameter = new TaskParameter("RhonVal", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Double(0));
        parameter.setDescription("Rhon Value");
        addTaskParameter(parameter);
        
        //OffsetOp
        parameter = new TaskParameter("OffsetOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("GetOffsetLeg"));
        parameter.setDescription("'GetOffsetLeg', 'GetOffsetAll' or 'ZeroOffset'");
        addTaskParameter(parameter);
        
        //BeamOp
        parameter = new TaskParameter("BeamOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("Simulated"));
        parameter.setDescription("'Simulated' or 'Empiric'");
        addTaskParameter(parameter);
        
        //PointingOp
        parameter = new TaskParameter("PointingOp", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("Bilinear"));
        parameter.setDescription("'Nearest' or 'Bilinear'");
        addTaskParameter(parameter);
        
        //StopMode
        parameter = new TaskParameter("StopMode", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new String("Manual"));
        parameter.setDescription("'Automatic' (based on free energy evolution) or 'Manual'");
        addTaskParameter(parameter);
        
        //StopValue
        parameter = new TaskParameter("StopValue", Double.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Determine the stoping value according to stopping mode");
        addTaskParameter(parameter);
        
        //dirBeams
        parameter = new TaskParameter("dirBeams", String.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(true);
        parameter.setDescription("Directory with beams fits file");
        addTaskParameter(parameter);
        
        //display
        parameter = new TaskParameter("display", Integer.class);
        parameter.setType(TaskParameter.IN);
        parameter.setMandatory(false);
        parameter.setDefaultValue(new Integer(0));
        parameter.setDescription("Dislay the evolution of the computation or not");
        addTaskParameter(parameter);
        
        //outList
        parameter = new TaskParameter("outList", ArrayList.class);
        parameter.setType(TaskParameter.OUT);
        parameter.setMandatory(false);
        parameter.setDescription("List of Product: Map: Output map, Estimated standard deviation map, Extra");
        addTaskParameter(parameter);

        
    }
    
    public void execute() {
        
    	ListContext data = (ListContext) getParameter("data").getValue();
    	String turnover = (String) getParameter("turnover").getValue();
		Double AlphaStep = (Double) getParameter("AlphaStep").getValue();
		String RhosOp = (String) getParameter("RhosOp").getValue();
		Double RhosVal = (Double) getParameter("RhosVal").getValue();
		String RhonOp = (String) getParameter("RhonOp").getValue();
		Double RhonVal = (Double) getParameter("RhonVal").getValue();
		String OffsetOp = (String) getParameter("OffsetOp").getValue();
		String BeamOp = (String) getParameter("BeamOp").getValue();
		String PointingOp = (String) getParameter("PointingOp").getValue();
		String StopMode = (String) getParameter("StopMode").getValue();
		Double StopValue = (Double) getParameter("StopValue").getValue();
    	String dirBeams = (String) getParameter("dirBeams").getValue();
    	Integer display = (Integer) getParameter("display").getValue();

    	//Input integrity Check
    	
    	if (data==null) {throw new NullPointerException("ListContext data is Null");}
    	
    	if (AlphaStep<0) {throw new IllegalArgumentException("AlphaStep should be positive.");}
    	
    	List<String> RhosOpList = new ArrayList<String>();
        String RhosOp_Values[]={"GetRhos","SetRhos"};
        for (int i = 0; i < RhosOp_Values.length; i++) {RhosOpList.add(RhosOp_Values[i]);}
        if(!RhosOpList.contains(RhosOp)) {throw new IllegalArgumentException("RhosOp - "+RhosOp+" is not a valid RhosOp option.");}
        
    	if (RhosVal<0) {throw new IllegalArgumentException("RhosVal should be positive.");}
        
    	List<String> RhonOpList = new ArrayList<String>();
        String RhonOp_Values[]={"GetRhon","SetRhon"};
        for (int i = 0; i < RhonOp_Values.length; i++) {RhonOpList.add(RhonOp_Values[i]);}
        if(!RhonOpList.contains(RhonOp)) {throw new IllegalArgumentException("RhonOp - "+RhonOp+" is not a valid RhonOp option.");}
        
    	if (RhonVal<0) {throw new IllegalArgumentException("RhonVal should be positive.");}

    	List<String> OffsetOpList = new ArrayList<String>();
        String OffsetOp_Values[]={"GetOffsetLeg","GetOffsetAll","ZeroOffset"};
        for (int i = 0; i < OffsetOp_Values.length; i++) {OffsetOpList.add(OffsetOp_Values[i]);}
        if(!OffsetOpList.contains(OffsetOp)) {throw new IllegalArgumentException("OffsetOp - "+OffsetOp+" is not a valid OffsetOp option.");}
        
    	List<String> BeamOpList = new ArrayList<String>();
        String BeamOp_Values[]={"Simulated","Empiric"};
        for (int i = 0; i < BeamOp_Values.length; i++) {BeamOpList.add(BeamOp_Values[i]);}
        if(!BeamOpList.contains(BeamOp)) {throw new IllegalArgumentException("BeamOp - "+BeamOp+" is not a valid BeamOp option.");}
        
    	List<String> PointingOpList = new ArrayList<String>();
        String PointingOp_Values[]={"Nearest","Bilinear"};
        for (int i = 0; i < PointingOp_Values.length; i++) {PointingOpList.add(PointingOp_Values[i]);}
        if(!PointingOpList.contains(PointingOp)) {throw new IllegalArgumentException("PointingOp - "+PointingOp+" is not a valid PointingOp option.");}
        
    	List<String> StopModeList = new ArrayList<String>();
        String StopMode_Values[]={"Automatic","Manual"};
        for (int i = 0; i < StopMode_Values.length; i++) {StopModeList.add(StopMode_Values[i]);}
        if(!StopModeList.contains(StopMode)) {throw new IllegalArgumentException("StopMode - "+StopMode+" is not a valid StopMode option.");}

    	if (StopValue<0) {throw new IllegalArgumentException("StopValue should be positive.");}
    	
    	dirBeams = dirBeams.replace("~",System.getProperty("user.home"));
    	File dirB = new File(dirBeams);
    	if (!dirB.exists()) {throw new IllegalArgumentException(dirBeams+" does not exists ! Please specify a valid beams directory.");}
    	
    	if (dirBeams.substring(dirBeams.length()-1)!=File.separator){dirBeams = dirBeams+File.separator;}
    	
    	String beamFiles[]={"theoretical_spire_beam_model_psw_V0_2.fits","theoretical_spire_beam_model_pmw_V0_2.fits","theoretical_spire_beam_model_plw_V0_2.fits",
            "plw_Cbeam_1arcsec.mat","pmw_Cbeam_1arcsec.mat","psw_Cbeam_1arcsec.mat"};
    	String allBeamFiles = new String();
    	for (int i = 0; i < beamFiles.length; i++) {allBeamFiles=allBeamFiles+"\n"+beamFiles[i];}
    	for (int i = 0; i < beamFiles.length; i++) {
    		File beamFile = new File(dirBeams+beamFiles[i]);
    		if (!beamFile.exists()) {throw new IllegalArgumentException(beamFiles[i]+" does not exists in "+dirBeams+". Please check your beams files directory. It should contains:"+allBeamFiles);}
    		}

    	//Input integrity Check Done !!
    	
        boolean verbose = false;
        
        List<Object> outList = new ArrayList<Object>();
        String obsids = new String("");
        String objectName = new String();
        
        try {
        	 
        	Integer nbrObs = data.getAllRefs().size();    	
        	
            int[] dim = {9,nbrObs};
        	
            MWCellArray Data = new MWCellArray(dim);
        	
            objectName = (String)(data.getRefs().get(0).getProduct().getMeta().get("object").getValue());
            
            for(int i=0;i<nbrObs;i++){

            	Product dataObs = new Product();
            	dataObs = data.getRefs().get(i).getProduct();
            	            	
            	Long obsid = (Long)(dataObs.getMeta().get("obsid").getValue());
            	
            	obsids = obsids + obsid.toString() + " ";
            	
            	System.out.print("Preparing Data for SupremePhoto from obsid "+obsid+" ... ");
            	InterpreterUtil.println("Preparing Data for SupremePhoto from obsid "+obsid+" ... ");
            	
            	double[][] sigObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("signal")).getData()).getArray();
            	MWNumericArray sigMat = new MWNumericArray(sigObs, MWClassID.DOUBLE);
            	int[] idxSig = {1, i+1};
            	Data.set(idxSig, sigMat);
            	if (verbose){System.out.println("#sig");}
            	
            	double[][] raObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("ra")).getData()).getArray();
            	MWNumericArray raMat = new MWNumericArray(raObs, MWClassID.DOUBLE);
            	int[] idxRa = {2, i+1};
            	Data.set(idxRa, raMat);
            	if (verbose){System.out.println("#ra");}
            	
            	double[][] decObs = ((Double2d) ((ArrayDataset) dataObs.getSets().get("dec")).getData()).getArray();
            	MWNumericArray decMat = new MWNumericArray(decObs, MWClassID.DOUBLE);
            	int[] idxDec = {3, i+1};
            	Data.set(idxDec, decMat);
            	if (verbose){System.out.println("#dec");}
            	
            	short[][] maskObs = ((Short2d) ((ArrayDataset) dataObs.getSets().get("mask")).getData()).getArray();
            	MWNumericArray maskMat = new MWNumericArray(maskObs, MWClassID.INT16);
            	int[] idxMsk = {4, i+1};
            	Data.set(idxMsk, maskMat);
            	if (verbose){System.out.println("#msk");}
            	
            	short[] turnObs = ((Short1d) ((ArrayDataset) dataObs.getSets().get("turnover")).getData()).getArray();
            	MWNumericArray turnMat = new MWNumericArray(turnObs, MWClassID.INT16);
            	int[] idxTrn = {5, i+1};
            	Data.set(idxTrn, turnMat);
            	if (verbose){System.out.println("#turn");}
            	
            	double[] timeObs = ((Double1d) ((ArrayDataset) dataObs.getSets().get("time")).getData()).getArray();
            	MWNumericArray timeMat = new MWNumericArray(timeObs, MWClassID.DOUBLE);
            	int[] idxTim = {6, i+1};
            	Data.set(idxTim, timeMat);
            	if (verbose){System.out.println("#time");}
            	
            	double ra_nomObs = (Double)(dataObs.getMeta().get("ra_nom").getValue());
            	MWNumericArray ra_nomMat = new MWNumericArray(ra_nomObs, MWClassID.DOUBLE);
            	int[] idxRaNom = {7, i+1};
            	Data.set(idxRaNom, ra_nomMat);
            	if (verbose){System.out.println("#ranom");}
            	
            	double dec_nomObs = (Double)(dataObs.getMeta().get("dec_nom").getValue());
            	MWNumericArray dec_nomMat = new MWNumericArray(dec_nomObs, MWClassID.DOUBLE);
            	int[] idxDecNom = {8, i+1};
            	Data.set(idxDecNom, dec_nomMat);
            	if (verbose){System.out.println("#decnom");}
            	
            	double posangleObs = (Double)(dataObs.getMeta().get("posAngle").getValue());
            	MWNumericArray posangleMat = new MWNumericArray(posangleObs, MWClassID.DOUBLE);
            	int[] idxPos = {9, i+1};
            	Data.set(idxPos, posangleMat);
            	if (verbose){System.out.println("#posangle");}
            	            	
            	System.out.println("Done!");
            	InterpreterUtil.println("Done!");
            	
    		}
        	
			System.out.println("Starting Supreme Photo Core Task...");
			InterpreterUtil.println("Starting Supreme Photo Core Task...");
			SupremePhotoMatlab x = new SupremePhotoMatlab();
			int fitsMatlab = 0;
			Object[] outSupreme = x.SupremePhoto_plugin(5,nbrObs,Data,turnover,AlphaStep,RhosOp,RhosVal,RhonOp,RhonVal,OffsetOp,BeamOp,PointingOp,StopMode,StopValue,dirBeams,display,fitsMatlab);
			//[Xh Xstd Xerrmap Extra WCS]
			
			//Extract Image
			System.out.print("Extract Image... ");
			InterpreterUtil.println("Extract Image... ");
			MWNumericArray Xh = (MWNumericArray)outSupreme[0]; 
			double[][] Xh_image = (double[][]) Xh.toDoubleArray();		
			
			/*
			int r = Xh_image.length;
			int c = Xh_image[r].length;
			for (int i = 0; i < r; i++) {
	            for (int j = i+1; j < Xh_image[r].length; j++) {
	                double temp = Xh_image[i][j];
	                Xh_image[i][j] = Xh_image[j][i];
	                Xh_image[j][i] = temp;
			*/
			
			//Extract Image Error
			System.out.print("ImageError... ");
			InterpreterUtil.println("ImageError... ");
			MWNumericArray Xerr = (MWNumericArray)outSupreme[2]; 
			double[][] Xerr_image = (double[][]) Xerr.toDoubleArray();
			
			//Extract and Create WCS
			System.out.println("Extract & Create WCS for the Map... ");
			InterpreterUtil.println("Extract & Create WCS for the Map... ");
			MWStructArray WCS = (MWStructArray)outSupreme[4];
			System.out.println(WCS);
			InterpreterUtil.println(""+WCS);
			MWNumericArray cdelt1 = (MWNumericArray)WCS.getField("CDELT1", 1);
			MWNumericArray cdelt2 = (MWNumericArray)WCS.getField("CDELT2", 1);
			MWNumericArray crval1 = (MWNumericArray)WCS.getField("CRVAL1", 1);
			MWNumericArray crval2 = (MWNumericArray)WCS.getField("CRVAL2", 1);
			MWNumericArray crpix1 = (MWNumericArray)WCS.getField("CRPIX1", 1);
			MWNumericArray crpix2 = (MWNumericArray)WCS.getField("CRPIX2", 1);
			MWNumericArray naxis1 = (MWNumericArray)WCS.getField("NAXIS1", 1);
			MWNumericArray naxis2 = (MWNumericArray)WCS.getField("NAXIS2", 1);
			MWNumericArray posang = (MWNumericArray)WCS.getField("POSANGLE", 1);
			Wcs wcs = new Wcs();
			wcs.setCdelt1(cdelt1.getDouble());
			wcs.setCdelt2(cdelt2.getDouble());
			wcs.setCrval1(crval1.getDouble());
			wcs.setCrval2(crval2.getDouble());
			wcs.setCrpix1(crpix1.getDouble());
			wcs.setCrpix2(crpix2.getDouble());
			wcs.setNAxis(2);
			wcs.setNaxis1(naxis1.getInt());
			wcs.setNaxis2(naxis2.getInt());
			wcs.setParameter("POSANGLE", posang.getDouble(), "");
			wcs.setCunit1("DEG");
			wcs.setCunit2("DEG");
			wcs.setCtype1("RA---TAN");
			wcs.setCtype2("DEC--TAN");
			
			//Creating Main Map
			System.out.print("Creating Simple Image for HIPE... ");
			InterpreterUtil.println("Creating Simple Image  for HIPE... ");
			SimpleImage outMap = new SimpleImage();
			outMap.setDescription("Output Map from SupremePhoto");
			//outMap.setCreator("SUPREME Photometer Version 2.4");
			//outMap.setInstrument("SPIRE");
			//outMap.setFormatVersion("2.4");
			outMap.setUnit(FluxDensity.JANSKYS.divide(Scalar.BEAM));
			outMap.setImage(new Double2d(Xh_image));
			outMap.setError(new Double2d(Xerr_image));
			outMap.setWcs(wcs);
			
			//METADATA
			MetaData metaMap = new MetaData();
			metaMap.set("creator", new StringParameter("SUPREME Photometer"));
			metaMap.set("formatVersion", new StringParameter("Version 2.4"));
			metaMap.set("instrument", new StringParameter("SPIRE Photometer"));
			metaMap.set("obsids", new StringParameter(obsids));
			metaMap.set("AlphaStep", new StringParameter(AlphaStep.toString()));
			metaMap.set("turnover", new StringParameter(turnover));
			metaMap.set("RhonOp", new StringParameter(RhonOp));
			metaMap.set("RhonVal", new StringParameter(RhonVal.toString()));
			metaMap.set("RhosOp", new StringParameter(RhosOp));
			metaMap.set("RhosVal", new StringParameter(RhosVal.toString()));
			metaMap.set("OffsetOp", new StringParameter(OffsetOp));
			metaMap.set("PointingOp", new StringParameter(PointingOp));
			metaMap.set("BeamOp", new StringParameter(BeamOp));
			metaMap.set("StopMode", new StringParameter(StopMode));
			metaMap.set("StopValue", new StringParameter(StopValue.toString()));
			outMap.setMeta(metaMap);
			
			System.out.println(" & Adding it to outList");
			outList.add(outMap);
			
			//Extract Standard Deviation Map
			System.out.println("Extract Standard Deviation Map");
			InterpreterUtil.println("Extract Standard Deviation Map");
			MWNumericArray Xstd = (MWNumericArray)outSupreme[1]; 
			double[][] Xstd_image = (double[][]) Xstd.toDoubleArray();
						
			//Creating STD Map
			System.out.print("Creating Standard Deviation Map for HIPE... ");
			InterpreterUtil.println("Creating Standard Deviation Map for HIPE... ");
			SimpleImage outMapSTD = new SimpleImage();
			outMapSTD.setDescription("Output Standard Deviation Map from SupremePhoto");
			outMapSTD.setUnit(FluxDensity.JANSKYS.divide(Scalar.BEAM));
			outMapSTD.setImage(new Double2d(Xstd_image));
			outMapSTD.setWcs(wcs);
			
			System.out.println(" & Adding it to outList");
			outList.add(outMapSTD);
			
			//Extra information
			System.out.println("Extra information... ");
			InterpreterUtil.println("Extra information... ");
			MWStructArray Extra = (MWStructArray)outSupreme[3];
			MWNumericArray Coadd = (MWNumericArray)Extra.getField("Coadd", 1);
			MWNumericArray Offset = (MWNumericArray)Extra.getField("Offset", 1);
			MWNumericArray VarOffset = (MWNumericArray)Extra.getField("VarOffset", 1);
			MWNumericArray FreeEnergy = (MWNumericArray)Extra.getField("FreeEnergy", 1);
			MWNumericArray Rhon = (MWNumericArray)Extra.getField("Rhon", 1);
			MWNumericArray Rhos = (MWNumericArray)Extra.getField("Rhos", 1);
			MWNumericArray VarRhon = (MWNumericArray)Extra.getField("VarRhon", 1);
			MWNumericArray VarRhos = (MWNumericArray)Extra.getField("VarRhos", 1);
			MWNumericArray EqBeam = (MWNumericArray)Extra.getField("EqBeam", 1);
			
			//Creating CoAdd Map
			System.out.print("Creating CoAdd Map for HIPE... ");
			InterpreterUtil.println("Creating CoAdd Map for HIPE... ");
			SimpleImage outMapCoAdd = new SimpleImage();
			outMapCoAdd.setDescription("Output CoAdd Map from SupremePhoto");
			outMapCoAdd.setUnit(FluxDensity.JANSKYS.divide(Scalar.BEAM));
			Double2d Coadd_DM = new Double2d((double[][]) Coadd.toDoubleArray());
			outMapCoAdd.setImage(new Double2d(Coadd_DM));
			outMapCoAdd.setWcs(wcs);
			
			System.out.println(" & Adding it to outList");
			outList.add(outMapSTD);
			
			Product ExtraHIPE = new Product();
			ExtraHIPE.setDescription("Extra information from SupremePhoto.");
			//ExtraHIPE.setCreator("SUPREME Photometer Version 2.4");
			//ExtraHIPE.setInstrument("SPIRE");
			//ExtraHIPE.setFormatVersion("2.4");
			/*
			System.out.print("Adding Std... ");
			InterpreterUtil.println("Std...");
			ExtraHIPE.getSets().put("Std",new ArrayDataset(new Double2d(Xstd_image)));
			*/
			System.out.print("Rhon... ");
			InterpreterUtil.println("Rhon... ");
			Double2d Rhon_D = new Double2d((double[][]) Rhon.toDoubleArray());
			ExtraHIPE.getSets().put("Rhon",new ArrayDataset(Rhon_D));
			
			System.out.print("VarRhon... ");
			InterpreterUtil.println("VarRhon... ");
			Double2d VarRhon_D = new Double2d((double[][]) VarRhon.toDoubleArray());
			ExtraHIPE.getSets().put("VarRhon",new ArrayDataset(VarRhon_D));
			
			System.out.print("Rhos... ");
			InterpreterUtil.println("Rhos... ");
			Double2d Rhos_D = new Double2d((double[][]) Rhos.toDoubleArray());
			ExtraHIPE.getSets().put("Rhos",new ArrayDataset(Rhos_D));
			
			System.out.print("VarRhos... ");
			InterpreterUtil.println("VarRhos... ");
			Double2d VarRhos_D = new Double2d((double[][]) VarRhos.toDoubleArray());
			ExtraHIPE.getSets().put("VarRhos",new ArrayDataset(VarRhos_D));
			
			System.out.print("Coadd... ");
			InterpreterUtil.println("Coadd... ");
			Double2d Coadd_D = new Double2d((double[][]) Coadd.toDoubleArray());
			ExtraHIPE.getSets().put("Coadd",new ArrayDataset(Coadd_D));
			
			System.out.print("Offset... ");
			InterpreterUtil.println("Offset... ");
			Double2d Offset_D = new Double2d((double[][]) Offset.toDoubleArray());
			ExtraHIPE.getSets().put("Offset",new ArrayDataset(Offset_D));
			
			System.out.print("VarOffset... ");
			InterpreterUtil.println("VarOffset... ");
			Double2d VarOffset_D = new Double2d((double[][]) VarOffset.toDoubleArray());
			ExtraHIPE.getSets().put("VarOffset",new ArrayDataset(VarOffset_D));
			
			System.out.println("FreeEnergy... ");
			InterpreterUtil.println("FreeEnergy... ");
			Double2d FreeEnergy_D = new Double2d((double[][]) FreeEnergy.toDoubleArray());
			ExtraHIPE.getSets().put("FreeEnergy",new ArrayDataset(FreeEnergy_D));
			
			System.out.println("EqBeam... ");
			InterpreterUtil.println("EqBeam... ");
			Double2d EqBeam_D = new Double2d((double[][]) EqBeam.toDoubleArray());
			ExtraHIPE.getSets().put("EqBeam",new ArrayDataset(EqBeam_D));
			
			MetaData metaEx = new MetaData();
			metaEx.set("creator", new StringParameter("SUPREME Photometer"));
			metaEx.set("formatVersion", new StringParameter("Version 2.4"));
			metaEx.set("instrument", new StringParameter("SPIRE"));
			metaEx.set("Rhon", new StringParameter(Rhon.toString()));
			metaEx.set("VarRhon", new StringParameter(VarRhon.toString()));
			metaEx.set("Rhos", new StringParameter(Rhos.toString()));
			metaEx.set("VarRhos", new StringParameter(VarRhos.toString()));
			ExtraHIPE.setMeta(metaEx);
			
			System.out.println("Adding ExtraHipe to outList");
			outList.add(ExtraHIPE);
			
			
			System.out.println("Final Map Product to HIPE !");
			InterpreterUtil.println("Final Map Product to HIPE !");
			
		} catch (MWException e) {
			
			System.out.println("Supreme Photo Error - Check your MCR paths !");
			e.printStackTrace();
			
		} catch (IOException e1) {
			
			System.out.println("IO Exception !!");
			e1.printStackTrace();
			
		} catch (GeneralSecurityException e2) {
			
			System.out.println("General Security Exception !!");
			e2.printStackTrace();
			
		}
           
        setValue("outList", outList);

	}
    
	
}


