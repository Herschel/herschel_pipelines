from herschel.share.unit import Angle
from herschel.ia.obs.auxiliary.pointing import PointingUtils
from herschel.share.fltdyn.time import FineTime
from herschel.spire.ia.util import SpireMask
from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.toolbox.util import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from herschel.spire.ia.pipeline.phot.fluxconv import *
from herschel.ia.obs.util import ObsParameter
from java.util import List

from java.lang import *
from java.util import *
import time

class SupremePhotoGetDataFromObs(Task):

	def __init__(self,name = "supremePhotoGetDataFromObs"):
		
		version = Configuration.getProjectInfo().getVersion().split('.')
		if int(version[0]) > 5:
			self.setDescription("This task gets all the data and information from a list of observation IDs (in the local "+\
			"pool or from the HSA) that will be used in the SupremePhoto task. The list of observation IDs and the band to process must be given."+\
			"You can use either a poolname or useHSA to retrieve the observations.")
		
		#Input
		p = TaskParameter("obsids",valueType = List, mandatory = 1,\
				description = "List of obsids.")
		self.addTaskParameter(p)
		p = TaskParameter("bandName",valueType = String, mandatory = 1,\
				description = "Band to process.")
		self.addTaskParameter(p)
		p = TaskParameter("poolName",valueType = String, mandatory = 0,\
				defaultValue='', \
				description = "Name of pool from where the observations are loaded.")
		self.addTaskParameter(p)
		p = TaskParameter("useHsa",valueType = Boolean, mandatory = 0,\
				defaultValue=False, \
				description = "UseHSA if you want to get the observations from Herschel Science Archive.")
		self.addTaskParameter(p)
		p = TaskParameter("doApplyExtendedEmissionGains",valueType = Boolean, mandatory = 0,\
				defaultValue=False, \
				description = "Apply extended emission gain task.")
		self.addTaskParameter(p)
		#Output
		p = TaskParameter("data",valueType = ListContext,type = OUT,\
				description = "ListContext containing all the data needed by SupremePhoto.")
		self.addTaskParameter(p)
		__all__=['SupremePhotoGetDataFromObs']
	
	def execute(self):
		
		dataObsList = ListContext()
		
		obsids = self.obsids
		poolName = self.poolName
		useHsa = self.useHsa
		bandName = self.bandName
		doApplyExtendedEmissionGains = self.doApplyExtendedEmissionGains
		
		##### Combine the obsids
		level1_all = SpireListContext()
		cut = [0]
		obsPerObsid = []
		leg = 0
		
		#TODO Idea:
		# Make a loop to store legs in a single level1 object. 
		# Then make another loop to process theses legs. So far I've identified 
		# possible problems, especially in meta data and gain retrieved from calibration that could render the gain
		# function inefficient or lead to some wrong doings. 
		for obsid in obsids:
			if useHsa:
				obs = getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
			else:
				obs = getObservation(obsid=obsid,poolName=poolName,instrument="SPIRE")
			
			obsPerObsid.append(obs)
			level1 = obs.level1
			
			## Create a temporary Level1, Excluding the Serendipity Scan. 
			tmpLevel = Level1Context()
			tmpLevel.meta = level1.meta
			# KD add remove serendipity data
			for ref in level1.refs:
				if ref.meta['bbid'].value>>16 == 0xa103:
					tmpLevel.addRef(ref)
			level1=tmpLevel
			
			if doApplyExtendedEmissionGains:
				chanRelGains = obs.calibration.phot.chanRelGain
			for i in range(level1.count):
				psp = level1.getProduct(i)
				if doApplyExtendedEmissionGains:
					if psp.type=="PPT": 
						psp.setType("PSP")   #for old Level 1 contexts
					psp = applyRelativeGains(psp, chanRelGains)
					
				level1_all.addProduct(psp)
				leg+=1
			cut.append(leg)
		
		##### Destripe the combined level1 (only when we effectively have more than one observation).
		if doApplyExtendedEmissionGains and len(obsids)>1:
			minVel=5
			maxVel=None
			polyDegree=0
			brightSourceThresh=1.5
			roi=None
			kappa=5.0
			kappa2=5.0
			jumpIter=100
			offsetFunction="perScan"
			tempStorage=True
			array=bandName
			print "Destriping... "
			(level1_all,map,diag,p4,p5) = destriper(level1=level1_all, \
                                array=array, nThreads=8, kappa=kappa,kappa2=kappa2, l2DeglitchRepeat=0, jumpThresh=-1, jumpIter=jumpIter, l2IterMax=3, \
                                withMedianCorrected=True, useSink=tempStorage,  offsetFunction=offsetFunction, \
                                polyDegree=polyDegree, brightSourceThresh=brightSourceThresh,  minVel=minVel, maxVel=maxVel)
			#~ destripedProduct = destriper(level1=level1_all, array=bandName, nThreads=4, l2DeglitchRepeat=0, withMedianCorrected=True, useSink=True)
			#~ level1_all = destripedProduct[0].copy()
			#~ map = destripedProduct[1].copy()
			#~ del destripedProduct

		##### Cut level1 according to obsids
		level1_obsid = []
		for i in range(len(cut)-1):
			#scans = Level1Context()
			scans = SpireListContext()
			for j in range(cut[i],cut[i+1]): 
				scans.addRef(level1_all.refs[j])
			level1_obsid.append(scans)
		
		##### Formatting data
		for i in range(len(obsids)):
			print "Preparing data from obsid "+str(obsids[i])
			obsid = obsids[i]
			obs = obsPerObsid[i]
			scans = level1_obsid[i]
			
			indexLeg = range(scans.count)
			
			metaMain = obs.meta
			
			if i==0:
				metaMultipleObs = metaMain.copy()
			else:
				for key in metaMain.keySet():
					if metaMultipleObs.containsKey(key)==True and metaMultipleObs[key].value!=metaMain[key].value:
						description = metaMain[key].description
						value = str(metaMultipleObs[key].value)+" & "+str(metaMain[key].value)
						try:
							unit = metaMain[key].unit
							metaMultipleObs[key] = StringParameter(value, description, unit)
						except:
							metaMultipleObs[key] = StringParameter(value, description)
			
			print "Retrieving good channels... ",
			chanMask = obs.calibration.phot.chanMask
			chanNum = obs.calibration.phot.chanNum
			chanRelGains = obs.calibration.phot.chanRelGain
			
			print "signal, mask, ra, dec...",
			signals = {}
			masks = {}
			ras = {}
			decs = {}
			FlagTurnOvers = []
			
			for i in indexLeg:
				oneLine = scans.getProduct(i)
				
				startNomTime = (oneLine.meta["startNominalScanLine"].value.microsecondsSince1958())/1E6
				endNomTime = (oneLine.meta["endNominalScanLine"].value.microsecondsSince1958())/1E6
				for j in range(oneLine.sampleTime.length()):
					if oneLine.sampleTime[j]<startNomTime or oneLine.sampleTime[j]>endNomTime:
						FlagTurnOvers.append(1)
					else:
						FlagTurnOvers.append(0)
				
				if i==0:
					goodChan = []
					for chan in oneLine.channelNames:
						conGoodChan = not chanMask.isDead(chan) and not chanMask.isNoisy(chan) and not chanMask.isSlow(chan) and chanNum.isBolometer(chan) and not chanNum.isDark(chan)
						if conGoodChan and chan.count(bandName)>0:
							goodChan.append(chan)
					goodChan.sort()
					nChan = len(goodChan)
					
					unitRa   = oneLine["ra"][goodChan[0]].unit
					unitDec  = oneLine["dec"][goodChan[0]].unit
					unitTime = oneLine["ra"]["sampleTime"].unit
					
					for chan in goodChan:
						signals[chan] = oneLine.getSignal(chan)
						masks[chan] = oneLine.getMask(chan)
						ras[chan] = oneLine.getRa(chan)
						decs[chan] = oneLine.getDec(chan)
					
					sampleTime = oneLine.sampleTime.copy()
				
				else:
					sampleTime.append(oneLine.sampleTime)
					for chan in goodChan:
						signals[chan].append(oneLine.getSignal(chan))
						masks[chan].append(oneLine.getMask(chan))
						ras[chan].append(oneLine.getRa(chan))
						decs[chan].append(oneLine.getDec(chan))
			
			del(oneLine)
			
			 # Tranform them to 2d arrays channel vs time
			FlagTurnOver = Short1d(len(sampleTime))
			for i in range(len(sampleTime)):
				FlagTurnOver[i] = FlagTurnOvers[i]
			pass
			sign = Double2d(nChan,len(sampleTime))
			mask = Short2d(nChan,len(sampleTime))
			for i in range(nChan):
				iSig = signals[goodChan[i]].to().double1d()
				iMask = masks[goodChan[i]]
				
				# TODO: see spire/ia/pipeline/util/AbstractMapperTask.java
				
				bad = iMask.where(SpireMask.MASTER | \
				SpireMask.GLITCH_FIRST_LEVEL_UNCORR | \
				SpireMask.GLITCH_SECOND_LEVEL_UNCORR | \
				SpireMask.GLITCH_FIRST_LEVEL | \
				SpireMask.GLITCH_SECOND_LEVEL | \
				SpireMask.TRUNCATED_UNCORR | \
				SpireMask.ISDEAD | \
				SpireMask.ISNOISY | \
				SpireMask.SERENDIPITY | \
				SpireMask.NO_RESP_DATA | \
				SpireMask.ISSLOW )
				
				# This is faulty on some mask... (maskVoltageOol and so on...)
				# So take the inverse (from 2.147. FixedMask )
				
				good = iMask.where(~SpireMask.MASTER.isSet(iMask) & \
				~SpireMask.GLITCH_FIRST_LEVEL_UNCORR.isSet(iMask) & \
				~SpireMask.GLITCH_SECOND_LEVEL_UNCORR.isSet(iMask) &  \
				~SpireMask.GLITCH_FIRST_LEVEL.isSet(iMask) & \
				~SpireMask.GLITCH_SECOND_LEVEL.isSet(iMask) & \
				~SpireMask.TRUNCATED_UNCORR.isSet(iMask) & \
				~SpireMask.ISDEAD.isSet(iMask) & \
				~SpireMask.ISNOISY.isSet(iMask) & \
				~SpireMask.SERENDIPITY.isSet(iMask) &   \
				~SpireMask.NO_RESP_DATA.isSet(iMask) &   \
				~SpireMask.ISSLOW.isSet(iMask) )
				
				if bad.length() > 0:
					iMask[bad] = 1
				if good.length() > 0:
					iMask[good] = 0
				bad = iSig.where(~IS_FINITE(iSig))
				if len(bad.toInt1d()) > 0:
					iMask[bad] = 1
				
				sign[i,:] = iSig
				mask[i,:] = iMask
				
				del(iSig,iMask)
			
			del(signals,masks)
			del(chanMask,chanNum)
			del(FlagTurnOvers)
			
			# Tranform them to 2d arrays channel vs time
			ra = Double2d(nChan,len(sampleTime))
			dec = Double2d(nChan,len(sampleTime))
			for i in range(nChan):
				ra[i,:] = ras[goodChan[i]]
				dec[i,:] = decs[goodChan[i]]
			del(ras,decs)
			
			print "channels list..."
			channels = TableDataset()
			channels["name"] = Column(String1d(goodChan))
			
			dataKept = Product(creator="HIPE", instrument="SPIRE", type="Product for SupremePhoto", description="Data for SupremePhoto - obsid "+str(obsid))
			dataKept["signal"] = ArrayDataset(sign)
			dataKept["lat"] = ArrayDataset(ra)
			dataKept["lon"] = ArrayDataset(dec)
			dataKept["mask"] = ArrayDataset(mask)
			dataKept["time"] = ArrayDataset(sampleTime)
			dataKept["channels"] = channels
			dataKept["turnover"] = ArrayDataset(FlagTurnOver)
			dataKept.meta["raNominal"] = obs.meta["raNominal"]
			dataKept.meta["decNominal"] = obs.meta["decNominal"]
			dataKept.meta["posAngle"] = obs.meta["posAngle"]
			dataKept.meta["object"] = obs.meta["object"]
			dataKept.meta["obsid"] = obs.meta["obsid"]
			
			dataObsList.refs.add(ProductRef(dataKept))
			
		#return dataObsList
		waveDic = {"PSW":250.0,"PMW":350.0,"PLW":500.0}
		metaMultipleObs["wavelength"] = DoubleParameter(waveDic[bandName])
		metaMultipleObs["band"] = StringParameter(bandName)
		if doApplyExtendedEmissionGains:
			metaMultipleObs["AdditionnalProcessing"] = StringParameter("Extended Emission Gains applied to Level1")
		
		dataObsList.meta = metaMultipleObs.copy()
		self.data = dataObsList

## Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()

print "Installing SupremePhotoGetDataFromObs... ",
toolRegistry.register(SupremePhotoGetDataFromObs(),[Category.SPIRE])

print "SupremePhotoMapMakingTask...",
from pluginSupremePhoto import SupremePhotoMapMakingTask
toolRegistry.register(SupremePhotoMapMakingTask(), [Category.SPIRE])

del(toolRegistry)

print "Done !"
