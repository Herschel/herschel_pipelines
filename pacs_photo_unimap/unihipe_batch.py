# -*- coding: utf-8 -*-
import os
import shutil
import sys
from string import *
from org.python.core import PyList
from javax.swing import *
from herschel.ia.numeric import *
from herschel.ia.io.fits import *
from herschel.ia.io.fits.FitsArchive import *
from herschel.ia.dataset import *
from herschel.ia.toolbox.util import *
from herschel.ia.gui.plot import *
from herschel.pacs.cal import *
from herschel.pacs.toolboxes.all import *
from java.util import Date
from java.lang import Runtime
from herschel.ia.task import *
from java.lang import Boolean
from herschel.ia.pal import *
from herschel.ia.obs import ObservationContext
from herschel.ia.pal.query import MetaQuery
from herschel.ia.pal.pool.lstore import LocalStoreFactory
from herschel.ia.pal.browser.v2.table.model import * 
from herschel.ia.pal.util import StorageResult
from herschel.ia.pal.pool.hsa import HsaReadPool
from herschel.ia.numeric.toolbox import *
from herschel.ia.numeric.toolbox.basic import MAX
from herschel.ia.toolbox.util import SimpleFitsReaderTask
from os import sys

from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter
from herschel.ia.pal.pool.lstore.util import TemporalPool
from herschel.spire.ia.pipeline.scripts.PARALLEL.PARALLEL_tasks import *
from herschel.spire.ia.pipeline.scripts.PARALLEL.PARALLEL_input import *
from herschel.spire.ia.scripts.tools.obsLoader import *

###########################SETTING INPUT

working_dir = '/data/glx-herschel/data1/herschel/scriptsPACS/Unimap/unimap_v2.39/level1/'

fieldObsids = {}
#fieldObsids.update( {'AFGL4029shift':[ 1342217393,1342217394]} )
#fieldObsids.update( {'Ced201-2':[1342196809,1342196810]} )
#fieldObsids.update( {'DC300-17-c2':[1342193044,1342193045]} )
#fieldObsids.update( {'DC300-17':[1342212300,1342211293]} )
#fieldObsids.update( {'HD37041':[1342191106,1342191107]} )
#fieldObsids.update( {'HH_intermediate':[1342226731,1342226732]} )
#fieldObsids.update( {'IC59-centre':[1342216405,1342216406]} )
#fieldObsids.update( {'IC63-int':[1342216407,1342216408]} )
#fieldObsids.update( {'IRAS16132-503':[1342216487,1342216488]} )
#fieldObsids.update( {'IVCG86_PacsPhoto':[1342210589,1342210590]} )
#fieldObsids.update( {'IVCG86':[1342197280,1342197281]} )
#fieldObsids.update( {'n2023':[1342204219,1342204220]} )
#fieldObsids.update( {'n7023_int':[1342187077,1342187078]} )
#fieldObsids.update( {'rcw79':[1342258816,1342258817]} )
#fieldObsids.update( {'rho_oph_h2':[1342204191,1342204192]} )
#fieldObsids.update( {'sh241MSX':[1342218723,1342218724]} )
#fieldObsids.update( {'spica_PacsPhoto':[1342213278,1342213279]} )
#fieldObsids.update( {'spica':[1342200114,1342200115]} )
#fieldObsids.update( {'ursamajors_s1':[1342194078,1342194079]} )
#fieldObsids.update( {'ursamajors_s2':[1342194076,1342194077]} )
#fieldObsids.update( {'UrsaMajor':[1342206692,1342206693]} )


#fieldObsids.update( {'HATLAS_NGP_NANB':[1342259329, 1342259328, 1342259325,1342259324,1342259323,1342259322,1342259321,1342259320]} )
#fieldObsids.update( {'S3':[1342204860,1342204861]} )
#fieldObsids.update( {'MACSJ1423':[1342188215,1342188216]} )

##fieldObsids.update( {'OrionA-C-1':[1342204098,1342204099]} )
#fieldObsids.update( {'OrionA-N-1':[1342218967,1342218968]} )
##fieldObsids.update( {'OrionA-S-1':[1342205076,1342205077]} )

#fieldObsids.update( {'OrionA-C-2':[1342218553, 1342218554]} )
#fieldObsids.update( {'OrionA-N-2':[1342206052, 1342206053]} )
#fieldObsids.update( {'OrionA-S-2':[1342228910, 1342228911]} )
##fieldObsids.update( {'OrionA-SS-2':[1342228908, 1342228909]} )

##fieldObsids.update( {'OrionB-N-1':[1342215982,1342215983]} )
##fieldObsids.update( {'OrionB-NN-1':[1342205074,1342205075]} )
#fieldObsids.update( {'OrionB-S-1':[1342215984,1342215985]} )

#fieldObsids.update( {'OrionB-C-2':[1342218555, 1342218556]} )
#fieldObsids.update( {'OrionB-N-2':[1342206054, 1342206055]} )
#fieldObsids.update( {'OrionB-NN-2':[1342206078, 1342206079]} )
#fieldObsids.update( {'OrionB-S-2':[1342206081, 1342206080]} )

#fieldObsids.update( {'Spider-1':[1342231359,1342231360]} )
#fieldObsids.update( {'draco-1-1':[1342224959,1342224960]} )

fieldObsids.update( {'L1521':[1342202254,1342202090]} )

instrument = 'PACS'
database = 'HSA'

#for i in range(len(field)): print field[i], obsids_list[i]
#sys.exit("Error message")

###########################################

exists_path =  os.path.exists(working_dir)
if exists_path == 0: os.mkdir(working_dir)

for fieldName, obsids in fieldObsids.iteritems():
    
    n_obsids = len(obsids)
    obsids = String1d(obsids)
    
    results_list = PyList()
    dim_samples = Int1d()
    
    #saving meta-data for the final header
    key = ['DATE-OBS','SPG','INSTRUME','BUNIT','WAVELNTH']
    
    comment = ['average JD','standard product generator','instrument','unit','wavelength in micron']
    
    julianDate = Double1d()
    spg_vect = String1d()
    wave_vect = String1d()
    
    for i in range(n_obsids):
        archive = HsaReadPool()
        store = ProductStorage()
        store.register(archive)
        print 'Performing meta query at the HSA for obsID:'
        print obsids[i]
        q = MetaQuery(ObservationContext,"p","p.meta['obsid'].value=='"+obsids[i]+"' and p.meta['instrument'].value=='"+instrument+"'")
        results_query = store.select(q)
        try:
            obs = results_query[0].product
        except herschel.share.io.HcssHttpGeneralSecurityException:
            JOptionPane.showMessageDialog(None, "You are not logged at the HSA or/and you are trying for accessing to proprietary data. \n Please make authentication procedure (also by using the tip reported in the README file) and start the unihipe tool again")
            sys.exit()
        startDate = obs.getMeta()['startDate'].time
        spg = obs.getMeta()['creator'].string[4:len(obs.getMeta()['creator'].string)]
        
        results = PyList()
        for w in ('blue','red'):
            try:
                frames = PacsContext(obs.level1).averaged.getCamera(w).product.selectAll()
            except:
                print("pas :",w)
                continue
            if w == 'blue':
                blue = frames.getMeta()['blue'].string
                if blue == 'blue1': wave = '70'
                else: wave = '100'
                wave_vect.append(wave)
            calTree = getCalTree()
            
            if w == 'blue': bol, pix_size, n_row = 2048, 3.2, 32
            else: bol, pix_size, n_row = 512, 6.4, 16 # 12 replaced by 16
            
            scanning_flag = Int1d(frames.dimensions[2])
            ######Select the telescope scanning    
            start_ind = frames['BlockTable']['StartIdx'].data
            end_ind = frames['BlockTable']['EndIdx'].data
            id = frames['BlockTable']['Id'].data
            start_scan = start_ind.where(id == "PHOT_SCANMODE").toInt1d()
            end_scan = end_ind.where(id == "PHOT_SCANMODE").toInt1d()
            is_scan = Range(start_ind[start_scan[0]], end_ind[end_scan[0]])
            scanning_flag[is_scan] = 1
            index = 2
            for i in range(len(start_scan)-1):
                    is_scan = Range(start_ind[start_scan[i+1]], end_ind[end_scan[i+1]])
                    scanning_flag[is_scan] = index
                    index = index + 1
        
        if (frames.containsKey("Ra") == False): frames = photAssignRaDec(frames, calTree=calTree)
        ####################################### 
        px = (pix_size / 3600.)**2
        sr = 57.2957795131**2
        f = (sr/px) * 1e-6
        signal = (frames['Signal'].data * f)
        ra = frames['Ra'].data
        dec = frames['Dec'].data
        sat = frames.getMask("SATURATION")
        bad = frames.getMask("BADPIXELS")[:,:,0]
        
        s = Float2d() #signal
        r = Float2d()
        d = Float2d()
        f = Int2d()
        gb = Int1d()
        index = 1
        for i_column in range(signal.dimensions[1]):
            print 'PACS frames: column', i_column
            for i_row in range(signal.dimensions[0]):
                if bad[i_row,i_column] == False:
                    gb.append(index)
                    pixel_I = Float1d(signal[i_row,i_column,:])
                    s.append(pixel_I,1)
                    pixel_Ra = Float1d(ra[i_row,i_column,:])
                    r.append(pixel_Ra,1)
                    pixel_Dec = Float1d(dec[i_row,i_column,:])
                    d.append(pixel_Dec,1)
                    ind = sat[i_row,i_column,:].where(sat[i_row,i_column,:] == True)
                    pixel_Flag = Int1d(signal.dimensions[2])
                    pixel_Flag[ind] = 1
                    f.append(pixel_Flag,1)
                else:
                     print 'PACS frames: pixel', i_column, i_row, 'is a bad detector!!'
                index = index + 1
        
        sample = Int1d(len(gb))
        sample[:] = signal.dimensions[2]
        results.append([s,r,d,f,gb,Int1d().append(bol),Int1d().append(scanning_flag), sample])
        
        del s,r,d,f,gb
        System.gc()
        
        #computing JD    
        date = str(startDate)[0:10]
        time = str(startDate)[11:19]   
        date=date.split("-")
        dd=int(date[2])
        mm=int(date[1])
        yyyy=int(date[0])
        time=time.split(":")
        hh=float(time[0])
        min=float(time[1]) 
        sec=float(time[2])
        UT=hh+min/60+sec/3600
        if (100*yyyy+mm-190002.5)>0: sig = 1
        else: sig = -1
        JD = 367*yyyy - int(7*(yyyy+int((mm+9)/12))/4) + int(275*mm/9) + dd + 1721013.5 + UT/24 - 0.5*sig +0.5
        julianDate.append(JD)
        
        spg_vect.append(spg)
        
        results_list.append(results)
        del results
        System.gc()
    
    JD_avg = str(MEAN(julianDate))
    print spg_vect
    ind = spg_vect.where(spg_vect == spg_vect[0])
    if (len(ind.toInt1d()) != n_obsids):
        print 'WARNING!! you are combinining observations with different SPG! EXIT'
        sys.exit()
    #meta_value = String1d([JD_avg,spg,instrument,'MJy/sr'])
    
    print wave_vect
    ind = wave_vect.where(wave_vect == wave_vect[0])
    if (len(ind.toInt1d()) != n_obsids):
        print 'WARNING!! you are mixing blue and green observations!'
    #   sys.exit() !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #n_bolo = results[0].dimensions[1]
    
    for t in (0,1):
        if t ==1:
            camera = 'red'
            wave = '160'
        ii = 0
        for i in range(n_obsids):
            if t ==0:
                if wave_vect[ii] == '70': camera = 'blue'
                if wave_vect[ii] == '100': camera = 'green'
                ii = ii+1
            print i, t
            s = results_list[i][t][0].copy()
            r = results_list[i][t][1].copy()
            d = results_list[i][t][2].copy()
            f = results_list[i][t][3].copy()
            gb = results_list[i][t][4].copy()
            sc = results_list[i][t][6].copy()
            sp = results_list[i][t][7].copy()
            print 'Saving PACS '+camera+' outputs'
            array = [s,r,d,f,gb,results_list[i][0][5],sc,sp]
            description = ['Signal','Ra','Dec','Flag','Good bolometers','Number bolometers'\
            ,'Scanning flag ', 'Number of samples']
            c=CompositeDataset()
            for j in range(len(array)):
                dataset = ArrayDataset(description=description[j])
                dataset.data = array[j]
                c[description[j]] = dataset
            p = Product(description='Unimap input')
            p[obsids[i]] = c
            fits = FitsArchive(reader=STANDARD_READER)
            fits.save(working_dir+'unimap_obsid_'+fieldName+'_'+camera+'_'+obsids[i]+'.fits',p)
            del array,p
            System.gc()
            m = TableDataset()
            meta_value = String1d([JD_avg,spg,instrument,'MJy/sr',wave])
        #meta_value.append(wave)
            m['meta'] = Column(meta_value)
    meta_name = working_dir+'unimap_obsid_'+fieldName+'_'+camera+'.dat'
    asciiTableWriter(m,meta_name,writeHeader=False)
