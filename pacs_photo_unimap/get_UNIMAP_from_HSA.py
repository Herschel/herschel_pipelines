#!/usr/bin/env python
# -*-coding:utf-8 -*
# UNIMAP is part of pacs photo
# UNIMAP info (from HIPE docs ; 3.2.4. Extended sources: Unimap)
# Unimap[3] is a Generalized Least Squares (GLS) mapmaker developed in 
# MATLAB and released by the DIET department of the University of Rome 
# 'La Sapienza’ (link). Please, look at the Unimap Documentation web page 
# for more information, and in particular for a detailed description of 
# this mapmaker, look at the Unimap User's Manual. Unimap performs an 
# accurate pre-processing to clean the dataset of systematic effects 
# (offset, drift, glitches), and it uses the GLS algorithm to remove the 
# correlated 1/f noise while preserving the sky signal over large spatial 
# scales. The GLS estimator provided by Unimap can introduce distortions 
# at the positions of bright sky emissions, especially if point like. The 
# distortions, that generally appear as cross-like artifacts, are due to 
# the approximations of the signal model and to a not perfect 
# compensation of disturbances at the pre-processing stage. A high-pass 
# filtering approach is implemented within Unimap to remove these 
# distortions within a specified spatial scale, by generating the 
# Post-GLS map (PGLS). The application of the high-pass filter method has 
# the drawback to inject back correlated noise over the same spatial 
# scale used for the removal of the distortions. To minimize this noise 
# injection, the filtering can be applied only over the bright emissions 
# that are selected by using a threshold approach, having at the latest 
# stage the Weighted-GLS map (WGLS). The values of parameters that 
# contribute for generating the GLS, PGLS and WGLS maps depend on the 
# characteristics of the sky emission and on the level of accuracy to be 
# achieved by the different estimators. The best choice for the 
# parameters values is automatically set by Unimap (from the track 6 and 
# beyond) by performing a statistical analysis of the sky emission. These 
# parameters are described below and they can be always fine tuned by the 
# users according their purposes.

# Launch the script via : 
#> hipe get_UNIMAP_from_HSA.py

# Import all needed classes
from herschel.spire.all import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter


# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *



# Input definition
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *
from herschel.spire.ia.scripts.tools.obsLoader import *
from herschel.ia.pal.util import StorageResult
from os import path, makedirs
import glob
import string
import pdb
#hcss.ia.pal.version = none

# get export2sanepic
from export import * 
from IAS_tools import *
import time

start_time = time.time()

def fixMetada(unimap, obs, obsid_list):
	"""
	unimap : object containing a unimap object (One color only)
	obs : The complete obsid object (used to compare metadata with 'unimap' in case some are missing
	obsid_list : the list of obsids refering to the unimap map.
	
	Return: unimap object, with metadata fixed.
	"""
	fix_HSAmeta =['instrument', 'modelName', 'observer', 'object','obsMode', 'cusMode','pointingMode'\
		'instMode','equinox','raDeSys','raNominal','decNominal','telescope', \
		'proposal','odNumber', 'startDate', 'endDate','aorLabel','posAngle','radialVelocity','blueband']
	
	obsids = TableDataset()
	array_obsIds = Long1d(obsid_list)
	
	# We define a standardized meta keyword for obsids list (used by hesiod)
	# In newer version of HIPE, this info exist, but we must keep our keywork for retrocompatibility reasons
	obsids["obsids"] = Column(array_obsIds,description="obsids")
	unimap["obsids"] = obsids
	
	for meta in fix_HSAmeta:
		if unimap.meta.containsKey(meta):
			if unimap.meta[meta].value == "Unknown" or meta=="startDate" or meta=="endDate":
				unimap.meta[meta] = obs.meta[meta]
		else:
			# If meta not present in unimap, but present in the original complete obs, we extract it. 
			if obs.meta.containsKey(meta):
				unimap.meta[meta] = obs.meta[meta]
	# PACS specific
	# TODO il faut modifier ça puisque camera ne sera plus défini dans le nom. mais on a l'info de la longueur d'onde. 
	
	# We must define the reference wavelength in meter. This info exist in micrometer in the metadata already
	lambda_micrometer = unimap.meta['wavelength'].value
	unimap.meta['restwav'] = DoubleParameter(lambda_micrometer * 1e-6, description="rest wavelength in vacuo [m]", unit=Length.METERS)

	return unimap

# Miss 1342204195 1342216417
obsids_sag4_pacs_photo = {
'n7023_int':[1342187077, 1342187078], 
'HD37041':[1342191106, 1342191107], 
'DC300-17-c2':[1342193044, 1342193045], 
'ursamajors_s2':[1342194076, 1342194077], 
'ursamajors_s1':[1342194078, 1342194079], 
'Ced201-2':[1342196809, 1342196810], 
'rho_oph_h2':[1342204191, 1342204192], 
'n2023':[1342204219, 1342204220], 
'IVCG86_PacsPhoto':[1342210589, 1342210590], 
'spica_PacsPhoto':[1342213278, 1342213279], 
'IC59-centre':[1342216405, 1342216406], 
'IC63-int':[1342216407, 1342216408], 
'IRAS16132-503':[1342216487, 1342216488], 
'AFGL4029shift':[1342217393, 1342217394], 
'sh241MSX':[1342218723, 1342218724], 
'HH_intermediate':[1342226731, 1342226732], 
'rcw79':[1342258816, 1342258817], 
'IVCG86':[1342197280, 1342197281], 
'spica':[1342200114, 1342200115], 
'UrsaMajor':[1342206692, 1342206693],
'DC300-17':[1342212300, 1342211293]}

obsids_sag3_pacs_photo = {
'S3':[1342204860,1342204861], 
"OrionA-C-1":[1342204098,1342204099], 
"OrionA-N-1":[1342218967,1342218968], 
"OrionA-S-1":[1342205076,1342205077], 
"OrionA-C-1__OrionA-S-1":[1342204098,1342204099,1342205076,1342205077], 
"OrionA-C-2":[1342218553, 1342218554], 
"OrionA-N-2":[1342206052, 1342206053], 
"OrionA-S-2":[1342228910, 1342228911], 
"OrionA-SS-2":[1342228908, 1342228909], 
"OrionA-C-2__OrionA-S-2__OrionA-SS-2":[1342218553, 1342218554,1342228910, 1342228911,1342228908, 1342228909], 
"OrionB-N-1":[1342215982,1342215983], 
"OrionB-NN-1":[1342205074,1342205075], 
"OrionB-S-1":[1342215984,1342215985], 
"OrionB-N-1__OrionB-NN-1":[1342215982,1342215983,1342205074,1342205075], 
"OrionB-C-2":[1342218555, 1342218556], 
"OrionB-N-2":[1342206054, 1342206055], 
"OrionB-NN-2":[1342206078, 1342206079], 
"OrionB-S-2":[1342206080, 1342206081], 
"OrionB-C-2__OrionB-N-2":[1342218555, 1342218556,1342206054, 1342206055]}


obsids_mmiville_pacs_photo = {
'Spider-1':[1342231359,1342231360],
'draco-1-1':[1342224959,1342224960]} 

obsids_polaris_pacs_photo = {
'polaris':[1342185573,1342185574],
'polaris_spirepacs':[1342186233,1342186234,1342198246,1342198246]} 
# We merge SAG-3 and SAG-4 dictionnaries
#obsids = obsids_sag4_pacs_photo.copy()
#obsids.update(obsids_sag3_pacs_photo)

obsids_Abell773_pacs_photo = {
'Abell773':[1342209362,1342209363]}
# Rewritting obsids dict for test
#~ obsids = {'n7023_int':[1342187077, 1342187078]}

obsids = obsids_Abell773_pacs_photo.copy()
outputDir = '/data/glx-herschel/data1/herschel/UNIMAP_Fits/MAPS_PACS'

#~ objects=[]
product_names = ['HPPUNIMAPB', 'HPPUNIMAPR']
wave_refs = {"blue":70.0, "green":100.0, "red":160.0} # Correspondance between color name and corresponding wavelength in the HIPE perspective
delta_l = 0.5 # tolerance in micro meter when searching for a wavelength match
for (object_name, obsid_list) in obsids.iteritems():
	
	# UNIMAP is redundant for each obsid in the list. No need to do it more than once for each list. 
	obsid = obsid_list[0]
	obs = getObservation(obsid, useHsa=True, instrument="PACS")
	
	tmp = string.join(obs.meta['object'].value.split(),'')
	#~ objects.append(tmp)
	print(tmp)
	
	
	for product_name in product_names:
		unimap = obs.level2_5.refs[product_name].product
		obs_name_identifier = obs.level2_5.refs[product_name].product.meta
		wavelength = unimap.meta['wavelength'].value
		
		color = None
		for (c, wave_ref) in wave_refs.iteritems():
			if ((wavelength > (wave_ref - delta_l)) and (wavelength < (wave_ref + delta_l))):
				color = c
				break
		
		
		filename = "_".join([object_name, color, "wgls", product_name])
		filename += ".fits"
		
		# Fixing metadata, and keeping retro-compatibility with previous version (in HESIOD)
		unimap = fixMetada(unimap=unimap, obs=obs, obsid_list=obsid_list)
		
		simpleFitsWriter(product=unimap, file=os.path.join(outputDir, filename))
	
stop_time = time.time()
m, s = divmod(stop_time - start_time, 60)
h, m = divmod(m, 60)
print "Duration: %d:%02d:%02d" % (h, m, s)


