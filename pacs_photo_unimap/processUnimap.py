#!/usr/bin/env python
# -*-coding:utf-8 -*
import os, sys

workingDir = "/data/glx-herschel/data1/herschel/scriptsPACS/Unimap/unimap_v2.39/"
level1Dir = workingDir + "level1/"
mcr2014b = "/usr/local/mcr2014b/v84/"

def processUnimap_jobs(jobs):
    
    for job in jobs:
        args = []
        args.append("python "+workingDir+"processUnimap.py")
        args.append(job)
        command = " ".join(args)
        print command
        os.system(command)

if __name__ == '__main__':

    args = sys.argv[1:]
    
    override = False
    if "override" in args:
        args.remove("override")
        override = True
    objects, band, start_image = args [:-2], args [-2], args [-1]
    print objects, band, start_image

    job = "__".join(sorted(objects))
    workDir = workingDir+job+"/"+band+"/"

    if os.path.isfile(workDir+"img_wgls.fits") and not override:
        print "Processing already done !"

    else:
        print "creating working directory..."
        command = "mkdir -p "+workDir
        print command
        os.system(command)
        
        print "rsync files..."
        for object in objects: 
            files = level1Dir+"unimap_obsid_"+object+"_"+band+"*fits"
            notFound = os.system("ls "+files)
            if notFound != 0:
                print "No level1 found !"
                sys.exit(1)
            command = "rsync -Lv "+files+" "+workDir
            print command
            os.system(command)
        
        pixelSize = {"red":"3","green":"2","blue":"2"}
        
        template = workingDir+"unimap_par_template.txt"
        tmp = open(template, 'r')
        templateString = tmp.read()
        tmp.close()
        parameterFile = template.replace("_template","")
        
        fichier = open(parameterFile, "w")
        templateStringJob = templateString.replace("LEVEL1DIRECTORY",workDir)
        templateStringJob = templateStringJob.replace("PIXELSIZE",pixelSize[band])
        templateStringJob = templateStringJob.replace("STARTIMAGE",start_image)
        
        fichier.write(templateStringJob)
        fichier.close()
        
        #command = "./"+workingDir+"run_unimap.sh "+mcr2014b+" > "+workingDir+"log/log_"+job+"_"+band+".txt"
        command = "./run_unimap.sh "+mcr2014b+" > "+workingDir+"log/log_"+job+"_"+band+".txt"
        print command
        os.system(command)
