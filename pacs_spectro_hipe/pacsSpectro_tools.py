import os, glob, string
from java.awt.geom import Point2D
#from java.awt import Color
from herschel.ia.gui.plot import PlotLegend

dirTools = os.environ['HANDSONTOOLS']

def getInfoObs(obs):
	try:
		lineID = obs.meta["Line 1"].getString()
	except:
		lineID = obs.meta["Line1"].getString()
	lineID = lineID.split(" micron")
	longOnde = lineID[0]
	lineInfo = lineID[1].split("ID: ")[1]
	lineInfo = lineInfo.replace(" ","")
	
	if lineInfo.__len__() > 6: lineInfo = lineInfo[0:lineInfo.__len__()-7]
	if float(longOnde) < 102: camera = "blue"
	else: camera = "red"
	
	return [camera,longOnde+"_"+lineInfo]

def getInfoObs_old(obs,line):
	lineID = obs.meta["Line "+str(line)].getString()
	lineID = lineID.split(" micron")
	longOnde = lineID[0]
	lineInfo = lineID[1].split("ID: ")[1]
	lineInfo = lineInfo.replace(" ","")
	
	if lineInfo.__len__() > 6: lineInfo = lineInfo[0:lineInfo.__len__()-7]
	if float(longOnde) < 102: camera = "blue"
	else: camera = "red"
	
	return [camera,longOnde+"_"+lineInfo]

def correctFaddersDippers_IAS(slicedFrames_orig,faders=True,dippers=True,saveToFits=""):
	
	slicedFrames = slicedFrames_orig.copy()
	
	if saveToFits!="": fitsName = saveToFits
	else: 
		obsidMeta = obs.meta["obsid"].getValue()
		fitsName = str(obsidMeta).replace("L","")+"_Frames.fits"
	
	os.system("rm -rf "+fitsName)
	
	nbFrames = slicedFrames.getNumberOfFrames()
	
	signalAll = Double4d(nbFrames,16,25,1000)
	waveAll = Double4d(nbFrames,16,25,1000)
	glitchAll = Bool4d(nbFrames,16,25,1000)
	timeAll = Long2d(nbFrames,1000)
	
	for raster in range(nbFrames):
		signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
		waveFrame = slicedFrames.refs[raster].product["Wave"].getData()
		glitchFrame = slicedFrames.refs[raster].product["Mask"]["GLITCH"].getData()
		timeFrame = slicedFrames.refs[raster].product["Status"].getColumn("FINETIME").getData()
		timeAll[raster,:] = timeFrame
		for scan in range(1,17):
			for spax in range(25):
				signalAll[raster,scan-1,spax,:] = signalFrame[scan,spax,:]
				waveAll[raster,scan-1,spax,:] = waveFrame[scan,spax,:]
				glitchAll[raster,scan-1,spax,:] = glitchFrame[scan,spax,:]
	
	productToWrite = Product()
	productToWrite.setMeta(slicedFrames.getMeta())
	productToWrite['Signal'] = ArrayDataset(signalAll)
	productToWrite['Wave'] = ArrayDataset(waveAll)
	productToWrite['Glitch'] = ArrayDataset(glitchAll)
	productToWrite['Time'] = ArrayDataset(timeAll)
	
	simpleFitsWriter(productToWrite,fitsName)
	
	del signalAll, waveAll, glitchAll,timeAll, productToWrite
	
	if faders and not dippers:
		os.system("python "+dirTools+"fadersDippers_multi.py "+fitsName+" faders")
		extToOpen = "CORRFAD"
	if not faders and dippers:
		os.system("python "+dirTools+"fadersDippers_multi.py "+fitsName+" dippers")
		extToOpen = "CORRDIP"
	if faders and dippers:
		os.system("python "+dirTools+"fadersDippers_multi.py "+fitsName+" all")
		extToOpen = "CORRALL"
	
	print "load data"
	if dippers or faders:
		fitsFrame = fitsReader(fitsName)
		dataFrame = fitsFrame[extToOpen].getData()
		
		for raster in range(nbFrames):
			signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
			for scan in range(1,17):
				for spax in range(25):
					signalFrame[scan,spax,:] = dataFrame[raster,scan-1,spax,:] + 0
			slicedFrames.refs[raster].product["Signal"].setData(signalFrame)
		
		if faders: faddTab = fitsFrame["FADDTAB"].getData()
	
	
	if saveToFits=="": os.system("rm -rf "+fitsName)
	
	if faders:
		fadersTab = ArrayDataset(faddTab)
		slicedFrames.set("fadersTab",fadersTab)
		
	return slicedFrames

def correctFaddersDippers_PACSman(slicedFrames_orig,pattern,faders=True,dippers=True,readOnly=False):
	
	slicedFrames = slicedFrames_orig.copy()
	nbFrames = slicedFrames.getNumberOfFrames()
	if not readOnly: os.system("python "+dirTools+"correctFadersDippers_PACSman.py "+pattern)#+" "+str(dippers)+" "+str(faders))
	for raster in range(nbFrames):
		filePattern = pattern+str(raster)+"_CorrPACSmanHIPE.fits"
		print filePattern
		fitsFrame = fitsReader(filePattern)
		dataFrame = fitsFrame["SIGNAL"].getData()
		signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
		for scan in range(1,17):
			for spax in range(25):
				signalFrame[scan,spax,:] = dataFrame[scan,spax,:] + 0
		slicedFrames.refs[raster].product["Signal"].setData(signalFrame)
	return slicedFrames

def saveFrameToFits(slicedFrames,fitsName):
	
	os.system("rm -rf "+fitsName)
	
	nbFrames = slicedFrames.getNumberOfFrames()
	
	signalAll = Double4d(nbFrames,16,25,1000)
	waveAll = Double4d(nbFrames,16,25,1000)
	#raAll = Double4d(nbFrames,16,25,1000)
	#decAll = Double4d(nbFrames,16,25,1000)
	glitchAll = Bool4d(nbFrames,16,25,1000)
	timeAll = Long2d(nbFrames,1000)
	
	for raster in range(nbFrames):
		signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
		waveFrame = slicedFrames.refs[raster].product["Wave"].getData()
	#	raFrame = slicedFrames.refs[raster].product["Ra"].getData()
	#	decFrame = slicedFrames.refs[raster].product["Dec"].getData()
		glitchFrame = slicedFrames.refs[raster].product["Mask"]["GLITCH"].getData()
		timeFrame = slicedFrames.refs[raster].product["Status"].getColumn("FINETIME").getData()
		timeAll[raster,:] = timeFrame
		for scan in range(1,17):
			for spax in range(25):
				signalAll[raster,scan-1,spax,:] = signalFrame[scan,spax,:]
				waveAll[raster,scan-1,spax,:] = waveFrame[scan,spax,:]
	#			raAll[raster,scan-1,spax,:] = raFrame[scan,spax,:]
	#			decAll[raster,scan-1,spax,:] = decFrame[scan,spax,:]
				glitchAll[raster,scan-1,spax,:] = glitchFrame[scan,spax,:]
	
	productToWrite = Product()
	productToWrite.setMeta(slicedFrames.getMeta())
	productToWrite['Signal'] = ArrayDataset(signalAll)
	productToWrite['Wave'] = ArrayDataset(waveAll)
	#productToWrite['Ra'] = ArrayDataset(raAll)
	#productToWrite['Dec'] = ArrayDataset(decAll)
	productToWrite['Glitch'] = ArrayDataset(glitchAll)
	productToWrite['Time'] = ArrayDataset(timeAll)
	
	simpleFitsWriter(productToWrite,fitsName)
	
	del signalAll, waveAll, glitchAll,timeAll, productToWrite

def saveCubeToFits(slicedCube,fitsName):
	
	os.system("rm -rf "+fitsName)
	
	nbRaster = slicedCube.getNumberOfFrames()
	
	signalAll = Double4d(nbRaster,5,5,16000)
	waveAll = Double4d(nbRaster,5,5,16000)
	#raAll = Double4d(nbRaster,5,5,16000)
	#decAll = Double4d(nbRaster,5,5,16000)
	glitchAll = Bool4d(nbRaster,5,5,16000)
	timeAll = Long2d(nbRaster,16000)
	
	for raster in range(nbRaster):
		signalFrame = slicedCube.refs[raster].product["flux"].getData()
		waveFrame = slicedCube.refs[raster].product["wave"].getData()
		#raFrame = slicedCube.refs[raster].product["Ra"].getData()
		#decFrame = slicedCube.refs[raster].product["Dec"].getData()
		glitchFrame = slicedCube.refs[raster].product["Mask"]["GLITCH"].getData()
		timeFrame = slicedCube.refs[raster].product["Status"].getColumn("FINETIME").getData()
		timeAll[raster,:] = timeFrame
		for spaxX in range(5):
			for spaxY in range(5):
				signalAll[raster,spaxX,spaxY,:] = signalFrame[:,spaxX,spaxY]
				waveAll[raster,spaxX,spaxY,:] = waveFrame[:,spaxX,spaxY]
	#			raAll[raster,spaxX,spaxY,:] = raFrame[:,spaxX,spaxY]
	#			decAll[raster,spaxX,spaxY,:] = decFrame[:,spaxX,spaxY]
				glitchAll[raster,spaxX,spaxY,:] = glitchFrame[:,spaxX,spaxY]
	
	productToWrite = Product()
	productToWrite.setMeta(slicedCube.getMeta())
	productToWrite['Signal'] = ArrayDataset(signalAll)
	productToWrite['Wave'] = ArrayDataset(waveAll)
	#productToWrite['Ra'] = ArrayDataset(raAll)
	#productToWrite['Dec'] = ArrayDataset(decAll)
	productToWrite['Glitch'] = ArrayDataset(glitchAll)
	productToWrite['Time'] = ArrayDataset(timeAll)
	
	simpleFitsWriter(productToWrite,fitsName)
	
	del signalAll, waveAll, glitchAll,timeAll, productToWrite

def saveFitsforPACSman(product,typeProduct,motifFile,option=""):
	
	productPACSman = product.copy()
	nbFrames = productPACSman.getNumberOfFrames()
	
	if typeProduct=="slicedFrame":
		for i in range(nbFrames):
			try:
				productFrame = productPACSman.refs[i].product
				if option=="": fitsFile = motifFile+str(i)+".fits"
				if option=="PACSman": fitsFile = motifFile+str(i+1)+".fits"
				simpleFitsWriter(productFrame,fitsFile)
			except:
				print "There is no slicedFrame ",i," to save!!"
	
	if typeProduct=="slicedCube" and option=="":
		for i in range(nbFrames):
			try:
				productCube = productPACSman.refs[i].product
				fitsFile = motifFile+str(i)+".fits"
				simpleFitsWriter(productCube,fitsFile)
			except:
				print "There is no slicedCube ",i," to save!!"
	
	if typeProduct=="slicedCube" and option=="PACSman":
		i_file = 0
		for i in range(nbFrames):
			try:
				productCube = productPACSman.refs[i].product
				if not productCube.meta["isOffPosition"].value:
					fitsFile = motifFile+str(i_file+1)+".fits"
					simpleFitsWriter(productCube,fitsFile)
					i_file += 1
				else:
					print "slicedCube ",i," is an Off Position!!"
			except:
				print "There is no slicedCube ",i," to save!!"
	
	
	del productPACSman


def plotTimelineFadders(slicedFrames, slicedFrames_c):
	
	nbFrames = slicedFrames.getNumberOfFrames()
	faddersTable = slicedFrames_c["fadersTab"].getData()
	print faddersTable.where(faddersTable!=0)
	listPixels = faddersTable.where(faddersTable>0).toInt1d()
	
	signalAll = Double2d(len(listPixels),nbFrames*1000)
	glitchAll = Bool2d(len(listPixels),nbFrames*1000)
	timeAll = Double1d(nbFrames*1000)
	
	signalAll_c = Double2d(len(listPixels),nbFrames*1000)
	
	for raster in range(nbFrames):
		signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
		signalFrame_c = slicedFrames_c.refs[raster].product["Signal"].getData()
#		waveFrame = slicedFrames.refs[raster].product["Wave"].getData()
		glitchFrame = slicedFrames.refs[raster].product["Mask"]["GLITCH"].getData()
		timeFrame = slicedFrames.refs[raster].product["Status"].getColumn("FINETIME").getData()
		timeAll[raster*1000:(raster+1)*1000] = timeFrame
		for pixels in range(len(listPixels)):
			scan, spax = listPixels[pixels]/25, listPixels[pixels]%25
			signalAll[pixels,raster*1000:(raster+1)*1000] = signalFrame[scan,spax,:]
			signalAll_c[pixels,raster*1000:(raster+1)*1000] = signalFrame_c[scan,spax,:]
#			waveAll[pixels,raster*1000:(raster+1)*1000] = waveFrame[scan,spax,:]
			glitchAll[pixels,raster*1000:(raster+1)*1000] = glitchFrame[scan,spax,:]
			
	
	timeAll[:] = (timeAll[:]-timeAll[0])*0.000001
	minS, maxS = 0., 0.
	for pixels in range(len(listPixels)):
		idxNoG = glitchAll[pixels,:].where(glitchAll[pixels,:] == 0)
		minS = minS + (MIN(signalAll[pixels,idxNoG]))/len(listPixels)
		maxS = maxS + (MAX(signalAll[pixels,idxNoG]))/len(listPixels)
	
	for pixels in range(len(listPixels)):
		
		scan, spax = listPixels[pixels]/25, listPixels[pixels]%25
		
		layer_c = LayerXY(timeAll[:],signalAll_c[pixels,:])
		layer_c.style=Style(line=0, color=Color.RED, symbolShape=SymbolShape.FCIRCLE, symbolSize=1.5)
		layer_c.xaxis=Axis(type=Axis.LINEAR, range=[timeAll[0]-100,timeAll[-1]+100], titleText="Time(s) since beginning of observation")
		layer_c.yaxis=Axis(type=Axis.LINEAR, range=[minS-50,maxS+150], titleText="Signal (Jy/m2)")
		layer_c.xaxis.title.fontSize=10
		layer_c.yaxis.title.fontSize=10
		layer_c.name="Corrected Signal"
		
		layer = LayerXY(timeAll[:],signalAll[pixels,:])
		layer.style=Style(line=0, color=Color.BLACK, symbolShape=SymbolShape.FCIRCLE, symbolSize=1.5)
		layer.name="Original Signal"
		
		plot = PlotXY(1200,1000)
#		plot.batch = 1
		plot.titleText="Transients Correction - Faders"
		plot.subtitleText="Spectral Pixel "+str(scan)+" - Module "+str(spax)+" (Spatial pixel ("+str(spax/5)+","+str(spax%5)+"))\n All rasters position (On & Off)"
		plot.addLayer(layer_c)
		plot.addLayer(layer)
		plot.legend.visible=1
		plot.legend.columns=2
#		plot.batch = 0

def plotOnOff(on, off, slice, spaxX=2, spaxY=2):
	
	on_signal = on.refs[slice].product["image"].getData()[10:-10,spaxX,spaxY]
	off_signal = off.refs[0].product["image"].getData()[10:-10,spaxX,spaxY]
	wave = on.refs[slice].product["waveGrid"].getData()[10:-10]
	
	min, max = MIN(on_signal), MAX(on_signal)
	
	layer_c = LayerXY(wave,on_signal)
	layer_c.style=Style(line=0, color=Color.RED, symbolShape=SymbolShape.FCIRCLE, symbolSize=1.5)
	layer_c.xaxis=Axis(type=Axis.LINEAR, range=[wave[0]-0.2,wave[-1]+0.2], titleText="Wavelength")
	layer_c.yaxis=Axis(type=Axis.LINEAR, range=[min-10,max+10], titleText="Signal (Jy/m2)")
	layer_c.xaxis.title.fontSize=10
	layer_c.yaxis.title.fontSize=10
	layer_c.name="On"
	
	layer = LayerXY(wave,off_signal)
	layer.style=Style(line=0, color=Color.BLACK, symbolShape=SymbolShape.FCIRCLE, symbolSize=1.5)
	layer.name="Off"
	
	plot = PlotXY(1200,1000)
	plot.titleText="On/Off Position"
	plot.subtitleText="Spatial pixel ("+str(spaxX)+","+str(spaxY)+")"
	plot.addLayer(layer_c)
	plot.addLayer(layer)
	plot.legend.visible=1
	plot.legend.columns=2

def plotDippers(slicedFrames, slicedFrames_c, slice,module=12,spectralPix=8):
	
	signalAll = Double1d(1000)
	signalAll_c = Double1d(1000)
	
	signalFrame = slicedFrames.refs[slice].product["Signal"].getData()
	signalFrame_c = slicedFrames_c.refs[slice].product["Signal"].getData()
	scan, spax = spectralPix, module
	signalAll[:] = signalFrame[scan,spax,:]
	signalAll_c[:] = signalFrame_c[scan,spax,:]
	
	readout = Double1d.range(1000)
	
	minS = MIN(signalAll_c)
	maxS = MAX(signalAll_c)
	
	layer_c = LayerXY(readout[:],signalAll_c[:])
	layer_c.style=Style(line=1, color=Color.RED)
	layer_c.xaxis=Axis(type=Axis.LINEAR, range=[readout[0]-5,readout[-1]+5], titleText="Readouts")
	layer_c.yaxis=Axis(type=Axis.LINEAR, range=[minS-50,maxS+150], titleText="Signal (Jy/m2)")
	layer_c.xaxis.title.fontSize=10
	layer_c.yaxis.title.fontSize=10
	layer_c.name="Corrected Signal"
	
	layer = LayerXY(readout[:],signalAll[:])
	layer.style=Style(line=1, color=Color.BLACK)
	layer.name="Original Signal"
	
	plot = PlotXY(1200,1000)
#	plot.batch = 1
	plot.titleText="Transients Correction - Dippers"
	plot.subtitleText="Spectral Pixel "+str(scan)+" - Module "+str(spax)+" (Spatial pixel ("+str(spax/5)+","+str(spax%5)+"))"
	plot.addLayer(layer_c)
	plot.addLayer(layer)
	plot.legend.visible=1
	plot.legend.columns=2
#	plot.batch = 0


def medianfilter(x=None,L=None):
	N = len(x)
	xin = x+0
	xout = xin*0
	L = int(L)
	if L%2 == 0: L += 1 
	else: pass 
	Lwing = (L-1)/2
	for i,xi in enumerate(xin):
		# left boundary (Lwing terms)
		if i < Lwing: xout[i] = MEDIAN(xin[0:i+Lwing+1]) # (0 to i+Lwing)
		# right boundary (Lwing terms)
		elif i >= N - Lwing: xout[i] = MEDIAN(xin[i-Lwing:N]) # (i-Lwing to N-1)
		# middle (N - 2*Lwing terms; input vector and filter window overlap completely)
		else: xout[i] = MEDIAN(xin[i-Lwing:i+Lwing+1]) # (i-Lwing to i+Lwing)
	return xout

def deNoiseSlicedFrames(slicedFrames_orig,window=5):

	slicedFrames = slicedFrames_orig.copy()
	print "Denoise"
	nbFrames = slicedFrames.getNumberOfFrames()
	
	for raster in range(nbFrames):
		print "Slice",raster
		signalFrame = slicedFrames.refs[raster].product["Signal"].getData()
		for scan in range(1,17):
			for spax in range(25):
				signalFrame[scan,spax,:] = medianfilter(signalFrame[scan,spax,:],window)
		slicedFrames.refs[raster].product["Signal"].setData(signalFrame)
	
	return slicedFrames

def compareSpectra(projectedCube1, projectedCube2, pixX, pixY):
	
	spectraAll1 = projectedCube1.refs[0].product["image"].getData()
	spectraAll2 = projectedCube2.refs[0].product["image"].getData()
	wave1 = projectedCube1.refs[0].product["ImageIndex"].getColumn("DepthIndex").getData()
	wave2 = projectedCube2.refs[0].product["ImageIndex"].getColumn("DepthIndex").getData()
	
	
	spectra1 = spectraAll1[:,pixX,pixY]
	spectra2 = spectraAll2[:,pixX,pixY]
	
#	minS = MIN(MIN(spectra1),MIN(spectra2))
#	maxS = MIN(MAX(spectra1),MAX(spectra2))
#	print minS, maxS
	
	layer1 = LayerXY(wave1[:],spectra1[:])
	layer1.style=Style(line=1, stroke=1,color=Color.BLACK)
	layer1.xaxis=Axis(type=Axis.LINEAR,titleText="Wavelenth")
	layer1.yaxis=Axis(type=Axis.LINEAR,titleText="Signal (Jy/pixel)")
	layer1.xaxis.title.fontSize=10
	layer1.yaxis.title.fontSize=10
	layer1.name="Original"
	
	layer2 = LayerXY(wave2[:],spectra2[:])
	layer2.style=Style(line=1, stroke=1,color=Color.RED)
	layer2.name="Corrected + Medianfilter"
	
	plot = PlotXY(1200,1000)
#	plot.batch = 1
	plot.titleText="Final Projected Cube Spectra"
	plot.subtitleText="Pixel ("+str(pixX)+","+str(pixY)+")"
	plot.addLayer(layer1)
	plot.addLayer(layer2)
	plot.legend.visible=1
	plot.legend.columns=2
#	plot.batch = 0

def PACSmanFIT(product,motifFile, pacsmanDir):
	
	productPACSman = product.copy()
	nbFrames = productPACSman.getNumberOfFrames()
	
	originalDir = motifFile.replace(motifFile.split("/")[-1],"")
	
	i_file = 0
	for i in range(nbFrames):
		try:
			productCube = productPACSman.refs[i].product
			if not productCube.meta["isOffPosition"].value:
				fitsFile = motifFile+str(i_file+1)+".fits"
				simpleFitsWriter(productCube,fitsFile)
				i_file += 1
			else:
				print "slicedCube ",i," is an Off Position!!"
		except:
			print "There is no slicedCube ",i," to save!!"
	
#	if motifFile.count("157.67_CII")>0: folderLine = "CII157"
#	if motifFile.count("63.18_OI")>0: folderLine = "OI63"
#	if motifFile.count("145.52_OI")>0: folderLine = "OI145"
#	if motifFile.count("121.9_NII")>0: folderLine = "NII122"
#	if motifFile.count("119.855_CH+")>0: folderLine = "CH119"
#	if motifFile.count("137.2_CO")>0: folderLine = "CO137"
#	if motifFile.count("84.5_OH")>0: folderLine = "OH84"
#	
#	os.system("mkdir "+pacsmanDir+"/"+folderLine)
#	os.system("cp "+motifFile+"* "+pacsmanDir+"/"+folderLine)
#	
#	fichierBatch = pacsmanDir+"/batchFitAllHipe.pro"
#	fichier = open(fichierBatch, "w")
#	fichier.write(".COMPILE "+pacsmanDir+"/pacsman_fit.pro"+"\n")
#	fichier.write("PACSman_fit, '"+folderLine+"', "+str(i_file)+", '"+motifFile.split("/")[-1]+"'"+"\n")
#	fichier.write("exit\n")
#	fichier.close()
#	
#	os.system("idl "+fichierBatch)
#	
#	os.system("mkdir "+originalDir+folderLine)
#	os.system("cp "+pacsmanDir+"/"+folderLine+"/* "+originalDir+folderLine)
