#!/usr/bin/env python
# -*-coding:utf-8 -*

from multiprocessing import Pool
import os

####################################################################################################################
####################################################################################################################

def launchScriptHIPE(i):
	os.system("hipe "+i)

####################################################################################################################
####################################################################################################################

# n7023E_fts_1	1342225573 1342225574 1342225575
# Ced 201-2	1342225576 1342225577 1342225578
# rho_oph_h2	1342227204 1342227205 1342227206
# HD37022	1342226894 1342226895 1342226896 1342226897
# IC63		1342227621 1342227622 1342227623 1342227624
# IC59		1342228179 1342228180 1342228181
# n2023-PACS	1342228458 1342228459 1342228460
# HH_IR_int-2	1342228507 1342228508 1342228509 1342228510
# HH_dense_core	1342228511 1342228512 1342228513 1342228514
# HD37041 	1342250994 1342250995 1342250996

####################################################################################################################
####################################################################################################################

parallelDir= "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/pacsSpectroPipelineV11Parallel/"
template = parallelDir+"UnchoppedLineScan_v11_oneLinePerObsid_parallel-template.py"
tmp = open(template, 'r')
templateString = tmp.read()
tmp.close()

OBSIDS = []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + []
OBSIDS = OBSIDS + [1342250994, 1342250996]


listCruncher = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]
nbreCore = 10
obsidPerCore = len(OBSIDS)/nbreCore
if (len(OBSIDS)%nbreCore)==0 : 
	obsidPerCore = len(OBSIDS)/nbreCore
else: 
	obsidPerCore = len(OBSIDS)/nbreCore + 1
obsidsPerFile = listCruncher(OBSIDS,obsidPerCore)

nbreFile = len(obsidsPerFile)

for i in range(nbreFile): 
	print obsidsPerFile[i]
	filenameJob = template.replace("parallel-template",str(i))
	fichier = open(filenameJob, "w")
	templateStringJob = templateString.replace("[XXXXXXXXXXXX]",str(obsidsPerFile[i]))
	fichier.write(templateStringJob)
	fichier.close()

spectro= "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/pacsSpectroPipelineV11Parallel/"
scriptToLaunch = [spectro+"UnchoppedLineScan_v11_oneLinePerObsid_"+str(i)+".py" for i in range(nbreFile)]
for i in scriptToLaunch: print i
Pool().map(launchScriptHIPE,scriptToLaunch)
