clear(all=True)

pacs = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/"
os.environ['HANDSONTOOLS'] = pacs
execfile(pacs+'pacsSpectro_tools.py')

# One line per obsid
# n7023E_fts_1	1342225573 1342225574 1342225575
# Ced 201-2	1342225576 1342225577 1342225578
# rho_oph_h2	1342227204 1342227205 1342227206
# HD37022	1342226894 1342226895 1342226896 1342226897
# IC63		1342227621 1342227622 1342227623 1342227624
# IC59		1342228179 1342228180 1342228181
# n2023-PACS	1342228458 1342228459 1342228460
# HH_IR_int-2	1342228507 1342228508 1342228509 1342228510
# HH_dense_core	1342228511 1342228512 1342228513 1342228514
# HD37041 	1342250994 1342250995 1342250996

verbose = 0

sag4 = "/data/glx-herschel/data1/herschel/HIPE_Fits/Spectro_PACS/SAG-4/v11/"

OBSIDS = []
OBSIDS = OBSIDS + [XXXXXXXXXXXX]

#obs = getObservation(1342225576, verbose=True, useHsa=True, poolLocation=None, poolName=None)

for obsid in OBSIDS:

	try:
	

		obs = getObservation(obsid, verbose=True, useHsa=True, poolLocation=None, poolName=None)
		object = obs.meta["object"].value
		object = object.replace(" ","")
		object = object.replace("(","-")
		object = object.replace(")","")
		#bg("saveObservation(obs, poolName=object+'_PACS-Spectro_orig', saveCalTree=True)")

		dirout = sag4+object+"/"
		os.system( "mkdir "+dirout )

		camera = getInfoObs(obs)[0]
		descripLine = getInfoObs(obs)[1]
		print object, obsid, camera, descripLine

		############### EXTRACTING SLICEDFRAMES FROM THE OBSERVATION ##################

		calTree = getCalTree(obs=obs)
		level1 = PacsContext(obs.level1)
		slicedFrames = SlicedFrames(level1.fitted.getCamera(camera).product)

		fitsFiles = dirout+object+"_"+str(obsid)+"_L1_"+camera+"_"+descripLine+"_Frame_R"
		saveFitsforPACSman(slicedFrames,"slicedFrame",fitsFiles)

		slicedCubes = specFrames2PacsCube(slicedFrames)

		################## FLATFIELD ######################
		# 1. Flag outliers and rebin
		waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
		maskToActivate = String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "BADPIXELS"])
		slicedCubes = activateMasks(slicedCubes, maskToActivate, exclusive = True)
		waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
		slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
		slicedCubes = activateMasks(slicedCubes, String1d(["OUTOFBAND","GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "OUTLIERS", "BADPIXELS"]), exclusive = True)
		slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)

		# 2. Mask the spectral lines
		slicedCubesMask = slicedMaskLines(slicedCubes,slicedRebinnedCubes, lineList=[], widthDetect=2.5, widthMask=2.5, threshold=10., copy=1, verbose=False, maskType="INLINE",calTree=calTree)

		# 3. Actual spectral flatfielding
		slicedCubes = slicedSpecFlatFieldLine(slicedCubesMask, scaling=1, copy=1, maxrange=[50.,230.], slopeInContinuum=1, maxScaling=2., maskType="OUTLIERS_FF", offset=0,calTree=calTree,verbose=False)

		# 4. Rename mask OUTLIERS to OUTLIERS_B4FF (specFlagOutliers would refuse to overwrite OUTLIERS) & deactivate mask INLINE
		slicedCubes.renameMask("OUTLIERS", "OUTLIERS_B4FF")
		slicedCubes = deactivateMasks(slicedCubes, String1d(["INLINE", "OUTLIERS_B4FF"]))

		# 5. Remove intermediate results
		del waveGrid, slicedRebinnedCubes, slicedCubesMask

		############## END OF FLATFIELD ########################################
		fitsFiles = dirout+object+"_"+str(obsid)+"_L1_"+camera+"_"+descripLine+"_Cube_R"
		saveFitsforPACSman(slicedCubes,"slicedCube",fitsFiles)


		# ------------------------------------------------------------------------------
		#         Processing      Level 1 -> Level 2
		# ------------------------------------------------------------------------------

		# SELECTING DATA YOU WISH TO REDUCE

		lineId      = []
		wavelength  = []
		rasterLine  = []
		rasterCol   = []
		nodPosition = ""
		nodCycle    = ""
		scical      = ""
		band        = ""
		sliceNumber = []

		onOff       = "ON"
		sCubesOn = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
					   rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
					   band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)
		onOff       = "OFF"
		sCubesOff = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
					   rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
					   band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)

		################## REBINNING THE CUBES ######################

		oversample = 4
		upsample   = 1

		waveGrid=wavelengthGrid(sCubesOn, oversample=oversample, upsample=upsample, calTree = calTree)

		# Flag the remaining outliers (sigma-clipping in wavelength domain)
		sCubesOn  = specFlagOutliers(sCubesOn, waveGrid, nSigma=5, nIter=1)
		sCubesOff = specFlagOutliers(sCubesOff, waveGrid, nSigma=5, nIter=1)

		# Rebin all selected cubes on the same wavelength (mandatory for specAddNod)
		slicedRebinnedCubesOn = specWaveRebin(sCubesOn, waveGrid)
		slicedRebinnedCubesOff = specWaveRebin(sCubesOff, waveGrid)

		# Average all the "ON" & all the "OFF" cubes (per raster position)
		slicedRebinnedCubesOn  = specAverageCubes(slicedRebinnedCubesOn)
		slicedRebinnedCubesOff = specAverageCubes(slicedRebinnedCubesOff)

		################## SUBTRACT OFF POSITIONS ######################

		# 1. Concatenate On and Off cubes in one product
		slicedRebinnedCubesAll = concatenateSliced([slicedRebinnedCubesOn, slicedRebinnedCubesOff])

		# 2. Off Subtraction
		slicedFinalCubes = specSubtractOffPosition(slicedRebinnedCubesAll)

		# specProject works on the final rebinned cubes.
		slicedProjectedCubes = specProject(slicedFinalCubes,outputPixelsize=3.0)
		fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+camera+"_"+descripLine+"_ProjectedCube.fits"
		simpleFitsWriter(slicedProjectedCubes.refs[0].product,fitsFiles)

		# DRIZZLING
		### SUBTRACT ON-OFF
		slicedDiffCubes = specSubtractOffPositionNotRebinned(sCubesOn, slicedRebinnedCubesOff, algorithm=0)

		### CREATE WAVELENGTH AND SPATIAL GRIDS
		oversampleWave = 2
		upsampleWave = 3
		waveGrid = wavelengthGrid(slicedDiffCubes, oversample=oversampleWave, upsample=upsampleWave, calTree = calTree)
		oversampleSpace = 3
		upsampleSpace = 2
		pixFrac = 0.6
		spaceGrid = spatialGrid(slicedDiffCubes, wavelengthGrid=waveGrid, oversample=oversampleSpace, upsample=upsampleSpace, pixfrac=pixFrac, calTree=calTree)

		### DRIZZLE
		slicedDrizzledCubes = drizzle(inCube=slicedDiffCubes, wavelengthGrid=waveGrid, spatialGrid=spaceGrid, superThreadCount=4)
		fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+camera+"_"+descripLine+"_DrizzledCube.fits"
		simpleFitsWriter(slicedDrizzledCubes.refs[0].product,fitsFiles)

	
	except:
		print obsid," Failed"
