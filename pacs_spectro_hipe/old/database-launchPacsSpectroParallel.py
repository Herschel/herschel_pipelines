#!/usr/bin/env python
# -*-coding:utf-8 -*

from multiprocessing import Pool
import os, sys
import pyfits
from random import shuffle
import psycopg2
from fitsToPng import makeMapThumbPlotHeader

####################################################################################################################
####################################################################################################################

def launchScriptPacsSpectro(i):
	script = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/production/UnchoppedLineScan_v13_oneLinePerObsid_template.py"
	os.system("nohup hipe "+script+" "+str(i)+ "> "+script.replace("template.py",str(i)+".txt")+" &")


####################################################################################################################
####################################################################################################################

#release = "R4_pacs_spectro"
#program = "SAG-4/"
#directory = '/data/glx-herschel/data1/herschel/Release/'+release+'/HIPE_Fits/SPECTRO_PACS/'+program



OBSIDS = []

#OBSIDS += [1342189071] # n7023_fts
#OBSIDS += [1342191152] # HD37041
OBSIDS += [1342225573,1342225574,1342225575,1342225576,1342225577,1342225578] # n7023E_fts_1, Ced 201-2 
#OBSIDS += [1342226894,1342226895,1342226896,1342226897] # HD 37022 (towards)
OBSIDS += [1342227204,1342227205,1342227206,1342227207,1342227208] # rho_oph
OBSIDS += [1342227621,1342227622,1342227623,1342227624] # IC63
OBSIDS += [1342228179,1342228180,1342228181,1342228182,1342228183,1342228184,1342228185,1342228186] # IC-59
OBSIDS += [1342228458,1342228459,1342228460] # n2023-PACS
OBSIDS += [1342228507,1342228508,1342228509,1342228510,1342228511,1342228512,1342228513,1342228514] # HH_IR_int-2, HH_dense_core

####################################################################################################################
####################################################################################################################

#nbreCore = int(sys.argv[1])
#
#toProcessPerScriptFile_tmp = []
#for i in range(nbreCore): toProcessPerScriptFile_tmp.append([])
#for i in range(len(OBSIDS)): toProcessPerScriptFile_tmp[i%nbreCore].append(OBSIDS[i])
#
#toProcessPerScriptFile = [x for x in toProcessPerScriptFile_tmp if x]
#nbreCore = len(toProcessPerScriptFile)
#
#for i in toProcessPerScriptFile: print i
#
##Create scripts
#template = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/production/database-UnchoppedLineScan_v11_oneLinePerObsid_template.py"
#tmp = open(template, 'r')
#templateString = tmp.read()
#tmp.close()
#
#filenameScripts = []
#for i in range(nbreCore): 
#	filenameJob = template.replace("template","job-"+str(i))
#	filenameJob = filenameJob.replace("production","production/database-pacsSpectroParallelScripts")
#	filenameScripts.append(filenameJob)
#	fichier = open(filenameJob, "w")
#	templateStringJob = templateString.replace("[XXXXXXXXXX]",str(toProcessPerScriptFile[i]))
#	templateStringJob = templateStringJob.replace("FITS_DIRECTORY",directory)
#	templateStringJob = templateStringJob.replace("RELEASE ",release+" ")
#	fichier.write(templateStringJob)
#	fichier.close()

#Pool().map(launchScriptPacsSpectro,filenameScripts)
Pool().map(launchScriptPacsSpectro,OBSIDS)
#os.system("chmod -R 775 "+directory)

