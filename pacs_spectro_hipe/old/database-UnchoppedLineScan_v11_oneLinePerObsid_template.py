clear(all=True)
pacs = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/"
os.environ['HANDSONTOOLS'] = pacs
execfile(pacs+'pacsSpectro_tools.py')
verbose = 0
sag4 = "FITS_DIRECTORY"
OBSIDS = [XXXXXXXXXX]
for obsid in OBSIDS:
#try:
	obs = getObservation(obsid, verbose=True, useHsa=True, poolLocation=None, poolName=None)
	object = obs.meta["object"].value
	object = object.replace(" ","").replace("(","-").replace(")","")
	dirout = sag4+object+"/"
	os.system( "mkdir -p "+dirout )
	os.system( "mkdir -p "+dirout+"Frame/")
	os.system( "mkdir -p "+dirout+"RasterCube/")
	camera = getInfoObs(obs)[0]
	descripLine = getInfoObs(obs)[1]
	print object, obsid, camera, descripLine
	############### EXTRACTING SLICEDFRAMES FROM THE OBSERVATION ##################
	calTree = getCalTree(obs=obs)
	pacsPropagateMetaKeywords(obs,'0', obs.level0)
	level0 = PacsContext(obs.level0)
	slicedFrames  = SlicedFrames(level0.fitted.getCamera(camera).product)
	slicedRawRamp = level0.raw.getCamera(camera).product
	slicedDmcHead = level0.dmc.getCamera(camera).product
	slicedFrames = specFlagSaturationFrames(slicedFrames, rawRamp = slicedRawRamp, calTree=calTree, copy=1)
	# Convert digital units to Volts, used cal file: Readouts2Volts
	slicedFrames = specConvDigit2VoltsPerSecFrames(slicedFrames, calTree=calTree)
	# Identify the calibration blocks and fill the CALSOURCE Status entry
	slicedFrames = detectCalibrationBlock(slicedFrames)
	# Add the time information in UTC to the Status
	slicedFrames = addUtc(slicedFrames, obs.auxiliary.timeCorrelation)
	# Add the pointing information of the central spaxel to the Status
	slicedFrames = specAddInstantPointing(slicedFrames, obs.auxiliary.pointing, calTree = calTree, orbitEphem = obs.auxiliary.orbitEphemeris, horizonsProduct = obs.auxiliary.horizons)
	# If SSO, move SSO target to a fixed position in sky. This is needed for mapping SSOs.
	if (isSolarSystemObject(obs)):
		slicedFrames = correctRaDec4Sso (slicedFrames, timeOffset=0, orbitEphem=obs.auxiliary.orbitEphemeris, horizonsProduct=obs.auxiliary.horizons, linear=0)
	# Extend the Status of Frames with the parameters GRATSCAN, CHOPPER, CHOPPOS, ONSOURCE, OFFSOURCE
	slicedFrames = specExtendStatus(slicedFrames, calTree=calTree)
	# Convert the chopper readouts to an angle wrt. focal plane unit and the sky
	slicedFrames = convertChopper2Angle(slicedFrames, calTree=calTree)
	# Add the positions for each pixel (ra and dec datasets)
	slicedFrames = specAssignRaDec(slicedFrames, calTree=calTree)
	# Add the wavelength for each pixel (wave dataset), used cal file: WavePolynomes
	slicedFrames = waveCalc(slicedFrames, calTree=calTree)
	# Correct the wavelength for the spacecraft velocity.
	slicedFrames = specCorrectHerschelVelocity(slicedFrames, obs.auxiliary.orbitEphemeris, obs.auxiliary.pointing, obs.auxiliary.timeCorrelation, obs.auxiliary.horizons)
	# Find the major logical blocks of this observation and organise them in the
	slicedFrames = findBlocks(slicedFrames, calTree = calTree)
	# Flag the known bad or noisy pixels in the masks "BADPIXELS" and "NOISYPIXELS"
	slicedFrames = specFlagBadPixelsFrames(slicedFrames, calTree=calTree)
	# Slice the data by Line/Range, Raster Point, nod position, nod cycle, on/off position and per band.
	slicedFrames = pacsSliceContext(slicedFrames,[slicedDmcHead],removeUndefined=True, removeMasked=True)
	slicedDmcHead = pacsSliceContext.additionalOutContexts[0]
	# Flag the data affected by the chopper movement in the mask "UNCLEANCHOP"
	# Uses the high resolution Dec/Mec header and the cal files ChopperAngle and ChopperJitterThreshold.
	# This task remains necessary for unchopped observations wrt the calibration block
	slicedFrames = flagChopMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)
	# Flag the data effected by the grating movement in the mask "GRATMOVE"
	# Uses the high resolution Dec/Mec header and the cal file GratingJitterThreshold
	slicedFrames = flagGratMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)
	# Add Status words GratingCycle & IndexInCycle, which are grating movement information
	slicedFrames = specAddGratingCycleStatus(slicedFrames)
	# Detect and flag glitches ("GLITCH" mask)
	slicedFrames = specFlagGlitchFramesQTest(slicedFrames,copy=1)
	# Activate all masks, for all slices
	slicedFrames = activateMasks(slicedFrames, slicedFrames.get(0).getMaskTypes())
	# Convert the signal level to the reference integration capacitance (which is the smallest one)
	slicedFrames = convertSignal2StandardCap(slicedFrames, calTree=calTree)
	# Derive detectors' response from calibration block
	calBlock = selectSlices(slicedFrames,scical="cal").get(0)
	csResponseAndDark = specDiffCs(calBlock, calTree = calTree)
	# subtract the dark, using the nominal dark current in the calibration tree
	slicedFrames = specSubtractDark(slicedFrames, calTree=calTree)
	gaps = specSignalGap (slicedFrames)
	# Divide by the relative spectral response function
	slicedFrames = rsrfCal(slicedFrames, calTree=calTree)
	# Divide by the response
	slicedFrames = specRespCal(slicedFrames, csResponseAndDark=csResponseAndDark)
	# Correct for detector transients
	slicedFrames = specLongTermTransient(slicedFrames, gaps=gaps)
	# Clip the calibration block out
	slicedFrames = selectSlices(slicedFrames,scical="sci")
	fitsFiles = dirout+"Frame/"+object+"_"+str(obsid)+"_L1_"+camera+"_"+descripLine+"_Frame_R"
	saveFitsforPACSman(slicedFrames,"slicedFrame",fitsFiles)
	slicedCubes = specFrames2PacsCube(slicedFrames)
	################## FLATFIELD ######################
	# 1. Flag outliers and rebin
	waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
	maskToActivate = String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "BADPIXELS"])
	slicedCubes = activateMasks(slicedCubes, maskToActivate, exclusive = True)
	waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
	slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
	slicedCubes = activateMasks(slicedCubes, String1d(["OUTOFBAND","GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "OUTLIERS", "BADPIXELS"]), exclusive = True)
	slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)
	# 2. Mask the spectral lines
	slicedCubesMask = slicedMaskLines(slicedCubes,slicedRebinnedCubes, lineList=[], widthDetect=2.5, widthMask=2.5, threshold=10., copy=1, verbose=False, maskType="INLINE",calTree=calTree)
	# 3. Actual spectral flatfielding
	slicedCubes = slicedSpecFlatFieldLine(slicedCubesMask, scaling=1, copy=1, maxrange=[50.,230.], slopeInContinuum=1, maxScaling=2., maskType="OUTLIERS_FF", offset=0,calTree=calTree,verbose=False)
	# 4. Rename mask OUTLIERS to OUTLIERS_B4FF (specFlagOutliers would refuse to overwrite OUTLIERS) & deactivate mask INLINE
	slicedCubes.renameMask("OUTLIERS", "OUTLIERS_B4FF")
	slicedCubes = deactivateMasks(slicedCubes, String1d(["INLINE", "OUTLIERS_B4FF"]))
	# 5. Remove intermediate results
	del waveGrid, slicedRebinnedCubes, slicedCubesMask
	############## END OF FLATFIELD ########################################
	fitsFiles = dirout+"RasterCube/"+object+"_"+str(obsid)+"_L1_"+camera+"_"+descripLine+"_RasterCube_R"
	saveFitsforPACSman(slicedCubes,"slicedCube",fitsFiles)
	# ------------------------------------------------------------------------------
	#         Processing      Level 1 -> Level 2
	# ------------------------------------------------------------------------------
	# SELECTING DATA YOU WISH TO REDUCE
	lineId, wavelength, rasterLine, rasterCol, sliceNumber      = [], [], [], [], []
	nodPosition, nodCycle, scical, band = "", "", "", "", ""
	onOff       = "ON"
	sCubesOn = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
				   rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
				   band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)
	onOff       = "OFF"
	sCubesOff = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
				   rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
				   band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)
	################## REBINNING THE CUBES ######################
	oversample, upsample = 4, 1
	waveGrid=wavelengthGrid(sCubesOn, oversample=oversample, upsample=upsample, calTree = calTree)
	# Flag the remaining outliers (sigma-clipping in wavelength domain)
	sCubesOn  = specFlagOutliers(sCubesOn, waveGrid, nSigma=5, nIter=1)
	sCubesOff = specFlagOutliers(sCubesOff, waveGrid, nSigma=5, nIter=1)
	# Rebin all selected cubes on the same wavelength (mandatory for specAddNod)
	slicedRebinnedCubesOn = specWaveRebin(sCubesOn, waveGrid)
	slicedRebinnedCubesOff = specWaveRebin(sCubesOff, waveGrid)
	# Average all the "ON" & all the "OFF" cubes (per raster position)
	slicedRebinnedCubesOn  = specAverageCubes(slicedRebinnedCubesOn)
	slicedRebinnedCubesOff = specAverageCubes(slicedRebinnedCubesOff)
	################## SUBTRACT OFF POSITIONS ######################
	# 1. Concatenate On and Off cubes in one product
	slicedRebinnedCubesAll = concatenateSliced([slicedRebinnedCubesOn, slicedRebinnedCubesOff])
	# 2. Off Subtraction
	slicedFinalCubes = specSubtractOffPosition(slicedRebinnedCubesAll)
	# specProject works on the final rebinned cubes.
	slicedProjectedCubes = specProject(slicedFinalCubes,outputPixelsize=3.0)
	fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+camera+"_"+descripLine+"_ProjectedCube.fits"
	simpleFitsWriter(slicedProjectedCubes.refs[0].product,fitsFiles)
	# DRIZZLING
	oversample, upsample = 1, 2
	waveGrid=wavelengthGrid(sCubesOff, oversample=oversample, upsample=upsample, calTree = calTree)
	slicedRebinnedCubesOff = specWaveRebin(sCubesOff, waveGrid)
	### SUBTRACT ON-OFF
	licedDiffCubes = specSubtractOffPositionNotRebinned(sCubesOn, slicedRebinnedCubesOff, algorithm=0)
	### CREATE WAVELENGTH AND SPATIAL GRIDS
	oversampleWave, upsampleWave = 2, 3
	waveGrid = wavelengthGrid(slicedDiffCubes, oversample=oversampleWave, upsample=upsampleWave, calTree = calTree)
	oversampleSpace, upsampleSpace, pixFrac = 3, 2, 0.6
	spaceGrid = spatialGrid(slicedDiffCubes, wavelengthGrid=waveGrid, oversample=oversampleSpace, upsample=upsampleSpace, pixfrac=pixFrac, calTree=calTree)
	### DRIZZLE
	slicedDrizzledCubes = drizzle(inCube=slicedDiffCubes, wavelengthGrid=waveGrid, spatialGrid=spaceGrid, superThreadCount=4)
	fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+camera+"_"+descripLine+"_DrizzledCube.fits"
	simpleFitsWriter(slicedDrizzledCubes.refs[0].product,fitsFiles)
	# Addinf files to database
	os.system("python /data/glx-herschel/data1/herschel/scriptsPACS/Spectro/production/database-addToDatabase.py "+"RELEASE "+dirout)
#except:
#	print obsid," Failed"
