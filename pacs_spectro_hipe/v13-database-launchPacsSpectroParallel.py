#!/usr/bin/env python
# -*-coding:utf-8 -*

from multiprocessing import Pool, cpu_count
import os, sys, itertools
#from fitsToPng import makeMapThumbPlotHeader

####################################################################################################################
####################################################################################################################

def launchScriptPacsSpectro(i):
	scriptPath = os.path.join(DirName, script)
	log = script.replace("template.py", str(i)+".txt")
	logPath = os.path.join(DirName, logDir, log)
	os.system("hipe "+scriptPath+" "+str(i)+" "+release+" > "+logPath)

####################################################################################################################
####################################################################################################################

current_script = os.path.realpath(__file__)
DirName = os.path.dirname(current_script)

#~ script = "UnchoppedLineScan_v13_oneLinePerObsid_template.py"
script = "UnchoppedLineScan_v14_oneLinePerObsid_template.py"
logDir = "log"
release = "R6_pacs_spectro"

####################################################################################################################

OBSIDS = []
#WARNING: This observation is identified as wavelength switching.
#WARNING: This function was neither validated nor even tested on such a case.
#OBSIDS += [1342189071] # n7023_fts
OBSIDS += [1342191152] # HD37041
#~ OBSIDS += [1342225573,1342225574,1342225575,1342225576,1342225577,1342225578] # n7023E_fts_1, Ced 201-2 
#~ OBSIDS += [1342226894,1342226895,1342226896,1342226897] # HD 37022 (towards)
#~ OBSIDS += [1342227204,1342227205,1342227206,1342227207,1342227208] # rho_oph
#~ OBSIDS += [1342227621,1342227622,1342227623,1342227624] # IC63
#~ OBSIDS += [1342228179,1342228180,1342228181,1342228182,1342228183,1342228184,1342228185,1342228186] # IC-59
#~ OBSIDS += [1342228458,1342228459,1342228460] # n2023-PACS
#~ OBSIDS += [1342228507,1342228508,1342228509,1342228510,1342228511,1342228512,1342228513,1342228514] # HH_IR_int-2, HH_dense_core 

####################################################################################################################
####################################################################################################################

print len(OBSIDS), "obsids to process in parallel"
try:
	print "Number of Cores on machine:", cpu_count()
except:
	pass
try:
	print "Load Average:"
	os.system("cat /proc/loadavg")
except:
	print "Error"
	
try:
	nberProcess = int(sys.argv[1])
	toDo = sys.argv[2]

	if toDo=="go":
		p = Pool(processes=nberProcess)
		p.map(launchScriptPacsSpectro,OBSIDS)
	
except: 
	print """
	Usage:
	To see which obsids will be processed and some machine related information like number of cores and load average:
	      python v13-database-launchPacsSpectroParallel.py 
	To do the actual processing:
	      python v13-database-launchPacsSpectroParallel.py #nberProcess go
	"""
