# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2011 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
"""    
  $Id: UnchoppedLineScanTransCorr.py,v 1.1.4.6 2015/03/06 08:01:15 jjacob Exp $      

----------------------------USER INSTRUCTIONS--------------------------------
-----------------------PLEASE READ BEFORE YOU BEGIN--------------------------
-----------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!--------------------------

Available from the HIPE GUI via:
 Pipeline -> PACS -> Spectrometer -> Unchopped Line Scan -> lineScan
 Also to be found in your HIPE installation directory in: scripts/pacs/scripts/ipipe/spec

Purpose:
  PACS Spectrometer interactive pipeline processing from level 0 to level 2 for 
  unchopped line spectroscopy observations. Other scripts for 
  reducing PACS spectroscopy are available via the Pipeline menu in HIPE.
  The differences between working with point and extended sources occur
  after Level 2 (formal end of the pipeline), in the post-processing part.

Note: 
  This file is the reference copy, taken from your current installation of HIPE.
  If you need to edit it, we recommend to copy it to a different location first.
   
Description:
- This script starts with various setup options. In particular the OBSID
  and camera ('red' or 'blue') should be tuned everytime you run it.
  We advise also to activate the option interactive = True to plot further
  diagnostic plots and decide if apply or not a correction based on the
  expected telescope background.
- This script is supposed to be run line-by-line.
- At every moment, you can save your intermediate products to a pool with
  saveSlicedCopy(mySlicedPacsProduct, poolName[, poolLocation]), or load a 
  product previously saved with readSliced(poolName [,poolLocation])
  Note that you can't overwrite an existing pool with the same name
- Comments are included in this script, but for a full explanation see the 
  PACS Data Reduction Guide (PDRG): 
    -> Help Menu -> Show Contents -> PACS Data Reduction Guide
  The pipeline tasks themselves are explained in their "URM" entries, which you
  can find in the HIPE help page, accessed via the HIPE GUI menu
     Help -> Help Contents
  under:
     References -> PACS User's Reference Manual

Inspection:
- At each and every moment between Level 0 and Level 2, 
  slicedSummary(mySlicedPacsProduct) will return information about the contents 
  and layout of your data. If you aim at a more visual representation of your
  data structure, use slicedSummaryPlot(mySlicedPacsProduct).
- Similarly, you can call maskSummary(mySlicedPacsProduct[, slice=<number>]) to 
  get an overview of the masks that exist and/or are taken into account 
  (i.e. 'active') at that moment.
- There exist a number of additional quick and easy visualisation tools, like
  MaskViewer, plotCubes, plotCubesMasks, plotPixel, slicedPlotPointing, etc. 
  Some of them appear below, others not. See the documentation for a complete list.
- Any individual PACS' products (i.e. any slice) can be inspected in detail in 
  the "Spectrum Explorer". For instance, you may do 
	oneFrame = slicedFrames.get(slice)
	openVariable("oneFrame","Spectrum Explorer")
- The source code of the pipeline tasks can be accessed the following way:
  In the 'Tasks' panel (Hipe Menu - Window - Show View - Workbench - Tasks),
  open the 'All', right click on the Task and 'view source'.

Author: PACS ICC
"""

import os,sys
from javax.swing import JOptionPane

pacs = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/pacs_spectro_hipe/"
os.environ['HANDSONTOOLS'] = pacs
execfile(pacs+'pacsSpectro_tools.py')



# ------------------------------------------------------------------------------
# GET THE DATA
# ------------------------------------------------------------------------------
#    
# First, set the OBSID of the observation to process.
# CHANGE THE OBSID here to your own.
obsid = sys.argv[1]
release = sys.argv[2]

program = "SAG-4/"
dirout = '/data/glx-herschel/data1/herschel/Release/'+release+'/HIPE_Fits/SPECTRO_PACS/'+program
print obsid, release, dirout

verbose = False

# Run a pipeline script in multi-threading
Configuration.setProperty("herschel.pacs.spg.common.superThreadCount","0")
Configuration.setProperty("herschel.pacs.spg.spec.threadCount","0")

# Next, get the data
useHsa = True
obs    = getObservation(obsid, verbose=True, useHsa=useHsa, poolLocation=None, poolName=None)
#if useHsa: saveObservation(obs, poolLocation=None, poolName=None)

object = obs.meta["object"].value
object = object.replace(" ","").replace("(","-").replace(")","")
dirout += object+"/"
try: 
	os.system( "mkdir -p "+dirout )
	os.system( "mkdir -p "+dirout+"Frame/")
	os.system( "mkdir -p "+dirout+"RasterCube/")
except:
	pass

camera = getInfoObs(obs)[0]
descripLine = getInfoObs(obs)[1]
print object, obsid, camera, descripLine

# Extract the level-0 products from the ObservationContext
pacsPropagateMetaKeywords(obs,'0', obs.level0)
level0 = PacsContext(obs.level0)

# ------------------------------------------------------------------------------
# COMPUTE A NEW POINTING PRODUCT
# ------------------------------------------------------------------------------
# If level0 was generated with SPG 11 or 12 (check in obs.meta["creator"]),
# run this block. For SPG 13 and later, don't
if obs.meta["creator"].value.count("v11")>0 or obs.meta["creator"].value.count("v12")>0:
	acmsProduct = obs.auxiliary.acms
	tcHistoryProduct = obs.auxiliary.teleCommHistory
	oldpp = obs.auxiliary.pointing
	newPP = calcAttitude(oldpp, acmsProduct, tcHistoryProduct)

# ------------------------------------------------------------------------------
# Set up the calibration tree. We take the most recent calibration files, 
# for the specific time of your observation (obs=obs)
calTree = getCalTree(obs=obs)

# ------------------------------------------------------------------------------
# SELECT DATA FROM ONE CAMERA 
# ------------------------------------------------------------------------------

# Red or blue camera ? 
#camera    = 'red'

# For your camera, extract the Frames (scientific data), the rawramps (raw data 
# for one pixel), and the DMC header (the mechanisms' status information, 
# sampled at a high frequency) 
slicedFrames  = SlicedFrames(level0.fitted.getCamera(camera).product)
slicedRawRamp = level0.raw.getCamera(camera).product
slicedDmcHead = level0.dmc.getCamera(camera).product    

# ------------------------------------------------------------------------------
# SETUP -> OUTPUT
# ------------------------------------------------------------------------------

# Extract basic information
calSet = str(calTree.version)

# ------------------------------------------------------------------------------
#        Processing      Level 0 -> Level 0.5
# ------------------------------------------------------------------------------

# flag the saturated data in a mask "SATURATION" (and "RAWSATURATION": this 
# uses the raw data we get for some pixels)
# used cal files: RampSatLimits and SignalSatLimits
# copy=1 makes slicedFrames a fully independent product; it is recommended you do
# this before you start pipelineing.
slicedFrames = specFlagSaturationFrames(slicedFrames, rawRamp = slicedRawRamp, calTree=calTree, copy=1)

# Convert digital units to Volts, used cal file: Readouts2Volts
slicedFrames = specConvDigit2VoltsPerSecFrames(slicedFrames, calTree=calTree)

# Identify the calibration blocks and fill the CALSOURCE Status entry
slicedFrames = detectCalibrationBlock(slicedFrames)

# Add the time information in UTC to the Status
slicedFrames = addUtc(slicedFrames, obs.auxiliary.timeCorrelation)

# Add the pointing information of the central spaxel to the Status
#   Uses the pointing, horizons product (solar system object ephemeries), 
#   orbitEphemeris products, and the SIAM cal file.
slicedFrames = specAddInstantPointing(slicedFrames, obs.auxiliary.pointing, calTree = calTree, orbitEphem = obs.auxiliary.orbitEphemeris, horizonsProduct = obs.auxiliary.horizons)    

# If SSO, move SSO target to a fixed position in sky. This is needed for mapping SSOs.
if (isSolarSystemObject(obs)):
	slicedFrames = correctRaDec4Sso (slicedFrames, timeOffset=0, orbitEphem=obs.auxiliary.orbitEphemeris, horizonsProduct=obs.auxiliary.horizons, linear=0)

# Extend the Status of Frames with the parameters GRATSCAN, CHOPPER, CHOPPOS, ONSOURCE, OFFSOURCE
# used cal file: ChopperThrowDescription
slicedFrames = specExtendStatus(slicedFrames, calTree=calTree)

# Convert the chopper readouts to an angle wrt. focal plane unit and the sky
# and add this to the Status, used cal files: ChopperAngle and ChopperSkyAngle
slicedFrames = convertChopper2Angle(slicedFrames, calTree=calTree)

# Add the positions for each pixel (ra and dec datasets)
# used cal files: ArrayInstrument and ModuleArray
slicedFrames = specAssignRaDec(slicedFrames, calTree=calTree)

# Add the wavelength for each pixel (wave dataset), used cal file: WavePolynomes
slicedFrames = waveCalc(slicedFrames, calTree=calTree)

# Correct the wavelength for the spacecraft velocity. 
# Uses the pointing, orbitEphemeris and timeCorrelation product.
slicedFrames = specCorrectHerschelVelocity(slicedFrames, obs.auxiliary.orbitEphemeris, obs.auxiliary.pointing, obs.auxiliary.timeCorrelation, obs.auxiliary.horizons)

# Find the major logical blocks of this observation and organise them in the 
# BlockTable attached to the Frames; used cal file: ObcpDescription
slicedFrames = findBlocks(slicedFrames, calTree = calTree)

# Flag the known bad or noisy pixels in the masks "BADPIXELS" and "NOISYPIXELS"
# used cal files: BadPixelMask and NoisyPixelMask
#   -> by default the bad pixels will be excluded later when final cubes are built, the noisy pixels are not
slicedFrames = specFlagBadPixelsFrames(slicedFrames, calTree=calTree)

# Slice the data by Line/Range, Raster Point, nod position, nod cycle, on/off position and per band.
# The parameters removeUndefined and removeMasked are for cleaning purposes
# If you want to keep the OUTOFBAND data, set removeMasked to False
slicedFrames, additionalOutContexts = pacsSliceContext(slicedFrames,[slicedDmcHead],removeUndefined=True, removeMasked=True)
slicedDmcHead = additionalOutContexts[0]

# Flag the data affected by the chopper movement in the mask "UNCLEANCHOP"
# Uses the high resolution Dec/Mec header and the cal files ChopperAngle and ChopperJitterThreshold. 
# This task remains necessary for unchopped observations wrt the calibration block
slicedFrames = flagChopMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)

# Flag the data effected by the grating movement in the mask "GRATMOVE"
# Uses the high resolution Dec/Mec header and the cal file GratingJitterThreshold 
slicedFrames = flagGratMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)

# Add Status words GratingCycle & IndexInCycle, which are grating movement information
slicedFrames = specAddGratingCycleStatus(slicedFrames)

# ------------------------------------------------------------------------------
#         Processing      Level 0.5 -> Level 1
# ------------------------------------------------------------------------------

# De-activate all masks before running the glitch flagging
slicedFrames = activateMasks(slicedFrames, String1d([" "]), exclusive = True)

# Detect and flag glitches ("GLITCH" mask)
slicedFrames = specFlagGlitchFramesMAD(slicedFrames,copy=1)

# Activate all masks, for all slices
slicedFrames = activateMasks(slicedFrames, slicedFrames.get(0).getMaskTypes())

# Convert the signal level to the reference integration capacitance (which is the smallest one)
# used cal file: capacitanceRatios
slicedFrames = convertSignal2StandardCap(slicedFrames, calTree=calTree)

# Derive detectors' response from calibration block
# used cal files: observedResponse, calSourceFlux
calBlock = selectSlices(slicedFrames,scical="cal").get(0)
csResponseAndDark = specDiffCs(calBlock, calTree = calTree)

# subtract the dark, using the nominal dark current in the calibration tree 
# calFile used: spectrometer.darkCurrent
slicedFrames = specSubtractDark(slicedFrames, calTree=calTree)

# Divide by the relative spectral response function 
# Used cal files: rsrfR1, rsrfB2A, rsrfB2B or rsrfB3A
slicedFrames = rsrfCal(slicedFrames, calTree=calTree)

# Divide by the response
# Use intermediate product from specDiffCs : csResponseAndDark
slicedFrames = specRespCal(slicedFrames, csResponseAndDark=csResponseAndDark)

# Clip the calibration block out
slicedFrames = selectSlices(slicedFrames,scical="sci")

slicedFrames = specLongTermTransCorr(slicedFrames,calTree=calTree,verbose=verbose, applyCorrection=1)

# correction of transients due to cosmic rays
slicedFrames = specMedianSpectrum(slicedFrames,calTree=calTree)
slicedFrames = specTransCorr(slicedFrames,calTree=calTree)

fitsFiles = dirout+"Frame/"+object+"_"+str(obsid)+"_L1_"+calSet+"_"+camera+"_"+descripLine+"_Frame_R"
saveFitsforPACSman(slicedFrames,"slicedFrame",fitsFiles)

# Convert the Frames to PacsCubes
slicedCubes = specFrames2PacsCube(slicedFrames)

################## FLATFIELD ######################
# 1. Flag outliers and rebin
waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
maskToActivate = String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "BADPIXELS"])
slicedCubes = activateMasks(slicedCubes, maskToActivate, exclusive = True)
waveGrid = wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
slicedCubes = activateMasks(slicedCubes, String1d(["OUTOFBAND","GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "OUTLIERS", "BADPIXELS"]), exclusive = True)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)

# 2. Mask the spectral lines
slicedCubesMask = maskLines(slicedCubes,slicedRebinnedCubes, lineList=[], widthDetect=2.5, widthMask=2.5, threshold=10., copy=1, verbose=False, maskType="INLINE",calTree=calTree)

# 3. Actual spectral flatfielding
slicedCubes = specFlatFieldLine(slicedCubesMask, scaling=1, copy=1, maxrange=[50.,230.], slopeInContinuum=1, maxScaling=2., maskType="OUTLIERS_FF", offset=0,calTree=calTree,verbose=False)

# 4. Rename mask OUTLIERS to OUTLIERS_B4FF (specFlagOutliers would refuse to overwrite OUTLIERS) & deactivate mask INLINE
slicedCubes.renameMask("OUTLIERS", "OUTLIERS_B4FF")
slicedCubes = deactivateMasks(slicedCubes, String1d(["INLINE", "OUTLIERS_B4FF"]))

# 5. Remove intermediate results
del waveGrid, slicedRebinnedCubes, slicedCubesMask

fitsFiles = dirout+"RasterCube/"+object+"_"+str(obsid)+"_L1_"+calSet+"_"+camera+"_"+descripLine+"_RasterCube_R"
saveFitsforPACSman(slicedCubes,"slicedCube",fitsFiles)

# ------------------------------------------------------------------------------
#         Processing      Level 1 -> Level 2
# ------------------------------------------------------------------------------

# Here we separate the ON- and OFF-source slices

lineId      = []
wavelength  = []
rasterLine  = []
rasterCol   = []
nodPosition = ""
nodCycle    = ""
scical      = ""
band        = ""
sliceNumber = []

onOff       = "ON"
sCubesOn = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
         rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
         band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)
onOff       = "OFF"
sCubesOff = selectSlices(slicedCubes, lineId=lineId, wavelength=wavelength, rasterLine=rasterLine,\
         rasterCol=rasterCol, nodPosition=nodPosition, nodCycle=nodCycle, scical=scical,\
         band=band, sliceNumber=sliceNumber, onOff=onOff, verbose=verbose)

# Building a wavelength grid from the first of the selected slices (this will then be used for all 
#   slices). Used cal file: wavelengthGrid
# The wavelength grid is used to perform a final sigma clipping on the spectrum (specFlagOutliers)
#     and then to rebin the spectrum (specWaveRebin)
# Note that for the final cube rebinning it is recommended that: 
#    - For single scan SED or Nyquist sampled range scans, it is recommended to perform the final rebinning with 
#        oversample=2, upsample>=1, which corresponds to the native resolution of the instrument
#    - For line scan, deep range scans or Nyquist sampled range scans with repetition factor > 1, 
#        oversampling > 2 is made possible by the high degree of redundancy provided by the observation
#
# Upsampling factors > 1 introduce correlated noise. If you want to assess noise in the spectrum set it to 1.
#
oversample = 4
upsample   = 1

waveGrid=wavelengthGrid(sCubesOn, oversample=oversample, upsample=upsample, calTree = calTree)

# Flag the remaining outliers (sigma-clipping in wavelength domain)
sCubesOn  = specFlagOutliers(sCubesOn, waveGrid, nSigma=5, nIter=1)
sCubesOff = specFlagOutliers(sCubesOff, waveGrid, nSigma=5, nIter=1)

# Rebin all selected cubes on the same wavelength (mandatory for specAddNod)
# To compare the rebinned spectrum to the dot cloud (possibly with various rebinning parameters), see the PDRG (Chap 4)
slicedRebinnedCubesOn = specWaveRebin(sCubesOn, waveGrid)
slicedRebinnedCubesOff = specWaveRebin(sCubesOff, waveGrid)

# Average all the "ON" & all the "OFF" cubes (per raster position)
# (=> slicedRebinnedCubesOff has only one slice / spectral line)

slicedRebinnedCubesOn  = specAverageCubes(slicedRebinnedCubesOn)
slicedRebinnedCubesOff = specAverageCubes(slicedRebinnedCubesOff)

# Subtract the off.  
# 1. Concatenate On and Off cubes in one product
slicedRebinnedCubesAll = concatenateSliced([slicedRebinnedCubesOn, slicedRebinnedCubesOff])

# 2. Off Subtraction
slicedFinalCubes = specSubtractOffPosition(slicedRebinnedCubesAll)

# ------------------------------------------------------------------------------
#         Processing Level 2.0
# ------------------------------------------------------------------------------
#
#
# ------------------------------------------------------------------------------
#         Post-Processing
# ------------------------------------------------------------------------------

# Before you proceed with the scientific analysis, you will need to make a choice
# based on the nature of your object (extended or not), the type of observation
# that was performed (pointed or raster mapping), and its spatial sampling
# (oversampled or not). 
# - Spatially-oversampled raster maps: are projected to a single data cube, 
# thereby recovering the fullest possible spatial information. The final 
# science-grade product is a "projected" or "drizzled" cube. 
# - Single pointing observations of point sources: to recover the point-source 
# calibrated spectrum, run extractCentralSpectrum (not covered here, since this task
# makes use of the continuum level, which is not guaranteed in unchopped observations)
# - Single pointing of extended source or spatially undersampled rasters: 
# the final science-grade product is the "rebinned" cubes, or "interpolated" cubes

# ------- EXTENDED SOURCES - SPATIALLY OVERSAMPLED RASTERS ---------------------

# SPECPROJECT
# specProject works on the final rebinned cubes.
slicedProjectedCubes = specProject(slicedFinalCubes,outputPixelsize=3.0)

fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+calSet+"_"+camera+"_"+descripLine+"_ProjectedCube.fits"
simpleFitsWriter(slicedProjectedCubes.refs[0].product,fitsFiles)

# DRIZZLING

# Drizzling is relevant for raster maps. It works from the not-rebinned PacsCubes.

"""
# Optional: often the OFF-source observations are less deep than the ON-source observations. 
# In this case you may want to build a smooth version of the OFF-source spectra (which will then 
# be subtracted from the ON-source spectra). For this, redo the rebinning with a small value of 
# 'oversample'. 
# However, if your OFF-spectra are of sufficient quality, it is preferable to 
# use the same rebinning parameter values as for the ON-source spectra. In this case you do 
# not need to redo the rebinning (and this is why this block is optional)
oversample = 1
upsample   = 2
waveGrid=wavelengthGrid(sCubesOff, oversample=oversample, upsample=upsample, calTree = calTree)
slicedRebinnedCubesOff = specWaveRebin(sCubesOff, waveGrid)
"""

### SUBTRACT ON-OFF
# In case the OFF position was visited multiple times, there are several
# ways to subtract the off-source spectra:
# Algorithm = AVERAGE for average OFF, CLOSEST for closest in time, INTERPOLATION for (time-based) interpolation
slicedDiffCubes = specSubtractOffPosition(sCubesOn, slicedRebinnedCubesOff, algorithm="AVERAGE")

### CREATE WAVELENGTH AND SPATIAL GRIDS
# For an explanation of the parameters of wavelengthGrid, spatialGrid and drizzle,
# see the PACS spectroscopy data reduction guide
# Recommended values for oversampleSpace and upsampleSpace are 3 and 2 respectively
# Recommended value for pixFrac is >= 0.3 for oversampled rasters, and >= 0.6 for Nyquist sampled rasters
# The raster step sizes can be found in meta data entries 'lineStep' and 'pointStep'
oversampleWave  = 2
upsampleWave    = 3
waveGrid        = wavelengthGrid(slicedDiffCubes, oversample=oversampleWave, upsample=upsampleWave, calTree = calTree)
oversampleSpace = 3
upsampleSpace   = 2
pixFrac         = 0.6
spaceGrid       = spatialGrid(slicedDiffCubes, wavelengthGrid=waveGrid, oversample=oversampleSpace, upsample=upsampleSpace, pixfrac=pixFrac, calTree=calTree)

### DRIZZLE
slicedDrizzledCubes = drizzle(slicedDiffCubes, wavelengthGrid=waveGrid, spatialGrid=spaceGrid)[0]

fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+calSet+"_"+camera+"_"+descripLine+"_DrizzledCube.fits"
simpleFitsWriter(slicedDrizzledCubes.refs[0].product,fitsFiles)

# ------- UNDERSAMPLED MAPS & SINGLE POINTING OF EXTENDED SOURCES --------------

# The final science grade products are the previously produced "slicedFinalCubes"
# You can also interpolate these cubes on a regular spatial grid using specInterpolate

outputPixelsize = 4.7
slicedInterpolatedCubes = specInterpolate(slicedFinalCubes, outputPixelsize=outputPixelsize, conserveFlux=True)

fitsFiles = dirout+object+"_"+str(obsid)+"_L2_"+calSet+"_"+camera+"_"+descripLine+"_RegularSpatialGridInterpolationCube.fits"
simpleFitsWriter(slicedInterpolatedCubes.refs[0].product,fitsFiles)

os.system("chmod -R 775 "+dirout)

#Add to database
os.system("python /data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/PACS_Spectro_Tools.py "+release+" "+dirout)
