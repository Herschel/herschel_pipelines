#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# Retrieve pacs spectro L2 from HSA and store separately all the products available, apart from the tables. 
# Note that the list of products is hard-coded and no products from the observations, not present in the dictionnaries of products can be extracted. 
# Launch the script via : 
#> hipe get_pacs_spectro_from_HSA.py

# Import all needed classes
from herschel.spire.all import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter

# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *

# Input definition
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *
from herschel.spire.ia.scripts.tools.obsLoader import *
from herschel.ia.pal.util import StorageResult
from os import path, makedirs
import glob
import string
import pdb

# get export2sanepic
from export import * 
from IAS_tools import *
import time

start_time = time.time()

outputDir = '/data/glx-herschel/data1/herschel/HIPE_Fits/Spectro_PACS' # Is this the right one?
#~ outputDir = '/home/ccossou/output_test'

# From PACS SPECTRO level 2, the following products are available:
# HPS3DR(spectrometer,red,cube) - Spectroscopy red 3d cube context
# HPS3DB(spectrometer,blue,cube) - Spectroscopy blue 3d cube context

# HPS3DRR(spectrometer,red,rcube) - Spectroscopy red rebinned 3d cube context
# HPS3DRB(spectrometer,blue,rcube) - Spectroscopy blue rebinned 3d cube context

# HPS3DPR(spectrometer,red,pcube) - Spectroscopy red projected 3d cube context
# HPS3DPB(spectrometer,blue,pcube) - Spectroscopy blue projected 3d cube context

# HPS3DDR(spectrometer,red,dcube) - Spectroscopy red drizzled 3d cube context

# HPS3DEQDR(spectrometer,red,deqcube) - Spectroscopy red equidistant drizzled 3d cube context

# HPS3DIB(spectrometer,blue,icube) - Spectroscopy blue interpolated 3d cube context

# HPS3DEQIB(spectrometer,blue,ieqcube) - Spectroscopy blue equidistant interpolated 3d cube context

# Tables have been omitted from products. Blue and red are not specified in the product detail
# HPSTBRR(spectrometer,red,cubetable) - Spectroscopy red rebinned cube as table
# HPSTBRB(spectrometer,blue,cubetable) - Spectroscopy blue rebinned cube as table
products = {
"HPS3DR":"Cube",
"HPS3DB":"Cube",
"HPS3DRR":"RebinnedCube",
"HPS3DRB":"RebinnedCube",
"HPS3DPR":"ProjectedCube",
"HPS3DPB":"ProjectedCube",
"HPS3DDR":"DrizzledCube",
"HPS3DEQDR":"EquidistantDrizzledCube",
"HPS3DIB":"InterpolatedCube",
"HPS3DEQIB":"EquidistantInterpolatedCube"}

#WARNING: This observation is identified as wavelength switching.
#WARNING: This function was neither validated nor even tested on such a case.
#OBSIDS += [1342189071] # n7023_fts
obsids = {'n7023E_fts_1': [1342225573, 1342225574, 1342225575], 
'rho_oph_fts_off_2': [1342227207], 
'n2023-PACS': [1342228458, 1342228459, 1342228460], 
'HD37022_towards': [1342226894, 1342226895, 1342226896, 1342226897], 
'Ced201-2': [1342225576, 1342225577, 1342225578], 
'rho_oph_fts_on': [1342227208], 
'IC59-S1': [1342228182], 
'IC59-S3': [1342228184], 
'IC59-S2': [1342228183], 
'rho_oph_h2': [1342227204, 1342227205, 1342227206], 
'IC59-S5': [1342228186], 
'IC59-S4': [1342228185], 
'HH_dense_core': [1342228511, 1342228512, 1342228513, 1342228514], 
'IC63': [1342227621, 1342227622, 1342227623, 1342227624], 
'HH_IR_int-2': [1342228507, 1342228508, 1342228509, 1342228510], 
'IC59': [1342228179, 1342228180, 1342228181], 
'HD37041': [1342191152]}

test = {}
for (object_name, obsid_list) in obsids.iteritems():
	
	
	for obsid in obsid_list:
		obs = getObservation(obsid, useHsa=True, instrument="PACS")
		retrieved_name = str(obs.meta['object'].value)
		retrieved_name = retrieved_name.replace(' ', '') # removing space in name
		retrieved_name = retrieved_name.replace('(', '_') # changing open parenthesis in name
		retrieved_name = retrieved_name.replace(')', '') # removing closing parenthesis in name
		
		if (object_name != retrieved_name):
			print("for key %s (obsid %d) metadata name (%s) doesn't match" % (object_name, obsid, retrieved_name))
		
		print("%s %d" % (retrieved_name, obsid))
		
		
		for (product_name, prod_description) in products.iteritems():
			
			# If product not present in observation, skip
			if (product_name not in obs.level2.refs.keySet()):
				continue
			
			# Something weird seems to be going on. For 62µm, there's either blue or red. Indeed, some *B and *R product exists with the same metadata wavelength
			color = None
			tmp = product_name[-1] # Last character of key is either R or B for Red or Blue
			if (tmp == 'R'):
				color = "red"
			elif (tmp == 'B'):
				color = "blue"
			
			product = obs.level2.refs[product_name].product
			
			
			# Sometimes, meanWave is not defined because there's several spectral lines. Thus we take the first line wavelenght. Indeed I didn't found how to retrieve the lines separately. I thought at first that the sub-objects (.refs[0].product would refer to the spectral line but it isn't the case at all. I found a case were there was 3 spectral line and .refs[1] didn't exist, there was only index 0)
			# We also want to retrieve the molecule targeted. But in "Line1", we also have the detail of the transitions levels. Which is problematic for 
			tmp = product.meta["Line1"].value
			tmp = tmp.replace(" ", "")
			tmp = tmp.split(",") # First is wavelength, second is repetition, third is molecule targeted
			wavelength = tmp[0].split('micron')[0] # In micro m
			molecule = tmp[2].split(":")[1]
			
			
			filename = "_".join([object_name, str(obsid), "L2", color, wavelength, molecule, prod_description])
			filename += ".fits"
		
			simpleFitsWriter(product=product.refs[0].product, file=os.path.join(outputDir, filename))
		
stop_time = time.time()
(m, s) = divmod(stop_time - start_time, 60)
(h, m) = divmod(m, 60)
print "Duration: %d:%02d:%02d" % (h, m, s)
