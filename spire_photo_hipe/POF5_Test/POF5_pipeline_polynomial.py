# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2011 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
#
# $Id: POF5_pipeline.py,v 1.96.2.1 2011/05/05 08:12:09 sguest Exp $
#
# Official SPG POF5 pipeline, i.e. the pipeline script executed 
# at ESAC to process SPIRE Large Map observations.
#
# This script can be used to reprocess Large Map observations although
# it contains many elements needed for the SPG processing that are 
# not needed for user reprocessing.
#
# Reprocessing scripts for general users are available via the HIPE
# Pipeline menu. Please look at these User Reprocessing Script for 
# examples of alternative and extra algorithms that may improve the
# quality of your data.
#
# When executed, the script opens a dialog window where the user can
# specify the level from which the reprocessing shall start and
# other parameters.
# The script assumes that the observation context to be processed
# is in a variable called "obs". If this variable is not present,
# a dialog window will be open and the user will be asked to provide
# an obsid and the name of the pool from which the observation context
# should be loaded.
#
# Author: Pasquale Panuzzo, CEA Saclay, Irfu/SAp, France
#
# History:
# Release 5.0: 
# - Comment out optical and electrical crosstalk corrections
# - Removed plotting blocks
# - Inserted time correlation task.
# - Inserted Concurrent glitch deglitcher
# - Inserted Signal Jump detector task as comment
# Release 6.0:
# - Signal Jump detector task uncommented
# Release 7.0:
# - Signal Jump detector use temperature drift correction calibration product
# - Patched for Track 7: comment in original version of Signal Jump detector

# Import all needed classes
from herschel.spire.all import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter
from herschel.ia.pal.pool.lstore.util import TemporalPool
from herschel.spire.ia.pipeline.phot.baseline import BaselineRemovalPolynomialTask

# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')
# Input definition
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *

# Import the script obsLoader.py that allows to load an ObservationContext from a storage.
from herschel.spire.ia.scripts.tools.obsLoader import *
#poolName="HD37041_SPIRE"
#obsid=1342192101L
#obs = getObs(poolName,obsid)
# Open the input dialog to enter inputs
#inputs.openDialog()
inputs.level   = 'level1'
#create a logger for the pipeline
logger=TaskModeManager.getMode().getLogger()

# Open a dialog to load the ObservationContext if "obs" is not defined.
#try:
#	obsid=obs.obsid
#except NameError:
#	loader=ObsLoader()
#	obs=loader.getObs().product
#pass

# Check that the data are really SPIRE data
if obs.instrument != "SPIRE": 
    raise BadDataException("This ObservationContext cannot be processed with this pipeline: it contains "+obs.instrument+" data, not SPIRE data")


# Shift of time origin for plots
t0=obs.startDate.microsecondsSince1958()*1e-6

# get and print the obsid
obsid=obs.obsid
print "processing OBSID=", obsid,"("+hex(obsid)+")"


cal = SpireCal.getInstance (ProductStorage ("spire_cal_7_0"))
# attach it to observation context
print "update calibration"
obs.calibration.update(cal)

# Extract from the ObservationContext the calibration products that
# will be used in the script
bsmPos=obs.calibration.phot.bsmPos
lpfPar=obs.calibration.phot.lpfPar
detAngOff=obs.calibration.phot.detAngOff
elecCross=obs.calibration.phot.elecCross
optCross=obs.calibration.phot.optCross
chanTimeConst=obs.calibration.phot.chanTimeConst
chanNum=obs.calibration.phot.chanNum
fluxConvList=obs.calibration.phot.fluxConvList
tempDriftCorrList=obs.calibration.phot.tempDriftCorrList

# Extract from the observation context the auxiliary products that
# will be used in the script
hpp=obs.auxiliary.pointing
siam=obs.auxiliary.siam
timeCorr=obs.auxiliary.timeCorrelation

# Set this to FALSE if you don't want to use the ProductSink
# and do all the processing in memory
tempStorage=Boolean.TRUE

# Initialize the ProductSink with a TemporalPool that will be removed when the
# HIPE session is closed, in case of interactive mode.
# The TemporalPool is created in a directory starting from the path defined by the
# var.hcss.workdir property. If this directory is inaccessible or not convenient, please
# change this property to a proper value.
if TaskModeManager.getType().toString() == "INTERACTIVE" and tempStorage:
    pname="tmp"+hex(System.currentTimeMillis())[2:-1]
    tmppool=TemporalPool.createTmpPool(pname,TemporalPool.CloseMode.DELETE_ON_CLOSE)
    ProductSink.getInstance().productStorage=ProductStorage(tmppool)
pass

# this is used to put in the creator metadata of level 1 and level 2 context the version of SPG or of the pipeline
# that was executed
creator=herschel.share.util.Configuration.getProperty("hcss.ia.dataset.creator", "$Revision: 1.96.2.1 $")


# From Level 0 to Level 0.5
if inputs.level=="level0":
	# Make Engineering conversion of level 0 products
	level0_5= engConversion(obs.level0,cal=obs.calibration, tempStorage=tempStorage)
	# Add the result to the observation in level 0.5
	obs.level0_5=level0_5
else:
	level0_5=obs.level0_5
	# promote to LEVEL0_5_PROCESSED
	obs.obsState = ObservationContext.OBS_STATE_LEVEL0_5_PROCESSED
pass

# set the progress
inputs.progress=20
# counter for computing progress
count=0

# From Level 0.5 to Level 1
if inputs.level=="level0" or inputs.level=="level0_5":
	# Create Level1 context
	level1=Level1Context(obsid)
	for key in level0_5.meta.keySet():
		if key != "creator" and (not key.endswith("Date")) and key != "fileName" and \
		key != "type" and key != "description" and key != "level": 
			level1.meta[key]=level0_5.meta[key].copy()
	level1.creator=creator
	bbids=level0_5.getBbids(0xa103)
	nlines=len(bbids)
	print "number of scan lines:",nlines
	#
	# Loop over scan lines
	for bbid in bbids:
		block=level0_5.get(bbid)
		print "processing BBID="+hex(bbid)
		# Now move to engineering data products
		pdt  = block.pdt
		nhkt = block.nhkt
		if pdt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block."
			continue
		if nhkt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block."
			continue
		#
		# access and attach turnaround data to the nominal scan line
		bbCount=bbid & 0xFFFF
		pdtLead=None
		nhktLead=None
		pdtTrail=None
		nhktTrail=None
		if bbCount >1:
			blockLead=level0_5.get(0xaf000000L+bbCount-1)
			pdtLead=blockLead.pdt
			nhktLead=blockLead.nhkt
			if pdtLead != None and pdtLead.sampleTime[-1] < pdt.sampleTime[0]-3.0:
				pdtLead=None
				nhktLead=None
		if bbid < MAX(Long1d(bbids)):
			blockTrail=level0_5.get(0xaf000000L+bbCount)
			pdtTrail=blockTrail.pdt
			nhktTrail=blockTrail.nhkt
			if pdtTrail != None and pdtTrail.sampleTime[0] > pdt.sampleTime[-1]+3.0:
				pdtTrail=None
				nhktTrail=None
		pdt=joinPhotDetTimelines(pdt,pdtLead,pdtTrail)
		nhkt=joinNhkTimelines(nhkt,nhktLead,nhktTrail)
		#
		# calculate BSM angles
		bat=calcBsmAngles(nhkt,bsmPos=bsmPos)
		#
		# create the SpirePointingProduct
		spp=createSpirePointing(detAngOff=detAngOff,bat=bat,hpp=hpp,siam=siam)
		#
		# run signal jump detector
		# run signal jump detector
		tempDriftCorr=tempDriftCorrList.getProduct(pdt.meta["biasMode"].value,pdt.startDate)
		pdt=signalJumpDetector(pdt,windowWidth=20,gapWidth=1.0,gamma=6.0,kappa=2.0) 
		#
		# run electrical crosstalk correction
		# This crosstalk correction was commented out from the script because crosstalk was found
		# null or very small. The crosstalk coefficients are not yet properly measured so this task
		# with the current calibration product doesn't change the data.
		#pdt=elecCrossCorrection(pdt,elecCross=elecCross)
		#
		# run the deglitcher for concurrent glitches
		pdt=concurrentGlitchDeglitcher(pdt,chanNum=chanNum,kappa=2.0,size=15,correctGlitches=True)
		#
		# run the wavelet deglitch
		pdt=waveletDeglitcher(pdt, scaleMin=1.0, scaleMax=8.0, scaleInterval=7, holderMin=-3.0,\
			holderMax=-0.3, correlationThreshold=0.3, optionReconstruction='linearInterpolation',\
			reconstructionPointsBefore=1, reconstructionPointsAfter=6)
		#
		# run electrical Low Pass Filter response correction
		pdt=lpfResponseCorrection(pdt,lpfPar=lpfPar)
		#
		# run the flux conversion
		fluxConv=fluxConvList.getProduct(pdt.meta["biasMode"].value,pdt.startDate)
		pdt=photFluxConversion(pdt,fluxConv=fluxConv)
		#
		# run the temperature drift correction
		pdt=temperatureDriftCorrection(pdt,tempDriftCorr=tempDriftCorr)
		#
		# run bolometer time response correction
		pdt=bolometerResponseCorrection(pdt,chanTimeConst=chanTimeConst)
		#
		# run optical crosstalk correction
		# This crosstalk correction was commented out from the script because crosstalk was found
		# null or very small. The crosstalk coefficients are not yet properly measured so this task
		# with the current calibration product doesn't change the data.
		#pdt=photOptCrossCorrection(pdt,optCross=optCross)
		#
		# add pointing
		psp=associateSkyPosition(pdt,spp=spp)
		#
		# cut the timeline back to scan line range.
		# If you want include turnaround data in map making, call the following
		# task with the option "extend=True" or set includeTurnaround to 1 in the input dialog.
		psp=cutPhotDetTimelines(psp,extend=True)
		#
		# run time correlation task
		psp=timeCorrelation(psp,timeCorr)
		#
		# Store Photometer Scan Product in Level 1 product storage
		if tempStorage:
			ref=ProductSink.getInstance().save(psp)
			level1.addRef(ref)
		else:
			level1.addProduct(psp)
		#
		print "Completed BBID=0x%x (%i/%i)"%(bbid,count+1,nlines)
		# set the progress
		count=count+1
		inputs.progress = 20+(60*count)/nlines
	#
	if level1.count == 0:
		logger.severe("No scan line processed due to missing data. This observation CANNOT be processed!")
		print "No scan line processed due to missing data. This observation CANNOT be processed!"
		raise MissingDataException("No scan line processed due to missing data. This observation CANNOT be processed!")
	#
	obs.level1=level1
	# promote to LEVEL1_PROCESSED
	obs.obsState = ObservationContext.OBS_STATE_LEVEL1_PROCESSED
else:
	level1=obs.level1
pass
# Flag to switch on and off the baseline removal

print "completed OBSID=",obsid,"("+hex(obsid)+")"

try:
	loader.saveObs(obs)
except NameError:
	pass
	# do nothing
pass

inputs.progress=100

#### End of the script ####

