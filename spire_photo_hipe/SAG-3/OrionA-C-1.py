from process import *

storage, obsids = "OrionA-C-1_SPIRE" , [1342204098,1342204099]
print storage, obsids
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
