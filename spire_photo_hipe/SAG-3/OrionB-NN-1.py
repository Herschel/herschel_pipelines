from process import *

storage, obsids = "OrionB-NN-1" , [1342205074,1342205074]
print storage, obsids
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,useHsa=False,doPipeline=False, doTdf=False, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
