from process import *
obsids=[1342239930L]
storage="OriA_SPIRE"
process(poolName=storage,obsids=obsids,useHsa=False,poolBaseName="OriA",doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
process(poolName=storage,obsids=obsids,useHsa=False,poolBaseName="OriA",doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
