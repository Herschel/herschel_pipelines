from IAS_tools import *
from export import *
from process import *
obsids=[1342205093L,1342205094L]
storage="L1688_SPIRE"
#~ # Direct from HSA
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)

process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)
#Export L1 only
process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False) 

