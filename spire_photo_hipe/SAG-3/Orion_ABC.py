from process import *
storage = "Orion_ABC_SPIRE"
#field += ['OrionA-C-1','OrionA-S-1','OrionA-N-1']
#obsids_list = [[1342204098,1342204099],[1342205076,1342205077],[1342218967,1342218968]]

#field += ['OrionB-NN-1','OrionB-N-1','OrionB-S-1']
#obsids_list += [[1342205074,1342205075],[1342215982,1342215983],[1342215984,1342215985]]
obsids_list = [[1342215982,1342215983],[1342215984,1342215985]]

# PACS PHOTO !
#field += ['OrionA-SS-2','OrionA-S-2','OrionA-N-2','OrionA-C-2']
#~ obsids_list += [[1342228908, 1342228909],[1342228910, 1342228911],[1342206052, 1342206053],[1342218553, 1342218554]]
#field += ['OrionA-SS-2','OrionA-S-2','OrionA-N-2','OrionA-C-2']
#~ obsids_list = [[1342228908, 1342228909],[1342228910, 1342228911],[1342206052, 1342206053],[1342218553, 1342218554]]
#field += ['OrionB-S-2','OrionB-NN-2','OrionB-N-2','OrionB-C-2']
#~ obsids_list += [[1342206081, 1342206080],[1342206078, 1342206079], [1342206054, 1342206055], [1342218555, 1342218556]]

for obsids in obsids_list:
	print obsids
	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)
	#Export L1 only
	process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False) 
