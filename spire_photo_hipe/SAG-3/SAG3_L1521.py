from process import *


#obsids=[1342202254L,1342202090L,1342190616L]
obsids=[1342202254L,1342202090L]
storage="L1521_SPIRE"

process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)
#Export L1 only
process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False)



