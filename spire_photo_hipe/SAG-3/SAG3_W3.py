from process import *

##
obsids=[1342216019L,1342216020L]
storage="w3_SPIRE"

#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)

process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)
#Export L1 only
process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False) 
