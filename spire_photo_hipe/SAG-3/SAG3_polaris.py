from process import *

# relaunch all with a new version of Hipe

obsids=[1342186233L,1342186234L,1342198246L,1342198247L]
storage="polaris_SPIRE"
#process(useHsa=True,poolBaseName="polaris",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="polaris", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,obsids=obsids,poolBaseName="polaris",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)

#~ # Direct from HSA
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doDestriper=True, doLevel1=False)
#~ process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)

process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=False)
#Export L1 only
process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False) 
