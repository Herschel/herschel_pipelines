from process import *
obsids=[1342239931L]
storage="OriB_SPIRE"

process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doMapsFromHSA=True,doSavePool=False)
process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=False,doDestriper=True,doApplyExtendedEmissionGains=True,doSavePool=False)
