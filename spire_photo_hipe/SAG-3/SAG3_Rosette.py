from process import *

obsids=[1342186121L]
storage="Rosette_SPIRE"
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doMapsFromHSA=True,doSavePool=False)
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=True,doDestriper=True,doApplyExtendedEmissionGains=True,doSavePool=False,doExport=True)
#~ # to export L1 only
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doExport=True,doLevel1=True,doTheMaps=False)

process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False)
process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True)
# to export L1 only
process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False)

#~ #process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#~ #process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#~ #process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#~ #process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doMapsFromHSA=True,doSavePool=False)
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=False,doDestriper=True,doApplyExtendedEmissionGains=True,doSavePool=False)
