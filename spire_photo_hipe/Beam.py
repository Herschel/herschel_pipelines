execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

params = java.util.HashMap()
params.put('id', 'Beams')
Beams = herschel.ia.pal.managers.PoolCreatorFactory.createPool( 'lstore', params)
del(params)
storage = ProductStorage()
storage.register( Beams )
obs_1342187440 = importUfDirToPal(pool=Beams, dirin="/home/abeelen/herschel/data/Beam/raw_spire_beam_product", xml="1342187440-herschel.ia.obs.ObservationContext-5.xml")
obs = obs_1342187440.product


process('Beams',0x500027B0L,doPipeline=True,doTdf=True,doSavePool=True,doExport=True,doAll=True)
process('Beams',0x500027B0L,doPipeline=True,doTdf=False,doSavePool=True,doExport=True,doAll=True)

#indexLeg=range(obs.level1.count)
#suffix=''
#export2Sanepic(obs,indexLeg,suffix=suffix+'_all_sanepic.fits',dir='/data/glx-herschel/data1/herschel/FITS_result/')
#
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_notdf.py')
#suffix='_notdf'
#savePool(obs,suffix)
#indexLeg=range(obs.level1.count)
#export2Sanepic(obs,indexLeg,suffix=suffix+'_all_sanepic.fits',dir='/data/glx-herschel/data1/herschel/FITS_result/')
#saveMaps(obs,suffix)
