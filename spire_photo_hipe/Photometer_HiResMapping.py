clear(all=True)

band = "PLW"

#fitsName = "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fitsRelease/SAG-4/spica/SupremePhoto_spica_combined_PLW.fits"
fitsName = "/data/glx-herschel/data1/herschel/Release/R3_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-4/DC300-17/OD600_0x500088ccL_SpirePacsParallel_DC300-17_destriped_"+band+".fits"
supremeFits = fitsReader(fitsName)
try: obsids = [supremeFits.meta["obsid"].value]
except: obsids = supremeFits["obsids"].getColumn("obsids").data

#obsids = [1342200112,1342200113,1342200121,1342200122,1342211317,1342211318,1342222625,1342224030,1342224138]
#obsids = [1342200114,1342200115]
#obsids = [1342224959,1342224960]


###########################################################################

workDir = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/"
beamName = "0x5000241aL_%s_pmcorr_1arcsec_norm_beam.fits"%band
bcenter = {'PSW':(700,699), 'PMW':(700,700), 'PLW':(698,700)} # Positions of peak pixel
beamfull = fitsReader(file = os.path.join(workDir,beamName))
beam = crop(beamfull, int(bcenter[band][0] - 100) , int(bcenter[band][1] - 100), int(bcenter[band][0] + 101), int(bcenter[band][1] + 101))

assert beam.dimensions[0] % 2 == 1, "Beam first dimensions is not odd"
assert beam.dimensions[1] % 2 == 1, "Beam second dimensions is not odd"
assert beam.getIntensity(beam.dimensions[0] / 2, beam.dimensions[1] / 2) == MAX(NAN_FILTER(beam.image)), "Beam is not centred on central pixel"

level1Corrected = Level1Context()
raCov, decCov = [], []
for obsid in obsids:
	obs = getObservation(obsid, useHsa=True, instrument='SPIRE')
	wcsObsid = obs.level2.getProduct("psrc"+band).wcs.copy()
	print wcsObsid
	raCov.extend( [ wcsObsid.crval1 +(0-wcsObsid.crpix1)*wcsObsid.cdelt1 , wcsObsid.crval1 +(wcsObsid.naxis1-wcsObsid.crpix1)*wcsObsid.cdelt1 ] )
	decCov.extend( [ wcsObsid.crval2 +(0-wcsObsid.crpix2)*wcsObsid.cdelt2 , wcsObsid.crval2 +(wcsObsid.naxis2-wcsObsid.crpix2)*wcsObsid.cdelt2 ] )
	cdelt1, cdelt2 = wcsObsid.cdelt1, wcsObsid.cdelt2 
	for ref in obs.level1.refs: level1Corrected.addRef(ref)

minR, maxR = min(raCov), max(raCov)
minD, maxD = min(decCov), max(decCov)

imagesize = [ abs(maxD-minD)*60. , abs(maxR-minR)*60. ] #in arcmin
imagecenter = [ 0.5*(minR+maxR) , 0.5*(minD+maxD) ]

#supremeFits = fitsReader("/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fitsRelease/SAG-4/spica/SupremePhoto_spica_combined_PLW.fits")
#superFits = fitsReader( "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fitsRelease/SAG-4/spica/spica_combined_HiResPLW.fits")

#wcs = supremeFits.wcs
#imagesize = [ wcs.naxis2*abs(wcs.cdelt2)*60, wcs.naxis1*abs(wcs.cdelt1)*60 ] #in arcmin
#imagecenter = [wcs.crval1+(wcs.naxis1/2-wcs.crpix1)*wcs.cdelt1, wcs.crval2+(wcs.naxis2/2-wcs.crpix2)*wcs.cdelt2]

#wcs = obs.level2.getProduct("psrc"+band).wcs.copy()
wcs = Wcs()
wcs.ctype1, wcs.ctype2 = "RA---TAN", "DEC--TAN"
wcs.equinox, wcs.crota2 = 2000.0, 0.0
wcs.crval1, wcs.crval2 = imagecenter[0], imagecenter[1]
wcs.cdelt1, wcs.cdelt2 = 0.5*cdelt1, 0.5*cdelt2
wcs.naxis1, wcs.naxis2 = int(imagesize[1]/60./abs(wcs.cdelt1) + 0.5), int(imagesize[0]/60./abs(wcs.cdelt2) + 0.5)
wcs.crpix1, wcs.crpix2 = (wcs.naxis1 + 1) / 2., (wcs.naxis2 + 1) / 2.

print wcs
print supremeFits.wcs

#level1Corrected = destriper(level1Corrected, array=band, useSink=True)[0]
#hiresImage, hiresBeam = hiresMapper(level1Corrected, beam=beam, wcs=wcs, maxIter=20)
#
############################################################################
#
#object = obs.meta["object"].value
#if len(obsids)==1: fitsNameHiRes = "OD"+str(obs.meta["odNumber"].value)+"_"+str(hex(obsids[0]))+"_HiRes_"+object+"_"+band+".fits"
#else: fitsNameHiRes = object+"_combined_HiRes"+band+"_fromObs.fits"
#dirFits = fitsName.split(fitsName.split("/")[-1])[0]
#simpleFitsWriter(hiresImage,dirFits+fitsNameHiRes)
