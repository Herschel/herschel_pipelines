from process import *
import sys

storage="DDT_mustdo_4_SPIRE"
obsids=[
1342251952L,\
1342253431L,\
1342253432L,\
1342254513L,\
1342254066L,\
1342255085L,\
1342256740L,\
1342256742L,\
1342256798L,\
1342258370,\
1342258392,\
1342258401,\
1342259397,\
1342259409,\
1342207041,\
1342234756,\
1342261665,\
1342261672,\
1342265381
]


#~ process(useHsa=True,poolBaseName="DDT_mustdo_4",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(useHsa=True,poolBaseName="DDT_mustdo_4",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(poolName=storage,poolBaseName="DDT_mustdo_4",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#~ #process(poolName=storage,poolBaseName="DDT_mustdo",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCombine=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="DDT_mustdo_4", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="DDT_mustdo_4", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="DDT_mustdo_4", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
