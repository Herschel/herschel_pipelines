execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')


storage="polaris_orig"
#process(storage, doPipeline=False,doPipelineTest=True, doTdf=True, doSavePool=True, doExport=True, doAll=False)
#process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=True, doExport=True, doAll=False)
#process(storage, doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=True, doExport=True, doAll=False,doBaseline=True)
process(storage, doPipeline=False, doPipelineTest=True,doTdf=False, doSavePool=True, doExport=True, doAll=False,doBaseline=True)
#process(storage,doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=False, doExport=True, doAll=False,doRemoveBaselineForExport=True)

## Only export the notdf (test of the masks)
#suffix='_notdf'
#for poolname in ["polaris","polaris-1","polaris-2"]:
#	process(poolname+suffix,doSavePool=False,doExport=True,doMaps=False)
#
## Combine all the obsid together (use the good map so suffix='')
#suffix=''
#
#poolnames=["polaris","polaris-1","polaris-2"]
#level2_combined=combineMaps(poolnames)
#saveMaps(level2_combined,suffix+'_combined',poolnames)
#
#wcsPSW = level2_combined.refs["PSW"].product.wcs
#wcsPMW = level2_combined.refs["PMW"].product.wcs
#wcsPLW = level2_combined.refs["PLW"].product.wcs
#
#del(level2_combined)
#
#for poolname in poolnames:
#	obsIds = getObsIds(poolname)
#	
#	for obsId in obsIds:
#		obs = reMap(obsId,poolname, wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,doCreateRGB=0)
#		level2=obs.level2
#		saveMaps(level2,suffix)
#		del(obs,level2)
#
### Test of a polynomial removal on the full scans at mapmaking stage....
#
#suffix=''
#poolnames=["polaris","polaris-1","polaris-2"]
#scans = SpireListContext()
#for poolname in poolnames:
#	obsIds = getObsIds(poolname)
#	for obsid in obsIds:
#		scans = getAllLegs(scans, obsid, poolname, doRemoveBaselinePerLeg=False)
#
#mapPsw=naiveScanMapper(scans, array="PSW",wcs=wcsPSW, mergingMaximumGap=200, baselinePolynomialOrder = 20, doInterpolation=True)

# Maps for only the first two obsids
obsIds = [1342186233L, 1342186234L]
scans = SpireListContext()
for obsid in obsIds:
	scans = getAllLegs(scans, obsid, "polaris_SPIRE_notdf")

mapPSW_notdf = naiveScanMapper(scans, array="PSW")
mapPMW_notdf = naiveScanMapper(scans, array="PMW")
mapPLW_notdf = naiveScanMapper(scans, array="PLW")

scans = SpireListContext()
for obsid in obsIds:
	scans = getAllLegs(scans, obsid, "polaris_SPIRE")

mapPSW = naiveScanMapper(scans, array="PSW")
mapPMW = naiveScanMapper(scans, array="PMW")
mapPLW = naiveScanMapper(scans, array="PLW")

simpleFitsWriter(product=mapPLW,file="/home/abeelen/polaris_PLW.fits")
simpleFitsWriter(product=mapPMW,file="/home/abeelen/polaris_PMW.fits")
simpleFitsWriter(product=mapPSW,file="/home/abeelen/polaris_PSW.fits")

simpleFitsWriter(product=mapPLW_notdf,file="/home/abeelen/polaris_PLW_notdf.fits")
simpleFitsWriter(product=mapPMW_notdf,file="/home/abeelen/polaris_PMW_notdf.fits")
simpleFitsWriter(product=mapPSW_notdf,file="/home/abeelen/polaris_PSW_notdf.fits")
