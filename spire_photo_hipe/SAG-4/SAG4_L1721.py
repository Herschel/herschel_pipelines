execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

#obsid =0x50001900L
#storage="OD121_0x50001900L_SpirePhotoLargeScan_n7023_int"
storage="l1721-5_orig"
#process(storage,  doPipeline=False, doPipelineTest=True,doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=False)
process(storage,  doPipeline=True, doTdf=True, noTimeRespCorr=False,doSavePool=True, doExport=True, doAll=True)
process(storage,  doPipeline=True, doTdf=False, noTimeRespCorr=False,doSavePool=True, doExport=True, doAll=True)

#listpool=["S3"]

#level2=combineMaps(listpool)
#saveMaps(level2,'_combined')
