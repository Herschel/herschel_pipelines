from IAS_tools import *
from export import *
from process import *



#storage="IC59-centre_orig"
storage="IC59-centre_SPIRE"
process(storage, doPipeline=False,doPipelineTest=False, doTdf=True, doSavePool=False, doExport=True, doAll=True)
#process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
