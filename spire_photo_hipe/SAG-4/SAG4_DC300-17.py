execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')



#storage="DC300-17_orig"
storage="DC300-17_SPIRE"
#process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipelineTest=True,doPipeline=False, doTdf=True, doSavePool=True, doExport=False, doAll=True,doRmBaselinePerLeg=False,doPolynomial=True)
#process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
