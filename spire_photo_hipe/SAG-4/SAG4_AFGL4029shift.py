from IAS_tools import *
from export import *
from process import *

#obsid =0x50001900L
#storage="OD121_0x50001900L_SpirePhotoLargeScan_n7023_int"
storage="AFGL4029shift_SPIRE_orig"
#process(storage,  doPipeline=False, doPipelineTest=True,doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=False)
process(storage,  doPipeline=True, doTdf=True, noTimeRespCorr=False,doSavePool=True, doExport=True, doAll=True)
process(storage,  doPipeline=True, doTdf=False, noTimeRespCorr=False,doSavePool=True, doExport=True, doAll=True)

#listpool=["S3"]

#level2=combineMaps(listpool)
#saveMaps(level2,'_combined')
