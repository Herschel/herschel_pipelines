from IAS_tools import *
from export import *
from process import *

obsids=[1342192100L]
storage="HH_dense_core_SPIRE"
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doPolynomial=True,doApplyExtendedEmissionGains=False,doDestriper=False)
storage="HH_dense_core_SPIRE_notdf"
process(poolName=storage,useHsa=False,doPipeline=False, doTdf=False, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=False,doTheMaps=False)
