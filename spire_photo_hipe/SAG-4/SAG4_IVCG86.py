execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')



storage="IVCG86_orig"
#process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=False,doPipelineTest=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
suffix=''
obsIds = getObsIds("IVCG86")
listpool=["IVCG86"]
level2=combineMaps(listpool)
saveMaps(level2,suffix+'_combined')
del(level2)
