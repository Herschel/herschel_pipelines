execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')



storage="polaris-1_orig"
#process(storage, doPipeline=False,doPipelineTest=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
storage="polaris-2_orig"
#process(storage, doPipeline=False,doPipelineTest=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=True, doExport=True, doAll=False)
#process(storage, doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=True, doExport=True, doAll=False,doRemoveBaselineForExport=True)
#process(storage,doPipeline=False, doPipelineTest=True,doTdf=True, doSavePool=False, doExport=True, doAll=False,doRemoveBaselineForExport=True)


#suffix=''
#obsIds = getObsIds("polaris")
#poolname=["polaris"]
#level2_combined=combineMaps(poolname)
#saveMaps(level2_combined,suffix+'_combined')

#wcsPSW = level2_combined.refs["PSW"].product.wcs
#wcsPMW = level2_combined.refs["PMW"].product.wcs
#wcsPLW = level2_combined.refs["PLW"].product.wcs

#for obsId in obsIds:
	#level2 = reMap([obsId],poolname[0], wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW)
	#saveMaps(level2,suffix+'_combined_single')

#del(level2)
