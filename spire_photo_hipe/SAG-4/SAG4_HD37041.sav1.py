execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py')
storage="HD37041_SPIRE_orig"
#storage="HD37041_SPIRE"
#storage="HD37041_notdf"
#process(storage, doPipeline=False, doTdf=False, doSavePool=False, doExport=True, doAll=True)
#process(storage, doPipelineTest=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,\
##doAll=False,doMaps=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
storage="HD37041_SPIRE"
process(storage, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True)
process(storage, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)

