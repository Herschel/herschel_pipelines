from IAS_tools import *
from export import *
from process import *

#RCW120 (SDP)
obsids=[1342183678L]
storage="rcw120_SPIRE"
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)

