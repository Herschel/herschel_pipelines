
storage="california-1_orig"
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
storage="california-3_orig"
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
storage="california-2_orig"
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)


## Combine all the obsid together (use the good map so suffix='')
#suffix='_chooseBolo'
#
poolnames=["california-1","california-2","california-3"]
#obs=getObs("polaris", 0x500022F9L)
#allchannels=obs.level1.getProduct(0).channelNames
#listChan_given=getRandomChannel(obs)
#listChan_given_part2=[]
#for mychan in allchannels:
#       if mychan not in listChan_given:
#               listChan_given_part2.append(mychan)
#
#print listChan_given_part2
#print listChan_given
#level2_combined=combineMaps(poolnames,listChan_given=listChan_given)
level2_combined=combineMaps(poolnames)
#suffix='_chooseBolo_part1'
saveMaps(level2_combined,'_combined',poolnames)
#
wcsPSW = level2_combined.refs["PSW"].product.wcs
wcsPMW = level2_combined.refs["PMW"].product.wcs
wcsPLW = level2_combined.refs["PLW"].product.wcs

for poolname in poolnames:
        obsIds = getObsIds(poolname)

        for obsId in obsIds:
                level2 = reMap(obsId,poolname, wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,doCreateRGB=0)
                saveMaps(level2)
                del(level2)
