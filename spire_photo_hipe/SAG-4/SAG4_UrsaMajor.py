execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')



storage="UrsaMajor_orig"
process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
