
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

dir='/data/glx-herschel/data1/herschel/data/SMPVT/OD145/GOODS'
string='OD145_0x50002040_SpirePhotoLargeScan_GOODS-N'

transformPV(dir,string,doAll=True)
transformPV(dir,string,doAll=False)

# two halves..

from os import path 
import glob

filelist=glob.glob(path.join(dir,"*"+string+"*"))
size = len(filelist)

first = filelist[0:size/2-1]
second = filelist[size/2:]
export2Sanepic_from_fits(first,suffix="_first_sanepic.fits")
export2Sanepic_from_fits(second,suffix="_second_sanepic.fits")
