
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/L1fitstoL2.py')
toProcess = []
toProcess.append({ 'obsid': '0x500026A2', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD192/'})
toProcess.append({ 'obsid': '0x500026A3', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD192/'})
toProcess.append({ 'obsid': '0x50004C95', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD374/'})
toProcess.append({ 'obsid': '0x50004C96', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD374/'})
toProcess.append({ 'obsid': '0x50004E22', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD380/'})
toProcess.append({ 'obsid': '0x50004E23', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD380/'})
toProcess.append({ 'obsid': '0x50004E3B', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD381/'})
toProcess.append({ 'obsid': '0x50004E3C', 'dir': '/data/glx-herschel/data1/herschel/data/SPIRE/H-ATLAS/ATLAS_Gama9_L1_v46dHP_fitsL1/OD381/'})

pseudoPoolnames = []
for index in range(len(toProcess)):
	pseudoPoolnames.append(toProcess[index]['obsid'])

level2_combined=multiL1fitstoL2(toProcess)

obsids = TableDataset()
obsids["obsids"] = Column(String1d(pseudoPoolnames),description="obsids")
level2_combined["obsids"] = obsids

saveMaps(level2_combined,'_combined',pseudoPoolnames)

wcsPSW = level2_combined.refs["PSW"].product.wcs
wcsPMW = level2_combined.refs["PMW"].product.wcs
wcsPLW = level2_combined.refs["PLW"].product.wcs

del(level2_combined)

for index in range(len(toProcess)):
	level2 = multiL1fitstoL2([toProcess[index]], wcsPSW=wcsPSW, wcsPMW=wcsPMW, wcsPLW=wcsPLW)
	saveMaps(level2)
	del(level2)
