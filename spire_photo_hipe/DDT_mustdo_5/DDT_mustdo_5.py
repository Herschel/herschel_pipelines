from process import *
import sys
### No more Bad obsidis to handle. Hipe does it now.

storage="DDT_mustdo_5_SPIRE"
obsids=[
1342263858,\
1342263857,\
1342263855,\
1342263854,\
1342263849,\
1342263848,\
1342263807,\
1342261701,\
1342261693,\
1342261692,\
1342261690,\
1342261689,\
1342261688,\
1342261667,\
1342261644,\
1342261637,\
1342261636,\
1342261634,\
1342261633,\
1342261631,\
1342261630,\
1342261628,\
1342261627,\
1342261626,\
1342261625,\
1342261621,\
1342261614,\
1342261592,\
1342261591,\
1342261589,\
1342261588,\
1342261586,\
1342261585,\
1342261584,\
1342261582,\
1342261581,\
1342261580,\
1342261577,\
1342261574,\
1342261573,\
1342261570,\
1342261565,\
1342261564,\
1342261556,\
1342261551,\
1342261547,\
1342261529,\
1342261528,\
1342261505,\
1342261504,\
1342261502,\
1342261498,\
1342261491,\
1342259478,\
1342259472,\
1342259470,\
1342259469,\
1342259458,\
1342259457,\
1342259454,\
1342259450,\
1342259447,\
1342259443,\
1342259434,\
1342259432,\
1342259430,\
1342259428,\
1342259414,\
1342259411,\
1342259404,\
1342259402,\
1342259389,\
1342259388,\
1342259387,\
1342258427,\
1342258399,\
1342258391,\
1342258388,\
1342258379,\
1342258377,\
1342258376,\
1342258374,\
1342257377,\
1342257376,\
1342257375,\
1342257374,\
1342257367,\
1342256893,\
1342256890,\
1342256885,\
1342256883,\
1342256877,\
1342256873,\
1342256847,\
1342256843,\
1342256841,\
1342256837,\
1342256819,\
1342256816,\
1342256814,\
1342256813,\
1342256812,\
1342256810,\
1342256808,\
1342256797,\
1342256753,\
1342256745,\
1342256743,\
1342256736,\
1342256734,\
1342256730,\
1342256729,\
1342256724,\
1342256705,\
1342256703,\
1342256695,\
1342256694,\
1342256692,\
1342256676,\
1342256659,\
1342256656,\
1342256646,\
1342256643,\
1342256641,\
1342256637,\
1342255228,\
1342255211,\
1342255208,\
1342255190,\
1342255185,\
1342255172,\
1342255164,\
1342255120,\
1342255113,\
1342255100,\
1342255091,\
1342255064,\
1342254640,\
1342254638,\
1342254505,\
1342254504,\
1342254503,\
1342254495,\
1342254467,\
1342254058,\
1342254091,\
1342193017,\
1342267742,\
1342267751,\
1342267752,\
1342268369,\
1342258376,\
1342266664,\
1342266682,\
1342265354,\
1342265359,\
1342265362,\
1342265366,\
1342265367,\
1342265372,\
1342265373,\
1342265374,\
1342265378,\
1342265398,\
1342265399,\
1342265402,\
1342270216,\
1342270220,\
1342270223,\
1342266661,\
1342266663,\
1342265336,\
1342265394,\
1342265395,\
1342265400,\
1342265403,\
1342265404,\
1342265405

]

storage="DDT_mustdo_5_SPIRE"

#~ #process(useHsa=True,poolBaseName="DDT_mustdo_5",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(useHsa=True,poolBaseName="DDT_mustdo_5",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(poolName=storage,poolBaseName="DDT_mustdo_5",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#~ process(poolName=storage,poolBaseName="DDT_mustdo_5",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=True, doExport=False, doCombine=False,doLevel1=False,doNoise=True,doDestriper=True, doSaveMaps=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
