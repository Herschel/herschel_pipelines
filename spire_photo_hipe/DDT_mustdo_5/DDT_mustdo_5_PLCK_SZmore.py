from process import *

storage="DDT_mustdo_5_SPIRE"
obsids=[1342265380,1342265376,1342265360,1342265353,1342265341,1342265334,1342265326,1342266674,1342266647]


# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="DDT_mustdo_5", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])

