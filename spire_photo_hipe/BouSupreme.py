print "Installing SupremePhotoGetDataFromObs... ",
from herschel.share.unit import Angle
from herschel.ia.obs.auxiliary.pointing import PointingUtils
from herschel.share.fltdyn.time import FineTime
from herschel.spire.ia.util import SpireMask
from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.toolbox.util import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from herschel.spire.ia.pipeline.phot.fluxconv import *
from herschel.ia.obs.util import ObsParameter
from java.util import List

from java.lang import *
from java.util import *
import time

class SupremePhotoGetDataFromObs(Task):

    def __init__(self,name = "supremePhotoGetDataFromObs"):

        version = Configuration.getProjectInfo().getVersion().split('.')
        if int(version[0]) > 5:
            self.setDescription("This task gets all the data and information from a list of observation IDs (in the local "+\
            "pool or from the HSA) that will be used in the SupremePhoto task. The list of observation IDs and the band to process must be given."+\
            "You can use either a poolname or useHSA to retrieve the observations.")

        #Input
        p = TaskParameter("obsids",valueType = List, mandatory = 1,\
                description = "List of obsids.")
        self.addTaskParameter(p)
        p = TaskParameter("bandName",valueType = String, mandatory = 1,\
                description = "Band to process.")
        self.addTaskParameter(p)
        p = TaskParameter("poolName",valueType = String, mandatory = 0,\
                defaultValue='', \
                description = "Name of pool from where the observations are loaded.")
        self.addTaskParameter(p)
        p = TaskParameter("useHsa",valueType = Boolean, mandatory = 0,\
                defaultValue=False, \
                description = "UseHSA if you want to get the observations from Herschel Science Archive.")
        self.addTaskParameter(p)
        p = TaskParameter("doApplyExtendedEmissionGains",valueType = Boolean, mandatory = 0,\
                defaultValue=False, \
                description = "Apply extended emission gain task.")
        self.addTaskParameter(p)
        p = TaskParameter("doBaseline",valueType = Boolean, mandatory = 0,\
                defaultValue=False, \
                description = "Apply baseline removal task.")
        self.addTaskParameter(p)
        p = TaskParameter("doDestripe",valueType = Boolean, mandatory = 0,\
                defaultValue=False, \
                description = "Apply destripe task.")
        self.addTaskParameter(p)
        #Output
        p = TaskParameter("data",valueType = ListContext,type = OUT,\
                description = "ListContext containing all the data needed by SupremePhoto.")
        self.addTaskParameter(p)
        __all__=['SupremePhotoGetDataFromObs']

    def execute(self):

        dataObsList = ListContext()

        obsids = self.obsids
        poolName = self.poolName
        useHsa = self.useHsa
        bandName = self.bandName
        doDestripe = self.doDestripe
        doApplyExtendedEmissionGains = self.doApplyExtendedEmissionGains
        doBaseline = self.doBaseline

        dataObsList = ListContext()

        for i in range(len(obsids)):
            print "Preparing data from obsid "+str(obsids[i])
            obsid = obsids[i]
            if useHsa: obs = getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
            else: obs = getObservation(obsid=obsid,poolName=poolName,instrument="SPIRE")

            scans = obs.level1

            if doDestripe:
                print "Destriping... ",
                destripedProduct = destriper(scans, array=bandName, useSink=True,nThreads=4)
                scans = destripedProduct[0].copy()
                mapDestriped = destripedProduct[1].copy()

            indexLeg = range(scans.count)

            metaMain = obs.meta

            if i==0:
                metaMultipleObs = metaMain.copy()
            else:
                for key in metaMain.keySet():
                    if metaMultipleObs.containsKey(key)==True and metaMultipleObs[key].value!=metaMain[key].value:
                        description = metaMain[key].description
                        value = str(metaMultipleObs[key].value)+" & "+str(metaMain[key].value)
                        try:
                            unit = metaMain[key].unit
                            metaMultipleObs[key] = StringParameter(value, description, unit)
                        except:
                            metaMultipleObs[key] = StringParameter(value, description)


            print "Retrieving good channels... ",
            chanMask = obs.calibration.phot.chanMask
            chanNum = obs.calibration.phot.chanNum
            chanRelGains = obs.calibration.phot.chanRelGain
            oneLine = scans.getProduct(indexLeg[0])
            goodChan = []
            #  TODO See also spire/ia/pipeline/util/AbstractMapperTask.java (ISSLOW & ISNOCHOPSKY)
            for chan in oneLine.channelNames:
                conGoodChan = not chanMask.isDead(chan) and not chanMask.isNoisy(chan) and not chanMask.isSlow(chan) and chanNum.isBolometer(chan) and not chanNum.isDark(chan)
                if conGoodChan and chan.count(bandName)>0: goodChan.append(chan)
            goodChan.sort()
            nChan = len(goodChan)

            print " ... Building signal and mask"
            signals={}
            masks={}
            FlagTurnOvers=[]

            oneLine = scans.getProduct(indexLeg[0])
            startNomTime = (oneLine.meta["startNominalScanLine"].value.microsecondsSince1958())/1E6
            endNomTime = (oneLine.meta["endNominalScanLine"].value.microsecondsSince1958())/1E6

            if doBaseline and not doDestripe:
                print " ... Removing Baseline ..."
                oneLine = removeBaseline(oneLine,chanNum=chanNum)

            if doApplyExtendedEmissionGains:
                print "Apply relative gains for bolometers for better extended maps"
                oneLine.meta['type'].value="PSP"  # Necessary for pre-HCSS 8.0 processed observations
                applyRelativeGains = ApplyRelativeGainsTask()
                oneLine = applyRelativeGains(oneLine, chanRelGains)

            sampleTime = oneLine.sampleTime
            for j in range(oneLine.sampleTime.length()):
                    if oneLine.sampleTime[j]<startNomTime or oneLine.sampleTime[j]>endNomTime: FlagTurnOvers.append(1)
                    else: FlagTurnOvers.append(0)

            for chan in goodChan:
                signals[chan] = oneLine.getSignal(chan)
                masks[chan] = oneLine.getMask(chan)

            for i in indexLeg[1:]:
                oneLine=scans.getProduct(i)
startNomTime=(oneLine.meta["startNominalScanLine"].value.microsecondsSince1958())/1E6
endNomTime=(oneLine.meta["endNominalScanLine"].value.microsecondsSince1958())/1E6
                #print startNomTime
                #print oneLine.sampleTime[0]
                for j in range(oneLine.sampleTime.length()):
                    if oneLine.sampleTime[j]<startNomTime or oneLine.sampleTime[j]>endNomTime: FlagTurnOvers.append(1)
                    else: FlagTurnOvers.append(0)

                if doBaseline:
                    oneLine = removeBaseline(oneLine,chanNum=chanNum)
                if doApplyExtendedEmissionGains:
                    oneLine.meta['type'].value="PSP"  # Necessary for pre-HCSS 8.0 processed observations
                    applyRelativeGains = ApplyRelativeGainsTask()
                    oneLine = applyRelativeGains(oneLine, chanRelGains)
                sampleTime.append(oneLine.sampleTime)
                for chan in goodChan:
                    signals[chan].append(oneLine.getSignal(chan))
                    masks[chan].append(oneLine.getMask(chan))

             # Tranform them to 2d arrays channel vs time
            FlagTurnOver=Short1d(sampleTime.length())
            for i in range(sampleTime.length()):
                FlagTurnOver[i]=FlagTurnOvers[i]
            pass
            sign=Double2d(nChan,sampleTime.length())
            mask=Short2d(nChan,sampleTime.length())
            for i in range(nChan):
                iSig = signals[goodChan[i]].to().double1d()
                iMask = masks[goodChan[i]]

                # TODO: see spire/ia/pipeline/util/AbstractMapperTask.java

                bad = iMask.where(SpireMask.MASTER | \
                SpireMask.GLITCH_FIRST_LEVEL_UNCORR | \
                SpireMask.GLITCH_SECOND_LEVEL_UNCORR | \
                SpireMask.GLITCH_FIRST_LEVEL | \
                SpireMask.GLITCH_SECOND_LEVEL | \
                SpireMask.TRUNCATED_UNCORR | \
                SpireMask.ISDEAD | \
                SpireMask.ISNOISY | \
                SpireMask.ISSLOW )

                # This is faulty on some mask... (maskVoltageOol and so on...)
                # So take the inverse (from 2.147. FixedMask )

                good = iMask.where(~SpireMask.MASTER.isSet(iMask) & \
                ~SpireMask.GLITCH_FIRST_LEVEL_UNCORR.isSet(iMask) & \
                ~SpireMask.GLITCH_SECOND_LEVEL_UNCORR.isSet(iMask) &  \
                ~SpireMask.GLITCH_FIRST_LEVEL.isSet(iMask) & \
                ~SpireMask.GLITCH_SECOND_LEVEL.isSet(iMask) & \
                ~SpireMask.TRUNCATED_UNCORR.isSet(iMask) & \
                ~SpireMask.ISDEAD.isSet(iMask) & \
                ~SpireMask.ISNOISY.isSet(iMask) & \
                ~SpireMask.ISSLOW.isSet(iMask) )

                if bad.length() > 0: iMask[bad] = 1
                if good.length() > 0: iMask[good] = 0
                bad = iSig.where(~IS_FINITE(iSig))
                if len(bad.toInt1d()) > 0: iMask[bad] = 1

                sign[i,:] = iSig
                mask[i,:] = iMask

                del(iSig,iMask)
            del(signals,masks)
            del(chanMask, chanNum)
            del(FlagTurnOvers)

            print "Building ra & dec..."
            ras={}
            decs={}
            oneLine=scans.getProduct(indexLeg[0])
            # Retrieve the unit (save it for later)
            unitRa   = oneLine["ra"][goodChan[0]].unit
            unitDec  = oneLine["dec"][goodChan[0]].unit
            unitTime = oneLine["ra"]["sampleTime"].unit
            for chan in goodChan:
                ras[chan]=oneLine.getRa(chan)
                decs[chan]=oneLine.getDec(chan)
            for i in indexLeg[1:]:
                oneLine=scans.getProduct(i)
                for chan in goodChan:
                    ras[chan].append(oneLine.getRa(chan))
                    decs[chan].append(oneLine.getDec(chan))
            del(oneLine)

            # Tranform them to 2d arrays channel vs time
            ra=Double2d(nChan,sampleTime.length())
            dec=Double2d(nChan,sampleTime.length())
            for i in range(nChan):
                ra[i,:]=ras[goodChan[i]]
                dec[i,:]=decs[goodChan[i]]
            del(ras,decs)

            print "Retrieving channels list..."
            channels = TableDataset()
            channels["name"] = Column(String1d(goodChan))

            dataKept = Product(creator="HIPE", instrument="SPIRE", type="Product for SupremePhoto", description="Data for SupremePhoto - obsid "+str(obsid))
            dataKept["signal"] = ArrayDataset(sign)
            dataKept["ra"] = ArrayDataset(ra)
            dataKept["dec"] = ArrayDataset(dec)
            dataKept["mask"] = ArrayDataset(mask)
            dataKept["time"] = ArrayDataset(sampleTime)
            dataKept["channels"] = channels
            dataKept["turnover"] = ArrayDataset(FlagTurnOver)
            dataKept.meta["ra_nom"] = obs.meta["raNominal"]
            dataKept.meta["dec_nom"] = obs.meta["decNominal"]
            dataKept.meta["posAngle"] = obs.meta["posAngle"]
            dataKept.meta["object"] = obs.meta["object"]
            dataKept.meta["obsid"] = obs.meta["obsid"]

            dataObsList.refs.add(ProductRef(dataKept))
            if doDestripe:
                dataObsList.refs.add(ProductRef(mapDestriped))
                dataObsList.refs.add(ProductRef(scans))

        #return dataObsList
        waveDic = {"PSW":250.0,"PMW":350.0,"PLW":500.0}
        metaMultipleObs["wavelength"] = DoubleParameter(waveDic[bandName])
        metaMultipleObs["band"] = StringParameter(bandName)
        if doDestripe: metaMultipleObs["option1"] = StringParameter("Destriped L1")
        if doBaseline: metaMultipleObs["option2"] = StringParameter("Baseline Removed")
        if doApplyExtendedEmissionGains: metaMultipleObs["option3"] = StringParameter("Extended Emission Gains applied")

        dataObsList.meta = metaMultipleObs.copy()
        self.data = dataObsList


# Registering task in HIPE session
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremePhotoGetDataFromObs(),[Category.SPIRE])
del(toolRegistry)

print "SupremePhotoMapMakingTask...",
from pluginSupremePhoto import SupremePhotoMapMakingTask
toolRegistry = TaskToolRegistry.getInstance()
toolRegistry.register(SupremePhotoMapMakingTask(), [Category.SPIRE])
del(toolRegistry)

print "Done !"

