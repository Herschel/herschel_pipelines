from process import *

obsidsInfo = []
obsidsInfo.append(["MRC_1138-262" , [1342248476, 1342248477, 1342248478, 1342248479, 1342248480] ])

for info in obsidsInfo:
	storage, obsids = info[0]+"_SPIRE", info[1]
	print storage, obsids
	#process(useHsa=True,obsids=obsids,poolBaseName="MRC_1138-262",doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=False, doTheMaps=False)
	process(poolName=storage,poolBaseName="MRC_1138-262",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=False,doApplyExtendedEmissionGains=False,doDestriper=True)
