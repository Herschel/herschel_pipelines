#===============================================================================
# Destriper Function for Parallel Mode Observations
#
# Destripe Level 1 from a combination of two observation contexts
# and create new observation context with combined old Level1, 
# destriped as level1.5 and maps as level2
#
# Input: 
# 	obs_1		first observation context with input data
# 	obs_2		second observation context with input data
# Output:
#	obs		observation context with old level 1, destriped 
#                       level 1.5 and destriped maps in level 2
#
# Edition History
# 2011-07-12 B. Schulz initial test version for HIPE 8.0
# 2011-07-12 B. Schulz HIPE 7.0 version
#
#===============================================================================


def destripePmodeObs(obs_1,obs_2):
  #
  #---------------------------------------------------------------------
  #Make new observation context
  obs = ObservationContext()       #new observation context
  obs.meta = obs_1.meta         #copy meta data
  obs.auxiliary = obs_1.auxiliary #link auxiliary context
  #
  #---------------------------------------------------------------------
  #Make new combined level1 context
  cmbLevel1 = Level1Context()
  #
  level1 = obs_1.refs["level1"].product
  #
  for key in level1.meta.keySet():
    cmbLevel1.meta[key]=level1.meta[key].copy()
  #
  for i in range(level1.count):
    cmbLevel1.addRef(level1.refs[i])
  #
  level1 = obs_2.refs["level1"].product
  for i in range(level1.count):
    cmbLevel1.addRef(level1.refs[i])
  #
  #---------------------------------------------------------------------
  #Save old combined level1 context
  #
  obs.level1 = cmbLevel1		#write combined Level1 to observation
  #
  #---------------------------------------------------------------------
  #Make new level2 context
  #
  level2=MapContext()
  for key in level1.meta.keySet():
    if key != "creator" and key != "creationDate": 
           level2.meta[key]=level1.meta[key].copy()
  #
  level2.type="level2context"
  level2.description="Context for SPIRE Level 2 products"
  level2.meta["level"]=StringParameter("20", "The level of the product")
  #
  #---------------------------------------------------------------------
  # Run through destriper for all arrays
  #
  arrays = ["PSW","PMW","PLW"]
  psize = [6.0,10.0,14.0]
  #
  #
  for i in range(len(arrays)):
    cmbLevel1 = destriper(Level1=cmbLevel1,pixelSize=psize[i],\
                               array=arrays[i])
    level2.refs.put(arrays[i], \
                    ProductRef(destriper.mapContext.refs[arrays[i]].product))
  #
  #
  #
  #---------------------------------------------------------------------
  # attach Level1.5 product to obs (destriped Level 1 timelines)
  #
  obs.refs["level1_5"]=ProductRef(cmbLevel1)
  #
  #
  #---------------------------------------------------------------------
  # Add Level2 to observation context
  #
  obs.level2 = level2     #link Level 2 context
  #
  #
  #---------------------------------------------------------------------
  # Create Browse Products
  #
  createRgbImage=CreateRgbImageTask()
  browseProduct=createRgbImage(red=level2.refs["PLW"].product, \
                             green=level2.refs["PMW"].product, \
                             blue =level2.refs["PSW"].product, \
                   percent=98.0, redFactor=1.0, greenFactor=1.0, blueFactor=1.0)
  #
  # Populate metadata of the browse product
  for par in ObsParameter.values():
    if obs.meta.containsKey(par.key) and par.key != "fileName":
      browseProduct.meta[par.key]=obs.meta[par.key].copy()
  #
  browseProduct.startDate=obs.startDate
  browseProduct.endDate=obs.endDate
  browseProduct.instrument=obs.instrument
  browseProduct.modelName=obs.modelName
  browseProduct.description="Browse Product"
  browseProduct.type="BROWSE"
  #
  # Attach the browse product to the ObservationContext
  obs.browseProduct=browseProduct
  #
  from herschel.ia.gui.image import ImageUtil
  imageUtil = ImageUtil()
  #
  browseProductImage=imageUtil.getRgbTiledImage(\
    browseProduct["red"].data, browseProduct["green"].data, \
    browseProduct["blue"].data)
  #
  obs.browseProductImage=browseProductImage.asBufferedImage
  #
  #outFile=( "%s_x%s_BrowseProduct.jpg" % (input[4],hex(input[0])[6:-1]) )
  #displayExplorer = Display(browseProduct)
  #displayExplorer.zoomFit()
  #displayExplorer.saveAsJPG(outFile)
  #displayExplorer.close()
  #
  return(obs)
#
#
#
#End of file
