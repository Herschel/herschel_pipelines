from IAS_tools import *
from export import *
from process import *

#obsid =0x50001900L
#storage="OD121_0x50001900L_SpirePhotoLargeScan_n7023_int"
#storage="HH_dense_core_orig"
#process(storage,  doPipeline=False, doPipelineTest=True,doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=False)
#process(storage,  doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage,  doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

storage="HCG92-R1_SPIRE_orig"
#process(storage,  doPipeline=False, doPipelineTest=True,doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=False)
process(storage,  doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage,  doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
storage="HCG92-R1_SPIRE"
process(storage,  doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
