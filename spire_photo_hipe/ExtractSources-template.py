clear(all=True)

fitsFiles = []
fwhm = {"PSW":18.0, "PMW":25.0, "PLW":36.0}

fits = '/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/extractSourcesParallel/OD1331_0x5001410dL_SpirePhotoLargeScan_PLCK_HZ_G104.0plus64.2_destriped_PSW.fits'

#for fits in fitsFiles: 

directory = fits.replace(fits.split("/")[-1],"")

imageFits = fitsReader(fits)
band = imageFits.meta["description"].value[:3]
filenameID = "ExtractedSources_"+imageFits.meta["object"].value+"_"+band+"_"
detThreshold = 0.1
beamArea = Math.PI * fwhm[band] ** 2 / 4 / LOG(2)

[sourceListSussex, filteredMapSussex, prfSussex] = sourceExtractorSussextractor( image = imageFits, detThreshold = 0.1, fwhm = fwhm[band], getFilteredMap=True, getPrf=True, useSignalToNoise=True)
simpleFitsWriter(sourceListSussex,directory+filenameID+'sourceList-Sussex_threshold_'+str(detThreshold)+'.fits')
simpleFitsWriter(prfSussex,directory+filenameID+'prf-Sussex_threshold_'+str(detThreshold)+'.fits')
simpleFitsWriter(filteredMapSussex,directory+filenameID+'filteredMap-Sussex_threshod_'+str(detThreshold)+'.fits')

#[sourceListDaophot, filteredMapDaophot, prfDaophot] = sourceExtractorDaophot( image = imageFits, detThreshold = 0.1, fwhm = fwhm[band], beamArea = beamArea, getFilteredMap=True, getPrf=True, useSignalToNoise=True)
#simpleFitsWriter(sourceListDaophot,directory+filenameID+'sourceList-Daophot_threshold_'+str(detThreshold)+'.fits')
#simpleFitsWriter(prfDaophot,directory+filenameID+'prf-Daophot_threshold_'+str(detThreshold)+'.fits')
#simpleFitsWriter(filteredMapDaophot,directory+filenameID+'filteredMap-Daophot_threshod_'+str(detThreshold)+'.fits')

obsid = imageFits.meta["obsid"].value
obs = getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
scans = obs.level1
tasksDone = scans.refs[0].product["History"]["HistoryTasks"].getColumn("Name").data
if "baselineRemovalMedian" not in tasksDone: scans = baselineRemovalMedian(scans)
rPeak = {"PSW":22.0, "PMW":30.0, "PLW":42.0}
sourceListTimeline = sourceExtractorTimeline(input = scans, array = band, inputSourceList = sourceListSussex, rPeak = rPeak[band])

simpleFitsWriter(sourceListTimeline,directory+filenameID+'sourceList-TimelineFitter.fits')

disp = Display(imageFits)
disp.addPositionList(sourceListSussex, java.awt.Color.GREEN)
#disp = Display(imageFits)
#disp.addPositionList(sourceListDaophot, java.awt.Color.YELLOW)
#disp = Display(imageFits)
disp.addPositionList(sourceListTimeline, java.awt.Color.BLACK)



