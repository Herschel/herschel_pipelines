from IAS_tools import *
from export import *
from process import *


obsids = [1342211604, 1342211605, 1342211606, 1342211617, 1342211294,1342211309, 1342211319, 1342213207  ]

# process(useHsa=True,obsids=obsids,poolBaseName="M31", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=False)
# process(useHsa=True,obsids=obsids,poolBaseName="M31", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=False)
process(useHsa=False,poolName="M31_SPIRE",           doPipeline=False, doTdf=True,  doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=True)
