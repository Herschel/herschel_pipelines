from process import *

storage="OT1_lmontier_SPIRE"
obsids=[
1342255127L, \
1342255106L, \
1342255105L, \
1342212373L,\
1342256714L,\
1342259413L,\
1342261609L,\
1342268331,\
1342268362,\
1342266684
]

#~ process(useHsa=True,poolBaseName="OT1_lmontier",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(useHsa=True,poolBaseName="OT1_lmontier",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(poolName=storage,poolBaseName="OT1_lmontier",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="OT1_lmontier", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="OT1_lmontier", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="OT1_lmontier", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)
