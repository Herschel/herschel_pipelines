def doMaps(scans, resolution='',wcsPSW='',wcsPMW='',wcsPLW=''):
	print "mapping..."
	print "    ...PSW"
	if wcsPSW == '' and resolution == '':
		mapPsw=naiveScanMapper(scans, array="PSW")
	if wcsPSW == '' and resolution != '':
		mapPsw=naiveScanMapper(scans, array="PSW",resolution=resolution)
	if wcsPSW != '' and resolution != '':
		mapPsw=naiveScanMapper(scans, array="PSW",resolution=resolution,wcs=wcsPSW)
	if wcsPSW != '' and resolution == '':
		mapPsw=naiveScanMapper(scans, array="PSW",wcs=wcsPSW)
	
	#mapPsw=naiveScanMapper(scans, array="PSW")
	print "    ...PMW"
	if wcsPMW == '' and resolution == '':
		mapPmw=naiveScanMapper(scans, array="PMW")
	if wcsPMW == '' and resolution != '':
		mapPmw=naiveScanMapper(scans, array="PMW",resolution=resolution)
	if wcsPMW != '' and resolution != '':
		mapPmw=naiveScanMapper(scans, array="PMW",resolution=resolution,wcs=wcsPMW)
	if wcsPMW != '' and resolution == '':
		mapPmw=naiveScanMapper(scans, array="PMW",wcs=wcsPMW)
	print "    ...PLW"
	if wcsPLW == '' and resolution == '':
		mapPlw=naiveScanMapper(scans, array="PLW")
	if wcsPLW == '' and resolution != '':
		mapPlw=naiveScanMapper(scans, array="PLW",resolution=resolution)
	if wcsPLW != '' and resolution != '':
		mapPlw=naiveScanMapper(scans, array="PLW",resolution=resolution,wcs=wcsPLW)
	if wcsPLW != '' and resolution == '':
		mapPlw=naiveScanMapper(scans, array="PLW",wcs=wcsPLW)
	
	level2=MapContext()
	level2.type="level2context"
	level2.description="Context for SPIRE Level 2 products"
	level2.meta["level"]=StringParameter("2.0", "The level of the product")
	level2.refs.put("PLW",ProductRef(mapPlw))
	level2.refs.put("PMW",ProductRef(mapPmw))
	level2.refs.put("PSW",ProductRef(mapPsw))
	
	return level2
	
def multiL1fitstoL2(toProcess,resolution='',wcsPSW='',wcsPMW='',wcsPLW=''):
	scans=SpireListContext()
	useRemoveBaseline=True
	tempStorage=Boolean.TRUE
	chanNum=FitsArchive().load("/data/glx-herschel/data1/herschel/data/SPIRE/ChanNum.fits")
	for index in range(len(toProcess)):
		print "Adding "+toProcess[index]['obsid']+"..."
		filelist=glob.glob(path.join(toProcess[index]['dir'],"*"+toProcess[index]['obsid']+"*"))
		for file in filelist[0:]:
			psp=FitsArchive().load(file)
			if useRemoveBaseline:
				psp=removeBaseline(psp,chanNum=chanNum)
				if tempStorage:
					ref=ProductSink.getInstance().save(psp)
					scans.addRef(ref)
				else:
					scans.addProduct(psp)
			else:
				scans.addRef(psp)
			pass
	
	level2 = doMaps(scans,resolution=resolution,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW)
	
	return level2

def L1fitstoL2(dir,string,resolution='',wcsPSW='',wcsPMW='',wcsPLW=''):
	filelist=glob.glob(path.join(dir,"*"+string+"*"))
	# Use the first let to get the list of good channels
	# TODO : Will it be the same over the AOR ?
	scans=SpireListContext()
	useRemoveBaseline=True
	tempStorage=Boolean.TRUE
	chanNum=FitsArchive().load("/data/glx-herschel/data1/herschel/data/SPIRE/ChanNum.fits")
	for file in filelist[0:]:
		psp=FitsArchive().load(file)
		if useRemoveBaseline:
			psp=removeBaseline(psp,chanNum=chanNum)
			if tempStorage:
				print file
				ref=ProductSink.getInstance().save(psp)
				scans.addRef(ref)
			else:
				scans.addProduct(psp)
		else:
			scans.addRef(psp)
		pass
	
	level2 = doMaps(scans,resolution=resolution,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW)
	
	return level2
