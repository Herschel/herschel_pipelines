from process import *

# relaunch all with a new version of Hipe

##
obsids=[
# KPOT_eegami_1
1342187174,\
1342187184,\
1342187715,\
1342187716,\
1342187717\
]


storage="clusters_lowz_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="clusters_lowz", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#process(useHsa=True,obsids=obsids,poolBaseName="clusters_lowz", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(poolName=storage,obsids=obsids,poolBaseName="clusters_lowz",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
