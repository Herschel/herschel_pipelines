from process import *
import sys
# relaunch all with a new version of Hipe

##
obsids=[
# OT1_eegami_4
1342211297,\
1342211298,\
1342211299,\
1342211300,\
1342211301,\
1342211302,\
1342211303,\
1342211360,\
1342211362,\
1342211363,\
1342211404,\
1342211597,\
1342211598,\
1342211599,\
1342211602,\
1342212291,\
1342212292,\
1342212295,\
1342212298,\
1342212370,\
1342212371,\
1342213187,\
1342213188,\
1342213191,\
1342213192,\
1342213193,\
1342214557,\
1342214559,\
1342214560,\
1342214564,\
1342214746,\
1342214747,\
1342214748,\
1342214750,\
1342214751,\
1342214753,\
1342214755,\
1342215993,\
1342216004,\
1342216006,\
1342216007,\
1342218976,\
1342218977,\
1342218978,\
1342218980,\
1342218983,\
1342218984,\
1342218987,\
1342218988,\
# FAILED in Observing LOG 1342219645,\
1342220866,\
1342220867,\
1342220876,\
1342222661,\
1342223226,\
1342223234,\
1342223243,\
1342223247,\
1342223253,\
1342224979,\
1342226644,\
1342226645,\
1342226646,\
1342226647,\
1342226648,\
1342226649,\
1342226981,\
1342226984,\
1342226985,\
1342226987,\
1342227697,\
1342227698,\
1342227699,\
1342227700,\
1342227701,\
1342227703,\
1342227714,\
1342227716,\
1342227721,\
1342229124,\
1342229128,\
1342229132,\
1342229144,\
1342229146,\
1342229163,\
1342229167,\
1342229168,\
1342229173,\
1342229231,\
1342229233,\
1342229234,\
1342229236,\
1342229238,\
1342229239,\
1342229464,\
1342229488,\
1342229505,\
1342229507,\
1342229516,\
1342229543,\
1342229547,\
1342229556,\
1342229566,\
1342229567,\
1342229569,\
1342229570,\
1342229571,\
1342229573,\
1342229575,\
1342229583,\
1342229584,\
1342229586,\
1342229588,\
1342229591,\
1342229599,\
1342229601,\
1342229603,\
1342229604,\
1342229609,\
1342229614,\
1342229618,\
1342229619,\
1342229631,\
1342229634,\
1342229637,\
1342229643,\
1342229655,\
1342229657,\
1342229661,\
1342229663,\
1342229665,\
1342229667,\
1342229668,\
1342229672,\
1342230751,\
1342230767,\
1342230771,\
1342230784,\
1342230786,\
1342230801
]

storage="clusters_lowz_SPIRE"
#~ process(useHsa=True,obsids=obsids,poolBaseName="clusters_lowz", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(useHsa=True,obsids=obsids,poolBaseName="clusters_lowz", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(poolName=storage,obsids=obsids,poolBaseName="clusters_lowz",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="clusters_lowz", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="clusters_lowz", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="clusters_lowz", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
