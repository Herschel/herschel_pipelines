from process import *
import sys

# relaunch all with a new version of Hipe
# requested by Marco Brocchio
##
obsids=[
1342188589,\
]
storage="KPGT_cwilso01_1_SPIRE"
#~ process(useHsa=True,obsids=obsids,poolBaseName="KPGT_cwilso01_1", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#~ process(poolName=storage,obsids=obsids,poolBaseName="KPGT_cwilso01_1",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#~ process(poolName=storage,obsids=obsids,poolBaseName="KPGT_cwilso01_1",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doApplyExtendedEmissionGains=True)
#~ process(useHsa=True,obsids=obsids,poolBaseName="KPGT_cwilso01_1", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

process(poolName=storage, poolBaseName="KPGT_cwilso01_1", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False, doCombine=False)
process(poolName=storage, poolBaseName="KPGT_cwilso01_1", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="KPGT_cwilso01_1", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
