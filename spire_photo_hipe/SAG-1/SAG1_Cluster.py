from process import *

# # Some data are properieritary
storage="Abell370_SPIRE"
obsids_A370 = [1342201311, 1342201312, 1342201313, 1342201314, 1342201315, 1342201316, 1342201317, 1342201318]
process(useHsa=True,obsids=obsids_A370,poolBaseName="Abell370", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids_A370,poolBaseName="Abell370", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(poolName=storage,poolBaseName="Abell370",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)


storage="A1689_SPIRE"
obsids_A1689 = [1342200130, 1342201246, 1342201247, 1342201248, 1342201249, 1342201250, 1342201251, 1342201252, 1342201253]
process(useHsa=True,obsids=obsids_A1689,poolBaseName="Abell1689", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids_A1689,poolBaseName="Abell1689", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(poolName=storage,poolBaseName="Abell1689",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)

storage="Abell2219_SPIRE"
obsids_A2219 = [1342193018, 1342193019, 1342193020, 1342193021, 1342193022, 1342193023, 1342193024, 1342193025, 1342201449]
process(useHsa=True,obsids=obsids_A2219,poolBaseName="Abell2219", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids_A2219,poolBaseName="Abell2219", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(poolName=storage,poolBaseName="Abell2219",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)

# Some data are properieritary
storage="RXJ13475_SPIRE"
obsids_RXJ13475 = [1342201256, 1342201257, 1342201258, 1342201259, 1342201260, 1342201261, 1342201262, 1342201263]
process(useHsa=True,obsids=obsids_RXJ13475,poolBaseName="RXJ13475", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids_RXJ13475,poolBaseName="RXJ13475", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(poolName=storage,poolBaseName="RXJ13475",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
