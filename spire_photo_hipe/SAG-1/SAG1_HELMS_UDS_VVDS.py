from process import *

# # Some data are properieritary
storage="HELMS_SPIRE"
obsids = [\
1342246580,\
1342246632,\
1342247216,\
1342234749,\
1342236232,\
1342236234,\
1342236240,\
1342237550,\
1342237553,\
1342237563,\
1342238251\
]
#process(useHsa=True,obsids=obsids,poolBaseName="HELMS", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
#process(useHsa=True,obsids=obsids,poolBaseName="HELMS", doPipeline=False, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
#process(poolName=storage,obsids=obsids,poolBaseName="HELMS",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)


storage="UDS_SPIRE"
obsids = [\
1342201437,\
1342201486,\
1342201487,\
1342201488,\
1342201489,\
1342201490,\
1342201491\
]
process(useHsa=True,obsids=obsids,poolBaseName="UDS", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids,poolBaseName="UDS", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
#process(poolName=storage,obsids=obsids,poolBaseName="UDS",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)

storage="VVDS_SPIRE"
obsids = [1342201438,\
1342201439,\
1342201440,\
1342201441,\
1342201442,\
1342201443,\
1342201444]
process(useHsa=True,obsids=obsids,poolBaseName="VVDS", doPipeline=True, doTdf=True,  doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
process(useHsa=True,obsids=obsids,poolBaseName="VVDS", doPipeline=True, doTdf=False, doSavePool=True,  doExport=True,  doAll=True,doCombine=True)
#process(poolName=storage,obsids=obsids,poolBaseName="VVDS",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
