from IAS_tools import *
from export import *
from process import *

# A2218 ADFS Bootes-Spitzer ECDFS ELAIS-N1-SCUBA FLS GOODS-N Lockman-North
#Lockman-SWIRE UDS VVDS XMM-LSS 

#obsids=[1342186125L,1342183679L]
#storage="A2218_SPIRE"
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)

obsids = [1342191158,1342191159,1342191160,1342191161,1342191162,1342191163,1342191164,1342191165,1342191166,1342191167,1342191168,1342191169,1342191170,1342191171,1342191172,1342191173,1342191174,1342191175,1342191176,1342201366,1342201367,1342201368,1342201369,1342201370,1342201371]
storage="ECDFS_SPIRE"
process(useHsa=True,obsids=obsids, poolBaseName="ECDFS",doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="ECDFS",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
#obsids=[1342187646L,1342187647L,1342187648L,1342187649L,1342187650L]
###poolnames=['ELAIS-N1-SCUBAoff103n-1-1', 'ELAIS-N1-SCUBAoff52n-1-1', 'ELAIS-N1-SCUBAoff77o-1-1', 'ELAIS-N1-SCUBA', 'ELAIS-N1-SCUBA']
#storage="ELAIS-N1-SCUBA_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="ELAIS-N1-SCUBA", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="ELAIS-N1-SCUBA",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
###process(poolName=storage,useHsa=False,obsids=obsids,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
obsids=[1342188096L,1342188097L]
storage="ADFS_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="ADFS", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="ADFS", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,useHsa=False,obsids=obsids,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
obsids=[1342188650L,1342188651L,1342187712L,1342187713L,1342189108L,1342188681L,1342188682L,1342187711L,1342188090L]
storage="Bootes-Spitzer_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="Bootes-Spitzer", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="Bootes-Spitzer",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,useHsa=False,obsids=obsids,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
obsids=[1342189003L,1342189004L,1342190313L,1342190324L,1342190325L,1342189031L]
storage="XMM_ALL_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="XMM_ALL", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="XMM_ALL",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,useHsa=False,obsids=obsids,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
obsids=[1342186123L,1342186294L]
storage="FLS_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="FLS", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="FLS",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

obsids=[1342186110L]
storage="Lockman-North_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="Lockman-North", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="Lockman-North",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

obsids=[1342222596, 1342222595, 1342222594, 1342222593, 1342222591, 1342222590, 1342222589, 1342222588, 1342186109, 1342186108]
storage="Lockman-SWIRE_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="Lockman-SWIRE", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="Lockman-SWIRE",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

obsids=[1342195947, 1342195948, 1342195949, 1342195950, 1342195951, 1342195952, 1342195953]
storage="Lockman-East_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="Lockman-SWIRE", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids, poolBaseName="Lockman-SWIRE",doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)

