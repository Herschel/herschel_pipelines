execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

storage="Lockman-North_SPIRE_orig"

#process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=False)

process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=False, doTdf=False, doSavePool=False, doExport=True, doAll=False)

