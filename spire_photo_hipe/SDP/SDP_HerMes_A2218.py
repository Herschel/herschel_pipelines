execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')



##A2218
#obsid =0x500018ff
#storage="0x500018ff"
#process(storage, obsid, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#del(storage)
#
##A2218
#obsid =0x5000228d
#storage="0x5000228d"
#process(storage, obsid, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#del(storage)
#

suffix=''
poolNames = ["0x500018ff","0x5000228d"]
newPoolNames = newPoolList(poolNames,suffix=suffix)
level2 = combineMaps(newPoolNames)
saveMaps(level2,suffix+'_combined')
del(level2)
