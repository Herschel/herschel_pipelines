execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

storageName='Lockman-SWIRE_SPIRE_orig'
#obsid=0x5000227DL
# WARNING Specific DetAngle for Lockman in POF5 script
process(storageName,poolBaseName="Lockman-SWIRE",  doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(storageName, poolBaseName="Lockman-SWIRE", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)


#obsid=0x5000227CL
#
#process(storageName,  doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storageName,  doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=False)
#
#process(storageName,  doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storageName,  doPipeline=False, doTdf=False, doSavePool=False, doExport=True, doAll=False)


#copyPool('Lockman-SWIRE-offset2-1-1','Lockman-SWIRE')
#copyPool('Lockman-SWIRE-offset2-1-1_notdf','Lockman-SWIRE_notdf')
