execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

storage='FLS_orig'
#obsid=0x50002336L
process(storage,  doPipeline=False,doPipelineTest=True, doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=True)
process(storage,  doPipeline=False,doPipelineTest=True, doTdf=False,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=True)
##process(storage, obsid, doPipeline=True, doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=False)
#
#obsid=0x5000228BL
#obsid=0x50002336L
#process(storage, obsid, doPipeline=True, doTdf=False,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, [obsid], doPipeline=False, doTdf=True, noTimeRespCorr=False,doSavePool=False, doExport=True, doAll=True)
#
#storage='FLS'
#obsid=0x5000228BL
#process(storage, obsid, doPipeline=True, doTdf=True,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=True)
##process(storage, obsid, doPipeline=False, doTdf=True, noTimeRespCorr=False,doSavePool=False, doExport=True, doAll=False)
#
#process(storage, obsid, doPipeline=True, doTdf=False,noTimeRespCorr=False, doSavePool=True, doExport=True, doAll=True)
##process(storage, obsid, doPipeline=False, doTdf=False, noTimeRespCorr=False,doSavePool=False, doExport=True, doAll=False)

#listobs=[0x50002336,0x5000228B]
#listpool=["FLS","FLS_2"]

# Make a full map.... But only with the full pipeline (suffix='')
#
#combineMaps(listpool,'original_test')

#newlistpool=newPoolList(listpool)
#print newlistpool

#combineMaps(newlistpool)
