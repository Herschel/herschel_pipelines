execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/export.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')

obsid =0x500039E5
storage_name="ORION_1342192101"
storage_name="OD301_0x500039E5L_SpirePhotoSmallScan_HD37041"
#storage="500022F9"
#storage_name="OD162_0x500022F9L_SpirePacsParallel_polaris"
#process(storage, obsid, doPipeline=False, doTdf=True,doSavePool=False, doExport=True, doAll=True)

storage=ProductStorage([storage_name])
query=MetaQuery(ObservationContext,"p","p.meta['obsid'].value==%iL"%obsid)
refs=storage.select(query)
obs=refs[0].product
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_mask.py')
suffix=''

saveMaps(obs.refs["level2"].product,suffix='_with_saturated')
#storage="500022FA"
del(storage)
storage_name="OD162_0x500022FAL_SpirePacsParallel_polaris"
obsid =0x500022FA
storage=ProductStorage([storage_name])
query=MetaQuery(ObservationContext,"p","p.meta['obsid'].value==%iL"%obsid)
refs=storage.select(query)
obs=refs[0].product
#process(storage, obsid, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline.py')
suffix=''

export2Sanepic(obs,range(obs.level1.count),suffix+'_all_sanepic_test_v2.fits')

del(storage)
