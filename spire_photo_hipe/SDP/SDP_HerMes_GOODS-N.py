from IAS_tools import *
from export import *
from process import *

storage='GOODS-N_orig'


process(storage, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=False)

process(storage, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(storage, obsid, doPipeline=False, doTdf=False, doSavePool=False, doExport=True, doAll=False)



# Smart Split of data...
# suffix='_notdf'
# obs = getObs(storage,obsid)
# storageName = makeName('',obs.meta,suffix)
# obs = getObs(storageName, obsid)
# range1 = range(0,101)
# range2 = range(101,130)
# range3 = range(130,359)

# export2Sanepic(obs,range1,suffix+"_myfirst_sanepic.fits"%i)
# export2Sanepic(obs,range2,suffix+"_mysecond_sanepic.fits"%i)
# export2Sanepic(obs,range3,suffix+"_mythird_sanepic.fits"%i)
