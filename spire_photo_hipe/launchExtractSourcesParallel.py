clear(all=True)
obsid = 0x50008915
obs = getObservation(obsid,useHsa=True)
CGS3_Coadd_200PSW_all = fitsReader(file = '/home/kdassas/Glx_herschel/data/CGS3_Coadd_200PSW_all.fits')
#http://herschel.esac.esa.int/hcss-doc-8.0/load/howtos/html/Dag.ImageAnalysis.HowTo.SourceExtraction.html

obs_level2_PSW = obs.refs["level2"].product.refs["PSW"].product
obs_level2_PSW = CGS3_Coadd_200PSW_all
###### 18.0 for PSW, 25.0 for PMW, 36.0 for PLW
beamArea = Math.PI * 18.0 ** 2 / 4 / LOG(2)

################# Sussex Method
resultListSussex= sourceExtractorSussextractor( \
    image = obs_level2_PSW,        # Image name \
    detThreshold = 0.1,      # Threshold in signal-to-noise ratio (or log evidence) \
    fwhm = 18.0,            # FWHM of PRF (arcsec) \
    beamArea = beamArea,    # Area of the beam \
    getFilteredMap=True,getPrf=True)

sourceListSussex=resultListSussex[0]
disp = Display(obs_level2_PSW)
disp.addPositionList(sourceListSussex, java.awt.Color.YELLOW)
filteredMapSussex=resultListSussex[1]
prfSussex=resultListSussex[2]
simpleFitsWriter(product=sourceListSussex, file='/home/kdassas/sourceListSussex_threshold0.1.fits')
simpleFitsWriter(product=prfSussex, file='/home/kdassas/prfSussex_threshold0.1.fits')
simpleFitsWriter(product=filteredMapSussex, file='/home/kdassas/filteredMapSussex_threshod0.1.fits')

################# Daophot method
resultListDaophot= sourceExtractorDaophot( \
    image = obs_level2_PSW,        # Image name \
    detThreshold = 0.1,      # Threshold in signal-to-noise ratio (or log evidence) \
    fwhm = 18.0,            # FWHM of PRF (arcsec) \
    beamArea = beamArea,    # Area of the beam \
    getFilteredMap=True,getPrf=True)

sourceListDaophot=resultListDaophot[0]
filteredMapDaophot=resultListDaophot[1]
prfDaophot=resultListDaophot[2]
disp = Display(obs_level2_PSW)
disp.addPositionList(sourceListDaophot, java.awt.Color.YELLOW)

simpleFitsWriter(product=sourceListDaophot, file='/home/kdassas/sourceListDaophot_threshold0.1.fits')
simpleFitsWriter(product=prfDaophot, file='/home/kdassas/prfDaophot_threshold0.1.fits')
simpleFitsWriter(product=filteredMapDaophot, file='/home/kdassas/filteredMapDaophot_threshod0.1.fits')


