#
# 
#
# Output Products:
# Level 0: RPDT    BuildingBlockProduct          Raw Photometer Detector Timeline
#          RNHKT   BuildingBlockProduct          Raw Nominal House Keeping Timeline
#
# Level 1: PSP     DetectorTimeline              Photometer Scan Product
#
# Author: Pasquale Panuzzo, CEA Saclay, France, pasquale.panuzzo@cea.fr
#
# 09 april 2010: check time bw legs. if more than 3 seconds, bw 2 legs, get the
# index of the discontinuity. (hole when calibration line)
#
# Import all needed classes
from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter


# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *

# Input definition:
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *

# Import the script obsLoader.py that allows to load an ObservationContext from a storage.
from herschel.spire.ia.scripts.tools.obsLoader import *

# Open the input dialog to enter inputs
#inputs.openDialog()
inputs.level   = 'level0_5'
inputs.plot    = False
inputs.mapping = 'naive'

#
print "processing OBSID=", obsid,"("+hex(obsid)+")"

# Set this to FALSE if you don't want to use the ProductSink
# and do all the processing in memory
tempStorage=Boolean.TRUE

# From Level 0 to Level 0.5
if inputs.level=="level0":
	# Make Engineering conversion of level 0 products
	level0_5= engConversion(obs.level0,cal=obs.calibration, tempStorage=tempStorage)
	# Add the result to the observation in level 0.5
	obs.level0_5=level0_5
else:
	level0_5=obs.level0_5
pass

# set the progress
inputs.progress=20
# counter for computing progress
count=0
holes_box=[]
# From Level 0.5 to Level 1
if inputs.level=="level0" or inputs.level=="level0_5":
	bbids=level0_5.getBbids(0xa103)
	# bbids=level0_5.getBbids()
	nlines=len(bbids)
	#print "number of scan lines:",nlines
	#
	# Loop over scan lines
	for bbid in bbids:
		block=level0_5.get(bbid)
		#print "processing BBID="+hex(bbid)
		# Now move to engineering data products
		pdt  = block.pdt
		nhkt = block.nhkt
		if pdt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block."
			continue
		if nhkt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block."
			continue
		#
		bbCount=bbid & 0xFFFF
		pdtFollow=None
		nhktFollow=None
		pdtTrail=None
		nhktTrail=None
		if bbid < MAX(Long1d(bbids)):
		    # 0xaf000000+bbCount=bbid for turnaround 2
			blockFollow=level0_5.get(0xaf000000L+bbCount)
			pdtFollow=blockFollow.pdt
			nhktFollow=blockFollow.nhkt
			#print " pdtFollow.sampleTime(0) "+str(pdtFollow.sampleTime[0])+" pdt.sampleTime[-1]  "+str(pdt.sampleTime[-1])+" bbid "+hex(bbid)
			if pdtFollow != None and pdtFollow.sampleTime[0] > pdt.sampleTime[-1]+3.0:
				pdtFollow=None
				nhktFollow=None
				print "pdtFollow.sampleTime(0) > pdt.sampleTime[-1]+3.0  bbcount "+str(bbCount)+" bbid "+hex(bbid)
				print "bbid "+hex(bbid)
				print "bbCount "+str(bbCount)
		if bbCount >1:
		    # 0xaf000000+bbCount=bbid for turnaround 1
			blockTrail=level0_5.get(0xaf000000L+bbCount-1)
			pdtTrail=blockTrail.pdt
			nhktTrail=blockTrail.nhkt
			if pdtTrail != None and pdtTrail.sampleTime[-1] < pdt.sampleTime[0]-3.0:
			 	# print " pdtTrail.sampleTime(-1) "+str(pdtTrail.sampleTime[0])+ " pdt.sampleTime[0]  "+str(pdt.sampleTime[0])
				print "pdtTrail.sampleTime < pdt.sampleTime(0)-3  bbcount "+str(bbCount)+" bbid "+hex(bbid)
				pdtTrail=None
				nhktTrail=None
				# store indexes where discontinuity
				holes_box.append(int(bbCount-2))
				print holes_box
		#
		#print "Completed BBID="+hex(bbid)+" bbid "+str(bbid)
		# set the progress
		count=count+1
		inputs.progress = 20+(60*count)/nlines
#



