# execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/export.py')

from herschel.share.unit import Angle
from herschel.ia.obs.auxiliary.pointing import PointingUtils
from herschel.share.fltdyn.time import FineTime

from herschel.spire.ia.util import SpireMask
from herschel.spire.all import *
from herschel.spire.util import *
from herschel.ia.all import *
from herschel.ia.toolbox.util import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter
from IAS_tools import makeName 
# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *
import time
from herschel.spire.ia.pipeline.phot.fluxconv import *
from IAS_tools import *
# TODO: Remove that if not needed
## Import the script tasks.py that contains the task definitions
#from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *


def export2Sanepic_from_fits(filelist,suffix='.fits',dir='/data/glx-herschel/data1/herschel/SANEPIC_Fits/L1_Sanepic_SPIRE/'):
	# Use the first let to get the list of good channels
	# TODO : Will it be the same over the AOR ?
	oneLine=FitsArchive().load(filelist[0])
	goodChan=[]
	goodChan=oneLine.getChannelNames()
	# Sort the array..."
	signals={}
	masks={}
	sampleTime=oneLine.sampleTime
	# Retrieve the unit of the signal (save it for later)
	unitSignal = oneLine.getSignalUnit(goodChan[0])
	for chan in goodChan:
		signals[chan]=oneLine.getSignal(chan)
		masks[chan]=oneLine.getMask(chan)
	pass
	del(oneLine)
	for file in filelist[1:]:
		oneLine=FitsArchive().load(file)
		sampleTime.append(oneLine.sampleTime)
		for chan in goodChan:
			signals[chan].append(oneLine.getSignal(chan))
			masks[chan].append(oneLine.getMask(chan))
		del(oneLine)
	#
#	System.gc()
	# Tranform them to 2d arrays channel vs time
	#print "Tranform them to 2d arrays channel vs time"
	sign=Float2d(len(goodChan),sampleTime.length())
	mask=Int2d(len(goodChan),sampleTime.length())
	
	for i in range(len(goodChan)):
		iSig = signals[goodChan[i]].to().double1d()
		iMask = masks[goodChan[i]]
		
		# TODO: see spire/ia/pipeline/util/AbstractMapperTask.java
		
		bad = iMask.where(SpireMask.MASTER | \
		SpireMask.GLITCH_FIRST_LEVEL_UNCORR | \
		SpireMask.GLITCH_SECOND_LEVEL_UNCORR | \
		SpireMask.GLITCH_FIRST_LEVEL | \
		SpireMask.GLITCH_SECOND_LEVEL | \
		SpireMask.TRUNCATED_UNCORR | \
		SpireMask.ISDEAD | \
		SpireMask.ISNOISY | \
		SpireMask.ISSLOW )
		
		# This is faulty on some mask... (maskVoltageOol and so on...)
		# So take the inverse (from 2.147. FixedMask )
		
		good = iMask.where(~SpireMask.MASTER.isSet(iMask) & \
		~SpireMask.GLITCH_FIRST_LEVEL_UNCORR.isSet(iMask) & \
		~SpireMask.GLITCH_SECOND_LEVEL_UNCORR.isSet(iMask) &  \
		~SpireMask.GLITCH_FIRST_LEVEL.isSet(iMask) & \
		~SpireMask.GLITCH_SECOND_LEVEL.isSet(iMask) & \
		~SpireMask.TRUNCATED_UNCORR.isSet(iMask) & \
		~SpireMask.ISDEAD.isSet(iMask) & \
		~SpireMask.ISNOISY.isSet(iMask) & \
		~SpireMask.ISSLOW.isSet(iMask) )
		
		if bad.length() > 0:
			iMask[bad] = 1
		
		if good.length() > 0:
			iMask[good] = 0
		
		# Crazy syntax to get the non finite element of iSig
		# from 6. Numeric library usage (6.4)
		bad = iSig.where(~IS_FINITE(iSig))
		if len(bad.toInt1d()) > 0:
			iMask[bad] = 1
		
		sign[i,:] = iSig
		mask[i,:] = iMask
		
		del(iSig,iMask)
	del(signals,masks)
#	System.gc()
	#
	ras={}
	decs={}
	oneLine=FitsArchive().load(filelist[0])
	# Retrieve the unit (save it for later)
	unitRa   = oneLine["ra"][goodChan[0]].unit
	unitDec  = oneLine["dec"][goodChan[0]].unit
	unitTime = oneLine["ra"]["sampleTime"].unit
	for chan in goodChan:
		ras[chan]=oneLine.getRa(chan)
		decs[chan]=oneLine.getDec(chan)
	del(oneLine)
	for file in filelist[1:]:
		oneLine=FitsArchive().load(file)
		for chan in goodChan:
			ras[chan].append(oneLine.getRa(chan))
			decs[chan].append(oneLine.getDec(chan))
#	System.gc()
	#print "end ra and dec"
	# Tranform them to 2d arrays channel vs time
	ra=Double2d(len(goodChan),sampleTime.length())
	dec=Double2d(len(goodChan),sampleTime.length())
	for i in range(len(goodChan)):
		ra[i,:]=ras[goodChan[i]]
		dec[i,:]=decs[goodChan[i]]
	pass
	del(ras,decs)
#	System.gc()
	# Channel names into a table Tabaset
	channels = TableDataset()
	channels["name"] = Column(String1d(goodChan))
	#
	# TODO : add available meta data here too...
	#
	# Put everything together and save it as fits file
	#p=Product()
	p=exportMetadata_fromFits(filelist)
	p.meta["unit"] = StringParameter(unitSignal.getName()+"/beam","unit of signal")
	p["signal"]=ArrayDataset(sign)
	p["signal"].meta["unit"] = StringParameter(unitSignal.getName()+"/beam","unit of signal")
	del(unitSignal)
	p["lon"]=ArrayDataset(ra)
	p["lon"].meta["unit"] = StringParameter(unitRa.getName(),"unit of RA")
	#p["ra"].meta["equinox"] = obs.meta["equinox"]
	del(unitRa)
	p["lat"]=ArrayDataset(dec)
	p["lat"].meta["unit"] = StringParameter(unitDec.getName(),"unit of Dec")
	#p["dec"].meta["equinox"] = obs.meta["equinox"]
	del(unitDec)
	p["mask"]=ArrayDataset(mask)
	p["time"]=ArrayDataset(sampleTime)
	p["time"].meta["unit"] = StringParameter(unitTime.getName(),"unit of Time")
	del(unitTime)
	p["channels"]=channels
	filename=makeName(dir,p.meta,suffix)
	print "Saving FinalProduct..."+filename
	FitsArchive().save(filename,p)
	del(p)


def export2Sanepic(obs,indexLeg,suffix='.fits',dir='/data/glx-herschel/data1/herschel/SANEPIC_Fits/L1_Sanepic_SPIRE/',doBaseline=False,doApplyExtendedEmissionGains=False,exportThermistors=False):
	# Use the first let to get the list of good channels
	# TODO : Will it be the same over the AOR ?
	print " ... Retrieving good channels"
	#print obs.meta
	chanMask=obs.calibration.phot.chanMask
	chanNum=obs.calibration.phot.chanNum
	chanRelGains=obs.calibration.phot.chanRelGain
	oneLine=obs.level1.getProduct(indexLeg[0])
	goodChan=[]
	#  TODO See also spire/ia/pipeline/util/AbstractMapperTask.java (ISSLOW & ISNOCHOPSKY)
	for chan in oneLine.channelNames:
		if not chanMask.isDead(chan) and not chanMask.isNoisy(chan) and not chanMask.isSlow(chan):
	        	if chanNum.isBolometer(chan) and not chanNum.isDark(chan):
	                	goodChan.append(chan)
	#
	#System.gc()
	# Sort the array...
	goodChan.sort()
	#print goodChan
	nChan = len(goodChan)
	# Retrieve the offsets of all detectors ...
	# by merging all bands ...
	print " ... Retrieving channels offsets"
	offsets=obs.calibration.phot.detAngOffList.refs[0].product
	X = {}
	Y = {}
	for array in ["PSW","PMW","PLW"]:
	        for index in range(len(offsets[array]["names"].data)):
	                X[offsets[array]["names"].data[index]] = offsets[array]["yangle"].data[index]
	                Y[offsets[array]["names"].data[index]] = offsets[array]["zangle"].data[index]
	del(offsets)
#	System.gc()
# ... and select only good detectors
	dX = Double1d(nChan)
	dY = Double1d(nChan)
	for i in range(nChan):
	        dX[i] = X[goodChan[i]]
	        dY[i] = Y[goodChan[i]]
	del(X,Y)
#
# Create the a new product (Future Binary Table) to take care of all Offsets
	offsets = TableDataset()
	offsets["name"] = Column(String1d(goodChan))
	offsets["dX"] = Column(dX,description="X offsets",unit=herschel.share.unit.Angle.SECONDS_ARC)
	offsets["dY"] = Column(dY,description="Y offsets",unit=herschel.share.unit.Angle.SECONDS_ARC)
	del(dX,dY)
#	System.gc()
	#
	# Retrieve signals, positions and masks, by concatenating all legs,
	# only for good channels
	# data between legs need to be present !
	#
	# Splitting into sampleTime+signal+mask / ras+decs for memory issues...
	#
	print " ... Building signal and mask"
	nbTurn = 0
	signals={}
	masks={}
	FlagTurnOvers=[]
	FlagBurpFounds=[]
	oneLine=obs.level1.getProduct(indexLeg[0])
	startNomTime=(oneLine.meta["startNominalScanLine"].value.microsecondsSince1958())/1E6
	endNomTime=(oneLine.meta["endNominalScanLine"].value.microsecondsSince1958())/1E6
	if doBaseline:
		print " ... Removing Baseline ..."
		oneLine=removeBaseline(oneLine,chanNum=chanNum)
	if doApplyExtendedEmissionGains:
                print "Apply relative gains for bolometers for better extended maps"
                oneLine.meta['type'].value="PSP"  # Necessary for pre-HCSS 8.0 processed observations
                applyRelativeGains=ApplyRelativeGainsTask()
		oneLine = applyRelativeGains(oneLine, chanRelGains)
	sampleTime=oneLine.sampleTime
	for j in range(oneLine.sampleTime.length()):
			if oneLine.sampleTime[j]<startNomTime or oneLine.sampleTime[j]>endNomTime:
				FlagTurnOvers.append(1)
				nbTurn+=1
			else:
				FlagTurnOvers.append(0)
			try:
				#print oneLine.meta["coolerBurpFound"].value
				if oneLine.meta["coolerBurpDetect"].value:
					#print oneLine.meta["coolerBurpFound"].value
                	        	FlagBurpFounds.append(1)
				else:
					FlagBurpFounds.append(0)
		        except:
                       		FlagBurpFounds.append(3)
	# Retrieve the unit of the signal (save it for later)
	unitSignal = oneLine.getSignalUnit(goodChan[0])
	for chan in goodChan:
		signals[chan]=oneLine.getSignal(chan)
		masks[chan]=oneLine.getMask(chan)
	#
	for i in indexLeg[1:]:
		oneLine=obs.level1.getProduct(i)
		startNomTime=(oneLine.meta["startNominalScanLine"].value.microsecondsSince1958())/1E6
		endNomTime=(oneLine.meta["endNominalScanLine"].value.microsecondsSince1958())/1E6
		#print startNomTime
		#print oneLine.sampleTime[0]
		for j in range(oneLine.sampleTime.length()):
			if oneLine.sampleTime[j]<startNomTime or oneLine.sampleTime[j]>endNomTime:
				FlagTurnOvers.append(1)
				nbTurn+=1
			else:
				FlagTurnOvers.append(0)
			try:
				if oneLine.meta["coolerBurpDetect"].value:
					print i,oneLine.meta["coolerBurpFound"].value
                                        FlagBurpFounds.append(1)
                                else:
                                        FlagBurpFounds.append(0)
		        except:
                       		FlagBurpFounds.append(3)
		if doBaseline:
			oneLine=removeBaseline(oneLine,chanNum=chanNum)
		if doApplyExtendedEmissionGains:
                        print "Apply relative gains for bolometers for better extended maps"
                        oneLine.meta['type'].value="PSP"  # Necessary for pre-HCSS 8.0 processed observations
                        applyRelativeGains=ApplyRelativeGainsTask()
                        oneLine = applyRelativeGains(oneLine, chanRelGains)
		sampleTime.append(oneLine.sampleTime)
		for chan in goodChan:
			signals[chan].append(oneLine.getSignal(chan))
			masks[chan].append(oneLine.getMask(chan))
	#
#	System.gc()
	 # Tranform them to 2d arrays channel vs time
	FlagTurnOver=Short1d(sampleTime.length())
	FlagBurpFound=Short1d(sampleTime.length())
	for i in range(sampleTime.length()):
		#print FlagTurnOver[i]
		#print FlagTurnOvers[i]
		FlagTurnOver[i]=FlagTurnOvers[i]
		FlagBurpFound[i]=FlagBurpFounds[i]
	pass
	sign=Double2d(nChan,sampleTime.length())
	mask=Short2d(nChan,sampleTime.length())
	for i in range(nChan):
		iSig = signals[goodChan[i]].to().double1d()
		iMask = masks[goodChan[i]]
		
		# TODO: see spire/ia/pipeline/util/AbstractMapperTask.java
		
		bad = iMask.where(SpireMask.MASTER | \
		SpireMask.GLITCH_FIRST_LEVEL_UNCORR | \
		SpireMask.GLITCH_SECOND_LEVEL_UNCORR | \
		SpireMask.GLITCH_FIRST_LEVEL | \
		SpireMask.GLITCH_SECOND_LEVEL | \
		SpireMask.TRUNCATED_UNCORR | \
		SpireMask.ISDEAD | \
		SpireMask.ISNOISY | \
		SpireMask.ISSLOW )
		
		# This is faulty on some mask... (maskVoltageOol and so on...)
		# So take the inverse (from 2.147. FixedMask )
		
		good = iMask.where(~SpireMask.MASTER.isSet(iMask) & \
		~SpireMask.GLITCH_FIRST_LEVEL_UNCORR.isSet(iMask) & \
		~SpireMask.GLITCH_SECOND_LEVEL_UNCORR.isSet(iMask) &  \
		~SpireMask.GLITCH_FIRST_LEVEL.isSet(iMask) & \
		~SpireMask.GLITCH_SECOND_LEVEL.isSet(iMask) & \
		~SpireMask.TRUNCATED_UNCORR.isSet(iMask) & \
		~SpireMask.ISDEAD.isSet(iMask) & \
		~SpireMask.ISNOISY.isSet(iMask) & \
		~SpireMask.ISSLOW.isSet(iMask) )
		
		if bad.length() > 0:
			iMask[bad] = 1
		
		if good.length() > 0:
			iMask[good] = 0
		
		# Crazy syntax to get the non finite element of iSig
		# from 6. Numeric library usage (6.4)
		bad = iSig.where(~IS_FINITE(iSig))
		if len(bad.toInt1d()) > 0:
			iMask[bad] = 1
		
		sign[i,:] = iSig
		mask[i,:] = iMask
		
		del(iSig,iMask)
	del(signals,masks)
	del(chanMask, chanNum)
	del(FlagTurnOvers)
	del(FlagBurpFounds)
#	System.gc()
	print "Building ra & dec..."
	ras={}
	decs={}
	oneLine=obs.level1.getProduct(indexLeg[0])
	# Retrieve the unit (save it for later)
	unitRa   = oneLine["ra"][goodChan[0]].unit
	unitDec  = oneLine["dec"][goodChan[0]].unit
	unitTime = oneLine["ra"]["sampleTime"].unit
	for chan in goodChan:
		ras[chan]=oneLine.getRa(chan)
		decs[chan]=oneLine.getDec(chan)
	for i in indexLeg[1:]:
		oneLine=obs.level1.getProduct(i)
		for chan in goodChan:
			ras[chan].append(oneLine.getRa(chan))
			decs[chan].append(oneLine.getDec(chan))
	del(oneLine)
#	System.gc()
	# Tranform them to 2d arrays channel vs time
	ra=Double2d(nChan,sampleTime.length())
	dec=Double2d(nChan,sampleTime.length())
	for i in range(nChan):
		ra[i,:]=ras[goodChan[i]]
		dec[i,:]=decs[goodChan[i]]
	# Retrieve the position of the reference bolometer (PSWE8)
	ref_ra = ras['PSWE8']
	ref_dec = decs['PSWE8']
	del(ras,decs)
#	System.gc()
	# Retrieve the reference position and position angle 
	# Get reference position and position angle
	print "Retrieving reference position and angle..."
	siamMatrix=obs.auxiliary.siam.getSiamSet("S14_0").matrix3.mInvert()
#	# TODO: This is a test : retrieve the reference position without the BSM angles...
#	ref_woBSM_ra  = Double1d(sampleTime.length())
#	ref_woBSM_dec = Double1d(sampleTime.length())
#	ref_woBSM_pa  = Double1d(sampleTime.length())
	# PSWE8 is always used as the reference position for the offsets, se below for explanation.... so get it from above 
	ref_pa  = Double1d(sampleTime.length())
	if obs.odNumber>319 and obs.odNumber<762:
        	print "get new pointing",str(obs.odNumber)
        	pointingpath='/data/glx-herschel/data1/herschel/data/corrected_pointing_od320_761/pointing_od_0'+str(obs.odNumber)+'.fits'
        	pointing = FitsArchive().load(pointingpath)
	else:
		pointing=obs.auxiliary.pointing
	for i in range(sampleTime.length()):
		# Inspired from spire/ia/dataset/SpirePointingProduct.java
		# We can not retrieve the exact position of the reference since the bsm angles are lost at this stage
		# the Aberration correction do not change the position angle, only the position, so we can drop it.
		FineTime_i = FineTime(long(sampleTime[i]*1e6))
		pointFilt=pointing.getItem(FineTime_i.microsecondsSince1958()).filtered
		pointSpire=pointFilt.offset(siamMatrix)
		ref_pa[i]  = pointSpire.pa
		# TODO: This is a test : retrieve the reference position without the BSM angles...
##		ref_ra[i]  = pointSpire.ra
##		ref_dec[i] = pointSpire.dec
#		pspire = PointingArray(1)
#		pspire.addPointing(0,pointSpire)
#		pspire = Aberration.correct(pspire,[FineTime_i])
#		ref_woBSM_pa[i]  = pspire.getPointing(0).pa
##		ref_woBSM_ra[i]  = pspire.getPointing(0).ra
##		ref_woBSM_dec[i] = pspire.getPointing(0).dec
##		# Get the direction of the Reference....
#		directions = pspire.getPointing(0).getDirection(0.,0.)
#		ref_woBSM_ra[i]  = directions.getRaDegrees()
#		ref_woBSM_dec[i] = directions.getDecDegrees()
#	del(pointing,pointFilt,pointSpire,pspire,siamMatrix,FineTime_i)
	del(pointing,pointFilt,pointSpire,siamMatrix,FineTime_i)
#	System.gc()
	 # Put it on a Table Product
	ref_pos = TableDataset()
	ref_pos["lon"]  = Column(ref_ra, description="reference RA",  unit=herschel.share.unit.Angle.DEGREES)
	ref_pos["lat"] = Column(ref_dec,description="reference DEC", unit=herschel.share.unit.Angle.DEGREES)
	ref_pos["phi"] = Column(ref_pa, description="position angle",unit=herschel.share.unit.Angle.DEGREES)
	del(ref_ra,ref_dec,ref_pa)
#	# TODO: This is a test : retrieve the reference position without the BSM angles...
#	ref_pos["ra_woBSM"] = Column(ref_woBSM_ra, description="reference RA without BSM (test)",  unit=herschel.share.unit.Angle.DEGREES)
#	ref_pos["dec_woBSM"] = Column(ref_woBSM_dec,description="reference DEC without BSM (test)", unit=herschel.share.unit.Angle.DEGREES)
#	ref_pos["phi_woBSM"] = Column(ref_woBSM_pa, description="position angle without BSM (test)",unit=herschel.share.unit.Angle.DEGREES)
#	del(ref_woBSM_ra,ref_woBSM_dec,ref_woBSM_pa)
#	System.gc()
	# Channel names into a table Tabaset
	print "Retrieving channels list..."
	channels = TableDataset()
	channels["name"] = Column(String1d(goodChan))
	# Put everything together and save it as fits file
	print "Constructing FinalProduct..."
	#p = Product()
	p=exportMetadata(obs,indexLeg)
	p.meta["unit"] = StringParameter(unitSignal.getName()+"/beam","unit of signal")
	p["signal"]=ArrayDataset(sign)
	p["signal"].meta["unit"] = StringParameter(unitSignal.getName()+"/beam","unit of signal")
	del(unitSignal)
	p["lon"]=ArrayDataset(ra)
	p["lon"].meta["unit"] = StringParameter(unitRa.getName(),"unit of RA")
	p["lon"].meta["equinox"] = obs.meta["equinox"]
	del(unitRa)
	p["lat"]=ArrayDataset(dec)
	p["lat"].meta["unit"] = StringParameter(unitDec.getName(),"unit of Dec")
	p["lat"].meta["equinox"] = obs.meta["equinox"]
	del(unitDec)
	p["mask"]=ArrayDataset(mask)
	p["time"]=ArrayDataset(sampleTime)
	p["time"].meta["unit"] = StringParameter(unitTime.getName(),"unit of Time")
	del(unitTime)
	p["channels"]=channels
	p["refPos"]=ref_pos
	p["offsets"] = offsets
	p["turnover"]=ArrayDataset(FlagTurnOver)
	p["burpFound"]=ArrayDataset(FlagBurpFound)
	if exportThermistors: p["temperature"]=exportThermistor(obs,indexLeg)
	filename = makeName(dir,p.meta,suffix)
	print "Saving FinalProduct..."+filename
	FitsArchive().save(filename,p)
	del(p)
	
	print nbTurn




# # Choose one observation and get the obs product...
# > storage=ProductStorage(["1342183678_nodrift_correction"])
# > obsid=0x500018FEL

# # Retrieve obs context
# > query=MetaQuery(ObservationContext,"p","p.meta['obsid'].value==%iL"%obsid)
# > refs=storage.select(query)
# > obs=refs[0].product

# # obs    : observation context of interest
# # suffix : suffix to add to the filename (default none)
# # dir    : directory prefix to save the fits file 

# To save all maps of a observationnal context at once :
# > execfile('save_maps.py')
# > saveMaps(obs,'_all','/home/myplace/')


def exportThermistor(obs, indexLeg,dir='/data/glx-herschel/data1/herschel/SANEPIC_Fits/L1_Sanepic_SPIRE/',p=None):	
	chanMask=obs.calibration.phot.chanMask
	chanNum=obs.calibration.phot.chanNum
	oneLine=obs.level1.getProduct(indexLeg[0])
	goodChan=[]
	#  TODO See also spire/ia/pipeline/util/AbstractMapperTask.java (ISSLOW & ISNOCHOPSKY)
	for chan in oneLine.channelNames:
		if chanNum.isThermistor(chan):
			goodChan.append(chan)
	nChan = len(goodChan)
	print " ... Retrieving the Thermistors"
	signals={}
	oneLine=obs.level1.getProduct(indexLeg[0])
	sampleTime=oneLine.sampleTime
        # Retrieve the unit of the signal (save it for later)
	signals[chan]=oneLine.getSignal(chan)
	for i in indexLeg[1:]:
		oneLine=obs.level1.getProduct(i)
		sampleTime.append(oneLine.sampleTime)
		for chan in goodChan:
			signals[chan].append(oneLine.getSignal(chan))
	sign=Double2d(nChan,sampleTime.length())
	for i in range(nChan):
                iSig = signals[goodChan[i]].to().double1d()
                sign[i,:] = iSig
                del(iSig)
        del(signals)
        del(chanNum)
       	p=exportMetadata(obs,indexLeg)
        p["temperature"]=ArrayDataset(sign)
        p["temperature"].meta["unit"] = StringParameter(unitSignal.getName(),"unit of signal")
        del(unitSignal)
        p["time"]=ArrayDataset(sampleTime)
	suffix="_thermistor.fits"
        filename = makeName(dir,p.meta,suffix)
        print "Saving FinalProduct..."+filename
        FitsArchive().save(filename,p)
	return p["temperature"]
        del(p)

