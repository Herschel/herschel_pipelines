from process import *

storage="OT1_pogle01_1_SPIRE"
obsids=[
1342238327L
]

process(useHsa=True,poolBaseName="OT1_pogle01_1",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(useHsa=True,poolBaseName="OT1_pogle01_1",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(poolName=storage,poolBaseName="OT1_pogle01_1",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#process(poolName=storage,poolBaseName="DDT_mustdo",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCombine=False)
