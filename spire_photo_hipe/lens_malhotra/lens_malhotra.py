from process import *
import sys

#1342210497 GT1_dlutz_4
#1342200237 GT1_dlutz_4
#1342210503 GT1_dlutz_4
#1342213197 GT1_dlutz_4
#1342206225 GT1_dlutz_4
#1342203596 GT1_dlutz_4
#1342200240 GT1_dlutz_4
#1342188583 KPOT_eegami_1
#1342233336 OT1_gstacey_3
#1342255035 OT2_smalhotr_3 
#1432247971 OT2_eegami_6 -> available 12/01/2013
#1342255112 OT2_smalhotr_3 


storage="lens_malhotra_SPIRE"
obsids=[\
1342210497,\
1342200237,\
1342210503,\
1342213197,\
1342206225,\
1342203596,\
1342200240,\
1342188583,\
1342233336,\
1342255035,\
1342255112,\
1342247971\
]

#~ process(useHsa=True,poolBaseName="lens_malhotra",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(useHsa=True,poolBaseName="lens_malhotra",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(poolName=storage,poolBaseName="lens_malhotra",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#process(poolName=storage,poolBaseName="DDT_mustdo",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCombine=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="lens_malhotra", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="lens_malhotra", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="lens_malhotra", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
