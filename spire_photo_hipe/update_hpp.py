import os
from IAS_tools import *
from process import *

hpp_dir= '/data/glx-herschel/data1/herschel/data/corrected_pointing_od320_761'

poolName = "polaris"+"_SPIRE_test"
obsids = getObsIds(poolName)
obsid = obsids[2]


obs = getObs(poolName,obsid)
od = obs.meta['odNumber']
corrected_hpp = fitsReader(os.path.join(hpp_dir,'pointing_od_0'+str(od.value)+'.fits'))
obs.auxiliary.pointing = corrected_hpp
saveObs(obs,poolName)

process(useHsa=False,poolName=poolName, obsids=[obsid], poolBaseName=poolName, doPipeline=True,doTdf=True, doSavePool=True, doExport=True,suffixMap='testhpp')
