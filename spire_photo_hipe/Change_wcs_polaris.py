# just open a pool to see data in hipe


poolName="polaris"
print  listObsIds(poolName)
listpool=[poolName]
obs=getObs(poolName,0x500022F9)
obs2=getObs(poolName,0x500022FAL)


execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')
meta=obs.getMeta()
suffix=''
print meta['raNominal'].value
resolution=2.
wcs=createWcs(2.,7499,720.,3872,305.,meta)
level2_test=combineMaps(listpool,resolution=resolution,wcs=wcs)
saveMaps(level2_test,suffix+'_wcs_resolution2_combined')
resolution=6.
naxis1=2403
naxis2=2501
crpix1=1292
crpix2=1020
wcs=createWcs(resolution,naxis1,naxis2,crpix1,crpix2,meta)
level2_test2=combineMaps(listpool,resolution=resolution,wcs=wcs)
saveMaps(level2_test2,suffix+'_wcs_resolution6__recentered_combined')

