# Masking & map-making parameter
whole   = False
rad     = 2
sample  = 50
channel = 'PSW'
order   = 1

# Load obs, level1 and PSW map
obs      = getObservation(1342192101, useHsa=1, instrument='SPIRE')
level1   = obs.level1
map_orig = obs.level2.refs[channel].product


###################################
# FUNCTION DEFINITION
###################################
def maskTimeline(input, nSamples=None, ra=None, dec=None, radiusArcmin=None, unmask=False):
	"""Mask all except the first and last nSamples in each scan,
	and all within radiusArcmin of (ra, dec).
	"""
	try: # Treat it as a context
		for i in range(input.count):
			input.getRefs().set(i, ProductRef(maskTimeline(input.getProduct(i), nSamples, ra, dec, radiusArcmin, unmask)))
	except: # pointed product
		for channel in input.getChannelKeySet():
			mask = input.getMask(channel)
			if nSamples is not None:
				# Mask all except the first and last nSamples in each scan
				if unmask:
					mask[nSamples:-nSamples] = SpireMask.MASTER.unset(mask[nSamples:-nSamples])
				else:
					mask[nSamples:-nSamples] = SpireMask.MASTER.set(mask[nSamples:-nSamples])
			if ra is not None and dec is not None and radiusArcmin is not None:
				# Mask all within radiusArcmin of (ra, dec)
				raTimeline = input.getRa(channel)
				decTimeline = input.getDec(channel)
				d2 = ((raTimeline - ra) * COS(dec * Math.PI / 180.)) ** 2 + (decTimeline - dec) ** 2
				maskRegion = d2 < (radiusArcmin / 60.) ** 2
				if unmask:
					mask[mask.where(maskRegion)] = SpireMask.MASTER.unset(mask[mask.where(maskRegion)])
				else:
					mask[mask.where(maskRegion)] = SpireMask.MASTER.set(mask[mask.where(maskRegion)])
			input.setMask(channel, mask)
		return input


########################################
# TEST MASKING AND BASELINE POLY-FITTING
########################################
from herschel.spire.ia.pipeline.phot.baseline import *
baselineRemovalPolynomial = BaselineRemovalPolynomialTask()

# No masking
scans1 = baselineRemovalPolynomial(input=level1, polyDegree=order, wholeTimeline=whole)
im1    = naiveScanMapper(scans1, array=channel, wcs=map_orig.getWcs())

# Mask all except the first and last nSamples in each scan
maskTimeline(level1, nSamples=sample)
scans2 = baselineRemovalPolynomial(input=level1, polyDegree=order, wholeTimeline=whole)
maskTimeline(level1, nSamples=sample, unmask=True)
maskTimeline(scans2, nSamples=sample, unmask=True)
im2 = naiveScanMapper(scans2, array=channel, wcs=map_orig.getWcs())

# Mask all within radiusArcmin of (ra, dec)
ra  = im1.getWcs().getCrval1() # Centre
dec = im1.getWcs().getCrval2() # Centre
maskTimeline(level1, ra=ra, dec=dec, radiusArcmin=rad)
scans3 = baselineRemovalPolynomial(input=level1, polyDegree=order, wholeTimeline=whole)
maskTimeline(level1, ra=ra, dec=dec, radiusArcmin=rad, unmask=True) 
maskTimeline(scans3, ra=ra, dec=dec, radiusArcmin=rad, unmask=True) 
im3 = naiveScanMapper(scans3, array=channel, wcs=map_orig.getWcs())

dif_1_2 = imageSubtract(image1=im1,ref=1,image2=im2)
dif_1_3 = imageSubtract(image1=im1,ref=1,image2=im3)
dif_2_3 = imageSubtract(image1=im2,ref=1,image2=im3)
