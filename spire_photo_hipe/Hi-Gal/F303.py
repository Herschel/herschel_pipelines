from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe

##
obsids=[1342189081L,1342189082L]
storage="Hi-Gal_SPIRE"
process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True, suffixMap='_F303')
process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True, suffixMap='_F303')
process(poolName=storage,poolBaseName="Hi-Gal",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True, suffixMap='_F303')
