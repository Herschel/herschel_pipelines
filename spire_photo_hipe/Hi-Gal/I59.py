from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe

##
obsids=[1342186236L,1342186235L]
storage="Hi-Gal_l59_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_l59", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_l59", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_l59",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
