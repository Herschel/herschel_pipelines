from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe

##
#obsids=[1342186275L,1342186276L]
#storage="Hi-Gal_l30_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_l30", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_l30", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_l30",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#########
#obsids=[1342204102L,1342204103L]
#storage="Hi-Gal_Field0_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field0_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field0_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_Field0_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
########
#obsids=[1342204104L,1342204105L]
#storage="Hi-Gal_Field2_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field2_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field2_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_Field2_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
###
#obsids=[1342214761L,1342214762L]
##storage="Hi-Gal_Field4_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field4_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field4_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_Field4_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
###
#obsids=[1342214763L,1342214764L]
##storage="Hi-Gal_Field6_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field6_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field6_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_Field6_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
###
#obsids=[1342218963L,1342218964L]
##storage="Hi-Gal_Field8_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field8_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field8_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
##process(poolName=storage,poolBaseName="Hi-Gal_Field8_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#

#Target Name: Field 11_0
#OD=695
#obsids=['1342218965','1342218966']
#
#Target Name: Field 13_0
#OD=696
#obsids=['1342218999','1342219000']
#
#Target Name: Field 15_0
#OD=696
#obsids=['1342218997','1342218998']
#
#Target Name: Field 17_0
#OD=696
#obsids=['1342218995','1342218996']

#### Addede from B. Bertincourt 11/06/14 Hi-Gal_supplemnt.py
obsids=[1342204046L,1342204047L] 
storage="Hi-Gal_Field332_0_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field332_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field332_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,poolBaseName="Hi-Gal_Field332_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)

#obsids=[1342204054L,1342204055L] 
#storage="Hi-Gal_Field334_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field334_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field334_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field334_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342204056L,1342204057L] 
#storage="Hi-Gal_Field336_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field336_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field336_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field336_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342204058L,1342204059L] 
#storage="Hi-Gal_Field338_0_SPIRE"
##process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field338_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field338_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field338_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342218692L,1342218693L] 
#storage="Hi-Gal_Field33_0_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field33_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field33_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field33_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342219630L,1342219631L] 
#storage="Hi-Gal_Field35_0_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field35_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field35_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field35_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342207026L,1342207027L] 
#storage="Hi-Gal_Field37_0_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field37_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field37_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field37_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
#
#obsids=[1342207028L,1342207029L] 
#storage="Hi-Gal_Field39_0_SPIRE"
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field39_0", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,obsids=obsids,poolBaseName="Hi-Gal_Field39_0", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="Hi-Gal_Field39_0",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
