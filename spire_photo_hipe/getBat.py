# execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/getBat.py')

def getBat(obs):
	allBat = TableDataset()
	bsmPos=obs.calibration.phot.bsmPos
	# Retrieve all the bat table
	bbids=obs.level0_5.getBbids(0xa103)
	nlines=len(bbids)
	print "number of scan lines:",nlines
	# Loop over scan lines
	for bbid in bbids:
		block=obs.level0_5.get(bbid)
		# print "processing BBID="+hex(bbid)
		# Now move to engineering data products
		pdt  = block.pdt
		nhkt = block.nhkt
		if pdt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block."
			continue
		if nhkt == None:
			logger.severe("Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block.")
			print "Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block."
			continue
		#
		# access and attach turnaround data to the nominal scan line
		bbCount=bbid & 0xFFFF
		pdtLead=None
		nhktLead=None
		pdtTrail=None
		nhktTrail=None
		if bbCount >1:
			blockLead=level0_5.get(0xaf000000L+bbCount-1)
			pdtLead=blockLead.pdt
			nhktLead=blockLead.nhkt
			if pdtLead != None and pdtLead.sampleTime[-1] < pdt.sampleTime[0]-3.0:
				pdtLead=None
				nhktLead=None
		if bbid < MAX(Long1d(bbids)):
			blockTrail=level0_5.get(0xaf000000L+bbCount)
			pdtTrail=blockTrail.pdt
			nhktTrail=blockTrail.nhkt
			if pdtTrail != None and pdtTrail.sampleTime[0] > pdt.sampleTime[-1]+3.0:
				pdtTrail=None
				nhktTrail=None
		pdt=joinPhotDetTimelines(pdt,pdtLead,pdtTrail)
		nhkt=joinNhkTimelines(nhkt,nhktLead,nhktTrail)
		#
		# calculate BSM angles
		bat=calcBsmAngles(nhkt,bsmPos=bsmPos)
		
		# Add to master Table
		allBat.addRowsByName(bat.table)
	return allBat
