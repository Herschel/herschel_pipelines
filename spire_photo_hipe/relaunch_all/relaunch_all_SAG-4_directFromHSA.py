from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe
# direct from HSA : get psrc and extd Maps for PSW PMW PLW saved in MAPS_SPIRE
###### SAG-4
observations = {
"HD37041_SPIRE":[1342192101L],
"n2023_SPIRE":[1342203630L],
"Ced201-2_SPIRE":[1342195987L],
"l1721-5_SPIRE":[1342192059L],
"rho_oph_h2_SPIRE":[1342203573L],
"california-1_SPIRE":[1342192090L],
"california-2_SPIRE":[1342192092L],
"california-3_SPIRE":[1342192091L],
"IC59-centre_SPIRE":[1342192089L],
"HH_dense_core_SPIRE":[1342192100L],
"n7023_int_SPIRE":[1342183680L],
"sh241MSX_SPIRE":[1342192093L],
"rcw120_SPIRE":[1342183678L],
"rcw82_SPIRE":[1342192053L],
"rcw79_SPIRE":[1342192054L],
"Sh104_SPIRE":[1342185535],
"RCW71_SPIRE":[1342203561],
"IC63_int_SPIRE":[1342203611L],
"IRAS16132-5039_SPIRE":[1342192055L],
"AFGL4029shift_SPIRE":[1342192088L],
"J2142-4423_SPIRE":[1342210526L]}

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
for (storage, obsids) in observations.iteritems():
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False)
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True)
	# to export L1 only
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False)

print "SAG-4 mono-obs completed successfully."

