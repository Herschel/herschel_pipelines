from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe
# direct from HSA : get psrc and extd Maps for PSW PMW PLW saved in MAPS_SPIRE
###### SAG-4
observations = {
"Abell773_SPIRE":[1342193798L],
}

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
for (storage, obsids) in observations.iteritems():
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False)
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True)
	# to export L1 only
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False)

print "SAG-4 mono-obs completed successfully."

