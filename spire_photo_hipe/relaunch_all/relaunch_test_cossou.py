from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe
# direct from HSA : get psrc and extd Maps for PSW PMW PLW saved in MAPS_SPIRE
###### SAG-4
#~ obsids=[1342192101L]
#~ storage="HD37041_SPIRE"
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=True,doDestriper=True,doApplyExtendedEmissionGains=True,doSavePool=False,doExport=True, suffixMap='test_cossou')

# With or without destriping
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=True,doDestriper=True,doApplyExtendedEmissionGains=True,doSavePool=False,doExport=True, suffixMap="_cossou_destrip")
#~ process(poolName=storage,obsids=obsids,useHsa=True,doPipeline=False, doLevel1=True,doDestriper=False,doApplyExtendedEmissionGains=True,doSavePool=False,doExport=True,suffixMap="_cossou_nodestrip")

# Test photometer_2pass_pipeline. The script is automatically called when the doPipeline keyword is True and doTdf=True but noTimeRespCorr=False and doCoolerBurpCorr!=1
obsids=[1342200114L,1342200115L]
storage="spica_SPIRE"
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True, suffixMap="cossou_test")

#
