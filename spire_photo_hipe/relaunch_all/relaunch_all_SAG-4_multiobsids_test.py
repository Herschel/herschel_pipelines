from process import *

obsids=[1342200114L,1342200115L]
process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doExtdMaps=True,doDestriper=True,doLevel1=False)
process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False)
process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)

