from IAS_tools import *
from export import *
from process import *

#execfile('/data/glx-herschel/data1/herschel/hipe_v13.0.0/src/herschel/spire/ia/pipeline/scripts/twopass/SpirePhotPipelineTaskIAS.py')
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/process.py')
execfile('/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py')

obsids=[1342183678L]
#storage="rcw120_SPIRE_L07"
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=True,suffix="_L07",script="Photometer_2Pass_Pipeline_L07")
#process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=False,doDestriper=True,suffix="_ExtZeroPointCorrection")
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=False,doDestriper=True,suffix="_L07")
#storage="rcw120_SPIRE_L1"
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=False,doDestriper=True,suffix="_L1")
#process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)
#process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False,doExtdMaps=True,suffixMap="_ExtZeroPointCorrection")
#process(poolName=storage,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,suffixMap="_naiveOnDestriped",suffix="_L1")
#process(poolName=storage,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,suffixMap="_ExtZeroPointCorrection",doExtdMaps=True,suffix="_L1")

storage="rcw120_SPIRE_L1"
process(poolName=storage,obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False,doExtdMaps=True,suffixMap="_ExtZeroPointCorrection",suffix="_L1")

