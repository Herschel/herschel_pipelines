from process import *
####### SAG-4
# relaunch all with a new version of Hipe

multiobs = {"DC300-17_SPIRE":[1342211293L,1342212300L],
"UrsaMajor_SPIRE":[1342206692L,1342206693L],
"IVCG86_SPIRE":[1342197280L,1342197281L],
"spica_SPIRE":[1342200114L,1342200115L]}

# Theses lines are valid only for multiobs
for (storage, obsids) in multiobs.iteritems():
#	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doExtdMaps=True, doDestriper=True, doLevel1=False)
#	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doDestriper=True, doLevel1=True)
#	process(obsids=obsids, useHsa=True, doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doApplyExtendedEmissionGains=True, doDestriper=True, doLevel1=True)
	#Export L1 only
	process(poolName=storage, obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doApplyExtendedEmissionGains=True,doTheMaps=False) 

print "SAG-4 multiobsids completed successfully."

#~ 
#~ obsids=[1342211293L,1342212300L]
#~ storage="DC300-17_SPIRE"
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doExtdMaps=True,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)
#~ 
#~ #
#~ #
#~ obsids=[1342206692L,1342206693L]
#~ storage="UrsaMajor_SPIRE"
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doExtdMaps=True,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)
#~ 
#~ #
#~ #
#~ obsids=[1342197280L,1342197281L]
#~ storage="IVCG86_SPIRE"
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doExtdMaps=True,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)
#~ 
#~ #
#~ #
#~ obsids=[1342200114L,1342200115L]
#~ storage="spica_SPIRE"
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doExtdMaps=True,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doDestriper=True,doLevel1=False)
#~ process(obsids=obsids,useHsa=True,doPipeline=False, doTdf=True, doSavePool=False, doExport=False,doApplyExtendedEmissionGains=True,doDestriper=True,doLevel1=False)

