from IAS_tools import *
from export import *
from process import *

# relaunch all with a new version of Hipe

obsids=[1342195262L,1342195263L,1342195264L,1342195265L,1342195266L,1342195267L,1342195268L,1342195269L,\
1342195270L,1342195271L,1342195272L,1342195273L,1342195274L,1342195275L,1342195276L,1342195277L,1342195278L,1342195279L,1342195280L,1342195281L,\
1342195310L,1342195311L,1342195312L,1342195313L,1342195314L,1342195315L,1342195316L,1342195317L,1342195318L,1342195319L,1342195320L,1342195321L,\
1342195716L,1342195717,1342195718L,1342195719L,1342195724L,1342195725L]
storage="GOODS-H_GOODS-N_SPIRE"
process(useHsa=True,poolBaseName="GOODS-H_GOODS-N",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,poolBaseName="GOODS-H_GOODS-N",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,poolBaseName="GOODS-H_GOODS-N",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#
