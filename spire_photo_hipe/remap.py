from IAS_tools import *
ELAIS_N1_SCUBA_combined_PSW_old = fitsReader(file = '/home/bhasnoun/Glx_herschel/HIPE_Fits/MAPS_SPIRE/SAG-1/ELAIS-N1-SCUBA/ELAIS-N1-SCUBA_combined_PSW_old.fits')
wcs=ELAIS_N1_SCUBA_combined_PSW_old.getWcs()
poolName="ELAIS-N1-SCUBA_SPIRE"
obsids = getObsIds(poolName)
level2=combineMaps(["ELAIS-N1-SCUBA_SPIRE"],obsIds=obsids,wcsPSW=wcs)
