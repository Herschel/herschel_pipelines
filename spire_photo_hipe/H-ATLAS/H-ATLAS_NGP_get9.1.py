from process import *

storage="NGP_SPIRE"
obsids=[
1342222677,\
1342222676,\
1342222626,\
1342211292,\
1342210947,\
1342210946,\
1342210932,\
1342210931,\
1342210918,\
1342210917,\
1342210903,\
1342210902,\
1342210568,\
1342210567,\
1342210559,\
1342210558
]
#for obsid in obsids:
#       obs=getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
#       saveObservation(obs,poolName='NGP_SPIRE_v9_1',saveCalTree=True)
#process(useHsa=True,poolBaseName="NGP",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=True,doCoolerBurpCorr=1)
#process(useHsa=True,poolBaseName="NGP",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(useHsa=True,poolBaseName="NGP",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="NGP",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,poolBaseName="NGP",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
storage="NGP_SPIRE_v9_1"
process(storage,obsids=obsids,poolBaseName="NGP",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,suffixMap="L2v10",suffix="_v9_1")
