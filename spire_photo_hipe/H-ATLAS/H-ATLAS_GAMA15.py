from process import *

#storage="GAMA15_SPIRE"
obsids=[1342189845L,1342189846L,1342190254L,1342190255L,1342189661L,1342189662L,1342189691L,1342189692L]
#for obsid in obsids:
#	obs=getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
#	saveObservation(obs,poolName='GAMA15_SPIRE_v9_1',saveCalTree=True)
#process(useHsa=True,poolBaseName="GAMA15",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=True,doCoolerBurpCorr=1)
# test to run coolBurpCorr with 9.2: failed
#process(storage,poolBaseName="GAMA15",obsids=obsids, useHsa=True,doPipeline=False,doPipelineTest=True, doTdf=True, doSavePool=False, doExport=False, doAll=True,suffixMap="L2v9.2",suffix="_v9_1",doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=1)

#storage="GAMA15_SPIRE_CoolerBurpCorr"
#process(storage,obsids=obsids,poolBaseName="GAMA15",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=1,suffix="_CoolerBurpCorr")
#process(storage,obsids=obsids,poolBaseName="GAMA15",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,suffixMap="L2v10",suffix="_v9_1")
#process(poolName=storage,obsids=obsids,poolBaseName="GAMA15",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,suffixMap="L1v9.1",suffix="_v9_1")

storage="GAMA15_SPIRE"
process(useHsa=True,poolBaseName="GAMA15",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,poolBaseName="GAMA15",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,poolBaseName="GAMA15",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
process(poolName=storage,poolBaseName="GAMA15",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=2)
