from process import *

storage="GAMA9_SPIRE"
obsids=[1342187170L,1342187171L,1342197307L,1342197308L,1342197282L,1342197283L,1342196885L,1342196886L]
#for obsid in obsids:
#       obs=getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
#       saveObservation(obs,poolName='GAMA9_SPIRE_v9_1',saveCalTree=True)
#obsids=[1342197282L,1342197283L]
#process(useHsa=True,poolBaseName="GAMA9",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=False, doAll=True,doCoolerBurpCorr=2)
#process(useHsa=True,poolBaseName="GAMA9",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,poolBaseName="GAMA9",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doTheMaps=False)
#process(poolName=storage,obsids=obsids,poolBaseName="GAMA9",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doCoolerBurpCorr=2)
#process(poolName=storage,poolBaseName="GAMA9",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=2)
#storage="GAMA9_SPIRE_v9_1"
#process(storage,obsids=obsids,poolBaseName="GAMA9",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,suffixMap="L2v10",suffix="_v9_1")

