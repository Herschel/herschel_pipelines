from process import *

storage="GAMA12_SPIRE"
#question on r03 patch up 1342224030
#question on failed r4 (1342211368 1342211369) 
obsids=[1342200112L,1342200113L,1342200121L,1342200122L,1342211317L,1342211318L,\
1342222625L,1342224138L,1342224030L]
#for obsid in obsids:
#       obs=getObservation(obsid=obsid,useHsa=True,instrument="SPIRE")
#       saveObservation(obs,poolName='GAMA12_SPIRE_v9_1',saveCalTree=True)
process(useHsa=True,poolBaseName="GAMA12",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
#process(poolName=storage,poolBaseName="GAMA12",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True)
process(useHsa=True,poolBaseName="GAMA12",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doTheMaps=False)
#process(poolName=storage,poolBaseName="GAMA12",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
#process(poolName=storage,poolBaseName="GAMA12",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=2)

#storage="GAMA12_SPIRE_CoolerBurpCorr"
#process(storage,obsids=obsids,poolBaseName="GAMA12",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCoolerBurpCorr=1,suffix="_CoolerBurpCorr")
#storage="GAMA12_SPIRE_v9_1"
#process(storage,obsids=obsids,poolBaseName="GAMA12",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,suffixMap="_L2v10",suffix="_v9_1")

