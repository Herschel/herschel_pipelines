# Import all needed classes
from herschel.spire.all import *
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter


# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *

# Input definition
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *
from herschel.spire.ia.scripts.tools.obsLoader import *
from herschel.ia.pal.util import StorageResult
from os import path, makedirs
import glob
import string
#hcss.ia.pal.version = none

# get export2sanepic
from export import * 
from IAS_tools import *
#execfile('/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py')


#doLevel1=False do only the maps
#doTheMaps = False = Level1 only
#doSaveMaps=False go with doSavePool to save the maps only in the pools.

def process(poolName=None, obsids=None, poolBaseName=None,useHsa=True,doPipeline=False, doPipelineTest=False,doTdf=True,noTimeRespCorr=False, doSavePool=False, doExport=False, doAll=True,doTheMaps=True, doBaseline=False, doRmBaselinePerLeg=False, doRmBaselinePerScan=False,doPolynomial=False, doDestriper=False,doApplyExtendedEmissionGains=False,doCombine=True,suffix='',suffixMap='',doJumpThresh=False,listChan=None,doCoolerBurpCorr=0,doNoise=False,doLevel1=True,doSaveMaps=True,script="Photometer_2Pass_Pipeline",doExtdMaps=False,doMapsFromHSA=False, outDir=None):
	if not obsids:
		# find the obsid
		obsids=getObsIds(poolName)
	
	# Must have the two.
	if doExtdMaps : doApplyExtendedEmissionGains=True
	
	if (outDir==None):
		dir = '/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE/'
	else:
		# We ensure that the last character is the separator between folders
		if (outDir[-1] != "/"):
			outDir += "/"
		dir = outDir
	suffix = suffix 
	wcsPSW=""
	wcsPMW=""
	wcsPLW=""
	if not doTdf: suffix += '_notdf'
	if not poolBaseName or poolBaseName =='':
		objects=[]
		for obsid in obsids:
			if useHsa: 
				obs=getObservation(obsid,useHsa=True,instrument="SPIRE")
				print "get obs from HSA", obsid
			else: obs = getObs(poolName,obsid)
			objects.append(string.join(obs.meta['object'].value.split(),''))
		poolBaseName = objects[0]
	
	if doLevel1:
		for obsid in obsids:
			if useHsa: obs=getObservation(obsid,useHsa=True,instrument="SPIRE")
			else:
				print "obsid ",obsid
				obs = getObs(poolName,obsid)
			print "Processing " + makeName('',obs.meta,suffix=suffix) + "..."
			if doPipeline:
				print " ... launching the pipeline..."
				if doTdf:
					if noTimeRespCorr:
						print "without bolo time constant correction, and no turn over"
						execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_nobolo_noturnover.py')
						#suffix='_noTimeRespCorr_noLpf'
						suffix='_noTimeRespCorr_noLpf_with_turnovers'
					elif doCoolerBurpCorr==1:
						# doCoolerBurpCorr==1 Cooler Burp Correction from L0 to L1
						# doCoolerBurpCorr==2 remove legs with Cooler Burp 
						print " ... with Cooler Burp Correction ..."
						suffix='_CoolerBurpCorr'
						execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/POF5_pipeline_CoolerBurpCorr.py')
					else :
						execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/'+script+'.py')
						#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/POF5_pipeline.py')
						#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/Photometer_2Pass_Pipeline.py')
				else:
					execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/POF5_pipeline_notdf.py')
					
			
			# Run the pipeline TEST TEST TEST works with doPipeline False  ...
			else:
				if doPipelineTest:
						#inputs.mapping = 'naive' 
						#print "double deglitch "      
						#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_double_deglitch.py')
						#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_v5.py')
						#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/POF5_pipeline_polynomial.py')
						print " ... with Cooler Burp Correction ..."
						suffix='_CoolerBurpCorr'
						execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/POF5_pipeline_CoolerBurpCorr_v9.2.py')
						#suffix='_v4jump'
						#suffix='_calibNew'
						#suffix='_rmbPoly_all_deg2'
			# Save it by default...
			obs.meta["scriptID"] = StringParameter("$Rev: 908 $","$Id: process.py 908 2011-07-06 10:11:27Z abeelen $")
			
			if doSavePool:
				# TODO: Erase previous pool if any
				print " ... saving the pool and Level1 ..."
				saveObs(obs,poolBaseName+"_SPIRE"+suffix)
			#else:
				# whatever was inputed first, look to the processed data
				#newStorageName = makeName('',obs.meta,suffix)
			#	newStorageName = poolBaseName+suffix
			#	obs = getObs(newStorageName,obsid)
			
			# export the data in sanepic format     
			if doExport:
				print " ... exporting the pool ..."
				# suffix2 for exported data only
				if  doBaseline:
					suffix2 = suffix+'_rmbaseline'
				else:
					suffix2=suffix
				if  doApplyExtendedEmissionGains:
					suffix2 = suffix2+'_ExtEmiGainsApplied'
				else:
					suffix2=suffix2
				if  doCoolerBurpCorr>0:
					suffix2 = suffix2+'_CoolerBurpCorr'+str(doCoolerBurpCorr)
				else:
					suffix2=suffix2
				if doAll:
					# Stitch all the Level1 legs together
					export2Sanepic(obs,range(obs.level1.count),suffix2+'_all_sanepic.fits',doBaseline=doBaseline,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
					#check if discontinuity between Level1 legs
					holes_box=Find_holes(obs)
					#holes_box=Find_holes(obs)
					if len(holes_box) != 0:
						start=0
						count=0
						for i in holes_box:
							export2Sanepic(obs,range(start,i+1),suffix2+"_part%i_sanepic.fits"%count,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
							start=i+1
							count=count+1
						export2Sanepic(obs,range(start,obs.level1.count),suffix2+"_part%i_sanepic.fits"%count,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
				else:
					# Export each individual Level1 leg
					for i in range(obs.level1.count):
						export2Sanepic(obs,[i],suffix2+"_ppt%3.3i_sanepic.fits"%i,doBaseline=doBaseline,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
######## create combinedMaps
	if doTheMaps:
		suffixMap=suffix+suffixMap
		if doApplyExtendedEmissionGains:
			suffixMap = suffixMap+ '_ExtEmiGainsApplied'
## ExdtMaps = with applyExtdEmissionGains and zeroPoint Correction from Planck
		if doExtdMaps:
			suffixMap = suffixMap+ '_ExtdZeroPoint'
		#if doDestriper:
			#suffixMap = suffixMap+'_destriped'
		if doJumpThresh:
			suffixMap = suffixMap+'_jumpCorrected'
		if doCoolerBurpCorr>0:
                        suffixMap = suffixMap+'_CoolerBurpCorr'+str(doCoolerBurpCorr)
		if len(obsids)>1 and doCombine:
			# scans_combined c'est pour avoir les observations separes, mais avec le meme WCS
			(level2_combined, scans_combined) = combineMaps([poolBaseName+"_SPIRE"+suffix],useHsa=useHsa,obsIds=obsids,doRmBaselinePerLeg=doRmBaselinePerLeg,doRmBaselinePerScan=doRmBaselinePerScan,doDestriper=doDestriper,doPolynomial=doPolynomial,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains,doJumpThresh=doJumpThresh,doCoolerBurpCorr=doCoolerBurpCorr,doExtdMaps=doExtdMaps)
			if doSaveMaps: 
				saveMaps(level2_combined,doExtdMaps,suffixMap+'_combined',[poolBaseName])
			wcsPSW = level2_combined.refs["PSW"].product.wcs
			wcsPMW = level2_combined.refs["PMW"].product.wcs
			wcsPLW = level2_combined.refs["PLW"].product.wcs
			for obsid in obsids:
				#scansObsid=cutLevel1Obsids(scans_combined,obsid)
				scansObsid,NbBurps=cutLevel1Obsids(scans_combined,obsid)
				# DoMap do only a naive scan mapper. The aim is to set a global WCS for each separate map
				level2_remap,scansObsid,diags=doMaps(scansObsid,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,doDestriper=False)
				if doExtdMaps : 
					# If planck correction is applied, a level 2 is generated, thus we need to redo the same operation when redefining the WCS
					colorCorrHfi= obs.calibration.phot.colorCorrHfi
					fluxConvList=obs.calibration.phot.fluxConvList
					level2_remap=CreateExtdMaps(scansObsid,level2_remap,diags,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,colorCorrHfi=colorCorrHfi,fluxConvList=fluxConvList)
				level2_remap.meta["NbBurps"]=LongParameter(NbBurps,"NbCoolerBurps")
				if doSaveMaps: saveMaps(level2_remap,doExtdMaps,suffixMap)
				if doSavePool:
					obs_remap = getObs(poolBaseName+"_SPIRE"+suffix, obsid)
					obs_remap.level2=level2_remap
					obs_remap = createRGB(obs_remap,doDestriper=doDestriper)
					saveObs(obs_remap,poolBaseName+"_SPIRE"+suffix)
		else:     #No combined maps				
			for obsid in obsids:
				print "obsid ",obsid
				#print "doRmBaselinePerLeg,doPolynomial: ",doRmBaselinePerLeg,doPolynomial
				if doMapsFromHSA:
					obs=getObservation(obsid,useHsa=True,instrument="SPIRE")
					level2_remap=obs.level2
					for map in ["psrcPSW","psrcPMW","psrcPLW","extdPSW","extdPMW","extdPLW"]:
						filename = makeName(dir,obs.level2.refs[map].product.meta,suffix)
						filename += '_'+map+'.fits'
						FitsArchive().save(filename,obs.level2.refs[map].product)
						print "HSA Map %s saved as FITS files to %s"%(map,dir)
				else:
					level2_remap = reMap(obsid,poolBaseName+"_SPIRE"+suffix,useHsa=useHsa,listChan_given=listChan,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,doRmBaselinePerLeg=doRmBaselinePerLeg,doRmBaselinePerScan=doRmBaselinePerScan,doDestriper=doDestriper,doPolynomial=doPolynomial,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains,doJumpThresh=doJumpThresh,doNoise=doNoise,doExtdMaps=doExtdMaps)
					if doSaveMaps: saveMaps(level2_remap,doExtdMaps,suffixMap, dir=dir)
				if doSavePool:
					obs_remap = getObs(poolBaseName+"_SPIRE"+suffix, obsid)
					obs_remap.level2=level2_remap
					obs_remap = createRGB(obs_remap,doDestriper=doDestriper)
					saveObs(obs_remap,poolBaseName+"_SPIRE"+suffix)

def transformPV(indir, string, outdir, doAll=True):
	filelist=glob.glob(path.join(indir,"*"+string+"*"))
	filelist.sort()
        #print filelist	
	if not path.isdir(outdir):
		makedirs(outdir)
	
	if doAll:
		export2Sanepic_from_fits(filelist,suffix='_all_sanepic.fits',dir=outdir)
	else:
		for i in range(len(filelist)):
			export2Sanepic_from_fits([filelist[i]],suffix="_ppt%3.3i_sanepic.fits"%i, dir=outdir)


