execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/process.py')

obsids=[1342210526L]
storage="J2142-4423_SPIRE"
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True)
process(useHsa=True,obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True)
process(poolName=storage,obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True)
process(poolName=storage,obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True)
