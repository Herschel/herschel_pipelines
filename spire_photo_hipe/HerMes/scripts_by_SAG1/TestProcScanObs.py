
import os
execfile("ProcScanObsL1.py")
execfile("ProcScanObsL2.py")

obsidlst = [1342183679L, 1342186110L, 1342186108L, 1342186109L, 1342185536L, 1342186123, 1342186294]
pools = ["Abell2218", "LockmanNorth", "LockmanSw", "LockmanSw", "GOODS-N", "FLS", "FLS_2"]
obsidlst = obsidlst[5:]
pools = pools[5:]

calpath = os.environ["HOME"]+"/hcss/calib/"

outpath = os.environ["HOME"]+"/maps/"

suffix = "_map2"

badlist = ['PSWC14', 'PSWD15', 'PSWA13','PSWA11','PSWC12', \
             'PSWG8','PSWG11','PLWA6']


for i in range(len(obsidlst)):
  #  
  LocalPoolIn = pools[i]
  ObsID = obsidlst[i]
  LocalPoolOut = LocalPoolIn + "_L1b"
  #
  ProcScanObsL1(ObsID, LocalPoolIn, LocalPoolOut, calpath, extend=False)
  #
  ProcScanObsL2(ObsID, LocalPoolOut, outpath, suffix, badlist)
  #


# End of File
