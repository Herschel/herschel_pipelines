#=================================================
# Reduce Scan Map
# 
# 09 Aug 2009 Bernhard Schulz for HIPE 1.2 795
# 16 Aug 2009 B. Schulz, test of angular offset cal file
# 08 Sep 2009 B. Schulz  test of 2nd new angular offset cal file
#                        and updated time constant file
# 09 Sep 2009 B. Schulz latest angular offset cal file with all working dets
#                       opt cross correction API updated for 1.2
# 14 Sep 2009 B. Schulz adapted for OD117 data
# 15 Sep 2009 B. Schulz separated Level1 and Level2 outputs
# 16 Sep 2009 B. Schulz changed for OD121 and CIB 1.2.1633
# 22 Sep 2009 B. Schulz moved simple offset subtraction to L2 proc.
# 07 Oct 2009 B. Schulz adapatation for OD134
# 02 Nov 2009 B. Schulz reduced single obs version
# 03 Nov 2009 B. Schulz changed to end with Level 1.0
# 04 Nov 2009 B. Schulz using flux/temp corr products from HIPE
# 05 Nov 2009 B. Schulz subroutine for L1 only
# 09 Nov 2009 B. Schulz bugfix attach level 1 promotion without actual data
#                       fixed indent error and removed mapper from parameters
# 23 Feb 2010 B. Schulz upgrade for CIB 3.0 1108, new deglitcher, and 
#                       79ms timing offset correction
# 
#=================================================


def ProcScanObsL1(ObsID, LocalPoolIn, LocalPoolOut, calpath, extend=False):
  #
  import os
  #
  #
  cal = SpireCal.getInstance()
  #
  # Extract from the observation context the calibration products that
  # will be used in the script
  bsmPos=cal.phot.bsmPos
  lpfPar=cal.phot.lpfPar
  detAngOff=cal.phot.detAngOff
  elecCross=cal.phot.elecCross
  optCross=cal.phot.optCross
  chanTimeConst=cal.phot.chanTimeConst
  chanNum=cal.phot.chanNum
  fluxConvList=cal.phot.fluxConvList
  tempDriftCorrList=cal.phot.tempDriftCorrList
  #
  # To use product sink set to true, otherwise keep false
  tempStorage=Boolean.TRUE   #Boolean.FALSE
  #
  #
  #detAngOff = \
  #  simpleFitsReader(file = calpath+'SCalPhotDetAngOff_S14_v3_BSchulz_special_09Sep2009.fits')
  #bsmPos =  \
  #  simpleFitsReader(file = calpath+'SCalPhotBsmPosDummy_v2.fits')
  #
  #
  #*****************************************************************
  #
  #
  #--------------------------------------------------------------
  from herschel.ia.task.mode import *
  from herschel.ia.pg import ProductSink, MissingDataException 
  from herschel.spire.util import Resource
  from herschel.ia.pal.pool.lstore.util import TemporalPool
  #
  #--------------------------------------------------------------
  # Import Common Glitches Remover
  from herschel.spire.ia.pipeline.common.deglitch import RemoveCommonGlitchesTask
  removeCommonGlitches = RemoveCommonGlitchesTask()
  #
  #--------------------------------------------------------------
  # Import the script tasks.py that contains the task definitions
  from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *
  #
  #--------------------------------------------------------------
  # Import the script obsLoader.py that allows to load an 
  # ObservationContext from a storage.
  from herschel.spire.ia.scripts.tools.obsLoader import *
  #
  #--------------------------------------------------------------
  #
  # start logger
  logger=TaskModeManager.getMode().getLogger()
  #
  #--------------------------------------------------------------
  # Get observation from pool
  storage     = ProductStorage( [LocalPoolIn] )
  storageOut1 = ProductStorage( [LocalPoolOut] )
  #
  q1=Query(ObservationContext, 'obsid == '+ObsID.toString())
  refs = storage.select(q1).toArray()
  if len(refs) == 0:
    raise RuntimeError, "Observation context "+hex(ObsID)+" not found!"
  #
  obs = storage.load(refs[0].urn).product
  # 
  # Extract from the observation context the auxiliary products that
  # will be used in the script
  hpp=obs.auxiliary.pointing
  siam=obs.auxiliary.siam
  #
  #
  # Initialize the ProductSink with a TemporalPool that will be removed when the
  # HIPE session is closed, in case of interactive mode.
  # The TemporalPool is created in a directory starting from the path defined by the
  # var.hcss.workdir property. If this directory is inaccessible or not convenient, please
  # change this property to a proper value.
  if TaskModeManager.getType().toString() == "INTERACTIVE" and tempStorage:
      pname="tmp"+hex(System.currentTimeMillis())[2:-1]
      tmppool=TemporalPool.createTmpPool(pname,TemporalPool.CloseMode.DELETE_ON_CLOSE)
      ProductSink.getInstance().productStorage=ProductStorage(tmppool)
  #
  #--------------------------------------------------------------
  # Make Engineering conversion of level 0 products
  #
  # From Level 0 to Level 0.5
  # only needed if the pool came from obsExporter and no 0.5 level was produced
  # We also use the new time offset of 79 msec
  #
  level0_5= engConversion(obs.level["level0"], \
                          cal=obs.calibration, tempStorage=tempStorage, \
                          offset = 0.079)
  # 
  #
  #--------------------------------------------------------------
  # Add the result to the observation in level 0.5
  #
  obs.level["level0_5"]=level0_5
  #
  #level0_5 = obs.level["level0_5"] #unless it already exists ...
  #
  #--------------------------------------------------------------
  # Create Level1 context
  level1=Level1Context(ObsID)
  #
  #--------------------------------------------------------------
  # Find all building blocks containing scan lines (BBID=0xa103)
  # and loopover them for processing
  #
  bbids = level0_5.getBbids(0xa103)
  #    
  for bbid in bbids:
    block=level0_5.get(bbid)
    print "processing BBID="+hex(bbid)
    #
    #--------------------------------------------------------------
    # extract photometer data and housekeeping data
    #
    pdt = block.pdt
    nhkt = block.nhkt
    #
    # check if essential products are missing and skip if so
    wmessage = "Building block "+hex(bbid)+" doesn't contain a "
    if pdt == None:
      logger.severe(wmessage+ "PDT")
      print wmessage+"PDT"
      continue
    if nhkt == None:
      logger.severe(wmessage+"NHKT")
      print wmessage+"PDT"
      continue
    #
    #--------------------------------------------------------------
    # Append turn around data to avoid ringing
    #
    bbCount=bbid & 0xFFFF
    pdtLead=None
    nhktLead=None
    pdtTrail=None
    nhktTrail=None
    if bbCount >1:
      blockLead=level0_5.get(0xaf000000L+bbCount-1)
      pdtLead=blockLead.pdt
      nhktLead=blockLead.nhkt
      if pdtLead != None and pdtLead.sampleTime[-1] < pdt.sampleTime[0]-3.0:
        pdtLead=None
        nhktLead=None
    if bbid < MAX(Long1d(bbids)):
      blockTrail=level0_5.get(0xaf000000L+bbCount)
      pdtTrail=blockTrail.pdt
      nhktTrail=blockTrail.nhkt
      if pdtTrail != None and pdtTrail.sampleTime[0] > pdt.sampleTime[-1]+3.0:
        pdtTrail=None
        nhktTrail=None
    pdt=joinPhotDetTimelines(pdt,pdtLead,pdtTrail)
    nhkt=joinNhkTimelines(nhkt,nhktLead,nhktTrail)
    #--------------------------------------------------------------
    # calculate BSM angles
    bat=calcBsmAngles(nhkt,bsmPos=bsmPos)
    #
    # create the SpirePointingProduct
    spp = SpirePointingProduct(detAngOff=detAngOff, bat=bat, hpp=hpp, siam=siam)
    #
    # run electrical crosstalk correction
    pdt=elecCrossCorrection(pdt,elecCross=elecCross)
    #
    # run the Wavelet deglitcher
    #pdt=deglitchTimeline(pdt, scaleMin=1.0, scaleMax=8.0, scaleInterval=5, holderMin=-1.9,\
    #      	      holderMax=-0.3, correlationThreshold=0.69)
    #
    # run the Common deglitcher
    pdt = removeCommonGlitches(pdt, chanNum=chanNum, kappa = 2.5, size=15, \
                      correctGlitches=True)
    #
    # run the Sigma Kappa deglitcher
    pdt = simpleDeGlitcher(pdt, iterationNumber=1, gamma=1.0, \
                     kappa=5.0, boxFilterWidth=6, boxFilterCascade=5, \
                     largeGlitchDiscriminatorTimeConstant=2, \
                     largeGlitchRemovalTimeConstant=5, filterType="BOXCAR")
    #
    # run electrical Low Pass Filter response correction
    pdt=lpfResponseCorrection(pdt,lpfPar=lpfPar)
    #
    # run the flux conversion
    fluxConv=cal.phot.fluxConvList.getProduct(pdt.meta["biasMode"].value,pdt.startDate)
    pdt=photFluxConversion(pdt,fluxConv=fluxConv)
    #
    # run the temperature drift correction
    tempDriftCorr=cal.phot.tempDriftCorrList.getProduct(pdt.meta["biasMode"].value,pdt.startDate)
    pdt=temperatureDriftCorrection(pdt,tempDriftCorr=tempDriftCorr)
    #
    # run bolometer time response correction
    pdt=bolometerResponseCorrection(pdt,chanTimeConst=chanTimeConst)
    #
    # run optical crosstalk correction
    pdt=photOptCrossCorrection(pdt,optCross=optCross)
    #
    # add pointing 
    psp=associateSkyPosition(pdt,spp=spp)
    #
    # cut the timeline back to scan line range
    psp=cutPhotDetTimelines(psp,extend=extend)
    #
    # add finished level 1 product to scan context
    if tempStorage:
      ref=ProductSink.getInstance().save(psp)
      level1.addRef(ref)
    else:
      level1.addProduct(psp)
    #
    #
    print "Completed BBID="+hex(bbid)
    del(pdt)
    del(psp)
    del(bat)
    del(spp)
    del(nhkt)
  #
  #--------------------------------------------------------------
  # End of BBID Loop   
  # 
  if level1.count == 0:
    logger.severe("No scan line processed due to missing data. This observation CANNOT be processed!")
    print "No scan line processed due to missing data. This observation CANNOT be processed!"
    raise MissingDataException("No scan line processed due to missing data. This observation CANNOT be processed!")
  #
  #--------------------------------------------------------------
  # promote to LEVEL1_PROCESSED state
  obs.level["level1"]=level1 
  obs.obsState = ObservationContext.OBS_STATE_LEVEL1_PROCESSED
  #
  #--------------------------------------------------------------
  # Put reference to new level 1 context into observation context  
  obs1 = ObservationContext()   #new observation contex  t
  obs1.meta = obs.meta	      #copy meta data
  obs1.level["level1"]=level1 
  #
  # Save observation in Level 1 pool
  urnout = storageOut1.save(obs1)
  #
  print "saved Level 1: OBSID=",ObsID,"("+hex(ObsID)+")" 
  # 
#
# End of script
