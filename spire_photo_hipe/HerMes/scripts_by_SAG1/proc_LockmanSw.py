
import os
#execfile("ProcScanObsL1.py")
#execfile("ProcScanObsL2.py")

obsidlst = [1342186108L, 1342186109L]
pools = ["LockmanSw", "LockmanSw"]


calpath = os.environ["HOME"]+"/hcss/calib/"

outpath = os.environ["HOME"]+"/maps/"

suffix = "_map2"

badlist = ['PSWC14', 'PSWD15', 'PSWA13','PSWA11','PSWC12', \
             'PSWG8','PSWG11','PLWA6']


for i in range(len(obsidlst)):
  #  
  LocalPoolIn = pools[i]
  ObsID = obsidlst[i]
  LocalPoolOut = LocalPoolIn + "_L1b"
  #
  ProcScanObsL1(ObsID, LocalPoolIn, LocalPoolOut, calpath, extend=False)
  #
  #ProcScanObsL2(ObsID, LocalPoolOut, outpath, suffix, badlist)
  #


# End of File
