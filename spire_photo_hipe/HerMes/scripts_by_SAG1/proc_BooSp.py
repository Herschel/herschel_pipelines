
import os
#execfile("ProcScanObsL1.py")
#execfile("ProcScanObsL2.py")

#obsidlst = [1342189108, 1342188681,1342188651,1342188650,1342188682]
#pools = ["BooSp_cp0002", "BooSp_d1-1","BooSp_l1-1","BooSp_r1-1","BooSp_u1-1"]
obsidlst = [1342188650,1342188682]
pools = ["BooSp_r1-1","BooSp_u1-1"]


calpath = os.environ["HOME"]+"/hcss/calib/"

outpath = os.environ["HOME"]+"/maps/"

suffix = "_map2"

badlist = ['PSWC14', 'PSWD15', 'PSWA13','PSWA11','PSWC12', \
             'PSWG8','PSWG11','PLWA6']


#Thermistor jumps in scans for FLS
# PLW 1(T2), 14(T1), 24(T2)
# PMW 33(T1), 50(T1) 
# PSW 17 (T1),  18(T2), 22(T2), 24(T2), 31(T2), 33(T1) 34(T2), 36(T2), 44(T2), 


for i in range(len(obsidlst)):
  #  
  LocalPoolIn = pools[i]
  ObsID = obsidlst[i]
  LocalPoolOut = LocalPoolIn + "_L1b"
  #
  ProcScanObsL1(ObsID, LocalPoolIn, LocalPoolOut, calpath, extend=True)
  #
  #ProcScanObsL2(ObsID, LocalPoolOut, outpath, suffix, badlist)
  #


# End of File
