#=================================================
# Reduce Scan Map
# 
# 09 Aug 2009 Bernhard Schulz for HIPE 1.2 795
# 16 Aug 2009 B. Schulz, test of angular offset cal file
# 08 Sep 2009 B. Schulz  test of 2nd new angular offset cal file
#                        and updated time constant file
# 09 Sep 2009 B. Schulz latest angular offset cal file with all working dets
#                       opt cross correction API updated for 1.2
# 14 Sep 2009 B. Schulz adapted for OD117 data
# 15 Sep 2009 B.Schulz separated Level1 and Level2 outputs
# 16 Sep 2009 B.Schulz changed for OD121 and CIB 1.2.1633
# 22 Sep 2009 B.Schulz moved simple offset subtraction to L2 proc.
# 07 Oct 2009 B. Schulz adapatation for OD134
# 02 Nov 2009 B. Schulz reduced single obs version
# 04 Nov 2009 B. Schulz converted for bulk processing
# 11 Nov 2009 B. Schulz removed clear all
# 11 Nov 2009 B. Schulz added baseline removal option
# 23 Feb 2010 B. Schulz get last observation found in pool
#=================================================

import os
from herschel.ia.numeric.toolbox.filter.Convolution import *

def ProcScanObsL2(ObsID, LocalPoolIn, outpath, suffix, badlist):
  #
  #
  #
  # Output Directory
  # Flag definitions
  MASTER = FixedMask (0, "Master")
  DEAD   = FixedMask (7, "Dead")
  #-----------------------------------------------------------
  # Define function to remove median offsets from PDT signals
  #  
  def remPdtOffsets(pdt):
    detlist = pdt.getChannelNames()
    for det in detlist:
      sig = pdt.getSignal(det)
      sig1 = sig - MEDIAN(sig)
      pdt.setSignal(det,sig1)
  #-----------------------------------------------------------
  # Define function to remove baseline from PDT signals
  #
  def remPdtBaseline(pdt):
    detlist = pdt.getChannelNames()
    for det in detlist:
      sig = pdt.getSignal(det)
      boxcar = BoxCarFilter(300, edge=REPEAT) # ~9.4 arcmin length (53 mHz)
      sig1 = sig - boxcar(sig)
      pdt.setSignal(det,sig1)
  #
  #--------------------------------------------------------------
  # Get observation from pool
  storage     = ProductStorage( [LocalPoolIn] )
  #
  #
  q1=Query(ObservationContext, 'obsid == '+ObsID.toString())
  refs = storage.select(q1).toArray()
  if len(refs) == 0:
    raise RuntimeError, "Observation context "+hex(ObsID)+" not found!"
  #
  obs = storage.load(refs[-1].urn).product
  #
  #--------------------------------------------------------------
  # Create level 2 context
  level2=MapContext()
  #
  #-----------------------------------------------------------
  #  Subtract flatfield from building blocks
  #
  nlevel1=Level1Context()
  nlevel1.meta = obs.level1.meta
  #
  nscan = obs.level1.count
  for iscan in range(nscan):
    psp = obs.level1.refs[iscan].product
    remPdtOffsets(psp)
    #remPdtBaseline(psp)
    # Flag bad detectors out
    for det in badlist:
      msk = psp.getMask(det)
      msk = MASTER.set(msk)
      msk = DEAD.set(msk)
      psp.setMask(det,msk)
    #
    nlevel1.addProduct(psp)
    print "Removing Offset Scan#:", iscan
  #
  #--------------------------------------------------------------
  # Create maps for each detector array and 
  # save observation as FITS files
  #
  arrays = ["PSW", "PMW", "PLW"]
  #
  for i in range(3):
    map=naiveScanMapper(nlevel1, array=arrays[i])
    level2.refs.put(arrays[i], ProductRef(map))
    filename = hex(nlevel1.meta["obsid"].long) + "_" + \
                   arrays[i] + suffix + ".fits"
    simpleFitsWriter(product=level2.refs[arrays[i]].product, \
      file=outpath+filename,warn=False)
    Display(map)
  #
  #
  #--------------------------------------------------------------
  #
  print "saved OBSID=",ObsID,"("+hex(ObsID)+")" 
  del(level2)
  del(nlevel1)
  del(obs)
#
#
# End of script
