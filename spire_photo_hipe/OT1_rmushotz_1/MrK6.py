from process import *

storage="MRK6_SPIRE"
obsids=[
1342229129
]


process(useHsa=True,poolBaseName="MRK6",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(useHsa=True,poolBaseName="MRK6",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(poolName=storage,poolBaseName="MRK6",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#process(poolName=storage,poolBaseName="DDT_mustdo",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCombine=False)
