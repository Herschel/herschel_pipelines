from IAS_tools import *
from export import *
from process import *
from btest import *

obsid1=[1342231359L]
obsid2=[1342231360L]
poolBaseName="spider"

#All Tests:
#doAllBTest(obsids,poolBaseName)

#Tests with rmBaselinePerScan:
#doOneBTest(obsids,poolBaseName,doRmBaselinePerLeg=False,doRmBaselinePerScan=True)

#All Test with rmBaselinePerLeg:
#doOneBTest(obsids,poolBaseName)
#doOneBTest(obsids,poolBaseName,doApplyExtendedEmissionGains=True)
doOneBTest(obsid1,poolBaseName,doPolynomial=True)
doOneBTest(obsid1,poolBaseName,doPolynomial=True,doApplyExtendedEmissionGains=True)

doOneBTest(obsid2,poolBaseName,doPolynomial=True)
doOneBTest(obsid2,poolBaseName,doPolynomial=True,doApplyExtendedEmissionGains=True)

#All Destriper Tests:
#doOneBTest(obsids,poolBaseName,doDestriper=True)
#doOneBTest(obsids,poolBaseName,doDestriper=True,doApplyExtendedEmissionGains=True)
