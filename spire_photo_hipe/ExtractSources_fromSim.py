from herschel.ia.numeric.toolbox.basic import Histogram
from herschel.ia.numeric.toolbox.basic import BinCentres


CGS3_Coadd_200PSW_all = fitsReader(file = '/home/kdassas/Glx_herschel/data/CGS3_Coadd_200PSW_all.fits')
#CGS3_error=fitsReader(file = '/home/kdassas/Glx_herschel/data/CGS3_ErrMap_200PSW_all.fits')
#http://herschel.esac.esa.int/hcss-doc-8.0/load/howtos/html/Dag.ImageAnalysis.HowTo.SourceExtraction.html
#CGS3_Coadd_200PSW_all["error"] = CGS3_error["image"]
CGS3_Coadd_200PSW_all.setError(CGS3_Coadd_200PSW_all.getImage() * 0 + 0.001)
###### 18.0 for PSW, 25.0 for PMW, 36.0 for PLW
beamArea = Math.PI * 18.0 ** 2 / 4 / LOG(2)
CGS3_Coadd_200PSW_all.setUnit("Jy/beam")

################# Sussex Method
resultListSussex= sourceExtractorSussextractor( \
    image = CGS3_Coadd_200PSW_all,        # Image name \
    detThreshold =2000,      # Threshold in signal-to-noise ratio (or log evidence) \
    fwhm = 18.0,            # FWHM of PRF (arcsec) \
    beamArea = beamArea,    # Area of the beam \
    getFilteredMap=True,getPrf=True)

sourceListSussex=resultListSussex[0]
disp = Display(CGS3_Coadd_200PSW_all)
disp.addPositionList(sourceListSussex, java.awt.Color.YELLOW)
filteredMapSussex=resultListSussex[1]
prfSussex=resultListSussex[2]
flux=sourceListSussex["sources"]["flux"]
binsize = 200
print binsize
hist=Histogram(binsize)
bins=BinCentres(binsize)
p2=PlotXY(bins(flux.data),hist(flux.data))
style=p2.getStyle()
style.setChartType(Style.HISTOGRAM)
simpleFitsWriter(product=sourceListSussex, file='/home/kdassas/sourceListSussex_threshold1.0sim.fits')
simpleFitsWriter(product=prfSussex, file='/home/kdassas/prfSussex_threshold1.0sim.fits')
simpleFitsWriter(product=filteredMapSussex, file='/home/kdassas/filteredMapSussex_threshold1.0sim.fits')

################# Daophot method
resultListDaophot= sourceExtractorDaophot( \
    image = CGS3_Coadd_200PSW_all,        # Image name \
    detThreshold =7.5,      # Threshold in signal-to-noise ratio (or log evidence) \
    fwhm = 18.0,            # FWHM of PRF (arcsec) \
    beamArea = beamArea,    # Area of the beam \
    getFilteredMap=True,getPrf=True)

sourceListDaophot=resultListDaophot[0]
filteredMapDaophot=resultListDaophot[1]
prfDaophot=resultListDaophot[2]
disp = Display(CGS3_Coadd_200PSW_all)
disp.addPositionList(sourceListDaophot, java.awt.Color.YELLOW)
flux=sourceListDaophot["sources"]["flux"]
binsize = 200
print binsize
hist=Histogram(binsize)
bins=BinCentres(binsize)
p2=PlotXY(bins(flux.data),hist(flux.data))
style=p2.getStyle()
style.setChartType(Style.HISTOGRAM)

simpleFitsWriter(product=sourceListDaophot, file='/home/kdassas/sourceListDaophot_threshold1.0sim.fits')
simpleFitsWriter(product=prfDaophot, file='/home/kdassas/prfDaophot_threshold1.0sim.fits')
simpleFitsWriter(product=filteredMapDaophot, file='/home/kdassas/filteredMapDaophot_threshold1.0sim.fits')


#obsid=0x50008915
#obs=getObservation(obsid,useHsa=True)
#obs_level2_PSW = obs.refs["level2"].product.refs["PSW"].product
#p=herschel.ia.dataset.image.SimpleImage()
#p["image"]=obs_level2_PSW["image"]
#p["error"]=obs_level2_PSW["image"]
#p["error"].data=p["error"].data*0+0.1
#p.setWcs(obs_level2_PSW.getWcs())
#p.setUnit("Jy/beam")
