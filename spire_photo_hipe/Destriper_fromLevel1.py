#===============================================================================
# Destriper Function for Parallel Mode Observations
#
# Destripe Level 1 from a combination of two observation contexts
# and create new observation context with combined old Level1, 
# destriped as level1.5 and maps as level2
#
# Input: 
# 	obs_1		first observation context with input data
# 	obs_2		second observation context with input data
# Output:
#	obs		observation context with old level 1, destriped 
#                       level 1.5 and destriped maps in level 2
#
# Edition History
# 2011-07-12 B. Schulz initial test version for HIPE 8.0
# 2011-07-12 B. Schulz HIPE 7.0 version
#
#===============================================================================
from IAS_tools import *

poolName="polaris_SPIRE"
obsids = getObsIds(poolName)
print obsids
obs=getObs(poolName,obsids[0])

level1=obs.level1
doDestriper=True
doRmBaselinePerScan = False
doRmBaselinePerLeg = False
scans = SpireListContext()
for obsid in obsids:
	scans = getAllLegs(scans, obsid, poolName, doRmBaselinePerLeg=doRmBaselinePerLeg, doRmBaselinePerScan=doRmBaselinePerScan)
#
listpool=["polaris"]
level2_combined = doMaps(scans,doDestriper=doDestriper)
obsids = obsIdTable(listpool)
level2_combined["obsids"] = obsids
if (listChan_given!=None):
	channels = TableDataset()
	channels["names"] = Column(String1d(listChan_given),description="channels used for processing")
	level2["channels"]=channels
suffix="_destriped"
saveMaps(level2_combined,suffix+'_combined',listpool)
