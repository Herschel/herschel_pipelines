from process import *

storage="OT1_lho_SPIRE"
obsids=[
1342236241L,
1342229127L,
1342229635L,
1342234873L,
1342234901L,
1342234789L,
1342232714L,
1342232715L,
1342236135L,
1342229612L,
1342220871L,
1342220637L
]

process(useHsa=True,poolBaseName="OT1_lho",obsids=obsids, doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(useHsa=True,poolBaseName="OT1_lho",obsids=obsids, doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
process(poolName=storage,poolBaseName="OT1_lho",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
#process(poolName=storage,poolBaseName="OT1_lho",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=True, doAll=True,doApplyExtendedEmissionGains=True,doDestriper=True,doCombine=False)
