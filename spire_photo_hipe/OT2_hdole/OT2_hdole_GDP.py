from process import *
import sys

# relaunch all with a new version of Hipe

##
obsids=[
1342239862L,\
1342239864L,\
1342239865L,\
1342239870L,\
1342239872L,\
1342239879L,\
1342239960L,\
1342239964L,\
1342240029L,\
1342240068L,\
1342240073L,\
1342240084L,\
1342240089L,\
1342240106L,\
1342241114L,\
1342241115L,\
1342241147L,\
1342241148L,\
1342241149L,\
1342241150L,\
1342241153L,\
1342241154L,\
1342241155L,\
1342241166L,\
1342245551L,\
1342245548L,\
1342245429L,\
1342245920L,\
1342247869L,\
1342254477L,\
1342255026L,\
1342256899L,\
1342256879L,\
1342256848L,\
1342256811L,\
1342256751L,\
1342256717L,\
1342256702L,\
1342256642L,\
1342256632L,\
1342257372L,\
1342261519L,\
1342261531L,\
1342261535L,\
1342261555L,\
1342261561L,\
1342261562L,\
1342261587L,\
1342261619L,\
1342261620L,\
1342261629L,\
1342261635L,\
1342261638L,\
1342261640L,\
1342261647L,\
1342254053L,\
1342255094L,\
1342255097L,\
1342255169L,\
1342255180L,\
1342255207L,\
1342255227L,\
1342258390L,\
1342258393L,\
1342259390L,\
1342259393L,\
1342259440L,\
1342259441L,\
1342259476L
]

# Add obsids for defective channels. HIPE is thought to ignore the defective parts
obsids.extend([
1342256899,\
1342256879,\
1342256848,\
1342256811,\
1342256751,\
1342256717,\
1342256702,\
1342256642,\
1342256632
])
storage="OT2_hdole_SPIRE"
#~ process(useHsa=True,obsids=obsids,poolBaseName="OT2_hdole", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ #process(useHsa=True,obsids=obsids,poolBaseName="OT2_hdole", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#~ process(poolName=storage,obsids=obsids,poolBaseName="OT2_hdole",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)

# Theses lines are valid only for single observation (the ones where combining then destriping is not necessary since there's only one obs)
# ,doCombine=False : these are in fact separate observation. We don't want to combine them
process(poolName=storage, poolBaseName="OT2_hdole", obsids=obsids, useHsa=True, doPipeline=False, doMapsFromHSA=True, doSavePool=False, doCombine=False)
process(poolName=storage, poolBaseName="OT2_hdole", obsids=obsids, useHsa=True, doPipeline=False, doLevel1=True, doDestriper=True, doApplyExtendedEmissionGains=True, doSavePool=False, doExport=True, doCombine=False)
# to export L1 only
process(poolName=storage, poolBaseName="OT2_hdole", obsids=obsids, useHsa=True, doPipeline=False, doExport=True, doLevel1=True, doTheMaps=False, doCombine=False)

print("%s executed successfully." % sys.argv[0])
