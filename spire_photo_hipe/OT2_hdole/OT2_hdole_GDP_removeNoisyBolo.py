from process import *

# relaunch all with a new version of Hipe

##
obsids=[
1342256899,\
1342256879,\
1342256848,\
1342256811,\
1342256751,\
1342256717,\
1342256702,\
1342256642,\
1342256632
]
listChan=["PSWB5","PSWF8","PSWE9"]
storage="OT2_hdole_SPIRE"
process(poolName=storage,poolBaseName="OT2_hdole",obsids=obsids,useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doCombine=False,suffixMap="_rmvNoisyBolo",listChan=listChan)
#process(useHsa=True,obsids=obsids,poolBaseName="OT2_hdole", doPipeline=True, doTdf=True, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#process(useHsa=True,obsids=obsids,poolBaseName="OT2_hdole", doPipeline=True, doTdf=False, doSavePool=True, doExport=True, doAll=True,doCombine=False)
#process(poolName=storage,obsids=obsids,poolBaseName="OT2_hdole",useHsa=False,doPipeline=False, doTdf=True, doSavePool=False, doExport=False, doAll=True,doDestriper=True,doCombine=False)
