clear(all=True)


listObjectObsidsBand = {}
listObjectObsidsBand.update( { "OBJECT_BAND" : OBSIDS } )

dirBeamsSuper = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/"
dirSupreme = "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fits/lastBatch/"

def seekSupremeFitsFileName(dir,object,band):
	import subprocess
	ls_proc = subprocess.Popen(['ls',dirFits,"|","grep",object,"|","grep",band,"|","grep",".fits"], stdout=subprocess.PIPE)
	ls_proc.wait()
	fitsAlreadyProduced = ls_proc.stdout.readlines()
	SupremeFitsName = ""
	for fits in fitsAlreadyProduced:
		if fits.count(object)>0 and fits.count(band)>0 and fits.count("SupremePhoto")>0 and fits.count("CoAdd")==0: SupremeFitsName = fits
	return SupremeFitsName


for infoObs, listObsids in listObjectObsidsBand.items():
#	try:
		object = infoObs[:-4]
		band = infoObs[-3:]
		
		fitsname = seekSupremeFitsFileName(dirSupreme,object,band)
		print fitsname
		if fitsname!="":
			
			
			beamName = "0x5000241aL_%s_pmcorr_1arcsec_norm_beam.fits"%band
			bcenter = {'PSW':(700,699), 'PMW':(700,700), 'PLW':(698,700)}
			beamfull = fitsReader(file = os.path.join(dirBeamsSuper,beamName))
			beam = crop(beamfull, int(bcenter[band][0] - 100) , int(bcenter[band][1] - 100), int(bcenter[band][0] + 101), int(bcenter[band][1] + 101))
			
			assert beam.dimensions[0] % 2 == 1, "Beam first dimensions is not odd"
			assert beam.dimensions[1] % 2 == 1, "Beam second dimensions is not odd"
			assert beam.getIntensity(beam.dimensions[0] / 2, beam.dimensions[1] / 2) == MAX(NAN_FILTER(beam.image)), "Beam is not centred on central pixel"
			
			level1Corrected = Level1Context()
			for obsid in listObsids:
				obs = getObservation(obsid, useHsa=True, instrument='SPIRE')
				for ref in obs.level1.refs: level1Corrected.addRef(ref)
			
			SupremeMap = simpleFitsReader(fitsname)
			
			imagesize = [ wcs.naxis1*abs(wcs.cdelt1), wcs.naxis2*abs(wcs.cdelt2) ] #in arcmin
			imagecenter = [ wcs.crval1+(wcs.naxis1/2-wcs.crpix1)*wcs.cdelt1, wcs.crval2+(wcs.naxis2/2-wcs.crpix2)*wcs.cdelt2 ]
			wcs.crval1, wcs.crval2 = imagecenter[0], imagecenter[1]
			wcs.naxis1, wcs.naxis2 = int(imagesize[0]/abs(wcs.cdelt1) + 0.5), int(imagesize[1]/abs(wcs.cdelt2) + 0.5)
			wcs.crpix1, wcs.crpix2 = (wcs.naxis1 + 1) / 2., (wcs.naxis2 + 1) / 2.
			
			level1Corrected = destriper(level1Corrected, array=band, useSink=True)[0]
			hiresImage, hiresBeam = hiresMapper(level1Corrected, beam=beam, wcs=wcs, maxIter=20)
			
			simpleFitsWriter(hiresImage,dirSupreme+fitsname.replace("SupremePhoto","HiRes"))
			
			print object,band
		
		else: print "No Supreme Fits for", object, band