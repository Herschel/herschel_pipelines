#!/usr/bin/env python
# -*-coding:utf-8 -*
from multiprocessing import Pool
import os, sys, subprocess

dirFits = "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fits/lastBatch/"

try:
	nbreCore = int(sys.argv[1])
	herschelMachine = int(sys.argv[2])
	program = sys.argv[3]
	launchJob = sys.argv[4]
except: 
	print """
	Usage:
	To see which object and band will be processed:
	      python combo-launchSupremeAndHiResPhoto.py #nbreCore #herschelMachine program check
	To do the actual processing:
	      python combo-launchSupremeAndHiResPhoto.py #nbreCore #herschelMachine program go
	"""
	sys.exit(0)

listObjectObsids = {}

if program == "SAG-3" or program == "All":
	listObjectObsids.update( { "Orion" : [1342215984,1342215985] } )
	listObjectObsids.update( { "polaris_polaris-2_polaris-1" : [1342186233,1342186234,1342198246,1342198247] } )
	
if program == "SAG-4" or program == "All":
	listObjectObsids.update( { "rcw120" : [1342183678] } )
	listObjectObsids.update( { "UrsaMajor" : [1342206692,1342206693] } )

####################################################################################################################
####################################################################################################################

def launchScriptHIPE(i): 
	os.system("nohup hipe "+i+ " > "+i.replace(".py",".txt")+" &")
	#print "hipe "+i+ " > "+i.replace(".py",".txt")+" &"

####################################################################################################################
####################################################################################################################

ls_proc = subprocess.Popen(['ls',dirFits], stdout=subprocess.PIPE)
ls_proc.wait()
fitsAlreadyProduced = ls_proc.stdout.readlines()

#os.system('rm -rf /data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/comboParallel/*')


listOfProcessing = []
for object in listObjectObsids.keys():
	for band in ["PLW","PSW","PMW"]:
		toDo = True
		for fits in fitsAlreadyProduced:
			if fits.count(object)>0 and fits.count(band)>0 and fits.count("HiRes")>0: toDo = False
		if toDo: listOfProcessing += [object+"_BAND_"+band]

print "To Process:",len(listOfProcessing)
print "Number of core:",nbreCore

toProcessPerScriptFile_tmp = []
for i in range(nbreCore): toProcessPerScriptFile_tmp.append([])
for i in range(len(listOfProcessing)): toProcessPerScriptFile_tmp[i%nbreCore].append(listOfProcessing[i])

toProcessPerScriptFile = [x for x in toProcessPerScriptFile_tmp if x]
nbreCore = len(toProcessPerScriptFile)
for i in toProcessPerScriptFile: print i

template = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/combo-template-hires.py"
tmp = open(template, 'r')
templateString = tmp.read()
tmp.close()

filenameScripts = []
for i in range(nbreCore): 
	#print listfilePerFile[i]
	filenameJob = template.replace("combo-template","comboParallel/comboHires-"+str(i)+"-glx-herschel"+str(herschelMachine))
	filenameScripts.append(filenameJob)
	fichier = open(filenameJob, "w")
	lineToReplace = 'listObjectObsidsBand.update( { "OBJECT_BAND" : OBSIDS } )'
	lineReplacedBy = ""
	for j in toProcessPerScriptFile[i]:
		object = j.split("_BAND_")[0]
		band = j.split("_BAND_")[1]
		newEntry = lineToReplace.replace("OBJECT",object).replace("BAND",band).replace("OBSIDS",str(listObjectObsids[object]))
		lineReplacedBy += newEntry+"\n"
	templateStringJob = templateString.replace(lineToReplace,lineReplacedBy)
	templateStringJob = templateStringJob.replace("FITS_DIRECTORY",dirFits)
	fichier.write(templateStringJob)
	fichier.close()

if launchJob=="go": Pool().map(launchScriptHIPE,filenameScripts)
