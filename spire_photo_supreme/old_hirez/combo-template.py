#clear(all=True)

listObjectObsidsBand = {}
listObjectObsidsBand.update( { "OBJECT_BAND" : OBSIDS } )

AlphaStepValues = {"PSW":3,"PMW":5,"PLW":7}

#useHsa = True
doDestripe = True
doApplyExtendedEmissionGains = False

doSaveL1 = False
doSupreme = False
doNaive = True
doSameResAsSupreme = True
doHiRes = False

StopMode, StopValue = "Manual", 200
OffsetOp = 'GetOffsetLeg'
if doDestripe: OffsetOp = 'ZeroOffset'
PointingOp = "Nearest"

options = "_"+PointingOp
if doDestripe: options += "_destriped"
if doApplyExtendedEmissionGains: options += "_ExtEmiGainsApplied"

dirBeamsSupreme = "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/beams"
dirBeamsSuper = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/"

dirSupreme = "FITS_DIRECTORY"

for infoObs, listObsids in listObjectObsidsBand.items():
#	try:
		object = infoObs[:-4]
		band = infoObs[-3:]
		AlphaStep = AlphaStepValues[band]
		
		data = supremePhotoGetDataFromObs(obsids=listObsids,bandName=band,poolName=object+"_SPIRE",doDestripe=doDestripe,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
		
		level1 = data.refs[0].product.copy()
		del data.refs[0]
		
		object = data.meta["object"].value
		object = object.replace(" ","")
		odNum = data.meta["odNumber"].value
		odNum = str(odNum).replace(" ","")
		
		if doSaveL1:
			for i in range(len(listObsids)):
				simpleFitsWriter(data.refs[i].product,dirSupreme+"OD"+odNum+"_"+str(hex(listObsids[i]))+"_SupremePhoto_"+object+"_"+band+options+"_level1.fits")
		
		if len(listObsids)==1: fitsNameSupreme = "OD"+odNum+"_"+str(hex(listObsids[0]))+"_SupremePhoto_"+object+"_"+band+".fits"
		else: fitsNameSupreme = object+"_combined_SupremePhoto_"+band+".fits"
		
		##### SUPREME #####
		if doSupreme:
			SupremeMap, StdMap, CoAddMap, Extra = supremePhotoMapMaking(data=data,alphaStep=AlphaStep,stopMode=StopMode,stopValue=StopValue,dirBeams=dirBeamsSupreme,pointingOp=PointingOp,offsetOp=OffsetOp)
			
			simpleFitsWriter(SupremeMap,dirSupreme+fitsNameSupreme)
			simpleFitsWriter(CoAddMap,dirSupreme+fitsNameSupreme.replace(".fits","_CoAdd.fits"))
			simpleFitsWriter(Extra,dirSupreme+fitsNameSupreme.replace(".fits","_Extra.fits"))
			
			os.system('mv /home/bhasnoun/dataHipeCell.mat /home/bhasnoun/MatFromSupreme/dataHipeCell_'+object+'_'+band+options+'.mat')
			
			del SupremeMap, StdMap, CoAddMap, Extra
		
		##### HIPE STD #####
		if doNaive:
			if doSameResAsSupreme: 
				mapNaiveScanMapper = naiveScanMapper(level1,array=band,resolution=AlphaStep)
				simpleFitsWriter(mapNaiveScanMapper,dirSupreme+fitsNameSupreme.replace("SupremePhoto","HIPE_STDR_sameResAsSupreme").replace(OffsetOp,"").replace(PointingOp,""))
			else: 
				mapNaiveScanMapper = naiveScanMapper(level1,array=band)
				simpleFitsWriter(mapNaiveScanMapper,dirSupreme+fitsNameSupreme.replace("SupremePhoto","HIPE_STDR").replace(OffsetOp,"").replace(PointingOp,""))
			
			del mapNaiveScanMapper
		
		del data, level1
		
		##### HIGH RES HIPE #####
		if doHiRes and doSupreme:
		
			beamName = "0x5000241aL_%s_pmcorr_1arcsec_norm_beam.fits"%band
			bcenter = {'PSW':(700,699), 'PMW':(700,700), 'PLW':(698,700)} # Positions of peak pixel
			beamfull = fitsReader(file = os.path.join(dirBeamsSuper,beamName))
			beam = crop(beamfull, int(bcenter[band][0] - 100) , int(bcenter[band][1] - 100), int(bcenter[band][0] + 101), int(bcenter[band][1] + 101))
			
			del beamName, bcenter, beamfull
			
			assert beam.dimensions[0] % 2 == 1, "Beam first dimensions is not odd"
			assert beam.dimensions[1] % 2 == 1, "Beam second dimensions is not odd"
			assert beam.getIntensity(beam.dimensions[0] / 2, beam.dimensions[1] / 2) == MAX(NAN_FILTER(beam.image)), "Beam is not centred on central pixel"
			
			level1Corrected = Level1Context()
			for obsid in listObsids:
				obs = getObservation(obsid, useHsa=True, instrument='SPIRE')
				for ref in obs.level1.refs: level1Corrected.addRef(ref)
			
			wcs = SupremeMap.wcs
			
			imagesize = [ wcs.naxis1*abs(wcs.cdelt1), wcs.naxis2*abs(wcs.cdelt2) ] #in arcmin
			imagecenter = [ wcs.crval1+(wcs.naxis1/2-wcs.crpix1)*wcs.cdelt1, wcs.crval2+(wcs.naxis2/2-wcs.crpix2)*wcs.cdelt2 ]
			wcs.crval1, wcs.crval2 = imagecenter[0], imagecenter[1]
			wcs.naxis1, wcs.naxis2 = int(imagesize[0]/abs(wcs.cdelt1) + 0.5), int(imagesize[1]/abs(wcs.cdelt2) + 0.5)
			wcs.crpix1, wcs.crpix2 = (wcs.naxis1 + 1) / 2., (wcs.naxis2 + 1) / 2.
			
			level1Corrected = destriper(level1Corrected, array=band, useSink=True)[0]
			hiresImage, hiresBeam = hiresMapper(level1Corrected, beam=beam, wcs=wcs, maxIter=20)
			
			if len(obsids)==1: fitsNameHiRes = "OD"+str(obs.meta["odNumber"].value)+"_"+str(hex(listObsids[0]))+"_HiRes_"+object+"_"+band+".fits"
			else: fitsNameHiRes = object+"_combined_HiRes_"+band+".fits"
			simpleFitsWriter(hiresImage,dirSupreme+fitsNameHiRes)
		
		print object,band
		
#	except:
#		print object,band," Failed !!"

