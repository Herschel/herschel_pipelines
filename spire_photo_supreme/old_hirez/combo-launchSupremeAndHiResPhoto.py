#!/usr/bin/env python
# -*-coding:utf-8 -*
from multiprocessing import Pool
import os, sys, subprocess

dirFits = "/data/glx-herschel/data1/herschel/SUPREME/SUPREME_PHOTO/HIPE/fits/lastBatch/hipeStdSameResAsSupreme/"

try:
	nbreCore = int(sys.argv[1])
	herschelMachine = int(sys.argv[2])
	program = sys.argv[3]
	launchJob = sys.argv[4]

except: 
	print """
	Usage:
	To see which object and band will be processed:
	      python combo-launchSupremeAndHiResPhoto.py #nbreCore #herschelMachine program check
	To do the actual processing:
	      python combo-launchSupremeAndHiResPhoto.py #nbreCore #herschelMachine program go
	"""
	sys.exit(0)

listObjectObsids = {}

#if program == "SAG-1" or program == "All":
#	listObjectObsids.update( { "ELAIS-N1-SCUBA" : [1342187646,1342187647,1342187648,1342187649,1342187650] } )
#	listObjectObsids.update( { "ADFS" : [1342188096,1342188097] } )
#	listObjectObsids.update( { "Bootes-Spitzer" : [1342188650,1342188651,1342187712,1342187713,1342189108,1342188681,1342188682,1342187711,1342188090] } )
#	listObjectObsids.update( { "XMM_ALL" : [1342189003,1342189004,1342190313,1342190324,1342190325,1342189031] } )
#	listObjectObsids.update( { "FLS" : [1342186123,1342186294] } )
#	listObjectObsids.update( { "Lockman-North" : [1342186110] } )
#	listObjectObsids.update( { "Lockman-SWIRE" : [1342186108,1342186109] } )

if program == "SAG-3" or program == "All":
#	listObjectObsids.update( { "L1688" : [1342205093,1342205094] } )
#	listObjectObsids.update( { "Aquila" : [1342186277,1342186278] } )
	listObjectObsids.update( { "Orion" : [1342215984,1342215985] } )
#	listObjectObsids.update( { "Rosette" : [1342186121] } )
#	listObjectObsids.update( { "S3" : [1342204860,1342204861] } )
#	listObjectObsids.update( { "W3" : [1342216019,1342216020] } )
#	listObjectObsids.update( { "polaris" : [1342186233,1342186234] } )
	listObjectObsids.update( { "polaris" : [1342186233,1342186234,1342198246,1342198247] } )
	
if program == "SAG-4" or program == "All":
#	listObjectObsids.update( { "n2023" : [1342203630] } )
#	listObjectObsids.update( { "IC63_int" : [1342203611] } )
#	listObjectObsids.update( { "rho_oph_h2" : [1342203573] } )
#	listObjectObsids.update( { "RCW71" : [1342203561] } )
#	listObjectObsids.update( { "Ced201-2" : [1342195987] } )
#	listObjectObsids.update( { "HD37041" : [1342192101] } )
#	listObjectObsids.update( { "HH_dense_core" : [1342192100] } )
#	listObjectObsids.update( { "sh241MSX" : [1342192093] } )
#	listObjectObsids.update( { "california-2" : [1342192092] } )
#	listObjectObsids.update( { "california-3" : [1342192091] } )
#	listObjectObsids.update( { "california-1" : [1342192090] } )
#	listObjectObsids.update( { "IC59-centre" : [1342192089] } )
#	listObjectObsids.update( { "AFGL4029shift" : [1342192088] } )
#	listObjectObsids.update( { "l1721-5" : [1342192059] } )
#	listObjectObsids.update( { "IRAS16132-5039" : [1342192055] } )
#	listObjectObsids.update( { "rcw79" : [1342192054] } )
#	listObjectObsids.update( { "rcw82" : [1342192053] } )
#	listObjectObsids.update( { "Sh104" : [1342185535] } )
#	listObjectObsids.update( { "n7023_int" : [1342183680] } )
	listObjectObsids.update( { "rcw120" : [1342183678] } )
#	listObjectObsids.update( { "spica" : [1342200114,1342200115] } )
#	listObjectObsids.update( { "DC300-17" : [1342211293,1342212300] } )
	listObjectObsids.update( { "UrsaMajor" : [1342206692,1342206693] } )
#	listObjectObsids.update( { "IVCG86" : [1342197280,1342197281] } )
	

####################################################################################################################
####################################################################################################################

def launchScriptHIPE(i): 
	os.system("hipe "+i+ " > "+i.replace(".py",".txt")+" &")
	#print "hipe "+i+ " > "+i.replace(".py",".txt")+" &"

####################################################################################################################
####################################################################################################################

ls_proc = subprocess.Popen(['ls',dirFits], stdout=subprocess.PIPE)
ls_proc.wait()
fitsAlreadyProduced = ls_proc.stdout.readlines()

#os.system('rm -rf /data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/comboParallel/*')


listOfProcessing = []
for object in listObjectObsids.keys():
	for band in ["PLW","PSW","PMW"]:
		toDo = True
		for fits in fitsAlreadyProduced:
			if fits.count(object)>0 and fits.count(band)>0: toDo = False
		if toDo: listOfProcessing += [object+"_BAND_"+band]

print "To Process:",len(listOfProcessing)
print "Number of core:",nbreCore

toProcessPerScriptFile_tmp = []
for i in range(nbreCore): toProcessPerScriptFile_tmp.append([])
for i in range(len(listOfProcessing)): toProcessPerScriptFile_tmp[i%nbreCore].append(listOfProcessing[i])

toProcessPerScriptFile = [x for x in toProcessPerScriptFile_tmp if x]
nbreCore = len(toProcessPerScriptFile)
for i in toProcessPerScriptFile: print i

template = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/SuperResolution/combo-template.py"
tmp = open(template, 'r')
templateString = tmp.read()
tmp.close()

filenameScripts = []
for i in range(nbreCore): 
	#print listfilePerFile[i]
	filenameJob = template.replace("combo-template","comboParallel/combo-"+str(i)+"-glx-herschel"+str(herschelMachine))
	filenameScripts.append(filenameJob)
	fichier = open(filenameJob, "w")
	lineToReplace = 'listObjectObsidsBand.update( { "OBJECT_BAND" : OBSIDS } )'
	lineReplacedBy = ""
	for j in toProcessPerScriptFile[i]:
		object = j.split("_BAND_")[0]
		band = j.split("_BAND_")[1]
		newEntry = lineToReplace.replace("OBJECT",object).replace("BAND",band).replace("OBSIDS",str(listObjectObsids[object]))
		lineReplacedBy += newEntry+"\n"
	templateStringJob = templateString.replace(lineToReplace,lineReplacedBy)
	templateStringJob = templateStringJob.replace("FITS_DIRECTORY",dirFits)
	fichier.write(templateStringJob)
	fichier.close()

if launchJob=="go": Pool().map(launchScriptHIPE,filenameScripts)
