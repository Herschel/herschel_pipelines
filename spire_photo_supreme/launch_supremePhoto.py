#!/usr/bin/env python
# -*-coding:utf-8 -*
# Example of use:
# nohup python launch_supremePhoto.py object_obsids.txt 4 /data/glx-herschel/data1/herschel/SUPREME_Fits/MAPS_SPIRE

from multiprocessing import Pool, cpu_count
import os, sys
import getopt

dir = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/spire_photo_supreme/"
script = dir+"supremePhoto.py"

def usage():
	"""
	print usage function
	"""
	print ""
	print "Usage:"
	print "python launch_supremePhoto.py object_obsids.txt nbCore outputDir"
	print ""
	print "This program process object/obsids data from a text file with supreme. Fits file are saved in outputDir."
	print "" 
	print "    obsids.txt             file that contain the object to process. One 'object obsid1 obsid2' per line"
	print "    nbCore                 number of core to use on machine"
	print "    outputDir              output directory for created fits files"
	print ""

####################################################################################################################
####################################################################################################################

def launchScriptHIPE(parameters):
	logParam = "_".join(parameters.split(" ")[1:])
	log = script.replace("spire_photo_supreme","spire_photo_supreme/log").replace(".py","_"+logParam+".txt")
	command = " ".join(["hipe",script,parameters,">",log])
	print command
	os.system(command)

def main(argv=None):

	if argv is None:
		argv = sys.argv

	try:
		opts, args = getopt.getopt(argv[1:],[])
	except getopt.GetoptError, err:
		print str(err)
		usage()
		return 2

	try:
		print ""
		print "Number of Cores on machine:", cpu_count()
	except:
		pass

	try:
		print ""
		print "Load Average:"
		os.system("cat /proc/loadavg")
	except:
		pass
	
	try:
		object_obsids = sys.argv[1]
		nberProcess = sys.argv[2]
		outputDir = sys.argv[3]
	except:
		print "Error - Check your arguments !"
		sys.exit(usage())
	
	parameters = [outputDir+" "+line.rstrip('\n') for line in open(object_obsids) if line[0]!="#"]
	print parameters
	
	nberProcess = min(int(nberProcess),len(parameters))
	print "Number of cores to use:",nberProcess
	
	os.system("mkdir -p log")
	os.system("mkdir -p "+outputDir)
	
	p = Pool(processes=nberProcess)
	p.map(launchScriptHIPE,parameters)

if __name__ == "__main__":
	sys.exit(main())

