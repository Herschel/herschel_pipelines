clear(all=True)
import sys

dirBeams = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/spire_photo_supreme/beams/"

outputDir = sys.argv[1]+"/"
object = sys.argv[2]
obsids_str = sys.argv[3:]
print obsids_str

#outputDir = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/spire_photo_supreme/"
#object = "Crab"
#obsids_str = ["1342191181"]

obsids = [ int(obsid) for obsid in obsids_str ]

print outputDir, object, obsids_str, obsids

alphaStepValues = {"PSW":3,"PMW":5,"PLW":7}
#AlphaStepValues = {"PSW":6,"PMW":10,"PLW":14} #HIPE Resolution

useHsa = True
stopMode, stopValue = 'Automatic', 5*1e-05
doApplyExtendedEmissionGains = True
turnOver = 'TurnOver'
offsetOp = 'GetOffsetAll'
pointingOp = 'Nearest'

obsids_str = "_".join(obsids_str)

for band in ["PLW","PMW","PSW"]:
	
	if doApplyExtendedEmissionGains:
		filename = "_".join(["Supreme",object,obsids_str,band,stopMode,turnOver,offsetOp,pointingOp, "ExtEmiGainsApplied"])+".fits"
	else:
		filename = "_".join(["Supreme",object,obsids_str,band,stopMode,turnOver,offsetOp,pointingOp])+".fits"
	print filename
	#~ # Decomment this line for test, when some obsids have been stored in a specific pool to avoid retrieving them each time from distant storage HSA
	#~ data = supremePhotoGetDataFromObs(obsids=obsids,useHsa=False, poolName="test_cossou",bandName=band,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
	data = supremePhotoGetDataFromObs(obsids=obsids,useHsa=useHsa,bandName=band,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains)
	Map, StdMap, CoAddMap, Extra = supremePhotoMapMaking(data=data,alphaStep=alphaStepValues[band],turnOver=turnOver,stopMode=stopMode,stopValue=stopValue,dirBeams=dirBeams,offsetOp=offsetOp,pointingOp=pointingOp)

	simpleFitsWriter(Map,outputDir+filename)
	simpleFitsWriter(Extra,outputDir+filename.replace("Supreme","Extra"))
	simpleFitsWriter(CoAddMap,outputDir+filename.replace("Supreme","CoAdd"))
