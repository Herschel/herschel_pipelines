
_________________________________________________
PACSman IDL Suite for Herschel/PACS spectrometer data
_________________________________________________

Authors: Vianney Lebouteiller, Diane Cormier
Service d'Astrophysique, CEA, Saclay, France

URL: http://www.myravian.fr/Homepage/Softwares.html

_________________________________________________
Components
_________________________________________________
The PACSman components are independent and do not require the other programs to run

Main routines:
- PACSman_fit: line fitting routine
- PACSman_map: creates the projected raster maps
- PACSman_analysis: measure fluxes and source geometry
- PACSman_fitpsf: to come

Special (not documented yet):
- PACSman_transients: procedure to call within HIPE to remove the transients in unchopped observations

Specific purpose routines (not documented yet):
- PACSman_convert: convert IDL structure of the spectra into an ASCII file
- PACSman_mosaic: creates a 5x5 spectral mosaic of a footprint
- PACSman_footprint: measures the flux of a line in various ways for a pointed observation 
- merge_cubes: merge flux cubes to make a single cube
- PACSman_psf: creates PSF template to compare to pointed observation, in preparation
- split: split the FITS files cube into one file per plane
- PACSman_makereg: makes DS9 region files 

PACSman contains and uses the following IDL packages:
- polyclip from JD Smith's CUBISM software: http://tir.astro.utoledo.edu/jdsmith/code/idl.php
- the MPFIT library: http://www.physics.wisc.edu/~craigm/idl/
- the princeton IDL library: http://spectro.princeton.edu/

_________________________________________________
Installation
Tested on IDL 7.* and IDL 8.0 on a Mac OSX 10.6
_________________________________________________

- Place the PACSman routines in the IDL path.
- Compile the routine to be used, e.g., .com PACSman_fit
- Help: you can simply type the name of the program to have the list of possible inputs

_________________________________________________
Versions
_________________________________________________

3.0: 
All routines:
	- Changed arrays into IDL structures. New output files. Continuum maps. Fixed bugs on velocity and FWHM maps. Conversion script for exporting ascii arrays.

3.4:
Known bugs:
	- the mapping projection routine PACSman_map would overestimate the fluxes (total in map and per pixel) by between ~10% and 20%, depending on several parameters (position angle, map sampling)

3.5: 
PACSman_fit:
	- added obsid optional parameter to append obsid number to output directories
	- fixed a bug to calculate cycle number in observations with more than 10 cubes
	- rebinned spectra are now all in the same wavelength scale for a given observation (i.e., for all raster positions and all spaxels)
	- improved plots (ranges and axes)
	- fixed a bug in which a small fraction of masked pixels at the edges were not flagged as such
PACSman_map:
	- the contribution of each spaxel to a given grid pixel is now calculated using polyclip from JD Smith, which calculates the intersection area
	- removed inverse rotation using PA before projection. This caused a bug with slight coordinates shifts
	- spaxels are rotated using PA of cube, and not a global PA value (relevant to projection of several independent exacubes)
	- "ortho" maps are not used anymore. There is just Flux.fits, and Flux_raw.fits, and a smoothed version (see below)
	- smoothed maps are now provided instead or "ortho". This is just the cube smoothed by interpolation with a small rotation angle (and inverse transformation)
	- possibility to load background image in batch mode (e.g., DSS)
	- fixed some bugs related to batch mode with no X server
	- fixed a bug concerning merged cubes and files being written at the wrong place
	- now providing projected spectral rebinned cube in MJy/sr
	- fixed a bug concerning the spectra displayed when clicking on the images
PACSman_analysis:
	- PACSman_analysis uses Flux.fits maps since ortho maps are not used anymore (see above)
merge_cubes:
	- changed output name to cube_merged.sav instead of merged_cube.sav
Known bugs:
	- Line map tab doesn't work
	
3.51:
PACSman_fit:
	- cube_fitparams.params was integer, changed to float (useful when using parameters to plot spectra)
	- changed cube_params.instrumental_fwhm to cube_params.reference_fwhm
	- corrected a bug for when several cycles, the number of cycles was changed back to 1
PACSman_mosaic:
	- plots have been updated, with the fit overlaid and common axes
PACSman_convert:
	- fitted flux is now provided as another column
PACSman_analysis:
	- fixed bug that caused a crash when aperture radius was changed

3.52:
PACSman_fit:
	- keyword no_correction_factor was removed. The correction only applies to old data with time dependence <13 in getCalTree(obs=obs).spectrometer). If the correction is needed, use the following keyword instead: /use_correction_factor
	- now providing /plots_publi switch for plots that can be used in publications (larger character size, less info etc...)
PACSman_map:
	- was overwriting color table 39. The default colors1.tbl (to copy in idlxx/resource/colors) is now provided in the PACSman/misc directory if one wants to revert to it. The new code now creates new color table (41 and 46) instead of overwriting 39
	- line_map is created automatically. Background image is chosen if input
	- corrected problem for big maps: spectral projection will not be performed. Projection of line fluxes will still be performed and output products will be written
merge_cubes:
	- fixed a bug when merging cubes and merged/ directory already exists
	- fixed bug when merging cubes with fits done with a different poly_degree parameter
PACSman_mosaic:
	- shifted the relevant row to agree with global footprint look
PACSman_footprint:
	- removed /combine keyword. If the 3x3 spectrum is available, the program will use it and display the flux
PACSman_fitpsf:
	- in preparation

3.53
PACSman_transient:
	- now removing large scale at the very beginning and end of the timeline. This is especially relevant for the transient systematically observed after the calibration block.
PACSman_fitpsf:
	- available by request
PACSman_map:
	- Line map improved, with possibility to overplot fits or data. When choosing fits, the detection level in sigma is shown in each spaxel

3.54
General
	- fixed legend bug for IDL 8.2
PACSman_map
	- fixed bug for line map creation
	- ds9 region files are better determined

_________________________________________________
How to use
_________________________________________________

First, in HIPE do the following:
- export level 1 cubes (PACS sliced cubes, before projection). Example:
	for i in range(num):
   	print 'Raster '+str(i+1)
   	cube = slicedCubes.refs[i].product
   	simpleFitsWriter(product=cube,file="/data/"+objname+"/PACS_spec/fits/"+linename+"mic/cube"+str(i+1)+".fits")
- Naming convention is cubeAn.fits and cubeBn.fits for chop/nod observations (A,B are the nods, n is the raster position, set to 1 for a pointed observation), and it is cuben.fits for an unchopped observation
   	
Then create some directories in the file system:
- Create a directory per object
- Within the object directory, create a directory per line, e.g., 'CII157', 'OI63', ... The naming convention is that in the pacsman_fit.pro routine
- Place the cubes in the relevant directory

Launch routines:
- From the object directory, run pacsman_fit to fit the lines. Launch pacsman_fit with no arguments to have some help. Output products are written in the line directory. In particular, the sav file contains all the parameters and be be restored within IDL.

- From the object directory, run pacsman_map to project the line flux maps on a supersampled grid. Output products are written in the line directory. pacsman_map can be called either as a GUI or in batch mode. 
- Difference between "Flux_raw", "Flux" and "Flux_smooth"? "Flux_raw" is the map created by projecting the spaxel line flux onto an oversampled grid. "Flux" is the same but with filled holes (in the case of maps not fully sampled). "Flux_smooth" is the same as "Flux" but smoothed, the smoothing being done by rotating the image back and forth by a tiny angle, which smears a bit the spatial distribution since an interpolation in involved in the rotation. This is basically to make the map look better for the publications. For the analysis, there is little difference, except that the smoothed version has an effective PSF that is smoother (as compared to the triangular shape of the intrinsic PACS PSF).
- From the object directory, run pacsman_analysis to examine the maps

