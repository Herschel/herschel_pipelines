;---------------------------------------------------------------------------------
FUNCTION str_round, input
  x = input
  ;stupid function to round the number in the string...
  if strpos(x, 'NaN') gt -1 or strpos(x, 'Inf') gt -1 or finite(x) eq 0 then return, 'NaN' else begin
    if strpos(x, 'e') gt -1 then begin
      expo = strmid(x, strpos(x, 'e'), strlen(x)-strpos(x, 'e')+1)
      x = strmid(x, 0, strpos(x, 'e'))
    endif else expo = ''
    if strpos(x, '.') eq -1 then x = x + '.'
    x = x + '0000000000000000000'
    if strpos(x, 'e') ge 0 then x = '0.0'
    sx = strsplit(x, '.', /extract)
    ;sx[1] = sx[1] + '000000000000000'
    ;r = nint(strmid(sx[1], 0, 3)/10.)
    ;if (r lt 10) then rst = '0'+sm_a2str(r) else rst = sm_a2str(r)
    r = a2str(long(strmid(sx[0]+sx[1], 0, strlen(sx[0])+3))/1000.)
    if strpos(r, '.') eq -1 then r = r + '.'
    r = r + '0000000000000000000'
    sr = strsplit(r, '.', /extract)
    rst = strmid(sr[1], 0, 2)
    tmp = sr[0] + '.' + rst
    if tmp eq '-0.00' then tmp = '0.00'
    return, tmp + expo
  endelse
END
;############################################################################################################
pro pacsman_footprint, lines = lines, cube = cube, redshift = redshift, center = center

  if not keyword_set(cube) then cube = "cubeAcubeB"
  
  if not keyword_set(lines) then begin
    spawn, 'find . -type d -maxdepth 1 -mindepth 1', lines
    for line_i = 0, n_elements(lines) - 1 do lines(line_i) = strtrans(lines(line_i), './', '')
  endif
  
  print, lines
  for line_i = 0, n_elements(lines) - 1 do begin
  
    if strpos(lines(line_i), 'ANALYSIS') gt -1 then continue
    line = lines(line_i)
    if not file_test(line+"/"+cube+".sav") then continue
    
    ;combined spectrum
    if file_test(line+"/"+cube+"_3x3.sav") then begin
      restore, line+"/"+cube+"_3x3.sav"
      flux9combint = total(cube_lres.integrated_flux, /nan )
      error9combint = total(cube_lres.integrated_noise, /nan )
      flux9comb = total(cube_lres.flux + cube_lres.localcomponent_flux, /nan )
      error9comb = total(cube_lres.error)
      tmp = total(cube_lres.localcomponent_error, /nan )
      if finite(tmp) then error9comb += tmp
      flux9comb_cont = total(cube_lres.continuum, /nan )
      error9comb_cont = total(cube_lres.continuum_error, /nan )
    endif else begin
      flux9comb = !values.f_nan
      error9comb = !values.f_nan
      flux9combint = !values.f_nan
      error9combint = !values.f_nan
      flux9comb_cont = !values.f_nan
      error9comb_cont = !values.f_nan
    endelse
    
    ;individual spectra
    restore, line+"/"+cube+".sav"
    
    val = max(cube_lres.raster)
    if val ne 1 then begin
      print, 'This procedure is designed for a single footprint observation'
      return
    endif
    
    pacsman_fitpsf, line+"/"+cube+".sav", /plot, res = res, /back
    pacsman_fitpsf, line+"/"+cube+".sav", /plot, /cont, res = rescont, /back
    
    ;lambda = 0.
    ;if strpos(line, 'CII157') gt -1 then lambda = 157.7409
    ;if strpos(line, 'OI63') gt -1 then lambda = 63.183705
    ;if strpos(line, 'OI145') gt -1 then lambda = 145.525439
    ;if strpos(line, 'NII122') gt -1 then lambda = 121.89806
    ;if strpos(line, 'OIII52') gt -1 then lambda = 52.
    ;if strpos(line, 'NIII57') gt -1 then lambda = 57.33
    ;if strpos(line, 'OIII88') gt -1 then lambda = 88.356
    ;if strpos(line, 'OH79') gt -1 then lambda = 79.
    ;if lambda eq 0. then stop
    lambda = cube_params.lambda_rest
    if not keyword_set(redshift) then redshift = cube_params.redshift
    lambda *= (1.+ redshift) 
    
    pls_frac_lambda = [50, 60, 70, 80, 90, 100, 180, 220]
    pls_frac = [0.75, 0.71, 0.70, 0.695, 0.69, 0.675, 0.45, 0.335]
    pls_corr = 1./interpol(pls_frac, pls_frac_lambda, lambda)
    pls_corr = pls_corr(0)
    
    PACSman_dir = find_with_def('pacsman_map.pro', !path)
    tmp = strsplit(PACSman_dir, '/', /extract)
    PACSman_dir = '/'
    for i = 0, n_elements(tmp) - 2 do PACSman_dir = PACSman_dir + tmp[i] + '/'
    if PACSman_dir eq '/' then PACSman_dir ='./'
    print, 'Reading corrections in:', PACSman_dir
    readcol, PACSman_dir+"/calib/Pointsource_3x3_over_total.txt", pls_frac3by3_lambda, pls_frac3by3, /silent
    pls3by3_corr = 1./interpol(pls_frac3by3, pls_frac3by3_lambda, lambda)
    pls3by3_corr = pls3by3_corr(0)
    
    ;#######################################################################
    
    ;signal and SNR
    signal_int = cube_lres.integrated_flux
    noise_int = cube_lres.integrated_noise
    signal = cube_lres.flux + cube_lres.localcomponent_flux
    noise = cube_lres.error 

    ind = where( finite(cube_lres.localcomponent_error), c )
    if c gt 0 then noise(ind) += cube_lres(ind).localcomponent_error

    snr = signal / noise
    noise_norm = noise / max(signal, /nan)
    signal_norm = signal / max(signal, /nan)
    
    ;continuum
    continuum = cube_lres.continuum
    ;ind = where( continuum lt 0., c )
    ;if c gt 0 then continuum(ind) = 0.
    ;if min(continuum, /nan) lt 0. then continuum += -min(continuum, /nan)
    continuum_noise = cube_lres.continuum_error
    snrcont = continuum / continuum_noise
    continuum_noise_norm = continuum_noise / max(continuum, /nan)
    continuum_norm = continuum / max(continuum, /nan)
    
    if keyword_set(center) then begin
      print, 'Using input reference spaxel. '
      indbrightest = where( abs(cube_lres.spaxel_X-center(0)) eq 0 and abs(cube_lres.spaxel_Y-center(1)) eq 0, c )
    endif else begin
      ;brightest spaxel
      if max(snr, /nan) le 1. then begin
        print, 'No detection in any spaxel. Assuming the source is in the central spaxel. '
        indbrightest = where( abs(cube_lres.spaxel_X-3) eq 0 and abs(cube_lres.spaxel_Y-3) eq 0, c )
      endif else begin
        indbrightest = where( signal eq max(signal, /nan), count )
      endelse
    endelse
    
    ;#######################################################################
    ;First method= brightest spaxel and PLS correction
    flux1 = (signal(indbrightest) * pls_corr)(0)
    error1 = (noise(indbrightest) * pls_corr)(0)
    flux1int = (signal_int(indbrightest) * pls_corr)(0)
    error1int = (noise_int(indbrightest) * pls_corr)(0)
    
    flux1_cont = (continuum(indbrightest) * pls_corr)(0)
    error1_cont = (continuum_noise(indbrightest) * pls_corr)(0)
    
    ;#######################################################################
    ;Second method= 3x3 around the brightest
    ind3by3 = where( abs(cube_lres.spaxel_X-cube_lres(indbrightest).spaxel_X) le 1 and abs(cube_lres.spaxel_Y-cube_lres(indbrightest).spaxel_Y) le 1, c )
    flux9 = total( signal(ind3by3), /nan ) * pls3by3_corr
    error9 = total( noise(ind3by3), /nan ) * pls3by3_corr
    flux9int = total( signal_int(ind3by3), /nan ) * pls3by3_corr
    error9int = total( noise_int(ind3by3), /nan ) * pls3by3_corr
    
    flux9_cont = total( continuum(ind3by3), /nan ) * pls3by3_corr
    error9_cont = total( continuum_noise(ind3by3), /nan ) * pls3by3_corr
    
    ind = where( signal gt 3.*noise, ndetected )
    if ndetected eq 0 then begin
      fluxdetected = !values.f_nan
      errordetected = !values.f_nan
      fluxdetectedint = !values.f_nan
      errordetectedint = !values.f_nan
      fluxdetected_cont = !values.f_nan
      errordetected_cont = !values.f_nan
    endif else begin
      fluxdetected = total( signal(ind), /nan )
      errordetected = total( noise(ind), /nan )
      fluxdetectedint = total( signal_int(ind), /nan )
      errordetectedint = total( noise_int(ind), /nan )
      fluxdetected_cont = total( continuum(ind), /nan )
      errordetected_cont = total( continuum_noise(ind), /nan )
    endelse
    
    ;#######################################################################
    ;Third method= 5x5
    flux25 = total( signal, /nan )
    error25 = total( noise, /nan )
    flux25int = total( signal_int, /nan )
    error25int = total( noise_int, /nan )
    
    flux25_cont = total( continuum, /nan )
    error25_cont = total( continuum_noise, /nan )
    
    ;#######################################################################
    ;Results
    print, 'Line: ', line
    print, ''
    print, 'redshift = ', redshift
    
    nsigma = str_round(res.flux/res.error)
    print, '  - PLS (PSF)          : ' + str_round(res.flux) + ' +/- ' + str_round(res.error) + ' W m-2 (' + nsigma + ' sigma)'
    
    nsigma = str_round(flux1/error1)
    print, '  - PLS (fit)          : ' + str_round(flux1) + ' +/- ' + str_round(error1) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls_corr) + ')'
    nsigma = str_round(flux1int/error1int)
    print, '  - PLS (int)          : ' + str_round(flux1int) + ' +/- ' + str_round(error1int) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls_corr) + ')'
    
    nsigma = str_round(flux9/error9)
    print, '  - 3x3 (fit+add)      : ' + str_round(flux9) + ' +/- ' + str_round(error9) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    nsigma = str_round(flux9int/error9int)
    print, '  - 3x3 (int+add)      : ' + str_round(flux9int) + ' +/- ' + str_round(error9int) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    
    nsigma = str_round(fluxdetected/errordetected)
    print, '  - detected (fit+add) : ' + str_round(fluxdetected) + ' +/- ' + str_round(errordetected) + ' W m-2 (' + nsigma + ' sigma) (ndetected: '+a2str(ndetected)+')'
    nsigma = str_round(fluxdetectedint/errordetectedint)
    print, '  - detected (int+add) : ' + str_round(fluxdetectedint) + ' +/- ' + str_round(errordetectedint) + ' W m-2 (' + nsigma + ' sigma) (ndetected: '+a2str(ndetected)+')'
    
    nsigma = str_round(flux9comb/error9comb)
    print, '  - 3x3 (comb+fit)     : ' + str_round(flux9comb) + ' +/- ' + str_round(error9comb) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    nsigma = str_round(flux9combint/error9combint)
    print, '  - 3x3 (comb+int)     : ' + str_round(flux9combint) + ' +/- ' + str_round(error9combint) + ' W m-2 (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    
    nsigma = str_round(flux25/error25)
    print, '  - 5x5 (fit+add)      : ' + str_round(flux25) + ' +/- ' + str_round(error25) + ' W m-2 (' + nsigma +' sigma)'
    nsigma = str_round(flux25int/error25int)
    print, '  - 5x5 (int+add)      : ' + str_round(flux25int) + ' +/- ' + str_round(error25int) + ' W m-2 (' + nsigma +' sigma)'
    
    print, ''
    print, 'Continuum'
    print, ''
    
    nsigma = str_round(rescont.flux/rescont.error)
    print, '  - PLS (PSF)          : ' + str_round(rescont.flux) + ' +/- ' + str_round(rescont.error) + ' Jy (' + nsigma + ' sigma)'
    
    nsigma = str_round(flux1_cont/error1_cont)
    print, '  - PLS                : ' + str_round(flux1_cont) + ' +/- ' + str_round(error1_cont) + ' Jy (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls_corr) + ')'
    
    nsigma = str_round(flux9_cont/error9_cont)
    print, '  - 3x3                : ' + str_round(flux9_cont) + ' +/- ' + str_round(error9_cont) + ' Jy (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    
    nsigma = str_round(fluxdetected_cont/errordetected_cont)
    print, '  - detected           : ' + str_round(fluxdetected_cont) + ' +/- ' + str_round(errordetected_cont) + ' Jy (' + nsigma + ' sigma) (ndetected: '+a2str(ndetected)+')'
    
    nsigma = str_round(flux9comb_cont/error9comb_cont)
    print, '  - 3x3 (comb)         : ' + str_round(flux9comb_cont) + ' +/- ' + str_round(error9comb_cont) + ' Jy (' + nsigma + ' sigma) (PLS correction factor: '  + str_round(pls3by3_corr) + ')'
    
    nsigma = str_round(flux25_cont/error25_cont)
    print, '  - 5x5                : ' + str_round(flux25_cont) + ' +/- ' + str_round(error25_cont) + ' Jy (' + nsigma +' sigma)'
    
    print, ''
    print, 'Results written in ' + line+'/footprint_results.sav'
    
    result = { line: line, integrated: keyword_set(integrated), $
      brightest: [nint(cube_lres(indbrightest).spaxel_X), nint(cube_lres(indbrightest).spaxel_Y)], $
      ;pls_proba: pls_proba, pls_proba_acc: pls_proba_acc, $
      ;plscont_proba: plscont_proba, plscont_proba_acc: plscont_proba_acc, $
      ;shifted: shifted, shifted_cont: shifted_cont, $
      flux1: double(flux1), error1: double(error1), $
      flux9: double(flux9), error9: double(error9), $
      fluxdetected: double(fluxdetected), errordetected: double(errordetected), $
      ndetected: ndetected, $
      flux9comb: double(flux9comb), error9comb: double(error9comb), $
      flux25: double(flux25), error25: double(error25), $
      flux1int: double(flux1int), error1int: double(error1int), $
      flux9int: double(flux9int), error9int: double(error9int), $
      fluxdetectedint: double(fluxdetectedint), errordetectedint: double(errordetectedint), $
      flux9combint: double(flux9combint), error9combint: double(error9combint), $
      flux25int: double(flux25int), error25int: double(error25int), $
      flux1_cont: double(flux1_cont), error1_cont: double(error1_cont), $
      flux9_cont: double(flux9_cont), error9_cont: double(error9_cont), $
      fluxdetected_cont: double(fluxdetected_cont), errordetected_cont: double(errordetected_cont), $
      flux9comb_cont: double(flux9comb_cont), error9comb_cont: double(error9comb_cont), $
      flux25_cont: double(flux25_cont), error25_cont: double(error25_cont), $
      fluxpsf: double(res.flux), errorpsf: double(res.error), chi2psf: res.redchi2, $
      fluxpsf_cont: double(rescont.flux), errorpsf_cont: double(rescont.error), chi2psf_cont: rescont.redchi2, $
      psfbrightest: res.brightest, psfbrightest_cont: rescont.brightest, $
      psfposition: res.position $
      }
      
    save, filename = line+'/footprint_results.sav', result
    spawn, '\rm -f '+line+'/extent_test.*'
    
  endfor
  
  
end
