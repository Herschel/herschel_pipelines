pro PACSman_transients_frames, files = files, nbreFrames = nbreFrames

  for i=0,nbreFrames do begin
    
    frame = files+trim(string(i))+'.fits'
    print,frame
    PACSman_transients, frame = frame, dippers = 1, verbose = 0, save_plots = 1, faders = 1, denoise = 0, redomask = 0
  
  endfor

end
