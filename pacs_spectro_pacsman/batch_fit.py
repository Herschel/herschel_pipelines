import os
from multiprocessing import Pool
import pyfits

def checkMotif(chaine,patterns,excluded):
    for pattern in patterns:
        if str(chaine).count(pattern)==0: return False
    for exclude in excluded:
        if str(chaine).count(exclude)>0: return False
    return True

def getFilesMotif(basePath,patterns,excluded=[]):
    listfile_all = []
    listroot = []
    for root, dirs, files in os.walk(basePath):
        listfile = [root+"/"+filename for filename in files if checkMotif(filename,patterns,excluded)]
        listfile_all.extend(listfile)
    return listfile_all

def remove_off_position_rasters(directory):
    listfile = getFilesMotif(directory,["_RasterCube_R",".fits"])
    root = listfile[0].split("_RasterCube_R")[0]+"_RasterCube_R"
    off_pos = []
    for fits in listfile:
        if pyfits.open(fits)[0].header["ISOFFPOS"]:
            os.system("rm -rf "+fits)
            off_pos.append(int(fits.split("_RasterCube_R")[1][:-5]))
    off_pos = sorted(off_pos)
    middleOff = [ int(off) for off in off_pos[1:-1] ]
    listfile = getFilesMotif(directory,["_RasterCube_R",".fits"])
    if middleOff!= []:
        for i in range(middleOff[0],len(listfile)+1):
            os.rename(root+str(i+1)+".fits",root+str(i)+".fits")
    
    return len(listfile)

fits_dir = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/"
pacsman_dir = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/PACSMAN/PACSman/"

lines = {"157.67_CII":"CII157","63.18_OI":"OI63","145.52_OI":"OI145","121.9_NII":"NII122"}
#fields = ["n7023E_fts_1","Ced201-2","n2023-PACS","HD37022-towards","HH_dense_core","IC59","IC63","rho_oph_h2","HH_IR_int-2"]
fields = ["HH_dense_core","IC59","IC63","rho_oph_h2","HD37022-towards"]
exclude = ["n7023_fts","HD37041","rho_oph_fts_off_2","rho_oph_fts_on","IC59-S"]

for field in fields:
    
    for line in lines.keys():
    
        folderLine = lines[line]
        
        motifs = ["_RasterCube_R",line,"fits"]
        
        listfile = getFilesMotif(fits_dir+field,motifs,exclude)
        listfile = list(sorted(listfile))
        nberCubes = len(listfile)
        
        if nberCubes>0:
            
            motifFile = listfile[0][:-6]
            print motifFile, folderLine, nberCubes
            try: os.system("mkdir -p "+pacsman_dir+folderLine)
            except : print pacsman_dir+folderLine+" already exist"
            os.system("rm -rf "+pacsman_dir+folderLine+"/*")
            for i in range(1,nberCubes-1): os.system("cp "+motifFile+str(i)+".fits "+pacsman_dir+folderLine+"/")
            rsync = "rsync -av "+motifFile+"*.fits "+pacsman_dir+folderLine
            os.system(rsync)
            
            nberCubes_on_pos = remove_off_position_rasters(pacsman_dir+folderLine)
            
            string_to_write  = ""
            string_to_write += ".COMPILE "+pacsman_dir+"pacsman_fit.pro"+"\n"
            string_to_write += "PACSman_fit, '"+folderLine+"', "+str(nberCubes_on_pos)+", '"+motifFile.split("/")[-1]+"'"+"\n"
            string_to_write += ".COMPILE "+pacsman_dir+"pacsman_map.pro"+"\n"
            string_to_write += "PACSman_map, object='"+field+"', makeall=1"+"\n"
            string_to_write += "exit\n"
            
            batch_file = pacsman_dir+"batch_fit.pro"
            file = open(batch_file, "w")
            file.write(string_to_write)
            file.close()
            
            os.system("idl "+batch_file)
            os.system("rename 's/LINE/PACSman_/' "+pacsman_dir+"/"+folderLine+"/*LINE"+folderLine+"*")
            os.system("rsync -av "+pacsman_dir+"/"+folderLine+"/*PACSman_"+folderLine+"* "+fits_dir+field)
            os.system("rsync -av "+pacsman_dir+"/"+folderLine+"/*sav "+fits_dir+field)

