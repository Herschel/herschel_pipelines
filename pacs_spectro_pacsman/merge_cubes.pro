pro merge_cubes, list = list, line = line

  if not keyword_set(list) and not keyword_set(line) then begin
    print, 'Calling sequence.'
    print, ''
    print, " merge_cubes, [line = 'CII157'], [list = 'dir/cube.sav', 'otherdir/cube.sav']]"
    retall
  endif

  print, '___________________________________________________'
  if keyword_set(line) then begin
    list = file_search('./', 'cube*.sav')
    print, list
    ind = where(strpos(list, line) gt -1)
    list = list(ind)
    print, 'Requested line: ', line
  endif
  
  print, 'Files found: ', list
  print, '___________________________________________________'
  
  n = n_elements(list)
  print, 'Reading cube : ', list(0)
  restore, list(0)
  merged_cube_lres = cube_lres
  merged_cube_fitparams = cube_fitparams
  merged_cube_spectra = cube_spectra
  merged_cube_params = cube_params
  
  for i = 1, n - 1 do begin
    if strpos(list(i), 'cube_merged') gt -1 then continue
    print, 'Reading cube : ', list(i)
    restore, list(i)
    
    ;increment number of rasters
    cube_lres.raster += merged_cube_params.n_rasters
    cube_spectra.raster += merged_cube_params.n_rasters
    merged_cube_params.n_rasters += cube_params.n_rasters
    
    ;no need to merge cube_params
    merged_cube_lres = [merged_cube_lres, cube_lres]
    merged_cube_fitparams = [merged_cube_fitparams, cube_fitparams]
    merged_cube_spectra = [merged_cube_spectra, cube_spectra]
  endfor
  cube_lres = merged_cube_lres
  cube_fitparams = merged_cube_fitparams
  cube_spectra = merged_cube_spectra
  cube_params = merged_cube_params
    
  if keyword_set(line) then begin
    file_mkdir, 'merged'
    file_mkdir, 'merged/'+line
    dir = './merged/'+line+'/'
  endif else dir = './'
  
  save, filename = dir+'/cube_merged.sav', cube_lres, cube_fitparams, cube_spectra, cube_params, cube_header
  
end
