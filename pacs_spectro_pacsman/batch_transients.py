import os, string, pyfits
import sys

directory = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/"
pacsmanDir = "/data/glx-herschel/data1/herschel/scriptsPACS/Spectro/PACSMAN/PACSman/"

def checkMotif(chaine,Motifs, Exclure):
    for motif in Motifs:
        if str(chaine).count(motif)==0: return False
    for exclu in Exclure:
        if str(chaine).count(exclu)>0: return False
    return True

def getFilesMotif(basePath,motifs,exclure):
    listfile_all = []
    listroot = []
    for root, dirs, files in os.walk(basePath):
        listfile = [root+"/"+filename for filename in files if checkMotif(filename,motifs,exclure)]
        listfile_all.extend(listfile)
    return listfile_all

champs = ["n7023E_fts_1","Ced201-2","HH_IR_int-2","n2023-PACS","HD37022-towards","HH_dense_core","IC59","IC63","rho_oph_h2"]
lines = {"157.67_CII":"CII157","63.18_OI":"OI63","145.52_OI":"OI145","121.9_NII":"NII122"}
exclure = ["test","Corr","n7023_fts","HD37041","rho_oph_fts_off_2","rho_oph_fts_on","IC59-S"]

for champ in champs[:1]:

    for line in lines.keys():
    
        folderLine = lines[line]
        
        motifs = ["_Frame_R",line]
        
        listfile = getFilesMotif(directory+champ,motifs,exclure)
        listfile = list(sorted(listfile))
        nbreFrames = len(listfile)
        print folderLine, nbreFrames
        
        if nbreFrames>0:
            motifFile = listfile[0][:-6]
            print motifFile
            fichierBatch = pacsmanDir+"batch_transients.pro"
            fichier = open(fichierBatch, "w")
            fichier.write(".COMPILE "+pacsmanDir+"pacsman_transients.pro\n")
            fichier.write(".COMPILE "+pacsmanDir+"pacsman_transients_frames.pro\n")
            fichier.write("PACSman_transients_frames, files='"+motifFile+"', nbreFrame="+str(nbreFrames)+"\n")
            fichier.write("exit\n")
            fichier.close()
            
            #os.system("idl "+fichierBatch)
            
            #for i in listfile:
            
                #filePACSman = i.replace(".fits","_CorrPACSman.fits")
                #fileKeep = i.replace(".fits","_CorrPACSmanHIPE.fits")

                #os.system("cp "+i+" "+fileKeep)

                #fits = pyfits.open(filePACSman)
                #signalCorr = fits["Primary"].data
                #fits.close()

                #fits = pyfits.open(fileKeep, mode='update')
                #fits["Signal"].data = signalCorr
                #fits.close()
