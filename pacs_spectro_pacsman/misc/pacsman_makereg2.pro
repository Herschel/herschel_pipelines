pro pacsman_makereg2, file, color = color

  if not keyword_set(color) then color = 'black'
  
  im = readfits(file, h, /sil)
  extast, h, astr
  
  flux = reform(im(*, *, 0))
  s = size(flux, /dim)
  
  text = ['# Region file format: DS9 version 4.1', $
    '# Filename: '+file, $
    'global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1', $
    'fk5' ]
    
  st = 'polygon(
  i = 0
  arr = intarr(1000, 2)
  for x = 0, s(0) - 1 do for y = 0, s(1) - 1 do begin
  
    aroundfinite = 1
    if x ge 1 then if not finite(flux(x-1, y)) then aroundfinite = 0
    if y ge 1 then if not finite(flux(x, y-1)) then aroundfinite = 0
    if x lt s(0)-1 then if not finite(flux(x+1, y)) then aroundfinite = 0
    if y lt s(1)-1 then if not finite(flux(x, y+1)) then aroundfinite = 0
    
    if finite(flux(x, y)) and aroundfinite eq 0 then begin
      arr(i, *) = [x, y] 
      i += 1
    endif
    
  endfor
  npoints = i
  arr = arr(0:npoints - 1, *)
  
  help, arr
  arrsort = arr
  n = 0
  i = 0
  remaining = intarr(npoints) + 1
  while n lt npoints do begin
    ;print, i, n
    ind = where( remaining eq 1 )
    ;help, ind
    ind2 = sort( sqrt( (arr(i, 0)-arr(ind, 0))^2. + (arr(i, 1)-arr(ind, 1))^2. ) )
    ind = ind(ind2)
    ;print, ind(0:5)
    arrsort(n, *) = arr(ind(0), *)
    remaining(ind(0)) = 0
    n += 1
    i = ind(0)
  endwhile
  
  
  for i = 0, npoints - 1 do begin
    xy2ad, arrsort(i, 0), arrsort(i, 1), astr, a, d
      if i gt 0 then st += ','
      st += a2str(a)+','+a2str(d)
  endfor
  
  
  
  st += '# color='+color+' width=2 font="helvetica 14 normal roman" text={}'
  text = [text, st]
  
  openw, lun, strtrans(file, '\.fits', '\.reg'), /get_lun, error = err
  if err ne 0 then print, !ERROR_STATE.MSG else begin
    for i = 0, n_elements(text) - 1 do printf, lun, text(i)
    close, /all
  endelse
  
end
