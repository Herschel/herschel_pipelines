clear(all=True)

object_obsids_file = "/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/SuperResolution/object_obsids.txt"
#object_obsids = [line.rstrip('\n') for line in open(object_obsids_file)]
object_obsids = [["_".join(line.rstrip('\n').split(" ")[:-1]),line.rstrip('\n').split(" ")[-1]] for line in open(object_obsids_file)]

outfile = open(object_obsids_file.replace(".txt","_fullSampling.txt"),"w")
for obj_obs in object_obsids:
	obs = getObservation(obsid=int(obj_obs[1]),useHsa=True,instrument="SPIRE")
	sampling = obs.meta["mapSampling"].value
	if obs.meta["mapSampling"].value.count("full")>0: 
		print sampling
		print >> outfile, obj_obs[0], obj_obs[1]