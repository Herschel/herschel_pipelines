clear(all=True)
import os, sys

release = "/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/SuperResolution/test/"
object = "EL29_on"
obsids_str = ["1342204895"]
obsids = [int(obsid) for obsid in obsids_str]

print release, object, obsids

#USING HSA
useHsa = True # Use Herschel Science Archive
apodName = {True:"apodized",False:"unapodized"}
AlphaStep = 8 # alpha step angle in arcsec. We recommend 8 arcsec for cross-treatment of SSW and SLW cubes.
stop = "Automatic"
tolerance = 5*10**(-5)

for band in ["SSW","SLW"]:
	for apod in [True,False]:
		# Retrieve the informations from the observations
		data = supremeSpectroGetDataFromObs(obsids=obsids,bandName=band,useHsa=useHsa,apodized=apod,saveSpectrum2d=release)
		# CUBE MAKING
		# Main SupremeSpectro task that return the cube and additionnal informations about the computation
		supremeCube, StdCube, supremeExtra = supremeSpectroCubeMaking(data=data,AlphaStep=AlphaStep,StopMode=stop,StopValue=tolerance)
		# Save the cube as a fits file
		filename = release+"_".join(["_".join(obsids_str),object,band,apodName[apod],"supreme_cube.fits"])
		print filename
		simpleFitsWriter(supremeCube,filename)
		simpleFitsWriter(supremeExtra,filename.replace("supreme_cube","supreme_extra"))
