import pyfits
from multiprocessing import Pool
from Database_Tools import getFilesMotif



def appendExtraExtensionsToCube(cubeFile):
	extra = pyfits.open(cubeFile.replace("supreme_cube","supreme_extra"))
	for i in range(1,len(extra)):
		pyfits.append(cubeFile, extra[i].data, extra[i].header)

directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_fts/HIPE_Fits/FTS_SPIRE/"
motifs = ["supreme_cube.fits"]
exclure = []
listfile = getFilesMotif(directory,motifs,exclure)[:]

#for file in listfile: print file

p = Pool(processes=24)
p.map(appendExtraExtensionsToCube,listfile)
