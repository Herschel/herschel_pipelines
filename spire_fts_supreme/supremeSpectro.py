clear(all=True)
import sys

outputDir = sys.argv[1]+"/"
object = sys.argv[2]
obsids_str = sys.argv[3:]
obsids = [int(obsid) for obsid in obsids_str]

print outputDir, object, obsids

#USING HSA
useHsa = True # Use Herschel Science Archive
apodName = {True:"apodized", False:"unapodized"}
AlphaStep = 8 # alpha step angle in arcsec. We recommend 8 arcsec for cross-treatment of SSW and SLW cubes.
stop = "Automatic"
tolerance = 5e-5

for band in ["SSW","SLW"]:
	for apod in [True,False]:
		print band, apod
		# Retrieve the informations from the observations
		prefix = "_".join(["_".join(obsids_str), object, band, apodName[apod]])
		data = supremeSpectroGetDataFromObs(obsids=obsids, bandName=band, useHsa=useHsa, apodized=apod)
		# CUBE MAKING
		# Main SupremeSpectro task that return the cube and additionnal informations about the computation
		supremeCube, StdCube, supremeExtra = supremeSpectroCubeMaking(data=data, AlphaStep=AlphaStep, StopMode=stop, StopValue=tolerance)
		# Save the cube as a fits file
		filename = outputDir+"_".join([prefix,"supreme_cube.fits"])
		print filename
		simpleFitsWriter(supremeCube, filename)
		simpleFitsWriter(supremeExtra, filename.replace("supreme_cube","supreme_extra"))
