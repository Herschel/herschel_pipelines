# Create and store a Spectral Cube via HIFI's regridding
# Peter Imhof
# April 25, 2010
# May 3, 2010 (PI): Only perform manipulations with HIFI build
# 

def subBL(wave,flux):
   poly = PolynomialModel(3)
   poly.setParameters(Double1d([0.0,0.0,0.0,0.0]))
   sswFitter = LevenbergMarquardtFitter(wave, poly)
   blp       = sswFitter.fit(flux)
   sswBl     = blp[0] + blp[1]*wave+ blp[2]*wave**2 + blp[3]*wave**3
   fluxBGsub = (flux - sswBl)**2
   return fluxBGsub

def calcRms(wave,flux):
   bls = subBL(wave,flux)
   rms=(SQRT( SUM(bls)/(len(wave)-1.0) ))
   return rms



# 4. Regrid spectrum2d to SpectralSimpleCube

from herschel.hifi.dp.otf import *

# path of the directory to store the result cubes
#
source = "orionBar"
dirname = "C:\\SPIRE\\results_2010\\dornoch_jiggle_results\\orionBar_spire_cal_4_0\\"

obsid=0x50003a2d
#[0x50003A2F,0x50003A2D,0x50003A2E]:

#for obsid in [0x50003A2F]:
if 1==1:
    # Load in fits files with pre-processed data
    #product = FitsArchive().load(dirname+"ProductUnvignettedForHifiRegridding_OrionBar1_50003a2d_SSW.fits")
    product = FitsArchive().load(dirname+"ProductUnvignettedForHifiRegridding50003A2D.fits")
    #
    ################################################
    # 4. Regrid spectrum2d to SpectralSimpleCube
    ################################################
    spectra = product["spectrum"].copy()
    f = spectra.getFlux()
    w = spectra.getWeight()
    # set the weights to 1
    fl = spectra.getFlag()
    wn = spectra.getWave()
    for i in range(len(w[:,0])):
        # calculate rms of spectrum in range 0 to 200 points        
        w[i,:] = calcRms(wn[i,0:200],f[i,0:200])

    spectra = Spectrum2d(f, w, fl) 
    spectra.setWave(wn)
    #
    # May 3, 2010 (PI): Add obsid to avoid exception at the end of griddingTask
    spectra.getMeta().set("obsid", LongParameter(description="Observation ID",value=obsid))
    #
    # make a copy of the coordinates dataset in a manner convenient to the plot
    # taking into account that RA increases towards the left
    #
    coordinates_copy = product["coordinates"].copy()
    plot = PlotXY(coordinates_copy['RA'].data, coordinates_copy['DEC'].data)
    plot.getLayerXY(0).setLine(0)
    plot.setVisible(False) # double click on the plot variable to make it visible again
    #
    # add the coordinates to the Spectrum2d
    # add latitude and longitude to the Spectrum2d
    #
    spectra['longitude'] = coordinates_copy['RA']
    spectra['latitude'] = coordinates_copy['DEC']
    DEG = herschel.share.unit.Angle.DEGREES
    spectra['longitude'].unit = DEG
    spectra['latitude'].unit = DEG
    #
    # workaround for missing parameters:
    #    
    spectra.meta['equinox']= DoubleParameter(2000.)
    spectra.meta['raDeSys']= StringParameter("ICRS")
    spectra['wave'].unit = herschel.share.unit.WaveNumber.RECIPROCAL_CENTIMETERS

    spectra['flux'].unit = herschel.share.unit.FluxDensity.WATTS_PER_SQUARE_METER_PER_HERTZ .divide(herschel.share.unit.SolidAngle.STERADIANS)

    #
    # Make the cube with the beam size set to FHWM/2
    #-------------------------------------------------------------------------------
    # 
    hpbw_arcsec = Double1d(2,19)
    # default reference pixel of the griddingTask is not at the central position; 
    # need to create SPR and fix this, meanwhile, set reference pixel by hand
    mean_ra = MEAN(coordinates_copy['RA'].data)
    mean_dec = MEAN(coordinates_copy['DEC'].data)
    refPixelCoordinates = Double1d([mean_ra, mean_dec])
    # FIXME: These values will have to be revisited!!
    refPixel = Double1d([49/2.,52/2.])
    #
    # OPTION A. pixels with with too low weight. please note this is the default case
    threshold = "0.0001"    # tighter threshold more selective. e.g. threshold = "0.1" 
    Configuration.setProperty("hcss.hifi.dp.otf.filter.threshold", threshold)
    filter_threshold = Configuration.getDouble("hcss.hifi.dp.otf.filter.threshold")
    
    cube_not_extrapolated = gridding(container=spectra, beam = hpbw_arcsec, extrapolate=False, weightMode = "equal")
    hificube = cube_not_extrapolated.refs['cube_0'].product



    """
    cube_not_extrapolated = griddingTask(container=spectra, beam = hpbw_arcsec, \
    extrapolate=False, refPixel=refPixel)
    """
    
    filename = "%s/OrionBar1_%x_finalSpectrum_SSW_Unvignetted_Hifi_EqualWeights.fits" %(dirname, obsid)
    simpleFitsWriter(product=cube_not_extrapolated.refs["cube_0"].product,file=filename)
    print cube_not_extrapolated.refs["cube_0"].product.wcs.isValid() # should be true
    
    wcs = cube_not_extrapolated.refs["cube_0"].product.wcs
    print wcs
    print wcs.getPixelCoordinates(mean_ra,mean_dec)
    stop
    
    # OPTION B. do not blank pixels with with too low weight
    # cube_extrapolated = griddingTask(container=spectra, beam = hpbw_arcsec, extrapolate=True, refPixel=refPixel)
    cube_extrapolated = gridding(container=spectra, beam = hpbw_arcsec, extrapolate=True)
    filename = "%s/cube_hpbw_%f_extrapolation_enabled.fits" % (dirname, hpbw_arcsec.get(0))
    simpleFitsWriter(product=cube_extrapolated.refs["cube_0"].product,file=filename)
        
    #
    #
    # Make the cube with a given pixel size
    # based on average separation between spectra
    #-------------------------------------------------------------------------------
    # 
    pixelSize = Double1d(2,9.5)  
    cube_given_pixel_size = griddingTask(container=spectra, pixelSize = pixelSize)
    #cube_given_pixel_size = griddingTask(container=spectra, pixelSize = pixelSize, refPixel=Double1d([9/2.,11/2.]))
    filename = "%s/cube_pixel_%f_arcsec_extrapolation_disabled.fits" % (dirname, pixelSize.get(0))
    simpleFitsWriter(product=cube_given_pixel_size,file=filename)
    
    cube_given_pixel_size_extrapolated = griddingTask(container=spectra, pixelSize = pixelSize, extrapolate=True)
    #cube_given_pixel_size_extrapolated = griddingTask(container=spectra, pixelSize = pixelSize, refPixel=Double1d([9/2.,11/2.]), extrapolate=True)
    filename = "%s/cube_pixel_%f_arcsec_extrapolation_enabled.fits" % (dirname, pixelSize.get(0))
    simpleFitsWriter(product=cube_given_pixel_size_extrapolated,file=filename)
    
    #
    #
    # Make the cube with a given number of pixels along each axis
    #-------------------------------------------------------------------------------
    # 
    # FIXME: These values will have to be revisited!!
    map_width_pixels  = 10
    map_height_pixels = 12
    
    mapSize = Int1d([map_width_pixels, map_height_pixels])  
    cube_given_map_dimensions = griddingTask(container=spectra, mapSize=mapSize)
    
    #refPixel = Double1d(mapSize).divide(2.)
    #cube_given_map_dimensions = griddingTask(container=spectra, mapSize=mapSize, refPixel = refPixel)
    
    filename = "%s/cube_map_%d_%d_pixels_extrapolation_disabled.fits" % \
    (dirname, map_width_pixels, map_height_pixels)
    simpleFitsWriter(product=cube_given_map_dimensions,file=filename)
    
    
    cube_given_map_dimensions_extrapolated = \
    griddingTask(container=spectra, mapSize=mapSize, extrapolate=True)
    #griddingTask(container=spectra, mapSize=mapSize, refPixel = refPixel, extrapolate=True)
    filename = "%s/cube_map_%d_%d_pixels_extrapolation_enabled.fits" % \
    (dirname, map_width_pixels, map_height_pixels)
    simpleFitsWriter(product=cube_given_map_dimensions_extrapolated,file=filename)
