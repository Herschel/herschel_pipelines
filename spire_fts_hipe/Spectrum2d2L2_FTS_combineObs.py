# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2010 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  Start from Fits (Spectrums) from multiple obsids to create a 
# combined output cubes

#
#  Updated: Oct/2013
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################

#
# (D) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
apodize = 0
if apodize:
    apodName = "_apod"
else:
    apodName = ""
#
#
# (E) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
from herschel.ia.pal.util import StorageResult
from herschel.ia.toolbox.spectrum.projection import WcsCreator
#from herschel.ia.toolbox.spectrum.projection import spireProjection 
from herschel.ia.toolbox.spectrum.projection import NearestNeighbourProjectionTask
from herschel.spire.ia.pipeline.spec.regrid import NaiveProjectionTask

# (F) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:	
outDir = "/data/glx-herschel/data1/herschel/HIPE_Fits/FTS_SPIRE/Orion_All/"
# old gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
sswFwhm = 19.0 / 3600.0
slwFwhm = 35.0 / 3600.0
gridSpacing={"full":        {"SSW": 0.5 * sswFwhm, "SLW": 0.5 * slwFwhm}, \
             "intermediate":{"SSW": 1.0 * sswFwhm, "SLW": 1.0 * slwFwhm}, \
             "sparse":      {"SSW": 2.0 * sswFwhm, "SLW": 2.0 * slwFwhm}}
#
# (G) HR+LR observations Only: 
#     Define whether to process LR or HR data
processRes = "HR"

#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS_utility.py')
#
# (H) N/A
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################

# ---------------------------------------------------------------
# Orion Bar Obsids
#obsids=[1342204919,1342204920,1342214846,1342192173,1342228734,1342242591,1342249465,1342192174,1342192175]
obsids=[1342204919,1342204920,1342192173,1342192174]
obsids=[1342192173,1342192174,1342192175, 1342214846,1342242591]
Version="15.0.3244"
object_name={1342204919:"HD37041",1342204920:"HD37022(towards)",1342214846:"HD37041",1342228734:"HD37041",1342192174:"OrionBar_map2",1342192175:"OrionBar_map3",1342192173:"HD37041",1342242591:"HD37041",1342249465:"HD37041"}
#1342228734_HD37041_SPIRE-FTS_12.0.2603_SSW_unapod_spectrum2d

obsids=[1342204920,1342214846,1342192173,1342228734,1342242591,1342249465,1342192174,1342192175]
obsid=1342204919 # the first one (without s )
myObsid=""
myObsid=str(obsid)+"_"+myObsid
outDataPool=object_name[obsid]+"_SPIRE-FTS_"+Version
preCubeSSW=simpleFitsReader("%s%i_%s_%s_SSW_spectrum2d%s.fits"%(outDir, \
           obsid,outDataPool, processRes,apodName ))
preCubeSLW=simpleFitsReader("%s%i_%s_%s_SLW_spectrum2d%s.fits"%(outDir, \
           obsid,outDataPool, processRes,apodName ))

spcSSW=SpirePreprocessedCube(preCubeSSW["spectrum2d"])
spcSLW=SpirePreprocessedCube(preCubeSLW["spectrum2d"])

for obsid in obsids:
    myObsid=str(obsid)+"_"+myObsid
    outDataPool=object_name[obsid]+"_SPIRE-FTS_"+Version
    print "%s%i_%s_%s_SLW_spectrum2d%s.fits"%(outDir, \
           obsid,outDataPool,processRes, apodName )
    preCubeSSW2=simpleFitsReader("%s%i_%s_%s_SSW_spectrum2d%s.fits"%(outDir, \
           obsid,outDataPool, processRes,apodName ))
    spcSSW2=SpirePreprocessedCube(preCubeSSW2["spectrum2d"])
    spcSSW=spcSSW.append(spcSSW2)
    preCubeSLW2=simpleFitsReader("%s%i_%s_%s_SLW_spectrum2d%s.fits"%(outDir, \
           obsid,outDataPool, processRes,apodName ))
    spcSLW2=SpirePreprocessedCube(preCubeSLW2["spectrum2d"])
    spcSLW=spcSLW.append(spcSLW2)


outDataPool="OrionAll_SPIRE-FTS_"+Version


preCubeList={"SSW":spcSSW,"SLW":spcSLW}
mapSampling="full"
# Carry out regridding
for array in ["SSW", "SLW"]:
    # Create a pre-processed cube (not regularly gridded):
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing:
    wcs = SpecWcsCreator.createWcs(preCubeList[array], gridSpacing[mapSampling][array], gridSpacing[mapSampling][array])
    for projtype in ["naive","nearest","gridding","convolution"]:
    	# Regrid the data using the Naive Projection algorithm:
    	cube=  spireProjection(spc=preCubeList[array], wcs=wcs, projectionType=projtype)
    	# Sort the metadata into a logical order
    	metaDataSorter(cube)
        HistoryTask_forVersion=cube["History"]["HistoryTasks"]
        cube.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
        cube.meta["wavelength"]=StringParameter(str(array),"wavelength")
	cube.meta["object"]=StringParameter(str(array),"wavelength")
        # Save the cube to FITS:
        simpleFitsWriter(cube, "%s%s_%s_%s_%s_%s_cube%s.fits"%(outDir, myObsid, \
                     outDataPool,processRes, array, projtype,apodName))
    # Save the processed products back into the Observation Context:
    #if obs.level2 and not apodize:
    #    obs.level2.setProduct("%s_%s_unapodized_spectrum"%(res, array), cube)
    #elif obs.level2 and apodize:
    #    obs.level2.setProduct("%s_%s_apodized_spectrum"%(res, array), cube)

print "Processing of observations %s complete"%(myObsid)
#### End of the script ####
