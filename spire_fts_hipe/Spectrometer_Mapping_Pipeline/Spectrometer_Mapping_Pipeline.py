# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2010 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            data using the latest SPIRE calibration products. The user 
#            can (C) provide satellite housekeeping products in a local 
#            directory and (D) produce either unapodized or apodized 
#            spectra. (E) An output directory is required to store 
#            results as fits files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (F) The
#            user must specify the size of the spatial pixels of the
#            resulting cubes. (G) For observations taken in High + Low 
#            Resolution mode, the user must select whether to process 
#            data in high or low resolution.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated: 14/Oct/2010
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
# (A) Specific OBSID and the name of the data storage in your Local Pool:
myObsid    = enterOBSID
myDataPool = "Enter Pool name here"
#
# (B) N/A
#
# (C) If the observation context does not contain a valid satellite
#     housekeeping product, then specify a local directory with such products:
hkpDir = "/enter/path/here/"
#
# (D) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
apodize = 0
if apodize:
    apodName = "NB_15"
else:
    apodName = "unapod"
#
# (E) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
outDir = "/enter/path/here/"
#
# (F) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:
gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
#
# (G) HR+LR observations Only: 
#     Define whether to process LR or HR data
processRes = "HR"
#
# (H) N/A
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
# Define a Jython  routine to fix the size of interferograms:
def makeSameOpds(sdi):
    res = sdi.commandedResolution
    if obs.meta["commandedResolution"].value == "H+L" and processRes == "LR":
        res = "LR"
    opdRanges = {"LR": [-0.555, 0.560],\
                 "MR": [-2.395, 2.400],\
                 "HR": [-0.555, 12.645],\
                 "CR": [-2.395, 12.645]}
    for scan in sdi.getScanNames():
        for det in sdi[scan].getChannelNames():
            thisIfgm = sdi[scan].getChannel(det).copy()
            thisOpd = thisIfgm.getOpdData()
            wh = thisOpd.where(thisOpd.ge(opdRanges[res][0]).and\
                              (thisOpd.le(opdRanges[res][1])))
            for col in thisIfgm.getColumnNames():
                thisIfgm[col].data = thisIfgm[col].data[wh]
            sdi[scan].setChannel(thisIfgm)
    return sdi

# map of the jiggle positions for full and intermediate sampling:
def getIntermediateJigEquiv(jiggle):
    if jiggle==0:
        return 6
    if jiggle==1:
        return 4
    if jiggle==2:
        return 12
    if jiggle==3:
        return 14
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################
# Define the thermistors, dark pixels and resistors:
thermistors = ["SLWT1", "SLWT2", "SSWT1", "SSWT2", \
               "SSWDP1", "SSWDP2", "SLWDP1", "SLWDP2"]
resistors   = ["SSWR1", "SLWR1"]

# Load in an observation context into HIPE:
obs = getObservation(myObsid, poolName=myDataPool)

print "Processing observation %i (0x%X) from data pool %s."%(myObsid, myObsid, myDataPool)

# Calibration Context and Calibration Files 
# get the latest calibration tree relevant to HCSS v5 from your local disc 
# For details on downloading to disc, see the "Calibration" chapter in the Data 
# Reduction Guide 
# cal = spireCal(pool="spire_cal_5_0")
# attach it to observation context
# obs.calibration.update(cal)

# Extract necessary Calibration Products from the Observation Context
nonLinCorr     = obs.calibration.spec.nonLinCorr
tempDriftCorr  = obs.calibration.spec.tempDriftCorr
lpfPar         = obs.calibration.spec.lpfPar
chanTimeConst  = obs.calibration.spec.chanTimeConst
bsmPos         = obs.calibration.spec.bsmPos
detAngOff      = obs.calibration.spec.detAngOff
smecZpd        = obs.calibration.spec.smecZpd
chanTimeOff    = obs.calibration.spec.chanTimeOff
smecStepFactor = obs.calibration.spec.smecStepFactor
phaseCorrLim   = obs.calibration.spec.phaseCorrLim
bandEdge       = obs.calibration.spec.bandEdge

# Extract necessary Auxiliary Products from the Observation Context
hpp  = obs.auxiliary.pointing
siam = obs.auxiliary.siam
hk = obs.auxiliary.hk
if hk.odNumber != obs.odNumber:
  # if necessary, get the satellite housekeeping product from the local drive:
  hk = fitsReader("%shkp_OD%i.fits"%(hkpDir,obs.odNumber))

# Start to process the observation from Level 0.5
# Process each SMEC scan building block (0xa106) individually, append to a list 

sdsList  = []
# Each building block is a different jiggle position
for bbid in obs.level0_5.getBbids(0xa106):
    print "Processing building block 0x%X"%(bbid)
    sdt   = obs.level0_5.get(bbid).sdt
    nhkt  = obs.level0_5.get(bbid).nhkt
    smect = obs.level0_5.get(bbid).smect
    bsmt  = obs.level0_5.get(bbid).bsmt

    # Extract the jiggle ID from the metadata
    jiggId = sdt.meta['jiggId'].value
    # Correct the jiggle ID for intermediate sampling
    if obs.meta['mapSampling'].value == "intermediate":
        jiggId = getIntermediateJigEquiv(jiggId)
    # Correct the jiggle ID for sparse sampling
    if obs.meta['mapSampling'].value == "sparse":
        jiggId = 6

    # Extract raster ID from the metadata
    raster = sdt.meta['pointNum'].value

    # -----------------------------------------------------------
    # 1st level deglitching:    
    sdt = waveletDeglitcher(sdt, reconstructionPointsAfter=3, \
                            reconstructionPointsBefore=2, \
                            correctGlitches=True,\
                            scaleMin=1, scaleMax=8, scaleInterval=5,\
                            holderMin=-1.4, holderMax=-0.6,\
                            correlationThreshold=0.85,\
                            optionReconstruction="polynomialFitting",\
                            degreePoly=6, fitPoints=8)
       
    # -----------------------------------------------------------
    # Run the Non-linearity and Temp Drift correction steps
    sdt = specNonLinearityCorrection(sdt, nonLinCorr=nonLinCorr)
    sdt = temperatureDriftCorrection(sdt, tempDriftCorr=tempDriftCorr) 
    
    # Remove the thermistors, dark pixels and resistors:
    for chan in sdt.getChannelNames():
        if chan in thermistors or chan in resistors:
            sdt.removeColumn(chan)

    # -----------------------------------------------------------
    # Repair clipped samples where needed:
    sdt = clippingCorrection(sdt)
       
    # -----------------------------------------------------------
    # Time domain phase correction:
    sdt = timeDomainPhaseCorrection(sdt,\
                            lpfPar=lpfPar,\
                            chanTimeConst=chanTimeConst) 
       
    # -----------------------------------------------------------
    # Add pointing info:
    bat = calcBsmAngles(bsmt, bsmPos=bsmPos)
    spp = createSpirePointing(hpp=hpp, siam=siam, \
                            detAngOff=detAngOff, bat=bat)
       
    # -----------------------------------------------------------
    # Create interferogram:
    sdi = createIfgm(sdt=sdt, smect=smect, nhkt=nhkt, spp=spp, \
                     smecZpd=smecZpd,\
                     chanTimeOff=chanTimeOff,\
                     smecStepFactor=smecStepFactor)
    # adjust OPD ranges to ensure that they are the same for all scans
    sdi = makeSameOpds(sdi)

    # Update the resolution if processing a H+L observation as LR
    if obs.meta["commandedResolution"].value == "H+L" and processRes == "LR":
        sdi.commandedResolution = "LR"
       
    # -----------------------------------------------------------
    # Baseline correction:
    sdi = baselineCorrection(sdi, type="fourier", threshold=4) 

    # -----------------------------------------------------------
    # 2nd level deglitching:
    sdi = deglitchIfgm(sdi, deglitchType="MAD")

    # -----------------------------------------------------------
    # Phase correction:
    biasMode  = sdi.meta["biasMode"].value
    startDate = sdi.startDate
    specNonLinPhase = obs.calibration.spec.nonLinPhaseList.getProduct( \
                         jiggId, biasMode, startDate)
    sdi = phaseCorrection(sdi, \
                         specNonLinPhase=specNonLinPhase, \
                         phaseCorrLim=phaseCorrLim)

    # Un-comment the following line to calculate the residual phase
    # (see Data Reduction Guide for explanation)
    # dsds   = fourierTransform(sdi=sdi, ftType="prePhaseCorr", zeroPad="None")

    # -----------------------------------------------------------
    # Apodize the interferogram:
    if apodize:
        sdi = apodizeIfgm(sdi, apodType="postPhaseCorr", apodName=="a%s"%apodName)

    # -----------------------------------------------------------
    # Fourier transform to the spectral domain:
    ssds = fourierTransform(sdi, ftType="postPhaseCorr", zeroPad="standard")

    # -----------------------------------------------------------
    # Get the RSRF calibration products
    # In case a "calibration" observation is processed, the HR SCalSpecInstRsrf,
    # SCalSpecTeleRsrf and SCalSpecBeamParam products should be used:
    res = ssds.commandedResolution
    if res == "CR": 
        res = "HR"
    instRsrf = obs.calibration.spec.instRsrfList.getProduct( \
                         res, apodName, biasMode, startDate)
    # Read SCalSpecTeleRsrf from a user specified file if available
    try:
        teleRsrf = fitsReader(myTeleRsrf)    
    except:
        teleRsrf = obs.calibration.spec.teleRsrfList.getProduct( \
                         6, res, apodName, biasMode, startDate)
    beamParam = obs.calibration.spec.beamParamList.getProduct( \
                         res, apodName, biasMode, startDate)

    # -----------------------------------------------------------
    # Correction for instrument emission:
    ssds = instCorrection(sds=ssds, nhkt=nhkt, scalInstRsrf=instRsrf)

    # -----------------------------------------------------------
    # Average scans, keeping forward and reverse separate:
    asds = averageSpectra(ssds, separateScanDirections=1, \
                      INCLUDE_OOB=0, bandEdge=bandEdge)

    # -----------------------------------------------------------
    # Correction for telescope emission:
    asds = telescopeCorrection(asds, hk=hk, scalTeleRsrf=teleRsrf)

    # -----------------------------------------------------------
    # Get the flux conversion calibration products and apply to spectra
    asds = specFluxConversion(asds, specTeleRsrf=teleRsrf)

    # -----------------------------------------------------------
    # Average forward and reverse scans:
    asds = averageSpectra(asds, separateScanDirections=0, \
                      INCLUDE_OOB=0, bandEdge=bandEdge)

    # ----------------------------------------------------------- 
    # Append this scan to the list, taking account whether the resolution
    # was H+L or not
    if obs.meta["commandedResolution"].value == "H+L":
        # for processing all scans as LR
        if processRes == "LR":
            sdsList.append(asds)
        # for processing the HR scans
        elif processRes == asds.commandedResolution:
            sdsList.append(asds)
    # or, otherwise
    else:
        sdsList.append(asds)

    # -----------------------------------------------------------
    # Save individual FITS files
    simpleFitsWriter(asds, "%s%i_spectrum_%s_%i_%i.fits"%(outDir, \
                      myObsid, apodName, raster, jiggId))
                                        
# ---------------------------------------------------------------
# Carry out regridding
nearestNeighbourProjection = herschel.ia.toolbox.spectrum.projection.NearestNeighbourProjectionTask()

for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    preCube = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing
    grid = nearestNeighbourProjection.targetGrid(preCube.ra, preCube.dec, \
                           gridSpacing[array], gridSpacing[array], preCube.wave)
    # Regrid the data
    cube = nearestNeighbourProjection.project(preCube.flux, preCube.error, preCube.flag, \
                                              preCube.ra, preCube.dec, grid, None, True)
    # Save the cube
    simpleFitsWriter(cube, "%s%i_%s_%s_cube.fits"%(outDir, myObsid, array, apodName))

print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
