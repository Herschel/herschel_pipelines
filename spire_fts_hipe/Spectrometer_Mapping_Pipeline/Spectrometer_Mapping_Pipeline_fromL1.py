# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2012 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            using the latest SPIRE calibration products. The user 
#            can (C) produce either unapodized or apodized 
#            spectra. (D) An output directory is required to store 
#            results as FITS files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (E) For 
#            observations taken in High + Low Resolution mode, the user 
#            must select whether to process data in high or low resolution.
#            (F) The user must specify the size of the spatial pixels of 
#            the resulting cubes.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated:  04/April/2013
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
# (A) Specify OBSID:
myObsid = 1342198923
#
# (B) N/A
#
# (C) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
apodize = 0
if apodize:
    apodName="aNB_15"
    productName = 'apodized_spectrum'
else:
    apodName="unapod"
    productName = 'unapodized_spectrum'
#
# (D) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
inputDir= "/Users/kdassas/HerschelWS2013/aabergel168106865/"
outDir = "/Users/kdassas/HerschelWS2013/n7023_fts_results/"
#
# (E) For H+L observations only - changing this option has no effect for
#     observations that were not observed in "High+Low" mode:
#     Choose whether to process LR or HR data (from a HR+LR observation)
processRes="LR"

#
# (F) Specify the map pixel size for the final data cubes (SSW and SLW)
#     depending on the spatial sampling in units of degree:
sswFwhm = 19.0 / 3600.0
slwFwhm = 35.0 / 3600.0
gridSpacing={"full":        {"SSW": 0.5 * sswFwhm, "SLW": 0.5 * slwFwhm}, \
             "intermediate":{"SSW": 1.0 * sswFwhm, "SLW": 1.0 * slwFwhm}, \
             "sparse":      {"SSW": 2.0 * sswFwhm, "SLW": 2.0 * slwFwhm}}
#
###########################################################################

# Define the thermistors, dark pixels and resistors:
thermistors = ["SLWR1","SSWR1","SLWT1","SLWT2","SSWT1","SSWT2",\
               "SSWDP1","SSWDP2","SLWDP1","SLWDP2"]


obs=getObservation (path=inputDir, obsid=1342198923)

# TOO long for the workshop: bg("saveProduct(product=obsid_1342198923, pool='n7023_orig', tag='Orig Data From Archive')")
# Load an observation context into HIPE from the pool you have created:
#obs = getObservation(myObsid,poolName="n7023_orig")
# To specify a pool name use:
# obs = getObservation(myObsid, poolName="poolName") 
# To load data directly from the HSA use:
# obs = getObservation(myObsid, useHsa=True)

# Spectral resolution of the input data
res = obs.meta["commandedResolution"].value
raster=0

         
# Calibration Context and Calibration Files 
# Read the latest calibration tree relevant to HCSS v10 from the local disc:
#cal = spireCal(pool="spire_cal_10_1")

cal = spireCal(jarFile="/Users/kdassas/HerschelWS2013/spire_cal_10_1.jar")
#saveProduct(product=cal, pool='spire_cal_10_1')
#cal = spireCal(calTree="spire_cal_10_1", saveTree=True)
# Attach the updated calibration tree to the observation context
obs.calibration.update(cal)

######### specific code to fill in the sdsLisl, input for cube computing.
sdsList = SpireMapContext()
NbJiggId=obs.refs["level1"].product.getRefs().size()
print NbJiggId
for jiggId in range(0,NbJiggId):
	print jiggId,raster,res
	sdslist_temp = obs.level1.getProduct('Point_%i_Jiggle_%i_%s'%(raster,jiggId,res)).getProduct(productName)
	sdsList.setProduct("%d"%(sdsList.refs.size()), sdslist_temp)

print "Processing observation %i (0x%X)"%(myObsid, myObsid)
# ---------------------------------------------------------------
# Carry out regridding
mapSampling = obs.meta['mapSampling'].value
### Choose between: projection= naive" or "nearest" or"gridding"
projection="gridding"
for array in ["SSW", "SLW"]:
    # Create a pre-processed cube (not regularly gridded):
    preCube = spirePreprocessCube(context=sdsList, arrayType=array, unvignetted=True)
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing:
    wcs = SpireWcsCreator.createWcs(preCube, gridSpacing[mapSampling][array], gridSpacing[mapSampling][array])
    # Regrid the data using the Naive Projection algorithm:
    if array == "SSW": 
        cube = cubeSSW = spireProjection(spc=preCube, wcs=wcs, projectionType=projection)
    elif array == "SLW": 
        cube = cubeSLW = spireProjection(spc=preCube, wcs=wcs, projectionType=projection)
    # Save the cube to FITS:
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_cube_%s.fits"%(outDir, myObsid, \
                     cube.meta["processResolution"].value, array, apodName,projection))
    # Save the processed products back into the Observation Context:
    if obs.level2 and not apodize:
        obs.level2.setProduct("%s_%s_unapodized_spectrum_%s"%(res, array,projection), cube)
    elif obs.level2 and apodize:
        obs.level2.setProduct("%s_%s_apodized_spectrum_%s"%(res, array,projection), cube)

# Finally we can save the new reprocessed observation back to your hard disk.
# Note that only the parts of the Observation Context covered by the script
# will have been updated! (i.e. apodized/unapodized products).
# Uncomment the next line and choose a poolName, either the existing one or a new one
#saveObservation(obs, poolName="n7023_reprocessed", saveCalTree=True)

print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
