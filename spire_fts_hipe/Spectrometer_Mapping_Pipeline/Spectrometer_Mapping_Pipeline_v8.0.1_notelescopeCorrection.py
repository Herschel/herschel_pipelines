# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2011 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            data using the latest SPIRE calibration products. The user 
#            can (C) produce either unapodized or apodized 
#            spectra. (D) An output directory is required to store 
#            results as fits files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (E) For 
#            observations taken in High + Low Resolution mode, the user 
#            must select whether to process data in high or low resolution.
#            (G) The user must specify the size of the spatial pixels of 
#            the resulting cubes.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated:  24/Nov/2011
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
# (A) Specific OBSID and the name of the data storage in your Local Pool:
#myObsid = enterOBSID
#myDataPool = "Enter Pool name here"
#
# (B) N/A
#
# (C) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
#apodize = 0

if apodize:
    apodName="aNB_15"
else:
    apodName="unapod"
#
# (D) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
outDir = "/data/glx-herschel/data1/herschel/FTS_SPIRE_Fits/"
#
# (E) For H+L observations only - changing this option has no effect for
#     observations that were not observed in "High+Low" mode:
#     Choose whether to process LR or HR data (from a HR+LR observation)
processRes="LR"
#
# (F) N/A
#
# (G) Specify the map pixel size for the final data cubes (SSW and SLW)
#     depending on the spatial sampling in units of degree:
sswFwhm = 19.0 / 3600.0
slwFwhm = 35.0 / 3600.0
gridSpacing={"full":        {"SSW": 0.5 * sswFwhm, "SLW": 0.5 * slwFwhm}, \
             "intermediate":{"SSW": 1.0 * sswFwhm, "SLW": 1.0 * slwFwhm}, \
             "sparse":      {"SSW": 2.0 * sswFwhm, "SLW": 2.0 * slwFwhm}}
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
# map of the jiggle positions for full and intermediate sampling:
def getIntermediateJigEquiv(jiggle):
    if jiggle==0:
        return 6
    if jiggle==1:
        return 4
    if jiggle==2:
        return 12
    if jiggle==3:
        return 14
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################
# Define the thermistors, dark pixels and resistors:
thermistors = ["SLWR1","SSWR1","SLWT1","SLWT2","SSWT1","SSWT2",\
               "SSWDP1","SSWDP2","SLWDP1","SLWDP2"]

# Load an observation context into HIPE:
obs = getObservation(myObsid, useHsa=True)

#print "Processing observation %i (0x%X) from data pool %s."%(myObsid, myObsid, myDataPool)
print "Processing observation %i (0x%X) ."%(myObsid, myObsid )

# State memory requirements (assuming there is enough RAM for the operating system)
# Assume full spatial sampling and high spectral resolution
print "####  REQUIRED MEMORY FOR NOMINAL MODE (CONSERVATIVE ESTIMATE)  ####"
#print "Allocate at least %iMB of RAM to process this %i rep. mapping observation"\
#      %(4096.0*obs.meta["numRepetitions"].value/10, obs.meta["numRepetitions"].value)
         
# Calibration Context and Calibration Files 
# Read the latest calibration tree relevant to HCSS v8 from the local disc:
cal = spireCal(pool="spire_cal_8_1")
# TO CORRECT AN ERROR ON THE ABOVE LINE, run the following command once only
# to load and save the calibration tree from the Archive (may take some time):
# (for more details, see the "Calibration" chapter in the SPIRE
# Data Reduction Guide)

#cal = spireCal(calTree="spire_cal_8_1", saveTree=True)

# Attach the updated calibration tree to the observation context
obs.calibration.update(cal)

# Find out the bias mode of the observation (nominal/bright)
biasMode = obs.meta["biasMode"].value

# Extract necessary Calibration Products from the Observation Context
lpfPar         = obs.calibration.spec.lpfPar
phaseCorrLim   = obs.calibration.spec.phaseCorrLim
chanTimeConst  = obs.calibration.spec.chanTimeConst
bsmPos         = obs.calibration.spec.bsmPos
detAngOff      = obs.calibration.spec.detAngOff
smecZpd        = obs.calibration.spec.smecZpd
chanTimeOff    = obs.calibration.spec.chanTimeOff
smecStepFactor = obs.calibration.spec.smecStepFactor
opdLimits      = obs.calibration.spec.opdLimits
bandEdge       = obs.calibration.spec.bandEdge
nonLinCorr     = obs.calibration.spec.nonLinCorr
tempDriftCorr  = obs.calibration.spec.tempDriftCorr
bolPar         = obs.calibration.spec.bolPar
chanNum        = obs.calibration.spec.chanNum
brightGain     = obs.calibration.spec.brightGain
nomPcal        = obs.calibration.spec.nomPcal

# Extract necessary Auxiliary Products from the Observation Context
hpp  = obs.auxiliary.pointing
siam = obs.auxiliary.siam
hk = obs.auxiliary.hk
#hk = fitsReader("%shkp_OD%i.fits"%(hkpDir,obs.odNumber))
# Reprocess the observation from Level-0 to Level-0.5 for Bright mode only
# (for nominal mode, use the Level-0.5 data directly from the observation)
if biasMode == "nominal":
    level0_5 = obs.level0_5
elif biasMode == "bright":
    level0_5 = engConversion(level0=obs.level0, cal=obs.calibration, usePhases=True)
    # Save the processed Level-0.5 data back into the Observation Context:
    obs.level0_5 = level0_5

# For Bright mode only: process the PCAL flash building block (0xb6b9)
if biasMode == "bright":
    sdt = level0_5.get(0xb6b90001).sdt
    scut = level0_5.get(0xb6b90001).scut
    sdt = calcOpticalPower(sdt, chanNum=chanNum, bolPar=bolPar, computeRes=True)
    sdt["voltage"] = sdt["temperature"]
    # Consult the Pipeline Specification Manual for more options of the sigmaKappaDeglitcher
    sdt = sigmaKappaDeglitcher(sdt, kappa=4.0, largeGlitchMode="ADDITIVE", \
                               largeGlitchDiscriminatorTimeConstant=4,\
                               largeGlitchRemovalTimeConstant=6)
    specPcal = pcal(sdt, scut=scut)

# Start to process the observation from Level 0.5
# Process each SMEC scan building block (0xa106) individually, append to a list 
sdsList = SpireMapContext()
# Each building block is a different jiggle position
object_name=obs.meta["object"].value
Version="8.0.3287"
outDataPool=object_name+"_SPIRE-FTS_noTelescopeCorrection_"+Version
for bbid in level0_5.getBbids(0xa106):
    Version="8.0.3287"
    print"Processing building block 0x%X (%i/%i)"%(bbid, bbid-0xa1060000L, len(obs.level0_5.getBbids(0xa106)))
    sdt   = level0_5.get(bbid).sdt
    # Record the calibration tree version used by the pipeline:
    sdt.calVersion = obs.calibration.version
    nhkt  = level0_5.get(bbid).nhkt
    smect = level0_5.get(bbid).smect
    bsmt  = level0_5.get(bbid).bsmt
    # Extract the jiggle ID from the metadata:
    jiggId = sdt.meta["jiggId"].value
    # Correct the jiggle ID for intermediate sampling:
    if obs.meta["mapSampling"].value == "intermediate":
        jiggId = getIntermediateJigEquiv(jiggId)
    # Correct the jiggle ID for sparse sampling:
    if obs.meta["mapSampling"].value == "sparse":
        jiggId = 6
    # Extract raster ID from the metadata:
    raster = sdt.meta["pointNum"].value
    # -----------------------------------------------------------
    # 1st level deglitching:    
    # Consult the Pipeline Specification Manual for more options of the waveletDeglitcher
    sdt = waveletDeglitcher(sdt)
    # -----------------------------------------------------------
    if biasMode == "nominal":
        # Run the Non-linearity and Temp Drift correction steps:
        sdt = specNonLinearityCorrection(sdt, nonLinCorr=nonLinCorr)
        sdt = temperatureDriftCorrection(sdt, tempDriftCorr=tempDriftCorr)
        # Remove the thermistors, dark pixels and resistors:
        sdt = filterChannels(sdt, removeChannels=thermistors, keepSet="UNVIGNETTED")
    elif biasMode == "bright":
        # Convert the voltage to bolometer temperature (bright source setting only):
        sdt = calcOpticalPower(sdt, chanNum=chanNum, bolPar=bolPar, computeRes=True)
        sdt["voltage"] = sdt["temperature"]
        # Remove the thermistors, dark pixels and resistors:
        sdt = filterChannels(sdt, removeChannels=thermistors, keepSet="UNVIGNETTED")
        # Apply bright PCAL gain (bright source setting only):
        sdt = specApplyPcalGain(sdt, nomPcal=nomPcal, specPcal=specPcal)
    # -----------------------------------------------------------
    # Repair clipped samples where needed:
    sdt = clippingCorrection(sdt)
    # -----------------------------------------------------------
    # Time domain phase correction:
    sdt = timeDomainPhaseCorrection(sdt, nhkt, lpfPar=lpfPar, \
               phaseCorrLim=phaseCorrLim, chanTimeConst=chanTimeConst)        
    sdt.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
    simpleFitsWriter(sdt, "%s%i_sdt_%s_%s_%i_%i.fits"%(outDir, \
                      myObsid,outDataPool, apodName, raster, jiggId))
    # -----------------------------------------------------------
    # Add pointing info:
    bat = calcBsmAngles(bsmt, bsmPos=bsmPos)
    spp = createSpirePointing(hpp=hpp, siam=siam, \
                            detAngOff=detAngOff, bat=bat)
    # -----------------------------------------------------------
    # Create interferogram:
    sdi = createIfgm(sdt, smect=smect, nhkt=nhkt, spp=spp, \
                     smecZpd=smecZpd,\
                     chanTimeOff=chanTimeOff,\
                     smecStepFactor=smecStepFactor)
    # -----------------------------------------------------------
    # Update the resolution if processing a H+L observation as LR
    if obs.meta["commandedResolution"].value == "H+LR" and processRes == "LR":
        sdi.commandedResolution = "LR"
    # Adjust OPD ranges to ensure that they are the same for all scans
    sdi = makeSameOpds(sdi, opdLimits=opdLimits)
    # -----------------------------------------------------------
    # Baseline correction:
    sdi = baselineCorrection(sdi, type="fourier", threshold=4) 
    # -----------------------------------------------------------
    # 2nd level deglitching:
    sdi = deglitchIfgm(sdi, deglitchType="MAD")
    # -----------------------------------------------------------
    # Apodize, Phase-correct & Fourier transform interferograms.
    # The phase correction is calculated from an averaged LR interferogram:
    lowResSdi = averageInterferogram(sdi.copy())
    lowResSdi.commandedResolution = "LR"
    lowResSdi = makeSameOpds(lowResSdi, opdLimits=opdLimits)
    # Make a product to contain the phase corrected spectrum:
    ssds = SpectrometerDetectorSpectrum()
    # Carry out Phase Correction (different order for different resolutions):
    res = sdi.commandedResolution
    # For High (& Calibration) resolution:
    if res == "HR" or res == "CR":
        sdi = phaseCorrection(sdi, avgSdi=lowResSdi, outsds=ssds)
        # Un-comment the following line to calculate the residual phase
        # (see Data Reduction Guide for explanation)
        #dsds   = fourierTransform(sdi, ftType="prePhaseCorr", zeroPad="None")
        if apodize:
            sdi = apodizeIfgm(sdi, apodType="postPhaseCorr", apodName=apodName)
        ssds = fourierTransform(sdi, ftType="postPhaseCorr", zeroPad="standard", \
                                spectralUnit="GHz")
    # For Medium and Low resolution:
    elif res == "MR" or res == "LR":
        if apodize:
            sdi = apodizeIfgm(sdi, apodType="prePhaseCorr", apodName=apodName)
        sdi  = phaseCorrection(sdi, avgSdi=lowResSdi, outsds=ssds, spectralUnit="GHz")
        # Un-comment the following line to calculate the residual phase
        # (see Data Reduction Guide for explanation)
        # dsds   = fourierTransform(sdi, ftType="prePhaseCorr", zeroPad="None")
    sdi.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
    simpleFitsWriter(sdi, "%s%i_sdi_%s_%s_%i_%i.fits"%(outDir, \
                      myObsid,outDataPool, apodName, raster, jiggId))
    # -----------------------------------------------------------
    # Get the RSRF calibration products
    startDate = ssds.startDate
    # In case a "calibration" observation is processed, the HR SCalSpecInstRsrf,
    # and SCalSpecTeleRsrf products should be used:
    if res == "CR": 
        res = "HR"
    instRsrf = obs.calibration.spec.instRsrfList.getProduct( \
                         res, apodName, biasMode, startDate)
    teleRsrf = obs.calibration.spec.teleRsrfList.getProduct( \
                         jiggId, res, apodName, biasMode, startDate)
    # -----------------------------------------------------------
    # Remove out of band data
    ssds = removeOutOfBand(ssds, bandEdge=bandEdge)
    # -----------------------------------------------------------
    # Apply bright gain correction (bright source setting only):
    if biasMode == "bright":
        ssds = specApplyBrightGain(ssds, brightGain=brightGain)
    # -----------------------------------------------------------
    # Correction for instrument emission:
    ssds = instCorrection(ssds, nhkt=nhkt, instRsrf=instRsrf)
    # -----------------------------------------------------------
    # Get the flux conversion calibration products and apply to spectra:
    ssds = specExtendedFluxConversion(ssds, teleRsrf=teleRsrf)
    # -----------------------------------------------------------
    # Correction for telescope emission:
    #ssds = telescopeCorrection(ssds, hk=hk)
    # ----------------------------------------------------------- 
    # Append this scan to the list, taking account whether the resolution
    # was H+L or not
    ssds.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
    if obs.meta["commandedResolution"].value == "H+LR":
        # for processing all scans as LR
        if processRes == "LR":
            sdsList.setProduct("%d"%(sdsList.refs.size()),ssds)
        # for processing the HR scans
        elif processRes == ssds.commandedResolution:
            sdsList.setProduct("%d"%(sdsList.refs.size()),ssds)
    # or, otherwise
    else:
        sdsList.setProduct("%d"%(sdsList.refs.size()),ssds)
        # Save individual FITS files:
    simpleFitsWriter(ssds, "%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
                myObsid,outDataPool, ssds.commandedResolution, apodName, raster, bbid-0xa1060001L))
    # -----------------------------------------------------------
    # Save the Level-1 data back into the Observation Context:
    #if obs.level1:
    #    obs.level1.getProduct("Point_%i_Jiggle_%i_%s"%(raster,\
    #        sdi.meta["jiggId"].value,sdi.commandedResolution)).\
    #                      setProduct("interferogram", sdi)
    #    if not apodize:
    #        obs.level1.getProduct("Point_%i_Jiggle_%i_%s"%(raster,\
    #         ssds.meta["jiggId"].value,ssds.commandedResolution)).\
    #         setProduct("unapodized_spectrum", ssds)
    #    else:
    #        obs.level1.getProduct("Point_%i_Jiggle_%i_%s"%(raster,\
    #         ssds.meta["jiggId"].value,ssds.commandedResolution)).\
    #         setProduct("apodized_spectrum", ssds)
                                   
# ---------------------------------------------------------------
# Carry out regridding
ListMetaL2=["type","description","instrument","startDate","endDate","obsid","odNumber","cusMode","instMode","obsMode","origin", "pointingMode",\
               "aorLabel","aot","equinox","object","raDeSys","raNominal","decNominal","ra","dec","posAngle","telescope","observer","proposal",\
               "numRepetitions","mapSampling","commandedResolution"]
metaObs=obs.meta
calVersion = obs.calibration.version
mapSampling = obs.meta['mapSampling'].value
for array in ["SSW", "SLW"]:
    # Create a pre-processed cube (not regularly gridded):
    preCube = spirePreprocessCube(context=sdsList, arrayType=array, UNVIGNETTED=True)
    preCubeAsSpectrum2d = preCube.toSpectrum2d()
    prod=Product()
    prod["spectrum"]=preCubeAsSpectrum2d
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing:
    wcs = SpireWcsCreator.createWcs(preCube, gridSpacing[mapSampling][array], gridSpacing[mapSampling][array])
    # Regrid the data using the Naive Projection algorithm:
    for projtype in ["naive","nearest","gridding"]:
   	if array == "SSW": 
        	cube = cubeSSW = spireProjection(spc=preCube, wcs=wcs, projectionType=projtype)
    	elif array == "SLW": 
        	cube = cubeSLW = spireProjection(spc=preCube, wcs=wcs, projectionType=projtype)
    # Add Metadata and Save spectrum 2d (prod) and cubes to FITS:
        for j in range(len(ListMetaL2)):
                    try:
                              prod.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              cube.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              print metaObs[ListMetaL2[j]]
                    except:
                              print "meta pas dans obs: ", ListMetaL2[j]
                              #framesLevel1.meta[ListMetaL1[j]]=StringParameter("None",ListMetaL1[j])
   	HistoryTask_forVersion=cube["History"]["HistoryTasks"]
   	Version=HistoryTask_forVersion["BuildVersion"].getData()
   	prod.meta["BuildVersion"]=StringParameter(str(Version[0]),"BuildVersion")
   	prod.meta["CalVersion"]=StringParameter(str(calVersion),"CalibrationVersion")
   	prod.meta["wavelength"]=StringParameter(str(array),"wavelength")
   	cube.meta["CalVersion"]=StringParameter(str(calVersion),"CalibrationVersion")
   	cube.meta["BuildVersion"]=StringParameter(str(Version[0]),"BuildVersion")
   	cube.meta["wavelength"]=StringParameter(str(array),"wavelength")
    	simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
    simpleFitsWriter(prod, "%s%i_%s_%s_%s_spectrum2d.fits"%(outDir, myObsid,outDataPool, array, apodName))
    # Save the processed products back into the Observation Context:
    #if obs.level2 and not apodize:
    #    obs.level2.setProduct("%s_%s_unapodized_spectrum"\
    #         %(cube.meta["commandedResolution"].value, array), cube)
    #elif obs.level2 and apodize:
    #    obs.level2.setProduct("%s_%s_apodized_spectrum"\
    #         %(cube.meta["commandedResolution"].value, array), cube)


# Finally we can save the new reprocessed observation back to your hard disk.
# Note that only the parts of the Observation Context covered by the script
# will have been updated! (i.e. apodized/unapodized products).
# Uncomment the next line and choose a poolName, either the existing one or a new one
#saveObservation(obs, poolName="enter-a-poolname", saveCalTree=True)


print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
