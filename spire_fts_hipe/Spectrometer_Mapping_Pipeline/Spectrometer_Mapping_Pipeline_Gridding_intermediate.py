# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2011 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            data using the latest SPIRE calibration products. The user 
#            can (C) produce either unapodized or apodized 
#            spectra. (D) An output directory is required to store 
#            results as fits files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (E) The
#            user must specify the size of the spatial pixels of the
#            resulting cubes. (F) For observations taken in High + Low 
#            Resolution mode, the user must select whether to process 
#            data in high or low resolution. (G) Select the path and
#            prefix of the fits files that contain jiggle-dependent 
#            telescope RSRFs - if available and desived.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated:  10/May/2011
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
hkpDir = "/data/glx-herschel/data1/herschel/FTS_HK/"
from herschel.ia.pal.util import StorageResult
from herschel.ia.toolbox.spectrum.projection import WcsCreator
from herschel.ia.toolbox.spectrum.projection import NearestNeighbourProjectionTask
from herschel.spire.ia.pipeline.spec.regrid import NaiveProjectionTask

execfile('/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py')
# (A) Specific OBSID and the name of the data storage in your Local Pool:
#myObsid    = 0x50006BF7L
#myDataPool = "HD37041-SPIRE-FTS_orig"
#outDataPool = "HD37041-SPIRE-FTS_apod1_v7.0.1866"

#
# (B) N/A
#
# (C) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
#apodize = 1
if apodize:
    apodName = "aNB_15"
else:
    apodName = "unapod"
#
# (D) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
#outDir = "/SPIRE/TMP/"
outDir =  "/data/glx-herschel/data1/herschel/FTS_SPIRE_Fits/"
#
# (E) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:
gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
#
# (F) For H+L observations only - changing this option has no effect for
#     observations that were not observed in "High+Low" mode: 
#     Choose whether to process LR or HR data (from a HR+LR observation)
processRes = "HR"
#
# (G) Specify the path of the jiggle-dependent telescope RSRF and the 
#     prefix for the fits file containing the observation ID of the 
#     dark sky observation - if available and desired:
myTeleRsrf = "/enter/path/SCalSpecTeleRsrf_0x50000000"
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
# map of the jiggle positions for full and intermediate sampling:
def getIntermediateJigEquiv(jiggle):
    if jiggle==0:
        return 6
    if jiggle==1:
        return 4
    if jiggle==2:
        return 12
    if jiggle==3:
        return 14
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################
# Define the thermistors, dark pixels and resistors:
thermistors = ["SLWT1", "SLWT2", "SSWT1", "SSWT2", \
               "SSWDP1", "SSWDP2", "SLWDP1", "SLWDP2"]
resistors   = ["SSWR1", "SLWR1"]

# Load in an observation context into HIPE:
obs = getObservation(myObsid, poolName=myDataPool)

print "Processing observation %i (0x%X) from data pool %s."%(myObsid, myObsid, myDataPool)

# Calibration Context and Calibration Files 
# Read the latest calibration tree relevant to HCSS v6 from the local disc:
cal = spireCal(pool="spire_cal_7_0")
# TO CORRECT AN ERROR ON THE ABOVE LINE, run the following two lines 
# once only to load the calibration tree from the Archive (may take some time):
# (for more details, see the "Calibration" chapter in the SPIRE
# Data Reduction Guide)

#cal = spireCal(calTree="spire_cal_7_0")
#localStoreWriter(cal, "spire_cal_7_0")

# Attach the updated calibration tree to the observation context
obs.calibration.update(cal)


obs.meta['mapSampling']=StringParameter('intermediate')
obs.meta['commandedResolution']=StringParameter('H')



# Extract necessary Calibration Products from the Observation Context
nonLinCorr     = obs.calibration.spec.nonLinCorr
tempDriftCorr  = obs.calibration.spec.tempDriftCorr
lpfPar         = obs.calibration.spec.lpfPar
phaseCorrLim   = obs.calibration.spec.phaseCorrLim
chanTimeConst  = obs.calibration.spec.chanTimeConst
bsmPos         = obs.calibration.spec.bsmPos
detAngOff      = obs.calibration.spec.detAngOff
smecZpd        = obs.calibration.spec.smecZpd
chanTimeOff    = obs.calibration.spec.chanTimeOff
smecStepFactor = obs.calibration.spec.smecStepFactor
opdLimits      = obs.calibration.spec.opdLimits
bandEdge       = obs.calibration.spec.bandEdge

# Extract necessary Auxiliary Products from the Observation Context
hpp  = obs.auxiliary.pointing
siam = obs.auxiliary.siam
hk = obs.auxiliary.hk

hk = fitsReader("%shkp_OD%i.fits"%(hkpDir,obs.odNumber))

# Start to process the observation from Level 0.5
# Process each SMEC scan building block (0xa106) individually, append to a list 
level1=Level1Context(myObsid)
sdsList  = []
# Each building block is a different jiggle position
for bbid in obs.level0_5.getBbids(0xa106):
    print "Processing building block 0x%X (%i/%i)"%(bbid, bbid-0xa1060000L, len(obs.level0_5.getBbids(0xa106)))
    sdt   = obs.level0_5.get(bbid).sdt
    nhkt  = obs.level0_5.get(bbid).nhkt
    smect = obs.level0_5.get(bbid).smect
    bsmt  = obs.level0_5.get(bbid).bsmt
    # Extract the jiggle ID from the metadata
    jiggId = sdt.meta['jiggId'].value
    # Correct the jiggle ID for intermediate sampling
    if obs.meta['mapSampling'].value == "intermediate":
        jiggId = getIntermediateJigEquiv(jiggId)
    # Correct the jiggle ID for sparse sampling
    if obs.meta['mapSampling'].value == "sparse":
        jiggId = 6
    # Extract raster ID from the metadata
    raster = sdt.meta['pointNum'].value
    # -----------------------------------------------------------
    # 1st level deglitching:    
    sdt = waveletDeglitcher(sdt, reconstructionPointsAfter=3, \
                            reconstructionPointsBefore=2, \
                            correctGlitches=True,\
                            scaleMin=1, scaleMax=8, scaleInterval=5,\
                            holderMin=-1.4, holderMax=-0.6,\
                            correlationThreshold=0.85,\
                            optionReconstruction="polynomialFitting",\
                            degreePoly=6, fitPoints=8)
    # -----------------------------------------------------------
    # Run the Non-linearity and Temp Drift correction steps
    sdt = specNonLinearityCorrection(sdt, nonLinCorr=nonLinCorr)
    sdt = temperatureDriftCorrection(sdt, tempDriftCorr=tempDriftCorr)
    # Remove the thermistors, dark pixels and resistors:
    sdt = filterChannels(sdt, removeChannels=String1d(thermistors+resistors))
    # -----------------------------------------------------------
    # Repair clipped samples where needed:
    sdt = clippingCorrection(sdt)
    # -----------------------------------------------------------
    # Time domain phase correction:
    sdt = timeDomainPhaseCorrection(sdt, nhkt, lpfPar=lpfPar, \
               phaseCorrLim=phaseCorrLim, chanTimeConst=chanTimeConst)  
    level1.addProduct(sdt)
    simpleFitsWriter(sdt, "%s%i_sdt_%s_%s_%i_%i.fits"%(outDir, \
                      myObsid,outDataPool, apodName, raster, jiggId))
    # -----------------------------------------------------------
    # Add pointing info:
    bat = calcBsmAngles(bsmt, bsmPos=bsmPos)
    spp = createSpirePointing(hpp=hpp, siam=siam, \
                            detAngOff=detAngOff, bat=bat)
    # -----------------------------------------------------------
    # Create interferogram:
    sdi = createIfgm(sdt, smect=smect, nhkt=nhkt, spp=spp, \
                     smecZpd=smecZpd,\
                     chanTimeOff=chanTimeOff,\
                     smecStepFactor=smecStepFactor)
    # Update the resolution if processing a H+L observation as LR
    if obs.meta["commandedResolution"].value == "H+LR" and processRes == "LR":
        sdi.commandedResolution = "LR"
    # adjust OPD ranges to ensure that they are the same for all scans
    sdi = makeSameOpds(sdi, opdLimits=opdLimits)
    # -----------------------------------------------------------
    # Baseline correction:
    sdi = baselineCorrection(sdi, type="fourier", threshold=4) 
    # -----------------------------------------------------------
    # 2nd level deglitching:
    sdi = deglitchIfgm(sdi, deglitchType="MAD")
    # -----------------------------------------------------------
    # Apodize, Phase-correct & Fourier transform interferograms.
    # The phase correction is calculated from an averaged LR interferogram:
    lowResSdi = averageInterferogram(sdi.copy())
    lowResSdi.commandedResolution = "LR"
    lowResSdi = makeSameOpds(lowResSdi, opdLimits=opdLimits)
    # Make a product to contain the phase corrected spectrum
    ssds = SpectrometerDetectorSpectrum()
    # Carry out Phase Correction (different order for different resolutions):
    res = sdi.commandedResolution
    # For High (& Calibration) resolution:
    if res == "HR" or res == "CR":
        sdi = phaseCorrection(sdi, avgSdi=lowResSdi, outsds=ssds)
        # Un-comment the following line to calculate the residual phase
        # (see Data Reduction Guide for explanation)
        dsds   = fourierTransform(sdi, ftType="prePhaseCorr", zeroPad="None")
        if apodize:
            sdi = apodizeIfgm(sdi, apodType="postPhaseCorr", apodName=apodName)
        ssds = fourierTransform(sdi, ftType="postPhaseCorr", zeroPad="standard")
    # For Medium and Low resolution:
    elif res == "MR" or res == "LR":
        if apodize:
            sdi = apodizeIfgm(sdi, apodType="prePhaseCorr", apodName=apodName)
        sdi  = phaseCorrection(sdi, avgSdi=lowResSdi, outsds=ssds)
        # Un-comment the following line to calculate the residual phase
        # (see Data Reduction Guide for explanation)
        # dsds   = fourierTransform(sdi, ftType="prePhaseCorr", zeroPad="None")
    level1.addProduct(sdi)
    simpleFitsWriter(sdi, "%s%i_sdi_%s_%s_%i_%i.fits"%(outDir, \
                      myObsid,outDataPool, apodName, raster, jiggId))
    # -----------------------------------------------------------
    # Get the RSRF calibration products
    biasMode  = ssds.meta["biasMode"].value
    startDate = ssds.startDate
    # In case a "calibration" observation is processed, the HR SCalSpecInstRsrf,
    # SCalSpecTeleRsrf and SCalSpecBeamParam products should be used:
    if res == "CR": 
        res = "HR"
    instRsrf = obs.calibration.spec.instRsrfList.getProduct( \
                         res, apodName, biasMode, startDate)
    # Read SCalSpecTeleRsrf
    try:
        myTeleRsrfJigg = "%s_Jiggle_%i_%s_%s_v7_0_0.fits"%(myTeleRsrf,jiggId,apodName,res)
        teleRsrf = fitsReader(myTeleRsrfJigg)
        print "Telescope RSRF from %s."%myTeleRsrfJigg
    except:
        teleRsrf = obs.calibration.spec.teleRsrfList.getProduct( \
                         jiggId, res, apodName, biasMode, startDate)
        print "Telescope RSRF from calibration context."
    # -----------------------------------------------------------
    # Remove out of band data
    ssds = removeOutOfBand(ssds, bandEdge=bandEdge)
    # -----------------------------------------------------------
    # Correction for instrument emission:
    ssds = instCorrection(ssds, nhkt=nhkt, instRsrf=instRsrf)
    # -----------------------------------------------------------
    # Get the flux conversion calibration products and apply to spectra
    ssds = specFluxConversion(ssds, teleRsrf=teleRsrf)
    # -----------------------------------------------------------
    # Correction for telescope emission:
    ssds = telescopeCorrection(ssds, hk=hk)
    # ----------------------------------------------------------- 
    # Append this scan to the list, taking account whether the resolution
    # was H+L or not
    if obs.meta["commandedResolution"].value == "H+LR":
        # for processing all scans as LR
        if processRes == "LR":
            sdsList.append(ssds)
            # Save individual FITS files
            simpleFitsWriter(ssds, "%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
                myObsid,outDataPool, ssds.commandedResolution, apodName, raster, jiggId))
        # for processing the HR scans
        elif processRes == ssds.commandedResolution:
            sdsList.append(ssds)
            # Save individual FITS files
            simpleFitsWriter(ssds, "%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
                myObsid, outDataPool,ssds.commandedResolution, apodName, raster, jiggId))
    # or, otherwise
    else:
        sdsList.append(ssds)
        # Save individual FITS files
        simpleFitsWriter(ssds, "%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
            myObsid, outDataPool,ssds.commandedResolution, apodName, raster, jiggId))
    level1.addProduct(ssds)
    # -----------------------------------------------------------
# ---------------------------------------------------------------
# save Level 1 in a new pool
obs.level1=level1
saveObs(obs.level1,outDataPool)                                       
ListMetaL2=["type","description","instrument","startDate","endDate","obsid","odNumber","cusMode","instMode","obsMode","origin", "pointingMode",\
               "aorLabel","aot","equinox","object","raDeSys","raNominal","decNominal","ra","dec","posAngle","telescope","observer","proposal",\
               "numRepetitions","mapSampling","commandedResolution"]
metaObs=obs.meta

# ---------------------------------------------------------------
# Carry out regridding
projtype="gridding"
for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    preCube = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing
    wcs = WcsCreator.createWcs(preCube.ra,preCube.dec,gridSpacing[array], gridSpacing[array],preCube.wave, \
                 preCube.raUnit,preCube.decUnit,preCube.waveUnit)
    # Regrid the data
    cube = griddingProjection(preCube, wcs=wcs)	
    for j in range(len(ListMetaL2)):
                    try:
                              cube.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              #print metaObs[ListMetaL2[j]]
                    except:
                              print "meta pas dans obs: ", ListMetaL2[j]
    # Save the cube
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
                     
projtype="nearestNeighbourProjection"
for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    spc = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    spcAsSpectrum2d = spc.toSpectrum2d()
    prod=Product()
    prod["spectrum"]=spcAsSpectrum2d
    # Calculate WCS for target grid
    #wcs = nearestNeighbourProjection.createWcs(spc.getRa(),spc.getDec(),spc.getWave())
    preCube=spc
    wcs = WcsCreator.createWcs(preCube.ra,preCube.dec,gridSpacing[array], gridSpacing[array],preCube.wave, \
                 preCube.raUnit,preCube.decUnit,preCube.waveUnit)
    # Create cube
    cube=nearestNeighbourProjection(spcAsSpectrum2d, target=wcs)
    for j in range(len(ListMetaL2)):
                    try:
                              cube.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              prod.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              #print metaObs[ListMetaL2[j]]
                    except:
                              print "meta pas dans obs: ", ListMetaL2[j]
    HistoryTask_forVersion=cube["History"]["HistoryTasks"]
    Version=HistoryTask_forVersion["BuildVersion"].getData()
    prod.meta["BuildVersion"]=StringParameter(str(Version[0]),"BuildVersion")
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
    simpleFitsWriter(prod, "%s%i_%s_%s_%s_spectrum2d.fits"%(outDir, myObsid,outDataPool, array, apodName))
                                    
projtype="naiveProjection"
for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    spc = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    flux = spc.getFlux()
    ra = spc.getRa()
    dec = spc.getDec()
    # Propagate meta data
    naiveProjection.setMetadata(spc.getMeta())
    # Get a world coordinate system for the data
    #wcs = naiveProjection.createWcs(ra,dec,spc.getWave())
    preCube=spc
    wcs = WcsCreator.createWcs(preCube.ra,preCube.dec,gridSpacing[array], gridSpacing[array],preCube.wave, \
                 preCube.raUnit,preCube.decUnit,preCube.waveUnit)
    # Create SLW spectral cube
    cube = naiveProjection(flux=flux,ra=ra,dec=dec,target=wcs)
    for j in range(len(ListMetaL2)):
                    try:
                              cube.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
                              #print metaObs[ListMetaL2[j]]
                    except:
                              print "meta pas dans obs: ", ListMetaL2[j]
    # Save the cube
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
                     
                     
                     
print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
