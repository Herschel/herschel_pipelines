# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2011 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            data using the latest SPIRE calibration products. The user 
#            can (C) produce either unapodized or apodized 
#            spectra. (D) An output directory is required to store 
#            results as fits files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (E) The
#            user must specify the size of the spatial pixels of the
#            resulting cubes. (F) For observations taken in High + Low 
#            Resolution mode, the user must select whether to process 
#            data in high or low resolution. (G) Select the path and
#            prefix of the fits files that contain jiggle-dependent 
#            telescope RSRFs - if available and desived.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated:  10/May/2011
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
hkpDir = "/data/glx-herschel/data1/herschel/FTS_HK/"
from herschel.ia.pal.util import StorageResult
from herschel.ia.toolbox.spectrum.projection import WcsCreator
from herschel.ia.toolbox.spectrum.projection import NearestNeighbourProjectionTask
from herschel.spire.ia.pipeline.spec.regrid import NaiveProjectionTask

execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/IAS_tools.py')
# (A) Specific OBSID and the name of the data storage in your Local Pool:
#myDataPool = "HD37041-SPIRE-FTS_orig"

#
# (B) N/A
#
# (C) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
apodize = 0
if apodize:
    apodName = "aNB_15"
else:
    apodName = "unapod"
#
# (D) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
#outDir = "/SPIRE/TMP/"
outDir =  "/data/glx-herschel/data1/herschel/FTS_SPIRE_Fits/"
#
# (E) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:
gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
#
# (F) For H+L observations only - changing this option has no effect for
#     observations that were not observed in "High+Low" mode: 
#     Choose whether to process LR or HR data (from a HR+LR observation)
processRes = "HR"
#
# (G) Specify the path of the jiggle-dependent telescope RSRF and the 
#     prefix for the fits file containing the observation ID of the 
#     dark sky observation - if available and desired:
myTeleRsrf = "/enter/path/SCalSpecTeleRsrf_0x50000000"
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
# map of the jiggle positions for full and intermediate sampling:
def getIntermediateJigEquiv(jiggle):
    if jiggle==0:
        return 6
    if jiggle==1:
        return 4
    if jiggle==2:
        return 12
    if jiggle==3:
        return 14
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################
# Define the thermistors, dark pixels and resistors:
thermistors = ["SLWT1", "SLWT2", "SSWT1", "SSWT2", \
               "SSWDP1", "SSWDP2", "SLWDP1", "SLWDP2"]
resistors   = ["SSWR1", "SLWR1"]

# Load in an observation context into HIPE:
print myDataPool
obs = getObservation(myObsid, poolName=myDataPool)

print "Processing observation %i (0x%X) from data pool %s."%(myObsid, myObsid, myDataPool)

# Calibration Context and Calibration Files 
# Read the latest calibration tree relevant to HCSS v6 from the local disc:
cal = spireCal(pool="spire_cal_7_0")
# TO CORRECT AN ERROR ON THE ABOVE LINE, run the following two lines 
# once only to load the calibration tree from the Archive (may take some time):
# (for more details, see the "Calibration" chapter in the SPIRE
# Data Reduction Guide)

#cal = spireCal(calTree="spire_cal_7_0")
#localStoreWriter(cal, "spire_cal_7_0")

# Attach the updated calibration tree to the observation context
obs.calibration.update(cal)

# Extract necessary Calibration Products from the Observation Context
nonLinCorr     = obs.calibration.spec.nonLinCorr
tempDriftCorr  = obs.calibration.spec.tempDriftCorr
lpfPar         = obs.calibration.spec.lpfPar
phaseCorrLim   = obs.calibration.spec.phaseCorrLim
chanTimeConst  = obs.calibration.spec.chanTimeConst
bsmPos         = obs.calibration.spec.bsmPos
detAngOff      = obs.calibration.spec.detAngOff
smecZpd        = obs.calibration.spec.smecZpd
chanTimeOff    = obs.calibration.spec.chanTimeOff
smecStepFactor = obs.calibration.spec.smecStepFactor
opdLimits      = obs.calibration.spec.opdLimits
bandEdge       = obs.calibration.spec.bandEdge

# Extract necessary Auxiliary Products from the Observation Context
hpp  = obs.auxiliary.pointing
siam = obs.auxiliary.siam
hk = obs.auxiliary.hk

# Start to process the observation from Level 0.5
# Process each SMEC scan building block (0xa106) individually, append to a list 
level1=Level1Context(myObsid)
sdsList=level1.sdsList
# ---------------------------------------------------------------
# Carry out regridding
ListMetaL2=["type","description","instrument","startDate","endDate","obsid","odNumber","cusMode","instMode","obsMode","origin", "pointingMode",\
               "aorLabel","aot","equinox","object","raDeSys","raNominal","decNominal","ra","dec","posAngle","telescope","observer","proposal",\
               "numRepetitions","mapSampling","commandedResolution"]

obsMeta=obs.meta
projtype="gridding"
for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    preCube = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing
    
    wcs = WcsCreator.createWcs(preCube.ra,preCube.dec,gridSpacing[array], gridSpacing[array],preCube.wave, \
                 preCube.raUnit,preCube.decUnit,preCube.waveUnit)
    # propagate meta
    griddingProjection.setMetadata(preCube.getMeta())
    # Regrid the data
    cube = griddingProjection(preCube, wcs=wcs)	
    # Save the cube
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube_test.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
                     
#projtype="nearestNeighbourProjection"
#for array in ['SSW','SLW']:
#    # Create a pre-processed cube (not regularly gridded)
#    spc = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
#    spcAsSpectrum2d = spc.toSpectrum2d()
#    prod=Product()
#    prod["spectrum"]=spcAsSpectrum2d
#    for j in range(len(ListMetaL2)):
#                    try:
#                              prod.meta[ListMetaL2[j]] = metaObs[ListMetaL2[j]]
#                    except:
#                              print "meta pas dans obs",ListMetaL2[j]
#                              #framesLevel1.meta[ListMetaL1[j]]=StringParameter("None",ListMetaL1[j])
#    simpleFitsWriter(prod, "%s%i_%s_%s_%s_spectrum2d_test.fits"%(outDir, myObsid,outDataPool, array, apodName))
#    # Calculate WCS for target grid
#    wcs = nearestNeighbourProjection.createWcs(spc.getRa(),spc.getDec(),spc.getWave())
#    # Create cube
#    cube=nearestNeighbourProjection(spcAsSpectrum2d, target=wcs)
#    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
#                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
#                                    
#projtype="naiveProjection"
#for array in ['SSW','SLW']:
#    # Create a pre-processed cube (not regularly gridded)
#    spc = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
#    flux = spc.getFlux()
#    ra = spc.getRa()
#    dec = spc.getDec()
#    # Propagate meta data
#    naiveProjection.setMetadata(spc.getMeta())
#    # Get a world coordinate system for the data
#    wcs = naiveProjection.createWcs(ra,dec,spc.getWave())
#    # Create SLW spectral cube
#    cube = naiveProjection(flux=flux,ra=ra,dec=dec,target=wcs)
#    # Save the cube
#    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
#                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))
                     
                     
                     
print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
