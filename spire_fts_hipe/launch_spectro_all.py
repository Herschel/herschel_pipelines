#clear(all=True)
import os, sys

#outDir="/data/glx-herschel/data1/herschel/HIPE_Fits/FTS_SPIRE/"
outDir = sys.argv[1]+"/"
object = sys.argv[2]
obsids_str = sys.argv[3:]
obsids = [int(obsid) for obsid in obsids_str]

print outDir, object, obsids

for obsid in obsids:
        myObsid    = obsid
        execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_v15.py')
