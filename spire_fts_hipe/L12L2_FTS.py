# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2010 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  A simplified version of the SPIRE mapping pipeline script. 
#            This script allows to reprocess (A) a specific observation 
#            data using the latest SPIRE calibration products. The user 
#            can (C) provide satellite housekeeping products in a local 
#            directory and (D) produce either unapodized or apodized 
#            spectra. (E) An output directory is required to store 
#            results as fits files containing the final spectral 
#            cubes and one spectrum file per jiggle position. (F) The
#            user must specify the size of the spatial pixels of the
#            resulting cubes. (G) For observations taken in High + Low 
#            Resolution mode, the user must select whether to process 
#            data in high or low resolution.
#
#  Usage:    The user needs to specify the options in the section
#            "User Selectable Options" at the beginning of the script.
#
#  Updated: 14/Oct/2010
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################
#
# (A) Specific OBSID and the name of the data storage in your Local Pool:
#myObsid    = 0x50006BF7L
#myDataPool = "HD37041-SPIRE-FTS_orig"
#
# (B) N/A
#
# (C) If the observation context does not contain a valid satellite
#     housekeeping product, then specify a local directory with such products:
hkpDir = ""
#
# (D) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
#apodize = 0
if apodize:
    apodName = "aNB_15"
else:
    apodName = "unapod"
#
#
# (E) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
from herschel.ia.pal.util import StorageResult
from herschel.ia.toolbox.spectrum.projection import WcsCreator
#from herschel.ia.toolbox.spectrum.projection import spireProjection 
from herschel.ia.toolbox.spectrum.projection import NearestNeighbourProjectionTask
from herschel.spire.ia.pipeline.spec.regrid import NaiveProjectionTask

# (F) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:	
outDir = "/data/glx-herschel/data1/herschel/FTS_SPIRE_Fits/"
gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
#
# (G) HR+LR observations Only: 
#     Define whether to process LR or HR data
processRes = "HR"

#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS_utility.py')
#
# (H) N/A
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################

# ---------------------------------------------------------------
# Carry out regridding
raster=0
sdsList=[]
for jiggId in range(0,16):
   print "%s%i_spectrum_%s_%s_%i_%i.fits"%(outDir, \
       myObsid,outDataPool, apodName, raster, jiggId)
   sdsList2=simpleFitsReader("%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
       myObsid,outDataPool, processRes,apodName, raster, jiggId))
   sdsList.append(sdsList2)

projtype="gridding"
for array in ['SSW','SLW']:
    # Create a pre-processed cube (not regularly gridded)
    preCube = spirePreprocessCube(sdsList=sdsList, arrayType=array, UNVIGNETTED=True)
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing

    wcs = WcsCreator.createWcs(preCube.ra,preCube.dec,gridSpacing[array], gridSpacing[array],preCube.wave, \
                 preCube.raUnit,preCube.decUnit,preCube.waveUnit)
    # propagate meta
    #griddingProjection.setMetadata(preCube.getMeta())
    # Regrid the data
    cube = griddingProjection(preCube, wcs=wcs)
    # Save the cube
    simpleFitsWriter(cube, "%s%i_%s_%s_%s_%s_%s_cube_test.fits"%(outDir, myObsid, \
                     outDataPool,preCube.meta["commandedResolution"].value, array, apodName,projtype))

print "Processing of observation %i (0x%X) complete"%(myObsid, myObsid)
#### End of the script ####
