execfile("/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py")
apodize=0
#
#version = "v8.0.3287"
##
###
#myObsid    = 0x50006BDCL
#myDataPool = "rho_oph_h2-SPIRE-FTS_orig"
#outDataPool = "rho_oph_h2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50006BDBL
#myDataPool = "rho_oph_fts_on-SPIRE-FTS_orig"
#outDataPool = "rho_oph_fts_on-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50006BDDL
#myDataPool = "rho_oph_fts_off-SPIRE-FTS_orig"
#outDataPool = "rho_oph_fts_off-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50006BDEL
#myDataPool = "rho_oph_fts_off_2-SPIRE-FTS_orig"
#outDataPool = "rho_oph_fts_off_2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50006BF9L
#myDataPool = "n2023_fts_1-SPIRE-FTS_orig"
#outDataPool = "n2023_fts_1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
###
#myObsid    = 0x50006BFAL
#myDataPool = "n2023_fts_2-SPIRE-FTS_orig"
#outDataPool = "n2023_fts_2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
##
#myObsid    = 0x5000548BL 
#myDataPool = "n7023_fts-SPIRE-FTS_orig"
#outDataPool = "n7023_fts-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
###
#myObsid    = 0x50005D74L 
#myDataPool = "n7023E_fts_1-SPIRE-FTS_orig"
#outDataPool = "n7023E_fts_1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50005D75L
#myDataPool = "n7023E_fts_2-SPIRE-FTS_orig"
#outDataPool = "n7023E_fts_2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
###
##
##
#myObsid    = 0x50006195L 
#myDataPool = "california-1-SPIRE-FTS_orig"
#outDataPool = "california-1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')
###
#myObsid    = 0x50006194L
#myDataPool = "california-2-SPIRE-FTS_orig"
#outDataPool = "california-2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')
###
###
#myObsid    = 0x50006196L
#myDataPool = "california-3-SPIRE-FTS_orig"
#outDataPool = "california-3-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')
###
#
#myObsid    = 0x5000548AL 
#myDataPool = "IC63-SPIRE-FTS_orig"
#outDataPool = "IC63-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_full.py')
#
#myObsid    = 0x50006BD5L 
#myDataPool = "l1721-1-SPIRE-FTS_orig"
#outDataPool = "l1721-1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
#
#myObsid    = 0x50006BD6L 
#myDataPool = "l1721-2-SPIRE-FTS_orig"
#outDataPool = "l1721-2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
#
#myObsid    = 0x50006BD7L 
#myDataPool = "l1721-3-SPIRE-FTS_orig"
#outDataPool = "l1721-3-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
##
#myObsid    = 0x50006BD8L 
#myDataPool = "l1721-4-SPIRE-FTS_orig"
#outDataPool = "l1721-4-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
#
#myObsid    = 0x50006BD9L 
#myDataPool = "l1721-5-SPIRE-FTS_orig"
#outDataPool = "l1721-5-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
#
#myObsid    = 0x50006BDAL 
#myDataPool = "l1721-6-SPIRE-FTS_orig"
#outDataPool = "l1721-6-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
#
#myObsid    = 0x50004EEFL 
#myDataPool = "Ced201-1-SPIRE-FTS_orig"
#outDataPool = "Ced201-1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')
#
#myObsid    = 0x50004EF0L	 
#myDataPool = "Ced201-2-SPIRE-FTS_orig"
#outDataPool = "Ced201-2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')

#myObsid    = 0x50004EF1L
#myDataPool = "Ced201-3-SPIRE-FTS_orig"
#outDataPool = "Ced201-3-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_intermediate.py')


#myObsid    = 0x50006BF7L
#myDataPool = "HD37041-SPIRE-FTS_orig"
#outDataPool = "HD37041-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
####
#myObsid    = 0x50006BF8L
#myDataPool = "HD37022(towards)-SPIRE-FTS_orig"
#outDataPool = "HD37022-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')

#myObsid    = 0x50003A2DL 
#myDataPool = "HD37041-SPIRE-FTS_orig"
#outDataPool = "HD37041-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_full.py')
#
#myObsid    = 0x50003A2EL 
#myDataPool = "OrionBar_map2-SPIRE-FTS_orig"
#outDataPool = "OrionBar_map2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_full.py')
#
#myObsid    = 0x50003A2FL 
#myDataPool = "OrionBar_map3-SPIRE-FTS_orig"
#outDataPool = "OrionBar_map3-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_full.py')
#
#myObsid    = 0x5000687AL 
#myDataPool = "IC59-SPIRE-FTS_orig"
#outDataPool = "IC59-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_full.py')
#myObsid    = 0x5000687BL
#myDataPool = "IC59-S1-SPIRE-FTS_orig"
#outDataPool = "IC59-S1-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_sparse.py')
#myObsid    = 0x5000687CL
#myDataPool = "IC59-S2-SPIRE-FTS_orig"
#outDataPool = "IC59-S2-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_sparse.py')
#myObsid    = 0x5000687DL
#myDataPool = "IC59-S3-SPIRE-FTS_orig"
#outDataPool = "IC59-S3-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_sparse.py')
#myObsid    = 0x5000687EL
#myDataPool = "IC59-S4-SPIRE-FTS_orig"
#outDataPool = "IC59-S4-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_sparse.py')
#myObsid    = 0x5000687FL
#myDataPool = "IC59-S5-SPIRE-FTS_orig"
#outDataPool = "IC59-S5-SPIRE-FTS_v8.0.3287"
#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_sparse.py')
##
myObsid    = 0x50009AC1L
myDataPool = "los_26.46+0.09_SPIRE-FTS_orig"
outDataPool = "los_26.46+0.09_SPIRE-FTS_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')

myObsid    = 0x50009ACBL 
myDataPool = "los_26.46-0.3_SPIRE-FTS_orig"
outDataPool = "los_26.46-0.3_SPIRE-FTS_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')

myObsid    = 0x50009ABFL 
myDataPool = "los_28.6+0.83_SPIRE-FTS_orig"
outDataPool = "los_28.6+0.83_SPIRE-FTS_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')

myObsid    = 0x50009ABEL
myDataPool = "los_30+3_SPIRE-FTS_orig"
outDataPool = "los_30+3_SPIRE-FTS_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_Gridding_generic.py')
