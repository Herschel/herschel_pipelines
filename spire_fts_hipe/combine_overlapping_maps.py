# fold several overlapping jiggled observations of NGC6334 I & I(N) into 
# combined output cubes

obsSbri = getObservation(1342251326, poolName='ngc6334i_bright_1342251326_hipe10')
obsSnom = getObservation(1342214827, poolName='ngc6334i_nom_1342214827_hipe10')
obsN = getObservation(1342214841, poolName='ngc6334iN_1342214841_hipe10')


# define velocity correction function (since applyRadialVelocity task will only be available from HPIE v12 on)
def velocityCorrection(sdsin):
    sds = sdsin.copy()
    # Radial velocity of satellite in m/s
    radialVelocity = 1000*sds.meta['radialVelocity'].value
    # Define the speed of light and Pi
    from herschel.share.unit import Constant
    c  = Constant.SPEED_OF_LIGHT.value
    pi = Math.PI
    # Check the units of the input spectrum to make sure wavescale is in frequency 
    # !! skip the unit check since sds[0]['SSWD4'] doesn't always exist (risk trouble later...)
#    sdsUnit = sds[0]['SSWD4']['wave'].unit
#    if sdsUnit.name == "GHz":
    for scan in sds.scanNames:
        for detector in sds[scan].channelNames:
            # Correct the data to the LSR frame using the satellite radial velocity 
            sds[scan][detector].wave = sds[scan][detector].wave * (1.0-radialVelocity/c)
    print "Corrected the frequency scale to the LSR using %s km/s velocity correction"%(radialVelocity/1e3)
    return sds



# create an empty spectrometer detector spectrum list:
sdsList = SpireMapContext()

# indicate which detectors in which jiggles in which obsid's are bad:
#  (determined from visual inspection of spectra in var allsds, after initial run 
#    of loop below)
badDet = {}  # key=detectorname, value=(index_obs,index_jiggle)
   # index_obs = (0,1,2) --> obsSbri, obsSnom, obsN
   # index_jiggle = (0,1,2,...,15)
badScans = {}  # key=detectorname, value=(index_obs,index_jiggle,[scan_numbers])

badDet['SSWB3'] = [(2,3), ]
badDet['SSWC3'] = [(1,1), (1,4), (1,5), (1,7), (2,8), (2,9), (2,10), (2,12), (2,13), (2,14), (2,15),    ]
badDet['SSWC4'] = [(2,4), ]
badDet['SSWD3'] = [(1,0), (1,1), (1,3), (2,4), (2,8), (2,12), (2,14), (2,15),   ]
badDet['SSWD4'] = [(1,2), (1,5), (2,11), (2,12), (2,13), (2,14),    ]
badDet['SSWD6'] = [(2,14), (2,15),  ]
badDet['SSWE2'] = [(1,3), (2,5),  ]
badDet['SSWE4'] = [(1,3), (2,1), (2,3), (2,4), (2,9), (2,10), (2,11), (2,13),    ]
badDet['SSWE5'] = [(2,15), ]
badDet['SSWE6'] = [(2,15), ]
badDet['SSWF2'] = [(1,3), (2,12), ]
badDet['SSWG1'] = [(1,1), (1,4),  ]
badDet['SSWG2'] = [(1,15), ]
badDet['SSWG3'] = [(1,0), ]

badDet['SLWA2'] = [(2,15),  ]
badDet['SLWA3'] = [(2,15),  ]
badDet['SLWB2'] = [(0,15), (1,7), (1,9), (1,12), (1,13), (2,7), (2,8), ] 
badDet['SLWB3'] = [(1,4), ]
badDet['SLWB4'] = [(2,0), (2,6), (2,7), (2,15), ]
badDet['SLWC2'] = [(1,2), (1,3),] 
badDet['SLWC3'] = [(0,9), (1,1), (1,3), (1,9), (1,12), (2,12), ] 
badDet['SLWD3'] = [(2,0), (2,1), ]

badScans['SSWD4'] = [ (1, 13, [3,5]),   ]
badScans['SSWC5'] = [ (1, 4, [3,5]),  ]

badScans['SLWB3'] = [ (2, 3, [4,6]),   ]   
badScans['SLWC2'] = [ (2, 12, [3,5]),  ]
badScans['SLWC3'] = [ (2, 4, [2,4,6,8]),  (2,13, [1,3,5,7]),   (2,14, [2,4,6,8]),    ]
badScans['SLWC4'] = [ (2, 13, [1]),  ]

 
# loop over the various observations and all 16 jiggle positions to create 
#   a single precube 
allsds = []
obsContextList = [obsSbri,obsSnom,obsN]
for obs_index in range(len(obsContextList)):
    obs = obsContextList[obs_index]
    allsds.append([])
    for jiggId in range(16):
        detToRemove = []
        sds = obs.level1.getProduct("Point_0_Jiggle_%i_HR"%jiggId).getProduct("unapodized_spectrum")        
        # Correct the frequency scale to be in the Local Standard of Rest
        # !! replace with applyRadialVelocity() task from HIPE v12 on
        sds = velocityCorrection(sds)

        allsds[-1].append(sds)  # save each sds for each obsid/jiggleID to inspect later
        # look up which detector spectra were marked as bad: 
        for detectorName in badDet.keys():
            badSpec_indexlist = badDet[detectorName]
            for badSpec_indices in badSpec_indexlist:
                if (badSpec_indices[0] == obs_index) and (badSpec_indices[1] == jiggId):
                    detToRemove.append(detectorName)
        # remove individual rows (obsid/jiggle/detector) with bad spectra:
        if len(detToRemove) > 0: 
            print 'removing detectors from obs %i, jiggle %i:' % (obs_index, jiggId), detToRemove
            sds = filterChannels(sds, removeChannels=detToRemove)
        # look up which individual _scans_ were marked as bad, and remove them: 
        for detectorName in badScans.keys():
            badScan_indexlist = badScans[detectorName]
            for badScan_indices in badScan_indexlist:
                if (badScan_indices[0] == obs_index) and (badScan_indices[1] == jiggId):
                    for badScanNum in badScan_indices[2]:
                        sds[badScanNum].remove(detectorName)
                        print 'removing scan %i from detector %s in obs %i, jiggle %i' % \
                               (badScanNum,detectorName, obs_index, jiggId)
        # add cleaned up sds to sdsList to be used for preCube:
        sdsList.setProduct("%d"%(sdsList.refs.size()), sds)



preCubeSSW = spirePreprocessCube(context=sdsList, arrayType="SSW", unvignetted=True)
preCubeSLW = spirePreprocessCube(context=sdsList, arrayType="SLW", unvignetted=True)

# create a WCS (gridspacing in deg, default 9.5" for SSW and 17.5" for SLW
wcsS = SpireWcsCreator.createWcs(preCubeSSW, 9.5/3600, 9.5/3600)
wcsL = SpireWcsCreator.createWcs(preCubeSLW, 17.5/3600, 17.5/3600)


# create regularly gridded cubes:
cubeNGC6334_SSW_all3_naive = spireProjection(spc=preCubeSSW, wcs=wcsS, projectionType="naive")
cubeNGC6334_SLW_all3_naive = spireProjection(spc=preCubeSLW, wcs=wcsL, projectionType="naive")

cubeNGC6334_SSW_all3_gridding = spireProjection(spc=preCubeSSW, wcs=wcsS, projectionType="gridding")
cubeNGC6334_SLW_all3_gridding = spireProjection(spc=preCubeSLW, wcs=wcsL, projectionType="gridding")

# create SSW cube with biger pixels to match SLW WCS (custom coordinates only work in HIPE11 and later):
desiredRA = 260.233708333
desiredDec = -35.7520277778  # (AOR coordinates of I(N) observation)
wcsS_samegrid = SpireWcsCreator.createWcs(preCubeSSW, desiredRA, desiredDec, 17.5/3600, 17.5/3600)
wcsL_samegrid = SpireWcsCreator.createWcs(preCubeSLW, desiredRA, desiredDec, 17.5/3600, 17.5/3600)
cubeNGC6334_SLW_all3_naive_samegrid    = spireProjection(spc=preCubeSLW,wcs=wcsL_samegrid, projectionType="naive")
cubeNGC6334_SSW_all3_naive_samegrid    = spireProjection(spc=preCubeSSW,wcs=wcsS_samegrid, projectionType="naive")
cubeNGC6334_SLW_all3_gridding_samegrid = spireProjection(spc=preCubeSLW,wcs=wcsL_samegrid, projectionType="gridding")
cubeNGC6334_SSW_all3_gridding_samegrid = spireProjection(spc=preCubeSSW,wcs=wcsS_samegrid, projectionType="gridding")


# save combined cube products to FITS files: 
savedir = '/Users/MHD/data/SPIRE_NGC6334/combined_I_IN'

simpleFitsWriter(product=cubeNGC6334_SLW_all3_naive, file='%s/SLW_all3_clean_naive.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_naive, file='%s/SSW_all3_clean_naive.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SLW_all3_gridding, file='%s/SLW_all3_clean_gridding.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_gridding, file='%s/SSW_all3_clean_gridding.fits'%(savedir))

simpleFitsWriter(product=cubeNGC6334_SLW_all3_gridding_samegrid, file='%s/SLW_all3_clean_gridding_samegrid.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_gridding_samegrid, file='%s/SSW_all3_clean_gridding_samegrid.fits'%(savedir))


