execfile("/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py")
apodize=0
#
#version = "v8.0.3287"
#
##
myObsid    = 0x5000832bL 
myDataPool = "ngc4038overlap_SPIRE-FTS_orig"
outDataPool = "ngc4038overlap_SPIRE-FTS_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_v8.0.1.py')
##
apodize=1
myObsid    = 0x5000832bL 
myDataPool = "ngc4038overlap_SPIRE-FTS_orig"
outDataPool = "ngc4038overlap_SPIRE-FTS_apod1_v8.0.3287"
execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/Spectrometer_Mapping_Pipeline_v8.0.1.py')
