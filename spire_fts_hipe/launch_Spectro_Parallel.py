#!/usr/bin/env python
# -*-coding:utf-8 -*

from multiprocessing import Pool, cpu_count
import os, sys
import getopt

script = "/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS/launch_spectro_all.py"

def usage():
	"""
	print usage function
	"""
	print ""
	print "Usage:"
	print "python launch_Spectro_parallel.py object_obsids.txt nbCore outPutDir"
	print ""
	print "This program process object/obsids data from a text file with supreme. Fits file are saved in outputDir."
	print "" 
	print "    object_obsids.txt      file that contain the object to process. One 'object obsid1 obsid2' per line"
	print "    nbCore                 number of core to use on machine"
	print ""

####################################################################################################################
####################################################################################################################

def launchScriptHIPE(parameters):
	logParam = "_".join(parameters.split(" ")[1:])
	log = script.replace("FTS","FTS/log").replace(".py","_"+logParam+".txt")
	command = " ".join(["/data/glx-herschel/data1/herschel/bin/hipe",script,parameters,">>",log])
	print command
	os.system(command)

def main(argv=None):

	if argv is None:
		argv = sys.argv

	try:
		opts, args = getopt.getopt(argv[1:],[])
	except getopt.GetoptError, err:
		print str(err)
		usage()
		return 2

	try:
		print ""
		print "Number of Cores on machine:", cpu_count()
	except:
		pass

	try:
		print ""
		print "Load Average:"
		os.system("cat /proc/loadavg")
	except:
		pass
	
	try:
		object_obsids = sys.argv[1]
		nberProcess = sys.argv[2]
		outputDir = sys.argv[3]
	except:
		print "Error - Check your arguments !"
		sys.exit(usage())
	
	parameters = [outputDir+" "+line.rstrip('\n') for line in open(object_obsids)]
	nberProcess = min(int(nberProcess),len(object_obsids))
	print "Number of cores to use:",nberProcess
	
	os.system("mkdir -p log")
	os.system("mkdir -p "+outputDir)
	
	p = Pool(processes=nberProcess)
	p.map(launchScriptHIPE,parameters)

if __name__ == "__main__":
	sys.exit(main())

