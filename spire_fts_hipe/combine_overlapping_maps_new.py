# fold several overlapping jiggled observations of NGC6334 I & I(N) into 
# combined output cubes, after deleting bad spectra from individual precubes

#obsSbri = getObservation(1342251326, poolName='ngc6334i_bright_1342251326_hipe12')
#obsSnom = getObservation(1342214827, poolName='ngc6334i_nom_1342214827_hipe12')
#obsN    = getObservation(1342214841, poolName='ngc6334iN_1342214841_hipe12')
obsSbri = getObservation(1342251326, useHsa=True)
obsSnom = getObservation(1342214827, useHsa=True)
obsN    = getObservation(1342214841, useHsa=True)

# create precubes for each observation:
print 'creating precubes from level1 contexts of individual observations..'
preCubeSLW_Sbri = spirePreprocessCube(context=obsSbri.level1, arrayType="SLW", unvignetted=False, apodName="unapod", resolution="HR")
preCubeSLW_Snom = spirePreprocessCube(context=obsSnom.level1, arrayType="SLW", unvignetted=False, apodName="unapod", resolution="HR")
preCubeSLW_N    = spirePreprocessCube(context=obsN.level1,    arrayType="SLW", unvignetted=False, apodName="unapod", resolution="HR")
preCubeSSW_Sbri = spirePreprocessCube(context=obsSbri.level1, arrayType="SSW", unvignetted=False, apodName="unapod", resolution="HR")
preCubeSSW_Snom = spirePreprocessCube(context=obsSnom.level1, arrayType="SSW", unvignetted=False, apodName="unapod", resolution="HR")
preCubeSSW_N    = spirePreprocessCube(context=obsN.level1,    arrayType="SSW", unvignetted=False, apodName="unapod", resolution="HR")

print preCubeSSW_N 
print '\t total number of initial spectra in SLW preCubes:', preCubeSLW_Sbri["spectrum2d"].rowCount + preCubeSLW_Snom["spectrum2d"].rowCount + preCubeSLW_N["spectrum2d"].rowCount
print '\t total number of initial spectra in SSW preCubes:', preCubeSSW_Sbri["spectrum2d"].rowCount + preCubeSSW_Snom["spectrum2d"].rowCount + preCubeSSW_N["spectrum2d"].rowCount


# indicate which detectors in which jiggles in which obsid's are bad:
#  (determined from visual inspection of spectra in preCubes defined above)
badDet = {}  # key=detectorname, value=(index_obs,index_jiggle)
   # index_obs = (0,1,2) --> obsSbri, obsSnom, obsN
   # index_jiggle = (0,1,2,...,15); index_jiggle = -1 indicates that all jiggles are bad for that detector/obs

# searched through preCubes to flag bad spectra:
# SLW Sbri Snon, N: completely
# SSW Sbri Snon (SSW N yet to do)

badDet['SLWA3'] = [(2,8), (2,9), (2,15), ]
badDet['SLWB2'] = [(1,7), (1,9), (1,13), (2,6), (2,7), (2,8), (2,9),   ]  # put (1,7) back in if it leads to holes in gridded cube
badDet['SLWB4'] = [(2,0), (2,8), (2,14), (2,15),  ]
badDet['SLWC2'] = [(0,15), ]
badDet['SLWC3'] = [(1,12), (2,11), (2,12), (2,15),  ]
badDet['SLWD3'] = [(2,0), ]
badDet['SLWE2'] = [(2,0), (2,1), (2,2), (2,5), (2,6), ]

badDet['SSWA3'] = [(0,-1), (1,-1), (2,-1)  ]
badDet['SSWB2'] = [(0,3), ]
badDet['SSWB3'] = [(1,3), (1,4), ]
badDet['SSWB5'] = [(0,15), ]
badDet['SSWC2'] = [(0,2), (0,4), (2,0), (2,1), (2,2), (2,3),  ]
badDet['SSWC3'] = [(1,1), (1,4), (2,9), (2,13), (2,14),   ]
badDet['SSWC4'] = [(2,4), ]
badDet['SSWD1'] = [(0,12), (0,13), (0,14), (0,15), (1,12), (1,13), (1,14), (1,15), (2,12), (2,13), (2,14), (2,15),  ]
badDet['SSWD3'] = [(1,3), (2,15), ]
badDet['SSWD4'] = [(1,2), (2,12),  ]
badDet['SSWD6'] = [(1,10), (1,11), (1,12), (1,13), ]
badDet['SSWE2'] = [(1,-1), (2,5),  ]
badDet['SSWE6'] = [(2,15), ]
badDet['SSWF2'] = [(1,-1), (2,-1), ]
badDet['SSWG2'] = [(1,15), ]
badDet['SSWG3'] = [(1,0), ]

# maybe list, e.g. wavy baseline but ok'ish line signal
# SLWA3: (2,12), 
# SLWB2: (0,15), (0,14), (1,10), (2,9), 
# SLWB3: (0,7), (0,6), (0,5), (0,12)
# SLWB4: (2,6), (2,7), 
# SLWC3: (0,7), (0,9), (0,6), (1,2), (1,1), (2,0), (2,1), (2,7), 
# SLWD2: (0,0), 
# SLWD3: (0,1), (0,2), (2,1), (2,6), (2,7), 
# SLWD4: (2,3), 
# SSWA4: (0,14), (0,15), 
# SSWD4: (0,10), 
# SSWC4: (1,5), (1,6), (1,7), (1,8), (1,9), (1,10), (1,11), (1,12), (1,13), (1,14), (1,15), 
# SSWD7: (1,0), (1,1), (1,2), (1,3), (2,1), (2,2), (2,3), 
# SSWE4: (1,6), (1,9),  
# SSWE5: (2,15), 
# SSWG1: (1,0), (1,1), (1,4), (1,5), (1,6), (1,10), 
# SSWA4: (2,almostall)

# throw out bad spectra marked above
print 'removing bad spectra..'
preCubeList = [preCubeSLW_Sbri,preCubeSLW_Snom,preCubeSLW_N,\
               preCubeSSW_Sbri,preCubeSSW_Snom,preCubeSSW_N]
for detName in sorted(badDet.keys()):
    for obs_jiggle_pair in badDet[detName]:
        obsindex = obs_jiggle_pair[0]
        jiggleid = obs_jiggle_pair[1]
        if jiggleid == -1: 
            print '\t * removing all spectra from detector %s for obs %i' % (detName,obsindex)
            preCubeList[obsindex].deleteSpectra(detName)    # SLW
            preCubeList[obsindex+3].deleteSpectra(detName)  # SSW
        else:
            print '\t * removing detector %s from obs %i, jiggle %i' % (detName, obsindex, jiggleid)
            preCubeList[obsindex].deleteSpectra(detName, jiggleid, 0)    # SLW 
            preCubeList[obsindex+3].deleteSpectra(detName, jiggleid, 0)  # SSW
    

# make a combined preCube containing all spectra from the three individual observations:
preCubeSLW = preCubeSLW_Sbri.append(preCubeSLW_Snom).append(preCubeSLW_N)
preCubeSSW = preCubeSSW_Sbri.append(preCubeSSW_Snom).append(preCubeSSW_N)
print '\t total number of spectra in combined SLW preCube after filtering:', preCubeSLW["spectrum2d"].rowCount 
print '\t total number of spectra in combined SSW preCube after filtering:', preCubeSSW["spectrum2d"].rowCount 



# create a WCS (gridspacing in deg, default 9.5" for SSW and 17.5" for SLW
wcsS = SpecWcsCreator.createWcs(preCubeSSW, 9.5/3600, 9.5/3600)
wcsL = SpecWcsCreator.createWcs(preCubeSLW, 17.5/3600, 17.5/3600)


# create regularly gridded cubes:
cubeNGC6334_SSW_all3_naive,convolutionTable = spireProjection(spc=preCubeSSW, wcs=wcsS, projectionType="naive")
cubeNGC6334_SLW_all3_naive,convolutionTable = spireProjection(spc=preCubeSLW, wcs=wcsL, projectionType="naive")

cubeNGC6334_SSW_all3_gridding,convolutionTable = spireProjection(spc=preCubeSSW, wcs=wcsS, projectionType="gridding")
cubeNGC6334_SLW_all3_gridding,convolutionTable = spireProjection(spc=preCubeSLW, wcs=wcsL, projectionType="gridding")

# create SSW cube with biger pixels to match SLW WCS (custom coordinates only work in HIPE11 and later):
desiredRA = 260.233708333
desiredDec = -35.7520277778  # (AOR coordinates of I(N) observation)
wcsS_samegrid = SpecWcsCreator.createWcs(preCubeSSW, desiredRA, desiredDec, 17.5/3600, 17.5/3600)
wcsL_samegrid = SpecWcsCreator.createWcs(preCubeSLW, desiredRA, desiredDec, 17.5/3600, 17.5/3600)
cubeNGC6334_SLW_all3_naive_samegrid,convolutionTable = spireProjection(spc=preCubeSLW,wcs=wcsL_samegrid, projectionType="naive")
cubeNGC6334_SSW_all3_naive_samegrid,convolutionTable = spireProjection(spc=preCubeSSW,wcs=wcsS_samegrid, projectionType="naive")
cubeNGC6334_SLW_all3_gridding_samegrid,convolutionTable = spireProjection(spc=preCubeSLW,wcs=wcsL_samegrid, projectionType="gridding")
cubeNGC6334_SSW_all3_gridding_samegrid,convolutionTable = spireProjection(spc=preCubeSSW,wcs=wcsS_samegrid, projectionType="gridding")


# save combined cube products to FITS files: 
savedir = '/home/kdassas'

simpleFitsWriter(product=preCubeSLW_Sbri, file='%s/PreCUbeSLW_Sbri.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SLW_all3_naive, file='%s/SLW_all3_clean_naive.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_naive, file='%s/SSW_all3_clean_naive.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SLW_all3_gridding, file='%s/SLW_all3_clean_gridding.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_gridding, file='%s/SSW_all3_clean_gridding.fits'%(savedir))

simpleFitsWriter(product=cubeNGC6334_SLW_all3_gridding_samegrid, file='%s/SLW_all3_clean_gridding_samegrid.fits'%(savedir))
simpleFitsWriter(product=cubeNGC6334_SSW_all3_gridding_samegrid, file='%s/SSW_all3_clean_gridding_samegrid.fits'%(savedir))
