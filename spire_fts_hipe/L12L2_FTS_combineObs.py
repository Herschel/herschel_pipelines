# 
#  This file is part of Herschel Common Science System (HCSS).
#  Copyright 2001-2010 Herschel Science Ground Segment Consortium
# 
#  HCSS is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
# 
#  HCSS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
# 
#  You should have received a copy of the GNU Lesser General
#  Public License along with HCSS.
#  If not, see <http://www.gnu.org/licenses/>.
# 
###########################################################################
###          SPIRE Spectrometer Mapping User Reprocessing Script        ###
###########################################################################
#  Purpose:  Start from Fits (Spectrums) from multiple obsids to create a 
# combined output cubes

#
#  Updated: Oct/2013
#
###########################################################################

###########################################################################
###                     User Selectable Options                         ###
###########################################################################

#
# (D) The final spectrum will be unapodized (apodize = 0) or 
#     apodized (apodize = 1):
apodize = 1
if apodize:
    apodName = "aNB_15"
else:
    apodName = "unapod"
#
#
# (E) Specify the output directory for writing the resulting spectra and 
#     cubes into FITS files:
from herschel.ia.pal.util import StorageResult
from herschel.ia.toolbox.spectrum.projection import WcsCreator
#from herschel.ia.toolbox.spectrum.projection import spireProjection 
from herschel.ia.toolbox.spectrum.projection import NearestNeighbourProjectionTask
from herschel.spire.ia.pipeline.spec.regrid import NaiveProjectionTask

# (F) Specify the map pixel size for the final data cubes (for SSW and SLW)
#     in units of degree:	
outDir = "/data/glx-herschel/data1/herschel/HIPE_Fits/FTS_SPIRE/"
# old gridSpacing = {"SSW": 9.5 / 3600.0, "SLW": 17.5 / 3600.0}
sswFwhm = 19.0 / 3600.0
slwFwhm = 35.0 / 3600.0
gridSpacing={"full":        {"SSW": 0.5 * sswFwhm, "SLW": 0.5 * slwFwhm}, \
             "intermediate":{"SSW": 1.0 * sswFwhm, "SLW": 1.0 * slwFwhm}, \
             "sparse":      {"SSW": 2.0 * sswFwhm, "SLW": 2.0 * slwFwhm}}
#
# (G) HR+LR observations Only: 
#     Define whether to process LR or HR data
processRes = "HR"

#execfile('/data/glx-herschel/data1/herschel/scriptsSPIRE/FTS_utility.py')
#
# (H) N/A
#
###########################################################################

###########################################################################
###                     User-Defined Jython Routines                    ###
###########################################################################
###########################################################################

###########################################################################
###                                 MAIN                                ###
###########################################################################

# ---------------------------------------------------------------
# Orion Bar Obsids
#obsids=[1342204919,1342204920,1342214846,1342192173,1342228734,1342242591,1342249465,1342192174,1342192175]
obsids=[1342204919,1342204920,1342192173,1342192174]
obsids=[1342192173,1342192174,1342192175, 1342214846,1342242591]
Version="12.0.2603"
object_name={1342204919:"HD37041",1342204920:"HD37022(towards)",1342214846:"HD37041",1342228734:"HD37041",1342192174:"OrionBar_map2",1342192175:"OrionBar_map3",1342192173:"HD37041",1342242591:"HD37041",1342249465:"HD37041"}

raster=0
sdsList = SpireMapContext()
myObsid=""
for obsid in obsids:
    myObsid=str(obsid)+"_"+myObsid
    outDataPool=object_name[obsid]+"_SPIRE-FTS_"+Version
    for jiggId in range(0,16):
       print "%s%i_spectrum_%s_%s_%i_%i.fits"%(outDir, \
           obsid,outDataPool, apodName, raster, jiggId)
       sdsList2=simpleFitsReader("%s%i_spectrum_%s_%s_%s_%i_%i.fits"%(outDir, \
           obsid,outDataPool, processRes,apodName, raster, jiggId))
       sdsList.setProduct("%d"%(sdsList.refs.size()), sdsList2) 

outDataPool="OrionAll_SPIRE-FTS_"+Version
#print sdsList

mapSampling="full"
# Carry out regridding
for array in ["SSW", "SLW"]:
    # Create a pre-processed cube (not regularly gridded):
    preCube = spirePreprocessCube(context=sdsList, arrayType=array, unvignetted=True)
    preCubeAsSpectrum2d = preCube.toSpectrum2d()
    prod=Product()
    prod["spectrum"]=preCubeAsSpectrum2d
    # Set up the grid - covering the RA and Dec of observed points using specified gridSpacing:
    wcs = SpecWcsCreator.createWcs(preCube, gridSpacing[mapSampling][array], gridSpacing[mapSampling][array])
    for projtype in ["naive","nearest","gridding"]:
    	# Regrid the data using the Naive Projection algorithm:
    	if array == "SSW": 
    	    cube = cubeSSW = spireProjection(spc=preCube, wcs=wcs, projectionType=projtype)
    	elif array == "SLW": 
    	    cube = cubeSLW = spireProjection(spc=preCube, wcs=wcs, projectionType=projtype)
    	# Sort the metadata into a logical order
    	metaDataSorter(cube)
        HistoryTask_forVersion=cube["History"]["HistoryTasks"]
        cube.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
        cube.meta["wavelength"]=StringParameter(str(array),"wavelength")
        # Save the cube to FITS:
        simpleFitsWriter(cube, "%s%s_%s_%s_%s_%s_%s_cube.fits"%(outDir, myObsid, \
                     outDataPool,cube.meta["processResolution"].value, array, apodName,projtype))
    # Save the spectrum2d:
    prod.meta["BuildVersion"]=StringParameter(str(Version),"BuildVersion")
    prod.meta["wavelength"]=StringParameter(str(array),"wavelength")
    simpleFitsWriter(prod, "%s%s_%s_%s_%s_spectrum2d.fits"%(outDir, myObsid,outDataPool, array, apodName))
    # Save the processed products back into the Observation Context:
    #if obs.level2 and not apodize:
    #    obs.level2.setProduct("%s_%s_unapodized_spectrum"%(res, array), cube)
    #elif obs.level2 and apodize:
    #    obs.level2.setProduct("%s_%s_apodized_spectrum"%(res, array), cube)

print "Processing of observations %s complete"%(myObsid)
#### End of the script ####
