import os, re, sys, getopt
from IAS_tools import *


####### Add missing metadata to unimap maps primary header
####### Add a obsids hdu extension with the list of combined obsids using OBSID00x from the primary header
####### read data from basepath, fixe them and copy them in outputdir
####### DicoObsids.update whith object and obisds needs to be added for each survey.

DicoObsids = {}

#SAG-4

DicoObsids.update( {'AFGL4029shift':[ 1342217393,1342217394]} )
DicoObsids.update( {'Ced201-2':[1342196809,1342196810]} )
DicoObsids.update( {'DC300-17-c2':[1342193044,1342193045]} )
DicoObsids.update( {'DC300-17':[1342212300,1342211293]} )
DicoObsids.update( {'HD37041':[1342191106,1342191107]} )
DicoObsids.update( {'HH_intermediate':[1342226731,1342226732]} )
DicoObsids.update( {'IC59-centre':[1342216405,1342216406]} )
DicoObsids.update( {'IC63-int':[1342216407,1342216408]} )
DicoObsids.update( {'IRAS16132-503':[1342216487,1342216488]} )
DicoObsids.update( {'IVCG86_PacsPhoto':[1342210589,1342210590]} )
DicoObsids.update( {'IVCG86':[1342197280,1342197281]} )
DicoObsids.update( {'n2023':[1342204219,1342204220]} )
DicoObsids.update( {'n7023_int':[1342187077,1342187078]} )
DicoObsids.update( {'rcw79':[1342258816,1342258817]} )
DicoObsids.update( {'rho_oph_h2':[1342204191,1342204192]} )
DicoObsids.update( {'sh241MSX':[1342218723,1342218724]} )
DicoObsids.update( {'spica_PacsPhoto':[1342213278,1342213279]} )
DicoObsids.update( {'spica':[1342200114,1342200115]} )
DicoObsids.update( {'ursamajors_s1':[1342194078,1342194079]} )
DicoObsids.update( {'ursamajors_s2':[1342194076,1342194077]} )
DicoObsids.update( {'UrsaMajor':[1342206692,1342206693]} )

DicoObsids.update( {'HATLAS_NGP_NANB':[1342259329, 1342259328, 1342259325,1342259324,1342259323,1342259322,1342259321,1342259320]} )
DicoObsids.update( {'S3':[1342204860,1342204861]} )
DicoObsids.update( {'MACSJ1423':[1342188215,1342188216]} )

#SAG-3

DicoObsids.update({"OrionA-C-1":[1342204098,1342204099]})
DicoObsids.update({"OrionA-N-1":[1342218967,1342218968]})
DicoObsids.update({"OrionA-S-1":[1342205076,1342205077]})

DicoObsids.update({"OrionA-C-1__OrionA-S-1":[1342204098,1342204099,1342205076,1342205077]})

DicoObsids.update({"OrionA-C-2":[1342218553, 1342218554]})
DicoObsids.update({"OrionA-N-2":[1342206052, 1342206053]})
DicoObsids.update({"OrionA-S-2":[1342228910, 1342228911]})
DicoObsids.update({"OrionA-SS-2":[1342228908, 1342228909]})

DicoObsids.update({"OrionA-C-2__OrionA-S-2__OrionA-SS-2":[1342218553, 1342218554,1342228910, 1342228911,1342228908, 1342228909]})


DicoObsids.update({"OrionB-N-1":[1342215982,1342215983]})
DicoObsids.update({"OrionB-NN-1":[1342205074,1342205075]})
DicoObsids.update({"OrionB-S-1":[1342215984,1342215985]})

DicoObsids.update({"OrionB-N-1__OrionB-NN-1":[1342215982,1342215983,1342205074,1342205075]})

DicoObsids.update({"OrionB-C-2":[1342218555, 1342218556]})
DicoObsids.update({"OrionB-N-2":[1342206054, 1342206055]})
DicoObsids.update({"OrionB-NN-2":[1342206078, 1342206079]})
DicoObsids.update({"OrionB-S-2":[1342206080, 1342206081]})

DicoObsids.update({"OrionB-C-2__OrionB-N-2":[1342218555, 1342218556,1342206054, 1342206055]})


def fixFiles(fitsfiles,DicoObs,outPutDir,verbose=False,debug=True,spirePhoto=False):
	"""
	Actually fixing the files
	"""
	obsid = None
	fix_HSAmeta =['instrument', 'modelName', 'observer', 'object','obsMode', 'cusMode','pointingMode'\
		'instMode','equinox','raDeSys','raNominal','decNominal','telescope', \
		'proposal','odNumber', 'startDate', 'endDate','aorLabel','posAngle','radialVelocity','blueband']
	for filepath in fitsfiles:
		camera = filepath.split("/")[-2]
		objectName = filepath.split("/")[-3]
		if camera not in ["red","green","blue"]: continue
		print filepath, objectName, camera
		if verbose: print "II - Opening file"
		try:
			orig = simpleFitsReader(filepath)
		except:
			print "EE - Could not open the file "
			continue
		OldMeta = orig.meta.copy()
		array_obsIds = Long1d()
		obsids = TableDataset()
		array_obsIds = Long1d()
		for obsid in DicoObs[objectName]:
			print "obsid",obsid
			array_obsIds.append(obsid)
		obsids["obsids"] = Column(array_obsIds,description="obsids")
		if verbose: print "II - Querying HSA"
		obs = getObservation(obsid,useHsa=True)
		if verbose: print "II - Fixing"
		
		for meta in fix_HSAmeta:
			if orig.meta.containsKey(meta):
				if orig.meta[meta].value == "Unknown" or meta=="startDate" or meta=="endDate":
					orig.meta[meta] = obs.meta[meta]
			else:
				if obs.meta.containsKey(meta):
					orig.meta[meta] = obs.meta[meta]
		# PACS specific
		if camera == "red": orig.meta['restwav'] = DoubleParameter(160e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
		if camera == "green": orig.meta['restwav'] = DoubleParameter(100e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
		if camera == "blue": orig.meta['restwav'] = DoubleParameter(70e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
		orig["obsids"] = obsids
		if not debug:
			if verbose: print "II - Saving"
			dirpath = os.path.dirname(filepath)
			newFilename = objectName+"_"+camera+"_"+typeProd+"_unimap.fits"
			os.system("mkdir -p "+outPutDir+objectName)
			orig["PrimaryImage"].setMeta(OldMeta)
			#FitsArchive().save(filepath,orig)
			simpleFitsWriter(orig,outPutDir+objectName+"/"+newFilename)

# Main program : 
#basePath="/data/glx-herschel/data1/herschel/SCANAMORPHOS_Fits/MAPS_PACS/SAG-4/"
basePath = "/data/glx-herschel/data1/herschel/scriptsPACS/Unimap/unimap_v2.39/SAG-4/"
outPutDir = "/data/glx-herschel/data1/herschel/Release/R2_pacs_photo/UNIMAP_Fits/MAPS_PACS/SAG-4/"
typeProd = "wgls"
typefits = "img_"+typeProd+".fits"
motifs = [typefits]
exclure = ["Level1IAS"]
debug, verbose = False, False
fitsfiles = getFilesMotif(basePath,motifs,exclure)
print fitsfiles
fixFiles(fitsfiles,DicoObsids,outPutDir,verbose,debug)
