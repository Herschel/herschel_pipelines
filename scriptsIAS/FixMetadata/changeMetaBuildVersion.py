#!/usr/bin/env python
import os, string
import time

execfile('/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/Database_Tools.py')
execfile('/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/PACS_Spectro_Tools.py')


directory = "/data/glx-herschel/data1/herschel/HIPE_Fits/FTS_SPIRE/SAG-4/"
#directory = "/data/glx-herschel/data1/herschel/Release/R1_spire_fts/HIPE_Fits/FTS_SPIRE/SAG-4/"
#motifs = ["1342204026_spectrum_IC59_SPIRE-FTS_10.0.2747_HR_unapod_0_1.fits"]
motifs = [".fits"]
exclure = ["sdt","sdi","spectrum_"]

listfile = getFilesMotif(directory,motifs,exclure)

for i in listfile:
	
	print i

	fits = pyfits.open(i,mode='update')
	hdr0 = fits[0].header
	cards = hdr0.ascardlist()
	DICO = makeDicoPacsSpectro(cards)
	
#	print DICO
	
	for key, value in DICO.iteritems():
		if value.count("'BuildVersion'") and fits[0].header[key]!="10.0.2747":
			print fits[0].header[key]
			fits[0].header[key]="10.0.2747"
		
#	print fits[0].header
	
	fits.close()
