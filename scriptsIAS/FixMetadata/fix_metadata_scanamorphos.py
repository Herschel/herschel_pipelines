import os, re, sys, getopt
from IAS_tools import *


####### Add missing metadata to scanamorphos primary header
####### Add a obsids hdu extension with the list of combined obsids using OBSID00x from the primary header


def fixFiles(fitsfiles, verbose=False, debug=True,spirePhoto=False,L2=True):
	"""
	Actually fixing the files
	"""
	obsid=None
	fix_HSAmeta =['instrument', 'modelName', 'observer', 'object','obsMode', 'cusMode','pointingMode'\
		'instMode','equinox','raDeSys','raNominal','decNominal','telescope', \
		'proposal','odNumber', 'startDate', 'endDate','aorLabel']

	
	if spirePhoto:
		listPhotoSpire=["scanSpeed","mapLength","mapHeight",\
                "numScanLinesNom","numScanLinesOrth"]
		fix_HSAmeta.extend(listPhotoSpire)
	for filename in fitsfiles:
		print "filename",filename
		print "Processing "+os.path.basename(filename)+"..."
		if verbose:
			print "  II - Opening file"
		try:
			orig = simpleFitsReader(filename)
		except:
			print "  EE - Could not open the file "
			continue
		if L2:
			array_obsIds = Long1d()
			obsids = TableDataset()
		if orig.meta.containsKey('obsid'):
			obsid=orig.meta['obsid'].value
		if orig.meta.containsKey('OBSID001'):
			obsid=orig.meta['OBSID001'].value
			# Add obsids extension with obsids list in a dataset
			array_obsIds = Long1d()
			counterObsids=1
			obsid_name='OBSID00'+str(counterObsids)
			print "obsid_name",obsid_name
			while(orig.meta.containsKey(obsid_name)):
				obsid=Long(orig.meta[obsid_name].value)
                       		array_obsIds.append(obsid)
				counterObsids=counterObsids+1
				# trick to get the proper number of 00
				if counterObsids < 10:
					obsid_name='OBSID00'+str(counterObsids)
				else:
					obsid_name='OBSID0'+str(counterObsids)
				print obsid_name
        			obsids["obsids"] = Column(array_obsIds,description="obsids")
		if obsid != None:
			if verbose:
				print "  II - Querying HSA"
			obs = getObservation(obsid,useHsa=True)
			if verbose: 
				print " II - Fixing"
			for meta in fix_HSAmeta:
				if orig.meta.containsKey(meta):
					if orig.meta[meta].value == "Unknown" or meta=="startDate" or meta=="endDate":
						print "etape 2",meta
						orig.meta[meta] = obs.meta[meta]
				else:
					if obs.meta.containsKey(meta):
						print "etape 3",meta
						orig.meta[meta] = obs.meta[meta]
			# PACS specific
			if orig.meta.containsKey("filter"):
				if ("red reformatted for Sanepic" in orig.meta['description'].value) or (orig.meta['filter'].value==160): 
					orig.meta['restwav'] = DoubleParameter(160e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
				if ("green reformatted for Sanepic" in orig.meta['description'].value) or orig.meta['filter'].value==100 : 
					orig.meta['restwav'] = DoubleParameter(100e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
				if ["blue reformatted for Sanepic" in orig.meta['description'].value] or orig.meta['filter'].value==70 : 
					orig.meta['restwav'] = DoubleParameter(70e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
			if L2:
				orig["obsids"] = obsids
			if not debug and verbose:
				print "  II - Saving"
				FitsArchive().save(filename,orig)
		else:
			print "  EE - Does not contains the obsid key"

# Main program : 
#basePath="/data/glx-herschel/data1/herschel/SCANAMORPHOS_Fits/MAPS_PACS/SAG-4/"
L2=False
basePath="/data/glx-herschel/data1/herschel/Release/Attic/SANEPIC_Fits/L1_Sanepic_PACS/H-ATLAS/gama12/"
motifs=["fits"]
exclure=["xdr"]
debug=False
verbose=True
fitsfiles = getFilesMotif(basePath,motifs,exclure)
print fitsfiles
###### Add spirePhoto=True for SPIRE PHOTO default = False
fixFiles(fitsfiles, verbose, debug,L2)

#if __name__ == "__main__":
#	sys.exit(main())
