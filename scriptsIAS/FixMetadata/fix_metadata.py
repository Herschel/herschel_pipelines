import os, re, sys, getopt

def usage():
	"""
	print usage information	
	"""
	print "fix_metadata.py [--debug] [--verbose] in re"
	print ""
	print "This program will try to retrieve missing meta data from the HSA"
	print ""
	print "positional arguments:"
	print " in  the starting directory to scan"
	print " re  the python regular expression of the filename to look for"
	print "     eg. '^OD.*sanepic\.fits$' for L1 SPIRE"
	print "     or  '^OD.*_PACS\.fits$' for L1 PACS"
	print "     they can be added together, separated by | "
	print ""
	print "optional arguments:"
	print " -h, --help    show this help message and exit"
	print " -d, --debug   only produce text output instead of performing action"
	print " -v, --verbose produce a more verbose output"

def scanODFitsFiles(basePath, regularExpression, verbose=False):
	"""
	Take a base Path as argument and construct an Hash containing all fits file and corresponding header key
	"""
	fitsFileName = []
	myRe = re.compile(regularExpression)
	print "Scanning " + basePath + "..."
	for root, dirs, files in os.walk(basePath):
		if verbose:
			print "  Scanning "+ root.replace(basePath,'')
		fitsfiles = [filename for filename in files if (myRe.match(filename))]
		for filename in fitsfiles:
			fitsFileName.append(os.path.join(root,filename))
	return fitsFileName


def fixFiles(fitsfiles, verbose=False, debug=True,spirePhoto=False):
	"""
	Actually fixing the files
	"""
	fix_HSAmeta =['instrument', 'modelName', 'observer', 'object','obsMode', 'cusMode',\
		'instMode','equinox','raDeSys','raNominal','decNominal','telescope', \
		'proposal','odNumber', 'startDate', 'endDate']
	
	if spirePhoto:
		listPhotoSpire=["scanSpeed","mapLength","mapHeight",\
                "numScanLinesNom","numScanLinesOrth"]
		fix_HSAmeta.extend(listPhotoSpire)
	for filename in fitsfiles:
		print "Processing "+os.path.basename(filename)+"..."
		if verbose:
			print "  II - Opening file"
		try:
			orig = FitsArchive().load(filename)
		except:
			print "  EE - Could not open the file "
			continue
		if orig.meta.containsKey('obsid') :
			if verbose:
				print "  II - Querying HSA"
			obs = getObservation(orig.meta['obsid'].value,useHsa=True)
			if verbose: 
				print " II - Fixing"
			for meta in fix_HSAmeta:
				if orig.meta.containsKey(meta):
					if orig.meta[meta].value == "Unknown":
						orig.meta[meta] = obs.meta[meta]
				else:
					if obs.meta.containsKey(meta):
						orig.meta[meta] = obs.meta[meta]
			# PACS specific
			if "red reformatted for Sanepic" in orig.meta['description'].value: 
				orig.meta['restwav'] = DoubleParameter(160e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
			if "green reformatted for Sanepic" in orig.meta['description'].value: 
				orig.meta['restwav'] = DoubleParameter(100e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
			if "blue reformatted for Sanepic" in orig.meta['description'].value: 
				orig.meta['restwav'] = DoubleParameter(70e-6,description="rest wavelength in vacuo [m]", unit=Length.METERS)
			if verbose:
				print "  II - Saving"
			if not debug:
				FitsArchive().save(filename,orig)
		else:
			print "  EE - Does not contains the obsid key"

# Main program : 
def main(argv=None):
	# Function HIPE/jylaunch does not know how to handle arguments....
	if argv is None:
		argv = sys.argv
	    
	try:
		opts, args = getopt.getopt(argv[1:],'hdv',['help','debug','verbose' ])
	except getopt.GetoptError, err:
		print str(err)	
		usage()
		return 2
	
	debug = False
	verbose = False

	
	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			return 1
		elif o in ("-d", "--debug"):
			debug=True
		elif o in ("-v", "--verbose"):
			verbose=True
		else:
			assert False, "unhandled option"
	
	try:
		basePath = args[0]
		if not os.path.isdir(basePath):
			raise NameError(basePath + " is not a directory")
	except:
		usage()
		return 2
	
	# 'OD.*sanepic\.fits$' ; for L1 SPIRE 
	# 'OD.*_PACS\.fits$' ; for L1 PACS
	
	try:
		regularExpression = args[1]
	except:
		usage()
		return 2
	
	print "Processing " + basePath 
	
	fitsfiles = scanODFitsFiles(basePath, regularExpression, verbose=verbose)
	###### Add spirePhoto=True for SPIRE PHOTO default = False
	fixFiles(fitsfiles, verbose=verbose, debug=debug,spirePhoto=True)


#if __name__ == "__main__":
#	sys.exit(main())
