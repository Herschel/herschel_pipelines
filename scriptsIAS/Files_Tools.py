import psycopg2
import pyfits,pywcs
import glob
import os, string, shutil
import time
from datetime import date
import math


def getListFiles(basePath):
	"""Get all the files from a directory, subdirectories included"""
	listfile_all=[]
	for root, dirs, files in os.walk(basePath):
		listfile = [root+'/'+filename for filename in files if (filename.endswith('.fits'))]
		listfile_all.extend(listfile)
	return listfile_all


def checkMotif(chaine,Motifs, Exclure):
	"""Check if a string contains some patterns and exclude some other"""
	for motif in Motifs:
		if str(chaine).count(motif)==0:return False

	for exclu in Exclure:
		if str(chaine).count(exclu)>0:return False

	return True


def ajoutRoot(root,exclure):
	for exclu in exclure:
		if str(root).count(exclu)>0:
			return False
	return True


def getFilesMotif(basePath,motifs,exclure):
	"""Get all the files from a directory, subdirectories included, that have some patterns and have not others"""
	listfile_all = []
	listroot = []
	for root, dirs, files in os.walk(basePath):
			##### WARNING KD added root in checkMotif call
		listfile = [root+"/"+filename for filename in files if ajoutRoot(root,exclure) and checkMotif(root+"/"+filename,motifs,exclure)]
		listfile_all.extend(listfile)
		
	listfile_all = sorted(listfile_all)
	
	return listfile_all


def logTime(message,fichier):
	"""Include Timestamped message in a file"""
	timeStamp = time.strftime("\n%d-%m-%Y %H:%M:%S - ")
	fileToWrite = open(fichier, "a")
	fileToWrite.write(timeStamp+message)
	fileToWrite.close()

def urlFilepath(filepath):

        #Je ne vais pas reinventer la roue !!!
        dicURL={}
        dicURL[" "] = "%20"
        dicURL[";"] = "%3b"
        #dicURL["/"] = "%2f"
        dicURL["?"] = "%3f"
        dicURL[":"] = "%3a"
        dicURL["="] = "%3d"
        dicURL["+"] = "%2b"

        #print "avant ",filepath
        #for i in dicURL.iterkeys():
        #  filepath = filepath.replace(i,dicURL[i])
        #print "apres ",filepath
        return filepath


def addMetadata(filepath="",key="", value="",comment=""):
        """Add a metadata to a fitsfile"""
        hdulist=pyfits.open(filepath,mode='update')
        hdr0=hdulist[0].header
        hdr0.update(key,value,comment)
        hdulist.close()


def addMetadata_List(filepath="",listMeta={"":""}):
        """Add a list of metadata to a fitsfile"""
        hdulist=pyfits.open(filepath)
        hdr0=hdulist[0].header
        for key, value in listMeta.iteritems(): hdr0.update(key,value,"")
        hdulist.close()
