#!/bin/sh 

HRSC_DIR=/data/glx-herschel/data1/herschel
INF1_DIR=/data/inf-calcul1/data2/herschel
GLX2_DIR=/data/glx-calcul2/data2/herschel

#chgrp -R herschel $INF1_DIR                           >& /dev/null
echo "updating permission on files"
find $HRSC_DIR -type f -exec chmod g+rw,o-rwx {} \;   >& /dev/null
echo "updating permission on directories"
find $HRSC_DIR -type d -exec chmod g+srwx,o-rwx {} \; >& /dev/null

# echo "rsyncing now...."
# rsync --delete -a $HRSC/ $INF1_DIR/
# rsync --delete -a $HRSC/ $GLX2_DIR/
