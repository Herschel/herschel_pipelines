import pyfits
import glob
import os, string, shutil
import time
from datetime import date
import math
import multiprocessing as mp

from Database_Tools import getFilesMotif
from fitsToPng import makeRGB
from multiprocessing import Pool

directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/HIPE_Fits/MAPS_SPIRE/"

motifs = ["destriped","PSW",".fits"]
exclure=[".png",".txt","SAG-4"]
listfile = getFilesMotif(directory,motifs,exclure)

listRGB = []
for i in listfile: listRGB.append(i.split("PSW")[0])

Pool().map(makeRGB,listRGB)
