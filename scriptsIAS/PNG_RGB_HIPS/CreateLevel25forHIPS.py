import pyfits, os
from multiprocessing import Pool

execfile("/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/Database_Tools.py")

#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/OT1_lmontier/"
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/clusters_lowz/"
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/oriB_S_SPIRE-satur-1/"
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-4/"
directory = "/data/glx-herschel/data1/herschel/scriptsSPIRE/PHOTOMETER/MCQA/catalog"
#outputDirectory = "/data/glx-herschel/data1/herschel/data/Aladin/"+wavelength+"/"
#BaseOutputDirectory = "/data/glx-herschel/data1/herschel/data/Aladin/HerschelPNG/"
BaseOutputDirectory = "/data/glx-herschel/data1/herschel/data/Aladin/Spire_Planck_Fields/"
#BaseOutputDirectory = "/data/glx-herschel/data1/herschel/data/Aladin/Herschel/"

def CreateHeaderFile(filepath):
	fits = pyfits.open(filepath)
	try:
		mainHeader = fits[0].header
		imageHeader = fits[1].header
	finally:
		fits.close()
	
	cardsMain, cards2nd = mainHeader.ascardlist(), imageHeader.ascardlist()
	mainStr, sndStr = "", ""
	for i in cardsMain: mainStr = mainStr+str(i)+"\n"
	for i in cards2nd: sndStr = sndStr+str(i)+"\n"
	toWrite = str(mainStr)+str(sndStr)
	#toWrite = str(sndStr)
	
	# create header text file
	filename = filepath.replace(".fits","_RGB_S.hhh").replace("_PSW","")
	fileTXT = open(filename,"w")
	fileTXT.write(toWrite)
	fileTXT.close()

def copyFile(filepath,outputDirectory):
	command="cp "+filepath+" "+outputDirectory
	print command
	os.system(command)

listTreatment=["CREATE_HEADERS"]
#listTreatment=["RGB_COPY","CREATE_HEADERS"]
#listTreatment=["FITS_COPY"]

#for wavelength in ["PSW","PMW","PLW"]:
for wavelength in ["PSW"]:
	########## maps with combined
	outputDirectory=BaseOutputDirectory+wavelength+"/"
	for treatment in listTreatment:
	########## WARNING to use moveheader comments the 2 following lines
		if treatment=="RGB_COPY":
			#motifs = ["combined","destriped","ExtEmi","RGB",".png"]
			motifs = ["combined","destriped","RGB",".png"]
			exclure=[".fits",".txt"]
		elif treatment=="FITS_COPY" or treatment=="CREATE_HEADERS":
			#motifs = ["combined","destriped","ExtEmi",wavelength,".fits"]
			motifs = ["combined","destriped",wavelength,".fits"]
			exclure=[".txt","png"]
		
		listfile = list(sorted(getFilesMotif(directory,motifs,exclure)))

#		for filepath in listfile:
#			print filepath 
#			# add field with "combined" in exclure
#			field = filepath.split("/")[-2]
#			print field
#			exclure.append(field)
#			print motifs, exclure

		########## maps without combined (only one obsid available)
		motifs.remove("combined")
		listfile_notCombined = getFilesMotif(directory,motifs,exclure)
		listfile.extend(listfile_notCombined)

		for filepath in listfile:
			print filepath 
			#copyFile(filepath,outputDirectory)
			if treatment=="CREATE_HEADERS" and wavelength=="PSW":
				filepath_copied = outputDirectory+os.path.basename(filepath)
				CreateHeaderFile(filepath_copied)
				

