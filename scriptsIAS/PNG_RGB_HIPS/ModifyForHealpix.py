import pyfits,pywcs
import glob
import os, string, shutil
import time
from datetime import date
import math
execfile("/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/Database_Tools.py")

wavelength="PSW"
#directory = "/data/glx-herschel/data1/herschel/data/Aladin/SAG-4"
directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3"
#outputDirectory="/data/glx-herschel/data1/herschel/data/Aladin/"+wavelength+"/"
outputDirectory="/data/glx-herschel/data1/herschel/data/Aladin/RGBpng/"

def moveHeader(filepath):

	fits = pyfits.open(filepath)
	output_fitsfile=outputDirectory+os.path.basename(filepath)
	print output_fitsfile
	try:
		mainHeader = fits[0].header
		ImageHeader=fits[1].header
		Imagedata=fits[1].data
		# save Image header into primary header
		pyfits.writeto(output_fitsfile,Imagedata,ImageHeader)
		# create header text file
		filename = output_fitsfile.replace(".fits","_RGB_S.hhh").replace("_PSW","")
		fileTXT = open(filename,"w")
		fileTXT.write(str(ImageHeader))
		fileTXT.close()
	finally:
		fits.close()

def copyPng(filepath):
	command="cp "+filepath+" "+outputDirectory
	print command
	os.system(command)
	
########## maps with combined
########## WARNING to use moveheader comments the 2 following lines
#motifs = ["combined","destriped","ExtEmi","RGB",".png"]
#exclure=[".fits",".txt"]
########## WARNING to use copypng comments the 2 following lines
motifs = ["combined","destriped","ExtEmi",wavelength,".fits"]
exclure=[".png",".txt"]
listfile = getFilesMotif(directory,motifs,exclure)
listfile = list(sorted(listfile))

for filepath in listfile:
	print filepath 
	# add field with "combined" in exclure
	field = filepath.split("/")[-2]
	exclure.append(field)
	print motifs

########## maps without combined (only one obsid available)
motifs.remove("combined")
listfile_notCombined = getFilesMotif(directory,motifs,exclure)
listfile.extend(listfile_notCombined)


for filepath in listfile:
	print filepath 
	moveHeader(filepath)
	#copyPng(filepath)
