import pyfits
import Image
import os
import Image
import numpy as np
import matplotlib.pyplot as plt
import io
from scipy.misc import imresize, fromimage
import sys

execfile('/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/fitsToPng.py')

try:
	fullpath = sys.argv[1]
except:
	print "Error - Input files"

try:
	orig = sys.argv[2]
except:
	print "Default Orig Band"
	orig = "L"

orig = sys.argv[2]

if orig =="L": bandList = ["L","M","S"]
if orig =="M": bandList = ["M","S","L"]
if orig =="S": bandList = ["S","M","L"]

dicBand = {"L":0,"M":1,"S":2}

for band in bandList:

	filename = fullpath+'P'+band+'W.fits'
	fits = pyfits.open(filename)
	imArray = fits["Image"].data
	xsize = fits["Image"].header["NAXIS1"]
	ysize = fits["Image"].header["NAXIS2"]
	res = abs(fits["Image"].header["CDELT1"])*3600
	print "fits",xsize, ysize, res

	#imTest = Image.fromarray(imArray)
	#cropBox = imTest.getbbox()
	#width = abs(cropBox[2]-cropBox[0])
	#height = abs(cropBox[3]-cropBox[1])
	#cropBox = (cropBox[0],ysize-cropBox[1]-height,cropBox[2],ysize-cropBox[3]+height)
	#imArray[fits_data==0] = numpy.nan

	vminZ, vmaxZ = zscale(imArray)
	#vminZ, vmaxZ = numpy.nanmin(imArray), numpy.nanmax(imArray)

	buff = io.BytesIO()
	plt.imsave(buff, imArray, vmin=vminZ, vmax=vmaxZ, cmap="gray", origin='lower', format='png')
	buff.seek(0)

	imArray[imArray==numpy.nan] = 0
	imTest = Image.fromarray(imArray)
	cropBox = imTest.getbbox()
	width = abs(cropBox[2]-cropBox[0])
	height = abs(cropBox[3]-cropBox[1])
	cropBox = (cropBox[0],ysize-cropBox[1]-height,cropBox[2],ysize-cropBox[3]+height)
	
	

	im = Image.open(buff)
	im = im.crop(cropBox)
	im.save(filename[:-5]+"_Orig.png")
	
	if band==orig:
		sizeL = im.size
		rgbArray = np.zeros((sizeL[0],sizeL[1],3), 'uint8')
		
	if band!=orig:
		im = im.resize(sizeL)
		
	im.save(filename[:-5]+".png")
#	print "%dx%d" % im.size, im.mode

	arrayData = fromimage(im,flatten=True).T
#	print "sizeL",sizeL,"SizeRead",arrayData.shape
	rgbArray[:,:, dicBand[band]] = arrayData[:,:]
	buff.close()
	
img = Image.fromarray(rgbArray)
print "%dx%d" % img.size, img.mode
img.save(filename[:-5]+'_RGB_'+orig+'.png')
