#!/usr/bin/env python
# -*-coding:utf-8 -*
# pywcs module must be installed. "apt-get install python-pywcs"

#bhasnoun
# please add /data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/ in your PYTHONPATH
from Files_Tools import getFilesMotif
from fitsToPngDBG import makeMapThumbPlotHeader, changeMetadata, makeRGB
from multiprocessing import Pool
from random import shuffle
import os, sys


####################################################################################################################
####################################################################################################################

# SANEPIC_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/Attic/SANEPIC_Fits/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/SANEPIC_Fits/L1_Sanepic_SPIRE/"

# HIPE_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/OrionB-NN-1/"
#directory = "/data/glx-herschel/data1/herschel/Release/R1_pacs_photo/HIPE_Fits/MAPS_PACS/SAG-3/"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R6_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/"
#directory= "/data/glx-herschel/data1/herschel/Release/R5_spire_fts/HIPE_Fits/FTS_SPIRE/SAG-4/"
#directory = "/data/glx-herschel/data1/herschel/Release/R6_spire_photo/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_fts/HIPE_Fits/FTS_SPIRE/"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/"


# SUPREME_FITS
#directory = "/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE/Supreme"
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/SUPREME_Fits/"

# UNIMAP_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/R2_pacs_photo/UNIMAP_Fits/MAPS_PACS/SAG-3/"

# DEBUG
#~ directory = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/HH_dense_core"
#directory = "/home/ccossou/output_test"


####################################################################################################################
####################################################################################################################

#motifs = ["UrsaMajor_combined_PLW.fits"]
#motifs = [".fits","SupremePhoto"]
motifs = [".fits"]
#motifs = ["cube.fits","1342197489_Ced201-3_SPIRE-FTS_10.0.2747_HR_SLW_aNB_15"]
#exclure = ["ATLAS","old","test"]
#exclure = ["RebinnedCube","SlicedCube","Frame","Raster","OT2_ehabart"]
exclure = []

####################################################################################################################
####################################################################################################################

# NO DIRECTORY SET TO FORCE THE SCRIPT TO CRASH IF THE listfile ARRAY IS NOT SET. 
#~ listfile = getFilesMotif(directory,motifs,exclure)[:]

#for i in listfile: print i

#~ Pool().map(makeMapThumbPlotHeader,listfile[:])

listfile = ['/data/glx-herschel/data1/herschel/Release/R7_spire_fts/HIPE_Fits/FTS_SPIRE/SAG-4/HD37022(towards)/1342204920_spectrum_HD37022(towards)_SPIRE-FTS_15.0.3244_HR_unapod_0_8.fits']


print len(listfile)

for filename in listfile:
	print filename
	makeMapThumbPlotHeader(filename)
##Pool().map(changeMetadata,listfile)

#~ os.system("chmod 775 -R "+directory)

print "Thumbnails created successfully."

