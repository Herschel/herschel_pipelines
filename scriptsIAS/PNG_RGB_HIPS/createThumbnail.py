#!/usr/bin/env python
# -*-coding:utf-8 -*
# pywcs module must be installed. "apt-get install python-pywcs"

#bhasnoun
# please add /data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/ in your PYTHONPATH
from Files_Tools import getFilesMotif
from fitsToPng import makeMapThumbPlotHeader, changeMetadata, makeRGB
from multiprocessing import Pool
from random import shuffle
import os, sys


####################################################################################################################
####################################################################################################################

# SANEPIC_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/Attic/SANEPIC_Fits/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/SANEPIC_Fits/L1_Sanepic_SPIRE/"

# HIPE_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/OrionB-NN-1/"
#directory = "/data/glx-herschel/data1/herschel/Release/R1_pacs_photo/HIPE_Fits/MAPS_PACS/SAG-3/"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R6_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/"
#directory= "/data/glx-herschel/data1/herschel/Release/R5_spire_fts/HIPE_Fits/FTS_SPIRE/SAG-4/"
directory = "/data/glx-herschel/data1/herschel/Release/R7_spire_fts/HIPE_Fits/FTS_SPIRE/"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R6_spire_fts/HIPE_Fits/FTS_SPIRE/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_pacs_photo"
#directory = "/data/glx-herschel/data1/herschel/Release/R7_pacs_spectro"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/"


# SUPREME_FITS
#directory = "/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE/Supreme"
#directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/SUPREME_Fits/"
#directory = "/data/glx-herschel/data1/herschel/Release/R8_spire_photo/SUPREME_Fits/MAPS_SPIRE/"
#directory = "/data/glx-herschel/data1/herschel/Release/R7_spire_fts/SUPREME_Fits/FTS_SPIRE/"

# UNIMAP_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/R2_pacs_photo/UNIMAP_Fits/MAPS_PACS/SAG-3/"

# DEBUG
#~ directory = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4/HH_dense_core"
#~ directory = "/data/glx-herschel/data1/herschel/Release/R6_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-3/L1521"
#directory = "/home/ccossou/output_test"

#directory = "/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE/"
#directory = "/data/glx-herschel/data1/herschel/UNIMAP_Fits/MAPS_PACS/"

####################################################################################################################
####################################################################################################################

#motifs = ["UrsaMajor_combined_PLW.fits"]
#motifs = [".fits","SupremePhoto"]
motifs = [".fits"]
#motifs = ["cube.fits","1342197489_Ced201-3_SPIRE-FTS_10.0.2747_HR_SLW_aNB_15"]
#exclure = ["ATLAS","old","test"]
#exclure = ["RebinnedCube","SlicedCube","Frame","Raster","OT2_ehabart"]
exclure = []

####################################################################################################################
####################################################################################################################

listfile = getFilesMotif(directory,motifs,exclure)[:]

# We delete the file where we store all the filenames we failed to generate a thumbnails for.
pbl_file = "problem_files.txt"
if os.path.exists(pbl_file):
	os.remove(pbl_file) 

# Temporary
#~ directory2 = "/data/glx-herschel/data1/herschel/Release/R5_pacs_spectro/"
#~ listfile.extend(getFilesMotif(directory2,motifs,exclure)[:])

print("Number of files: %d" % len(listfile))
#for i in listfile: print i

#~ print(listfile[:])

Pool().map(makeMapThumbPlotHeader,listfile[:])
##Pool().map(changeMetadata,listfile)

# chmod doesn't work at all. 
#~ os.system("chmod 775 -R "+directory)



if os.path.exists(pbl_file):
	print("\nFind attached in %s all the .fits file where thumbnail failed." % pbl_file)
else:
	print("\nThumbnails created successfully.")
