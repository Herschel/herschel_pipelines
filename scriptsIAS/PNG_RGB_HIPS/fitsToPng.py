#!/usr/bin/env python
# -*-coding:utf-8 -*

#bhasnoun

"""Tools for generating maps png files, spectra plot files, header texts files, html files for Hesiod Portal"""


import os, io
import math
from astropy.io import fits as pyfits
import numpy
import Image 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import sys
from matplotlib.axes import Axes
import shutil # To copy files
import pdb

MAX_REJECT = 0.5
MIN_NPIXELS = 5
GOOD_PIXEL = 0
BAD_PIXEL = 1
KREJ = 2.5
MAX_ITERATIONS = 5

def zscale (image, nsamples=1000, contrast=0.25):#, bpmask=None, zmask=None):
	"""Implement IRAF zscale algorithm
	nsamples=1000 and contrast=0.25 are the IRAF display task defaults
	bpmask and zmask not implemented yet
	image is a 2-d numpy array
	returns (z1, z2)
	"""

	# Sample the image
	samples = zsc_sample (image, nsamples)#, bpmask, zmask)
	npix = len(samples)
	samples.sort()
	zmin = samples[0]
	zmax = samples[-1]
	# For a zero-indexed array
	center_pixel = (npix - 1) / 2
	if npix%2 == 1:
		
		median = samples[center_pixel]
	else:
		median = 0.5 * (samples[center_pixel] + samples[center_pixel + 1])

	# Fit a line to the sorted array of samples
	minpix = max(MIN_NPIXELS, int(npix * MAX_REJECT))
	ngrow = max (1, int (npix * 0.01))
	ngoodpix, zstart, zslope = zsc_fit_line (samples, npix, KREJ, ngrow,MAX_ITERATIONS)

	if ngoodpix < minpix:
		
		z1 = zmin
		z2 = zmax
	else:
		if contrast > 0:
			zslope = zslope / contrast
		z1 = max (zmin, median - (center_pixel - 1) * zslope)
		z2 = min (zmax, median + (npix - center_pixel) * zslope)
	return z1, z2

def zsc_sample (image, maxpix):#, bpmask=None, zmask=None):

	""" Figure out which pixels to use for the zscale algorithm
	Returns the 1-d array samples
	Don't worry about the bad pixel mask or zmask for the moment
	Sample in a square grid, and return the first maxpix in the sample"""

	nc = image.shape[0]
	nl = image.shape[1]
	stride = max (1.0, math.sqrt((nc - 1) * (nl - 1) / float(maxpix)))
	stride = int (stride)
	
	samples = image[::stride,::stride].flatten()
	indicesNoNan = numpy.where(samples!=samples)[0]
	for i in sorted(indicesNoNan, reverse=True): samples = numpy.delete(samples, i)
	
	return samples[:maxpix]


def zsc_fit_line (samples, npix, krej, ngrow, maxiter):

	#
	# First re-map indices from -1.0 to 1.0
	xscale = 2.0 / (npix - 1)
	xnorm = numpy.arange(npix)
	xnorm = xnorm * xscale - 1.0

	ngoodpix = npix
	minpix = max (MIN_NPIXELS, int (npix*MAX_REJECT))
	last_ngoodpix = npix + 1

	# This is the mask used in k-sigma clipping.  0 is good, 1 is bad
	badpix = numpy.zeros(npix, dtype="int32")

	#
	#  Iterate

	for niter in range(maxiter):

		if (ngoodpix >= last_ngoodpix) or (ngoodpix < minpix):
			
				break

		# Accumulate sums to calculate straight line fit
		goodpixels = numpy.where(badpix == GOOD_PIXEL)
		sumx = xnorm[goodpixels].sum()
		sumxx = (xnorm[goodpixels]*xnorm[goodpixels]).sum()
		sumxy = (xnorm[goodpixels]*samples[goodpixels]).sum()
		sumy = samples[goodpixels].sum()
		sum = len(goodpixels[0])

		delta = sum * sumxx - sumx * sumx
		# Slope and intercept
		intercept = (sumxx * sumy - sumx * sumxy) / delta
		slope = (sum * sumxy - sumx * sumy) / delta

		# Subtract fitted line from the data array
		fitted = xnorm*slope + intercept
		flat = samples - fitted

		# Compute the k-sigma rejection threshold
		ngoodpix, mean, sigma = zsc_compute_sigma (flat, badpix, npix)

		threshold = sigma * krej

		# Detect and reject pixels further than k*sigma from the fitted line
		lcut = -threshold
		hcut = threshold
		below = numpy.where(flat < lcut)
		above = numpy.where(flat > hcut)

		badpix[below] = BAD_PIXEL
		badpix[above] = BAD_PIXEL

		# Convolve with a kernel of length ngrow
		kernel = numpy.ones(ngrow,dtype="int32")
		badpix = numpy.convolve(badpix, kernel, mode='same')

		ngoodpix = len(numpy.where(badpix == GOOD_PIXEL)[0])

		niter += 1

	# Transform the line coefficients back to the X range [0:npix-1]
	zstart = intercept - slope
	zslope = slope * xscale

	return ngoodpix, zstart, zslope

def zsc_compute_sigma (flat, badpix, npix):

	"""Compute the rms deviation from the mean of a flattened array.
	Ignore rejected pixels"""

	# Accumulate sum and sum of squares
	goodpixels = numpy.where(badpix == GOOD_PIXEL)
	sumz = flat[goodpixels].sum()
	sumsq = (flat[goodpixels]*flat[goodpixels]).sum()
	ngoodpix = len(goodpixels[0])
	if ngoodpix == 0:
		
		mean = None
		sigma = None
	elif ngoodpix == 1:
	
		mean = sumz
		sigma = None
	else:
		mean = sumz / ngoodpix
		temp = sumsq / (ngoodpix - 1) - sumz*sumz / (ngoodpix * (ngoodpix - 1))
		if temp < 0:
			sigma = 0.0
		else: sigma = math.sqrt (temp)

	return ngoodpix, mean, sigma

def changeMetadata(i):
	
	if (i.count("map")>0 and i.count("L1")==0) and i.count("FTS")==0 or i.count("PLW")>0 or i.count("PMW")>0 or i.count("PSW")>0:
		
		makePNG = True
		try:
			makeThumbMap(i,makePNG)
		except: 
			sys.stderr.write("[%s] %s\n" % ("MAP SPIRE", i))


def makeDefaultThumbnail(pathname):
	"""
	If the standard generation failed, we make a default Thumbnail. 
	We'll copy an existing thumbnail saying "not available" in place of the 
	thumbnail we were expecting to create in the first place. 
	
	@param pathname The complete absolute path toward the .fits file we wanted to create the thumbnail from
	"""
	default_thumbnail = "not_available.png"
	script_path = os.path.dirname(os.path.realpath(__file__))
	thumb_path = os.path.join(script_path, default_thumbnail)
	
	#png name is based on the .fits name
	png_name = pathname.replace(".fits", ".png")
	
	# Copying the default Thumbnail in place of the one we couldn't create
	shutil.copyfile(thumb_path, png_name)
	
	# This print prepare the construction of a list of filenames of all the .fits were .png failed
	filename = "problem_files.txt"
	pbl_file = open(filename, 'a')
	pbl_file.write("'%s', \n" % pathname)
	pbl_file.close()
	

def makeMapThumbPlotHeader(i):
	
	fileIgnored = True # Does the current file treated by one or more regExp, or not?
	
	# Skip the current .fits file if a corresponding .png already exists.
	#png name is based on the .fits name
	png_name = i.replace(".fits", ".png")
	
	# File size is hard coded here because we always use the same file. In case this file change, this value must also be updated. 
	# This is due to the fact that we want to try again to generate thumbnail if it failed previously, just in case something evolved
	if (os.path.exists(png_name) and os.path.getsize(png_name) != 14817):
		return
	
	# MAP SPIRE
	if i.count("L1_")==0 and i.count("FTS")==0 and (i.count("PLW")>0 or i.count("PMW")>0 or i.count("PSW")>0):
		
		fileIgnored = False
		makeThumbMap(i)
		#~ try: 
			#~ makeThumbMap(i)
		#~ except: 
			#~ makeDefaultThumbnail(i)
			#~ sys.stderr.write("[%s] %s\n" % ("MAP SPIRE", i))
	
	# MAP PACS
	if i.count("MAPS_PACS")==1 and i.count("L1_")==0 and (i.count("red")>0 or i.count("green")>0 or i.count("blue")>0):
		
		fileIgnored = False
		makeThumbMap(i)
		#~ try: 
			#~ makeThumbMap(i)
		#~ except: 
			#~ makeDefaultThumbnail(i)
			#~ sys.stderr.write("[%s] %s\n" % ("MAP PACS", i))
	
	# MAP SPIRE ONLY
	# Only for PSW since we combine the 3 channels at once. We must not enter here for each channel
	if i.count("MAPS_SPIRE") and (i.count("PSW")>0):
		
		fileIgnored = False
		makeRGB(i)
		#~ try: 
			#~ makeRGB(i)
		#~ except: 
			#~ sys.stderr.write("[%s] %s\n" % ("RGB", i))
	
	# PACS SPECTRO
	# "_" prefix is mandatory because we hit all the files only with the "Cube" item, even RasterCube we don't want.
	spectro_cubes = ["_Cube", "_RebinnedCube", "_ProjectedCube", "_DrizzledCube", "_EquidistantDrizzledCube", "_InterpolatedCube", "_EquidistantInterpolatedCube"]
	#~ if i.count("SPECTRO_PACS")==1 and ( i.count("DrizzledCube.fits")>0 or i.count("ProjectedCube.fits")>0 or i.count("RegularSpatialGridInterpolationCube")>0 ):
	
	if i.count("SPECTRO_PACS")==1 and (any(product in i for product in spectro_cubes)):
		
		fileIgnored = False
		makeThumbCubePACS(i)
		#~ try: 
			#~ makeThumbCubePACS(i)
		#~ except: 
			#~ makeDefaultThumbnail(i)
			#~ sys.stderr.write("[%s] %s\n" % ("CUBE PACS", i))
	
	if i.count("MapFlux")>0:
		
		fileIgnored = False
		makeThumbMapFlux(i)
		#~ try: 
			#~ print i; 
			#~ makeThumbMapFlux(i)
		#~ except: 
			#~ makeDefaultThumbnail(i)
			#~ sys.stderr.write("[%s] %s\n" % ("MAPFLUX PACS", i))
	
	# FTS SPIRE
	if i.count("cube.fits")>0:
		
		fileIgnored = False
		makeThumbCubeSPIRE(i)
	if i.count("spectrum")>0 and i.count("spectrum2d")==0:
		
		fileIgnored = False
		makeThumbSpectrum(i)
	if i.count("spectrum2d")>0:
		
		fileIgnored = False
		makeThumbSpectrum2d(i)
	if i.count("sdt")>0:
		
		fileIgnored = False
		makeThumbVoltage(i)
	if i.count("sdi")>0:
		
		fileIgnored = False
		makeThumbInter(i)
	
	if fileIgnored:
		
		print("Ignored: %s" % i)


def makeRGB(fullpath):
	"""Make RGB image from 3 SPIRE Maps P[S,M,L]W fits files
	
	This function is prepared to work only for the PSW .fits file, 
	retrieving the other two based on the filename of the PSW one.
	
	All that because PSW is the biggest of the 3, and we want to combine them. 
	Otherwise, we have problem when resizing the others."""
	import Image
	from scipy.misc import fromimage
	
	ref = "PSW"
	threeBandExists = os.path.isfile(fullpath) and os.path.isfile(fullpath.replace(ref,"PMW")) and os.path.isfile(fullpath.replace(ref,"PLW"))
	if threeBandExists:
		
		
		
		bandList = ["PSW","PMW","PLW"]
		dicBand = {"PLW":0,"PMW":1,"PSW":2}
		Mask = []

		for band in bandList:

			filename = fullpath.replace(ref,band)
			fits = pyfits.open(filename)
			imArray = fits["Image"].data
			xsize = fits["Image"].header["NAXIS1"]
			ysize = fits["Image"].header["NAXIS2"]
			#print band,imArray.shape
			if band==ref:
				flip = (fits["Image"].header["CDELT2"]>0)

			vminZ, vmaxZ = zscale(imArray)

			buff = io.BytesIO()
			plt.imsave(buff, imArray, vmin=vminZ, vmax=vmaxZ, cmap="gray", origin='upper', format='png')
			buff.seek(0)

			imTest = Image.fromarray(imArray)
			cropBox = imTest.getbbox()
			width = abs(cropBox[2]-cropBox[0])
			height = abs(cropBox[3]-cropBox[1])
			cropBox = (cropBox[0],ysize-cropBox[1]-height,cropBox[2],ysize-cropBox[3]+height)

			im = Image.open(buff)
			im = im.crop(cropBox)

			maskArray = imArray*0 + 255
			maskArray[imArray!=imArray] = 0
			imMask = Image.fromarray(maskArray)

			if band==ref:
				
				sizeRef = im.size
				rgbArray = numpy.zeros((sizeRef[1],sizeRef[0],4), 'uint8')
				maskArrayGlobal = numpy.zeros((sizeRef[1],sizeRef[0]))

			if band!=ref:
				
				im = im.resize(sizeRef)
				imMask = imMask.resize(sizeRef)

			maskBand = numpy.array(imMask.getdata()).reshape(sizeRef[1], sizeRef[0])
			maskArrayGlobal = maskArrayGlobal + maskBand

			### RGB final data
			arrayData = fromimage(im,flatten=True)
			rgbArray[:,:, dicBand[band]] = arrayData[:,:] # Stock band data in each layer
			rgbArray[:,:,3] = 255 # Opaque alpha for all pixels

			buff.close()

		maskArrayGlobal[maskArrayGlobal!=(3*255)] = 0
		maskX = numpy.where(maskArrayGlobal==0)[0]
		maskY = numpy.where(maskArrayGlobal==0)[1]
		rgbArray[maskX,maskY,3] = 0

		imRGB = Image.fromarray(rgbArray)
		# TODO il faut renommer ça differemment pour tenir compte des subtilités 
		# du nom de fichier au lieu d'enlever les 8 dernier caractères, extension comprise. 
		RGBfile = filename[:-8]+'RGB_'+ref+'.png' 
		if flip:
			imRGB = imRGB.transpose(Image.FLIP_TOP_BOTTOM)
		imRGB.save(RGBfile)
		#if display=="display":
	#os.system("display "+RGBfile)


def makeSolarPNG(fitsfile):
	"""Generate Solar png files from fits files"""
	
	print fitsfile
	fits = pyfits.open(fitsfile)
	fix = fits.verify('silentfix')
	fits_data = fits[1].data
	fits.close()
	try: vminZ, vmaxZ = zscale(fits_data)
	except: vminZ, vmaxZ = numpy.nanmin(fits_data), numpy.nanmax(fits_data)
	plt.imsave(fitsfile[:len(fitsfile)-5]+".png", fits_data, vmin=vminZ, vmax=vmaxZ, cmap=cm.hot, origin='lower', format='png')


def makeMapFluxImage(fileToProcess):

	import pywcsgrid2
	from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable

	fitsfile = fileToProcess.replace("ProjectedCube","MapFlux")
	fits = pyfits.open(fitsfile)
	mapFluxData = fits["MAPFLUX"].data
	fig = plt.figure(1)
#	fig.suptitle(champ+" "+titre, fontsize=12, fontweight='bold')
	ax = pywcsgrid2.subplot(111, header=fits["MAPFLUX"].header)
	#ax.set_title("MapFlux - Flux integration for each pixel")
	ax.set_ticklabel_type('absdeg','absdeg')
	divider = make_axes_locatable(ax)
	cax = divider.new_horizontal("7%", pad=0.2, axes_class=Axes)
	fig.add_axes(cax)
	im = ax.imshow(mapFluxData, origin="lower", interpolation="none")
	cbar = plt.colorbar(im, cax=cax)
#	cbar.ax.set_yticklabels([ '{0:.2e}'.format(numpy.nanmin(mapFluxData)) ,  '{0:.2e}'.format(numpy.nanmax(mapFluxData)) ])
	cbar.outline.remove()
	ax.set_xlabel("Ra")
	ax.set_ylabel("Dec")
	cax.set_ylabel("$\mathrm{W/m^2/Sr}$")
	
	plt.savefig(fitsfile.replace(".fits",".png"))
	fig.clear()

def makeMapFluxFits(i):

	from scipy import optimize

	fileToWrite = i.replace("ProjectedCube","MapFlux")
	os.system("rm -rf "+fileToWrite)

	fits = pyfits.open(i)
	xsize = fits["Image"].header["NAXIS1"]
	ysize = fits["Image"].header["NAXIS2"]
	layer = fits["Image"].header["NAXIS3"]
	cubeHIPE = fits["Image"].data
	waveHIPE = fits["ImageIndex"].data.field(0)
	cdelt1 = abs(fits["Image"].header["CDELT1"])
	primaryHeaderCube = fits[0].header
	fits.close()

	mapFlux = cubeHIPE[int(layer/2),:,:]*0

	wlth = getWavelength(i)
	keep = (max(waveHIPE)-min(waveHIPE))/4.
	wmin = wlth - keep
	wmax = wlth + keep

	tronc = int(0.10*len(waveHIPE))
	wave = waveHIPE[tronc:-1*tronc]

	print wave[0], wmin, wave[-1], wmax

	idxFlat = []
	idxRaie = []

	for i in xrange(len(wave)):
		if wave[i]<wmin or wave[i]>wmax:
			
			idxFlat.append(i)
		else:
			idxRaie.append(i)

	for pixX in range(xsize):
		for pixY in range(ysize):

			spectrum = cubeHIPE[tronc:-1*tronc,pixY,pixX] * 299792458. / (waveHIPE[tronc:-1*tronc]**2)

			idxNan = numpy.where(spectrum != spectrum)[0]
			if len(idxNan)==len(wave):
				
				mapFlux[pixY,pixX] = numpy.nan
				continue

			for i in idxNan:
				if i in idxFlat:
					idxFlat.remove(i)

			poly = numpy.poly1d(numpy.polyfit(wave[idxFlat],spectrum[idxFlat],1))
			spectrumFlat = spectrum - poly(wave)
			spectrumMax = numpy.nanmax(spectrumFlat[idxRaie])
			lambdaMax = wave[numpy.where(spectrumFlat == spectrumMax)[0][0]]
			raieFit = lambda p, x: p[0]*numpy.exp( -0.5*((x-p[1])/p[2])**2 ) # Target function
			errfunc = lambda p, x, y: raieFit(p, x) - y  # Distance to the target function
			p0 = [ spectrumMax,lambdaMax,0.05 ] # Initial guess for the parameters
			p, success = optimize.leastsq(errfunc, p0, args=(wave, spectrumFlat))

			#integration Flux
			if (p[1]>wmin) and (p[1]<wmax) and p[0]>0:
				
				mapFlux[pixY,pixX] = numpy.trapz(raieFit(p,wave)[idxRaie])
			else:
				mapFlux[pixY,pixX] = numpy.trapz(spectrumFlat[idxRaie]+min(spectrumFlat[idxRaie]))

	#change unit
	cdelt1 = cdelt1*3600.
	pixelToSr = (1/cdelt1**2) * (1/((3600.*180.)/numpy.pi))**2
	mapFlux = mapFlux*pixelToSr
	#pixel_sr = pixel_dx^2. * 2.3504e-11

	#Ecriture du fits
	cardList = []
	cardList.append(pyfits.Card("SIMPLE", "T"))
	#cardList.append(pyfits.Card("EXTNAME", "IMAGE"))
	cardList.append(pyfits.Card("NAXIS", 2))
	listMETA = ["NAXIS1","NAXIS2","CRPIX1","CRPIX2","CRVAL1","CRVAL2","CDELT1","CDELT2","CTYPE1","CTYPE2"]
	for META in listMETA:
		try:
			cardList.append(pyfits.Card(META, fits["Image"].header[META]))
		except:
			print META, "Copy Error"
	cardList.append(pyfits.Card("QTTY", "W/m^2/Sr"))

	hdulist = pyfits.HDUList()
	PrimaryHDU = pyfits.PrimaryHDU(header=primaryHeaderCube)
	hdulist.append(PrimaryHDU)
	headerMapFlux = pyfits.Header(cards=cardList)
	mapFluxHDU = pyfits.ImageHDU(data=mapFlux, header=headerMapFlux, name="MapFlux")
	hdulist.append(mapFluxHDU)
	hdulist.writeto(fileToWrite)


def makeThumbMap(fitsfile,makePNG=True):
	
	import warnings
	warnings.filterwarnings('ignore')
	
	fits = pyfits.open(fitsfile,mode='update')
	primaryHeader = fits['Primary'].header

	#DATA IMAGE
	try:
		if fitsfile.count("SCANA")==0 or fitsfile.count("Scana")==0:
			fits_data = fits[1].data
		else: fits_data = fits["PrimaryImage"].data[0]
	except: 
		print "FAIL Image Data"

	#METADATA INSTRU
	try:
		if fitsfile.count("SANEPIC")>0:
			instru = fits["Image"].header["INSTRUME"]
		else: 
			instru = primaryHeader["INSTRUME"]
			creator = primaryHeader["CREATOR"]
		if fitsfile.count("SANEPIC")==0:
			desc = primaryHeader["DESC"]#TODO redundant. Change that when I can get the script to work.
		if instru.count("SPIRE")>0 and fitsfile.count("SANEPIC")==0:
			wave = primaryHeader["WAVELNTH"]
	except: 
		print "FAIL Metadata instru/desc/wave"

# Pas de obsids dans les fichiers Supreme Photo
	if (instru.count("SPIRE")>0 or instru.count("PACS")>0) and creator.count("Supreme")==0:
		
		try:
			obsids_str = ""
			obsids = fits['obsids'].data
			for i in reversed(range(len(obsids))):
				obsid = int(str(obsids[i]).replace("(","").replace(")",""))
				obsids_str = obsids_str + str(obsid) + " "
				primaryHeader.update('OBSID'+("%03d"%(i+1)),value=obsid,comment="Observation Identifier")
				primaryHeader.update('OBSIDS',value=obsids_str,comment="Observations Identifier")
		except:
			primaryHeader.update('OBSID001',value=primaryHeader["OBS_ID"],comment="Observation Identifier")
			primaryHeader.update('OBSIDS',value=primaryHeader["OBS_ID"],comment="Observations Identifier")
	
	if fitsfile.count("_psrcPS")==0 and fitsfile.count("_extdPS")==0:
		
		primaryHeader.update('ORIGIN',value="IDOC @ Institut d'Astrophysique Spatiale",comment="Site that created this product",after='DESC')
	else:
		primaryHeader.update('ORIGIN',value="Herschel Science Archive",comment="Site that created this product",after='DESC')
	fits.close()

	if fitsfile.count("PSW")>0 or fitsfile.count("Red")>0 or fitsfile.count("red")>0:
		color = "red"
	if fitsfile.count("PLW")>0 or fitsfile.count("Green")>0 or fitsfile.count("green")>0:
		color = "green"
	if fitsfile.count("PMW")>0 or fitsfile.count("Blue")>0 or fitsfile.count("blue")>0:
		color = "blue"

	if makePNG:
		
		filename = fitsfile[:-5]
		makeImageFromData(fits_data,color,filename)
		print filename

def makeImageFromData(fits_data,color,filename,thumbnailSize=100.):
	
	import Image,ImageDraw, io
	import matplotlib.pyplot as plt
	import matplotlib.cm as cm
	
	colors ={"red":cm.Reds_r,"green":cm.Greens_r,"blue":cm.Blues_r}
	
	#Remove NaNs
	fits_data[numpy.isnan(fits_data)] = 0
	(ysize, xsize) = fits_data.shape
	imTest = Image.fromarray(fits_data)
	
	#Cropping
	cropBox = imTest.getbbox()
	width = abs(cropBox[2]-cropBox[0])
	height = abs(cropBox[3]-cropBox[1])
	cropBox = (cropBox[0],ysize-cropBox[1]-height,cropBox[2],ysize-cropBox[3]+height)
	fits_data[fits_data==0] = numpy.nan

	try: vminZ, vmaxZ = zscale(fits_data)
	except: vminZ, vmaxZ = numpy.nanmin(fits_data), numpy.nanmax(fits_data)
	
	#Fullsize
	buff = io.BytesIO()
	plt.imsave(buff, fits_data, vmin=vminZ, vmax=vmaxZ, cmap="gray", origin='lower', format='png')
	buff.seek(0)

	im = Image.open(buff)
	im = im.crop(cropBox)
	im.save(filename+"_fullsize.png")
	buff.close()

	#Thumbnail
	buff = io.BytesIO()
	plt.imsave(buff, fits_data, vmin=vminZ, vmax=vmaxZ, cmap=colors[color], origin='lower', format='png')
	buff.seek(0)

	im = Image.open(buff)
	im = im.crop(cropBox)

	xsize, ysize = im.size
	rap = xsize / thumbnailSize
	size = int(xsize/rap) , int(ysize/rap)
	im.thumbnail(size, Image.ANTIALIAS)
	im.save(filename+".png")
	buff.close()


def makeThumbCube_old(fitsfile,dicflag):

	import ImageDraw

	fits = pyfits.open(fitsfile)

	xsize = fits["Image"].header["NAXIS1"]
	ysize = fits["Image"].header["NAXIS2"]
	layer = fits["Image"].header["NAXIS3"]
	fitsDataCube = fits["Image"].data
	instru = fits["Primary"].header["INSTRUME"]
	creator = fits["Primary"].header["CREATOR"]

	#if creator.count("Gridding")>0:
	
	try :
		waveRef  = fits["Image"].header["CRPIX3"]
		waveOrig = fits["Image"].header["CRVAL3"]
		waveDelt = fits["Image"].header["CDELT3"]
		waveOrigRef = waveOrig - (waveRef)*waveDelt
		wave = waveOrigRef + numpy.arange(layer)*waveDelt

	#else:
	except:
		wave = fits["ImageIndex"].data.field(0)

	#if instru=="SPIRE":
	
		#wave = (299792458/(wave[:]*10**9))*10**6
	#print wave[0], wave[-1], "largeur ", wave[0]-wave[-1]

	#aor = fits["Primary"].header["AOR"]
	aor =""
	obsid = fits["Primary"].header["OBS_ID"]
	obj = fits["Primary"].header["OBJECT"]

	instru = fits["Primary"].header["INSTRUME"]

	if instru=="PACS":
		
		desc = fits["Primary"].header["DESC"]
		band = fits["Primary"].header["BAND"]

		if desc.count("Blue")>0 or band.count("Blue")>0:
			
			color = cm.Blues_r
			colorPlot = "b-"
			colorFont = "b"
			colorOutline = "Red"
			colorLayer = "r-"

		if desc.count("Red")>0 or band.count("Red")>0 or fitsfile.count("red")>0:
			
			color = cm.Reds_r
			colorPlot = "r-"
			colorFont = "r"
			colorOutline = "Blue"
			colorLayer = "b-"

	if instru=="SPIRE":
		
		color = cm.Oranges_r
		colorPlot = "k-"
		colorFont = "k"
		colorOutline = "Black"

	#finally:
	fits.close()

	#############################
	#############################
	#############################

	fitsDataImage = fitsDataCube[int(layer/2),:,:]

	if instru=="PACS":
		
		try: vminZ, vmaxZ = zscale(fitsDataImage)
		except: vminZ, vmaxZ = numpy.nanmin(fitsDataImage), numpy.nanmax(fitsDataImage)

	if instru=="SPIRE":
		
		try: vminZ, vmaxZ = zscale(fitsDataImage)
		except: vminZ, vmaxZ = numpy.nanmin(fitsDataImage), numpy.nanmax(fitsDataImage)

	filename = fitsfile[:len(fitsfile)-5]
	plt.imsave(filename+".png", fitsDataImage, vmin=vminZ, vmax=vmaxZ, cmap=color, origin='lower')
	im = Image.open(filename+".png")

	rap = ysize / 400.
	thumbW = int(xsize/rap)
	thumbH = int(ysize/rap)
	size = thumbW , thumbH 

	imNew = im.resize(size)

	repChamp = fitsfile.split( fitsfile.split("/")[-1] )[0]
	repChamp = repChamp[:len(repChamp)-1]

	if instru=="PACS" and fitsfile.count("OT2")==0 and len(dicflag)>0:
		
	
		champAvecCII = dicflag[repChamp][0]
	
		if (champAvecCII==False) or (champAvecCII==True and fitsfile.count("_CII_")==1) or (champAvecCII==True and fitsfile.count("blue")==1):
			
			brightPix = numpy.where(fitsDataImage == numpy.nanmax(fitsDataImage))
			briX = brightPix[0][0]
			briY = brightPix[1][0]

		if (champAvecCII==True and fitsfile.count("_CII_")==1):
			dicflag[repChamp][1] = (briX,briY)
		if ( champAvecCII==True and fitsfile.count("_CII_")==0  and fitsfile.count("red")==1 ):
			(briX,briY) = dicflag[repChamp][1]

	if instru=="SPIRE" or fitsfile.count("OT2")>0 or len(dicflag)==0:
		
		brightPix = numpy.where(fitsDataImage == numpy.nanmax(fitsDataImage))
		briX = brightPix[0][0]
		briY = brightPix[1][0]

	briXt = briY+1
	briYt = ysize - (briX+1)

	top = (int(briXt/rap),int(briYt/rap))
	top2 = (int(briXt/rap)+1,int(briYt/rap)-1)
	bot = (int(briXt/rap-1/rap),int(briYt/rap+1/rap))
	bot2 = (int(briXt/rap-1/rap)-1,int(briYt/rap+1/rap)+1)

	draw = ImageDraw.Draw(imNew)
	draw.rectangle( (top,bot), outline=colorOutline)
	draw.rectangle( (top2,bot2), outline=colorOutline)
	del draw

	fig = plt.figure(1)
	titre = fitsfile.split("/")[-1]

	if instru=="PACS":
		
		if titre.count("Deg")>0:
			
			titre = titre.replace("Cube","")
			titre = titre.replace(".fits","")
			titre = titre.replace("deglitch","")
			raie = titre.split("_")[-2]
		else: 
			titre = titre.replace("_Cube.fits","")
			raie = titre.split("_")[-1]
		titre = titre.replace("L2","")
		champ = titre.split("_PACS-Spectro")[0]
		titre = titre.replace("_"," ")
		plt.title(titre+"\nPreview at "+str(wave[int(layer/2)])[:6]+"$\mathrm{\mu m}$.  Spectrum for pixel ("+str(briY)+","+str(briX)+")")
		plt.xlabel("Wavelength ( $\mathrm{\mu m}$ )")
		plt.ylabel("Flux ( $\mathrm{W/m2/Hz/sr}$ )")      

	if instru=="SPIRE":
		
		titre = titre.replace("_cube.fits","")
		champ = ""
		raie = ""
		titre = titre.replace("_"," ")
		plt.title(titre+"\nPreview at "+str(wave[int(layer/2)])[:6]+"GHz.  Spectrum for pixel ("+str(briY)+","+str(briX)+")")
		plt.xlabel("Frequency ( Ghz )")
		plt.ylabel("Flux ( W/m2/Hz/sr )")
		
	#print champ, raie

	minS = numpy.nanmin(fitsDataCube[:,briX,briY])
	maxS = numpy.nanmax(fitsDataCube[:,briX,briY])

	plt.axis([min(wave), max(wave), minS - 0.1*(maxS-minS), maxS + 0.1*(maxS-minS)])
	ax = plt.gca()
	ax.set_autoscale_on(False)

	plt.plot([min(wave),max(wave)],[0,0],colorPlot,linewidth=1)
	plt.plot(wave,fitsDataCube[:,briX,briY],colorPlot,linewidth=2)
	plt.annotate(champ, xy=((max(wave)+min(wave))/2,0), xytext=(0.01,0.95), xycoords='data',textcoords='axes fraction', arrowprops=None, size='large', color=colorFont)
	plt.annotate(raie, xy=((max(wave)+min(wave))/2,0), xytext=(0.95,0.95), xycoords='data',textcoords='axes fraction', arrowprops=None, size='large', color=colorFont)

	#fig.savefig(filename+"_Spectrum.png")
	imPlot = fig2img(fig)
	fig.clear()

	#imPlot = Image.open(filename+"_Spectrum.png") 
	plotH = 400
	plotW = int(800/1.5)+1
	size = plotW, plotH
	imPlot.thumbnail(size, Image.ANTIALIAS)
	#imPlot.save(filename+"_Spectrum.png")

	#print imNew.size, imPlot.size

	size = thumbW + plotW, thumbH
	comb = Image.new(imPlot.mode, size)
	comb.paste(imNew,(0,0))
	comb.paste(imPlot,(thumbW,0))
	comb.save(filename+".png")


def makeThumbCubePACS_old(fitsfile):

	from scipy import optimize

	fits = pyfits.open(fitsfile)

	xsize = fits["Image"].header["NAXIS1"]
	ysize = fits["Image"].header["NAXIS2"]
	layer = fits["Image"].header["NAXIS3"]
	instru = fits["Primary"].header["INSTRUME"]
	creator = fits["Primary"].header["CREATOR"]
	obsid = fits["Primary"].header["OBS_ID"]
	obj = fits["Primary"].header["OBJECT"]
	instru = fits["Primary"].header["INSTRUME"]
	desc = fits["Primary"].header["DESC"]
	band = fits["Primary"].header["BAND"]

	fitsDataCubeHIPE = fits["Image"].data
	waveHIPE = fits["ImageIndex"].data.field(0)

	if desc.count("Blue")>0 or band.count("Blue")>0 or fitsfile.count("blue")>0:
		
		color = cm.Blues_r
		colorHTML = "#1D7CF2"
	if desc.count("Red")>0 or band.count("Red")>0 or fitsfile.count("red")>0:
		
		color = cm.Reds_r
		colorHTML = "#ff0000"

	#finally:
	fits.close()


	###################################
	makeImage = True
	if makeImage:
		
	
		mapFlux = fitsDataCubeHIPE[0,:,:]*0

		wlth = getWavelength(fitsfile)
		keep = (max(waveHIPE)-min(waveHIPE))/4.
		wmin = wlth - keep
		wmax = wlth + keep
	
		tronc = int(0.10*len(waveHIPE))
		wave = waveHIPE[tronc:-1*tronc]
		fitsDataCube = fitsDataCubeHIPE[tronc:-1*tronc,:,:]

		#print wmin, wmax, wave[0], wave[-1]

		idxFlat = []
		idxRaie = []

		for i in xrange(len(wave)):
			if wave[i]<wmin or wave[i]>wmax:
				idxFlat.append(i)
			else: idxRaie.append(i)

		for pixX in range(xsize):
			for pixY in range(ysize):

				#conversion
				spectrum = fitsDataCube[:,pixY,pixX] * 299792458. / (wave**2)
				
				idxNan = numpy.where(spectrum != spectrum)[0]
				if len(idxNan)==len(wave):
					
					mapFlux[pixY,pixX] = numpy.nan
					continue

				for i in idxNan:
					if i in idxFlat:
						idxFlat.remove(i)

				#substract baseline
				poly = numpy.poly1d(numpy.polyfit(wave[idxFlat],spectrum[idxFlat],1))
				spectrumFlat = spectrum - poly(wave)
				
				#flux integration
				spectrumMax = numpy.nanmax(spectrumFlat[idxRaie])
				lambdaMax = wave[numpy.where(spectrumFlat == spectrumMax)[0][0]]
				raieFit = lambda p, x: p[0]*numpy.exp( -0.5*((x-p[1])/p[2])**2 ) +p[3]*x# Target function
				errfunc = lambda p, x, y: raieFit(p, x) - y  # Distance to the target function
				p0 = [ spectrumMax,lambdaMax,0.05, spectrumFlat[-1]-spectrumFlat[0]] # Initial guess for the parameters
				p, success = optimize.leastsq(errfunc, p0, args=(wave, spectrumFlat))
				if (p[1]>wmin) and (p[1]<wmax) and p[0]>0:
					
					mapFlux[pixY,pixX] = numpy.trapz(raieFit(p,wave)[idxRaie])
				else:
					mapFlux[pixY,pixX] = 0#numpy.trapz(spectrumFlat[idxRaie]+min(spectrumFlat[idxRaie]))

		###################################

		buff = io.BytesIO()
		plt.imsave(buff, mapFlux, origin='lower')
		buff.seek(0)
		
		#thumbnail
		im = Image.open(buff)
		rap = xsize / 500.
		thumbW = int(xsize/rap)
		thumbH = int(ysize/rap)
		size = thumbW , thumbH
		imNew = im.resize(size)
		filename = fitsfile.replace(".fits",".png")
		imNew.save(filename)
		buff.close()

	makeCubeExplorer(fitsfile,waveHIPE,fitsDataCubeHIPE,obj,obsid,xsize,ysize,layer,"PACS")
	
	print fitsfile,"Done !"

def makeThumbCubePACS(fitsfile):

	from scipy import optimize

	fits = pyfits.open(fitsfile)
	
	# Primary is present everywhere
	instru = fits["Primary"].header["INSTRUME"]
	creator = fits["Primary"].header["CREATOR"]
	obsid = fits["Primary"].header["OBS_ID"]
	obj = fits["Primary"].header["OBJECT"]
	instru = fits["Primary"].header["INSTRUME"]
	desc = fits["Primary"].header["DESC"]
	band = fits["Primary"].header["BAND"]
	
	# There's two types of .fits files. Thoses with "Image" and thoses with "flux" and "waves"
	try:
		tmp = fits.index_of("Image") # If we get an error, the Image extension doesn't exist
		data_extension = "Image"
		
		xsize = fits["Image"].header["NAXIS1"]
		ysize = fits["Image"].header["NAXIS2"]
		layer = fits["Image"].header["NAXIS3"] # Number of wavelenghts
		cdelt1 = abs(fits["Image"].header["CDELT1"]) # Pixel scale axis 1 (angle)

		fitsDataCubeHIPE = fits["Image"].data # Jy/pixel
		try:
			fitsDataWaveHIPE = fits["ImageIndex"].data.field(0)
		except KeyError: # If key ImageIndex doesn't exist, we do this
			#~ pdb.set_trace()
			waveRef  = fits["Image"].header["CRPIX3"] # WCS: Reference pixel position axis 3
			waveOrig = fits["Image"].header["CRVAL3"] # WCS: Third coordinate of reference pixel
			waveDelt = fits["Image"].header["CDELT3"] # WCS: Pixel scale axis 3
			
			# First wavelength of the 1st axis
			waveOrigRef = waveOrig - (waveRef) * waveDelt
			
			# List of wavelength corresponding to the 3rd axis
			fitsDataWaveHIPE = waveOrigRef + numpy.arange(layer) * waveDelt

		if desc.count("Blue")>0 or band.count("Blue")>0 or fitsfile.count("blue")>0:
			
			color = cm.Blues_r
			colorHTML = "#1D7CF2"
		if desc.count("Red")>0 or band.count("Red")>0 or fitsfile.count("red")>0:
			
			color = cm.Reds_r
			colorHTML = "#ff0000"
		
		trunc = int(0.10*layer) # Trunc is 1/10th of the number of layer
		# Eliminate border ID (left and right). We truncate 'trunc' numbers at the start and end of the array
		CubeHIPE = fitsDataCubeHIPE[trunc:(-1)*trunc,:,:]
		waveHIPE = fitsDataWaveHIPE[trunc:(-1)*trunc]

		wlth = getWavelength(fitsfile)
		
	except:
		CubeHIPE = fits["flux"].data # Jy/pixel
		waveHIPE = fits["wave"].data
		# I know this is now correct strictly speaking but this is just to provide an image, even an awfull one.
		waveHIPE = waveHIPE[:,0,0] # We only use the list of the first pixel. 
		
		(layer, xsize, ysize) = CubeHIPE.shape
		
		wlth = -1 # for to get in the else of the next section when making the image
	#finally:
	
	fits.close()
	


	###################################
	# Making the image
		
	
	# For some reason, metadata are wrong sometimes and give a 
	# wavelength transition that isn't even in the wavelenght range of the dataset
	# In this case, we retrieve the wavelenght corresponding to the max flux of the "mean" pixel
	if (wlth > waveHIPE.min() and wlth < waveHIPE.max()):
	
		keep = (max(waveHIPE)-min(waveHIPE))/5.
		wmin, wmax = wlth - keep, wlth + keep

		def spectrumFit(x, a, b, c, d, e): 
			return a*numpy.exp( -0.5*((x-b)/c)**2 ) + d*x + e
		
		raieFit = lambda p, x: p[0]*numpy.exp( -0.5*((x-p[1])/p[2])**2 ) +p[3]*x + p[4]# Target function
		raieFlat = lambda p, x: p[0]*numpy.exp( -0.5*((x-p[1])/p[2])**2 )
		errfunc = lambda p, x, y: raieFlat(p, x) - y  # Distance to the target function
		
		test = 3
		plot = False
		
		mapFlux = CubeHIPE[0,:,:]*numpy.nan
		
		# NaN is the only value not equal to itself. Thus, the following line return the index list of finite values
		#~ NotNaN = numpy.where(fitsDataCubeHIPE[int(layer/2),:,:]==fitsDataCubeHIPE[int(layer/2),:,:])
		NotNaN = numpy.where(numpy.isfinite(fitsDataCubeHIPE[int(layer/2),:,:]))
		for (pixY,pixX) in zip(NotNaN[0],NotNaN[1]):

			spectrum = CubeHIPE[:,pixY,pixX] + 0
			idx_NaN = list(numpy.where(numpy.isnan(spectrum))[0])
			#x = x[numpy.logical_not(numpy.isnan(x))]
			
			if len(idx_NaN)>int(0.1*len(spectrum)):
				continue
			
			#conversion
			spectrum = CubeHIPE[:,pixY,pixX] * 299792458. / (waveHIPE**2)

			#idx_NaN = numpy.asarray(idx_NaN)
			spectrum = numpy.delete(spectrum,idx_NaN)
			wave = numpy.delete(waveHIPE,idx_NaN)
			idxFlat = list(numpy.where((wave<wmin)|(wave>wmax))[0])
			idxRaie = list(numpy.where((wave>wmin)&(wave<wmax))[0])
			
			poly = numpy.poly1d(numpy.polyfit(wave[idxFlat],spectrum[idxFlat],1))
			spectrumFlat = spectrum - poly(wave)
		
			#flux fitting and integration
			try:
				spectrumMax = numpy.max(spectrumFlat[idxRaie])
			except:
				pdb.set_trace()
			lambdaMax = wave[numpy.where(spectrumFlat == spectrumMax)[0][0]]
			p0 = [ spectrumMax,lambdaMax,0.05,0,0] # Initial guess for the parameters
			p, success = optimize.leastsq(errfunc, p0, args=(wave, spectrumFlat))
			#p, pcov = optimize.curve_fit(spectrumFit, wave, spectrum, p0)
			if (p[1]>wmin) and (p[1]<wmax) and p[0]>0:
				
				mapFlux[pixY,pixX] = numpy.trapz(raieFit(p,wave)[idxRaie])
			else:
				mapFlux[pixY,pixX] = 0#numpy.trapz(spectrumFlat[idxRaie]+min(spectrumFlat[idxRaie]))

		cdelt1 = cdelt1*3600.
		pixelToSr = (1/cdelt1**2) * (1/((3600.*180.)/numpy.pi))**2
		mapFlux = mapFlux*pixelToSr
		
		#plt.imshow(mapFlux, interpolation='none')
		
		if plot:
			plt.show()
		
		###################################
	else:
		# No clear spectral line as reference
		
		# There might be some NaN but they'll be ignored by ptp. Replacing 
		# NaN by 0 could instead lead to errors, forcing ptp to compare 
		# maximum to 0 values instead of the minimum that will be masked that way.
		mapFlux = CubeHIPE.ptp(axis=(0)) # Axis 0 are the frequencies
		
		#We don't convert NaN to 0. this allow transparent pixels in the map, if needed. The thumbnails are nicer this way.
		
	#Create map for integrated flux
	buff = io.BytesIO()
	plt.imsave(buff, mapFlux, origin='lower')
	buff.seek(0)
	
	im = Image.open(buff)
	rap = xsize / 500.
	thumbW = int(xsize/rap)
	thumbH = int(ysize/rap)
	size = thumbW , thumbH
	imNew = im.resize(size)
	filename = fitsfile.replace(".fits",".png")
	imNew.save(filename)
	buff.close()
	
	#print fitsDataWaveHIPE.shape, fitsDataCubeHIPE.shape
	#makeCubeExplorer(fitsfile,fitsDataWaveHIPE,fitsDataCubeHIPE,obj,obsid,xsize,ysize,layer,thumbH, colorHTML,"PACS")
	
	print fitsfile,"Done !"


def makeThumbCubeSPIRE(fitsfile):

	fits = pyfits.open(fitsfile)
	print fitsfile

	xsize = fits["Image"].header["NAXIS1"]
	ysize = fits["Image"].header["NAXIS2"]
	layer = fits["Image"].header["NAXIS3"]
	instru = fits["Primary"].header["INSTRUME"]
	creator = fits["Primary"].header["CREATOR"]
	#obsid = fits["Primary"].header["OBS_ID"]
	obj = fits["Primary"].header["OBJECT"]

	fitsDataCube = fits["Image"].data
	#if creator.count("Gridding")>0 or creator.count("SUPREME")>0 or creator.count("Supreme")>0:
	
	try:
		waveRef  = fits["Image"].header["CRPIX3"]
		waveOrig = fits["Image"].header["CRVAL3"]
		waveDelt = fits["Image"].header["CDELT3"]
		waveOrigRef = waveOrig - (waveRef)*waveDelt
		waveHIPE = waveOrigRef + numpy.arange(layer)*waveDelt

	#else:
	except:
		waveHIPE = fits["ImageIndex"].data.field(0)

	color = cm.Oranges_r
	colorHTML = "#000000"

	#finally:
	fits.close()

	fitsDataImage = fitsDataCube[int(layer/2),:,:]
	try: vminZ, vmaxZ = zscale(fitsDataImage)
	except: vminZ, vmaxZ = numpy.nanmin(fitsDataImage), numpy.nanmax(fitsDataImage)

	vmaxZ = vmaxZ*1.00001 # cheat to avoid darkest for science brighter one

	filename = fitsfile[:len(fitsfile)-5]
#	plt.imsave(filename+".png", fitsDataImage, vmin=vminZ, vmax=vmaxZ, cmap=color, origin='lower')
#	im = Image.open(filename+".png")
	buff = io.BytesIO()
	plt.imsave(buff, fitsDataImage, vmin=vminZ, vmax=vmaxZ, cmap=color, origin='lower')
	buff.seek(0)
	im = Image.open(buff)

	rap = xsize / 500.
	thumbW = int(xsize/rap)
	thumbH = int(ysize/rap)
	size = thumbW , thumbH

	imNew = im.resize(size)
	imNew.save(filename+".png")
	
	buff.close()
	
	#makeCubeExplorer(fitsfile,waveHIPE,fitsDataCube,obj,obsid,xsize,ysize,layer,thumbH,colorHTML,"SPIRE")
	
	print fitsfile,"Done !"


def makeCubeExplorer(fitsfile,waveHIPE,fitsDataCubeHIPE,obj,obsid,xsize,ysize,layer,thumbH,colorHTML,instru):

	import json
	from collections import OrderedDict
	
	if instru =="SPIRE":
		
		from json import encoder
		encoder.FLOAT_REPR = lambda o: format(o, '.5E')
		truncG, truncD = 0, -1
		line = ""
	
	if instru=="PACS":
		
		truncG, truncD = 3, -5
		try: 
			line = getLine(fitsfile)
		except:
			line = ""
	
	
	waveJSON = waveHIPE[truncG:truncD]
	cube = fitsDataCubeHIPE[truncG:truncD,:,:]
	
	dataForJSON = OrderedDict()
	dataForJSON["OBJECT"] = obj
	dataForJSON["OBSID"] = obsid
	dataForJSON["NAXIS1"] = xsize
	dataForJSON["NAXIS2"] = ysize
	dataForJSON["NAXIS3"] = layer
	waveJSON = numpy.around(waveJSON, decimals=2)
	dataForJSON["WAVE"] = waveJSON.tolist()
	
	jsonArray = numpy.empty((xsize,ysize,len(waveJSON)))
	
	for x in range(xsize):
		for y in range(ysize):
			jsonArray[x,y,:] = cube[:,y,x] + 0

	if instru=="PACS":
		spectrumJSON = numpy.around(jsonArray, decimals=2)
	if instru=="SPIRE":
		spectrumJSON = jsonArray + 0
	spectrumJSON[numpy.isnan(spectrumJSON)] = 0
	dataForJSON["SPECTRUM"] = spectrumJSON.tolist()
	filename = fitsfile[:len(fitsfile)-5]
	with open(filename+".json", 'w') as f: json.dump(dataForJSON,f,indent=4)
	
	template = "/data/glx-herschel/data1/herschel/scriptsIAS/HidDatabase/templateCubeExplorer_"+instru+".html"
	tmp = open(template, 'r')
	htmlString = tmp.read()
	tmp.close()

	filenameFits = fitsfile.split("/")[-1].replace(".fits","")
	filenameHtml = fitsfile.replace(".fits",".html")
	
	fichier = open(filenameHtml, "w")
	htmlString_i = htmlString.replace("OBJECT",obj.replace(" ",""))
	htmlString_i = htmlString_i.replace("OBSID",str(obsid))
	htmlString_i = htmlString_i.replace("LINE",line)
	htmlString_i = htmlString_i.replace("FILENAME",filenameFits)
	htmlString_i = htmlString_i.replace("HEIGHT",str(thumbH))
	htmlString_i = htmlString_i.replace("COLORPLOT",colorHTML)
	if instru=="SPIRE":
		htmlString_i = htmlString_i.replace("FACTOR","19")
	fichier.write(htmlString_i)
	fichier.close()


def getLine(i):
	if i.count("157.67_CII")>0:
		return "CII157"
	if i.count("145.52_OI")>0:
		return "OI145"
	if i.count("121.9_NII")>0:
		return "NII122"
	if i.count("63.18_OI")>0:
		return "OI63"
	if i.count("119.855_CH")>0:
		return "CH119"
	if i.count("137.2_CO")>0:
		return "CO137"
	if i.count("84.5_OH")>0:
		return "OH84"
	if i.count("157.741_CIICplus")>0:
		return "CIICplus"


def getWavelength(fitsfile):
	"""Sometimes, like in the case of the following file, the line doesn't correspond AT ALL to the effective wavelength range. And thus, you're in trouble.
	Shit happens."""
	
	if fitsfile.count("157.67_CII")>0:
		return 157.75
	if fitsfile.count("63.18_OI")>0:
		return 63.189
	if fitsfile.count("145.52_OI")>0:
		return 145.548
	if fitsfile.count("121.9_NII")>0:
		return 121.9
	if fitsfile.count("119.855_CHplus")>0:
		return 119.855
	if fitsfile.count("137.2_CO")>0:
		return 137.2
	if fitsfile.count("84.5_OH")>0:
		return 84.5
	if fitsfile.count("157.741_CIICplus")>0:
		return 157.7


def generatePACSman(i):

	template = "/data/glx-herschel/data1/herschel/Release/R1_pacs_spectro/PACSman_Fits/templatePreview.html"
	tmp = open(template, 'r')
	htmlString = tmp.read()
	tmp.close()

	champ = i.split("/")[-3]
	line = i.split("/")[-2]
	filenameFits = i.split("/")[-1]
	matchObj = re.match( r'.*(\d{10}).*', filenameFits)
	obsid = str(matchObj.group(1))
	filenameHtml = i.replace(".fits",".html")
	
	fichier = open(filenameHtml, "w")
	htmlString_i = htmlString.replace("CHAMP",champ)
	htmlString_i = htmlString_i.replace("OBSID",obsid)
	htmlString_i = htmlString_i.replace("LINE",line)
	fichier.write(htmlString_i)
	fichier.close()
	
	makeTxtPACSman(i)


def makeTxtPACSman(fitsfile):

	fits = pyfits.open(fitsfile)
	try:
		mainHeader = fits[0].header
	finally:
		fits.close()

	filename = fitsfile[:len(fitsfile)-5]+".txt"
	fileToWrite = open(filename,"w")
	toWrite = "Main Header\n\n"+str(mainHeader)
	fileToWrite.write(toWrite)


def fig2data(fig):
	"""
	@brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
	@param fig a matplotlib figure
	@return a numpy 3D array of RGBA values
	"""
	# draw the renderer
	rect = fig.patch
	rect.set_facecolor('white')
	fig.canvas.draw ()

	# Get the RGBA buffer from the figure
	w,h = fig.canvas.get_width_height()
	buf = numpy.fromstring (fig.canvas.tostring_argb(),dtype=numpy.uint8)
	buf.shape = (w,h,4)

	# canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
	buf = numpy.roll (buf,3,axis = 2)
	return buf

def fig2img(fig):
	"""
	@brief Convert a Matplotlib figure to a PIL Image in RGBA format and return it
	@param fig a matplotlib figure
	@return a Python Imaging Library ( PIL ) image
	"""
	# put the figure pixmap into a numpy array
	buf = fig2data(fig )
	w,h,d = buf.shape
	return Image.fromstring("RGBA",(w,h),buf.tostring())


def makeThumbSpectrum(fitsfile):

	fits = pyfits.open(fitsfile)
	HDUs = numpy.array(fits.info(False))
	try:
		aor = ""
		obsid = fits["Primary"].header["OBS_ID"]
		obj = fits["Primary"].header["OBJECT"]

		try:
			SLWC3_wave, SLWC3_signal = fits["SLWC3"].data.field(0), fits["SLWC3"].data.field(1)
			slw = "SLWC3"
		except:
			SLW = ["SLWC3","SLWC2","SLWC4","SLWC1"]
			for slw in SLW:
				idx = numpy.where(HDUs[:,1]==slw)[0]
				print("SLW")
				if len(idx)>0:
					SLW_hdu = idx[0]
					break
			try:
				SLWC3_wave, SLWC3_signal = fits[SLW_hdu].data.field(0), fits[SLW_hdu].data.field(1)
			except:
				print fitsfile
			print slw

		try:
			SSWD4_wave, SSWD4_signal = fits["SSWD4"].data.field(0), fits["SSWD4"].data.field(1)
			ssw = "SSWD4"
		except:
			SSW = ["SSWD4","SSWD2","SSWD3","SSWD1"]
			for ssw in SSW:
				idx = numpy.where(HDUs[:,1]==ssw)[0]
				if len(idx)>0:
					SSW_hdu = idx[0]
					break
			SSWD4_wave, SSWD4_signal = fits[SSW_hdu].data.field(0), fits[SSW_hdu].data.field(1)
			print ssw

	finally:
		fits.close()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nSpectrum for Bolometer "+slw)
	plt.xlabel("Frequency (GHz)")
	plt.ylabel("Flux (W/m2/Hz/sr)")
	plt.plot([min(SLWC3_wave),max(SLWC3_wave)],[0,0],"r-",linewidth=1)
	plt.plot(SLWC3_wave,SLWC3_signal,linewidth=1)

	imSLWC3 = fig2img(fig)
	fig.clear()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nSpectrum for Bolometer "+ssw)
	plt.xlabel("Frequency (GHz)")
	plt.ylabel("Flux (W/m2/Hz/sr)")
	plt.plot([min(SSWD4_wave),max(SSWD4_wave)],[0,0],"r-",linewidth=1)
	plt.plot(SSWD4_wave,SSWD4_signal,linewidth=1)

	imSSWD4 = fig2img(fig)
	fig.clear()

	plotH = 400
	plotW = int(800/1.5)+1
	size = plotW, plotH
	imSLWC3.thumbnail(size, Image.ANTIALIAS)
	imSSWD4.thumbnail(size, Image.ANTIALIAS)

	size = 2*plotW, plotH
	comb = Image.new(imSSWD4.mode,size)
	comb.paste(imSSWD4,(0,0))
	comb.paste(imSLWC3,(plotW,0))

	filename = fitsfile[:len(fitsfile)-5]
	comb.save(filename+".png")


def makeThumbSpectrum2d(fitsfile):

	fits = pyfits.open(fitsfile)

	try:
		aor = ""
		obsid = fits["spectrum"].header["OBS_ID"]
		obj = fits["spectrum"].header["OBJECT"]

		bolo = fits["spectrum"].data.field(7)	#v9 data.field(5) v7 data.field(0)
		flux = fits["spectrum"].data.field(1)	#v9 data.field(0) v7 data.field(1)
		wave = fits["spectrum"].data.field(0)	#v9 data.field(2) v7 data.field(2)

	except:
		
		try:   # from v13 spectrum2d instead of spetrum in spectrum2d fits
			aor = ""
                	obsid = fits[0].header["OBS_ID"]
                	obj = fits[0].header["OBJECT"]
                	bolo = fits["spectrum2d"].data.field(7)   #v9 data.field(5) v7 data.field(0)
                	flux = fits["spectrum2d"].data.field(1)   #v9 data.field(0) v7 data.field(1)
                	wave = fits["spectrum2d"].data.field(0)   #v9 data.field(2) v7 data.field(2)
		except:
			print("Problem with fits "+fitsfile)
			sys.exit()

	finally:
		fits.close()


	if str(bolo[0]).count("SLW")==1:
		boloType = "SLWC3"
	else: boloType = "SSWD4"

	bolo_idx = numpy.where(bolo == boloType)[0]

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nSpectrum2d for Bolometer "+boloType)
	plt.xlabel("Frequency (GHz)")
	plt.ylabel("Flux (W/m2/Hz/sr)")
	#for i in range(56):#SLWC3idx:
	plt.plot(wave[bolo_idx[0],:],flux[bolo_idx[0],:],"r-",linewidth=2)
	filename = fitsfile[:len(fitsfile)-5]    
	plt.savefig(filename+".png")
	fig.clear()


def makeThumbVoltage(fitsfile):

	fits = pyfits.open(fitsfile)

	try:
		#aor = fits["Spectrum"].header["AOR"]
		aor = ""
		obsid = fits[0].header["OBS_ID"]
		obj = fits[0].header["OBJECT"]

		timeSample = fits["Voltage"].data.field(0)
		SLWC3_voltage = fits["Voltage"].data.field("SLWC3")
		SSWD4_voltage = fits["Voltage"].data.field("SSWD4")

	finally:
		fits.close()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nVoltage V(t) for Bolometer SLWC3")
	plt.xlabel("Time")
	plt.ylabel("Voltage(V)")
	plt.plot([min(timeSample),max(timeSample)],[0,0],"r-",linewidth=1)
	plt.plot(timeSample,SLWC3_voltage,linewidth=1)

	imSLWC3 = fig2img(fig)
	fig.clear()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nVoltage V(t) for Bolometer SSWD4")
	plt.xlabel("Time")
	plt.ylabel("Voltage(V)")
	plt.plot([min(timeSample),max(timeSample)],[0,0],"r-",linewidth=1)
	plt.plot(timeSample,SSWD4_voltage,linewidth=1)

	imSSWD4 = fig2img(fig)
	fig.clear()

	plotH = 400
	plotW = int(800/1.5)+1
	size = plotW, plotH
	imSLWC3.thumbnail(size, Image.ANTIALIAS)
	imSSWD4.thumbnail(size, Image.ANTIALIAS)

	size = 2*plotW, plotH
	comb = Image.new(imSSWD4.mode,size)
	comb.paste(imSSWD4,(0,0))
	comb.paste(imSLWC3,(plotW,0))

	filename = fitsfile[:len(fitsfile)-5]
	comb.save(filename+".png")
	fig.clear()


def makeThumbInter(fitsfile):

	fits = pyfits.open(fitsfile)

	try:
		#aor = fits["Spectrum"].header["AOR"]
		aor = ""
		obsid = fits[0].header["OBS_ID"]
		obj = fits[0].header["OBJECT"]

		SLWC3_opd = fits[11].data.field("opd")
		SLWC3_sig = fits[11].data.field("signal")

		SSWD4_opd = fits[39].data.field("opd")
		SSWD4_sig = fits[39].data.field("signal")

	finally:
		fits.close()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nInterference for Bolometer SLWC3")
	plt.xlabel("Optical Path Difference (cm)")
	plt.ylabel("Signal (V)")
	plt.plot([min(SLWC3_opd),max(SLWC3_opd)],[0,0],"r-",linewidth=1)
	plt.plot(SLWC3_opd,SLWC3_sig,linewidth=1)

	imSLWC3 = fig2img(fig)
	fig.clear()

	fig = plt.figure(1)
	plt.title(str(obj)+" "+str(obsid)+" "+str(aor)+"\nInterference for Bolometer SSWD4")
	plt.xlabel("Optical Path Difference (cm)")
	plt.ylabel("Signal (V)")
	plt.plot([min(SSWD4_opd),max(SSWD4_opd)],[0,0],"r-",linewidth=1)
	plt.plot(SSWD4_opd,SSWD4_sig,linewidth=1)

	imSSWD4 = fig2img(fig)
	fig.clear()

	plotH = 400
	plotW = int(800/1.5)+1
	size = plotW, plotH
	imSLWC3.thumbnail(size, Image.ANTIALIAS)
	imSSWD4.thumbnail(size, Image.ANTIALIAS)

	size = 2*plotW, plotH
	comb = Image.new(imSSWD4.mode,size)
	comb.paste(imSSWD4,(0,0))
	comb.paste(imSLWC3,(plotW,0))

	filename = fitsfile[:len(fitsfile)-5]
	comb.save(filename+".png")
	fig.clear()


def makeHeader(fitsfile):

	fits = pyfits.open(fitsfile)
	try:
		mainHeader = fits[0].header
		imageHeader = fits[1].header
	finally:
		fits.close()
	filename = fitsfile[:len(fitsfile)-5]+".txt"
	fileToWrite = open(filename,"w")
	cardsMain, cards2nd = mainHeader.ascardlist(), imageHeader.ascardlist()
	mainStr, sndStr = "", ""
	for i in cardsMain: mainStr = mainStr+str(i)+"\n"
	for i in cards2nd: sndStr = sndStr+str(i)+"\n"
	#toWrite = "Main Header\n\n"+str(mainStr)+"\n\nSecondary Header\n\n"+str(sndStr)
	toWrite = "\n"+str(mainStr)+"\n"+str(sndStr)
	fileToWrite.write(toWrite)


def array2image(a):
	if a.typecode() == Numeric.UnsignedInt8:
		
		mode = "L"
	elif a.typecode() == Numeric.Float32:
	
		mode = "F"
	else:
		raise ValueError, "unsupported image mode"
	return Image.fromstring(mode, (a.shape[1], a.shape[0]), a.tostring())


def makeDico(cards):
	DICO = {}
	for card in cards:
		chaine_card=str(card)
		if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
			
			chaine_split = value=chaine_card.split("=")
			chaine_resplit = chaine_split[0].split()
			chaine_resplit = chaine_resplit[1].split(".")
			meta_des = chaine_split[1].strip()
			meta_key = chaine_resplit[1]
			DICO[meta_key] = meta_des
	return DICO


def getMainHeaderMetadata(hdr):

	METADATA = {}
	
	cards = hdr.ascardlist()
	DICO = makeDico(cards)
	#print sorted(DICO.iteritems())
	
	for card in cards:
		#print card
		chaine_card=str(card)
		if chaine_card.count("META")==0 and chaine_card.count("HIERARCH")==0 and chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")==1:
			
			#print chaine_card
			cle = chaine_card.split("=")[0]
			valeur = chaine_card.split("=")[1].split("/")[0]
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
			METADATA[cle] = valeur

		
		if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==0 and chaine_card.count("=")>=1:
			
			#print chaine_card
			if chaine_card.count("\n"):
				
				chaine_card = chaine_card.split("\n")[0]
				chaine_card = chaine_card.replace("&","")
			
			cle = chaine_card.split("=")[0]
			valeur = chaine_card.split("=")[1].split("/")[0]
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
			try: METADATA[DICO[cle].replace("'","").upper()] = valeur
			except: METADATA[DICO[cle].replace("'","").upper()] = "PROBLEM"
			
	return METADATA
	
