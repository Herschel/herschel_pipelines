--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: HID_glx; Type: DATABASE; Schema: -; Owner: sitools
--

CREATE DATABASE "HID_glx" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE "HID_glx" OWNER TO sitools;

\connect "HID_glx"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

--
-- Name: sbox; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sbox;


--
-- Name: sbox_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_in(cstring) RETURNS sbox
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_in';


ALTER FUNCTION public.sbox_in(cstring) OWNER TO sitools;

--
-- Name: sbox_out(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_out(sbox) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_out';


ALTER FUNCTION public.sbox_out(sbox) OWNER TO sitools;

--
-- Name: sbox; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sbox (
    INTERNALLENGTH = 32,
    INPUT = sbox_in,
    OUTPUT = sbox_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE sbox OWNER TO sitools;

--
-- Name: scircle; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE scircle;


--
-- Name: scircle_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_in(cstring) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_in';


ALTER FUNCTION public.scircle_in(cstring) OWNER TO sitools;

--
-- Name: scircle_out(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_out(scircle) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_out';


ALTER FUNCTION public.scircle_out(scircle) OWNER TO sitools;

--
-- Name: scircle; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE scircle (
    INTERNALLENGTH = 24,
    INPUT = scircle_in,
    OUTPUT = scircle_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE scircle OWNER TO sitools;

--
-- Name: sellipse; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sellipse;


--
-- Name: sellipse_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_in(cstring) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_in';


ALTER FUNCTION public.sellipse_in(cstring) OWNER TO sitools;

--
-- Name: sellipse_out(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_out(sellipse) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_out';


ALTER FUNCTION public.sellipse_out(sellipse) OWNER TO sitools;

--
-- Name: sellipse; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sellipse (
    INTERNALLENGTH = 40,
    INPUT = sellipse_in,
    OUTPUT = sellipse_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE sellipse OWNER TO sitools;

--
-- Name: sline; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sline;


--
-- Name: sline_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_in(cstring) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_in';


ALTER FUNCTION public.sline_in(cstring) OWNER TO sitools;

--
-- Name: sline_out(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_out(sline) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_out';


ALTER FUNCTION public.sline_out(sline) OWNER TO sitools;

--
-- Name: sline; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE sline (
    INTERNALLENGTH = 32,
    INPUT = sline_in,
    OUTPUT = sline_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE sline OWNER TO sitools;

--
-- Name: spath; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spath;


--
-- Name: spath_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_in(cstring) RETURNS spath
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_in';


ALTER FUNCTION public.spath_in(cstring) OWNER TO sitools;

--
-- Name: spath_out(spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_out(spath) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_out';


ALTER FUNCTION public.spath_out(spath) OWNER TO sitools;

--
-- Name: spath; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spath (
    INTERNALLENGTH = variable,
    INPUT = spath_in,
    OUTPUT = spath_out,
    ALIGNMENT = int4,
    STORAGE = external
);


ALTER TYPE spath OWNER TO sitools;

--
-- Name: spherekey; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spherekey;


--
-- Name: spherekey_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spherekey_in(cstring) RETURNS spherekey
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherekey_in';


ALTER FUNCTION public.spherekey_in(cstring) OWNER TO sitools;

--
-- Name: spherekey_out(spherekey); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spherekey_out(spherekey) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherekey_out';


ALTER FUNCTION public.spherekey_out(spherekey) OWNER TO sitools;

--
-- Name: spherekey; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spherekey (
    INTERNALLENGTH = 24,
    INPUT = spherekey_in,
    OUTPUT = spherekey_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE spherekey OWNER TO sitools;

--
-- Name: spoint; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spoint;


--
-- Name: spoint_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_in(cstring) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_in';


ALTER FUNCTION public.spoint_in(cstring) OWNER TO sitools;

--
-- Name: spoint_out(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_out(spoint) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_out';


ALTER FUNCTION public.spoint_out(spoint) OWNER TO sitools;

--
-- Name: spoint; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spoint (
    INTERNALLENGTH = 16,
    INPUT = spoint_in,
    OUTPUT = spoint_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE spoint OWNER TO sitools;

--
-- Name: spoly; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spoly;


--
-- Name: spoly_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_in(cstring) RETURNS spoly
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_in';


ALTER FUNCTION public.spoly_in(cstring) OWNER TO sitools;

--
-- Name: spoly_out(spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_out(spoly) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_out';


ALTER FUNCTION public.spoly_out(spoly) OWNER TO sitools;

--
-- Name: spoly; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE spoly (
    INTERNALLENGTH = variable,
    INPUT = spoly_in,
    OUTPUT = spoly_out,
    ALIGNMENT = int4,
    STORAGE = external
);


ALTER TYPE spoly OWNER TO sitools;

--
-- Name: strans; Type: SHELL TYPE; Schema: public; Owner: sitools
--

CREATE TYPE strans;


--
-- Name: strans_in(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_in(cstring) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_in';


ALTER FUNCTION public.strans_in(cstring) OWNER TO sitools;

--
-- Name: strans_out(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_out(strans) RETURNS cstring
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_out';


ALTER FUNCTION public.strans_out(strans) OWNER TO sitools;

--
-- Name: strans; Type: TYPE; Schema: public; Owner: sitools
--

CREATE TYPE strans (
    INTERNALLENGTH = 32,
    INPUT = strans_in,
    OUTPUT = strans_out,
    ALIGNMENT = int4,
    STORAGE = plain
);


ALTER TYPE strans OWNER TO sitools;

--
-- Name: area(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION area(sbox) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_area';


ALTER FUNCTION public.area(sbox) OWNER TO sitools;

--
-- Name: FUNCTION area(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION area(sbox) IS 'area of a spherical box';


--
-- Name: area(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION area(scircle) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_area';


ALTER FUNCTION public.area(scircle) OWNER TO sitools;

--
-- Name: FUNCTION area(scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION area(scircle) IS 'area of spherical circle';


--
-- Name: area(spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION area(spoly) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_area';


ALTER FUNCTION public.area(spoly) OWNER TO sitools;

--
-- Name: FUNCTION area(spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION area(spoly) IS 'returns area of spherical polygon';


--
-- Name: axes(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION axes(strans) RETURNS character
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_type';


ALTER FUNCTION public.axes(strans) OWNER TO sitools;

--
-- Name: FUNCTION axes(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION axes(strans) IS 'returns the axis of Euler angles of a transformation object';


--
-- Name: center(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION center(scircle) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_center';


ALTER FUNCTION public.center(scircle) OWNER TO sitools;

--
-- Name: FUNCTION center(scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION center(scircle) IS 'center of spherical circle';


--
-- Name: center(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION center(sellipse) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_center';


ALTER FUNCTION public.center(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION center(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION center(sellipse) IS 'center of spherical ellipse';


--
-- Name: circum(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION circum(sbox) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_circ';


ALTER FUNCTION public.circum(sbox) OWNER TO sitools;

--
-- Name: FUNCTION circum(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION circum(sbox) IS 'circumference of spherical box';


--
-- Name: circum(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION circum(scircle) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_circ';


ALTER FUNCTION public.circum(scircle) OWNER TO sitools;

--
-- Name: FUNCTION circum(scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION circum(scircle) IS 'circumference of spherical circle';


--
-- Name: circum(spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION circum(spoly) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_circ';


ALTER FUNCTION public.circum(spoly) OWNER TO sitools;

--
-- Name: FUNCTION circum(spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION circum(spoly) IS 'returns circumference of spherical polygon';


--
-- Name: dist(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION dist(scircle, scircle) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_distance';


ALTER FUNCTION public.dist(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION dist(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION dist(scircle, scircle) IS 'distance between two spherical circles';


--
-- Name: dist(scircle, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION dist(scircle, spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_point_distance';


ALTER FUNCTION public.dist(scircle, spoint) OWNER TO sitools;

--
-- Name: FUNCTION dist(scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION dist(scircle, spoint) IS 'distance between spherical circle and spherical point';


--
-- Name: dist(spoint, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION dist(spoint, scircle) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_point_distance_com';


ALTER FUNCTION public.dist(spoint, scircle) OWNER TO sitools;

--
-- Name: FUNCTION dist(spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION dist(spoint, scircle) IS 'distance between spherical circle and spherical point';


--
-- Name: dist(spoint, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION dist(spoint, spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_distance';


ALTER FUNCTION public.dist(spoint, spoint) OWNER TO sitools;

--
-- Name: FUNCTION dist(spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION dist(spoint, spoint) IS 'distance between spherical points';


--
-- Name: g_sbox_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sbox_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sbox_compress';


ALTER FUNCTION public.g_sbox_compress(internal) OWNER TO sitools;

--
-- Name: g_sbox_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sbox_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sbox_consistent';


ALTER FUNCTION public.g_sbox_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_scircle_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_scircle_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_scircle_compress';


ALTER FUNCTION public.g_scircle_compress(internal) OWNER TO sitools;

--
-- Name: g_scircle_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_scircle_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_scircle_consistent';


ALTER FUNCTION public.g_scircle_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_sellipse_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sellipse_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sellipse_compress';


ALTER FUNCTION public.g_sellipse_compress(internal) OWNER TO sitools;

--
-- Name: g_sellipse_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sellipse_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sellipse_consistent';


ALTER FUNCTION public.g_sellipse_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_sline_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sline_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sline_compress';


ALTER FUNCTION public.g_sline_compress(internal) OWNER TO sitools;

--
-- Name: g_sline_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_sline_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_sline_consistent';


ALTER FUNCTION public.g_sline_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_spath_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spath_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spath_compress';


ALTER FUNCTION public.g_spath_compress(internal) OWNER TO sitools;

--
-- Name: g_spath_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spath_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spath_consistent';


ALTER FUNCTION public.g_spath_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_spherekey_decompress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spherekey_decompress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spherekey_decompress';


ALTER FUNCTION public.g_spherekey_decompress(internal) OWNER TO sitools;

--
-- Name: g_spherekey_penalty(internal, internal, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spherekey_penalty(internal, internal, internal) RETURNS internal
    LANGUAGE c STRICT
    AS '$libdir/pg_sphere', 'g_spherekey_penalty';


ALTER FUNCTION public.g_spherekey_penalty(internal, internal, internal) OWNER TO sitools;

--
-- Name: g_spherekey_picksplit(internal, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spherekey_picksplit(internal, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spherekey_picksplit';


ALTER FUNCTION public.g_spherekey_picksplit(internal, internal) OWNER TO sitools;

--
-- Name: g_spherekey_same(spherekey, spherekey, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spherekey_same(spherekey, spherekey, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spherekey_same';


ALTER FUNCTION public.g_spherekey_same(spherekey, spherekey, internal) OWNER TO sitools;

--
-- Name: g_spherekey_union(bytea, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spherekey_union(bytea, internal) RETURNS spherekey
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spherekey_union';


ALTER FUNCTION public.g_spherekey_union(bytea, internal) OWNER TO sitools;

--
-- Name: g_spoint_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spoint_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spoint_compress';


ALTER FUNCTION public.g_spoint_compress(internal) OWNER TO sitools;

--
-- Name: g_spoint_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spoint_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spoint_consistent';


ALTER FUNCTION public.g_spoint_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: g_spoly_compress(internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spoly_compress(internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spoly_compress';


ALTER FUNCTION public.g_spoly_compress(internal) OWNER TO sitools;

--
-- Name: g_spoly_consistent(internal, internal, integer, oid, internal); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION g_spoly_consistent(internal, internal, integer, oid, internal) RETURNS internal
    LANGUAGE c
    AS '$libdir/pg_sphere', 'g_spoly_consistent';


ALTER FUNCTION public.g_spoly_consistent(internal, internal, integer, oid, internal) OWNER TO sitools;

--
-- Name: inc(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION inc(sellipse) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_incl';


ALTER FUNCTION public.inc(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION inc(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION inc(sellipse) IS 'inclination of spherical ellipse';


--
-- Name: lat(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION lat(spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_lat';


ALTER FUNCTION public.lat(spoint) OWNER TO sitools;

--
-- Name: FUNCTION lat(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION lat(spoint) IS 'latitude of spherical point';


--
-- Name: length(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION length(sline) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_length';


ALTER FUNCTION public.length(sline) OWNER TO sitools;

--
-- Name: FUNCTION length(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION length(sline) IS 'returns the length of a spherical line ( in radians )';


--
-- Name: length(spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION length(spath) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_length';


ALTER FUNCTION public.length(spath) OWNER TO sitools;

--
-- Name: FUNCTION length(spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION length(spath) IS 'returns length of spherical path';


--
-- Name: long(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION long(spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_long';


ALTER FUNCTION public.long(spoint) OWNER TO sitools;

--
-- Name: FUNCTION long(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION long(spoint) IS 'longitude of spherical point';


--
-- Name: lrad(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION lrad(sellipse) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_rad1';


ALTER FUNCTION public.lrad(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION lrad(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION lrad(sellipse) IS 'large radius of spherical ellipse';


--
-- Name: meridian(double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION meridian(double precision) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_meridian';


ALTER FUNCTION public.meridian(double precision) OWNER TO sitools;

--
-- Name: FUNCTION meridian(double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION meridian(double precision) IS 'returns a spherical line as a meridian along longitude arg';


--
-- Name: ne(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION ne(sbox) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_ne';


ALTER FUNCTION public.ne(sbox) OWNER TO sitools;

--
-- Name: FUNCTION ne(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION ne(sbox) IS 'north-east corner of spherical box';


--
-- Name: npoints(spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION npoints(spath) RETURNS integer
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_npts';


ALTER FUNCTION public.npoints(spath) OWNER TO sitools;

--
-- Name: FUNCTION npoints(spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION npoints(spath) IS 'returns number of points of spherical path';


--
-- Name: npoints(spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION npoints(spoly) RETURNS integer
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_npts';


ALTER FUNCTION public.npoints(spoly) OWNER TO sitools;

--
-- Name: FUNCTION npoints(spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION npoints(spoly) IS 'returns number of points of spherical polygon';


--
-- Name: nw(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION nw(sbox) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_nw';


ALTER FUNCTION public.nw(sbox) OWNER TO sitools;

--
-- Name: FUNCTION nw(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION nw(sbox) IS 'north-west corner of spherical box';


--
-- Name: pg_sphere_version(); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION pg_sphere_version() RETURNS cstring
    LANGUAGE c
    AS '$libdir/pg_sphere', 'pg_sphere_version';


ALTER FUNCTION public.pg_sphere_version() OWNER TO sitools;

--
-- Name: phi(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION phi(strans) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_phi';


ALTER FUNCTION public.phi(strans) OWNER TO sitools;

--
-- Name: FUNCTION phi(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION phi(strans) IS 'returns the first angle of Euler angles of a transformation object';


--
-- Name: psi(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION psi(strans) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_psi';


ALTER FUNCTION public.psi(strans) OWNER TO sitools;

--
-- Name: FUNCTION psi(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION psi(strans) IS 'returns the third angle of Euler angles of a transformation object';


--
-- Name: radius(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION radius(scircle) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_radius';


ALTER FUNCTION public.radius(scircle) OWNER TO sitools;

--
-- Name: FUNCTION radius(scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION radius(scircle) IS 'radius of spherical circle';


--
-- Name: sbox(spoint, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox(spoint, spoint) RETURNS sbox
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_in_from_points';


ALTER FUNCTION public.sbox(spoint, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sbox(spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox(spoint, spoint) IS 'returns a spherical box from south-west corner( arg1 ) and north-east corner( arg2 )';


--
-- Name: sbox_cont_point(sbox, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_cont_point(sbox, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_point';


ALTER FUNCTION public.sbox_cont_point(sbox, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sbox_cont_point(sbox, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_cont_point(sbox, spoint) IS 'true if spherical box contains spherical point ';


--
-- Name: sbox_cont_point_com(spoint, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_cont_point_com(spoint, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_point_com';


ALTER FUNCTION public.sbox_cont_point_com(spoint, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_cont_point_com(spoint, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_cont_point_com(spoint, sbox) IS 'true if spherical point is contained by spherical box';


--
-- Name: sbox_cont_point_com_neg(spoint, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_cont_point_com_neg(spoint, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_point_com_neg';


ALTER FUNCTION public.sbox_cont_point_com_neg(spoint, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_cont_point_com_neg(spoint, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_cont_point_com_neg(spoint, sbox) IS 'true if spherical point is not contained by spherical box ';


--
-- Name: sbox_cont_point_neg(sbox, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_cont_point_neg(sbox, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_point_neg';


ALTER FUNCTION public.sbox_cont_point_neg(sbox, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sbox_cont_point_neg(sbox, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_cont_point_neg(sbox, spoint) IS 'true if spherical box does not contain spherical point ';


--
-- Name: sbox_contains_box(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_box(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_box';


ALTER FUNCTION public.sbox_contains_box(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_box(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_box(sbox, sbox) IS 'true if spherical box contains spherical box ';


--
-- Name: sbox_contains_box_com(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_box_com(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_box_com';


ALTER FUNCTION public.sbox_contains_box_com(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_box_com(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_box_com(sbox, sbox) IS 'true if spherical box contains spherical box ';


--
-- Name: sbox_contains_box_com_neg(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_box_com_neg(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_box_com_neg';


ALTER FUNCTION public.sbox_contains_box_com_neg(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_box_com_neg(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_box_com_neg(sbox, sbox) IS 'true if spherical box does not contain spherical box ';


--
-- Name: sbox_contains_box_neg(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_box_neg(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_box_neg';


ALTER FUNCTION public.sbox_contains_box_neg(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_box_neg(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_box_neg(sbox, sbox) IS 'true if spherical box does not contain spherical box ';


--
-- Name: sbox_contains_circle(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_circle(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_circle';


ALTER FUNCTION public.sbox_contains_circle(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_circle(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_circle(sbox, scircle) IS 'true if spherical box contains spherical circle ';


--
-- Name: sbox_contains_circle_com(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_circle_com(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_circle_com';


ALTER FUNCTION public.sbox_contains_circle_com(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_circle_com(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_circle_com(scircle, sbox) IS 'true if spherical box contains spherical circle ';


--
-- Name: sbox_contains_circle_com_neg(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_circle_com_neg(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_circle_com_neg';


ALTER FUNCTION public.sbox_contains_circle_com_neg(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_circle_com_neg(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_circle_com_neg(scircle, sbox) IS 'true if spherical box does not contain spherical circle ';


--
-- Name: sbox_contains_circle_neg(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_circle_neg(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_circle_neg';


ALTER FUNCTION public.sbox_contains_circle_neg(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_circle_neg(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_circle_neg(sbox, scircle) IS 'true if spherical box does not contain spherical circle ';


--
-- Name: sbox_contains_ellipse(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_ellipse(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_ellipse';


ALTER FUNCTION public.sbox_contains_ellipse(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_ellipse(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_ellipse(sbox, sellipse) IS 'true if spherical box contains spherical ellipse ';


--
-- Name: sbox_contains_ellipse_com(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_ellipse_com(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_ellipse_com';


ALTER FUNCTION public.sbox_contains_ellipse_com(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_ellipse_com(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_ellipse_com(sellipse, sbox) IS 'true if spherical box contains spherical ellipse ';


--
-- Name: sbox_contains_ellipse_com_neg(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_ellipse_com_neg(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_ellipse_com_neg';


ALTER FUNCTION public.sbox_contains_ellipse_com_neg(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_ellipse_com_neg(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_ellipse_com_neg(sellipse, sbox) IS 'true if spherical box does not contain spherical ellipse ';


--
-- Name: sbox_contains_ellipse_neg(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_ellipse_neg(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_ellipse_neg';


ALTER FUNCTION public.sbox_contains_ellipse_neg(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_ellipse_neg(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_ellipse_neg(sbox, sellipse) IS 'true if spherical box does not contain spherical ellipse ';


--
-- Name: sbox_contains_line(sbox, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_line(sbox, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_line';


ALTER FUNCTION public.sbox_contains_line(sbox, sline) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_line(sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_line(sbox, sline) IS 'true if spherical box contains spherical line ';


--
-- Name: sbox_contains_line_com(sline, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_line_com(sline, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_line_com';


ALTER FUNCTION public.sbox_contains_line_com(sline, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_line_com(sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_line_com(sline, sbox) IS 'true if spherical box contains spherical line ';


--
-- Name: sbox_contains_line_com_neg(sline, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_line_com_neg(sline, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_line_com_neg';


ALTER FUNCTION public.sbox_contains_line_com_neg(sline, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_line_com_neg(sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_line_com_neg(sline, sbox) IS 'true if spherical box does not contain spherical line ';


--
-- Name: sbox_contains_line_neg(sbox, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_line_neg(sbox, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_line_neg';


ALTER FUNCTION public.sbox_contains_line_neg(sbox, sline) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_line_neg(sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_line_neg(sbox, sline) IS 'true if spherical box does not contain spherical line ';


--
-- Name: sbox_contains_path(sbox, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_path(sbox, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_path';


ALTER FUNCTION public.sbox_contains_path(sbox, spath) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_path(sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_path(sbox, spath) IS 'true if spherical box contains spherical path ';


--
-- Name: sbox_contains_path_com(spath, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_path_com(spath, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_path_com';


ALTER FUNCTION public.sbox_contains_path_com(spath, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_path_com(spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_path_com(spath, sbox) IS 'true if spherical box contains spherical path ';


--
-- Name: sbox_contains_path_com_neg(spath, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_path_com_neg(spath, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_path_com_neg';


ALTER FUNCTION public.sbox_contains_path_com_neg(spath, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_path_com_neg(spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_path_com_neg(spath, sbox) IS 'true if spherical box does not contain spherical path ';


--
-- Name: sbox_contains_path_neg(sbox, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_path_neg(sbox, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_path_neg';


ALTER FUNCTION public.sbox_contains_path_neg(sbox, spath) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_path_neg(sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_path_neg(sbox, spath) IS 'true if spherical box does not contain spherical path ';


--
-- Name: sbox_contains_poly(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_poly(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_poly';


ALTER FUNCTION public.sbox_contains_poly(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_poly(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_poly(sbox, spoly) IS 'true if spherical box contains spherical polygon ';


--
-- Name: sbox_contains_poly_com(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_poly_com(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_poly_com';


ALTER FUNCTION public.sbox_contains_poly_com(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_poly_com(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_poly_com(spoly, sbox) IS 'true if spherical box contains spherical polygon ';


--
-- Name: sbox_contains_poly_com_neg(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_poly_com_neg(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_poly_com_neg';


ALTER FUNCTION public.sbox_contains_poly_com_neg(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_poly_com_neg(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_poly_com_neg(spoly, sbox) IS 'true if spherical box does not contain spherical polygon ';


--
-- Name: sbox_contains_poly_neg(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_contains_poly_neg(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_cont_poly_neg';


ALTER FUNCTION public.sbox_contains_poly_neg(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sbox_contains_poly_neg(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_contains_poly_neg(sbox, spoly) IS 'true if spherical box does not contain spherical polygon ';


--
-- Name: sbox_equal(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_equal(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_equal';


ALTER FUNCTION public.sbox_equal(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_equal(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_equal(sbox, sbox) IS 'returns true, if spherical boxes are equal';


--
-- Name: sbox_equal_neg(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_equal_neg(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_equal_neg';


ALTER FUNCTION public.sbox_equal_neg(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_equal_neg(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_equal_neg(sbox, sbox) IS 'returns true, if spherical boxes are not equal';


--
-- Name: sbox_overlap_box(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_box(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_box';


ALTER FUNCTION public.sbox_overlap_box(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_box(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_box(sbox, sbox) IS 'true if spherical box overlap spherical box ';


--
-- Name: sbox_overlap_box_neg(sbox, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_box_neg(sbox, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_box_neg';


ALTER FUNCTION public.sbox_overlap_box_neg(sbox, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_box_neg(sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_box_neg(sbox, sbox) IS 'true if spherical box does not overlap spherical box ';


--
-- Name: sbox_overlap_circle(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_circle(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_circle';


ALTER FUNCTION public.sbox_overlap_circle(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_circle(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_circle(sbox, scircle) IS 'true if spherical circle overlap spherical box ';


--
-- Name: sbox_overlap_circle_com(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_circle_com(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_circle_com';


ALTER FUNCTION public.sbox_overlap_circle_com(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_circle_com(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_circle_com(scircle, sbox) IS 'true if spherical circle overlap spherical box ';


--
-- Name: sbox_overlap_circle_com_neg(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_circle_com_neg(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_circle_com_neg';


ALTER FUNCTION public.sbox_overlap_circle_com_neg(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_circle_com_neg(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_circle_com_neg(scircle, sbox) IS 'true if spherical circle does not overlap spherical box ';


--
-- Name: sbox_overlap_circle_neg(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_circle_neg(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_circle_neg';


ALTER FUNCTION public.sbox_overlap_circle_neg(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_circle_neg(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_circle_neg(sbox, scircle) IS 'true if spherical circle does not overlap spherical box ';


--
-- Name: sbox_overlap_ellipse(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_ellipse(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_ellipse';


ALTER FUNCTION public.sbox_overlap_ellipse(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_ellipse(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_ellipse(sbox, sellipse) IS 'true if spherical ellipse overlap spherical box ';


--
-- Name: sbox_overlap_ellipse_com(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_ellipse_com(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_ellipse_com';


ALTER FUNCTION public.sbox_overlap_ellipse_com(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_ellipse_com(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_ellipse_com(sellipse, sbox) IS 'true if spherical ellipse overlap spherical box ';


--
-- Name: sbox_overlap_ellipse_com_neg(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_ellipse_com_neg(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_ellipse_com_neg';


ALTER FUNCTION public.sbox_overlap_ellipse_com_neg(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_ellipse_com_neg(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_ellipse_com_neg(sellipse, sbox) IS 'true if spherical ellipse does not overlap spherical box ';


--
-- Name: sbox_overlap_ellipse_neg(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_ellipse_neg(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_ellipse_neg';


ALTER FUNCTION public.sbox_overlap_ellipse_neg(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_ellipse_neg(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_ellipse_neg(sbox, sellipse) IS 'true if spherical ellipse does not overlap spherical box ';


--
-- Name: sbox_overlap_line(sbox, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_line(sbox, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_line';


ALTER FUNCTION public.sbox_overlap_line(sbox, sline) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_line(sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_line(sbox, sline) IS 'true if spherical line overlap spherical box ';


--
-- Name: sbox_overlap_line_com(sline, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_line_com(sline, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_line_com';


ALTER FUNCTION public.sbox_overlap_line_com(sline, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_line_com(sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_line_com(sline, sbox) IS 'true if spherical line overlap spherical box ';


--
-- Name: sbox_overlap_line_com_neg(sline, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_line_com_neg(sline, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_line_com_neg';


ALTER FUNCTION public.sbox_overlap_line_com_neg(sline, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_line_com_neg(sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_line_com_neg(sline, sbox) IS 'true if spherical line does not overlap spherical box ';


--
-- Name: sbox_overlap_line_neg(sbox, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_line_neg(sbox, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_line_neg';


ALTER FUNCTION public.sbox_overlap_line_neg(sbox, sline) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_line_neg(sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_line_neg(sbox, sline) IS 'true if spherical line does not overlap spherical box ';


--
-- Name: sbox_overlap_path(sbox, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_path(sbox, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_path';


ALTER FUNCTION public.sbox_overlap_path(sbox, spath) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_path(sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_path(sbox, spath) IS 'true if spherical path overlap spherical box ';


--
-- Name: sbox_overlap_path_com(spath, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_path_com(spath, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_path_com';


ALTER FUNCTION public.sbox_overlap_path_com(spath, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_path_com(spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_path_com(spath, sbox) IS 'true if spherical path overlap spherical box ';


--
-- Name: sbox_overlap_path_com_neg(spath, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_path_com_neg(spath, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_path_com_neg';


ALTER FUNCTION public.sbox_overlap_path_com_neg(spath, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_path_com_neg(spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_path_com_neg(spath, sbox) IS 'true if spherical path does not overlap spherical box ';


--
-- Name: sbox_overlap_path_neg(sbox, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_path_neg(sbox, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_path_neg';


ALTER FUNCTION public.sbox_overlap_path_neg(sbox, spath) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_path_neg(sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_path_neg(sbox, spath) IS 'true if spherical path does not overlap spherical box ';


--
-- Name: sbox_overlap_poly(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_poly(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_poly';


ALTER FUNCTION public.sbox_overlap_poly(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_poly(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_poly(sbox, spoly) IS 'true if spherical polygon overlap spherical box ';


--
-- Name: sbox_overlap_poly_com(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_poly_com(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_poly_com';


ALTER FUNCTION public.sbox_overlap_poly_com(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_poly_com(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_poly_com(spoly, sbox) IS 'true if spherical polygon overlap spherical box ';


--
-- Name: sbox_overlap_poly_com_neg(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_poly_com_neg(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_poly_com_neg';


ALTER FUNCTION public.sbox_overlap_poly_com_neg(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_poly_com_neg(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_poly_com_neg(spoly, sbox) IS 'true if spherical polygon does not overlap spherical box ';


--
-- Name: sbox_overlap_poly_neg(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sbox_overlap_poly_neg(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_overlap_poly_neg';


ALTER FUNCTION public.sbox_overlap_poly_neg(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sbox_overlap_poly_neg(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sbox_overlap_poly_neg(sbox, spoly) IS 'true if spherical polygon does not overlap spherical box ';


--
-- Name: scircle(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle(sellipse) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_circle';


ALTER FUNCTION public.scircle(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION scircle(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle(sellipse) IS 'spherical bounding circle of spherical ellipse';


--
-- Name: scircle(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle(spoint) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_to_circle';


ALTER FUNCTION public.scircle(spoint) OWNER TO sitools;

--
-- Name: FUNCTION scircle(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle(spoint) IS 'spherical circle with radius 0 and spherical point as center';


--
-- Name: scircle(spoint, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle(spoint, double precision) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_by_center';


ALTER FUNCTION public.scircle(spoint, double precision) OWNER TO sitools;

--
-- Name: FUNCTION scircle(spoint, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle(spoint, double precision) IS 'spherical circle with spherical point as center and float8 as radius in radians';


--
-- Name: scircle_contained_by_circle(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contained_by_circle(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_in_circle';


ALTER FUNCTION public.scircle_contained_by_circle(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contained_by_circle(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contained_by_circle(scircle, scircle) IS 'true if spherical circle is contained by spherical circle';


--
-- Name: scircle_contained_by_circle_neg(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contained_by_circle_neg(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_in_circle_neg';


ALTER FUNCTION public.scircle_contained_by_circle_neg(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contained_by_circle_neg(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contained_by_circle_neg(scircle, scircle) IS 'true if spherical circle is not contained by spherical circle';


--
-- Name: scircle_contains_box(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_box(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_box';


ALTER FUNCTION public.scircle_contains_box(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_box(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_box(scircle, sbox) IS 'true if spherical circle contains spherical box ';


--
-- Name: scircle_contains_box_com(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_box_com(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_box_com';


ALTER FUNCTION public.scircle_contains_box_com(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_box_com(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_box_com(sbox, scircle) IS 'true if spherical circle contains spherical box ';


--
-- Name: scircle_contains_box_com_neg(sbox, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_box_com_neg(sbox, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_box_com_neg';


ALTER FUNCTION public.scircle_contains_box_com_neg(sbox, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_box_com_neg(sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_box_com_neg(sbox, scircle) IS 'true if spherical circle does not contain spherical box ';


--
-- Name: scircle_contains_box_neg(scircle, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_box_neg(scircle, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_box_neg';


ALTER FUNCTION public.scircle_contains_box_neg(scircle, sbox) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_box_neg(scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_box_neg(scircle, sbox) IS 'true if spherical circle does not contain spherical box ';


--
-- Name: scircle_contains_circle(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_circle(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_in_circle_com';


ALTER FUNCTION public.scircle_contains_circle(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_circle(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_circle(scircle, scircle) IS 'true if spherical circle contains spherical circle';


--
-- Name: scircle_contains_circle_neg(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_circle_neg(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_in_circle_com_neg';


ALTER FUNCTION public.scircle_contains_circle_neg(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_circle_neg(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_circle_neg(scircle, scircle) IS 'true if spherical circle does not contain spherical circle';


--
-- Name: scircle_contains_ellipse(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_ellipse(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_ellipse';


ALTER FUNCTION public.scircle_contains_ellipse(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_ellipse(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_ellipse(scircle, sellipse) IS 'true if spherical circle contains spherical ellipse ';


--
-- Name: scircle_contains_ellipse_com(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_ellipse_com(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_ellipse_com';


ALTER FUNCTION public.scircle_contains_ellipse_com(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_ellipse_com(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_ellipse_com(sellipse, scircle) IS 'true if spherical circle contains spherical ellipse ';


--
-- Name: scircle_contains_ellipse_com_neg(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_ellipse_com_neg(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_ellipse_com_neg';


ALTER FUNCTION public.scircle_contains_ellipse_com_neg(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_ellipse_com_neg(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_ellipse_com_neg(sellipse, scircle) IS 'true if spherical circle does not contain spherical ellipse ';


--
-- Name: scircle_contains_ellipse_neg(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_ellipse_neg(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_ellipse_neg';


ALTER FUNCTION public.scircle_contains_ellipse_neg(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_ellipse_neg(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_ellipse_neg(scircle, sellipse) IS 'true if spherical circle does not contain spherical ellipse ';


--
-- Name: scircle_contains_line(scircle, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_line(scircle, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_line';


ALTER FUNCTION public.scircle_contains_line(scircle, sline) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_line(scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_line(scircle, sline) IS 'returns true if spherical circle contains spherical line';


--
-- Name: scircle_contains_line_com(sline, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_line_com(sline, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_line_com';


ALTER FUNCTION public.scircle_contains_line_com(sline, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_line_com(sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_line_com(sline, scircle) IS 'returns true if spherical circle contains spherical line';


--
-- Name: scircle_contains_line_com_neg(sline, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_line_com_neg(sline, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_line_com_neg';


ALTER FUNCTION public.scircle_contains_line_com_neg(sline, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_line_com_neg(sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_line_com_neg(sline, scircle) IS 'returns true if spherical circle does not contain spherical line';


--
-- Name: scircle_contains_line_neg(scircle, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_line_neg(scircle, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_line_neg';


ALTER FUNCTION public.scircle_contains_line_neg(scircle, sline) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_line_neg(scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_line_neg(scircle, sline) IS 'returns true if spherical circle does not contain spherical line';


--
-- Name: scircle_contains_path(scircle, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_path(scircle, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_path';


ALTER FUNCTION public.scircle_contains_path(scircle, spath) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_path(scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_path(scircle, spath) IS 'true if spherical circle contains spherical path ';


--
-- Name: scircle_contains_path_com(spath, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_path_com(spath, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_path_com';


ALTER FUNCTION public.scircle_contains_path_com(spath, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_path_com(spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_path_com(spath, scircle) IS 'true if spherical circle contains spherical path ';


--
-- Name: scircle_contains_path_com_neg(spath, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_path_com_neg(spath, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_path_com_neg';


ALTER FUNCTION public.scircle_contains_path_com_neg(spath, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_path_com_neg(spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_path_com_neg(spath, scircle) IS 'true if spherical circle does not contain spherical path ';


--
-- Name: scircle_contains_path_neg(scircle, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_path_neg(scircle, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_path_neg';


ALTER FUNCTION public.scircle_contains_path_neg(scircle, spath) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_path_neg(scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_path_neg(scircle, spath) IS 'true if spherical circle does not contain spherical path ';


--
-- Name: scircle_contains_polygon(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_polygon(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_poly';


ALTER FUNCTION public.scircle_contains_polygon(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_polygon(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_polygon(scircle, spoly) IS 'true if spherical circle contains spherical polygon ';


--
-- Name: scircle_contains_polygon_com(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_polygon_com(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_poly_com';


ALTER FUNCTION public.scircle_contains_polygon_com(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_polygon_com(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_polygon_com(spoly, scircle) IS 'true if spherical circle contains spherical polygon ';


--
-- Name: scircle_contains_polygon_com_neg(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_polygon_com_neg(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_poly_com_neg';


ALTER FUNCTION public.scircle_contains_polygon_com_neg(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_polygon_com_neg(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_polygon_com_neg(spoly, scircle) IS 'true if spherical circle does not contain spherical polygon ';


--
-- Name: scircle_contains_polygon_neg(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_contains_polygon_neg(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_cont_poly_neg';


ALTER FUNCTION public.scircle_contains_polygon_neg(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION scircle_contains_polygon_neg(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_contains_polygon_neg(scircle, spoly) IS 'true if spherical circle does not contain spherical polygon ';


--
-- Name: scircle_equal(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_equal(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_equal';


ALTER FUNCTION public.scircle_equal(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_equal(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_equal(scircle, scircle) IS 'returns true, if spherical circles are equal';


--
-- Name: scircle_equal_neg(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_equal_neg(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_equal_neg';


ALTER FUNCTION public.scircle_equal_neg(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_equal_neg(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_equal_neg(scircle, scircle) IS 'returns true, if spherical circles are not equal';


--
-- Name: scircle_overlap(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap';


ALTER FUNCTION public.scircle_overlap(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap(scircle, scircle) IS 'true if spherical circles overlap';


--
-- Name: scircle_overlap_neg(scircle, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap_neg(scircle, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap_neg';


ALTER FUNCTION public.scircle_overlap_neg(scircle, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap_neg(scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap_neg(scircle, scircle) IS 'true if spherical circles do not overlap';


--
-- Name: scircle_overlap_path(scircle, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap_path(scircle, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap_path';


ALTER FUNCTION public.scircle_overlap_path(scircle, spath) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap_path(scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap_path(scircle, spath) IS 'true if spherical circle overlap spherical path ';


--
-- Name: scircle_overlap_path_com(spath, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap_path_com(spath, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap_path_com';


ALTER FUNCTION public.scircle_overlap_path_com(spath, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap_path_com(spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap_path_com(spath, scircle) IS 'true if spherical circle overlap spherical path ';


--
-- Name: scircle_overlap_path_com_neg(spath, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap_path_com_neg(spath, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap_path_com_neg';


ALTER FUNCTION public.scircle_overlap_path_com_neg(spath, scircle) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap_path_com_neg(spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap_path_com_neg(spath, scircle) IS 'true if spherical circle overlap spherical path ';


--
-- Name: scircle_overlap_path_neg(scircle, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION scircle_overlap_path_neg(scircle, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_overlap_path_neg';


ALTER FUNCTION public.scircle_overlap_path_neg(scircle, spath) OWNER TO sitools;

--
-- Name: FUNCTION scircle_overlap_path_neg(scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION scircle_overlap_path_neg(scircle, spath) IS 'true if spherical circle does not overlap spherical path ';


--
-- Name: se(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION se(sbox) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_se';


ALTER FUNCTION public.se(sbox) OWNER TO sitools;

--
-- Name: FUNCTION se(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION se(sbox) IS 'south-east corner of spherical box';


--
-- Name: sellipse(scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse(scircle) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherecircle_ellipse';


ALTER FUNCTION public.sellipse(scircle) OWNER TO sitools;

--
-- Name: FUNCTION sellipse(scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse(scircle) IS 'returns spherical circle as spherical ellipse';


--
-- Name: sellipse(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse(spoint) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_ellipse';


ALTER FUNCTION public.sellipse(spoint) OWNER TO sitools;

--
-- Name: FUNCTION sellipse(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse(spoint) IS 'returns spherical point as spherical ellipse';


--
-- Name: sellipse(spoint, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse(spoint, double precision, double precision, double precision) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_infunc';


ALTER FUNCTION public.sellipse(spoint, double precision, double precision, double precision) OWNER TO sitools;

--
-- Name: FUNCTION sellipse(spoint, double precision, double precision, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse(spoint, double precision, double precision, double precision) IS 'returns spherical ellipse from center, radius1, radius2 and inclination';


--
-- Name: sellipse_contains_box(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_box(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_box';


ALTER FUNCTION public.sellipse_contains_box(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_box(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_box(sellipse, sbox) IS 'true if spherical ellipse contains spherical box ';


--
-- Name: sellipse_contains_box_com(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_box_com(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_box_com';


ALTER FUNCTION public.sellipse_contains_box_com(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_box_com(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_box_com(sbox, sellipse) IS 'true if spherical ellipse contains spherical box ';


--
-- Name: sellipse_contains_box_com_neg(sbox, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_box_com_neg(sbox, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_box_com_neg';


ALTER FUNCTION public.sellipse_contains_box_com_neg(sbox, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_box_com_neg(sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_box_com_neg(sbox, sellipse) IS 'true if spherical ellipse does not contain spherical box ';


--
-- Name: sellipse_contains_box_neg(sellipse, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_box_neg(sellipse, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_box_neg';


ALTER FUNCTION public.sellipse_contains_box_neg(sellipse, sbox) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_box_neg(sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_box_neg(sellipse, sbox) IS 'true if spherical ellipse does not contain spherical box ';


--
-- Name: sellipse_contains_circle(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_circle(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_circle';


ALTER FUNCTION public.sellipse_contains_circle(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_circle(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_circle(sellipse, scircle) IS 'true if spherical ellipse contains spherical circle ';


--
-- Name: sellipse_contains_circle_com(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_circle_com(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_circle_com';


ALTER FUNCTION public.sellipse_contains_circle_com(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_circle_com(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_circle_com(scircle, sellipse) IS 'true if spherical ellipse contains spherical circle ';


--
-- Name: sellipse_contains_circle_com_neg(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_circle_com_neg(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_circle_com_neg';


ALTER FUNCTION public.sellipse_contains_circle_com_neg(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_circle_com_neg(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_circle_com_neg(scircle, sellipse) IS 'true if spherical ellipse does not contain spherical circle ';


--
-- Name: sellipse_contains_circle_neg(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_circle_neg(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_circle_neg';


ALTER FUNCTION public.sellipse_contains_circle_neg(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_circle_neg(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_circle_neg(sellipse, scircle) IS 'true if spherical ellipse does not contain spherical circle ';


--
-- Name: sellipse_contains_ellipse(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_ellipse(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_ellipse';


ALTER FUNCTION public.sellipse_contains_ellipse(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_ellipse(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_ellipse(sellipse, sellipse) IS 'true if spherical ellipse contains spherical ellipse ';


--
-- Name: sellipse_contains_ellipse_com(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_ellipse_com(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_ellipse_com';


ALTER FUNCTION public.sellipse_contains_ellipse_com(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_ellipse_com(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_ellipse_com(sellipse, sellipse) IS 'true if spherical ellipse is contained by spherical ellipse ';


--
-- Name: sellipse_contains_ellipse_com_neg(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_ellipse_com_neg(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_ellipse_com_neg';


ALTER FUNCTION public.sellipse_contains_ellipse_com_neg(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_ellipse_com_neg(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_ellipse_com_neg(sellipse, sellipse) IS 'true if spherical ellipse is not contained by spherical ellipse ';


--
-- Name: sellipse_contains_ellipse_neg(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_ellipse_neg(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_ellipse_neg';


ALTER FUNCTION public.sellipse_contains_ellipse_neg(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_ellipse_neg(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_ellipse_neg(sellipse, sellipse) IS 'true if spherical ellipse does not contain spherical ellipse ';


--
-- Name: sellipse_contains_line(sellipse, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_line(sellipse, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_line';


ALTER FUNCTION public.sellipse_contains_line(sellipse, sline) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_line(sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_line(sellipse, sline) IS 'returns true if spherical ellipse contains spherical line';


--
-- Name: sellipse_contains_line_com(sline, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_line_com(sline, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_line_com';


ALTER FUNCTION public.sellipse_contains_line_com(sline, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_line_com(sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_line_com(sline, sellipse) IS 'returns true if spherical ellipse contains spherical line';


--
-- Name: sellipse_contains_line_com_neg(sline, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_line_com_neg(sline, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_line_com_neg';


ALTER FUNCTION public.sellipse_contains_line_com_neg(sline, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_line_com_neg(sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_line_com_neg(sline, sellipse) IS 'returns true if spherical ellipse does not contain spherical line';


--
-- Name: sellipse_contains_line_neg(sellipse, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_line_neg(sellipse, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_line_neg';


ALTER FUNCTION public.sellipse_contains_line_neg(sellipse, sline) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_line_neg(sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_line_neg(sellipse, sline) IS 'returns true if spherical ellipse does not contain spherical line';


--
-- Name: sellipse_contains_path(sellipse, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_path(sellipse, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_path';


ALTER FUNCTION public.sellipse_contains_path(sellipse, spath) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_path(sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_path(sellipse, spath) IS 'true if spherical ellipse contains spherical path ';


--
-- Name: sellipse_contains_path_com(spath, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_path_com(spath, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_path_com';


ALTER FUNCTION public.sellipse_contains_path_com(spath, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_path_com(spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_path_com(spath, sellipse) IS 'true if spherical ellipse contains spherical path ';


--
-- Name: sellipse_contains_path_com_neg(spath, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_path_com_neg(spath, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_path_com_neg';


ALTER FUNCTION public.sellipse_contains_path_com_neg(spath, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_path_com_neg(spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_path_com_neg(spath, sellipse) IS 'true if spherical ellipse does not contain spherical path ';


--
-- Name: sellipse_contains_path_neg(sellipse, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_path_neg(sellipse, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_path_neg';


ALTER FUNCTION public.sellipse_contains_path_neg(sellipse, spath) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_path_neg(sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_path_neg(sellipse, spath) IS 'true if spherical ellipse does not contain spherical path ';


--
-- Name: sellipse_contains_point(sellipse, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_point(sellipse, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_point';


ALTER FUNCTION public.sellipse_contains_point(sellipse, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_point(sellipse, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_point(sellipse, spoint) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: sellipse_contains_point_com(spoint, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_point_com(spoint, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_point_com';


ALTER FUNCTION public.sellipse_contains_point_com(spoint, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_point_com(spoint, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_point_com(spoint, sellipse) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: sellipse_contains_point_com_neg(spoint, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_point_com_neg(spoint, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_point_com_neg';


ALTER FUNCTION public.sellipse_contains_point_com_neg(spoint, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_point_com_neg(spoint, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_point_com_neg(spoint, sellipse) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: sellipse_contains_point_neg(sellipse, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_point_neg(sellipse, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_point_neg';


ALTER FUNCTION public.sellipse_contains_point_neg(sellipse, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_point_neg(sellipse, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_point_neg(sellipse, spoint) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: sellipse_contains_polygon(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_polygon(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_poly';


ALTER FUNCTION public.sellipse_contains_polygon(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_polygon(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_polygon(sellipse, spoly) IS 'true if spherical ellipse contains spherical polygon ';


--
-- Name: sellipse_contains_polygon_com(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_polygon_com(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_poly_com';


ALTER FUNCTION public.sellipse_contains_polygon_com(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_polygon_com(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_polygon_com(spoly, sellipse) IS 'true if spherical ellipse contains spherical polygon ';


--
-- Name: sellipse_contains_polygon_com_neg(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_polygon_com_neg(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_poly_com_neg';


ALTER FUNCTION public.sellipse_contains_polygon_com_neg(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_polygon_com_neg(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_polygon_com_neg(spoly, sellipse) IS 'true if spherical ellipse does not contain spherical polygon ';


--
-- Name: sellipse_contains_polygon_neg(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_contains_polygon_neg(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_cont_poly_neg';


ALTER FUNCTION public.sellipse_contains_polygon_neg(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_contains_polygon_neg(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_contains_polygon_neg(sellipse, spoly) IS 'true if spherical ellipse does not contain spherical polygon ';


--
-- Name: sellipse_equal(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_equal(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_equal';


ALTER FUNCTION public.sellipse_equal(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_equal(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_equal(sellipse, sellipse) IS 'returns true, if spherical ellipses are equal';


--
-- Name: sellipse_equal_neg(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_equal_neg(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_equal_neg';


ALTER FUNCTION public.sellipse_equal_neg(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_equal_neg(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_equal_neg(sellipse, sellipse) IS 'returns true, if spherical ellipses are not equal';


--
-- Name: sellipse_overlap_circle(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_circle(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_circle';


ALTER FUNCTION public.sellipse_overlap_circle(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_circle(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_circle(sellipse, scircle) IS 'true if spherical circle overlap spherical ellipse ';


--
-- Name: sellipse_overlap_circle_com(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_circle_com(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_circle_com';


ALTER FUNCTION public.sellipse_overlap_circle_com(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_circle_com(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_circle_com(scircle, sellipse) IS 'true if spherical circle overlap spherical ellipse ';


--
-- Name: sellipse_overlap_circle_com_neg(scircle, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_circle_com_neg(scircle, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_circle_com_neg';


ALTER FUNCTION public.sellipse_overlap_circle_com_neg(scircle, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_circle_com_neg(scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_circle_com_neg(scircle, sellipse) IS 'true if spherical circle does not overlap spherical ellipse ';


--
-- Name: sellipse_overlap_circle_neg(sellipse, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_circle_neg(sellipse, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_circle_neg';


ALTER FUNCTION public.sellipse_overlap_circle_neg(sellipse, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_circle_neg(sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_circle_neg(sellipse, scircle) IS 'true if spherical circle does not overlap spherical ellipse ';


--
-- Name: sellipse_overlap_ellipse(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_ellipse(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_ellipse';


ALTER FUNCTION public.sellipse_overlap_ellipse(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_ellipse(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_ellipse(sellipse, sellipse) IS 'true if spherical ellipse overlaps spherical ellipse ';


--
-- Name: sellipse_overlap_ellipse_neg(sellipse, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_ellipse_neg(sellipse, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_ellipse_neg';


ALTER FUNCTION public.sellipse_overlap_ellipse_neg(sellipse, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_ellipse_neg(sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_ellipse_neg(sellipse, sellipse) IS 'true if spherical ellipse does not overlap spherical ellipse';


--
-- Name: sellipse_overlap_line(sellipse, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_line(sellipse, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_line';


ALTER FUNCTION public.sellipse_overlap_line(sellipse, sline) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_line(sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_line(sellipse, sline) IS 'returns true if spherical line overlaps spherical ellipse';


--
-- Name: sellipse_overlap_line_com(sline, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_line_com(sline, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_line_com';


ALTER FUNCTION public.sellipse_overlap_line_com(sline, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_line_com(sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_line_com(sline, sellipse) IS 'returns true if spherical line overlaps spherical ellipse';


--
-- Name: sellipse_overlap_line_com_neg(sline, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_line_com_neg(sline, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_line_com_neg';


ALTER FUNCTION public.sellipse_overlap_line_com_neg(sline, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_line_com_neg(sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_line_com_neg(sline, sellipse) IS 'returns true if spherical line does not overlap spherical ellipse';


--
-- Name: sellipse_overlap_line_neg(sellipse, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_line_neg(sellipse, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_line_neg';


ALTER FUNCTION public.sellipse_overlap_line_neg(sellipse, sline) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_line_neg(sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_line_neg(sellipse, sline) IS 'returns true if spherical line overlaps spherical ellipse';


--
-- Name: sellipse_overlap_path(sellipse, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_path(sellipse, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_path';


ALTER FUNCTION public.sellipse_overlap_path(sellipse, spath) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_path(sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_path(sellipse, spath) IS 'true if spherical ellipse overlap spherical path ';


--
-- Name: sellipse_overlap_path_com(spath, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_path_com(spath, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_path_com';


ALTER FUNCTION public.sellipse_overlap_path_com(spath, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_path_com(spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_path_com(spath, sellipse) IS 'true if spherical ellipse overlap spherical path ';


--
-- Name: sellipse_overlap_path_com_neg(spath, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_path_com_neg(spath, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_path_com_neg';


ALTER FUNCTION public.sellipse_overlap_path_com_neg(spath, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_path_com_neg(spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_path_com_neg(spath, sellipse) IS 'true if spherical ellipse overlap spherical path ';


--
-- Name: sellipse_overlap_path_neg(sellipse, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sellipse_overlap_path_neg(sellipse, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_overlap_path_neg';


ALTER FUNCTION public.sellipse_overlap_path_neg(sellipse, spath) OWNER TO sitools;

--
-- Name: FUNCTION sellipse_overlap_path_neg(sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sellipse_overlap_path_neg(sellipse, spath) IS 'true if spherical ellipse does not overlap spherical path ';


--
-- Name: set_sphere_output(cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION set_sphere_output(cstring) RETURNS cstring
    LANGUAGE c
    AS '$libdir/pg_sphere', 'set_sphere_output';


ALTER FUNCTION public.set_sphere_output(cstring) OWNER TO sitools;

--
-- Name: set_sphere_output_precision(integer); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION set_sphere_output_precision(integer) RETURNS cstring
    LANGUAGE c
    AS '$libdir/pg_sphere', 'set_sphere_output_precision';


ALTER FUNCTION public.set_sphere_output_precision(integer) OWNER TO sitools;

--
-- Name: sl_beg(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sl_beg(sline) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_begin';


ALTER FUNCTION public.sl_beg(sline) OWNER TO sitools;

--
-- Name: FUNCTION sl_beg(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sl_beg(sline) IS 'returns the begin of a spherical line';


--
-- Name: sl_end(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sl_end(sline) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_end';


ALTER FUNCTION public.sl_end(sline) OWNER TO sitools;

--
-- Name: FUNCTION sl_end(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sl_end(sline) IS 'returns the end of a spherical line';


--
-- Name: sline(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline(spoint) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_from_point';


ALTER FUNCTION public.sline(spoint) OWNER TO sitools;

--
-- Name: FUNCTION sline(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline(spoint) IS 'casts a spherical point to a spherical line';


--
-- Name: sline(spoint, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline(spoint, spoint) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_from_points';


ALTER FUNCTION public.sline(spoint, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sline(spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline(spoint, spoint) IS 'returns a spherical line using begin ( arg1 ) and end ( arg2 )';


--
-- Name: sline(strans, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline(strans, double precision) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_from_trans';


ALTER FUNCTION public.sline(strans, double precision) OWNER TO sitools;

--
-- Name: FUNCTION sline(strans, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline(strans, double precision) IS 'returns a spherical line using Euler transformation ( arg1 ) and length ( arg2 )';


--
-- Name: sline_contains_point(sline, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_contains_point(sline, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_cont_point';


ALTER FUNCTION public.sline_contains_point(sline, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sline_contains_point(sline, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_contains_point(sline, spoint) IS 'returns true if spherical line contains spherical point';


--
-- Name: sline_contains_point_com(spoint, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_contains_point_com(spoint, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_cont_point_com';


ALTER FUNCTION public.sline_contains_point_com(spoint, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_contains_point_com(spoint, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_contains_point_com(spoint, sline) IS 'returns true if spherical line contains spherical point';


--
-- Name: sline_contains_point_com_neg(spoint, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_contains_point_com_neg(spoint, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_cont_point_com_neg';


ALTER FUNCTION public.sline_contains_point_com_neg(spoint, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_contains_point_com_neg(spoint, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_contains_point_com_neg(spoint, sline) IS 'returns true if spherical line does not contain spherical point';


--
-- Name: sline_contains_point_neg(sline, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_contains_point_neg(sline, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_cont_point_neg';


ALTER FUNCTION public.sline_contains_point_neg(sline, spoint) OWNER TO sitools;

--
-- Name: FUNCTION sline_contains_point_neg(sline, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_contains_point_neg(sline, spoint) IS 'returns true if spherical line does not contain spherical point';


--
-- Name: sline_crosses(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_crosses(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_crosses';


ALTER FUNCTION public.sline_crosses(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_crosses(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_crosses(sline, sline) IS 'returns true if spherical lines cross';


--
-- Name: sline_crosses_neg(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_crosses_neg(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_crosses_neg';


ALTER FUNCTION public.sline_crosses_neg(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_crosses_neg(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_crosses_neg(sline, sline) IS 'returns true if spherical lines do not cross';


--
-- Name: sline_equal(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_equal(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_equal';


ALTER FUNCTION public.sline_equal(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_equal(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_equal(sline, sline) IS 'returns true, if spherical lines are equal';


--
-- Name: sline_equal_neg(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_equal_neg(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_equal_neg';


ALTER FUNCTION public.sline_equal_neg(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_equal_neg(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_equal_neg(sline, sline) IS 'returns true, if spherical lines are not equal';


--
-- Name: sline_overlap(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap';


ALTER FUNCTION public.sline_overlap(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap(sline, sline) IS 'returns true if spherical lines overlap or cross';


--
-- Name: sline_overlap_circle(sline, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap_circle(sline, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap_circle';


ALTER FUNCTION public.sline_overlap_circle(sline, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap_circle(sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap_circle(sline, scircle) IS 'returns true if spherical line overlaps spherical circle';


--
-- Name: sline_overlap_circle_com(scircle, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap_circle_com(scircle, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap_circle_com';


ALTER FUNCTION public.sline_overlap_circle_com(scircle, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap_circle_com(scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap_circle_com(scircle, sline) IS 'returns true if spherical line overlaps spherical circle';


--
-- Name: sline_overlap_circle_com_neg(scircle, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap_circle_com_neg(scircle, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap_circle_com_neg';


ALTER FUNCTION public.sline_overlap_circle_com_neg(scircle, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap_circle_com_neg(scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap_circle_com_neg(scircle, sline) IS 'returns true if spherical line overlaps spherical circle';


--
-- Name: sline_overlap_circle_neg(sline, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap_circle_neg(sline, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap_circle_neg';


ALTER FUNCTION public.sline_overlap_circle_neg(sline, scircle) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap_circle_neg(sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap_circle_neg(sline, scircle) IS 'returns true if spherical line does not overlap spherical circle';


--
-- Name: sline_overlap_neg(sline, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sline_overlap_neg(sline, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_overlap_neg';


ALTER FUNCTION public.sline_overlap_neg(sline, sline) OWNER TO sitools;

--
-- Name: FUNCTION sline_overlap_neg(sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sline_overlap_neg(sline, sline) IS 'returns true if spherical lines do not overlap or cross';


--
-- Name: spath_add_point_aggr(spath, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_add_point_aggr(spath, spoint) RETURNS spath
    LANGUAGE c IMMUTABLE
    AS '$libdir/pg_sphere', 'spherepath_add_point';


ALTER FUNCTION public.spath_add_point_aggr(spath, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spath_add_point_aggr(spath, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_add_point_aggr(spath, spoint) IS 'adds a spherical point to spherical path. Do not use it standalone!';


--
-- Name: spath_add_points_fin_aggr(spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_add_points_fin_aggr(spath) RETURNS spath
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_add_points_finalize';


ALTER FUNCTION public.spath_add_points_fin_aggr(spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_add_points_fin_aggr(spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_add_points_fin_aggr(spath) IS 'Finalize spherical point adding to spherical path. Do not use it standalone!';


--
-- Name: spath_contains_point(spath, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_contains_point(spath, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_cont_point';


ALTER FUNCTION public.spath_contains_point(spath, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spath_contains_point(spath, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_contains_point(spath, spoint) IS 'true if spherical path contains spherical point';


--
-- Name: spath_contains_point_com(spoint, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_contains_point_com(spoint, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_cont_point_com';


ALTER FUNCTION public.spath_contains_point_com(spoint, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_contains_point_com(spoint, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_contains_point_com(spoint, spath) IS 'true if spherical path contains spherical point';


--
-- Name: spath_contains_point_com_neg(spoint, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_contains_point_com_neg(spoint, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_cont_point_com_neg';


ALTER FUNCTION public.spath_contains_point_com_neg(spoint, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_contains_point_com_neg(spoint, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_contains_point_com_neg(spoint, spath) IS 'true if spherical path does not contain spherical point';


--
-- Name: spath_contains_point_neg(spath, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_contains_point_neg(spath, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_cont_point_neg';


ALTER FUNCTION public.spath_contains_point_neg(spath, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spath_contains_point_neg(spath, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_contains_point_neg(spath, spoint) IS 'true if spherical path does not contain spherical point';


--
-- Name: spath_equal(spath, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_equal(spath, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_equal';


ALTER FUNCTION public.spath_equal(spath, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_equal(spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_equal(spath, spath) IS 'returns true, if spherical pathes are equal';


--
-- Name: spath_equal_neg(spath, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_equal_neg(spath, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_equal_neg';


ALTER FUNCTION public.spath_equal_neg(spath, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_equal_neg(spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_equal_neg(spath, spath) IS 'returns true, if spherical pathes are equal';


--
-- Name: spath_overlap_line(spath, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_line(spath, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_line';


ALTER FUNCTION public.spath_overlap_line(spath, sline) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_line(spath, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_line(spath, sline) IS 'true if spherical path overlaps spherical line ';


--
-- Name: spath_overlap_line_com(sline, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_line_com(sline, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_line_com';


ALTER FUNCTION public.spath_overlap_line_com(sline, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_line_com(sline, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_line_com(sline, spath) IS 'true if spherical path overlaps spherical line ';


--
-- Name: spath_overlap_line_com_neg(sline, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_line_com_neg(sline, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_line_com_neg';


ALTER FUNCTION public.spath_overlap_line_com_neg(sline, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_line_com_neg(sline, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_line_com_neg(sline, spath) IS 'true if spherical path does not overlap spherical line ';


--
-- Name: spath_overlap_line_neg(spath, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_line_neg(spath, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_line_neg';


ALTER FUNCTION public.spath_overlap_line_neg(spath, sline) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_line_neg(spath, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_line_neg(spath, sline) IS 'true if spherical path does not overlap spherical line ';


--
-- Name: spath_overlap_path(spath, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_path(spath, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_path';


ALTER FUNCTION public.spath_overlap_path(spath, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_path(spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_path(spath, spath) IS 'true if spherical path overlaps spherical path ';


--
-- Name: spath_overlap_path_neg(spath, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spath_overlap_path_neg(spath, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_overlap_path_neg';


ALTER FUNCTION public.spath_overlap_path_neg(spath, spath) OWNER TO sitools;

--
-- Name: FUNCTION spath_overlap_path_neg(spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spath_overlap_path_neg(spath, spath) IS 'true if spherical path does not overlap spherical path ';


--
-- Name: spoint(double precision, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint(double precision, double precision) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_from_long_lat';


ALTER FUNCTION public.spoint(double precision, double precision) OWNER TO sitools;

--
-- Name: FUNCTION spoint(double precision, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint(double precision, double precision) IS 'returns a spherical point from longitude ( arg1 ) , latitude ( arg2 )';


--
-- Name: spoint(spath, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint(spath, double precision) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_point';


ALTER FUNCTION public.spoint(spath, double precision) OWNER TO sitools;

--
-- Name: FUNCTION spoint(spath, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint(spath, double precision) IS 'returns n-th point of spherical path using linear interpolation';


--
-- Name: spoint(spath, integer); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint(spath, integer) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_get_point';


ALTER FUNCTION public.spoint(spath, integer) OWNER TO sitools;

--
-- Name: FUNCTION spoint(spath, integer); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint(spath, integer) IS 'returns n-th point of spherical path';


--
-- Name: spoint_contained_by_circle(spoint, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_contained_by_circle(spoint, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_in_circle';


ALTER FUNCTION public.spoint_contained_by_circle(spoint, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoint_contained_by_circle(spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_contained_by_circle(spoint, scircle) IS 'true if spherical point is contained by spherical circle';


--
-- Name: spoint_contained_by_circle_com(scircle, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_contained_by_circle_com(scircle, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_in_circle_com';


ALTER FUNCTION public.spoint_contained_by_circle_com(scircle, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoint_contained_by_circle_com(scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_contained_by_circle_com(scircle, spoint) IS 'true if spherical circle contains spherical point ';


--
-- Name: spoint_contained_by_circle_com_neg(scircle, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_contained_by_circle_com_neg(scircle, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_in_circle_com_neg';


ALTER FUNCTION public.spoint_contained_by_circle_com_neg(scircle, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoint_contained_by_circle_com_neg(scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_contained_by_circle_com_neg(scircle, spoint) IS 'true if spherical circle does not contain spherical point ';


--
-- Name: spoint_contained_by_circle_neg(spoint, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_contained_by_circle_neg(spoint, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_in_circle_neg';


ALTER FUNCTION public.spoint_contained_by_circle_neg(spoint, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoint_contained_by_circle_neg(spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_contained_by_circle_neg(spoint, scircle) IS 'true if spherical point is not contained by spherical circle ';


--
-- Name: spoint_equal(spoint, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_equal(spoint, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_equal';


ALTER FUNCTION public.spoint_equal(spoint, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoint_equal(spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_equal(spoint, spoint) IS 'returns true, if spherical points are equal';


--
-- Name: spoint_equal_neg(spoint, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoint_equal_neg(spoint, spoint) RETURNS boolean
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$SELECT NOT spoint_equal($1,$2);$_$;


ALTER FUNCTION public.spoint_equal_neg(spoint, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoint_equal_neg(spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoint_equal_neg(spoint, spoint) IS 'returns true, if spherical points are not equal';


--
-- Name: spoly_add_point_aggr(spoly, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_add_point_aggr(spoly, spoint) RETURNS spoly
    LANGUAGE c IMMUTABLE
    AS '$libdir/pg_sphere', 'spherepoly_add_point';


ALTER FUNCTION public.spoly_add_point_aggr(spoly, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoly_add_point_aggr(spoly, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_add_point_aggr(spoly, spoint) IS 'adds a spherical point to spherical polygon. Do not use it standalone!';


--
-- Name: spoly_add_points_fin_aggr(spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_add_points_fin_aggr(spoly) RETURNS spoly
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_add_points_finalize';


ALTER FUNCTION public.spoly_add_points_fin_aggr(spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_add_points_fin_aggr(spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_add_points_fin_aggr(spoly) IS 'Finalize spherical point adding to spherical polygon. Do not use it standalone!';


--
-- Name: spoly_contains_box(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_box(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_box';


ALTER FUNCTION public.spoly_contains_box(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_box(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_box(spoly, sbox) IS 'true if spherical polygon contains spherical box ';


--
-- Name: spoly_contains_box_com(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_box_com(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_box_com';


ALTER FUNCTION public.spoly_contains_box_com(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_box_com(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_box_com(sbox, spoly) IS 'true if spherical polygon contains spherical box ';


--
-- Name: spoly_contains_box_com_neg(sbox, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_box_com_neg(sbox, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_box_com_neg';


ALTER FUNCTION public.spoly_contains_box_com_neg(sbox, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_box_com_neg(sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_box_com_neg(sbox, spoly) IS 'true if spherical polygon does not contain spherical box ';


--
-- Name: spoly_contains_box_neg(spoly, sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_box_neg(spoly, sbox) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_box_neg';


ALTER FUNCTION public.spoly_contains_box_neg(spoly, sbox) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_box_neg(spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_box_neg(spoly, sbox) IS 'true if spherical polygon does not contain spherical box ';


--
-- Name: spoly_contains_circle(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_circle(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_circle';


ALTER FUNCTION public.spoly_contains_circle(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_circle(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_circle(spoly, scircle) IS 'true if spherical polygon contains spherical circle ';


--
-- Name: spoly_contains_circle_com(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_circle_com(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_circle_com';


ALTER FUNCTION public.spoly_contains_circle_com(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_circle_com(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_circle_com(scircle, spoly) IS 'true if spherical polygon contains spherical circle ';


--
-- Name: spoly_contains_circle_com_neg(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_circle_com_neg(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_circle_com_neg';


ALTER FUNCTION public.spoly_contains_circle_com_neg(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_circle_com_neg(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_circle_com_neg(scircle, spoly) IS 'true if spherical polygon does not contain spherical circle ';


--
-- Name: spoly_contains_circle_neg(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_circle_neg(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_circle_neg';


ALTER FUNCTION public.spoly_contains_circle_neg(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_circle_neg(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_circle_neg(spoly, scircle) IS 'true if spherical polygon does not contain spherical circle ';


--
-- Name: spoly_contains_ellipse(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_ellipse(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_ellipse';


ALTER FUNCTION public.spoly_contains_ellipse(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_ellipse(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_ellipse(spoly, sellipse) IS 'true if spherical polygon contains spherical ellipse ';


--
-- Name: spoly_contains_ellipse_com(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_ellipse_com(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_ellipse_com';


ALTER FUNCTION public.spoly_contains_ellipse_com(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_ellipse_com(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_ellipse_com(sellipse, spoly) IS 'true if spherical polygon contains spherical ellipse ';


--
-- Name: spoly_contains_ellipse_com_neg(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_ellipse_com_neg(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_ellipse_com_neg';


ALTER FUNCTION public.spoly_contains_ellipse_com_neg(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_ellipse_com_neg(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_ellipse_com_neg(sellipse, spoly) IS 'true if spherical polygon does not contain spherical ellipse ';


--
-- Name: spoly_contains_ellipse_neg(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_ellipse_neg(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_ellipse_neg';


ALTER FUNCTION public.spoly_contains_ellipse_neg(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_ellipse_neg(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_ellipse_neg(spoly, sellipse) IS 'true if spherical polygon does not contain spherical ellipse ';


--
-- Name: spoly_contains_line(spoly, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_line(spoly, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_line';


ALTER FUNCTION public.spoly_contains_line(spoly, sline) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_line(spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_line(spoly, sline) IS 'true if spherical polygon contains spherical line ';


--
-- Name: spoly_contains_line_com(sline, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_line_com(sline, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_line_com';


ALTER FUNCTION public.spoly_contains_line_com(sline, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_line_com(sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_line_com(sline, spoly) IS 'true if spherical polygon contains spherical line ';


--
-- Name: spoly_contains_line_com_neg(sline, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_line_com_neg(sline, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_line_com_neg';


ALTER FUNCTION public.spoly_contains_line_com_neg(sline, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_line_com_neg(sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_line_com_neg(sline, spoly) IS 'true if spherical polygon does not contain spherical line ';


--
-- Name: spoly_contains_line_neg(spoly, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_line_neg(spoly, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_line_neg';


ALTER FUNCTION public.spoly_contains_line_neg(spoly, sline) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_line_neg(spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_line_neg(spoly, sline) IS 'true if spherical polygon does not contain spherical line ';


--
-- Name: spoly_contains_path(spoly, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_path(spoly, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_path';


ALTER FUNCTION public.spoly_contains_path(spoly, spath) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_path(spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_path(spoly, spath) IS 'true if spherical polygon contains spherical path ';


--
-- Name: spoly_contains_path_com(spath, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_path_com(spath, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_path_com';


ALTER FUNCTION public.spoly_contains_path_com(spath, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_path_com(spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_path_com(spath, spoly) IS 'true if spherical polygon contains spherical path ';


--
-- Name: spoly_contains_path_com_neg(spath, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_path_com_neg(spath, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_path_com_neg';


ALTER FUNCTION public.spoly_contains_path_com_neg(spath, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_path_com_neg(spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_path_com_neg(spath, spoly) IS 'true if spherical polygon does not contain spherical path ';


--
-- Name: spoly_contains_path_neg(spoly, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_path_neg(spoly, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_path_neg';


ALTER FUNCTION public.spoly_contains_path_neg(spoly, spath) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_path_neg(spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_path_neg(spoly, spath) IS 'true if spherical polygon does not contain spherical path ';


--
-- Name: spoly_contains_point(spoly, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_point(spoly, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_point';


ALTER FUNCTION public.spoly_contains_point(spoly, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_point(spoly, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_point(spoly, spoint) IS 'true if spherical polygon contains spherical point ';


--
-- Name: spoly_contains_point_com(spoint, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_point_com(spoint, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_point_com';


ALTER FUNCTION public.spoly_contains_point_com(spoint, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_point_com(spoint, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_point_com(spoint, spoly) IS 'true if spherical polygon contains spherical point ';


--
-- Name: spoly_contains_point_com_neg(spoint, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_point_com_neg(spoint, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_point_com_neg';


ALTER FUNCTION public.spoly_contains_point_com_neg(spoint, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_point_com_neg(spoint, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_point_com_neg(spoint, spoly) IS 'true if spherical polygon does not contain spherical point ';


--
-- Name: spoly_contains_point_neg(spoly, spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_point_neg(spoly, spoint) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_point_neg';


ALTER FUNCTION public.spoly_contains_point_neg(spoly, spoint) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_point_neg(spoly, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_point_neg(spoly, spoint) IS 'true if spherical polygon does not contain spherical point ';


--
-- Name: spoly_contains_polygon(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_polygon(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_poly';


ALTER FUNCTION public.spoly_contains_polygon(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_polygon(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_polygon(spoly, spoly) IS 'true if spherical polygon contains spherical polygon ';


--
-- Name: spoly_contains_polygon_com(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_polygon_com(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_poly_com';


ALTER FUNCTION public.spoly_contains_polygon_com(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_polygon_com(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_polygon_com(spoly, spoly) IS 'true if spherical polygon is contained by spherical polygon ';


--
-- Name: spoly_contains_polygon_com_neg(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_polygon_com_neg(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_poly_com_neg';


ALTER FUNCTION public.spoly_contains_polygon_com_neg(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_polygon_com_neg(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_polygon_com_neg(spoly, spoly) IS 'true if spherical polygon is not contained by spherical polygon ';


--
-- Name: spoly_contains_polygon_neg(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_contains_polygon_neg(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_cont_poly_neg';


ALTER FUNCTION public.spoly_contains_polygon_neg(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_contains_polygon_neg(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_contains_polygon_neg(spoly, spoly) IS 'true if spherical polygon does not contain spherical polygon ';


--
-- Name: spoly_equal(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_equal(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_equal';


ALTER FUNCTION public.spoly_equal(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_equal(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_equal(spoly, spoly) IS 'returns true, if spherical polygons are equal';


--
-- Name: spoly_not_equal(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_not_equal(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_equal_neg';


ALTER FUNCTION public.spoly_not_equal(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_not_equal(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_not_equal(spoly, spoly) IS 'returns true, if spherical polygons are not equal';


--
-- Name: spoly_overlap_circle(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_circle(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_circle';


ALTER FUNCTION public.spoly_overlap_circle(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_circle(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_circle(spoly, scircle) IS 'true if spherical circle overlap spherical polygon ';


--
-- Name: spoly_overlap_circle_com(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_circle_com(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_circle_com';


ALTER FUNCTION public.spoly_overlap_circle_com(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_circle_com(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_circle_com(scircle, spoly) IS 'true if spherical circle overlap spherical polygon ';


--
-- Name: spoly_overlap_circle_com_neg(scircle, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_circle_com_neg(scircle, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_circle_com_neg';


ALTER FUNCTION public.spoly_overlap_circle_com_neg(scircle, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_circle_com_neg(scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_circle_com_neg(scircle, spoly) IS 'true if spherical circle does not overlap spherical polygon ';


--
-- Name: spoly_overlap_circle_neg(spoly, scircle); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_circle_neg(spoly, scircle) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_circle_neg';


ALTER FUNCTION public.spoly_overlap_circle_neg(spoly, scircle) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_circle_neg(spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_circle_neg(spoly, scircle) IS 'true if spherical circle does not overlap spherical polygon ';


--
-- Name: spoly_overlap_ellipse(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_ellipse(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_ellipse';


ALTER FUNCTION public.spoly_overlap_ellipse(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_ellipse(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_ellipse(spoly, sellipse) IS 'true if spherical ellipse overlap spherical polygon ';


--
-- Name: spoly_overlap_ellipse_com(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_ellipse_com(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_ellipse_com';


ALTER FUNCTION public.spoly_overlap_ellipse_com(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_ellipse_com(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_ellipse_com(sellipse, spoly) IS 'true if spherical ellipse overlap spherical polygon ';


--
-- Name: spoly_overlap_ellipse_com_neg(sellipse, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_ellipse_com_neg(sellipse, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_ellipse_com_neg';


ALTER FUNCTION public.spoly_overlap_ellipse_com_neg(sellipse, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_ellipse_com_neg(sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_ellipse_com_neg(sellipse, spoly) IS 'true if spherical ellipse does not overlap spherical polygon ';


--
-- Name: spoly_overlap_ellipse_neg(spoly, sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_ellipse_neg(spoly, sellipse) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_ellipse_neg';


ALTER FUNCTION public.spoly_overlap_ellipse_neg(spoly, sellipse) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_ellipse_neg(spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_ellipse_neg(spoly, sellipse) IS 'true if spherical ellipse does not overlap spherical polygon ';


--
-- Name: spoly_overlap_line(spoly, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_line(spoly, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_line';


ALTER FUNCTION public.spoly_overlap_line(spoly, sline) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_line(spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_line(spoly, sline) IS 'true if spherical line overlap spherical polygon ';


--
-- Name: spoly_overlap_line_com(sline, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_line_com(sline, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_line_com';


ALTER FUNCTION public.spoly_overlap_line_com(sline, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_line_com(sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_line_com(sline, spoly) IS 'true if spherical line overlap spherical polygon ';


--
-- Name: spoly_overlap_line_com_neg(sline, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_line_com_neg(sline, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_line_com_neg';


ALTER FUNCTION public.spoly_overlap_line_com_neg(sline, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_line_com_neg(sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_line_com_neg(sline, spoly) IS 'true if spherical line does not overlap spherical polygon ';


--
-- Name: spoly_overlap_line_neg(spoly, sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_line_neg(spoly, sline) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_line_neg';


ALTER FUNCTION public.spoly_overlap_line_neg(spoly, sline) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_line_neg(spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_line_neg(spoly, sline) IS 'true if spherical line does not overlap spherical polygon ';


--
-- Name: spoly_overlap_path(spoly, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_path(spoly, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_path';


ALTER FUNCTION public.spoly_overlap_path(spoly, spath) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_path(spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_path(spoly, spath) IS 'true if spherical polygon overlap spherical path ';


--
-- Name: spoly_overlap_path_com(spath, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_path_com(spath, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_path_com';


ALTER FUNCTION public.spoly_overlap_path_com(spath, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_path_com(spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_path_com(spath, spoly) IS 'true if spherical polygon overlap spherical path ';


--
-- Name: spoly_overlap_path_com_neg(spath, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_path_com_neg(spath, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_path_com_neg';


ALTER FUNCTION public.spoly_overlap_path_com_neg(spath, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_path_com_neg(spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_path_com_neg(spath, spoly) IS 'true if spherical polygon overlap spherical path ';


--
-- Name: spoly_overlap_path_neg(spoly, spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_path_neg(spoly, spath) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_path_neg';


ALTER FUNCTION public.spoly_overlap_path_neg(spoly, spath) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_path_neg(spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_path_neg(spoly, spath) IS 'true if spherical polygon does not overlap spherical path ';


--
-- Name: spoly_overlap_polygon(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_polygon(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_poly';


ALTER FUNCTION public.spoly_overlap_polygon(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_polygon(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_polygon(spoly, spoly) IS 'true if spherical polygon overlaps spherical polygon ';


--
-- Name: spoly_overlap_polygon_neg(spoly, spoly); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION spoly_overlap_polygon_neg(spoly, spoly) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoly_overlap_poly_neg';


ALTER FUNCTION public.spoly_overlap_polygon_neg(spoly, spoly) OWNER TO sitools;

--
-- Name: FUNCTION spoly_overlap_polygon_neg(spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION spoly_overlap_polygon_neg(spoly, spoly) IS 'true if spherical polygon does not overlap spherical polygon';


--
-- Name: srad(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION srad(sellipse) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_rad2';


ALTER FUNCTION public.srad(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION srad(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION srad(sellipse) IS 'small radius of spherical ellipse';


--
-- Name: strans(sellipse); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans(sellipse) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereellipse_trans';


ALTER FUNCTION public.strans(sellipse) OWNER TO sitools;

--
-- Name: FUNCTION strans(sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans(sellipse) IS 'returns Euler transformation of spherical ellipse';


--
-- Name: strans(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans(sline) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_from_line';


ALTER FUNCTION public.strans(sline) OWNER TO sitools;

--
-- Name: FUNCTION strans(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans(sline) IS 'converts line to a transformation (ZXZ)';


--
-- Name: strans(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans(strans) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans';


ALTER FUNCTION public.strans(strans) OWNER TO sitools;

--
-- Name: FUNCTION strans(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans(strans) IS 'returns Euler transformation';


--
-- Name: strans(double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans(double precision, double precision, double precision) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_from_float8';


ALTER FUNCTION public.strans(double precision, double precision, double precision) OWNER TO sitools;

--
-- Name: FUNCTION strans(double precision, double precision, double precision); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans(double precision, double precision, double precision) IS 'returns an transformation object using Euler angles (ZXZ)';


--
-- Name: strans(double precision, double precision, double precision, cstring); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans(double precision, double precision, double precision, cstring) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_from_float8_and_type';


ALTER FUNCTION public.strans(double precision, double precision, double precision, cstring) OWNER TO sitools;

--
-- Name: FUNCTION strans(double precision, double precision, double precision, cstring); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans(double precision, double precision, double precision, cstring) IS 'returns an transformation object using Euler angles and axis';


--
-- Name: strans_circle(scircle, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_circle(scircle, strans) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_circle';


ALTER FUNCTION public.strans_circle(scircle, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_circle(scircle, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_circle(scircle, strans) IS 'returns a transformated spherical circle';


--
-- Name: strans_circle_inverse(scircle, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_circle_inverse(scircle, strans) RETURNS scircle
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_circle_inverse';


ALTER FUNCTION public.strans_circle_inverse(scircle, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_circle_inverse(scircle, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_circle_inverse(scircle, strans) IS 'returns a inverse transformated spherical circle';


--
-- Name: strans_ellipse(sellipse, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_ellipse(sellipse, strans) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_ellipse';


ALTER FUNCTION public.strans_ellipse(sellipse, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_ellipse(sellipse, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_ellipse(sellipse, strans) IS 'returns a transformated spherical ellipse';


--
-- Name: strans_ellipse_inverse(sellipse, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_ellipse_inverse(sellipse, strans) RETURNS sellipse
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_ellipse_inv';


ALTER FUNCTION public.strans_ellipse_inverse(sellipse, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_ellipse_inverse(sellipse, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_ellipse_inverse(sellipse, strans) IS 'returns a inverse transformated spherical ellipse';


--
-- Name: strans_equal(strans, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_equal(strans, strans) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_equal';


ALTER FUNCTION public.strans_equal(strans, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_equal(strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_equal(strans, strans) IS 'returns true, if Euler transformations are equal';


--
-- Name: strans_invert(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_invert(strans) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_invert';


ALTER FUNCTION public.strans_invert(strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_invert(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_invert(strans) IS 'returns inverse Euler transformation';


--
-- Name: strans_line(sline, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_line(sline, strans) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_line';


ALTER FUNCTION public.strans_line(sline, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_line(sline, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_line(sline, strans) IS 'returns a transformated spherical line';


--
-- Name: strans_line_inverse(sline, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_line_inverse(sline, strans) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_line_inverse';


ALTER FUNCTION public.strans_line_inverse(sline, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_line_inverse(sline, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_line_inverse(sline, strans) IS 'returns a inverse transformated spherical line';


--
-- Name: strans_not_equal(strans, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_not_equal(strans, strans) RETURNS boolean
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_not_equal';


ALTER FUNCTION public.strans_not_equal(strans, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_not_equal(strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_not_equal(strans, strans) IS 'returns true, if Euler transformations are not equal';


--
-- Name: strans_path(spath, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_path(spath, strans) RETURNS spath
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_path';


ALTER FUNCTION public.strans_path(spath, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_path(spath, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_path(spath, strans) IS 'returns a transformated spherical path';


--
-- Name: strans_path_inverse(spath, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_path_inverse(spath, strans) RETURNS spath
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_path_inverse';


ALTER FUNCTION public.strans_path_inverse(spath, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_path_inverse(spath, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_path_inverse(spath, strans) IS 'returns a inverse transformated spherical path';


--
-- Name: strans_point(spoint, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_point(spoint, strans) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_point';


ALTER FUNCTION public.strans_point(spoint, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_point(spoint, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_point(spoint, strans) IS 'returns a transformated spherical point';


--
-- Name: strans_point_inverse(spoint, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_point_inverse(spoint, strans) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_point_inverse';


ALTER FUNCTION public.strans_point_inverse(spoint, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_point_inverse(spoint, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_point_inverse(spoint, strans) IS 'returns a inverse transformated spherical point';


--
-- Name: strans_poly(spoly, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_poly(spoly, strans) RETURNS spoly
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_poly';


ALTER FUNCTION public.strans_poly(spoly, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_poly(spoly, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_poly(spoly, strans) IS 'returns a transformated spherical polygon';


--
-- Name: strans_poly_inverse(spoly, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_poly_inverse(spoly, strans) RETURNS spoly
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_poly_inverse';


ALTER FUNCTION public.strans_poly_inverse(spoly, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_poly_inverse(spoly, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_poly_inverse(spoly, strans) IS 'returns a inverse transformated spherical polygon';


--
-- Name: strans_trans(strans, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_trans(strans, strans) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_trans';


ALTER FUNCTION public.strans_trans(strans, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_trans(strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_trans(strans, strans) IS 'returns a transformated Euler transformation';


--
-- Name: strans_trans_inv(strans, strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_trans_inv(strans, strans) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_trans_inv';


ALTER FUNCTION public.strans_trans_inv(strans, strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_trans_inv(strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_trans_inv(strans, strans) IS 'returns a inverse transformated Euler transformation';


--
-- Name: strans_zxz(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION strans_zxz(strans) RETURNS strans
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_zxz';


ALTER FUNCTION public.strans_zxz(strans) OWNER TO sitools;

--
-- Name: FUNCTION strans_zxz(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION strans_zxz(strans) IS 'returns Euler transformation as ZXZ transformation';


--
-- Name: sw(sbox); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION sw(sbox) RETURNS spoint
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherebox_sw';


ALTER FUNCTION public.sw(sbox) OWNER TO sitools;

--
-- Name: FUNCTION sw(sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION sw(sbox) IS 'south-west corner of spherical box';


--
-- Name: swap(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION swap(sline) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_swap_beg_end';


ALTER FUNCTION public.swap(sline) OWNER TO sitools;

--
-- Name: FUNCTION swap(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION swap(sline) IS 'returns a spherical line with swapped begin and end';


--
-- Name: swap(spath); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION swap(spath) RETURNS spath
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepath_swap';


ALTER FUNCTION public.swap(spath) OWNER TO sitools;

--
-- Name: FUNCTION swap(spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION swap(spath) IS 'returns a swapped spherical path (changed direction)';


--
-- Name: theta(strans); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION theta(strans) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spheretrans_theta';


ALTER FUNCTION public.theta(strans) OWNER TO sitools;

--
-- Name: FUNCTION theta(strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION theta(strans) IS 'returns the second angle of Euler angles of a transformation object';


--
-- Name: turn(sline); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION turn(sline) RETURNS sline
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'sphereline_turn';


ALTER FUNCTION public.turn(sline) OWNER TO sitools;

--
-- Name: FUNCTION turn(sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION turn(sline) IS 'returns a turned spherical line but keeps begin and end';


--
-- Name: x(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION x(spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_x';


ALTER FUNCTION public.x(spoint) OWNER TO sitools;

--
-- Name: FUNCTION x(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION x(spoint) IS 'cartesian x value of spherical point';


--
-- Name: xyz(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION xyz(spoint) RETURNS double precision[]
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_xyz';


ALTER FUNCTION public.xyz(spoint) OWNER TO sitools;

--
-- Name: FUNCTION xyz(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION xyz(spoint) IS 'cartesian values of spherical point';


--
-- Name: y(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION y(spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_y';


ALTER FUNCTION public.y(spoint) OWNER TO sitools;

--
-- Name: FUNCTION y(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION y(spoint) IS 'cartesian y value of spherical point';


--
-- Name: z(spoint); Type: FUNCTION; Schema: public; Owner: sitools
--

CREATE FUNCTION z(spoint) RETURNS double precision
    LANGUAGE c IMMUTABLE STRICT
    AS '$libdir/pg_sphere', 'spherepoint_z';


ALTER FUNCTION public.z(spoint) OWNER TO sitools;

--
-- Name: FUNCTION z(spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON FUNCTION z(spoint) IS 'cartesian z value of spherical point';


--
-- Name: spath(spoint); Type: AGGREGATE; Schema: public; Owner: sitools
--

CREATE AGGREGATE spath(spoint) (
    SFUNC = spath_add_point_aggr,
    STYPE = spath,
    FINALFUNC = spath_add_points_fin_aggr
);


ALTER AGGREGATE public.spath(spoint) OWNER TO sitools;

--
-- Name: spoly(spoint); Type: AGGREGATE; Schema: public; Owner: sitools
--

CREATE AGGREGATE spoly(spoint) (
    SFUNC = spoly_add_point_aggr,
    STYPE = spoly,
    FINALFUNC = spoly_add_points_fin_aggr
);


ALTER AGGREGATE public.spoly(spoint) OWNER TO sitools;

--
-- Name: !; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ! (
    PROCEDURE = turn,
    RIGHTARG = sline
);


ALTER OPERATOR public.! (NONE, sline) OWNER TO sitools;

--
-- Name: OPERATOR ! (NONE, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ! (NONE, sline) IS 'turns a spherical line, but keep begin and end,';


--
-- Name: !#; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !# (
    PROCEDURE = sline_crosses_neg,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = !#,
    NEGATOR = #,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!# (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR !# (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !# (sline, sline) IS 'true, if spherical lines do not cross';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = scircle_overlap_neg,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, scircle) IS 'true if spherical circles do not overlap';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sline_overlap,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, sline) IS 'true if spherical lines do not overlap or cross';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sline_overlap_circle_neg,
    LEFTARG = sline,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, scircle) IS 'true if spherical line does not overlap spherical circle';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sline_overlap_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, sline) IS 'true if spherical line does not overlap spherical circle';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_ellipse_neg,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, sellipse) IS 'true if spherical ellipse does not overlap spherical ellipse';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_circle_neg,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, scircle) IS 'true if spherical circle does not overlap spherical ellipse';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, sellipse) IS 'true if spherical circle does not overlap spherical ellipse';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_line_neg,
    LEFTARG = sellipse,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, sline) IS 'true if spherical line does not overlap spherical ellipse';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, sellipse) IS 'true if spherical line does not overlap spherical ellipse';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_polygon,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, spoly) IS 'true if spherical polygon does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_circle_neg,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, scircle) IS 'true if spherical circle does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, spoly) IS 'true if spherical circle does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_line_neg,
    LEFTARG = spoly,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, sline) IS 'true if spherical line does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, spoly) IS 'true if spherical line does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_ellipse_neg,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, sellipse) IS 'true if spherical ellipse does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, spoly) IS 'true if spherical ellipse does not overlap spherical polygon';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spath_overlap_path,
    LEFTARG = spath,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, spath) IS 'true if spherical path does not overlap spherical path ';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = scircle_overlap_path_neg,
    LEFTARG = scircle,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, spath) IS 'true if spherical circle does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = scircle_overlap_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, scircle) IS 'true if spherical circle does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spath_overlap_line_neg,
    LEFTARG = spath,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, sline) IS 'true if spherical line does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spath_overlap_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, spath) IS 'true if spherical line does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_path_neg,
    LEFTARG = sellipse,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, spath) IS 'true if spherical ellipse does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sellipse_overlap_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, sellipse) IS 'true if spherical ellipse does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_path_neg,
    LEFTARG = spoly,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, spath) IS 'true if spherical polygon does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = spoly_overlap_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, spoly) IS 'true if spherical polygon does not overlap spherical path';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_box_neg,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, sbox) IS 'true if spherical box does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_circle_neg,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, scircle) IS 'true if spherical circle does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (scircle, sbox) IS 'true if spherical circle does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_line_neg,
    LEFTARG = sbox,
    RIGHTARG = sline,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, sline) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, sline) IS 'true if spherical line does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sline, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sline, sbox) IS 'true if spherical line does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_ellipse_neg,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, sellipse) IS 'true if spherical ellipse does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sellipse, sbox) IS 'true if spherical ellipse does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_poly_neg,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, spoly) IS 'true if spherical polygon does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_poly_com_neg,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spoly, sbox) IS 'true if spherical polygon does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_path_neg,
    LEFTARG = sbox,
    RIGHTARG = spath,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (sbox, spath) OWNER TO sitools;

--
-- Name: OPERATOR !&& (sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (sbox, spath) IS 'true if spherical path does not overlap spherical box';


--
-- Name: !&&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !&& (
    PROCEDURE = sbox_overlap_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = sbox,
    COMMUTATOR = !&&,
    NEGATOR = &&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!&& (spath, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !&& (spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !&& (spath, sbox) IS 'true if spherical path does not overlap spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contained_by_circle_neg,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (scircle, scircle) IS 'true if spherical circle is not contained by spherical circle';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoint_contained_by_circle_neg,
    LEFTARG = spoint,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, scircle) IS 'true if spherical point is not contained by spherical circle';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sline_contains_point_com_neg,
    LEFTARG = spoint,
    RIGHTARG = sline,
    COMMUTATOR = !~,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, sline) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, sline) IS 'true if spherical line does not contain spherical point';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contains_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sline, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sline, scircle) IS 'true if spherical circle does not contain spherical line';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sellipse, sellipse) IS 'true if spherical ellipse is not contained by spherical ellipse';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_point_com_neg,
    LEFTARG = spoint,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, sellipse) IS 'true if spherical ellipse does not contain spherical point';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (scircle, sellipse) IS 'true if spherical ellipse does not contain spherical circle';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contains_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sellipse, scircle) IS 'true if spherical circle does not contain spherical ellipse';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sline, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sline, sellipse) IS 'true if spherical ellipse does not contain spherical line';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_polygon_com_neg,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoly, spoly) IS 'true if spherical polygon is not contained by spherical polygon';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_point_com_neg,
    LEFTARG = spoint,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, spoly) IS 'true if spherical polygon does not contain spherical point';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (scircle, spoly) IS 'true if spherical polygon does not contain spherical circle';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contains_polygon_com_neg,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoly, scircle) IS 'true if spherical circle does not contain spherical polygon';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sline, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sline, spoly) IS 'true if spherical polygon does not contain spherical line';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sellipse, spoly) IS 'true if spherical polygon does not contain spherical ellipse';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_polygon_com_neg,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoly, sellipse) IS 'true if spherical ellipse does not contain spherical polygon';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spath_contains_point_com_neg,
    LEFTARG = spoint,
    RIGHTARG = spath,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, spath) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, spath) IS 'true if spherical path does not contain spherical point';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contains_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spath, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spath, scircle) IS 'true if spherical circle does not contain spherical path';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spath, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spath, sellipse) IS 'true if spherical ellipse does not contain spherical path';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spath, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spath, spoly) IS 'true if spherical polygon does not contain spherical path';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_box_com_neg,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sbox, sbox) IS 'true if spherical box does not contain spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_cont_point_com_neg,
    LEFTARG = spoint,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoint, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoint, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoint, sbox) IS 'true if spherical point is not contained by spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (scircle, sbox) IS 'true if spherical box does not contain spherical circle';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = scircle_contains_box_com_neg,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sbox, scircle) IS 'true if spherical circle does not contain spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_line_com_neg,
    LEFTARG = sline,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sline, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sline, sbox) IS 'true if spherical box does not contain spherical line';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_ellipse_com_neg,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sellipse, sbox) IS 'true if spherical box does not contain spherical ellipse';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sellipse_contains_box_com_neg,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sbox, sellipse) IS 'true if spherical ellipse does not contain spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_poly_com_neg,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spoly, sbox) IS 'true if spherical box does not contain spherical polygon';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = spoly_contains_box_com_neg,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !@ (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (sbox, spoly) IS 'true if spherical polygon does not contain spherical box';


--
-- Name: !@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !@ (
    PROCEDURE = sbox_contains_path_com_neg,
    LEFTARG = spath,
    RIGHTARG = sbox,
    COMMUTATOR = !~,
    NEGATOR = @,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!@ (spath, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !@ (spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !@ (spath, sbox) IS 'true if spherical box does not contain spherical path';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_circle_neg,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, scircle) IS 'true if spherical circle does not contain spherical circle';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoint_contained_by_circle_com_neg,
    LEFTARG = scircle,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, spoint) IS 'true if spherical circle does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sline_contains_point,
    LEFTARG = sline,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sline, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sline, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sline, spoint) IS 'true if spherical line does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_line_neg,
    LEFTARG = scircle,
    RIGHTARG = sline,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, sline) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, sline) IS 'true if spherical circle does not contain spherical line';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_ellipse_neg,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, sellipse) IS 'true if spherical ellipse does not contain spherical ellipse';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_point_neg,
    LEFTARG = sellipse,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, spoint) IS 'true if spherical ellipse does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_circle_neg,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, scircle) IS 'true if spherical ellipse does not contain spherical circle';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_ellipse_neg,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, sellipse) IS 'true if spherical circle does not contain spherical ellipse';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_line_neg,
    LEFTARG = sellipse,
    RIGHTARG = sline,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, sline) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, sline) IS 'true if spherical ellipse does not contain spherical line';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_polygon_neg,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, spoly) IS 'true if spherical polygon does not contain spherical polygon';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_point,
    LEFTARG = spoly,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, spoint) IS 'true if spherical polygon does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_circle_neg,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, scircle) IS 'true if spherical polygon does not contain spherical circle';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_polygon_neg,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, spoly) IS 'true if spherical circle does not contain spherical polygon';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_line,
    LEFTARG = spoly,
    RIGHTARG = sline,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, sline) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, sline) IS 'true if spherical polygon does not contain spherical line';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_ellipse_neg,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, sellipse) IS 'true if spherical polygon does not contain spherical ellipse';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_polygon_neg,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, spoly) IS 'true if spherical ellipse does not contain spherical polygon';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spath_contains_point_neg,
    LEFTARG = spath,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spath, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spath, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spath, spoint) IS 'true if spherical path does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_path_neg,
    LEFTARG = scircle,
    RIGHTARG = spath,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, spath) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, spath) IS 'true if spherical circle contains spherical path';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_path_neg,
    LEFTARG = sellipse,
    RIGHTARG = spath,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, spath) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, spath) IS 'true if spherical ellipse contains spherical path';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_path_neg,
    LEFTARG = spoly,
    RIGHTARG = spath,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, spath) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, spath) IS 'true if spherical polygon contains spherical path';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_box_neg,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, sbox) IS 'true if spherical box does not contain spherical box';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_cont_point_neg,
    LEFTARG = sbox,
    RIGHTARG = spoint,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, spoint) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, spoint) IS 'true if spherical box does not contain spherical point';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_circle_neg,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, scircle) IS 'true if spherical box does not contain spherical circle';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = scircle_contains_box_neg,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !~ (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (scircle, sbox) IS 'true if spherical circle does not contain spherical box';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_line_neg,
    LEFTARG = sbox,
    RIGHTARG = sline,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, sline) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, sline) IS 'true if spherical box does not contain spherical line';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_ellipse_neg,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, sellipse) IS 'true if spherical box does not contain spherical ellipse';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sellipse_contains_box_neg,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sellipse, sbox) IS 'true if spherical ellipse does not contain spherical box';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_poly_neg,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, spoly) IS 'true if spherical box does not contain spherical polygon';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = spoly_contains_box_neg,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR !~ (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (spoly, sbox) IS 'true if spherical polygon does not contain spherical box';


--
-- Name: !~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR !~ (
    PROCEDURE = sbox_contains_path_neg,
    LEFTARG = sbox,
    RIGHTARG = spath,
    COMMUTATOR = !@,
    NEGATOR = ~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.!~ (sbox, spath) OWNER TO sitools;

--
-- Name: OPERATOR !~ (sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR !~ (sbox, spath) IS 'true if spherical box does not contain spherical path';


--
-- Name: #; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR # (
    PROCEDURE = sline_crosses,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = #,
    NEGATOR = !#,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.# (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR # (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR # (sline, sline) IS 'true, if spherical lines cross';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = scircle_overlap,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, scircle) IS 'true if spherical circles overlap';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sline_overlap,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, sline) IS 'true if spherical line overlap or cross';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sline_overlap_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, sline) IS 'true if spherical line overlaps spherical circle';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sline_overlap_circle,
    LEFTARG = sline,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, scircle) IS 'true if spherical line overlaps spherical circle';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_ellipse,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, sellipse) IS 'true if spherical ellipses overlap ';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, sellipse) IS 'true if spherical circle overlap spherical ellipse';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_circle,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, scircle) IS 'true if spherical circle overlap spherical ellipse';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_line_com,
    LEFTARG = sline,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, sellipse) IS 'true if spherical line overlaps spherical ellipse';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_line,
    LEFTARG = sellipse,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, sline) IS 'true if spherical line overlaps spherical ellipse';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_polygon,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, spoly) IS 'true if spherical polygons overlap ';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_circle_com,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, spoly) IS 'true if spherical circle overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_circle,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, scircle) IS 'true if spherical circle overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_line_com,
    LEFTARG = sline,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, spoly) IS 'true if spherical line overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_line,
    LEFTARG = spoly,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, sline) IS 'true if spherical line overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, spoly) IS 'true if spherical ellipse overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_ellipse,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, sellipse) IS 'true if spherical ellipse overlap spherical polygon';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spath_overlap_path,
    LEFTARG = spath,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, spath) IS 'true if spherical pathes overlap ';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = scircle_overlap_path_com,
    LEFTARG = spath,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, scircle) IS 'true if spherical circle overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = scircle_overlap_path,
    LEFTARG = scircle,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, spath) IS 'true if spherical circle overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spath_overlap_line_com,
    LEFTARG = sline,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, spath) IS 'true if spherical line overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spath_overlap_line,
    LEFTARG = spath,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, sline) IS 'true if spherical line overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_path_com,
    LEFTARG = spath,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, sellipse) IS 'true if spherical ellipse overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sellipse_overlap_path,
    LEFTARG = sellipse,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, spath) IS 'true if spherical ellipse overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_path_com,
    LEFTARG = spath,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, spoly) IS 'true if spherical polygon overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = spoly_overlap_path,
    LEFTARG = spoly,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, spath) IS 'true if spherical polygon overlap spherical path';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_box,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, sbox) IS 'true if spherical box overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (scircle, sbox) IS 'true if spherical circle overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_circle,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, scircle) IS 'true if spherical circle overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_line_com,
    LEFTARG = sline,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sline, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sline, sbox) IS 'true if spherical line overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_line,
    LEFTARG = sbox,
    RIGHTARG = sline,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, sline) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, sline) IS 'true if spherical line overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sellipse, sbox) IS 'true if spherical ellipse overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_ellipse,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, sellipse) IS 'true if spherical ellipse overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_poly_com,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spoly, sbox) IS 'true if spherical polygon overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_poly,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, spoly) IS 'true if spherical polygon overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_path_com,
    LEFTARG = spath,
    RIGHTARG = sbox,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (spath, sbox) OWNER TO sitools;

--
-- Name: OPERATOR && (spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (spath, sbox) IS 'true if spherical path overlap spherical box';


--
-- Name: &&; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR && (
    PROCEDURE = sbox_overlap_path,
    LEFTARG = sbox,
    RIGHTARG = spath,
    COMMUTATOR = &&,
    NEGATOR = !&&,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.&& (sbox, spath) OWNER TO sitools;

--
-- Name: OPERATOR && (sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR && (sbox, spath) IS 'true if spherical path overlap spherical box';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (NONE, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (NONE, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (NONE, strans) IS 'returns Euler transformation';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_point,
    LEFTARG = spoint,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (spoint, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (spoint, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (spoint, strans) IS 'transforms a spherical point ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_trans,
    LEFTARG = strans,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (strans, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (strans, strans) IS 'transforms a Euler transformation ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_circle,
    LEFTARG = scircle,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (scircle, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (scircle, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (scircle, strans) IS 'transforms a spherical circle ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_line,
    LEFTARG = sline,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (sline, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (sline, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (sline, strans) IS 'transforms a spherical line ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_ellipse,
    LEFTARG = sellipse,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (sellipse, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (sellipse, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (sellipse, strans) IS 'transforms a spherical ellipse ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_poly,
    LEFTARG = spoly,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (spoly, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (spoly, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (spoly, strans) IS 'transforms a spherical polygon ';


--
-- Name: +; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR + (
    PROCEDURE = strans_path,
    LEFTARG = spath,
    RIGHTARG = strans
);


ALTER OPERATOR public.+ (spath, strans) OWNER TO sitools;

--
-- Name: OPERATOR + (spath, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR + (spath, strans) IS 'transforms a spherical path ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_invert,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (NONE, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (NONE, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (NONE, strans) IS 'inverts Euler transformation';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = swap,
    RIGHTARG = sline
);


ALTER OPERATOR public.- (NONE, sline) OWNER TO sitools;

--
-- Name: OPERATOR - (NONE, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (NONE, sline) IS 'swaps begin and and of a spherical line';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = swap,
    RIGHTARG = spath
);


ALTER OPERATOR public.- (NONE, spath) OWNER TO sitools;

--
-- Name: OPERATOR - (NONE, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (NONE, spath) IS 'changes the direction of a spherical path';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_point_inverse,
    LEFTARG = spoint,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (spoint, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (spoint, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (spoint, strans) IS 'transforms inverse a spherical point ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_trans_inv,
    LEFTARG = strans,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (strans, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (strans, strans) IS 'transforms inverse a Euler transformation ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_circle_inverse,
    LEFTARG = scircle,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (scircle, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (scircle, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (scircle, strans) IS 'transforms inverse a spherical circle ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_line_inverse,
    LEFTARG = sline,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (sline, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (sline, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (sline, strans) IS 'transforms inverse a spherical line';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_ellipse_inverse,
    LEFTARG = sellipse,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (sellipse, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (sellipse, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (sellipse, strans) IS 'transforms inverse a spherical ellipse ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_poly_inverse,
    LEFTARG = spoly,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (spoly, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (spoly, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (spoly, strans) IS 'transforms inverse a spherical polygon ';


--
-- Name: -; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR - (
    PROCEDURE = strans_path_inverse,
    LEFTARG = spath,
    RIGHTARG = strans
);


ALTER OPERATOR public.- (spath, strans) OWNER TO sitools;

--
-- Name: OPERATOR - (spath, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR - (spath, strans) IS 'transforms inverse a spherical path ';


--
-- Name: <->; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <-> (
    PROCEDURE = dist,
    LEFTARG = spoint,
    RIGHTARG = spoint,
    COMMUTATOR = <->
);


ALTER OPERATOR public.<-> (spoint, spoint) OWNER TO sitools;

--
-- Name: OPERATOR <-> (spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <-> (spoint, spoint) IS 'distance between spherical points';


--
-- Name: <->; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <-> (
    PROCEDURE = dist,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = <->
);


ALTER OPERATOR public.<-> (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR <-> (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <-> (scircle, scircle) IS 'distance between two spherical circles';


--
-- Name: <->; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <-> (
    PROCEDURE = dist,
    LEFTARG = spoint,
    RIGHTARG = scircle,
    COMMUTATOR = <->
);


ALTER OPERATOR public.<-> (spoint, scircle) OWNER TO sitools;

--
-- Name: OPERATOR <-> (spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <-> (spoint, scircle) IS 'distance between spherical circle and spherical point';


--
-- Name: <->; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <-> (
    PROCEDURE = dist,
    LEFTARG = scircle,
    RIGHTARG = spoint,
    COMMUTATOR = <->
);


ALTER OPERATOR public.<-> (scircle, spoint) OWNER TO sitools;

--
-- Name: OPERATOR <-> (scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <-> (scircle, spoint) IS 'distance between spherical circle and spherical point';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = spoint_equal_neg,
    LEFTARG = spoint,
    RIGHTARG = spoint,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (spoint, spoint) OWNER TO sitools;

--
-- Name: OPERATOR <> (spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (spoint, spoint) IS 'true, if spherical points are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = strans_not_equal,
    LEFTARG = strans,
    RIGHTARG = strans,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (strans, strans) OWNER TO sitools;

--
-- Name: OPERATOR <> (strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (strans, strans) IS 'true, if spherical Euler transformations are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = scircle_equal_neg,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR <> (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (scircle, scircle) IS 'true, if spherical circles are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = sline_equal_neg,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR <> (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (sline, sline) IS 'true, if spherical lines are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = sellipse_equal_neg,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR <> (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (sellipse, sellipse) IS 'true, if spherical ellipses are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = spoly_not_equal,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR <> (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (spoly, spoly) IS 'true, if spherical polygons are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = spath_equal_neg,
    LEFTARG = spath,
    RIGHTARG = spath,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (spath, spath) OWNER TO sitools;

--
-- Name: OPERATOR <> (spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (spath, spath) IS 'true, if spherical pathes are not equal';


--
-- Name: <>; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR <> (
    PROCEDURE = sbox_equal_neg,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = <>,
    NEGATOR = =,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.<> (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR <> (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR <> (sbox, sbox) IS 'true, if spherical boxes are not equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = spoint_equal,
    LEFTARG = spoint,
    RIGHTARG = spoint,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (spoint, spoint) OWNER TO sitools;

--
-- Name: OPERATOR = (spoint, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (spoint, spoint) IS 'true, if spherical points are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = strans_equal,
    LEFTARG = strans,
    RIGHTARG = strans,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (strans, strans) OWNER TO sitools;

--
-- Name: OPERATOR = (strans, strans); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (strans, strans) IS 'true, if Euler transformations are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = scircle_equal,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR = (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (scircle, scircle) IS 'true, if spherical circles are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = sline_equal,
    LEFTARG = sline,
    RIGHTARG = sline,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (sline, sline) OWNER TO sitools;

--
-- Name: OPERATOR = (sline, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (sline, sline) IS 'true, if spherical lines are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = sellipse_equal,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR = (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (sellipse, sellipse) IS 'true, if spherical ellipses are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = spoly_equal,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR = (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (spoly, spoly) IS 'true, if spherical polygons are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = spath_equal,
    LEFTARG = spath,
    RIGHTARG = spath,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (spath, spath) OWNER TO sitools;

--
-- Name: OPERATOR = (spath, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (spath, spath) IS 'true, if spherical pathes are equal';


--
-- Name: =; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR = (
    PROCEDURE = sbox_equal,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = =,
    NEGATOR = <>,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.= (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR = (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR = (sbox, sbox) IS 'true, if spherical boxes are equal';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contained_by_circle,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (scircle, scircle) IS 'true if spherical circle is contained by spherical circle';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoint_contained_by_circle,
    LEFTARG = spoint,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, scircle) IS 'true if spherical point is contained by spherical circle';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sline_contains_point_com,
    LEFTARG = spoint,
    RIGHTARG = sline,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, sline) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, sline) IS 'true if spherical line contains spherical point';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contains_line_com,
    LEFTARG = sline,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sline, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (sline, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sline, scircle) IS 'true if spherical circle contains spherical line';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sellipse, sellipse) IS 'true if spherical ellipse is contained by spherical ellipse';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_point_com,
    LEFTARG = spoint,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, sellipse) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (scircle, sellipse) IS 'true if spherical ellipse contains spherical circle';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contains_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sellipse, scircle) IS 'true if spherical circle contains spherical ellipse';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_line_com,
    LEFTARG = sline,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sline, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (sline, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sline, sellipse) IS 'true if spherical ellipse contains spherical line';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_polygon_com,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoly, spoly) IS 'true if spherical polygon is contained by spherical polygon';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_point_com,
    LEFTARG = spoint,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, spoly) IS 'true if spherical polygon contains spherical point';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_circle_com,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (scircle, spoly) IS 'true if spherical polygon contains spherical circle';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contains_polygon_com,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoly, scircle) IS 'true if spherical circle contains spherical polygon';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_line_com,
    LEFTARG = sline,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sline, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (sline, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sline, spoly) IS 'true if spherical polygon contains spherical line';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sellipse, spoly) IS 'true if spherical polygon contains spherical ellipse';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_polygon_com,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoly, sellipse) IS 'true if spherical ellipse contains spherical polygon';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spath_contains_point_com,
    LEFTARG = spoint,
    RIGHTARG = spath,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, spath) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, spath) IS 'true if spherical path contains spherical point';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contains_path_com,
    LEFTARG = spath,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spath, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (spath, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spath, scircle) IS 'true if spherical circle contains spherical path';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_path_com,
    LEFTARG = spath,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spath, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (spath, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spath, sellipse) IS 'true if spherical ellipse contains spherical path';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_path_com,
    LEFTARG = spath,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spath, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (spath, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spath, spoly) IS 'true if spherical polygon contains spherical path';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_box_com,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sbox, sbox) IS 'true if spherical box contains spherical box';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_cont_point_com,
    LEFTARG = spoint,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoint, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoint, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoint, sbox) IS 'true if spherical point is contained by spherical box';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_circle_com,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (scircle, sbox) IS 'true if spherical box contains spherical circle';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = scircle_contains_box_com,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @ (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sbox, scircle) IS 'true if spherical circle contains spherical box';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_line_com,
    LEFTARG = sline,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sline, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (sline, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sline, sbox) IS 'true if spherical box contains spherical line';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_ellipse_com,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sellipse, sbox) IS 'true if spherical box contains spherical ellipse';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sellipse_contains_box_com,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @ (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sbox, sellipse) IS 'true if spherical ellipse contains spherical box';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_poly_com,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spoly, sbox) IS 'true if spherical box contains spherical polygon';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = spoly_contains_box_com,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @ (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (sbox, spoly) IS 'true if spherical polygon contains spherical box';


--
-- Name: @; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @ (
    PROCEDURE = sbox_contains_path_com,
    LEFTARG = spath,
    RIGHTARG = sbox,
    COMMUTATOR = ~,
    NEGATOR = !@,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.@ (spath, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @ (spath, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @ (spath, sbox) IS 'true if spherical box contains spherical path';


--
-- Name: @-@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @-@ (
    PROCEDURE = circum,
    RIGHTARG = scircle
);


ALTER OPERATOR public.@-@ (NONE, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @-@ (NONE, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @-@ (NONE, scircle) IS 'circumference of spherical circle';


--
-- Name: @-@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @-@ (
    PROCEDURE = length,
    RIGHTARG = sline
);


ALTER OPERATOR public.@-@ (NONE, sline) OWNER TO sitools;

--
-- Name: OPERATOR @-@ (NONE, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @-@ (NONE, sline) IS 'length of spherical line';


--
-- Name: @-@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @-@ (
    PROCEDURE = circum,
    RIGHTARG = spoly
);


ALTER OPERATOR public.@-@ (NONE, spoly) OWNER TO sitools;

--
-- Name: OPERATOR @-@ (NONE, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @-@ (NONE, spoly) IS 'returns circumference of spherical polygon';


--
-- Name: @-@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @-@ (
    PROCEDURE = length,
    RIGHTARG = spath
);


ALTER OPERATOR public.@-@ (NONE, spath) OWNER TO sitools;

--
-- Name: OPERATOR @-@ (NONE, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @-@ (NONE, spath) IS 'returns length of spherical path';


--
-- Name: @-@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @-@ (
    PROCEDURE = circum,
    RIGHTARG = sbox
);


ALTER OPERATOR public.@-@ (NONE, sbox) OWNER TO sitools;

--
-- Name: OPERATOR @-@ (NONE, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @-@ (NONE, sbox) IS 'circumference of spherical box';


--
-- Name: @@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @@ (
    PROCEDURE = center,
    RIGHTARG = scircle
);


ALTER OPERATOR public.@@ (NONE, scircle) OWNER TO sitools;

--
-- Name: OPERATOR @@ (NONE, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @@ (NONE, scircle) IS 'center of spherical circle';


--
-- Name: @@; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR @@ (
    PROCEDURE = center,
    RIGHTARG = sellipse
);


ALTER OPERATOR public.@@ (NONE, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR @@ (NONE, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR @@ (NONE, sellipse) IS 'center of spherical ellipse';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_circle,
    LEFTARG = scircle,
    RIGHTARG = scircle,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, scircle) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, scircle) IS 'true if spherical circle contains spherical circle';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoint_contained_by_circle_com,
    LEFTARG = scircle,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, spoint) IS 'true if spherical circle contains spherical point';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sline_contains_point,
    LEFTARG = sline,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sline, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sline, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sline, spoint) IS 'true if spherical line contains spherical point';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_line,
    LEFTARG = scircle,
    RIGHTARG = sline,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, sline) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, sline) IS 'true if spherical circle contains spherical line';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_ellipse,
    LEFTARG = sellipse,
    RIGHTARG = sellipse,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, sellipse) IS 'true if spherical ellipse contains spherical ellipse';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_point,
    LEFTARG = sellipse,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, spoint) IS 'true if spherical ellipse contains spherical point ';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_circle,
    LEFTARG = sellipse,
    RIGHTARG = scircle,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, scircle) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, scircle) IS 'true if spherical ellipse contains spherical circle';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_ellipse,
    LEFTARG = scircle,
    RIGHTARG = sellipse,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, sellipse) IS 'true if spherical circle contains spherical ellipse';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_line,
    LEFTARG = sellipse,
    RIGHTARG = sline,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, sline) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, sline) IS 'true if spherical ellipse contains spherical line';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_polygon,
    LEFTARG = spoly,
    RIGHTARG = spoly,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, spoly) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, spoly) IS 'true if spherical polygon contains spherical polygon';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_point,
    LEFTARG = spoly,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, spoint) IS 'true if spherical polygon contains spherical point';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_circle,
    LEFTARG = spoly,
    RIGHTARG = scircle,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, scircle) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, scircle) IS 'true if spherical polygon contains spherical circle';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_polygon,
    LEFTARG = scircle,
    RIGHTARG = spoly,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, spoly) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, spoly) IS 'true if spherical circle contains spherical polygon';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_line,
    LEFTARG = spoly,
    RIGHTARG = sline,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, sline) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, sline) IS 'true if spherical polygon contains spherical line';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_ellipse,
    LEFTARG = spoly,
    RIGHTARG = sellipse,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, sellipse) IS 'true if spherical polygon contains spherical ellipse';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_polygon,
    LEFTARG = sellipse,
    RIGHTARG = spoly,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, spoly) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, spoly) IS 'true if spherical ellipse contains spherical polygon';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spath_contains_point,
    LEFTARG = spath,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spath, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spath, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spath, spoint) IS 'true if spherical path contains spherical point';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_path,
    LEFTARG = scircle,
    RIGHTARG = spath,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, spath) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, spath) IS 'true if spherical circle contains spherical path';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_path,
    LEFTARG = sellipse,
    RIGHTARG = spath,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, spath) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, spath) IS 'true if spherical ellipse contains spherical path';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_path,
    LEFTARG = spoly,
    RIGHTARG = spath,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, spath) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, spath) IS 'true if spherical polygon contains spherical path';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_box,
    LEFTARG = sbox,
    RIGHTARG = sbox,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, sbox) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, sbox) IS 'true if spherical box contains spherical box';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_cont_point,
    LEFTARG = sbox,
    RIGHTARG = spoint,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, spoint) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, spoint); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, spoint) IS 'true if spherical box contains spherical point';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_circle,
    LEFTARG = sbox,
    RIGHTARG = scircle,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, scircle) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, scircle); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, scircle) IS 'true if spherical box contains spherical circle';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = scircle_contains_box,
    LEFTARG = scircle,
    RIGHTARG = sbox,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (scircle, sbox) OWNER TO sitools;

--
-- Name: OPERATOR ~ (scircle, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (scircle, sbox) IS 'true if spherical circle contains spherical box';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_line,
    LEFTARG = sbox,
    RIGHTARG = sline,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, sline) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, sline); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, sline) IS 'true if spherical box contains spherical line';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_ellipse,
    LEFTARG = sbox,
    RIGHTARG = sellipse,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, sellipse) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, sellipse); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, sellipse) IS 'true if spherical box contains spherical ellipse';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sellipse_contains_box,
    LEFTARG = sellipse,
    RIGHTARG = sbox,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sellipse, sbox) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sellipse, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sellipse, sbox) IS 'true if spherical ellipse contains spherical box';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_poly,
    LEFTARG = sbox,
    RIGHTARG = spoly,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, spoly) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, spoly); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, spoly) IS 'true if spherical box contains spherical polygon';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = spoly_contains_box,
    LEFTARG = spoly,
    RIGHTARG = sbox,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (spoly, sbox) OWNER TO sitools;

--
-- Name: OPERATOR ~ (spoly, sbox); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (spoly, sbox) IS 'true if spherical polygon contains spherical box';


--
-- Name: ~; Type: OPERATOR; Schema: public; Owner: sitools
--

CREATE OPERATOR ~ (
    PROCEDURE = sbox_contains_path,
    LEFTARG = sbox,
    RIGHTARG = spath,
    COMMUTATOR = @,
    NEGATOR = !~,
    RESTRICT = contsel,
    JOIN = contjoinsel
);


ALTER OPERATOR public.~ (sbox, spath) OWNER TO sitools;

--
-- Name: OPERATOR ~ (sbox, spath); Type: COMMENT; Schema: public; Owner: sitools
--

COMMENT ON OPERATOR ~ (sbox, spath) IS 'true if spherical box contains spherical path';


--
-- Name: sbox; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY sbox USING gist;


ALTER OPERATOR FAMILY public.sbox USING gist OWNER TO postgres;

--
-- Name: sbox; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS sbox
    DEFAULT FOR TYPE sbox USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(sbox,sbox) ,
    OPERATOR 11 @(sbox,scircle) ,
    OPERATOR 12 @(sbox,spoly) ,
    OPERATOR 13 @(sbox,sellipse) ,
    OPERATOR 14 @(sbox,sbox) ,
    OPERATOR 21 ~(sbox,spoint) ,
    OPERATOR 22 ~(sbox,scircle) ,
    OPERATOR 23 ~(sbox,sline) ,
    OPERATOR 24 ~(sbox,spath) ,
    OPERATOR 25 ~(sbox,spoly) ,
    OPERATOR 26 ~(sbox,sellipse) ,
    OPERATOR 27 ~(sbox,sbox) ,
    OPERATOR 31 &&(sbox,scircle) ,
    OPERATOR 32 &&(sbox,sline) ,
    OPERATOR 33 &&(sbox,spath) ,
    OPERATOR 34 &&(sbox,spoly) ,
    OPERATOR 35 &&(sbox,sellipse) ,
    OPERATOR 36 &&(sbox,sbox) ,
    FUNCTION 1 (sbox, sbox) g_sbox_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (sbox, sbox) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (sbox, sbox) g_sbox_compress(internal) ,
    FUNCTION 4 (sbox, sbox) g_spherekey_decompress(internal) ,
    FUNCTION 5 (sbox, sbox) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (sbox, sbox) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (sbox, sbox) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.sbox USING gist OWNER TO sitools;

--
-- Name: scircle; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY scircle USING gist;


ALTER OPERATOR FAMILY public.scircle USING gist OWNER TO postgres;

--
-- Name: scircle; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS scircle
    DEFAULT FOR TYPE scircle USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(scircle,scircle) ,
    OPERATOR 11 @(scircle,scircle) ,
    OPERATOR 12 @(scircle,spoly) ,
    OPERATOR 13 @(scircle,sellipse) ,
    OPERATOR 14 @(scircle,sbox) ,
    OPERATOR 21 ~(scircle,spoint) ,
    OPERATOR 22 ~(scircle,scircle) ,
    OPERATOR 23 ~(scircle,sline) ,
    OPERATOR 24 ~(scircle,spath) ,
    OPERATOR 25 ~(scircle,spoly) ,
    OPERATOR 26 ~(scircle,sellipse) ,
    OPERATOR 27 ~(scircle,sbox) ,
    OPERATOR 31 &&(scircle,scircle) ,
    OPERATOR 32 &&(scircle,sline) ,
    OPERATOR 33 &&(scircle,spath) ,
    OPERATOR 34 &&(scircle,spoly) ,
    OPERATOR 35 &&(scircle,sellipse) ,
    OPERATOR 36 &&(scircle,sbox) ,
    FUNCTION 1 (scircle, scircle) g_scircle_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (scircle, scircle) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (scircle, scircle) g_scircle_compress(internal) ,
    FUNCTION 4 (scircle, scircle) g_spherekey_decompress(internal) ,
    FUNCTION 5 (scircle, scircle) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (scircle, scircle) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (scircle, scircle) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.scircle USING gist OWNER TO sitools;

--
-- Name: sellipse; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY sellipse USING gist;


ALTER OPERATOR FAMILY public.sellipse USING gist OWNER TO postgres;

--
-- Name: sellipse; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS sellipse
    DEFAULT FOR TYPE sellipse USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(sellipse,sellipse) ,
    OPERATOR 11 @(sellipse,scircle) ,
    OPERATOR 12 @(sellipse,spoly) ,
    OPERATOR 13 @(sellipse,sellipse) ,
    OPERATOR 14 @(sellipse,sbox) ,
    OPERATOR 21 ~(sellipse,spoint) ,
    OPERATOR 22 ~(sellipse,scircle) ,
    OPERATOR 23 ~(sellipse,sline) ,
    OPERATOR 24 ~(sellipse,spath) ,
    OPERATOR 25 ~(sellipse,spoly) ,
    OPERATOR 26 ~(sellipse,sellipse) ,
    OPERATOR 27 ~(sellipse,sbox) ,
    OPERATOR 31 &&(sellipse,scircle) ,
    OPERATOR 32 &&(sellipse,sline) ,
    OPERATOR 33 &&(sellipse,spath) ,
    OPERATOR 34 &&(sellipse,spoly) ,
    OPERATOR 35 &&(sellipse,sellipse) ,
    OPERATOR 36 &&(sellipse,sbox) ,
    FUNCTION 1 (sellipse, sellipse) g_sellipse_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (sellipse, sellipse) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (sellipse, sellipse) g_sellipse_compress(internal) ,
    FUNCTION 4 (sellipse, sellipse) g_spherekey_decompress(internal) ,
    FUNCTION 5 (sellipse, sellipse) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (sellipse, sellipse) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (sellipse, sellipse) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.sellipse USING gist OWNER TO sitools;

--
-- Name: sline; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY sline USING gist;


ALTER OPERATOR FAMILY public.sline USING gist OWNER TO postgres;

--
-- Name: sline; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS sline
    DEFAULT FOR TYPE sline USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(sline,sline) ,
    OPERATOR 2 #(sline,sline) ,
    OPERATOR 11 @(sline,scircle) ,
    OPERATOR 12 @(sline,spoly) ,
    OPERATOR 13 @(sline,sellipse) ,
    OPERATOR 14 @(sline,sbox) ,
    OPERATOR 21 ~(sline,spoint) ,
    OPERATOR 31 &&(sline,scircle) ,
    OPERATOR 32 &&(sline,sline) ,
    OPERATOR 33 &&(sline,spath) ,
    OPERATOR 34 &&(sline,spoly) ,
    OPERATOR 35 &&(sline,sellipse) ,
    OPERATOR 36 &&(sline,sbox) ,
    FUNCTION 1 (sline, sline) g_sline_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (sline, sline) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (sline, sline) g_sline_compress(internal) ,
    FUNCTION 4 (sline, sline) g_spherekey_decompress(internal) ,
    FUNCTION 5 (sline, sline) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (sline, sline) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (sline, sline) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.sline USING gist OWNER TO sitools;

--
-- Name: spath; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY spath USING gist;


ALTER OPERATOR FAMILY public.spath USING gist OWNER TO postgres;

--
-- Name: spath; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS spath
    DEFAULT FOR TYPE spath USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(spath,spath) ,
    OPERATOR 11 @(spath,scircle) ,
    OPERATOR 12 @(spath,spoly) ,
    OPERATOR 13 @(spath,sellipse) ,
    OPERATOR 14 @(spath,sbox) ,
    OPERATOR 21 ~(spath,spoint) ,
    OPERATOR 31 &&(spath,scircle) ,
    OPERATOR 32 &&(spath,sline) ,
    OPERATOR 33 &&(spath,spath) ,
    OPERATOR 34 &&(spath,spoly) ,
    OPERATOR 35 &&(spath,sellipse) ,
    OPERATOR 36 &&(spath,sbox) ,
    FUNCTION 1 (spath, spath) g_spath_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (spath, spath) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (spath, spath) g_spath_compress(internal) ,
    FUNCTION 4 (spath, spath) g_spherekey_decompress(internal) ,
    FUNCTION 5 (spath, spath) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (spath, spath) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (spath, spath) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.spath USING gist OWNER TO sitools;

--
-- Name: spoint; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY spoint USING gist;


ALTER OPERATOR FAMILY public.spoint USING gist OWNER TO postgres;

--
-- Name: spoint; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS spoint
    DEFAULT FOR TYPE spoint USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(spoint,spoint) ,
    OPERATOR 11 @(spoint,scircle) ,
    OPERATOR 12 @(spoint,sline) ,
    OPERATOR 13 @(spoint,spath) ,
    OPERATOR 14 @(spoint,spoly) ,
    OPERATOR 15 @(spoint,sellipse) ,
    OPERATOR 16 @(spoint,sbox) ,
    FUNCTION 1 (spoint, spoint) g_spoint_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (spoint, spoint) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (spoint, spoint) g_spoint_compress(internal) ,
    FUNCTION 4 (spoint, spoint) g_spherekey_decompress(internal) ,
    FUNCTION 5 (spoint, spoint) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (spoint, spoint) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (spoint, spoint) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.spoint USING gist OWNER TO sitools;

--
-- Name: spoly; Type: OPERATOR FAMILY; Schema: public; Owner: postgres
--

CREATE OPERATOR FAMILY spoly USING gist;


ALTER OPERATOR FAMILY public.spoly USING gist OWNER TO postgres;

--
-- Name: spoly; Type: OPERATOR CLASS; Schema: public; Owner: sitools
--

CREATE OPERATOR CLASS spoly
    DEFAULT FOR TYPE spoly USING gist AS
    STORAGE spherekey ,
    OPERATOR 1 =(spoly,spoly) ,
    OPERATOR 11 @(spoly,scircle) ,
    OPERATOR 12 @(spoly,spoly) ,
    OPERATOR 13 @(spoly,sellipse) ,
    OPERATOR 14 @(spoly,sbox) ,
    OPERATOR 21 ~(spoly,spoint) ,
    OPERATOR 22 ~(spoly,scircle) ,
    OPERATOR 23 ~(spoly,sline) ,
    OPERATOR 24 ~(spoly,spath) ,
    OPERATOR 25 ~(spoly,spoly) ,
    OPERATOR 26 ~(spoly,sellipse) ,
    OPERATOR 27 ~(spoly,sbox) ,
    OPERATOR 31 &&(spoly,scircle) ,
    OPERATOR 32 &&(spoly,sline) ,
    OPERATOR 33 &&(spoly,spath) ,
    OPERATOR 34 &&(spoly,spoly) ,
    OPERATOR 35 &&(spoly,sellipse) ,
    OPERATOR 36 &&(spoly,sbox) ,
    FUNCTION 1 (spoly, spoly) g_spoly_consistent(internal,internal,integer,oid,internal) ,
    FUNCTION 2 (spoly, spoly) g_spherekey_union(bytea,internal) ,
    FUNCTION 3 (spoly, spoly) g_spoly_compress(internal) ,
    FUNCTION 4 (spoly, spoly) g_spherekey_decompress(internal) ,
    FUNCTION 5 (spoly, spoly) g_spherekey_penalty(internal,internal,internal) ,
    FUNCTION 6 (spoly, spoly) g_spherekey_picksplit(internal,internal) ,
    FUNCTION 7 (spoly, spoly) g_spherekey_same(spherekey,spherekey,internal);


ALTER OPERATOR CLASS public.spoly USING gist OWNER TO sitools;

SET search_path = pg_catalog;

--
-- Name: CAST (public.scircle AS public.sellipse); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.scircle AS public.sellipse) WITH FUNCTION public.sellipse(public.scircle) AS IMPLICIT;


--
-- Name: CAST (public.sellipse AS public.scircle); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.sellipse AS public.scircle) WITH FUNCTION public.scircle(public.sellipse) AS IMPLICIT;


--
-- Name: CAST (public.sellipse AS public.strans); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.sellipse AS public.strans) WITH FUNCTION public.strans(public.sellipse) AS IMPLICIT;


--
-- Name: CAST (public.sline AS public.strans); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.sline AS public.strans) WITH FUNCTION public.strans(public.sline) AS IMPLICIT;


--
-- Name: CAST (public.spoint AS public.scircle); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.spoint AS public.scircle) WITH FUNCTION public.scircle(public.spoint) AS IMPLICIT;


--
-- Name: CAST (public.spoint AS public.sellipse); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.spoint AS public.sellipse) WITH FUNCTION public.sellipse(public.spoint) AS IMPLICIT;


--
-- Name: CAST (public.spoint AS public.sline); Type: CAST; Schema: pg_catalog; Owner: 
--

CREATE CAST (public.spoint AS public.sline) WITH FUNCTION public.sline(public.spoint) AS IMPLICIT;


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: pacs_photo_l1; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_photo_l1 (
    l1_id integer NOT NULL,
    obsid integer,
    object character varying,
    observer character varying,
    cusmode character varying,
    odnumber integer,
    creationdate character varying,
    startdate character varying,
    enddate character varying,
    obsmode character varying,
    pointingmode character varying,
    aorlabel character varying,
    equinox character varying,
    radesys character varying,
    ranominal real,
    decnominal real,
    ra real,
    "dec" real,
    blue character varying,
    repfactor integer,
    camera character varying,
    filepath character varying,
    program character varying,
    processing character varying,
    filename character varying,
    x real,
    y real,
    z real,
    filesize_bytes real,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE pacs_photo_l1 OWNER TO sitools;

--
-- Name: pacs_photo_l2; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_photo_l2 (
    l2_id integer NOT NULL,
    object character varying,
    filepath character varying,
    observer character varying,
    public character varying,
    program character varying,
    processing character varying,
    filename character varying,
    image character varying,
    fullsize_image character varying,
    crval1 real,
    crval2 real,
    cdelt1 real,
    cdelt2 real,
    crota2 real,
    naxis1 integer,
    naxis2 integer,
    crpix1 real,
    crpix2 real,
    ctype1 character varying,
    ctype2 character varying,
    equinox character varying,
    wavelength real,
    description character varying,
    buildversion character varying,
    x real,
    y real,
    z real,
    filesize_bytes real,
    filesize real,
    release character varying,
    last character varying,
    spoly spoly
);


ALTER TABLE pacs_photo_l2 OWNER TO sitools;

--
-- Name: pacs_photo_l2_obsids; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_photo_l2_obsids (
    l2_obsids_id integer,
    l2_id integer,
    obsid integer,
    l1_id integer,
    processing character varying,
    release character varying
);


ALTER TABLE pacs_photo_l2_obsids OWNER TO sitools;

--
-- Name: pacs_spectro_l1; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_spectro_l1 (
    l1_id integer NOT NULL,
    object character varying,
    obsid character varying,
    observer character varying,
    camera character varying,
    ra real,
    "dec" real,
    x real,
    y real,
    z real,
    observingmode character varying,
    algorithm character varying,
    line character varying,
    rasterid character varying,
    isoffposition character varying,
    naxis1 integer,
    naxis2 integer,
    naxis3 integer,
    program character varying,
    product character varying,
    processing character varying,
    filename character varying,
    filepath character varying,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE pacs_spectro_l1 OWNER TO sitools;

--
-- Name: pacs_spectro_l2; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_spectro_l2 (
    l2_id integer NOT NULL,
    object character varying,
    obsid character varying,
    observer character varying,
    camera character varying,
    ra real,
    "dec" real,
    x real,
    y real,
    z real,
    observingmode character varying,
    algorithm character varying,
    line character varying,
    builversion character varying,
    naxis1 integer,
    naxis2 integer,
    naxis3 integer,
    program character varying,
    product character varying,
    processing character varying,
    filename character varying,
    image character varying,
    filepath character varying,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE pacs_spectro_l2 OWNER TO sitools;

--
-- Name: pacs_spectro_l25; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE pacs_spectro_l25 (
    l25_id integer NOT NULL,
    object character varying,
    obsid character varying,
    ra real,
    "dec" real,
    x real,
    y real,
    z real,
    unit character varying,
    line character varying,
    builversion_hipe character varying,
    version_pacsman character varying,
    naxis1 integer,
    naxis2 integer,
    program character varying,
    product character varying,
    descrip character varying,
    processing character varying,
    filename character varying,
    image character varying,
    filepath character varying,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE pacs_spectro_l25 OWNER TO sitools;

--
-- Name: spire_catalog; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_catalog (
    source_id integer NOT NULL,
    ra real,
    "dec" real,
    x real,
    y real,
    rapluserr real,
    decpluserr real,
    raminuserr real,
    decminuserr real,
    xpluserr real,
    ypluserr real,
    xminuserr real,
    yminuserr real,
    flux real,
    fluxpluserr real,
    fluxminuserr real,
    background real,
    bgpluserr real,
    bgminuserr real,
    quality real,
    object character varying,
    proposal character varying,
    band character varying,
    obsid integer
);


ALTER TABLE spire_catalog OWNER TO sitools;

--
-- Name: spire_fts_l1; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_fts_l1 (
    l1_id integer NOT NULL,
    obsid integer,
    object character varying,
    observer character varying,
    cusmode character varying,
    odnumber integer,
    creationdate character varying,
    startdate character varying,
    enddate character varying,
    timeoffset real,
    timedrift real,
    biasfrequency real,
    obsmode character varying,
    aorlabel character varying,
    equinox character varying,
    radesys character varying,
    ranominal real,
    decnominal real,
    ra real,
    "dec" real,
    posangle character varying,
    numscans integer,
    commandedresolution character varying,
    mapsampling character varying,
    apodname character varying,
    buildversion character varying,
    calibrationversion character varying,
    filepath character varying,
    program character varying,
    origin character varying,
    filetype character varying,
    filename character varying,
    image character varying,
    x real,
    y real,
    z real,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE spire_fts_l1 OWNER TO sitools;

--
-- Name: spire_fts_l2; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_fts_l2 (
    l2_id integer NOT NULL,
    obsid integer,
    object character varying,
    observer character varying,
    cusmode character varying,
    odnumber integer,
    creationdate character varying,
    startdate character varying,
    enddate character varying,
    obsmode character varying,
    equinox character varying,
    radesys character varying,
    ranominal real,
    decnominal real,
    ra real,
    "dec" real,
    posangle character varying,
    commandedresolution character varying,
    mapsampling character varying,
    apodname character varying,
    projection character varying,
    wavelenth character varying,
    crval1 real,
    crval2 real,
    cdelt1 real,
    cdelt2 real,
    naxis integer,
    naxis1 integer,
    naxis2 integer,
    naxis3 integer,
    crpix1 integer,
    crpix2 integer,
    ctype1 character varying,
    ctype2 character varying,
    buildversion character varying,
    calibrationversion character varying,
    filepath character varying,
    program character varying,
    origin character varying,
    filename character varying,
    image character varying,
    x real,
    y real,
    z real,
    filesize real,
    public character varying,
    release character varying,
    last character varying
);


ALTER TABLE spire_fts_l2 OWNER TO sitools;

--
-- Name: spire_photo_density; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_photo_density (
    l2_id integer NOT NULL,
    obsid integer,
    object character varying,
    filepath character varying,
    observer character varying,
    proposal character varying,
    program character varying,
    filename character varying,
    image character varying,
    crval1 real,
    crval2 real,
    cdelt1 real,
    cdelt2 real,
    crota2 real,
    naxis1 integer,
    naxis2 integer,
    crpix1 real,
    crpix2 real,
    ctype1 character varying,
    ctype2 character varying,
    equinox character varying,
    description character varying,
    maxdens real,
    maxsig real,
    buildversion character varying,
    x real,
    y real,
    z real,
    filesize_bytes real,
    filesize real,
    release character varying,
    last character varying
);


ALTER TABLE spire_photo_density OWNER TO sitools;

--
-- Name: spire_photo_l1; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_photo_l1 (
    l1_id integer NOT NULL,
    obsid integer,
    object character varying,
    observer character varying,
    proposal character varying,
    cusmode character varying,
    odnumber integer,
    creationdate character varying,
    startdate character varying,
    enddate character varying,
    elecside character varying,
    biasmode character varying,
    timeoffset real,
    timedrift real,
    invalidoffsetflag character varying,
    adcerrflag character varying,
    plwbiasampl real,
    pmwbiasampl real,
    pswbiasampl real,
    ptcbiasampl real,
    rcrollapp character varying,
    obsmode character varying,
    aorlabel character varying,
    equinox character varying,
    radesys character varying,
    ranominal real,
    decnominal real,
    ra real,
    "dec" real,
    posangle character varying,
    buildversion character varying,
    calibrationversion character varying,
    scriptid character varying,
    fluxconversiondone character varying,
    temperaturedriftcorrectiondone character varying,
    electricalcrosstalkcorrectiondone character varying,
    opticalcrosstalkcorrectiondone character varying,
    filepath character varying,
    program character varying,
    filename character varying,
    x real,
    y real,
    z real,
    filesize_bytes real,
    filesize real,
    public character varying,
    release character varying,
    processing character varying,
    last character varying
);


ALTER TABLE spire_photo_l1 OWNER TO sitools;

--
-- Name: spire_photo_l1_line_id_seq; Type: SEQUENCE; Schema: public; Owner: sitools
--

CREATE SEQUENCE spire_photo_l1_line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spire_photo_l1_line_id_seq OWNER TO sitools;

--
-- Name: spire_photo_l2; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_photo_l2 (
    l2_id integer NOT NULL,
    object character varying,
    filepath character varying,
    observer character varying,
    proposal character varying,
    public character varying,
    program character varying,
    processing character varying,
    filename character varying,
    image character varying,
    fullsize_image character varying,
    crval1 real,
    crval2 real,
    cdelt1 real,
    cdelt2 real,
    crota2 real,
    naxis1 integer,
    naxis2 integer,
    crpix1 real,
    crpix2 real,
    ctype1 character varying,
    ctype2 character varying,
    equinox character varying,
    wavelength real,
    description character varying,
    buildversion character varying,
    x real,
    y real,
    z real,
    filesize_bytes real,
    filesize real,
    release character varying,
    combine character varying,
    last character varying,
    spoly spoly,
    obsid integer,
    level25 character varying
);


ALTER TABLE spire_photo_l2 OWNER TO sitools;

--
-- Name: spire_photo_l2_line_id_seq; Type: SEQUENCE; Schema: public; Owner: sitools
--

CREATE SEQUENCE spire_photo_l2_line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spire_photo_l2_line_id_seq OWNER TO sitools;

--
-- Name: spire_photo_l2_obsids; Type: TABLE; Schema: public; Owner: sitools; Tablespace: 
--

CREATE TABLE spire_photo_l2_obsids (
    l2_obsids_id integer,
    l2_id integer,
    obsid integer,
    l1_id integer,
    processing character varying,
    release character varying
);


ALTER TABLE spire_photo_l2_obsids OWNER TO sitools;

--
-- Name: spirepacs_photo_l25; Type: VIEW; Schema: public; Owner: sitools
--

CREATE VIEW spirepacs_photo_l25 AS
 SELECT 'spire photometer'::text AS instrument,
    spire_photo_l2.object,
    spire_photo_l2.filepath,
    spire_photo_l2.observer,
    spire_photo_l2.program,
    spire_photo_l2.processing,
    spire_photo_l2.filename,
    spire_photo_l2.crval1,
    spire_photo_l2.crval2,
    spire_photo_l2.cdelt1,
    spire_photo_l2.cdelt2,
    spire_photo_l2.crota2,
    spire_photo_l2.naxis1,
    spire_photo_l2.naxis2,
    spire_photo_l2.crpix1,
    spire_photo_l2.crpix2,
    spire_photo_l2.ctype1,
    spire_photo_l2.ctype2,
    spire_photo_l2.equinox,
    (spire_photo_l2.wavelength * (0.000001)::double precision) AS wavelength,
    spire_photo_l2.description,
    spire_photo_l2.buildversion,
    spire_photo_l2.x,
    spire_photo_l2.y,
    spire_photo_l2.z,
    spire_photo_l2.filesize_bytes,
    spire_photo_l2.filesize,
    spire_photo_l2.release,
    spire_photo_l2.last,
    spire_photo_l2.spoly
   FROM spire_photo_l2
  WHERE ((((spire_photo_l2.level25)::text = 'true'::text) AND ((((spire_photo_l2.program)::text = 'SAG-4'::text) OR ((spire_photo_l2.program)::text = 'SAG-3'::text)) OR ((spire_photo_l2.program)::text = 'clusters_lowz'::text))) AND ((spire_photo_l2.release)::text ~~ 'R5_spire_photo'::text))
UNION ALL
 SELECT 'pacs photometer'::text AS instrument,
    pacs_photo_l2.object,
    pacs_photo_l2.filepath,
    pacs_photo_l2.observer,
    pacs_photo_l2.program,
    pacs_photo_l2.processing,
    pacs_photo_l2.filename,
    pacs_photo_l2.crval1,
    pacs_photo_l2.crval2,
    pacs_photo_l2.cdelt1,
    pacs_photo_l2.cdelt2,
    pacs_photo_l2.crota2,
    pacs_photo_l2.naxis1,
    pacs_photo_l2.naxis2,
    pacs_photo_l2.crpix1,
    pacs_photo_l2.crpix2,
    pacs_photo_l2.ctype1,
    pacs_photo_l2.ctype2,
    pacs_photo_l2.equinox,
    pacs_photo_l2.wavelength,
    pacs_photo_l2.description,
    pacs_photo_l2.buildversion,
    pacs_photo_l2.x,
    pacs_photo_l2.y,
    pacs_photo_l2.z,
    pacs_photo_l2.filesize_bytes,
    pacs_photo_l2.filesize,
    pacs_photo_l2.release,
    pacs_photo_l2.last,
    pacs_photo_l2.spoly
   FROM pacs_photo_l2
  WHERE ((((pacs_photo_l2.program)::text = 'SAG-4'::text) OR ((pacs_photo_l2.program)::text = 'SAG-3'::text)) AND ((pacs_photo_l2.release)::text ~~ 'R1_pacs_photo'::text));


ALTER TABLE spirepacs_photo_l25 OWNER TO sitools;

--
-- Name: file_unicity; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_photo_l2
    ADD CONSTRAINT file_unicity UNIQUE (filename, release, buildversion);


--
-- Name: pacs_photo_l1_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY pacs_photo_l1
    ADD CONSTRAINT pacs_photo_l1_pkey PRIMARY KEY (l1_id);


--
-- Name: pacs_photo_l2_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY pacs_photo_l2
    ADD CONSTRAINT pacs_photo_l2_pkey PRIMARY KEY (l2_id);


--
-- Name: pacs_spectro_l1_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY pacs_spectro_l1
    ADD CONSTRAINT pacs_spectro_l1_pkey PRIMARY KEY (l1_id);


--
-- Name: pacs_spectro_l25_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY pacs_spectro_l25
    ADD CONSTRAINT pacs_spectro_l25_pkey PRIMARY KEY (l25_id);


--
-- Name: pacs_spectro_l2_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY pacs_spectro_l2
    ADD CONSTRAINT pacs_spectro_l2_pkey PRIMARY KEY (l2_id);


--
-- Name: spire_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_catalog
    ADD CONSTRAINT spire_catalog_pkey PRIMARY KEY (source_id);


--
-- Name: spire_fts_l1_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_fts_l1
    ADD CONSTRAINT spire_fts_l1_pkey PRIMARY KEY (l1_id);


--
-- Name: spire_fts_l2_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_fts_l2
    ADD CONSTRAINT spire_fts_l2_pkey PRIMARY KEY (l2_id);


--
-- Name: spire_photo_density_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_photo_density
    ADD CONSTRAINT spire_photo_density_pkey PRIMARY KEY (l2_id);


--
-- Name: spire_photo_l1_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_photo_l1
    ADD CONSTRAINT spire_photo_l1_pkey PRIMARY KEY (l1_id);


--
-- Name: spire_photo_l2_pkey; Type: CONSTRAINT; Schema: public; Owner: sitools; Tablespace: 
--

ALTER TABLE ONLY spire_photo_l2
    ADD CONSTRAINT spire_photo_l2_pkey PRIMARY KEY (l2_id);


--
-- Name: crval1_spire_photo_l2; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX crval1_spire_photo_l2 ON spire_photo_l2 USING btree (crval1);


--
-- Name: crval2_spire_photo_l2; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX crval2_spire_photo_l2 ON spire_photo_l2 USING btree (crval2);


--
-- Name: last_spire_photo_l2; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX last_spire_photo_l2 ON spire_photo_l2 USING btree (last);


--
-- Name: program_spire_photo_l2; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX program_spire_photo_l2 ON spire_photo_l2 USING btree (program);


--
-- Name: spoly_pacs_photo_l2_idx; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX spoly_pacs_photo_l2_idx ON pacs_photo_l2 USING gist (spoly);


--
-- Name: spoly_spire_photo_l2_idx; Type: INDEX; Schema: public; Owner: sitools; Tablespace: 
--

CREATE INDEX spoly_spire_photo_l2_idx ON spire_photo_l2 USING gist (spoly);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

