'''
Created on Sep 22, 2015

@author: bhasnoun
'''

import tools
import pyfits, os, numpy, warnings, time


def get_all_metadata_spire(fitsfile):
    
    metadata = tools.get_all_metadata(fitsfile)
    
    filename = metadata["FILENAME"].lower()

    if metadata["INSTRUMENT"].lower()=="maps_spire":
        
        if len(numpy.unique([filename.count(i) for i in ["extemigainsapplied","destriped","supreme","psrc","extdzeropoint","combined"]])) != 1:
            metadata["PRODUCT_LEVEL"] = "Level25"
            metadata["LEVEL25"] = True
            metadata["VO"] = True
        else:
            metadata["PRODUCT_LEVEL"] = "Level2"
            metadata["LEVEL25"] = False
            metadata["VO"] = False
        
    if metadata["FILEPATH"].lower().count("l1_sanepic_spire") > 0: 
        metadata["PRODUCT_LEVEL"] = "Level1"
        metadata["LEVEL25"] = False
        metadata["VO"] = False

    if metadata["INSTRUMENT"].lower()=="fts_spire":

        if filename.count("sdi")>0:             metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "sdi", "Level1"
        if filename.count("sdt")>0:             metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "sdt", "Level1"
        if filename.count("spectrum")>0:        metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Spectrum", "Level1"
        if filename.count("spectrum2d")>0:      metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Spectrum2d", "Level1"
        if filename.count("gridding_cube")>0:   metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Gridding Cube", "Level2"
        if filename.count("naive_cube")>0:      metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Naive Cube", "Level2"
        if filename.count("nearest_cube")>0:    metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Nearest Cube", "Level2"
        if filename.count("supreme")>0:         metadata["PRODUCT"], metadata["PRODUCT_LEVEL"] = "Supreme Cube", "Level2"
    
        if filename.count("apodized")>0: metadata["APODNAME"]="apod"
        else: metadata["APODNAME"]="unapod"
        
        metadata["VO"] = False
        
        if "OBSID002" in metadata.keys():
            for meta in ["ra_nom","dec_nom"]: metadata[meta.upper()] = float(str(metadata[meta.upper()]).split("&")[0])
            for meta in ["odnumber"]: metadata[meta.upper()] = int(metadata[meta.upper()].split("&")[0])
    
    metadata["PROCESSING"] = "HIPE"
    if (filename.count("_dbl") == 0 and filename.count("smpvt") == 0 and filename.count("fixed") == 0 and filename.count("test") == 0 and filename.count("_all") == 1 and filename.count("_extemigainsapplied") == 0):
        metadata["PROCESSING"] = "HIPE_Sanepic"
    if filename.count("supreme") > 0:
        metadata["PROCESSING"] = "Supreme Plugin"
    
    return metadata

def table_spire(table,conn=None,col=False):
    
    if table == "spire_photo_l1":
        sql = "CREATE TABLE spire_photo_l1 \
            ( \
            l1_id serial primary key, \
            obsid integer[],\
            object varchar,\
            observer varchar,\
            proposal varchar,\
            cusmode varchar,\
            odnumber integer,\
            creationdate varchar,\
            startdate varchar,\
            enddate varchar,\
            elecside varchar,\
            biasmode varchar,\
            timeoffset real,\
            timedrift real,\
            invalidoffsetflag varchar,\
            adcerrflag varchar,\
            plwbiasampl real,\
            pmwbiasampl real,\
            pswbiasampl real,\
            ptcbiasampl real,\
            rcrollapp varchar,\
            obsmode varchar,\
            aorlabel varchar,\
            equinox varchar,\
            radesys varchar,\
            ranominal real,\
            decnominal real,\
            ra real,\
            dec real,\
            posangle varchar,\
            buildversion varchar,\
            calibrationversion varchar,\
            scriptid varchar,\
            fluxconversiondone varchar,\
            temperaturedriftcorrectiondone varchar,\
            electricalcrosstalkcorrectiondone varchar,\
            opticalcrosstalkcorrectiondone varchar,\
            filepath varchar,\
            program varchar,\
            filename varchar,\
            x real,\
            y real,\
            z real,\
            filesize_bytes real,\
            filesize real,\
            public varchar,\
            release varchar,\
            processing varchar,\
            last varchar,\
            metadata json\
            )"
    
    if table == "spire_photo_l2":
        sql = "CREATE TABLE spire_photo_l2 \
            (\
            l2_id serial primary key,\
            object varchar,\
            filepath varchar,\
            observer varchar,\
            proposal varchar,\
            public varchar,\
            program varchar,\
            processing varchar,\
            filename varchar,\
            image varchar,\
            fullsize_image varchar,\
            crval1 real,\
            crval2 real,\
            cdelt1 real,\
            cdelt2 real,\
            crota2 real,\
            naxis1 integer,\
            naxis2 integer,\
            crpix1 real,\
            crpix2 real,\
            ctype1 varchar,\
            ctype2 varchar,\
            equinox varchar,\
            wavelength real,\
            description varchar,\
            buildversion varchar,\
            x real,\
            y real,\
            z real,\
            filesize_bytes real,\
            filesize real,\
            release varchar,\
            combine varchar,\
            last varchar,\
            spoly spoly,\
            obsid integer[],\
            level25 varchar,\
            metadata json\
            )"
    
    if table == "spire_fts_l1":
        sql = "CREATE TABLE spire_fts_l1 \
            (\
            l1_id serial primary key,\
            obsid integer[],\
            object varchar,\
            observer varchar,\
            cusmode varchar,\
            odnumber integer,\
            creationdate varchar,\
            startdate varchar,\
            enddate varchar,\
            timeoffset real,\
            timedrift real,\
            biasfrequency real,\
            obsmode varchar,\
            aorlabel varchar,\
            equinox varchar,\
            radesys varchar,\
            ranominal real,\
            decnominal real,\
            ra real,\
            dec real,\
            posangle varchar,\
            numscans integer,\
            commandedresolution varchar,\
            mapsampling varchar,\
            apodname varchar,\
            buildversion varchar,\
            calibrationversion varchar,\
            filepath varchar,\
            program varchar,\
            origin varchar,\
            filetype varchar,\
            filename varchar,\
            image varchar,\
            x real,\
            y real,\
            z real,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
     
    if table == "spire_fts_l2":
        sql = "CREATE TABLE spire_fts_l2 \
            (\
            l2_id serial primary key,\
            obsid integer[],\
            object varchar,\
            observer varchar,\
            cusmode varchar,\
            odnumber integer,\
            creationdate varchar,\
            startdate varchar,\
            enddate varchar,\
            obsmode varchar,\
            equinox varchar,\
            radesys varchar,\
            ranominal real,\
            decnominal real,\
            ra real,\
            dec real,\
            posangle varchar,\
            commandedresolution varchar,\
            mapsampling varchar,\
            apodname varchar,\
            projection varchar,\
            wavelenth varchar,\
            crval1 real,\
            crval2 real,\
            cdelt1 real,\
            cdelt2 real,\
            naxis integer,\
            naxis1 integer,\
            naxis2 integer,\
            naxis3 integer,\
            crpix1 integer,\
            crpix2 integer,\
            ctype1 varchar,\
            ctype2 varchar,\
            buildversion varchar,\
            calibrationversion varchar,\
            filepath varchar,\
            program varchar,\
            origin varchar,\
            filename varchar,\
            image varchar,\
            x real,\
            y real,\
            z real,\
            filesize real,\
            public varchar,\
            release varchar,\
            last varchar,\
            metadata json\
            )"
    
    if table == "spire_photo_density":
        sql = "CREATE TABLE spire_photo_density \
            (\
            l2_id serial primary key,\
            obsid integer[],\
            object varchar,\
            filepath varchar,\
            observer varchar,\
            proposal varchar,\
            program varchar,\
            filename varchar,\
            image varchar,\
            crval1 real,\
            crval2 real,\
            cdelt1 real,\
            cdelt2 real,\
            crota2 real,\
            naxis1 integer,\
            naxis2 integer,\
            crpix1 real,\
            crpix2 real,\
            ctype1 varchar,\
            ctype2 varchar,\
            equinox varchar,\
            description varchar,\
            maxdens real,\
            maxsig real,\
            buildversion varchar,\
            x real,\
            y real,\
            z real,\
            filesize_bytes real,\
            filesize real,\
            release varchar,\
            last varchar\
            )"
    
    if table == "spire_catalog":
        sql = "CREATE TABLE spire_catalog \
            (\
            source_id serial primary key,\
            ra real,\
            dec real,\
            x real,\
            y real,\
            rapluserr real,\
            decpluserr real,\
            raminuserr real,\
            decminuserr real,\
            xpluserr real,\
            ypluserr real,\
            xminuserr real,\
            yminuserr real,\
            flux real,\
            fluxpluserr real,\
            fluxminuserr real,\
            background real,\
            bgpluserr real,\
            bgminuserr real,\
            quality real,\
            object varchar,\
            proposal varchar,\
            band varchar,\
            obsid integer\
            )"
    
    if conn!=None:
        print "create table "+table
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
    else:
        columns = sql.split('(', 1)[1].split(')')[0]
        if col==False: 
            keys = [ i.lstrip().rstrip().split(" ")[0].upper() for i in columns.split(",") ]
            return keys[1:] #the first column is auto incremented
        else:
            return [ i.lstrip().rstrip() for i in columns.split(",")]

def fill_spire_photo_catalog(listfile, table, mode):
    """
    Fill Spire Photo Catalog database
    
    This function create the Spire Photo Sources Catalog table.
    
    :param listfile: list of source catalog fits files
    :param table: name of the table
    :param mode: connection mode to the database
    """
    
    conn = tools.get_connection(mode)
    cur = conn.cursor()
    
    for i in listfile:
        fits = pyfits.open(i, mode='update')
        allSources = fits[1].data
        for j in range(len(allSources)):
            source = list(allSources[j])
            cur.execute("select count (*) from " + table)
            nblines = cur.fetchone()
            if nblines is not None: source_id = nblines[0]
            else: source_id = 0
            source.insert(0, int(source_id))
            source[-1] = int(source[-1])  # obsid to int not int32
            source = tuple(source)
            print source
            cur.execute("insert into " + table + " values (" + (len(source) - 1) * "%s," + "%s)", source)
    
    cur.close()
    tools.close_connection(mode,conn)

def create_spire_pacs_l25_view(release_spire, release_pacs, mode):
    """
    This function create the Spire-Pacs L25 view.
    
    :param release_spire: Last Spire release
    :param release_pacs: Last Pacs release
    :param mode: connection mode to the database
    """
    conn = tools.get_connection(mode)
    cur = conn.cursor()
    #cur.execute("drop view spirepacs_photo_L25")
    cur.execute(
        "create view spirepacs_photo_l25  as select 'spire photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength*1E-6 as \"wavelength\" ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from spire_photo_l2 where level25='true' and (program='SAG-4' or program='SAG-3' or program='clusters_lowz') and release like '" + release_spire + 
        "' UNION ALL SELECT 'pacs photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from pacs_photo_l2 where (program='SAG-4' or program='SAG-3' ) and release like '" + release_pacs + "'")
    
    cur.close()
    tools.close_connection(mode,conn)
