from sitools2_v3_product_tools import *
import json
import requests, simplejson
import sys


login = "admin"
pwd = "Motdepassedeouf!!"
auth = (login,pwd)
url = "http://idoc-herschel-test.ias.u-psud.fr/sitools/"
header = {"content-type":"application/json","accept":"application/json"}

removeDefault = True
if removeDefault:
    for service_name in ["Plot_service","Columns_Definition","Add_Cart_Service"]:
        removeDefaultGUIServices(url,service_name,header,auth)

deleteDatasets = True
if deleteDatasets:
    print "Delete existing dataset..."
    url_d = url+"datasets/"
    ids = getListAllDs(url_d,auth,header)
    if ids!=[]: delListDs(url_d,auth,header,ids)
    print "Done !"

deleteProjects = True
if deleteProjects:
    print "Delete existing projects..."
    url_p = url+"projects/"
    projects = getListAllDs(url_p,auth,header)
    if projects!=[]: delListDs(url_p,auth,header,projects)
    print "Done !"

createStorage = True
if createStorage:
    url_s = url+"datastorage/admin/directories"
    storage = "file:///mnt/data4/HERSCHEL/Release"
    post = {"id":"","name":"storageRelease","description":"","localPath":storage,"attachUrl":"/storageRelease","deeplyAccessible":True,"listingAllowed":True,"modifiable":False}
    post = simplejson.dumps(post,indent=4)
    post = requests.post(url=url_s,data=post,headers=header,auth=auth)
    if post.status_code == 200: print "datastorage",storage,"created" 


spire_photo   = ["spire_photo_l1","spire_photo_l2"]
spire_fts     = ["spire_fts_l1","spire_fts_l2"]
pacs_photo    = ["pacs_photo_l1","pacs_photo_l2"]
pacs_spectro  = ["pacs_spectro_l1","pacs_spectro_l2"]

photo = spire_photo + pacs_photo

all_spire = spire_photo + spire_fts
all_pacs = pacs_photo + pacs_spectro

all = all_spire + all_pacs

generate = True
if generate:

    projects = {}
    projects.update({"Herschel-Public":
                {
                "tables":all,
                "clauses":[{"column":"public varchar","comparator":"EQ","value":"'true'"}]
                }
            })
    projects.update({"Herschel-Private":
                {
                "tables":all,
                "clauses":[{"column":"public varchar","comparator":"EQ","value":"'true'"}]
                }
            })
    projects.update({"SAG-1":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'SAG-1'"}]
                }
            })
    projects.update({"SAG-3":
                {
                "tables":photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'SAG-3'"}]
                }
            })
    projects.update({"SAG-4":
                {
                "tables":all,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'SAG-4'"}]
                }
            })
    projects.update({"OT1_atielens":
                {
                "tables":spire_fts,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'OT1_atielens'"}]
                }
            })
    projects.update({"OT1_lho":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'OT1_lho'"}]
                }
            })
    projects.update({"OT1_mmiville":
                {
                "tables":photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'OT1_mmiville'"}]
                }
            })
    projects.update({"OT2_ehabart":
                {
                "tables":pacs_spectro,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'OT2_ehabart'"}]
                }
            })
    projects.update({"DDT_mustdo_4":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'DDT_mustdo_4'"}]
                }
            })
    projects.update({"H-ATLAS":
                {
                "tables":photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'H-ATLAS'"}]
                }
            })
    projects.update({"Lens_Malhotra":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'Lens_Malhotra'"}]
                }
            })
    projects.update({"Cluster-Low-Z":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'clusters_lowz'"}]
                }
            })
    projects.update({"Planck-High-Z":
                {
                "tables":spire_photo,
                "clauses":[{"column":"program varchar","comparator":"EQ","value":"'OT2_hdole'"}]
                }
            })

    for project in ["SAG-4"]:#projects.keys():
        
        dataset_properties_list = []
        
        for table in projects[project]["tables"]:
            
            for last in [True, False]:
                
                project_lower = project.lower().replace("-","_")
                
                dataset_name = project_lower+"_"+table
                if last == False: dataset_name = project_lower+"_"+table+"_previous_releases"
                
                dataset_description = "All "+" ".join(table.split("_"))+" Products from "+project+" project."
                
                sql_clause = []
                
                for clause in projects[project]["clauses"]:
                    sql_clause += set_sql_clause(table,clause["column"],clause["comparator"],clause["value"])
                
                if last: sql_clause += set_sql_clause(table,"last varchar","EQ","'true'")
                else: sql_clause += set_sql_clause(table,"last varchar","EQ","'false'")
                
                url_d = url+"datasets/"
                
                # Dataset creation
                post = create_dataset_post(table,dataset_name,dataset_description,sql_clause)
                post = simplejson.dumps(post,indent=4)
                request = requests.post(url=url_d,headers=header,data=post,auth=auth)
                if request.status_code == 200: 
                    print project, dataset_name, "created",
                else:
                    print project, dataset_name, "not created !!"
                    sys.exit()
                
                # Dataset activation
                activate = url_d+json.loads(request.content)["dataset"]["id"]+"/start"
                put = requests.put(url=activate,auth=auth)
                if put.status_code == 200: 
                    print "and activated."
                else:
                    print "and not activated !!"
                    sys.exit()
                
                # Adding Window Image Zoomer Service
                if table.count("photo_l2")>0:
                    json_gui = {"name":"Window Image Zoomer","description":"Display an image with zoom functions","label":"label.windowImgZoomer","xtype":"sitools.user.component.datasets.services.WindowImageZoomerService","author":"AKKA","version":"1.0","icon":"/sitools/common/res/images/icons/zoom_image.png","parameters":[{"name":"featureType","value":"Image"},{"name":"columnAlias","value":"fullsize_image"},{"name":"thumbnailColumnImage","value":"image"},{"name":"sizeLimitWidth","value":"500"},{"name":"sizeLimitHeight","value":"500"},{"name":"zoomFactor","value":"20"},{"name":"maxZoom","value":"10000"}],"priority":0,"dataSetSelection":"SINGLE","defaultVisibility":True,"descriptionAction":""}
                    json_gui = simplejson.dumps(json_gui,indent=4)
                    activate_gui = url_d+json.loads(request.content)["dataset"]["id"]+"/services/gui"
                    put = requests.post(url=activate_gui,data=json_gui,headers=header,auth=auth)
                    if put.status_code == 200: print "GUI Image Zoomer service added to", dataset_name 
                
                # Adding donwload service
                json_server = download_service_post()
                json_server = simplejson.dumps(json_server,indent=4)
                activate_server = url_d+json.loads(request.content)["dataset"]["id"]+"/services/server"
                put = requests.post(url=activate_server,data=json_server,headers=header,auth=auth)
                if put.status_code == 200: print "Server Download service added to", dataset_name 
                
                
                dataset_properties = {}
                for key in ["id","description","name","nbRecords"]: 
                    dataset_properties[key] = json.loads(request.content)["dataset"][key]
                dataset_properties_list.append(dataset_properties)
                
        
        url_p = url+"projects/"
        
        # Project creation
        post = create_project_post(project,dataset_properties_list)
        post = simplejson.dumps(post,indent=4)
        
        request = requests.post(url=url_p,headers=header,data=post,auth=auth)
        if request.status_code==200: 
            print "PROJECT", project.upper(), "created"
        else:
            print "PROJECT", project.upper(), "not created !!"
            sys.exit()
        
        # Project activation
        id = json.loads(request.content)["project"]["id"]
        activate = url_p+id+"/start"
        put = requests.put(url=activate,auth=auth)
        if put.status_code==200: 
            print "and activated."
        else:
            print "and not activated !!"
            sys.exit()
        
        # Project graph creation
        url_pg = url_p+id+"/graph"
        project_graph_post = create_project_graph_post(project_lower,dataset_properties_list)
        project_graph_post = simplejson.dumps(project_graph_post,indent=4)
        request = requests.post(url=url_pg,headers=header,data=project_graph_post,auth=auth)
        if request.status_code==200: 
            print "PROJECT GRAPH", project.upper(), "created"
        else:
            print "PROJECT GRAPH", project.upper(), "not created !!"
            sys.exit()
        
        
        
        
