import psycopg2
import pyfits,pywcs
import glob
import os, string, shutil
import time
from datetime import date
import math


def getListFiles(basePath):
	"""Get all the files from a directory, subdirectories included"""
	listfile_all=[]
	for root, dirs, files in os.walk(basePath):
		listfile = [root+'/'+filename for filename in files if (filename.endswith('.fits'))]
		listfile_all.extend(listfile)
	return listfile_all


def checkMotif(chaine,Motifs, Exclure):
	"""Check if a string contains some patterns and exclude some other"""
	for motif in Motifs:
		if str(chaine).count(motif)==0:return False

	for exclu in Exclure:
		if str(chaine).count(exclu)>0:return False

	return True


def ajoutRoot(root,exclure):
	for exclu in exclure:
		if str(root).count(exclu)>0:
			return False
	return True


def getFilesMotif(basePath,motifs,exclure):
	"""Get all the files from a directory, subdirectories included, that have some patterns and have not others"""
	listfile_all = []
	listroot = []
	for root, dirs, files in os.walk(basePath):
			##### WARNING KD added root in checkMotif call
		listfile = [root+"/"+filename for filename in files if ajoutRoot(root,exclure) and checkMotif(root+"/"+filename,motifs,exclure)]
		listfile_all.extend(listfile)
		
	listfile_all = sorted(listfile_all)
	
	return listfile_all


def logTime(message,fichier):
	"""Include Timestamped message in a file"""
	timeStamp = time.strftime("\n%d-%m-%Y %H:%M:%S - ")
	fileToWrite = open(fichier, "a")
	fileToWrite.write(timeStamp+message)
	fileToWrite.close()


def isPublic(timeObs):
	"""Check if an observation is public, more than one year old"""
	today=date.today()
	
	timeObsSplit=timeObs.split("-")
	year=timeObsSplit[0]
	month=timeObsSplit[1]
	timeObsSplit2=timeObsSplit[2].split("T")
	day=timeObsSplit2[0]
	
	dateTimeObs=date(int(year),int(month),int(day))
	deltaTime=abs(dateTimeObs-today)

	if deltaTime.days<365 :
		return False
	
	return True


def checkPublic(listfile):
	listPublic=[]
	for i in listfile:
	#if  str(i).count("_notdf")==0 and str(i).count("_dbl")==0 and  str(i).count("SMPVT")==0 and str(i).count("fixed")==0 and str(i).count("test")==0 and str(i).count("_all")==1 :	
		if  str(i).count("_notdf")==0 and str(i).count("Pointing")==0 and str(i).count("_dbl")==0 and  str(i).count("SMPVT")==0 and str(i).count("fixed")==0 and str(i).count("test")==0 and str(i).count("Old")==0  and str(i).count("old")==0 :	
			filepath=str(i)
			#print filepath
			hdulist=pyfits.open(filepath)
			hdr0=hdulist[0].header
			#print hdr0['DATE-OBS']
			today=date.today()
			timeObs=hdr0['DATE-OBS']
			#print"timeObs", timeObs
			timeObsSplit=timeObs.split("-")
			year=timeObsSplit[0]
			month=timeObsSplit[1]
			timeObsSplit2=timeObsSplit[2].split("T")
			day=timeObsSplit2[0]
			#print "year",year,"month", month,"day", day
			#print time.strftime('%d/%m/%y %H:%M',time.localtime());
			dateTimeObs=date(int(year),int(month),int(day))
			deltaTime=abs(dateTimeObs-today)
			#print deltaTime.days
			if deltaTime.days>365 :
				#print filepath
				listPublic.append(filepath)
	return listPublic


def fillDico(cards,DICO):
	for card in cards:
		chaine_card=str(card)
		print chaine_card
		if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
			#print chaine_card
			chaine_split=chaine_card.split("=")
			chaine_resplit=chaine_split[0].split()
			chaine_resplit=chaine_resplit[1].split(".")
			DICO[chaine_split[1].strip()]=chaine_resplit[1]
	return DICO


def deleteRelease(listTable,cur,release):
	for table in listTable:
		#test Release existence
		test_release_request="select count (*) from "+(table)+"  where release ='"+release+"'"
		print(test_release_request)
		cur.execute(test_release_request)
		NbLines= cur.fetchone()
		if NbLines!=None:
			request_param=[(table),(release)]
			delete_request="delete from "+(table)+" where release = '"+release+"'"
			print delete_request 
			cur.execute(delete_request)


def drop_tables(listTable,cur):
	for table in listTable:
		print table
		cur.execute("SELECT tablename from pg_tables where tablename like %s" , [table])
		if cur.fetchone()!=None:
			print "drop table",table
			cur.execute("drop table %s" % table)
		else:
			print "no  table ",table
	return cur


def create_and_drop(listTable,cur):
	table2=[]
	for table in listTable:
		print table
		# drop temporary tables
		table2=[table]
		print table2
		cur.execute("SELECT tablename from pg_tables where tablename like %s",table2)
		table2=(table+"_temp",table)
		print table2
		table1=(table)
		print table1
		if cur.fetchone()!=None:
			print "copy table ",table
			cur.execute("CREATE TABLE %s AS SELECT * FROM %s" % table2) 
			print "drop table ",table
			cur.execute("drop table %s" % table1)
		else:
			print "no HID tables ",table
	return cur


def RADECtoXYZ(RA,DEC):
	"""Convert RA DEC pointing to X Y Z"""
	#convert degrees to radians
	RArad=math.radians(RA)
	DECrad=math.radians(DEC)
	X=math.cos(DECrad)*math.cos(RArad)
	Y=math.cos(DECrad)*math.sin(RArad)
	Z=math.sin(DECrad)
	ResultXYZ=[X,Y,Z]
	return ResultXYZ


def convert_MegaBytes(bytes):
	bytes = float(bytes)
	megabytes = bytes / 1048576
	return round(megabytes,2) 


def convert_bytes(bytes):
	bytes = float(bytes)
	if bytes >= 1099511627776:
		terabytes = bytes / 1099511627776
		size = '%.2fT' % terabytes
	elif bytes >= 1073741824:
		gigabytes = bytes / 1073741824
		size = '%.2fG' % gigabytes
	elif bytes >= 1048576:
		megabytes = bytes / 1048576
		size = '%.2fM' % megabytes
	elif bytes >= 1024:
		kilobytes = bytes / 1024
		size = '%.2fK' % kilobytes
	else:
		size = '%.2fb' % bytes

	return size


def urlFilepath(filepath):

	#Je ne vais pas reinventer la roue !!!
	dicURL={}
	dicURL[" "] = "%20"
	dicURL[";"] = "%3b"
	#dicURL["/"] = "%2f"
	dicURL["?"] = "%3f"
	dicURL[":"] = "%3a"
	dicURL["="] = "%3d"
	dicURL["+"] = "%2b"

	#print "avant ",filepath
	#for i in dicURL.iterkeys():
	#  filepath = filepath.replace(i,dicURL[i])
	#print "apres ",filepath
	return filepath


def addMetadata(filepath="",key="", value="",comment=""):
	"""Add a metadata to a fitsfile"""
	hdulist=pyfits.open(filepath,mode='update')
	hdr0=hdulist[0].header
	hdr0.update(key,value,comment)
	hdulist.close()


def addMetadata_List(filepath="",listMeta={"":""}):
	"""Add a list of metadata to a fitsfile"""
	hdulist=pyfits.open(filepath)
	hdr0=hdulist[0].header
	for key, value in listMeta.iteritems(): hdr0.update(key,value,"")
	hdulist.close()

def calculateSpoly(filepath=""):
	hdulist=pyfits.open(filepath)
	print filepath
	try:
		if filepath.count("SCANA")==0:
			Image = hdulist[1]
			hdr1=Image.header
			hdr1['CUNIT1']="deg"
			hdr1['CUNIT2']="deg"
			wcs = pywcs.WCS(hdr1)
		else:
			###### for SCANAMORPHOS fits files (3D)
			Image=hdulist['PrimaryImage'] 
			PrimaryHeader=hdulist['Primary'].header
			wcs = pywcs.WCS(PrimaryHeader)
			hdr1=Image.header
			#print wcs
			#print hdr1
	except KeyError:
			print "EE - No 'Image' extension in "+os.path.basename(inputImage)
			return 2
	poly1=wcs.wcs_pix2sky([[0.5,0.5]],0)
	#print "poly1",poly1
	poly2=wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5]],0)	
	#print poly2
	poly3=wcs.wcs_pix2sky([[0.5+hdr1['NAXIS1'],0.5+hdr1['NAXIS2']]],0)	
	#print poly3
	poly4=wcs.wcs_pix2sky([[0.5,0.5+hdr1['NAXIS2']]],0)	
	#print poly4
	#poly=wcs.wcs_pix2sky([[0.5,0.5],[0.5+hdr1['NAXIS1'],0.5],[0.5+hdr1['NAXIS1'],0.5+hdr1['NAXIS2']],[0.5,0.5+hdr1['NAXIS2']]],0)	
	#print "poly",poly
	poly1="("+str(poly1[0,0])+"d,"+str(poly1[0,1])+"d)"
	poly2="("+str(poly2[0,0])+"d,"+str(poly2[0,1])+"d)"
	poly3="("+str(poly3[0,0])+"d,"+str(poly3[0,1])+"d)"
	poly4="("+str(poly4[0,0])+"d,"+str(poly4[0,1])+"d)"
	poly="{"+poly4+","+poly3+","+poly2+","+poly1+"}"
	#poly="("+poly1+","+poly3+")"
	return poly


def createIndexSpoly(listTable,cur):
	for table in listTable:
		command="CREATE UNIQUE INDEX spoly_"+table+"_idx ON "+table+" (spoly);"
		print command
		cur.execute(command)
	return cur


def updateToFalse(listTable,cur,Release,program_list):
	for table in listTable:
		for program in program_list:
			command="UPDATE "+table+" SET last = false WHERE program = '"+program +"' and Release= '"+Release+"';"
			print command
			cur.execute(command)
	return cur


def updateToTrue(listTable,cur,Release,program_list):
	for table in listTable:
		for program in program_list:
			command="UPDATE "+table+" SET last = true  WHERE program = '"+program +"' and Release= '"+Release+"';"
			print command
			cur.execute(command)
	return cur

def updateToLevel25(listTable,filelist,prefix,cur):
	for table in listTable:
		for filepath in filelist:
			if filepath.count("combine")==1:
				hdulist=pyfits.open(filepath)
				try:
					for j in range(len(hdulist["obsids"].data)):
						command="UPDATE "+table+" SET level25 =false WHERE obsid = '"+str(hdulist["obsids"].data[j][0])+"' and Release= '"+prefix+"';"
						print command
						cur.execute(command)
				except:
					print "Level25 not updated, old file? "+filepath
				hdulist.close()
			#if filepath.count("destriped")==0:
			#	print filepath
			#	filename=os.path.basename(filepath)
                        #        try:
                        #                        command="UPDATE "+table+" SET level25 =false where  filename = '"+filename+"' and Release= '"+prefix+"';"
                        #                        print command
                        #                        cur.execute(command)
                        #        except:
                         #               print "Level25 not updated, old file? "+filepath

				
	return cur

def CreateSPIREPACSL25View(cur,releaseSPIRE,releasePACS):
        ############ L2 Table
	cur.execute("drop view spirepacs_photo_L25");
        cur.execute("CREATE VIEW spirepacs_photo_L25  AS SELECT 'spire photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength*1E-6 as \"wavelength\" ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from spire_photo_l2 where level25='true' and (program='SAG-4' or program='SAG-3') and release like '"+releaseSPIRE+"' UNION ALL SELECT 'pacs photometer' as instrument,object ,filepath ,observer ,program ,processing ,filename ,crval1 ,crval2 ,cdelt1 ,cdelt2 ,crota2 ,naxis1 ,naxis2 ,crpix1 ,crpix2 ,ctype1 ,ctype2 ,equinox ,wavelength ,description ,BuildVersion ,X ,Y ,Z ,filesize_bytes ,filesize ,release ,last ,spoly from pacs_photo_l2 where (program='SAG-4' or program='SAG-3') and release like '"+releasePACS+"'")

