#!/usr/bin/env python
"""
Updates a table and adds columns coord_x, coord_y and coord_z computed from it's ra and dec (in deg) informations.
"""
import math
import psycopg2

###########################PARAMETERS##########################################################

host= "glx-herschel"
dbName = "HID_glx"
user = "sitools"
password = ""
tableName= "spire_catalog"
#Name of the columns in the target table that provide ra-dec coordinates in degrees
raColName = "ra"
decColName =  "dec"
#Name of the Primary-Key column of the target table
idColName = "source_id"
#Name of the columns that will be created or should be used to fill xyz coordinates
xColName = "coord_x"
yColName = "coord_y"
zColName = "coord_z"
#Name of the function that returns a server side cursor to read through all ra-dec columns
#Pick a name that isn't used by any other function in the database
funcName = "getIdRaDecCursor"


##############################################################################################

def RADECtoXYZ(RA,DEC):
        """Convert RA DEC pointing to X Y Z"""
        #convert degrees to radians
        RArad=math.radians(RA)
        DECrad=math.radians(DEC)
        #compute x, y and z values
        X=math.cos(DECrad)*math.cos(RArad)
        Y=math.cos(DECrad)*math.sin(RArad)
        Z=math.sin(DECrad)
        return[X,Y,Z]

def main():
    print("main")
    #connect to database
    #Define our connection string
    conn_string = "host='{}' dbname='{}' user='{}' password='{}'".format(host, dbName, user, password) 

    # print the connection string we will use to connect
    print("Connecting to database\n    ->{}".format(conn_string))

    # get a connection, if a connect cannot be made an exception will be raised here
    with psycopg2.connect(conn_string) as conn:
        
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        with conn.cursor() as cursor:
            print("Connected!\n")
            
            # add column "coord_x", "coord_y", "coord_z" to table "spire_catalog"
            command = """
                DO $$ 
                    BEGIN
                        BEGIN
                            ALTER TABLE {tableName} ADD COLUMN {xName} real;
                            ALTER TABLE {tableName} ADD COLUMN {yName} real;
                            ALTER TABLE {tableName} ADD COLUMN {zName} real;
                        EXCEPTION
                            WHEN duplicate_column THEN RAISE NOTICE 'columns already exist in {tableName}.';
                        END;
                    END;
                $$
                """.format(tableName=tableName, xName=xColName,  yName=yColName, zName=zColName)            
            cursor.execute(command)
            print("Columns x y z added if weren't there")
            
            # declare a server side cursor that will go through the table record more efficiently than just requesting all rows at once
            # let the program error if the function is already defined
            radecCursorDef = """
            CREATE FUNCTION {funcName}(refcursor) RETURNS refcursor AS $$
            BEGIN
                OPEN $1 FOR SELECT {idColName}, {raColName}, {decColName} FROM {tableName};
                RETURN $1;
            END;
            $$ LANGUAGE plpgsql;
            """.format(funcName=funcName, idColName=idColName, raColName=raColName, decColName=decColName, tableName=tableName)
           
            cursor.execute(radecCursorDef);
            cursor.callproc(funcName, ['radecCursor'])
            readCursor = conn.cursor('radecCursor')
            print("Grabbed server side radecCursor")

            #Go through the table with the cursor and update the x y z records at the same time
            update = """
            UPDATE spire_catalog
            SET {xName}={{rowVals[0]}}, {yName}={{rowVals[1]}}, {zName}={{rowVals[2]}}
            WHERE {idName}={{rowVals[3]}}
            """.format(xName = xColName, yName = yColName, zName = zColName, idName = idColName)
            for record in readCursor: 
                source_id, ra, dec = record
                xyz = RADECtoXYZ(ra, dec)
                xyz.append(source_id)
                cursor.execute(update.format(rowVals = xyz))
            print("Filled x y z columns")
            
            #close readCursor
            readCursor.close()
            print("Closed reader cursor")
            
            #delete database cursor getter function
            delRaDecCursorFunc = " DROP FUNCTION {}(refcursor)".format(funcName)
            cursor.execute(delRaDecCursorFunc)
            print("Deleted database function {}".format(funcName))
            # close communication with the PostgreSQL database server for both cursors
            
            cursor.close()
            print("Closed write Cursor")

        # commit the changes and close connection
        conn.commit()
        
    print("Committed changes")

if __name__ == "__main__":
    main()
