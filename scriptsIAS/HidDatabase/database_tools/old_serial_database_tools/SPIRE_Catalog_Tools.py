#bhasnoun

import pyfits, numpy

def FillSPIRECatalogTable(listfile,cur,prefix,last):

	for i in listfile:
		fits = pyfits.open(i,mode='update')
		table = fits[1].data
		for j in range(len(table)):
			source = list(table[j])
			#check if source is already in the database
			#cur.execute("SELECT * FROM SPIRE_Catalog WHERE filepath = '%s'" % META['FILEPATH'])
			#if cur.fetchone()==None:
			cur.execute("select count (*) from SPIRE_Catalog")
			NbLines= cur.fetchone()
			if NbLines!=None: source_id = NbLines[0]
			source.insert(0,int(source_id))
			source[-1] = int(source[-1]) #obsid to int not int32
			source = tuple(source)
			print source
			cur.execute("insert into SPIRE_Catalog values ("+(len(source)-1)*"%s,"+"%s)" , source)


def fill_spire_photo_catalog(listfile, table, conn):
    """Fill Spire Photo Catalog database"""

    cur = conn.cursor()

    for i in listfile:
        fits = pyfits.open(i, mode='update')
        allSources = fits[1].data
        for j in range(len(allSources)):
            source = list(allSources[j])
            cur.execute("select count (*) from " + table)
            nblines = cur.fetchone()
            if nblines is not None: source_id = nblines[0]
            else: source_id = 0
            source.insert(0, int(source_id))
            source[-1] = int(source[-1])  # obsid to int not int32
            source = tuple(source)
            print source
            cur.execute("insert into " + table + " values (" + (len(source) - 1) * "%s," + "%s)", source)

    cur.close()
