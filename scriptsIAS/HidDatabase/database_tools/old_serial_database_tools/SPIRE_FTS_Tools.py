import psycopg2
import pyfits
import glob
import os, string, shutil
import time
from datetime import date
import math
old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/HidDatabase/database_tools/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")

def initDicoFTS():
        DICO={}
        DICO["'BuildVersion'"]='BuildVersion'
        DICO["'calVersion'"]='calVersion'
        DICO["'timeOffset'"]='timeOffset'
        DICO["'timeDrift'"]='timeDrift'
        DICO["'apodName'"]='apodName'
        DICO['OBS_ID']='OBS_ID'
        DICO['OBJECT']='OBJECT'
        DICO['OBSERVER']='OBSERVER'
        DICO['CUSMODE']='CUSMODE'
        DICO['INST_MODE']='INST_MODE'
        DICO['ODNUMBER']='ODNUMBER'
        DICO['RA']='RA'
        DICO['DEC']='DEC'
        DICO['OBS_MODE']='OBS_MODE'
        DICO["'biasFreq'"]='biasFreq'
        DICO['AOR']='AOR'
        DICO['EQUINOX']='EQUINOX'
        DICO['RADESYS']='RADESYS'
        DICO['RA_NOM']='RA_NOM'
        DICO['DEC_NOM']='DEC_NOM'
        DICO['POSANGLE']='POSANGLE'
        DICO["'numScans'"]='numScans'
        DICO["'commandedResolution'"]='commandedResolution'
        DICO["'mapSampling'"]='mapSampling'
        DICO['MAPSAMPL']='MAPSAMPL'
        return DICO

def initDicoFTScube():
        DICO={}
        DICO["'BuildVersion'"]='BuildVersion'
        DICO["'CalVersion'"]='CalVersion'
        DICO["'apodName'"]='apodName'
        DICO["'naxis'"]='naxis'
        DICO["'naxis1'"]='naxis1'
        DICO["'naxis2'"]='naxis2'
        DICO["'naxis3'"]='naxis3'
        DICO['NAXIS']='NAXIS'
        DICO['NAXIS1']='NAXIS3'
        DICO['NAXIS2']='NAXIS2'
        DICO['NAXIS3']='NAXIS1'
        DICO['CRVAL1']='CRVAL1'
        DICO['CRVAL2']='CRVAL2'
        DICO['CTYPE1']='CTYPE1'
        DICO['CTYPE2']='CTYPE2'
        DICO['CDELT1']='CDELT1'
        DICO['CDELT2']='CDELT2'
        DICO['CRPIX1']='CRPIX1'
        DICO['CRPIX2']='CRPIX2'
        DICO['OBS_ID']='OBS_ID'
        DICO['OBJECT']='OBJECT'
        DICO['OBSERVER']='OBSERVER'
        DICO['CUSMODE']='CUSMODE'
        DICO['INST_MODE']='INST_MODE'
        DICO['ODNUMBER']='ODNUMBER'
        DICO['RA']='RA'
        DICO['DEC']='DEC'
        DICO['OBS_MODE']='OBS_MODE'
        DICO['AOR']='AOR'
        DICO['EQUINOX']='EQUINOX'
        DICO['RADESYS']='RADESYS'
        DICO['RA_NOM']='RA_NOM'
        DICO['DEC_NOM']='DEC_NOM'
        DICO['POSANGLE']='POSANGLE'
        DICO["'numScans'"]='numScans'
        DICO["'commandedResolution'"]='commandedResolution'
        DICO["'mapSampling'"]='mapSampling'
        DICO['MAPSAMPL']='MAPSAMPL'
	return DICO

def initDicoFTS2():
        DICO={}
        DICO['BuildVersion']=None
        DICO['calVersion']=None
        DICO['timeOffset']=None
        DICO['timeDrift']=None
        DICO['apodName']="NA"
        DICO['OBS_ID']=None
        DICO['OBJECT']=None
        DICO['OBSERVER']=None
        DICO['CUSMODE']=None
        DICO['INST_MODE']=None
        DICO['ODNUMBER']=None
        DICO['RA']=None
        DICO['DEC']=None
        DICO['OBS_MODE']=None
        DICO['AOR']=None
        DICO['EQUINOX']=None
        DICO['RADESYS']=None
        DICO['RA_NOM']=None
        DICO['DEC_NOM']=None
        DICO['POSANGLE']=None
        DICO['numScans']=None
        DICO['commandedResolution']=None
        DICO['mapSampling']=None
        DICO['MAPSAMPL']=None
        DICO['biasFreq']=None
        return DICO

def initDicoFTScube2():
        DICO={}
        DICO['BuildVersion']=None
        DICO['CalVersion']=None
        DICO['apodName']=None
        DICO['naxis']=None
        DICO['naxis1']=None
        DICO['naxis2']=None
        DICO['naxis3']=None
        DICO['CRVAL1']=None
        DICO['CRVAL2']=None
        DICO['CTYPE1']=None
        DICO['CTYPE2']=None
        DICO['CDELT1']=None
        DICO['CDELT2']=None
        DICO['CRPIX1']=None
        DICO['CRPIX2']=None
        DICO['OBS_ID']=None
        DICO['OBJECT']=None
        DICO['OBSERVER']=None
        DICO['CREATOR']=None
        DICO['CUSMODE']=None
        DICO['INST_MODE']=None
        DICO['ODNUMBER']=None
        DICO['RA']=None
        DICO['DEC']=None
        DICO['OBS_MODE']=None
        DICO['AOR']=None
        DICO['EQUINOX']=None
        DICO['RADESYS']=None
        DICO['RA_NOM']=None
        DICO['DEC_NOM']=None
        DICO['POSANGLE']=None
        DICO['numScans']=None
        DICO['commandedResolution']=None
        DICO['mapSampling']=None
        DICO['MAPSAMPL']=None
        return DICO


	
def FillSPIREFtsL1Table(listfile,cur,prefix,last):
	L1_id=0
	XYZ=[0.,0.,0.]
	for i in listfile:
		if str(i).count("_cube")==0 and str(i).count("Old")==0 and str(i).count("Test")==0 and str(i).count("OTHER")==0:
			DICO={}
			DICO2={}
			DICO=initDicoFTS()
			DICO2=initDicoFTS2()
	                filepath=str(i)
			print filepath
	                hdulist=pyfits.open(filepath)
	                hdr0=hdulist[0].header
			Public=isPublic(hdr0['DATE-OBS'])
			metadata=[]
			#print DICO2
			#print filepath
			dirpath=os.path.dirname(filepath)
			filename=os.path.basename(filepath)
			filetype=filename.split("_")[1]
			if filetype not in ["sdt","sdi","spectrum"]:
				filetype="spectrum2d"
			program_orig = filepath.split("/")[-3]
			#print program_orig
			try:
				program = program_orig.split("_")[0]
				origin=program_orig.split("_")[1]
			except:
				program = program_orig
				origin="IAS"
			# copy the "unavailable image" by default
			#shutil.copy(template_image,imagepath)
			tempName=filepath.split(prefix.capitalize())
			#print tempName
			filepath="/"+prefix.capitalize()+tempName[1]
			imagepath=filepath.replace(".fits",".png")
			hdr1=hdulist[1].header
			# create dictionnary to make correspondance between META and real name
			cards1 = hdr1.cards
			cards0 = hdr0.cards
			DICO=fillDico(cards1,DICO)
			#print "DICO", DICO
			DICO=fillDico(cards0,DICO)
			#print "DICO", DICO
			cur.execute("select count (*) from SPIRE_FTS_L1")
			NbLines= cur.fetchone()
			if NbLines!=None:
				L1_id=NbLines[0]
			#print "L1_ID",L1_id
			metadata.append(L1_id)
			L1_metadata_names=(DICO['OBS_ID'],DICO['OBJECT'],DICO['OBSERVER'],DICO['CUSMODE'],DICO['ODNUMBER'],'DATE','DATE-OBS','DATE-END',\
			DICO["'timeOffset'"],DICO["'timeDrift'"],DICO["'biasFreq'"],DICO['OBS_MODE'],DICO['AOR'],\
			DICO['EQUINOX'],DICO['RADESYS'],DICO['RA_NOM'],DICO['DEC_NOM'],DICO['RA'],DICO['DEC'],\
			#DICO['POSANGLE'],DICO["'numScans'"],DICO["'commandedResolution'"],DICO["'mapSampling'"],DICO["'apodName'"])
			DICO['POSANGLE'],DICO["'numScans'"],DICO["'commandedResolution'"],DICO['MAPSAMPL'],DICO["'apodName'"])
			#print DICO
			#if   str(i).count("_spectrum2d")==0:
			for name in L1_metadata_names:
				try:
					print hdr0[name]
					metadata.append(hdr0[name])
					#print "hdr0",name, hdr0[name]
				except:
					try:
						metadata.append(hdr1[name])
					except:
						print "2 hdr0",name, DICO2[name] 
						metadata.append(DICO2[name])
			#else:
			#	for name in L1_metadata_names:
			#		try:
			#			metadata.append(hdr1[name])
			#			#print "hdr1",name, hdr1[name]
			#		except:
			#			metadata.append(DICO2[name])
			try:
				metadata.append(hdr0[DICO["'BuildVersion'"]])
			except:
				metadata.append(DICO2['BuildVersion'])
			try:
				metadata.append(hdr0[DICO["'calVersion'"]])
			except:
				metadata.append(DICO2['calVersion'])
			metadata.append(filepath)
			metadata.append(program)
			metadata.append(origin)
			metadata.append(filetype)
			metadata.append(filename)
			metadata.append(imagepath)
			try:
				XYZ=RADECtoXYZ(hdr0['RA'],hdr0['DEC'])
				#print XYZ
			except:
				print "no RA DEC available"
			metadata.append(XYZ[0])
			metadata.append(XYZ[1])
			metadata.append(XYZ[2])
			metadata.append( convert_MegaBytes(os.path.getsize(str(i))) )
			metadata.append(Public)
			metadata.append(prefix)
			metadata.append(last)
			cur.execute("insert into SPIRE_FTS_L1 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", metadata)
			#print hdr1['OBS_ID'], "inserted in the database"
			hdulist.close()





def FillSPIREFtsL2Table(listfile,cur,prefix,last):
	L2_id=0
	XYZ=[0.,0.,0.]
	for i in listfile:
		if (str(i).count("_cube.fits")==1 and str(i).count("Old")==0 and str(i).count("old")==0 and str(i).count("Test")==0 and str(i).count("OTHER")==0):
			print str(i)
			filepath=urlFilepath(str(i))
			hdulist=pyfits.open(filepath)
			hdr0=hdulist[0].header
			Public=isPublic(hdr0['DATE-OBS'])
			DICO={}
			DICO2={}
			metadata=[]
			DICO=initDicoFTScube()
			DICO2=initDicoFTScube2()
			#print filepath
			dirpath=os.path.dirname(filepath)
			filename=os.path.basename(filepath)
			program_orig = filepath.split("/")[-3]
			if program_orig=="SAG-4_IAS" or program_orig=="SAG-4_LAM":
					program = program_orig.split("_")[0]
					origin=program_orig.split("_")[1]
			else:
					program = program_orig
					origin="IAS"
			#image=filename.replace(".fits",".png")
			#imagepath=dirpath+"/"+image
			#print imagepath
			# copy the "unavailable image" by default
			#shutil.copy(template_image,imagepath)

			#filepath=filepath.replace("/data/glx-herschel/data1/herschel/","/archive/HERSCHEL/")
			tempName=filepath.split(prefix.capitalize())
                        filepath="/"+prefix.capitalize()+urlFilepath(tempName[1])
			imagepath=filepath.replace(".fits",".png")
			hdr1=hdulist[1].header
			# create dictionnary to make correspondance between META and real name
			cards0 = hdr0.cards
			cards1 = hdr1.cards
			DICO=fillDico(cards0,DICO)
			DICO=fillDico(cards1,DICO)
			#print "DICO 1", DICO
			cur.execute("select count (*) from SPIRE_FTS_L2")
			NbLines= cur.fetchone()
			if NbLines!=None:
				L2_id=NbLines[0]
			metadata.append(L2_id)
			L2_metadata_names_ext0 = (DICO['OBS_ID'],DICO['OBJECT'],DICO['OBSERVER'],DICO['CUSMODE'],DICO['ODNUMBER'],'DATE','DATE-OBS','DATE-END',\
			DICO['OBS_MODE'],\
			DICO['EQUINOX'],DICO['RADESYS'],DICO['RA_NOM'],DICO['DEC_NOM'],DICO['RA'],DICO['DEC'],\
			#DICO['POSANGLE'],DICO["'commandedResolution'"],DICO["'mapSampling'"],DICO["'apodName'"],'CREATOR','WAVELNTH')
			DICO['POSANGLE'],DICO["'commandedResolution'"],DICO['MAPSAMPL'],DICO["'apodName'"],'CREATOR','WAVELNTH')
			L2_metadata_names_ext1 = (\
			#DICO['CRVAL1'],DICO['CRVAL2'],DICO['CDELT1'],DICO['CDELT2'],DICO["'naxis'"],DICO["'naxis1'"],DICO["'naxis2'"],DICO["'naxis3'"],DICO['CRPIX1'],DICO['CRPIX2'],\
			DICO['CRVAL1'],DICO['CRVAL2'],DICO['CDELT1'],DICO['CDELT2'],DICO['NAXIS'],DICO['NAXIS1'],DICO['NAXIS2'],DICO['NAXIS3'],DICO['CRPIX1'],DICO['CRPIX2'],\
			DICO['CTYPE1'],DICO['CTYPE2'])
			#print DICO
			# add metadata from fits extension 0 (primary header)
			for name in L2_metadata_names_ext0:
				try:
					#print hdr0[name]
					if name == "CREATOR":
					 projection=hdr0[name].split("ProjectionTask")[0]
					 if projection.count("herschel.spire.ia.pipeline.spec.regrid")==1 : projection = "Naive"
					 metadata.append(projection)
					else:
					 metadata.append(hdr0[name])
					  
				except:
						metadata.append(DICO2[name])
			# add metadata from fits extension 1 (image header)
			for name in L2_metadata_names_ext1:
				try:
					#print name
					metadata.append(hdr1[name])
				except:
					metadata.append(DICO2[name])
			try:
				metadata.append(hdr0[DICO["'BuildVersion'"]])
			except:
				metadata.append(DICO2['BuildVersion'])
			# add buildVersion from History task
			#try:
		#		metadata.append(hdulist["HistoryTasks"].data[0][3])
		#	except:
		#		metadata.append(DICO2[name])
			try:
				metadata.append(hdr0[DICO["'CalVersion'"]])
			except:
				metadata.append(DICO2['CalVersion'])
			metadata.append(filepath)
			metadata.append(program)
			metadata.append(origin)
			metadata.append(filename)
			metadata.append(imagepath)
			
			try:
				XYZ=RADECtoXYZ(hdr0['RA'],hdr0['DEC'])
				#print XYZ
			except:
				print "no RA DEC available"
			metadata.append(XYZ[0])
			metadata.append(XYZ[1])
			metadata.append(XYZ[2])
			metadata.append(convert_MegaBytes(os.path.getsize(str(i))) )
			metadata.append(Public)
			metadata.append(prefix)
			metadata.append(last)
			# trick to remove concatenated metadata for combined obsids - the first one is kept
			for index,meta in enumerate(metadata):
				if str(meta).find("&")>0 :
					print meta
					metadata[index]=meta.split("&")[0]
					print metadata
			#cur.execute("insert into SPIRE_FTS_L2 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", metadata)
			cur.execute("insert into SPIRE_FTS_L2 values (%s%%s)" % ("%s," * (len(metadata)-1)), metadata)
			print filename, "inserted in the database"
			hdulist.close()

