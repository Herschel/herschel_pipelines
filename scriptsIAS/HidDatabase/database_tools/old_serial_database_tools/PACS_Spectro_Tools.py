import psycopg2
import pyfits
import glob
import os, string, shutil, sys
import time
from datetime import date
import math
import pdb
#from fitsToPng import makeMapThumbPlotHeader
from multiprocessing import Pool

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/HidDatabase/database_tools/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")


def addToDatabase(release,fitsDir):

	# Database connection
	baseName = "'HID_glx'"
	try: conn = psycopg2.connect("dbname="+baseName+" user='sitools'")
	except: sys.exit()
	
	cur = conn.cursor()
	
	# Add L1 files to database
	motifs, exclure = [".fits","L1"], []
	listfile = getFilesMotif(fitsDir,motifs,exclure)
	FillPACSspectroL1Table(listfile,cur,release,False)
	
	# Add L1 files to database
	motifs, exclure = [".fits","L2"], []
	listfile = getFilesMotif(fitsDir,motifs,exclure)
	FillPACSspectroL2Table(listfile,cur,release,False)
	
	conn.commit()
	
	cur.close()
	conn.close()
	
	Pool().map(makeMapThumbPlotHeader,fitsDir)

def makeDicoPacsSpectro(hdr):
	cards = hdr.cards
	DICO = {}
	for card in cards:
		chaine_card=str(card)
		if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
			chaine_split = value=chaine_card.split("=")
			chaine_resplit = chaine_split[0].split()
			chaine_resplit = chaine_resplit[1].split(".")
			meta_des = chaine_split[1].strip()
			meta_key = chaine_resplit[1]
			DICO[meta_key] = meta_des
	return DICO

def getMetadata(hdr,ext):

	METADATA = {}
	cards = hdr.cards
	DICO = makeDicoPacsSpectro(hdr)
	
	for card in cards:
		chaine_card=str(card)
		#print chaine_card
		#if chaine_card.count("META")==0 and chaine_card.count("HIERARCH")==0 and chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")==1:
		if chaine_card.count("META")==0 and chaine_card.count("HIERARCH")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")==1:
			#print chaine_card			
			cle = chaine_card.split("=")[0]
			#print cle
			valeur = chaine_card.split("=")[1].split("/")[0]
			#print valeur 
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
			print valeur 
			METADATA[ext+"_"+cle] = valeur

		
		if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==0 and chaine_card.count("=")>=1:
			
			if chaine_card.count("\n"):
				
				chaine_card = chaine_card.split("\n")[0]
				chaine_card = chaine_card.replace("&","")
			
			cle = chaine_card.split("=")[0]
			valeur = chaine_card.split("=")[1].split("/")[0]
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
			try: METADATA[ext+"_"+DICO[cle].replace("'","").upper()] = valeur
			except: METADATA[ext+"_"+DICO[cle].replace("'","").upper()] = "PROBLEM"
	return METADATA

def getAllMetadataPacsSpectro(fitsFile,prefix):

	METADATA = {}
	
	filepathComplete = str(fitsFile)
	hdulist = pyfits.open(filepathComplete)

	filename = os.path.basename(filepathComplete)
	filepath = "/"+prefix.capitalize()+filepathComplete.split(prefix.capitalize())[1]
	
	METADATA["FILENAME"] = filename
	METADATA["FILEPATH"] = urlFilepath(filepath)
	METADATA["IMAGEPATH"] = urlFilepath(filepath)[:len(filepath)-5]+".png"
	METADATA["FILESIZE"] = convert_MegaBytes(os.path.getsize(filepathComplete))
	
	METADATA["PROGRAM"] = filepathComplete.split("/")[-3]
	METADATA["CHAMP"]   = filepathComplete.split("/")[-2]
	
	tmp = os.path.splitext(filename)[0] # We get rid of the extension
	tmp = tmp.split("_") # The last one if the product name as defined in get_pacs_spectro_from_HSA.py
	
	METADATA["PRODUCT"] = tmp[-1]
	

	METADATA["PROCESSING"] = "HIPE Only"
	if filename.count("DegIAS")>0:
		METADATA["PROCESSING"] = "HIPE + Removed Dippers IAS"
	if filename.count("DegPACSman")>0:
		METADATA["PROCESSING"] = "HIPE + Removed Dippers PACSman"

	HDR_2nd = {'Frame':"Signal", 'RasterCubeBFF':"Flux", 'RasterCube':"Flux", 'RebinnedCube':"Image", 'RebinnedCubeAfterSub':"Image", 'MapFlux':"MAPFLUX", 'SpecProject FinalCube':"Image", 'Drizzle FinalCube':"Image", 'RegularSpatialGridInterpolationCube':"Image",
	# New denomination
"Cube":"Flux",
"Cube":"Flux",
"RebinnedCube":"Image",
"RebinnedCube":"Image",
"ProjectedCube":"Image",
"ProjectedCube":"Image",
"DrizzledCube":"Image",
"EquidistantDrizzledCube":"Image",
"InterpolatedCube":"Image",
"EquidistantInterpolatedCube":"Image"}

	#HDR0
	hdr0=hdulist[0].header
	METADATA["PUBLIC"] = isPublic(hdr0['DATE-OBS'])
	DICO = makeDicoPacsSpectro(hdr0)
	#print sorted(DICO.iteritems())
	METADATA.update(getMetadata(hdr0,"0"))

	#HDR1
	hdr1=hdulist[HDR_2nd[METADATA["PRODUCT"]]].header

	DICO = makeDicoPacsSpectro(hdr1)
	#print sorted(DICO.iteritems())
	METADATA.update(getMetadata(hdr1,"1"))
	
	#~ if fitsFile.count("_ProjectedCube")>0 or filename.count("_DrizzledCube")>0 or filename.count("_RegularSpatialGridInterpolationCube")>0:
	# By default define a value. Change it if it falls into the following if. Don't know if "mapflux" is still used though. Keep it for retrocompatibility purpose.
	METADATA["BUILDVERSION"] = hdulist["HistoryTasks"].data[-1][-1]
	if fitsFile.count("MapFlux")==1 :
		METADATA["BUILDVERSION"] = "See Corresponding FinalCube"
		METADATA["1_NAXIS3"] = "0"
		
	try:
		if METADATA["0_ISOFFPOSITION"]=="T":
			METADATA["0_ISOFFPOSITION"]="True"
		if METADATA["0_ISOFFPOSITION"]=="F":
			METADATA["0_ISOFFPOSITION"]="False"
	except:
		pass
	
	try: 
		METADATA["DURATION"]=hdr0['EPOCH']
	except: 
		METADATA["DURATION"]=hdr0['EQUINOX']
	
	xyz = RADECtoXYZ(float(METADATA['0_RA']),float(METADATA['0_DEC']))
	METADATA['XYZ_X']= xyz[0]
	METADATA['XYZ_Y']= xyz[1]
	METADATA['XYZ_Z']= xyz[2]
	
	hdulist.close()
	
	return METADATA

def FillPACSspectroL1Table(listfile,cur,prefix,last):

	L1_id=0
	for i in listfile:
		print i
		META = getAllMetadataPacsSpectro(i,prefix)
		#check if file is already in the database
		cur.execute("SELECT * FROM PACS_Spectro_L1 WHERE filepath = '%s'" % META['FILENAME'])
		if cur.fetchone()==None:
			
			cur.execute("select count (*) from PACS_Spectro_L1")
			NbLines= cur.fetchone()
			if NbLines!=None:
				L1_id=NbLines[0]
			metadata_L1=(L1_id,META['0_OBJECT'],META['0_OBS_ID'],META['0_OBSERVER'],META['0_CAMNAME'],META['0_RA'],META['0_DEC'],\
			META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],\
			META['0_OBSERVING MODE'],META['0_ALGORITHM'],META['0_LINE 1'],META['0_RASTERID'],META['0_ISOFFPOSITION'],\
			META['1_NAXIS1'],META['1_NAXIS2'],META['1_NAXIS3'],META["PROGRAM"],META["PRODUCT"],META["PROCESSING"],META['FILENAME'],META['FILEPATH'],META["FILESIZE"],META["PUBLIC"],prefix,last)
			cur.execute("insert into PACS_Spectro_L1 values ("+(len(metadata_L1)-1)*"%s,"+"%s)" , metadata_L1)

def FillPACSspectroL2Table(listfile,cur,prefix,last):

	L2_id=0
	for i in listfile:
		print i
		META = getAllMetadataPacsSpectro(i,prefix)
		#print META
		#check if file is already in the database
		cur.execute("SELECT * FROM PACS_Spectro_L2 WHERE filepath = '%s'" % META['FILEPATH'])
		if cur.fetchone()==None:
			
			cur.execute("select count (*) from PACS_Spectro_L2")
			NbLines= cur.fetchone()
			if NbLines!=None:
				L2_id=NbLines[0]
			
			metadata_L2=(L2_id,META['0_OBJECT'],META['0_OBS_ID'],META['0_OBSERVER'],META['0_CAMNAME'],META['0_RA'],META['0_DEC'],\
			META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],\
			#META['0_OBS_MODE'],META['0_ALGORITH'],META['0_LINE1'],\ ### warning chaqnge in v15 LINE01
			META['0_OBS_MODE'],META['0_ALGORITH'],META['0_LINE01'],\
			META['BUILDVERSION'],META['1_NAXIS1'],META['1_NAXIS2'],META['1_NAXIS3'],META["PROGRAM"],META["PRODUCT"],META["PROCESSING"],META['FILENAME'],META['IMAGEPATH'],META['FILEPATH'],\
			META["FILESIZE"],META["PUBLIC"],prefix,last)
			
			cur.execute("insert into PACS_Spectro_L2 values ("+(len(metadata_L2)-1)*"%s,"+"%s)" , metadata_L2)

################################################################################################################################################################################
################################################################################################################################################################################
################################################################################################################################################################################
################################################################################################################################################################################
################################################################################################################################################################################


def getAllMetadataPacsSpectro25(fitsFile,prefix):

	METADATA = {}
	
	filepathComplete = str(fitsFile)
	hdulist = pyfits.open(filepathComplete, mode='update')

	filename = os.path.basename(filepathComplete)
	filepath = "/"+prefix.capitalize()+filepathComplete.split(prefix.capitalize())[1]
	
	METADATA["FILENAME"] = filename
	METADATA["FILEPATH"] = urlFilepath(filepath)
	METADATA["IMAGEPATH"] = METADATA["FILEPATH"][:len(filepath)-5]+".png".replace("_raw","").replace("_wm-2px-1","")
	METADATA["FILESIZE"] = convert_MegaBytes(os.path.getsize(filepathComplete))
	METADATA["PROGRAM"] = filepathComplete.split("/")[-4]
	METADATA["CHAMP"]   = filepathComplete.split("/")[-3]
	METADATA["PRODUCT"] = "MapFlux"
	METADATA["LINE"] = filepathComplete.split("/")[-2]
	METADATA["PROCESSING"] = "HIPE RasterCube + PACSman"

	#HDR0
	hdr0 = hdulist[0].header
#   METADATA["PUBLIC"] = isPublic(hdr0['DATE-OBS'])
	cards = hdr0.cards
	for card in cards:
		#print card
		chaine_card=str(card)
		cle = chaine_card.split("=")[0]
		valeur = chaine_card.split("=")[1].split(" /")[0]
		cle = cle.replace(" ","").strip().strip("_")
		valeur = valeur.replace("_","").replace("'","")
		valeur = valeur.strip()
		METADATA[cle] = valeur

	if filename.count("Flux")>0:
		
		METADATA["DESCRIP"] = "Spaxel line flux projection onto an oversampled grid."
		if filename.count("Flux.fits")>0:
			METADATA["DESCRIP"] = METADATA["DESCRIP"] + " Filled holes."
	if filename.count("spectra")>0:
		METADATA["DESCRIP"] = "Spaxel spectra projection onto an oversampled grid."
	METADATA["DESCRIP"] = METADATA["DESCRIP"] + " " + METADATA["BUNIT"]
	
	METADATA["DURATION"] = hdr0['EQUINOX']
	
	METADATA['RA'] = 0.
	METADATA['DEC'] = 0.
	xyz = RADECtoXYZ(float(METADATA['RA']),float(METADATA['DEC']))
	METADATA['XYZ_X']= xyz[0]
	METADATA['XYZ_Y']= xyz[1]
	METADATA['XYZ_Z']= xyz[2]

	#Rajout MetaData
	if filename.count("Flux")>0:
		
		mapDescrip = {"Map1":"Flux Map", "Map2":"Noise", "Map3":"Velocity Map", "Map4":"Velocity Map Error", "Map5":"FWHM Map", "Map6":"FWHM Map Error", "Map7":"Continuum Map", "Map8":"Continuum Map"}
		for key,value in mapDescrip.iteritems(): hdr0.update(key,value)
	
	basePathCorres = filepathComplete.replace("PACSman_Fits","HIPE_Fits").split(METADATA["LINE"])[0]
	#print basePathCorres
	motifs = ["ProjectedCube.fits",METADATA['OBS_ID']]
	correspondingProjectedCube = getFilesMotif(basePathCorres,motifs,[])
	if len(correspondingProjectedCube)==1:
		
		#print correspondingProjectedCube[0]
		fits_Corres=pyfits.open(correspondingProjectedCube[0])
		dateObs = fits_Corres[0].header["DATE-OBS"]
		fits_Corres.close()
		METADATA["PUBLIC"] = isPublic(dateObs)
		hdr0.update("DATE-OBS",str(dateObs))
	else: METADATA["PUBLIC"] = False

	#print METADATA["PUBLIC"]
	
	hdulist.close()
	
	return METADATA

def FillPACSspectroL25Table(listfile,cur,prefix,last):

	L25_id=0
	for i in listfile:
		print i
		META = getAllMetadataPacsSpectro25(i,prefix)
		#check if file is already in the database
		cur.execute("SELECT * FROM PACS_Spectro_L25 WHERE filename = '%s'" % META['FILEPATH'])
		if cur.fetchone()==None:
			
			cur.execute("select count (*) from PACS_Spectro_L25")
			NbLines= cur.fetchone()
			if NbLines!=None:
				L25_id=NbLines[0]
			metadata_L25=(L25_id,META['CHAMP'],META['OBS_ID'],
			META['RA'],META['DEC'],META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],META['BUNIT'],\
			META['LINE'],META['HIPE'],META['PACSMAN'],META['NAXIS1'],META['NAXIS2'],META["PROGRAM"],\
			META["PRODUCT"],META["DESCRIP"],\
			META["PROCESSING"],META['FILENAME'],META['IMAGEPATH'],META['FILEPATH'],\
			META["FILESIZE"],META["PUBLIC"],prefix,last)
			cur.execute("insert into PACS_Spectro_L25 values ("+(len(metadata_L25)-1)*"%s,"+"%s)" , metadata_L25)
			#print META['FILENAME'], " in the database."



