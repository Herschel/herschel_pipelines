import psycopg2
import pyfits
import glob
import os, string, shutil
import time
from datetime import date
import math
import pdb

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/HidDatabase/database_tools/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")

def initDicoPACS2():
	DICO={}
	DICO['OBJECT']=None
	DICO['OBSERVER']=None
	DICO['OBS_ID']=None
	DICO['RA']=None
	DICO['DEC']=None
	DICO['OBS_MODE']=None
	DICO['AOR']=None
	DICO['CUSMODE']=None
	DICO['POINTMOD']=None
	DICO['DEC_NOM']=None
	DICO['RA_NOM']=None
	DICO['RADESYS']=None
	DICO['EQUINOX']=None
	DICO['OBS_MODE']=None
	DICO['ODNUMBER']=None
	DICO['blue']=None
	DICO['repFactor']=None
	DICO['restwav']=None
	return DICO

def initDicoPACS():
	DICO={}
	DICO["'BuildVersion'"]='BuildVersion'
	DICO['OBJECT']='OBJECT'
	DICO['OBSERVER']='OBSERVER'
	DICO['OBS_ID']='OBS_ID'
	DICO['RA']='RA'
	DICO['DEC']='DEC'
	DICO['OBS_MODE']='OBS_MODE'
	DICO['AOR']='AOR'
	DICO['CUSMODE']='CUSMODE'
	DICO['POINTMOD']='POINTMOD'
	DICO['DEC_NOM']='DEC_NOM'
	DICO['RA_NOM']='RA_NOM'
	DICO['RADESYS']='RADESYS'
	DICO['EQUINOX']='EQUINOX'
	DICO['OBS_MODE']='OBS_MODE'
	DICO['ODNUMBER']='ODNUMBER'
	DICO["'blue'"]='blue'
	DICO["'repFactor'"]='repFactor'
	DICO["'restwav'"]='restwav'
	return DICO


def FillPACSPhotoL1Table(listfile,cur,blue,prefix,last):
	XYZ=[0.,0.,0.]
	L1_id=0
	for i in listfile:
		metadata=[]
		DICO_PACS=initDicoPACS()
		##### init Dictionnaries
		#### need DICO2 to initialize with values with correct type when metadata not found in the fits file
		#### need DICO  to get correspondance bw META and known keyword
		#DICO_PACS2=initDicoPACS2()
		if  str(i).count(blue)==1   and  str(i).count("level1Frames")==1 and str(i).count("test")==0 and str(i).count("old")==0:
			filepath=urlFilepath(str(i))
			print filepath
			program = filepath.split("/")[-3]
			filename=os.path.basename(filepath)
			dirpath=os.path.dirname(filepath)
			hdulist=pyfits.open(filepath)
			#filepath=filepath.replace("/data/glx-herschel/data1/herschel/","/archive/HERSCHEL/")
			hdr0=hdulist[0].header
			Public=isPublic(hdr0['DATE-OBS'])
			# create dictionnary to make correspondance between META and real name
			cards = hdr0.cards
			for card in cards:
				chaine_card=str(card)
				if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
					#print chaine_card
					chaine_split=chaine_card.split("=")
					#print "Chaine 1",chaine_split[1].strip()
					chaine_resplit=chaine_split[0].split()
					chaine_resplit=chaine_resplit[1].split(".")
					#print "Chaine resplit",chaine_resplit
					DICO_PACS[chaine_split[1].strip()]=chaine_resplit[1]
				#TODO creer un dictionnary avec META_10:'script_ID'
			#print "DICO_PACS", DICO_PACS
			#tempName=filepath.split("/")
			#filepath="/"+tempName[len(tempName)-4]+"/"+tempName[len(tempName)-3]+"/"+tempName[len(tempName)-2]+"/"+filename
			tempName=filepath.split(prefix.capitalize())
			filepath="/"+prefix.capitalize()+tempName[1]
			cur.execute("select max(l1_id) from pacs_photo_l1")
			MaxL1=cur.fetchone()
			print MaxL1[0]
			if MaxL1[0]!=None:
				 L1_id=MaxL1[0]+1
			#print "L1_ID",L1_id
			metadata.append(L1_id)
			L1_metadata_names=(DICO_PACS['OBS_ID'],DICO_PACS['OBJECT'],DICO_PACS['OBSERVER'],DICO_PACS['CUSMODE'],DICO_PACS['ODNUMBER'],'DATE','DATE-OBS','DATE-END',\
			DICO_PACS['OBS_MODE'],DICO_PACS['POINTMOD'],DICO_PACS['AOR'],DICO_PACS['EQUINOX'],DICO_PACS['RADESYS'],DICO_PACS['RA_NOM'],DICO_PACS['DEC_NOM'],DICO_PACS['RA'],DICO_PACS['DEC'],\
			DICO_PACS["'blue'"],DICO_PACS["'repFactor'"])
			#print "L1_metat",L1_metadata_names
			for name in L1_metadata_names:
				#print name
				try:
					#print "name",name
					#print hdr0[name]
					metadata.append(hdr0[name])
				except:
					if (name=='OBS_ID'):
						splitName=filename.split("_")
						obsid=splitName[1].replace("L","")
						obsid=int(obsid,16)
						hdr0.update(name,obsid)
						metadata.append(hdr0[name])
					else: 
						#print "DICO_PACS2[name]",DICO_PACS2[name]
						metadata.append(None)
			#print "metadata",metadata
			metadata.append(blue)
			metadata.append(filepath)
			metadata.append(program)
			processing="HIPE_Scanamorphos"
			metadata.append(processing)
			metadata.append(filename)
			try:
				XYZ=RADECtoXYZ(hdr0['RA'],hdr0['DEC'])
				#print XYZ
			except:
				print "no RA DEC available"
			metadata.append(XYZ[0])
			metadata.append(XYZ[1])
			metadata.append(XYZ[2])
			metadata.append( float(os.path.getsize(str(i))) )
			metadata.append( convert_MegaBytes(os.path.getsize(str(i))) )
			metadata.append(Public)
			metadata.append(prefix)
			metadata.append(last)
			# check L1 obsid in the database: if not in L1 table,  insert, else update
			cur.execute("insert into PACS_Photo_L1 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", metadata)
			print hdr0['OBS_ID'], "inserted in the database"
			#else:
			#	print "update ",filepath 
			#	cur.execute("UPDATE L1 SET observer = '%s', cusmode = '%s',creationDate = '%s', startDate = '%s' WHERE obsid ='%d'" % (hdr0['OBSERVER'],hdr0['CUSMODE'],hdr0['DATE'],hdr0['DATE-OBS'],hdr0['OBS_ID
			#	']))
			hdulist.close()
	
	
def FillPACSPhotoL2Table(listfile,color,cur,prefix,last):
	############ L2 Table
	L2_id=0 # primary key to L2 table PACS_PHOTO_L2
	L2_obsids_id=0 # primary key to PACS_Photo_L2_obsids 
	for i in listfile:
		XYZ=[0.,0.,0.]
		if   str(i).count(color)==1 and str(i).count("old")==0 and str(i).count("test")==0 :
			DICO_PACS=initDicoPACS()
			metadata_L2=[]
			DICO2_PACS=initDicoPACS2()
			filepath=urlFilepath(str(i))
			print filepath
			program = filepath.split("/")[-3]
			if str(i).count("SCANAMORPHOS")==1:
				processing="SCANAMORPHOS"
				processingL1="HIPE_Scanamorphos"
			elif str(i).count("HIPE")==1:
				processing="HIPE"
				processingL1="HIPE_Scanamorphos"
			# KD elif str(i).count("UNIMAP")==1:
			#~ elif str(i).count('UNIMAP')!=0:
			elif "UNIMAP" in i:
				processing="UNIMAP"
				processingL1="HIPE_Scanamorphos"
			else:
				processing="SANEPIC"
				processingL1="HIPE_Sanepic"
			print processing
			filename=os.path.basename(filepath)
			dirpath=os.path.dirname(filepath)
			hdulist=pyfits.open(filepath)
			#filepath=filepath.replace("/data/glx-herschel/data1/herschel/","/archive/HERSCHEL/")
			tempName=filepath.split(prefix.capitalize())
			filepath="/"+prefix.capitalize()+tempName[1]
			imagepath=filepath.replace(".fits",".png")
			fullsizeimagepath=filepath.replace(".fits","_fullsize.png")
			hdr0=hdulist[0].header
			hdr1=hdulist[1].header
			Public=isPublic(hdr0['DATE-OBS'])
			# create dictionnary to make correspondance between META and real name
			cards = hdr0.cards
			for card in cards:
				chaine_card=str(card)
				if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==1:
					#print chaine_card
					chaine_split=chaine_card.split("=")
					#print "Chaine 1",chaine_split[1].strip()
					chaine_resplit=chaine_split[0].split()
					chaine_resplit=chaine_resplit[1].split(".")
					#print "Chaine resplit",chaine_resplit
					DICO_PACS[chaine_split[1].strip()]=chaine_resplit[1]
				#TODO creer un dictionnary avec META_10:'script_ID'
			#print "DICO_PACS", DICO_PACS
			#print filepath 
			#check if file is already in the database
			cur.execute("SELECT * FROM PACS_Photo_L2 WHERE filename like '"+filename+"' and release like '"+prefix+"'")
			try: 
				duration=hdr1['EPOCH']
			except:
				print " hdr1"
				try:
					duration=hdr1['EQUINOX']
				except:
					try:
						duration=hdr0['EQUINOX']
					except:
						duration=DICO2_PACS['EQUINOX']	
			try:
				crota2=hdr1['CROTA2']
			except:
				try:
					crota2=hdr0['CROTA2']
				except:
					crota2=None
			try:
				observer=hdr0['OBSERVER']
			except:
				observer=None
			if cur.fetchone()==None:
			###### STEP 1 insert into L2
				cur.execute("select max(l2_id) from pacs_photo_l2")
				MaxL2=cur.fetchone()
				print MaxL2[0]
				if MaxL2[0]!=None:
				 L2_id=MaxL2[0]+1
				#print L2_id
				poly=calculateSpoly(str(i))
				print poly
				if processing=="HIPE":
					try:
						wavelength=hdr0['WAVELNTH']*1E-06 # in micrometers in the header
					except:
						wavelength=None
					metadata_L2 = [L2_id, hdr0['OBJECT'],filepath,observer,Public,program,processing,filename,imagepath,fullsizeimagepath,hdr1['CRVAL1'],hdr1['CRVAL2'],hdr1['CDELT1'],hdr1['CDELT2'],crota2,hdr1['NAXIS1'],hdr1['NAXIS2'],hdr1['CRPIX1'],hdr1['CRPIX2'],\
					hdr1['CTYPE1'],hdr1['CTYPE2'],duration,wavelength,hdr0['DESC']]
				else:
					try:
						wavelength=hdr0[DICO_PACS["'restwav'"]]	
					except:
						wavelength=None
					# KD metadata_L2 = [L2_id, hdr0['OBJECT'],filepath,observer,Public,program,processing,filename,imagepath,fullsizeimagepath,hdr0['CRVAL1'],hdr0['CRVAL2'],hdr0['CDELT1'],hdr0['CDELT2'],crota2,hdr1['NAXIS1'],hdr1['NAXIS2'],hdr0['CRPIX1'],hdr0['CRPIX2'],\
					metadata_L2 = [L2_id, hdr0['OBJECT'],filepath,observer,Public,program,processing,filename,imagepath,fullsizeimagepath,hdr1['CRVAL1'],hdr1['CRVAL2'],hdr1['CDELT1'],hdr1['CDELT2'],crota2,hdr1['NAXIS1'],hdr1['NAXIS2'],hdr1['CRPIX1'],hdr1['CRPIX2'],\
					hdr1['CTYPE1'],hdr1['CTYPE2'],duration,wavelength,hdr0['DESC']]
				# add buildVersion from History task
				try:
					print "BuildVersion", hdulist["HistoryTasks"].data[0][3]
					metadata_L2.append(hdulist["HistoryTasks"].data[0][3])
				except:
					metadata_L2.append(None)
				try:
					XYZ=RADECtoXYZ(hdr1['CRVAL1'],hdr1['CRVAL2'])
					#print XYZ
				except:
					try:
						XYZ=RADECtoXYZ(hdr0['CRVAL1'],hdr0['CRVAL2'])
					except:
						print "no RA DEC available"
				metadata_L2.append(XYZ[0])
				metadata_L2.append(XYZ[1])
				metadata_L2.append(XYZ[2])
				metadata_L2.append( float(os.path.getsize(str(i))) )
				metadata_L2.append( convert_MegaBytes(os.path.getsize(str(i))) )
				metadata_L2.append(prefix)
				metadata_L2.append(last)
				metadata_L2.append(poly)
				cur.execute("insert into PACS_Photo_L2 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" , metadata_L2)
				print filename, " in the database"
				###### STEP 2 insert into L2_obsids
				# combined map
				# OBSIDS extension exists
				#for j in range(len(hdulist[len(hdulist)-1].data)):
				for j in range(len(hdulist["obsids"].data)):
					#print "j ",j
					cur.execute("select max(L2_obsids_id) from pacs_photo_l2_obsids")
					MaxL2obsids=cur.fetchone()
					print MaxL2obsids[0]
					if MaxL2obsids[0]!=None:
				 		L2_obsids_id=MaxL2obsids[0]+1
					#cur.execute("select L1_id from PACS_Photo_L1 where obsid="+str(hdulist[len(hdulist)-1].data[j][0])+" and camera like '"+color+"'"+" and processing like '"+processingL1+"'"+" and release like '"+prefix+"'")
					cur.execute("select L1_id from PACS_Photo_L1 where obsid="+str(hdulist["obsids"].data[j][0])+" and camera like '"+color+"'"+" and processing like '"+processingL1+"'"+" and release like '"+prefix+"'")
					L1_id=cur.fetchone()
					print "obsid, L1_id,color ",str(hdulist["obsids"].data[j][0]),L1_id, color
					metadata_L2_obsids=(L2_obsids_id,L2_id,int(hdulist["obsids"].data[j][0]),L1_id,processing,prefix)
					#print metadata_L2_obsids
					cur.execute("insert into PACS_Photo_L2_obsids values (%s,%s,%s,%s,%s,%s)",metadata_L2_obsids)
				#print "insert into L2_obsids",L2_obsids_id
			hdulist.close()

def FillPACSPhotoPSFTable(listfile,cur,prefix,last):
	buildVersion="Scanamorphos v24"
	############ PSF Table
	PSF_id=0
	metadata_PSF=[]
	XYZ=[0.,0.,0.]
	#listR1=["OT2_hdole","DDT_mustdo","SAG-4","OT1_lmontier"]
	for i in listfile:
			filepath=str(i)
			hdulist=pyfits.open(filepath)
			hdr0=hdulist[0].header
			dirpath=os.path.dirname(filepath)
			filename=os.path.basename(filepath)
			print filename
			try: crota2=hdr0['CROTA2']
			except: crota2=None
			#check if file is already in the database
			cur.execute("SELECT * FROM PACS_Photo_PSF WHERE filename like '"+filename+"' and release like '"+prefix+"'" )
			if cur.fetchone()==None:
				###### STEP 1 insert into PSF
				cur.execute("select max(PSF_id) from PACS_Photo_PSF")
				MaxPSF=cur.fetchone()
				if MaxPSF[0]!=None:
					PSF_id=int(MaxPSF[0])+1
					#print PSF_id
				cur.execute("SELECT * FROM PACS_Photo_PSF")
				colnames = [desc[0] for desc in cur.description]
				metadata_PSF = [PSF_id, filepath,hdr0['DATE'],hdr0['FILTER'],hdr0['VSCAN'],hdr0['OBSMODE'],filename,\
				hdr0['CRVAL1'],hdr0['CRVAL2'],hdr0['CDELT1'],hdr0['CDELT2'],crota2,hdr0['NAXIS1'],hdr0['NAXIS2'],hdr0['CRPIX1'],hdr0['CRPIX2'],hdr0['CTYPE1'],hdr0['CTYPE2'],\
				hdr0['EQUINOX']]
				#print  metadata_PSF
				# add buildVersion Hard Coded (bou...) 
				metadata_PSF.append(buildVersion)
				# add XYZ
				try: XYZ=RADECtoXYZ(hdr0['CRVAL1'],hdr0['CRVAL2'])
				except: print "no CRAVL1 CRVAPSF available"
				metadata_PSF.append(XYZ[0])
				metadata_PSF.append(XYZ[1])
				metadata_PSF.append(XYZ[2])
				metadata_PSF.append(float(os.path.getsize(str(i))) )
				metadata_PSF.append( convert_MegaBytes(os.path.getsize(str(i))) )
				metadata_PSF.append(prefix)
				metadata_PSF.append(last)
				cur.execute("insert into PACS_Photo_PSF values ("+(len(metadata_PSF)-1)*"%s,"+"%s)", metadata_PSF)
				print filename, " in the database"
			hdulist.close()
