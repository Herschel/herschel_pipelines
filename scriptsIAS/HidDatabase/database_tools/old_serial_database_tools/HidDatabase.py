#!/usr/bin/env python 
# Small script to show PostgreSQL and Pyscopg together
# insert metadata from PACS photo SPIRE photo and FTS  into the Herchel Datatabase (HID)
# /!\ WARNING: This script can only works when launched from the glx-herschel server since the database is stored here locally.
#
# HOW TO USE
# Example : You want to add the release R9X_pacs_spectro
# 1) Change "prefix_list_pacs_spectro" for the right release
# 2) Add R9X_pacs_spectro to the dictionnary "last_dico_pacs_spectro" and turn it to True
# 3) Turn all the others to False in the same dictionary to tell the script that this release is the last. 

import psycopg2
import pyfits
import glob
import os, string, shutil
import socket # To test the hostname

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/HidDatabase/database_tools/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Photo_Tools.py")
execfile(old_serial_database_tools+"SPIRE_FTS_Tools.py")
execfile(old_serial_database_tools+"SPIRE_Catalog_Tools.py")
execfile(old_serial_database_tools+"PACS_Photo_Tools.py")
execfile(old_serial_database_tools+"PACS_Spectro_Tools.py")
execfile(old_serial_database_tools+"SANEPIC_Tools.py")

expected_host = "glx-herschel"
if (socket.gethostname() != expected_host):
	print("Please launch this script from %s where the local database is. You're presently on %s" % (expected_host, socket.gethostname()))
	exit(1)

baseName = "'HID_glx'"
drop=False
#prefix_list_spire_photo=["attic","R0_spire_photo","R1_spire_photo","R2_spire_photo","R3_spire_photo","R4_spire_photo","R5_spire_photo","R6_spire_photo","R7_spire_photo","R8_spire_photo"]
prefix_list_spire_photo=["R8_spire_photo"]
#prefix_list_pacs_photo=["attic","R0_pacs_photo","R1_pacs_photo","R2_pacs_photo","R3_pacs_photo","R4_pacs_photo"]
prefix_list_pacs_photo=["R4_pacs_photo"]
#prefix_list_spire_fts=["attic","R1_spire_fts","R2_spire_fts","R3_spire_fts","R4_spire_fts","R5_spire_fts","R6_spire_fts","R7_spire_fts"]
prefix_list_spire_fts=["R7_spire_fts"]
prefix_list_pacs_spectro=["R7_pacs_spectro"]
prefix_list_pacs_photo_psf=["R0_PSF"]
#prefix_list_pacs_spectro=["R1_pacs_spectro","R2_pacs_spectro","R3_pacs_spectro","R4_pacs_spectro","R5_pacs_spectro", "R6_pacs_spectro", "R7_pacs_spectro"]

last_dico_spire_photo={"R8_spire_photo":True,"R7_spire_photo":False,"R6_spire_photo":False,"R5_spire_photo":False,"R4_spire_photo":False,"R3_spire_photo":False,"R2_spire_photo":True,"R1_spire_photo":False, "R0_spire_photo":False,"attic":False}
last_dico_pacs_photo={"R4_pacs_photo":True,"R3_pacs_photo":False,"R2_pacs_photo":False,"R1_pacs_photo":False,"R0_pacs_photo":False,"attic":False}
last_dico_pacs_photo_psf={"R0_PSF":True}
last_dico_spire_FTS={"R7_spire_fts":True,"R6_spire_fts":False,"R5_spire_fts":False,"R4_spire_fts":False,"R3_spire_fts":False,"R2_spire_fts":False,"R1_spire_fts":False,"attic":False}
last_dico_pacs_spectro={"attic":False,"R0_pacs_spectro":False,"R1_pacs_spectro":False,"R2_pacs_spectro":False,"R3_pacs_spectro":False,"R4_pacs_spectro":False,"R5_pacs_spectro":False, "R6_pacs_spectro":False,"R7_pacs_spectro":True}

#Instrument to process
#~ traitement = ["SPIRE_PHOTO_L1","SPIRE_FTS_L1","PACS_PHOTO_L1","PACS_SPECTRO_L1","SPIRE_FTS_L2","PACS_PHOTO_L2","PACS_SPECTRO_L2","PACS_SPECTRO_L25"] 
#~ traitement = ["PACS_SPECTRO_L1","PACS_SPECTRO_L2"]
#~ traitement = ["PACS_SPECTRO_L25"]
#done v15 traitement = ["PACS_SPECTRO_L2"]
# done v15 traitement = ["SPIRE_FTS_L1","SPIRE_FTS_L2"]
# done v15 traitement = ["SPIRE_PHOTO_L1","SPIRE_PHOTO_L2"]
traitement = ["SPIREPACS_PHOTO_L25"]
#~ traitement = ["SPIRE_PHOTO_DENSITY"]
#~ traitement = ["PACS_PHOTO_PSF"]
#~ traitement = ["SPIRE_CATALOG"]
# done v15 traitement = ["PACS_PHOTO_L2"]

##### connect to the database
try:
	conn = psycopg2.connect("dbname="+baseName+" user='sitools'")
	#conn = psycopg2.connect("dbname='HID_spectro' user='sitools'")
except:
	print "I am unable to connect to the database"
cur = conn.cursor()


if "SPIRE_PHOTO_L1" in traitement :
	
	listTable=["spire_photo_l1"]
	if drop:
		cur=drop_tables(listTable,cur)
		command='''create table SPIRE_Photo_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,proposal varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar, elecSide varchar,biasMode varchar,timeOffset  real,timeDrift  real,invalidOffsetFlag varchar,adcErrFlag varchar,plwBiasAmpl real,pmwBiasAmpl real, pswBiasAmpl real,ptcBiasAmpl  real,rcRollApp varchar,obsMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,
posAngle varchar,BuildVersion varchar,CalibrationVersion varchar,scriptID varchar,fluxConversionDone varchar,temperatureDriftCorrectionDone varchar,electricalCrosstalkCorrectionDone varchar,opticalCrosstalkCorrectionDone varchar,filepath varchar,program varchar,filename varchar,X real,Y real,Z real,filesize_bytes real,filesize real,public varchar,release varchar,processing varchar,last varchar)'''
		cur.execute(command)

	for prefix in prefix_list_spire_photo:
		# Test Release existence and delete lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		conn.commit()
		# SPIRE PHOTO L1 Files (Sanepic)
		basePath_L1_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/L1_Sanepic_SPIRE/'
		listfile_L1_all=getListFiles(basePath_L1_SPIRE_Photo)
		print basePath_L1_SPIRE_Photo
		FillSPIREPhotoL1Table(listfile_L1_all,cur,prefix,last_dico_spire_photo[prefix])
		conn.commit()
	
if "SPIRE_PHOTO_L2" in traitement :
	
	listTable = ["spire_photo_l2","spire_photo_l2_obsids"]
	if drop:
		cur=drop_tables(listTable,cur)
		#create table for SPIRE Photo L2
		cur.execute('''create table SPIRE_Photo_L2 (L2_id integer PRIMARY KEY,object varchar,filepath varchar,observer varchar,proposal varchar,public varchar,program varchar,processing varchar,filename varchar,image varchar,fullsize_image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,wavelength real,description varchar,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,combine varchar,last varchar,spoly spoly,obsid integer,level25 varchar)''')
		cur.execute('''create table SPIRE_Photo_L2_obsids (L2_obsids_id integer,L2_id integer,obsid integer,L1_id integer,processing varchar,release varchar)''')
	
	for prefix in prefix_list_spire_photo:
		# Test Release existence and delete lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		conn.commit()
		# SPIRE PHOTO L2 HIPE
		basePath_L2_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_SPIRE/'
		listfile_L2_all=getListFiles(basePath_L2_SPIRE_Photo)
		FillSPIREPhotoL2Table(listfile_L2_all,cur,prefix,last_dico_spire_photo[prefix])
		conn.commit()
		#updateToLevel25(listTable,listfile_L2_all,prefix,cur)
		#conn.commit()
		# SPIRE PHOTO L2 SANEPIC 
		basePath_L2_SPIRE_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/MAPS_SPIRE/'
		listfile_L2_SPIRE_Photo_Sanepic = getListFiles(basePath_L2_SPIRE_Photo_Sanepic)
		FillSanepicSPIREPhotoL2Table(listfile_L2_SPIRE_Photo_Sanepic,cur,prefix,last_dico_spire_photo[prefix])
		conn.commit()

if "PACS_PHOTO_L1" in traitement :
	
	listTable = ["pacs_photo_l1"]
	if drop:
		cur=drop_tables(listTable,cur)
		
		#create table for PACS Photo L1
		#Red and blue
		cur.execute('''create table PACS_Photo_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,
		obsMode varchar,pointingMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,blue varchar,repfactor integer,
		camera varchar,filepath varchar,program varchar,processing varchar,filename varchar,X real,Y real,Z real,filesize_bytes real,filesize real,public varchar,release varchar,last varchar)''')
	# PACS PHOTO L1 Files
	for prefix in prefix_list_pacs_photo:
		deleteRelease(listTable,cur,prefix.capitalize())
		conn.commit()
		basePath_L1_PACS_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/L1_PACS/'
		listfile_L1_PACS=getListFiles(basePath_L1_PACS_Photo)
		FillPACSPhotoL1Table(listfile_L1_PACS,cur,"red",prefix,last_dico_pacs_photo[prefix])
		FillPACSPhotoL1Table(listfile_L1_PACS,cur,"blue",prefix,last_dico_pacs_photo[prefix])
		FillPACSPhotoL1Table(listfile_L1_PACS,cur,"green",prefix,last_dico_pacs_photo[prefix])
		conn.commit()
		basePath_L1_PACS_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/L1_Sanepic_PACS/'
		listfile_L1_PACS_Sanepic=getListFiles(basePath_L1_PACS_Photo_Sanepic)
		FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"red",prefix,last_dico_pacs_photo[prefix])
		FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"blue",prefix,last_dico_pacs_photo[prefix])
		FillSanepicPACSPhotoL1Table(listfile_L1_PACS_Sanepic,cur,"green",prefix,last_dico_pacs_photo[prefix])
		conn.commit()

if "PACS_PHOTO_L2" in traitement :
	
	listTable = ["pacs_photo_l2","pacs_photo_l2_obsids"]
	if drop:
		cur=drop_tables(listTable,cur)
		#cur=create_and_drop(listTable,cur,conn)
		#create table for PACS Photo L2
		cur.execute('''create table PACS_Photo_L2 (L2_id integer PRIMARY KEY,object varchar,filepath varchar,observer varchar,public varchar,program varchar,processing varchar,filename varchar,image varchar,fullsize_image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,wavelength real,description varchar,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last varchar,spoly spoly)''')
		cur.execute('''create table PACS_Photo_L2_obsids (L2_obsids_id integer,L2_id integer,obsid integer,L1_id integer,processing varchar,release varchar)''')

	# PACS PHOTO L2 Files
	for prefix in prefix_list_pacs_photo:
		deleteRelease(listTable,cur,prefix.capitalize())
		conn.commit()
		basePath_L2_PACS_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_PACS/'
		listfile_L2_all_Hipe=getListFiles(basePath_L2_PACS_Photo)
		basePath_L2_PACS_Photo_Scanamorphos = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SCANAMORPHOS_Fits/MAPS_PACS/'
		basePath_L2_PACS_Photo_Unimap = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/UNIMAP_Fits/MAPS_PACS/'
		#print "UNIMAP",basePath_L2_PACS_Photo_Unimap
		listfile_L2_all_Scanamorphos=getListFiles(basePath_L2_PACS_Photo_Scanamorphos)
		listfile_L2_all_Unimap=getListFiles(basePath_L2_PACS_Photo_Unimap)
		listfile_L2_all=listfile_L2_all_Hipe+listfile_L2_all_Scanamorphos+listfile_L2_all_Unimap
		print "listHIPE + SCANA + UNIMAP",listfile_L2_all
		FillPACSPhotoL2Table(listfile_L2_all,"red",cur,prefix,last_dico_pacs_photo[prefix])
		FillPACSPhotoL2Table(listfile_L2_all,"blue",cur,prefix,last_dico_pacs_photo[prefix])
		FillPACSPhotoL2Table(listfile_L2_all,"green",cur,prefix,last_dico_pacs_photo[prefix])
		conn.commit()
		# SPIRE PHOTO L2 SANEPIC L2 Files
		basePath_L2_PACS_Photo_Sanepic = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SANEPIC_Fits/MAPS_PACS/'
		listfile_L2_PACS_Photo_Sanepic = getListFiles(basePath_L2_PACS_Photo_Sanepic)
		print listfile_L2_PACS_Photo_Sanepic
		FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"red",prefix,last_dico_pacs_photo[prefix])
		FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"blue",prefix,last_dico_pacs_photo[prefix])
		FillSanepicPACSPhotoL2Table(listfile_L2_PACS_Photo_Sanepic,cur,"green",prefix,last_dico_pacs_photo[prefix])
		conn.commit()

if "SPIRE_FTS_L1" in traitement :
	
	listTable = ["spire_fts_l1"]
	if drop:
		cur=drop_tables(listTable,cur)
		# create table for SPIRE FTS L1
		cur.execute('''create table SPIRE_FTS_L1 (L1_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,timeOffset real,timeDrift real,biasFrequency real,obsMode varchar,aorLabel varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,posAngle varchar,numScans integer,commandedResolution varchar,mapSampling varchar,apodName varchar,BuildVersion varchar,CalibrationVersion varchar,filepath varchar,program varchar,origin varchar,filetype varchar,filename varchar,image varchar,X real,Y real,Z real,filesize real,public varchar,release varchar,last varchar)''')

	# SPIRE FTS L1 Files
	for prefix in prefix_list_spire_fts:
		# Test Release existence and delete matching lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		basePath_SPIRE_FTS = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/FTS_SPIRE/'
		motifs = [".fits"]
		exclure = ["_cube","extra","supreme"]
                listfile_FTS = getFilesMotif(basePath_SPIRE_FTS,motifs,exclure)
		FillSPIREFtsL1Table(listfile_FTS,cur,prefix,last_dico_spire_FTS[prefix])
		conn.commit()

if "SPIRE_FTS_L2" in traitement :
	
	listTable = ["spire_fts_l2"]
	if drop:
		cur=drop_tables(listTable,cur)
		# create table for SPIRE FTS L2
		cur.execute('''create table SPIRE_FTS_L2 (L2_id integer PRIMARY KEY,obsid integer,object varchar,observer varchar,cusmode varchar,odnumber integer,creationDate varchar,startDate varchar,endDate varchar,obsMode varchar,equinox varchar,raDeSys varchar, raNominal real,decNominal real,ra real,dec real,posAngle varchar,commandedResolution varchar,mapSampling varchar,apodName varchar,projection varchar, wavelenth varchar, crval1 real,crval2 real,cdelt1 real,cdelt2 real,naxis integer,naxis1 integer,naxis2 integer,naxis3 integer,crpix1 integer,crpix2 integer,ctype1 varchar,ctype2 varchar,BuildVersion varchar,CalibrationVersion varchar,filepath varchar,program varchar,origin varchar,filename varchar,image varchar,X real,Y real,Z real,filesize real,public varchar,release varchar,last varchar )''')

	# SPIRE FTS L2 Files
	for prefix in prefix_list_spire_fts:
		# Test Release existence and delete matching lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		basePath_SPIRE_FTS = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/FTS_SPIRE/'
		motifs = ["_cube"]
		exclure = ["extra","png"]
		listfile_FTS = getFilesMotif(basePath_SPIRE_FTS,motifs,exclure)
		for i in listfile_FTS: print i
		FillSPIREFtsL2Table(listfile_FTS,cur,prefix,last_dico_spire_FTS[prefix])
		conn.commit()
		#### from Release R7_spire_fts : directory SUPREME_Fits/FTS_SPIRE : before, supreme Fits in HIPE_Fits/FTS_SPIRE
		basePath_SPIRE_FTS = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/SUPREME_Fits/FTS_SPIRE/'
		motifs = ["_cube"]
		exclure = ["extra","png"]
		listfile_FTS = getFilesMotif(basePath_SPIRE_FTS,motifs,exclure)
		for i in listfile_FTS: print i
		FillSPIREFtsL2Table(listfile_FTS,cur,prefix,last_dico_spire_FTS[prefix])
		conn.commit()

if "PACS_SPECTRO_L1" in traitement :
	
	listTable = ["pacs_spectro_l1"]
	if drop:
		cur=drop_tables(listTable,cur)
		# create table for PACS Spectro L1
		cur.execute('''create table PACS_Spectro_L1 (L1_id integer PRIMARY KEY, object varchar, obsid varchar, observer varchar, camera varchar, ra real, dec real, x real, y real, z real, observingmode varchar, algorithm varchar, line varchar, rasterid varchar, isoffposition varchar, naxis1 integer, naxis2 integer, naxis3 integer, program varchar, product varchar, processing varchar, filename varchar, filepath varchar,filesize real,public varchar,release varchar,last varchar)''')

	# PACS SPECTRO L1 Files
	for prefix in prefix_list_pacs_spectro:
		# Test Release existence and delete matching lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		basePath_L1_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/SPECTRO_PACS/'
		motifs = [".fits","L1","RasterCube"]
		exclure = ["_flag","Frame","_ProjectedCube","PACSman","MapFlux"]
		listfile_L1_SPECTRO = getFilesMotif(basePath_L1_SPECTRO,motifs,exclure)
		FillPACSspectroL1Table(listfile_L1_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
		conn.commit()

if "PACS_SPECTRO_L2" in traitement :
	
	listTable = ["pacs_spectro_l2"]
	if drop:
		cur=drop_tables(listTable,cur)
		cur.execute('''create table PACS_Spectro_L2 (L2_id integer PRIMARY KEY, object varchar, obsid varchar, observer varchar, camera varchar, ra real, dec real, x real, y real, z real, observingmode varchar, algorithm varchar, line varchar, builversion varchar, naxis1 integer, naxis2 integer, naxis3 integer, program varchar, product varchar, processing varchar, filename varchar, image varchar, filepath varchar,filesize real,public varchar,release varchar,last varchar)''')
	
	for prefix in prefix_list_pacs_spectro:
		# Test Release existence and delete matching lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		basePath_L2_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/SPECTRO_PACS/'
		motifs = ["Cube.fits","L2"]
		exclure = ["MapFlux","PACSman","SAG-4/HD37041","RebinnedCube","RebinnedCubeAfterSub","RasterCube","RasterCubeBFF","Frame"]
		listfile_L2_SPECTRO = getFilesMotif(basePath_L2_SPECTRO,motifs,exclure)
		FillPACSspectroL2Table(listfile_L2_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
		conn.commit()

if "PACS_SPECTRO_L25" in traitement :
	
	listTable = ["pacs_spectro_l25"]
	if drop:
		cur=drop_tables(listTable,cur)
		cur.execute('''create table PACS_Spectro_L25 (L25_id integer PRIMARY KEY, object varchar, obsid varchar, ra real, dec real, x real, y real, z real, unit varchar, line varchar, builversion_HIPE varchar, version_PACSman varchar, naxis1 integer, naxis2 integer, program varchar, product varchar, descrip varchar, processing varchar, filename varchar, image varchar, filepath varchar,filesize real, public varchar, release varchar,last varchar)''')
	
	for prefix in prefix_list_pacs_spectro:
		# Test Release existence and delete matching lines if needed
		deleteRelease(listTable,cur,prefix.capitalize())
		basePath_L25_SPECTRO = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/SPECTRO_PACS/SAG-4/'
		motifs = [".fits","Flux"]
		exclure = ["RasterCubeCorrPACSman","mask","smooth","spectra"]
		listfile_L25_SPECTRO = getFilesMotif(basePath_L25_SPECTRO,motifs,exclure)
		FillPACSspectroL25Table(listfile_L25_SPECTRO,cur,prefix,last_dico_pacs_spectro[prefix])
		conn.commit()

if "SPIREPACS_PHOTO_L25" in traitement :
	releaseSPIRE="R8_spire_photo"
	releasePACS="R4_pacs_photo"
	CreateSPIREPACSL25View(cur,releaseSPIRE,releasePACS)
	conn.commit()

if "SPIRE_PHOTO_DENSITY" in traitement :

    listTable = ["spire_photo_density"]
    if drop:
        cur=drop_tables(listTable,cur)
        #create table for SPIRE Photo L2
        cur.execute('''create table SPIRE_Photo_Density (L2_id integer PRIMARY KEY,obsid integer,object varchar,filepath varchar,observer varchar,proposal varchar,program varchar,filename varchar,image varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,description varchar,maxdens real,maxsig real,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last varchar)''')

    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete lines if needed
        deleteRelease(listTable,cur,prefix.capitalize())
        conn.commit()
        # SPIRE PHOTO L2 HIPE 
        basePath_L2_SPIRE_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/MAPS_SPIRE/'
        listfile_L2_all=getListFiles(basePath_L2_SPIRE_Photo)
        motifs = ["colorsel"]
        exclure = []
        listfile_L2_all = getFilesMotif(basePath_L2_SPIRE_Photo,motifs,exclure)
        FillSPIREPhotoDensityTable(listfile_L2_all,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()

if "PACS_PHOTO_PSF" in traitement :

    listTable = ["pacs_photo_psf"]
    if drop:
        cur=drop_tables(listTable,cur)
        #create table for SPIRE Photo L2
        cur.execute('''create table PACS_Photo_PSF (PSF_id integer PRIMARY KEY,filepath varchar,creationDate varchar,filter integer,scanVelocity real,obsmod varchar,filename varchar,crval1 real,crval2 real,cdelt1 real,cdelt2 real,crota2 real,naxis1 integer,naxis2 integer,crpix1 real,crpix2 real,ctype1 varchar,ctype2 varchar,equinox varchar,BuildVersion varchar,X real,Y real,Z real,filesize_bytes real,filesize real,release varchar,last varchar)''')

    for prefix in prefix_list_pacs_photo_psf:
        # Test Release existence and delete lines if needed
        deleteRelease(listTable,cur,prefix)
        conn.commit()
        # SPIRE PHOTO L2 HIPE 
        basePath_PSF_PACS_Photo = '/data/glx-herschel/data1/herschel/Release/'+prefix+'/PACS/PHOTO/'
	print basePath_PSF_PACS_Photo
        motifs = [".fits"]
        exclure = []
        listfile_PSF_all = getFilesMotif(basePath_PSF_PACS_Photo,motifs,exclure)
	print listfile_PSF_all
        FillPACSPhotoPSFTable(listfile_PSF_all,cur,prefix,last_dico_pacs_photo_psf[prefix])
        conn.commit()

if "SPIRE_CATALOG" in traitement :

    listTable = ["spire_catalog"]
    if drop:
        cur=drop_tables(listTable,cur)
        cur.execute('''create table SPIRE_Catalog (source_id integer PRIMARY KEY, ra real,dec real,x real,y real,raPlusErr real,decPlusErr real,raMinusErr real,decMinusErr real,xPlusErr real,yPlusErr real,xMinusErr real,yMinusErr real,flux real,fluxPlusErr real,fluxMinusErr real,background real,bgPlusErr real,bgMinusErr real,quality real,object varchar,proposal varchar,band varchar,obsid integer)''')

    for prefix in prefix_list_spire_photo:
        # Test Release existence and delete matching lines if needed
        #deleteRelease(listTable,cur,prefix.capitalize())
        basePath_SPIRE_CATALOG = '/data/glx-herschel/data1/herschel/Release/'+prefix.capitalize()+'/HIPE_Fits/CATALOG_SPIRE/'
        motifs = ["catalog","mask","beam",".fits"]
        exclure = []
        listfile_SPIRE_CATALOG = getFilesMotif(basePath_SPIRE_CATALOG,motifs,exclure)
        FillSPIRECatalogTable(listfile_SPIRE_CATALOG,cur,prefix,last_dico_spire_photo[prefix])
        conn.commit()

cur.close()
conn.close()
