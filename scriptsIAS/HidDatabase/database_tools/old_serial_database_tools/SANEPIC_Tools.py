import psycopg2
import pyfits
import glob
import os, string, shutil
import time
from datetime import date
import math, numpy

old_serial_database_tools = "/data/glx-herschel/data1/herschel/scriptsProduction/herschel_pipelines/scriptsIAS/HidDatabase/database_tools/old_serial_database_tools/"

execfile(old_serial_database_tools+"Database_Tools.py")

def getSourceFile(basePath,source):
        listfile_all=[]
        for root, dirs, files in os.walk(basePath):
		listfile = [root+'/'+filename for filename in files if (filename == source)]
		listfile_all.extend(listfile)
        return listfile_all


def isSourcePublic(fileMotif,source):
	
	if fileMotif.count("MAPS_PACS")>0:
		basePath1 = fileMotif.split("/MAPS_PACS/")[0]+"/L1_Sanepic_PACS/"
		temp = fileMotif.split("/MAPS_PACS/")[1].split("/")[0]+"/"
		basePath1 = basePath1 + temp
	if fileMotif.count("MAPS_SPIRE")>0:
		basePath1 = fileMotif.split("/MAPS_SPIRE/")[0]+"/L1_Sanepic_SPIRE/"
		temp = fileMotif.split("/MAPS_SPIRE/")[1].split("/")[0]+"/"
		basePath1 = basePath1 + temp
			
	for i in source:
		#print i
		sourceFile = getSourceFile(basePath1,i)
		#print sourceFile
		fits=pyfits.open(sourceFile[0])
		dateObs = fits[0].header['DATE-OBS']
		if isPublic(dateObs)==False:
			return False
	
	fits.close()
	
	return True

def getObsIDsSource(fileMotif,source):
	
	obsIDs = []
	if fileMotif.count("MAPS_PACS")>0:
		basePath2 = fileMotif.split("/MAPS_PACS/")[0]+"/L1_Sanepic_PACS/"
		temp = fileMotif.split("/MAPS_PACS/")[1].split("/")[0]+"/"
		basePath2 = basePath2 + temp
	if fileMotif.count("MAPS_SPIRE")>0:
		basePath2 = fileMotif.split("/MAPS_SPIRE/")[0]+"/L1_Sanepic_SPIRE/"
		temp = fileMotif.split("/MAPS_SPIRE/")[1].split("/")[0]+"/"
		basePath2 = basePath2 + temp
		
	for i in source:
		#print i
		sourceFile = getSourceFile(basePath2,i)
		#print sourceFile
		fits=pyfits.open(sourceFile[0])
		obsIDs.append(fits[0].header['OBS_ID'])
	
	fits.close()
		
	return obsIDs


def getAllMetadataPacsSanepic(fitsFile):
        METADATA = {}

        filepathComplete=str(fitsFile)
        filename=os.path.basename(filepathComplete)
        dirpath=os.path.dirname(filepathComplete)
        hdulist=pyfits.open(filepathComplete)
        filepath="/"+prefix.capitalize()+filepathComplete.split(prefix.capitalize())[1]

        #HDR0
        hdr0=hdulist[0].header
        METADATA["PUBLIC"] = isPublic(hdr0['DATE-OBS'])
        #print filepath, METADATA["PUBLIC"]
        cards = hdr0.cards

        DICO = makeDicoPacsSpectro(hdr0)
        #print sorted(DICO.iteritems())

        for card in cards:
                #print card
                chaine_card=str(card)
                if chaine_card.count("META")==0 and chaine_card.count("HIERARCH")==0 and chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")==1:
                        #print chaine_card
                        cle = chaine_card.split("=")[0]
                        valeur = chaine_card.split("=")[1].split("/")[0]
                        cle = cle.replace(" ","").strip("_")
                        if valeur.count("'")==0:
                                valeur = valeur.replace(" ","").strip(" ")

                        if valeur.count("'")>0:
                                valeur = valeur.replace("'","").strip(" ")

                        METADATA["0_"+cle] = valeur


                if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==0 and chaine_card.count("=")>=1:
                        #print chaine_card
                        if chaine_card.count("\n"):
                                chaine_card = chaine_card.split("\n")[0]
                                chaine_card = chaine_card.replace("&","")

                        cle = chaine_card.split("=")[0]
                        valeur = chaine_card.split("=")[1].split("/")[0]
                        cle = cle.replace(" ","").strip("_")
                        if valeur.count("'")==0:
                                valeur = valeur.replace(" ","").strip(" ")

                        if valeur.count("'")>0:
                                valeur = valeur.replace("'","").strip(" ")

                        try:
                                METADATA["0_"+DICO[cle].replace("'","").upper()] = valeur

                        except:
                                METADATA["0_"+DICO[cle].replace("'","").upper()] = "PROBLEM"

        #HDR1
        hdr1=hdulist[1].header
        cards = hdr1.cards

        DICO = makeDicoPacsSpectro(hdr1)
        #print sorted(DICO.iteritems())

        for card in cards:
                chaine_card=str(card)
                if chaine_card.count("META")==0 and chaine_card.count("HIERARCH")==0 and chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")>=1:
                        #print chaine_card
                        cle = chaine_card.split("=")[0]
                        valeur = chaine_card.split("=")[1].split("/")[0]
                        #print cle, hdr1[str(cle)]
                        cle = cle.replace(" ","").strip("_")
                        if valeur.count("'")==0:
                                valeur = valeur.replace(" ","").strip(" ")

                        if valeur.count("'")>0:
                                valeur = valeur.replace("'","").strip(" ")

                        METADATA["1_"+cle] = valeur

                if chaine_card.count("META")==1 and chaine_card.count("HIERARCH")==0 and chaine_card.count("=")>=1:
                        #print chaine_card
                        if chaine_card.count("\n"):
                                chaine_card = chaine_card.split("\n")[0]
                                chaine_card = chaine_card.replace("&","")

                        cle = chaine_card.split("=")[0]
                        valeur = chaine_card.split("=")[1].split("/")[0]
                        cle = cle.replace(" ","").strip("_")
                        if valeur.count("'")==0:
                                valeur = valeur.replace(" ","").strip(" ")

                        if valeur.count("'")>0:
                                valeur = valeur.replace("'","").strip(" ")

                        try:
                                METADATA["1_"+DICO[cle].replace("'","").upper()] = valeur

                        except:
                                METADATA["1_"+DICO[cle].replace("'","").upper()] = "PROBLEM"


        if fitsFile.count("Cube")==1:
                METADATA["BUILDVERSION"] = hdulist[6].data[0][3]


        try:
                METADATA["DURATION"]=hdr0['EPOCH']
        except:
		try:
                	METADATA["DURATION"]=hdr0['EQUINOX']
		except:
			METADATA["DURATION"]=None
        try:
                METADATA["OBSERVER"]=hdr0['OBSERVER']
        except:
                METADATA["OBSERVER"]=None
        try:
                METADATA["BuildVersion"]=hdr0['BuildVersion']
        except:
                METADATA["BuildVersion"]=None

        try:
                METADATA["0_POINTMOD"]=hdr0['POINTMOD']
        except:
                METADATA["0_POINTMOD"]=None
        try:
                METADATA["0_REPFACTOR"]=hdr0['REPFACTOR']
        except:
                METADATA["0_REPFACTOR"]=None
	try:
        	xyz = RADECtoXYZ(float(METADATA['0_RA']),float(METADATA['0_DEC']))
	except:
		xyz = RADECtoXYZ(float(METADATA['0_RA_NOM']),float(METADATA['0_DEC_NOM']))
		METADATA['0_RA']=METADATA['0_RA_NOM']
		METADATA['0_DEC']=METADATA['0_DEC_NOM']
        try:
                METADATA["WAVELNTH"]=hdr0['RESTWAV']
        except:
                METADATA["WAVELNTH"]=None

        METADATA['XYZ_X']= xyz[0]
        METADATA['XYZ_Y']= xyz[1]
        METADATA['XYZ_Z']= xyz[2]

        METADATA["FILENAME"] = filename
        METADATA["FILEPATH"] = urlFilepath(filepath)
        METADATA["IMAGENAME"] = filepath[:len(filepath)-5]+".png"
        METADATA["FULLIMAGENAME"] = filepath[:len(filepath)-5]+"_fullsize.png"
        METADATA["FILESIZE_bytes"] = float(os.path.getsize(filepathComplete))
        METADATA["FILESIZE"] = convert_MegaBytes(os.path.getsize(filepathComplete))
        METADATA["PROGRAM"] = filepathComplete.split("/")[-3]
        METADATA["PROCESSING"] = "SANEPIC"
        METADATA["PROCESSING_L1"] = "HIPE_Sanepic"
        METADATA["DESCRIPTION"] = "SANEPIC"
        METADATA["BuildVersion"] =  None


        hdulist.close()

        return METADATA




def getAllMetadataSANEPIC(fitsFile):
	
	METADATA = {}
	
	filepathComplete=str(fitsFile)
	filename=os.path.basename(filepathComplete)
	dirpath=os.path.dirname(filepathComplete)
	hdulist=pyfits.open(filepathComplete)
	filepath="/"+prefix.capitalize()+filepathComplete.split(prefix.capitalize())[1]
        print filepath
	#HDR0
	hdr0=hdulist[0].header
	
	cards = hdr0.cards
	
	for card in cards:
		#print card
		chaine_card=str(card)
		if chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")==1:
			#print chaine_card
			cle = chaine_card.split("=")[0]
			valeur = chaine_card.split("=")[1].split("/")[0]
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
				
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
				
			METADATA["0_"+cle] = valeur
	
	#HDR1
	hdr1=hdulist[1].header
	cards = hdr1.cards

	for card in cards:
		chaine_card=str(card)
		if chaine_card.count("CONTINUE")==0 and chaine_card.count("COMMENT")==0 and chaine_card.count("=")>=1:
			#print chaine_card
			cle = chaine_card.split("=")[0]
			valeur = chaine_card.split("=")[1].split("/")[0]
			#print cle, hdr1[str(cle)]
			cle = cle.replace(" ","").strip("_")
			if valeur.count("'")==0:
				valeur = valeur.replace(" ","").strip(" ")
				
			if valeur.count("'")>0:
				valeur = valeur.replace("'","").strip(" ")
				
			METADATA["1_"+cle] = valeur

	#try: 
		#METADATA["DURATION"]=hdr0['EPOCH']
	#except:
		#METADATA["DURATION"]=hdr0['EQUINOX']	
			
	METADATA["FILENAME"] = filename
	METADATA["FILEPATH"] = urlFilepath(filepath)
	METADATA["IMAGENAME"] = filepath[:len(filepath)-5]+".png"
	METADATA["FULLIMAGENAME"] = filepath[:len(filepath)-5]+"_fullsize.png"
	METADATA["FILESIZE_bytes"] = float(os.path.getsize(filepathComplete))
	METADATA["FILESIZE"] = convert_MegaBytes(os.path.getsize(filepathComplete))
	METADATA["PROGRAM"] = filepathComplete.split("/")[-3]
	METADATA["PROCESSING"] = "SANEPIC" 
	METADATA["PROCESSING_L1"] = "HIPE_Sanepic" 
	METADATA["DESCRIPTION"] = "SANEPIC" 
	METADATA["BuildVersion"] =  None
	
	print "\n",METADATA["FILEPATH"], METADATA["PROGRAM"] 
        try:
                METADATA["WAVELNTH"]=hdr0['RESTWAV']
        except:
                METADATA["WAVELNTH"]=None

	try:
                METADATA["PROPOSAL"]=hdr0['PROPOSAL']
        except:
                METADATA["PROPOSAL"]=None
	
	#for k in sorted(METADATA.keys()):
		#print k, METADATA[k]
	
	try:
		METADATA['0_RA'] = METADATA['0_CRVAL1']
		METADATA['0_DEC'] = METADATA['0_CRVAL2']
	except:
		METADATA['0_RA'] = METADATA['1_CRVAL1']
		METADATA['0_DEC'] = METADATA['1_CRVAL2']
		
	xyz = RADECtoXYZ(float(METADATA['0_RA']),float(METADATA['0_DEC']))
	METADATA['XYZ_X']= xyz[0]
	METADATA['XYZ_Y']= xyz[1]
	METADATA['XYZ_Z']= xyz[2]
	try:
                METADATA['1_CROTA2'] = METADATA['1_CROTA2']
        except:
                METADATA['1_CROTA2'] = None 
	try:
		key = hdulist["History"].data.field(0)
		values = hdulist["History"].data.field(1)
		sourceFiles = values[numpy.where(key == "SOURCES")[0]]
		#print "!!!!!!!!!!!!!!!! Source files in HISTORY !!!!!!!!!!!!!!!!"
	except:
		sourceFiles = hdulist["InputFiles"].data.field(0)
		#print "!!!!!!!!!!!!!!!! Source files in INPUTFILES !!!!!!!!!!!!!!!!"
	

	hdulist.close()
	
	#print "Source Files"
	#print sourceFiles
	
	#METADATA["PUBLIC"] = isSourcePublic(filepathComplete,sourceFiles)
	#METADATA["OBS_IDs"] = getObsIDsSource(filepathComplete,sourceFiles)
	
	try:
		METADATA["PUBLIC"] = isSourcePublic(filepathComplete,sourceFiles)
		METADATA["OBS_IDs"] = getObsIDsSource(filepathComplete,sourceFiles)
		
	except:
		METADATA["PUBLIC"] = "ERRRRRRRRRROOOOOOOOOORRRRRRRRRRRR"
		METADATA["OBS_IDs"] = [35505]
	
	print METADATA["PUBLIC"], METADATA["OBS_IDs"]
	
	return METADATA
	
def FillSanepicSPIREPhotoL2Table(listfile,cur,prefix,last):
	metadata_L2=[]
	L2_id=0
	L2_obsids_id=0
	for i in listfile:
		META = getAllMetadataSANEPIC(i)
		#print META['FILENAME']
		#STEP 1: insert into L2
		#check if file is already in the database
		cur.execute("SELECT * FROM SPIRE_Photo_L2 WHERE filename like '"+META['FILENAME']+"' and release like '"+prefix+"'" )
		#if not
		if cur.fetchone()==None:
				###### STEP 1 insert into L2
				# for Sanepic, combine = True
				combine=True
				cur.execute("select count (*) from SPIRE_PHOTO_L2")
                                poly=calculateSpoly(str(i))
                                obsid=999999 #Non available for Sanepic Maps
                                level25=True
				NbLines= cur.fetchone()
				if NbLines!=None:
					#L2_id=NbLines[0]
                                         cur.execute("select max(l2_id) from SPIRE_Photo_L2")
                                         MaxL2=cur.fetchone()
					 if MaxL2[0]!=None:
                                        	 L2_id=MaxL2[0]+1
				metadata_L2=[L2_id,META['1_OBJECT'],META['FILEPATH'],META['1_OBSERVER'],META['PROPOSAL'],META['PUBLIC'],\
				META["PROGRAM"],META["PROCESSING"], META['FILENAME'],META['IMAGENAME'],META['FULLIMAGENAME'],\
				META['1_CRVAL1'],META['1_CRVAL2'],META['1_CDELT1'],META['1_CDELT2'],META['1_CROTA2'],META['1_NAXIS1'],META['1_NAXIS2'],\
				META['1_CRPIX1'],META['1_CRPIX2'],META['1_CTYPE1'],META['1_CTYPE2'],META['1_EQUINOX'],\
				META["1_WAVELNTH"].replace("D","E"),META["DESCRIPTION"],META["BuildVersion"],META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],META["FILESIZE_bytes"],META["FILESIZE"],prefix,combine,last,poly,obsid,level25]
				cur.execute("insert into SPIRE_PHOTO_L2 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" , metadata_L2)
				print META['FILENAME'], " in the database."
				# STEP 2: insert into L2_obsids
				processing=META["PROCESSING"]
				processing_L1=META["PROCESSING_L1"]
				for j in META['OBS_IDs']:
					cur.execute("select L1_id from SPIRE_Photo_L1 where obsid="+str(j)+" and processing like '"+processing_L1+"'"+" and release like '"+prefix+"'" +" and filename like '%notdf%'")
                                        L1_id=cur.fetchone()
                                        print "SPIRE sanepic obsid,L1_id",j,L1_id
					cur.execute("select count (*) from SPIRE_Photo_L2_obsids")
                                        NbLines_L2_obsids=cur.fetchone()
                                        if NbLines_L2_obsids!=None:
                                                        #L2_obsids_id=NbLines_L2_obsids[0]
                                                        cur.execute("select max(l2_obsids_id) from SPIRE_Photo_L2_obsids")
                                                        MaxL2obsids=cur.fetchone()
							if MaxL2obsids[0]!=None:
                                                        	L2_obsids_id=MaxL2obsids[0]+1
                                        metadata_L2_obsids=(L2_obsids_id,L2_id,j,L1_id,processing_L1,prefix)
					cur.execute("insert into SPIRE_PHOTO_L2_obsids values (%s,%s,%s,%s,%s,%s)",metadata_L2_obsids)

def FillSanepicPACSPhotoL2Table(listfile,cur,blue,prefix,last):
	metadata_L2=[]
	L2_id=0
	L2_obsids_id=0
	for i in listfile:
	 if str(i).count(blue)==1:
		#META=getAllMetadataPacsSanepic(i)
		META = getAllMetadataSANEPIC(i)
		#print META['FILENAME']
		#STEP 1: insert into L2
		#check if file is already in the database
		cur.execute("SELECT * FROM PACS_PHOTO_L2 WHERE filename like '"+META['FILENAME']+"' and release like '"+prefix+"'" )
		#if not
		if cur.fetchone()==None:
				###### STEP 1 insert into L2
				cur.execute("select count (*) from PACS_PHOTO_L2")
				NbLines= cur.fetchone()
				if NbLines!=None:
					cur.execute("select max(l2_id) from PACS_Photo_L2")
                                        MaxL2=cur.fetchone()
					if MaxL2[0]!=None:
                                        	L2_id=MaxL2[0]+1
					#L2_id=NbLines[0]
                                poly=calculateSpoly(str(i))
				NbLines= cur.fetchone()
				metadata_L2=[L2_id,META['1_OBJECT'],META['FILEPATH'],META['0_OBSERVER'],META['PUBLIC'],\
				META["PROGRAM"],META["PROCESSING"], META['FILENAME'],META['IMAGENAME'],META['FULLIMAGENAME'],\
				META['1_CRVAL1'],META['1_CRVAL2'],META['1_CDELT1'],META['1_CDELT2'],META['1_CROTA2'],META['1_NAXIS1'],META['1_NAXIS2'],\
				META['1_CRPIX1'],META['1_CRPIX2'],META['1_CTYPE1'],META['1_CTYPE2'],META['1_EQUINOX'],\
				META["WAVELNTH"],META["DESCRIPTION"],META["BuildVersion"],META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],META["FILESIZE_bytes"],META["FILESIZE"],prefix,last,poly]
				cur.execute("insert into PACS_PHOTO_L2 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" , metadata_L2)
				print META['FILENAME'], " in the database."
				# STEP 2: insert into L2_obsids
				processing=META["PROCESSING"]
				processing_L1=META["PROCESSING_L1"]
				for j in META['OBS_IDs']:
					cur.execute("select L1_id from PACS_Photo_L1 where obsid="+str(j)+" and camera like '"+blue+"'"+" and processing like '"+processing_L1+"'"+" and release like '"+prefix+"'")
                                        L1_id=cur.fetchone()
					cur.execute("select count (*) from PACS_Photo_L2_obsids")
                                        NbLines_L2_obsids=cur.fetchone()
                                        if NbLines_L2_obsids!=None:
                                              #L2_obsids_id=NbLines_L2_obsids[0]
						cur.execute("select max(l2_obsids_id) from PACS_Photo_L2_obsids")
                                                MaxL2obsids=cur.fetchone()
						if MaxL2obsids[0]!=None:
                                                	L2_obsids_id=MaxL2obsids[0]+1
                                        print "PACS sanepic obsid,L1_id,blue",j,L1_id, blue
                                        metadata_L2_obsids=(L2_obsids_id,L2_id,j,L1_id,processing,prefix)
                                        #print metadata_L2_obsids
                                        cur.execute("insert into PACS_Photo_L2_obsids values (%s,%s,%s,%s,%s,%s)",metadata_L2_obsids)

def FillSanepicPACSPhotoL1Table(listfile,cur,blue,prefix,last):

	L1_id=0
	metadata_L1=[]
	for i in listfile:
	 if  str(i).count(blue)==1   and   str(i).count("test")==0 and str(i).count("old")==0:
	 #if  str(i).count(blue)==1    and str(i).count("test")==0 and str(i).count("old")==0:
		print str(i)
		META = getAllMetadataPacsSanepic(i)
		#print META['FILENAME']
		#STEP 1: insert into L2
		#check if file is already in the database
		cur.execute("SELECT * FROM PACS_PHOTO_L1 WHERE filename = '%s'" % META['FILENAME'])
		#if not
		if cur.fetchone()==None:
				###### STEP 1 insert into L1
				cur.execute("select count (*) from PACS_PHOTO_L1")
				NbLines= cur.fetchone()
				if NbLines!=None:
                                	cur.execute("select max(l1_id) from PACS_Photo_L1")
                               		MaxL1=cur.fetchone()
					if MaxL1[0]!=None:
                                		L1_id=MaxL1[0]+1
				metadata_L1=[L1_id,META['0_OBS_ID'],META['0_OBJECT'],META['0_OBSERVER'],META['0_CUSMODE'],META['0_ODNUMBER'],\
				META['0_DATE'],META['0_DATE-OBS'],META['0_DATE-END'],META['0_OBS_MODE'],META['0_POINTMOD'],META['0_AOR'],META['0_EQUINOX'],\
				META['0_RADESYS'],META['0_RA_NOM'],META['0_DEC_NOM'],META['0_RA'],META['0_DEC'],\
				META["0_BLUE"],META["0_REPFACTOR"],blue,META['FILEPATH'],META["PROGRAM"],META["PROCESSING_L1"], META['FILENAME'],META['XYZ_X'],META['XYZ_Y'],META['XYZ_Z'],META["FILESIZE_bytes"],META["FILESIZE"],META['PUBLIC'],prefix,last]
				for index,meta in enumerate(metadata_L1):
					print meta
					if metadata_L1[index]=="None":
						metadata_L1[index]=None	
				cur.execute("insert into PACS_PHOTO_L1 values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" , metadata_L1)
				print META['FILENAME'], " in the database."
