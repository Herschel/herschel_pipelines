#!/usr/bin/env python

import pyfits
import os, sys, glob, string, shutil, getopt

#
# Moved from Observer to Proposal based classification
#
# # Define the correspondance between observer and output directories
# ObserverHash={}
# ObserverHash['soliver']   = 'SAG-1'
# ObserverHash['mviero']    = 'SAG-1'
# ObserverHash['gsmith01']  = 'SAG-1'

# ObserverHash['aabergel']  = 'SAG-4'
# ObserverHash['pandre']    = 'SAG-3'
# ObserverHash['fmotte']    = 'SAG-3'

# ObserverHash['lmontier']  = 'OT1_lmontier'
# ObserverHash['delbaz']    = 'GOODS-H'
# ObserverHash['baltieri']  = 'GT1_baltieri'
# ObserverHash['seales01']  = 'H-ATLAS'
# ObserverHash['mmiville']  = 'OT1_mmiville'
# ObserverHash['pvspire']   = 'H-ATLAS'
# ObserverHash['pguillar']  = 'HCG92_pguillard'
# ObserverHash['lho']       = 'OT1_lho'
# ObserverHash['hdole']     = 'OT2_hdole'
# ObserverHash['mustdo']    = 'DDT_mustdo'

# ObserverHash['atielens']    = 'OT1_atielens'
# ObserverHash['cwilso01']    = 'OT1_cwilso01'

# #ObserverHash['cwilso01']  = 'MapMaker_Test'
# ObserverHash['smolinar']  = 'MapMaker_Test'
# ObserverHash['zbalog']    = 'MapMaker_Test'
# ObserverHash['rpaladin']  = 'MapMaker_Test'
# ObserverHash['esturm']    = 'MapMaker_Test'  
# ObserverHash['mgroen01']  = 'MapMaker_Test'
# ObserverHash['mhuang01']  = 'MapMaker_Test'
# ObserverHash['mjuvela']   = 'MapMaker_Test'
# ObserverHash['okrause']   = 'MapMaker_Test'
# ObserverHash['rkennicu']  = 'MapMaker_Test'
# ObserverHash['tmegeath']  = 'MapMaker_Test'

# ObserverHash['molinar']  = 'Hi-Gal'

#/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE
#/data/glx-herschel/data1/herschel/SANEPIC_Fits/L1_Sanepic_SPIRE
ProposalHash={}
ProposalHash['GT2_mviero_1']     = 'SAG-1'
ProposalHash['KPGT_soliver_1']   = 'SAG-1'
ProposalHash['SDP_soliver_3']    = 'SAG-1'
ProposalHash['KPOT_gsmith01_1']  = 'SAG-1'

ProposalHash['KPGT_aabergel_1']   = 'SAG-4'
ProposalHash['SDP_aabergel_3']    = 'SAG-4'

ProposalHash['KPGT_fmotte_1']     = 'SAG-3'
ProposalHash['KPGT_pandre_1']     = 'SAG-3'
ProposalHash['GT2_pandre_5']     = 'SAG-3'
ProposalHash['SDP_fmotte_3']      = 'SAG-3'
ProposalHash['SDP_pandre_3']      = 'SAG-3'
ProposalHash['aquila_M2_00']      = 'SAG-3'

ProposalHash['KPOT_seales01_2']   = 'H-ATLAS'
ProposalHash['SDP_seales01_6']    =  'H-ATLAS'
ProposalHash['Calibration_pvspire_57'] = 'H-ATLAS'
ProposalHash['OT1_rivison_1']          = 'H-ATLAS'
ProposalHash['OT2_jwardlow_1']         = 'H-ATLAS'


ProposalHash['OT1_lmontier_1']    = 'OT1_lmontier'
ProposalHash['KPOT_delbaz_1']     = 'GOODS-H'
ProposalHash['GT1_baltieri_3']    = 'GT1_baltieri'
ProposalHash['OT1_mmiville_2']    = 'OT1_mmiville'
ProposalHash['OT1_pguillar_1']    = 'HCG92_pguillard'
ProposalHash['OT1_lho_1']         = 'OT1_lho'
ProposalHash['OT2_hdole_1']       = 'OT2_hdole'

ProposalHash['KPGT_cwilso01_1']   = 'KPGT_cwilso01'

ProposalHash['SDP_cwilso01_3']    = 'MapMaker_Test'
ProposalHash['SDP_smolinar_3']    = 'MapMaker_Test'
ProposalHash['GT2_zbalog_2']      = 'MapMaker_Test'
# ProposalHash['rpaladin']  = 'MapMaker_Test'
ProposalHash['KPGT_esturm_1']     = 'MapMaker_Test'  
ProposalHash['KPGT_mgroen01_1']   = 'MapMaker_Test'
ProposalHash['OT1_mhuang01_1']    = 'MapMaker_Test'
ProposalHash['KPOT_mjuvela_1']    = 'MapMaker_Test'
ProposalHash['GT1_okrause_4']     = 'MapMaker_Test'
ProposalHash['KPOT_rkennicu_1']   = 'MapMaker_Test'
ProposalHash['KPOT_tmegeath_2']   = 'MapMaker_Test'

#ProposalHash['KPOT_smolinar_1']   = 'MapMaker_Test'
ProposalHash['KPOT_smolinar_1']   = 'Hi-Gal'

ProposalHash['GT1_jfritz_1']      = 'M31'
ProposalHash['GT1_okrause_4']     = 'M31'

ProposalHash['DDT_mustdo_4']      = 'DDT_mustdo_4'
ProposalHash['KPOT_aedge_1']      = 'DDT_mustdo_4'
ProposalHash['DDT_mustdo_5']      = 'DDT_mustdo_5'
ProposalHash['OT1_pogle01_1']      = 'OT1_pogle01_1'


ProposalHash['GT1_dlutz_4']      = 'lens_malhotra'
#ProposalHash['KPOT_eegami_1']      = 'lens_malhotra'
ProposalHash['OT1_gstacey_3']      = 'lens_malhotra'
ProposalHash['OT2_smalhotr_3']      = 'lens_malhotra'
#ProposalHash['OT2_eegami_6']      = 'lens_malhotra'
ProposalHash['Calibration_rpspire_12']      = 'SAG-4'
ProposalHash['Calibration_rpspire_128']      = 'SAG-4'
ProposalHash['Calibration_rpspire_49']      = 'SAG-4'
ProposalHash['Calibration_rpspire_76']      = 'SAG-4'
ProposalHash['Calibration_rpspire_102']      = 'SAG-4'
ProposalHash['OT1_atielens_1']      = 'OT1_atielens'

####### Warning common data bw lens_lahotra and clusters-lowz
ProposalHash['OT2_eegami_6']      = 'clusters_lowz'
ProposalHash['OT1_eegami_4']      = 'clusters_lowz'
ProposalHash['KPOT_eegami_1']      = 'clusters_lowz'


# Define group of sources and corresponding output directories
SourceHash={}
SourceHash['XMM-LSSNorth']  = 'XMM-LSS'
SourceHash['XMM-LSSSouth']  = 'XMM-LSS'
SourceHash['XMM-LSS-Centre'] = 'XMM-LSS'
SourceHash['XMM-LSSEast']   = 'XMM-LSS'
SourceHash['XMM-LSSWest']   = 'XMM-LSS'


SourceHash['GOODS-S']    = 'ECDFS'
SourceHash['CDFS-SWIRE'] = 'ECDFS'

SourceHash['Lockman-SWIRE-offset1-1']   = 'Lockman-SWIRE'
SourceHash['Lockman-SWIRE-offset2-1-1'] = 'Lockman-SWIRE'

SourceHash['polaris-1'] = 'polaris'
SourceHash['polaris-2'] = 'polaris'
SourceHash['Main2']   = 'Aquila'
SourceHash['L1521_a']   = 'L1521'
SourceHash['L1521_b']   = 'L1521'

SourceHash['ELAIS-N1-SCUBAoff103n-1-1'] = 'ELAIS-N1-SCUBA'
SourceHash['ELAIS-N1-SCUBAoff52n-1-1']  = 'ELAIS-N1-SCUBA'
SourceHash['ELAIS-N1-SCUBAoff77o-1-1']  = 'ELAIS-N1-SCUBA'

SourceHash['Bootes-Spitzer-right-1-1'] = 'Bootes-Spitzer'
SourceHash['Bootes-Spitzer-left-1-1']  = 'Bootes-Spitzer'
SourceHash['Bootes-Spitzeroff77n-1-4'] = 'Bootes-Spitzer'
SourceHash['Bootes-Spitzer-down-1']    = 'Bootes-Spitzer'
SourceHash['Bootes-Spitzer-up-1']      = 'Bootes-Spitzer'

SourceHash['gama15_rn1']  = 'gama15'
SourceHash['gama15_rn2']  = 'gama15'
SourceHash['gama15_rn3']  = 'gama15'
SourceHash['gama15_rn4']  = 'gama15'
                          
SourceHash['gama_rn1']    = 'gama9'
SourceHash['gama_rn2']    = 'gama9'
SourceHash['gama_rn3']    = 'gama9'
SourceHash['gama_rn4']    = 'gama9'
SourceHash['ATLAS_SDn']   = 'gama9'
                          
SourceHash['GAMA12_rn1']  = 'gama12'
SourceHash['GAMA12_rn2']  = 'gama12'
SourceHash['GAMA12_rn3']  = 'gama12'
SourceHash['GAMA12_rn4']  = 'gama12'
SourceHash['GAMA12_rn3_patchup'] = 'gama12'

SourceHash['NGP-v1']  = 'NGP'
SourceHash['NGP-v2']  = 'NGP'
SourceHash['NGP-v3']  = 'NGP'
SourceHash['NGP-v4']  = 'NGP'
SourceHash['NGP-v5']  = 'NGP'
SourceHash['NGP-v6']  = 'NGP'
SourceHash['NGP-v7']  = 'NGP'
SourceHash['NGP-v8']  = 'NGP'
SourceHash['NGP_h1']  = 'NGP'
SourceHash['NGP_h2']  = 'NGP'
SourceHash['NGP_h3']  = 'NGP'
SourceHash['NGP_h4']  = 'NGP'
SourceHash['NGP_h5']  = 'NGP'
SourceHash['NGP_h6']  = 'NGP'
SourceHash['NGP_h7']  = 'NGP'
SourceHash['NGP_h8']  = 'NGP'

SourceHash['draco-1-1']   = 'draco'
SourceHash['Spider-1']    = 'spider'
                         
SourceHash['l2b-1-2']    = 'HELMS'
SourceHash['M2B-1-3']    = 'HELMS'
SourceHash['M2A-1-4']    = 'HELMS'
SourceHash['S1-3']       = 'HELMS'
SourceHash['S2-1-1']     = 'HELMS'
SourceHash['M1B-1-2']    = 'HELMS'
SourceHash['L1B-1-1']    = 'HELMS'
SourceHash['L2A-1-4']    = 'HELMS'
SourceHash['L1A-1-2']    = 'HELMS'
SourceHash['M1A-1-5']    = 'HELMS'
SourceHash['L2B-1-2']    = 'HELMS'
SourceHash['L3B-1-2']    = 'HELMS'
SourceHash['L3A-1-2']    = 'HELMS'
                         
SourceHash['Abell1689']  = 'A1689'
SourceHash['A1689New']   = 'A1689'

SourceHash['Abell2219']  = 'A2219'


def usage():
    """
    print usage function
    """
    print "usage organize.py [-h] [--copy] [--debug] [--recursive] [--verbose] in [out]"
    print ""
    print "This program move or copy fits files based on some header values"
    print "" 
    print "positional arguments:"
    print " in      the starting directory to scan"
    print ""
    print "optional arguments:"
    print " out            output directory to put files (default [in]) "
    print " -h, --help      show this help message and exit"
    print " -c, --copy      copy file instead of moving"
    print " -d, --debug     only produce shell script instead of performing action"
    print " -r, --recursive recursively scan the directories"
    print " -v, --verbose   produce a more verbose output"
    print " -i, --image     only move/copy fits file containing an 'Image' extension"

def scanFits(filename):
    hdulist = pyfits.open(filename)
    
    proposal   = None
    objectName = None
    
    try:
        proposal = hdulist[0].header['PROPOSAL']
    except KeyError:
        proposal = None
        
    try:
        objectName = hdulist[0].header['OBJECT']
    except KeyError:
        objectName = None
            
    try: 
        if hdulist['Image']:
            image = True
    except KeyError:
        image = False

    hdulist.close()

    return {'proposal': proposal, 'objectName': objectName, 'image': image}


def scanFitsDirectories(basePath, verbose=False):
    """
    Take a basePath as argument and construct a Hash containing all fits file and corresponding header keys
    """
    fitsFileName = {}
    print "Scanning " + basePath + "..."

    for root, dirs, files in os.walk(basePath):
        if verbose:
            print "  Scanning " + root.replace(basePath,'')
    
        # retrieve the fits file only
        # TODO: Not sure if it would work with .fits.gz files
        fitsfiles = [filename for filename in files if (os.path.splitext(filename)[1] == '.fits')]

        for filename in fitsfiles:
            if verbose:
                print "    "+filename
            fitsFileName[os.path.join(root,filename)] = scanFits(os.path.join(root,filename))

    return fitsFileName

def scanFitsDirectory(basePath, verbose=False):
    """
    Take a basePath as argument and construct a Hash containing all fits file and corresponding header keys
    """
    fitsFileName = {}
    print "Scanning " + basePath + "..."

    fitsfiles = glob.glob(os.path.join(basePath,'*.fits*'))
    
    for filename in fitsfiles:
        if verbose:
            print "    "+filename
        fitsFileName[os.path.join(basePath,filename)] = scanFits(os.path.join(basePath,filename))
        
    return fitsFileName



def removeBad(fitsFileName, ProposalHash, imageOnly = False, verbose = False):
    """
    take a fitsFileName Hash and remove the bad entries
    """

    print "Removing bad entries..."

    for filename in sorted(fitsFileName):

        bad = False
        if fitsFileName[filename]['proposal'] == None or  fitsFileName[filename]['proposal'] not in ProposalHash.keys():
            if verbose:
                print filename + " has no or unknown proposal : " + fitsFileName[filename]['proposal']
            bad = True

        if fitsFileName[filename]['objectName'] == None:
            if verbose:
                print filename + " has no objectName"
            bad = True

        if imageOnly and ( not fitsFileName[filename]['image'] ):
            if verbose:
                print filename +" is not an fits file image"
            bad = True


        if bad:
            del fitsFileName[filename]

    return fitsFileName



def moveFitsFile(fitsFileName, ProposalHash, SourceHash, outPath, debug=False, copy=False, verbose=False):
    """
    move the fits files according to a common naming scheme
    """

    if copy:
        print "Copying files..."
    else:
        print "Moving files..."

    for filename, item in fitsFileName.iteritems():
        proposal   = item['proposal']
        objectName = item['objectName']
    
        #objectName = string.join( string.split(objectName), '_')
        objectName = objectName.replace(' ','').replace("+","plus") 

        # Check for multiple source name
        if objectName in SourceHash.keys():
            objectName = SourceHash[objectName]

        newdir = os.path.join(outPath,ProposalHash[proposal],objectName)

        # Test for destination directory, and creates it if necessary
        if not os.path.isdir(newdir):
            if not debug:
                os.makedirs(newdir)
            else:
                print "mkdir "+newdir

        # Remove any remaining space in the new filename
        newfilename = string.join(string.split(os.path.basename(filename)),'_')

        # Move the file if necessary (i.e. different name or newer)
        src  = filename
        dest = os.path.join(newdir,newfilename)
        if src != dest or os.path.getmtime(src) > os.path.getmtime(dest):
            if copy:
                if not debug:
                    shutil.copy(src, dest)
                else:
                    print "cp " + src + " " + dest
            else:
                if not debug:
                    shutil.move(src, dest)
                else:
                    print "mv " + src + " " + dest
        else:
            if verbose:
                print dest.replace(outPath,'') + " is already in place"



def removeEmptyDir(basePath):
    """
    Removing Empty Directories from basePath
    """
    print "Removing empty directories"
    for root, dirs, files in os.walk(basePath):
        if dirs == [] and files == []:
            os.rmdir(root)



# Main program : 
def main(argv=None):

    if argv is None:
        argv = sys.argv
    
    try:
        opts, args = getopt.getopt(argv[1:],'hcdrvi',['help','copy', 'debug','recursive', 'verbose', 'image'])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        return 2
    
    copy      = False
    debug     = False
    verbose   = False
    imageOnly = False
    recursive = False

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            return 1
        elif o in ("-c", "--copy"):
            copy=True
        elif o in ("-d", "--debug"):
            debug=True
        elif o in ("-r", "--recursive"):
            recursive=True
        elif o in ("-v", "--verbose"):
            verbose=True
        elif o in ("-i", "--image"):
            imageOnly=True
        else:
            assert False, "unhandled option"

    try:
        basePath = args[0]
        if not os.path.isdir(basePath):
            raise NameError(basePath + " is not a directory")
    except:
        usage()
        return 2

    try:
        outPath = args[1]
    except IndexError:
        outPath = basePath

    print "Processing " + basePath + ' to ' + outPath

        
    if recursive:
        fitsFileName = scanFitsDirectories(basePath,verbose=verbose)
    else:
        fitsFileName = scanFitsDirectory(basePath, verbose=verbose)
        
    fitsFileName = removeBad(fitsFileName, ProposalHash, imageOnly=imageOnly, verbose=verbose)
    moveFitsFile(fitsFileName, ProposalHash, SourceHash, outPath, copy=copy, debug=debug, verbose=verbose)
    if not debug:
        removeEmptyDir(basePath)


if __name__ == "__main__":
    sys.exit(main())
