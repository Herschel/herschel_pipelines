#!/usr/bin/env python

import pyfits, pywcs, asciitable, numpy
import os, sys, string, shutil, getopt, copy

def usage():
    """
    print usage function
    """
    print "usage cutoff.py inputImage inputFileList size outputImage"
    print ""
    print "This program generate small cutoff around position in images"
    print "" 
    print "positional arguments:"
    print " inputImage     path to the input fits image"
    print " inputFileList  path to the input file containing the list of positions"
    print " size           size in deg of the cutoffs"
    print " outputImage    path for the output cutoff in a multiextension fits file"
    print ""
    print "optional arguments:"
    print " -h, --help     show this help message and exit"
    print " -v, --verbose  produce a more verbose output"
    print ""

def inputfilelist():
    """
    print the inputFilelist description 
    """
    print "The inputFileList describing the list of position can be of two formats, either "
    print " - 2 columns (ra & dec in decimal degree )"
    print " - 3 columns (ID (ascii without space) ra & dec in decimal degree)"
    print ""
 

def main(argv=None):

    if argv is None:
        argv = sys.argv
   
    try:
        opts, args = getopt.getopt(argv[1:],'hv',['help','verbose'])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        return 2

    
    verbose = False
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            return 1
        elif o in ("-v", "--verbose"):
            verbose=True
        else:
            assert False, "unhandled option"


    try:
        inputImage = args[0]
        if not os.path.isfile(inputImage):
            raise NameError(inputImage + " is not a file")

        inputFileList = args[1]
        if not os.path.isfile(inputFileList):
            raise NameError(inputFileList + " is not a file")

        imsize      = float(args[2])
        outputImage = args[3]
        
    except NameError as error:
        print "EE - " + error[0]
        print ""
        usage()
        return 2
    except IndexError:
        usage()
        return 2

    hdulist = pyfits.open(inputImage)
    try:
        Image = hdulist['Image']
        wcs = pywcs.WCS(Image.header)
            
    except KeyError:
        print "EE - No 'Image' extension in "+os.path.basename(inputImage)
        return 2


    data = asciitable.read(inputFileList)
    if len(data.dtype) != 2 and len(data.dtype) != 3:
        inputfilelist()
        return 2

    print "Extracting " + os.path.basename(inputFileList) + " from " + os.path.basename(inputImage) + " into " + os.path.basename(outputImage)

    nObject = numpy.size(data)

    if (len(data.dtype) == 2):
        ID  = numpy.array(["ID"+str(iObject) for iObject in numpy.arange(nObject)]).view(numpy.recarray)
        ra  = data[data.dtype.names[0]]
        dec = data[data.dtype.names[1]]
    else:
        ID  = data[data.dtype.names[0]]
        ra  = data[data.dtype.names[1]]
        dec = data[data.dtype.names[2]]
    
    pixcrd = wcs.wcs_sky2pix(numpy.transpose(numpy.array([ra,dec])),0)
    
    # Size in pixel for the stamps
    if wcs.wcs.has_cd():
        pixSizeX = int(round(imsize / abs(wcs.wcs.cd[0,0])))
        pixSizeY = int(round(imsize / abs(wcs.wcs.cd[1,1])))
    else:
        pixSizeX = int(round(imsize / abs(wcs.wcs.cdelt[0])))
        pixSizeY = int(round(imsize / abs(wcs.wcs.cdelt[1])))


    # Make sure we have a odd size
    if (pixSizeX % 2) == 0:
        pixSizeX += 1

    if (pixSizeY % 2) == 0:
        pixSizeY += 1

        #TODO: Check presence sources in map...

    Output_HDUs = [pyfits.PrimaryHDU()]
    for iObject in numpy.arange(nObject):
        subMap = Image.data[round(pixcrd[iObject][1]-(pixSizeY-1)/2):round(pixcrd[iObject][1]+(pixSizeY-1)/2), \
                                round(pixcrd[iObject][0]-(pixSizeX-1)/2):round(pixcrd[iObject][0]+(pixSizeX-1)/2)]
        # print ID[iObject], ra[iObject], dec[iObject],pixcrd[iObject][0],  pixcrd[iObject][1], numpy.shape(subMap)
        subWcs = wcs.deepcopy()
        subWcs.naxis1 = pixSizeX
        subWcs.naxis2 = pixSizeY
        subWcs.wcs.crpix = subWcs.wcs.crpix-numpy.array([pixcrd[iObject][0]-(pixSizeX-1)/2,pixcrd[iObject][1]-(pixSizeY-1)/2])
        if subWcs.wcs.has_cd():
            subWcs.wcs.cdelt = [subWcs.wcs.cd[0,0], subWcs.wcs.cd[1,1]]
            subWcs.wcs.pc    = [[1,0],[0,1]]
            del subWcs.wcs.cd
            subWcs.wcs.set()
        hdu = pyfits.ImageHDU(data=subMap, header=subWcs.to_header(), name=ID[iObject])
        Output_HDUs.append(hdu)

    hdulist = pyfits.HDUList(Output_HDUs)
    hdulist.writeto(outputImage, clobber=True)

if __name__ == "__main__":
    sys.exit(main())
