startDir = "."

filenames = FILE_SEARCH(startDir,'*.fits')
FOR iFile=0L, N_ELEMENTS(filenames)-1 DO BEGIN

   FITS_OPEN,filenames[iFile],fcb, MESSAGE=error,/UPDATE
   IF error NE '' THEN CONTINUE

   posSig  = (WHERE(STRMATCH(fcb.extname, 'signal') NE 0))[0]

   IF posSig  EQ -1 THEN BEGIN
      FITS_CLOSE,fcb
      CONTINUE
   ENDIF
   
   PRINT,filenames[iFile]

   IF posSig NE -1 THEN BEGIN
      FITS_READ, fcb, data, mainHeader,EXTEN_NO=0,      /header_only
      FITS_READ, fcb, data,     header,EXTEN_NO=posSig, /header_only
      ;; Unit is under the f... HIERARCH system from ESO/ESA
      ;; so need to take care of this 
      whereKey =  WHERE(STRMATCH(header,"*'unit'*",/fold_case) EQ 1, isKey)
      IF isKey NE 0 THEN BEGIN
         key = ($
               STRSPLIT( header[whereKey],' (key.)=',/extract)$
               )[1]
         unit = SXPAR(header,key,COMMENT=comment, COUNT=count)
         
         IF count NE 0 THEN BEGIN
            SXADDPAR,mainheader,'UNIT',unit, comment
            MODFITS,fcb,data,mainHeader,EXTEN_NO=0
         ENDIF
      ENDIF
   ENDIF
   
   FITS_CLOSE,fcb

ENDFOR

END
