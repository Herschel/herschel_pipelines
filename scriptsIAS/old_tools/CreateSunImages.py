#! /usr/bin/python
"""

@author: Pablo ALINGERY
"""

from sdo_client_medoc import *

def getListFiles(basePath):
        listfile_all=[]
        for root, dirs, files in os.walk(basePath):
                listfile = [root+'/'+filename for filename in files if (filename.endswith('.fits'))]
                listfile_all.extend(listfile)
        return listfile_all


d1 = datetime(2011,06,7,0,0,0)
d2 = d1 + timedelta(days=1)

sdo_data_list = media_search( DATES=[d1,d2], WAVES=['304'], CADENCE=['1 h'], nb_res_max=1000 )
#Need to get a tar ball or zip file :
#A bit slower than the previous one
#media_get (MEDIA_DATA_LIST=sdo_data_list,DOWNLOAD_TYPE="tar", target_dir="/tmp" ,FILENAME="my_download_file.tar")

#And if you want to specifies files name do sthg like
for item in sdo_data_list :
#    file_date_obs=item.date_obs
#    file_wave=item.wave
    item.get_file( DECOMPRESS=False, TARGET_DIR='results', QUIET=False )

#Search meta data info 
basepath="./results"
listfiles=getListFiles(basepath)

for filename in listfiles:
	command="ds9 "+filename+" -zoom to fit  -zscale -saveimage png "+filename.replace("fits","png")+" -cmap heat -exit"
	os.system(command)

