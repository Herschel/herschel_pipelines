import pyfits
#from PACS_Spectro_Tools import getMetadata
from Database_Tools import getFilesMotif, getMetadata
from random import shuffle

directory = "/data/glx-herschel/data1/herschel/Release/"

motifs = ["Release/R1","MAPS",".fits"]
exclure = []

listfile = getFilesMotif(directory,motifs,exclure)[:]
print len(listfile)
lsitfile = shuffle(listfile)

def getAllKeys(filename,value=False):
	#print filename
	fits = pyfits.open(filename)
	meta = {}
	for i in range(2): 
		try:
			meta.update(getMetadata(fits[i].header))
		except:
			pass
	
	if value: return meta
	else: return sorted(list(meta.keys()))

def incrementMetaNumber(metaOccurence,metaFile,metadata):
	for meta in metaFile:
		if meta in metadata:
			if meta in metaOccurence.keys(): metaOccurence[meta]+=1
			else: metaOccurence[meta]=1
	return metaOccurence

#for key, value in getAllKeys(listfile[0],value=True).iteritems(): print key, value
#for key in getAllKeys(listfile[0]): print key

meta = []
metaOccurence = {}
#for filename in listfile[:]:
for i in range(len(listfile[:])):
	filename = listfile[i]
	print i,filename
	#print "before:",len(meta),
	metaFile = getAllKeys(filename)
	meta = meta + list( set(metaFile) - set(meta) )
	metaOccurence = incrementMetaNumber(metaOccurence,metaFile,meta)
	#print "after:",len(meta)

allMetaFile = "allMetadata_maps.txt"
outfile = open(allMetaFile,"wb")
outfile.write("Number of maps %s \n" % len(listfile))
#outfile.write("\n".join(sorted(meta)))
from collections import OrderedDict
metaOccurence_ordered = OrderedDict(sorted(metaOccurence.items(), key=lambda x: x[1],reverse=True))
for k, v in metaOccurence_ordered.items(): outfile.write("%s %s\n" % (k,v))

