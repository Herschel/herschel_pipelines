import os, string, sys, re

directory = "/data/glx-herschel/data1/herschel/Release/R1_pacs_spectro/PACSman_Fits/SPECTRO_PACS/SAG-4/"
template = "/data/glx-herschel/data1/herschel/Release/R1_pacs_spectro/PACSman_Fits/templatePreview.html"

def checkMotif(chaine,Motifs, Exclure):
	for motif in Motifs:
		if str(chaine).count(motif)==0:
			return False
	
	for exclu in Exclure:
		if str(chaine).count(exclu)>0:
			return False
	
	return True

def getFilesMotif(basePath,motifs,exclure):
  
	listfile_all = []
	listroot = []
	for root, dirs, files in os.walk(basePath):
		listfile = [root+"/"+filename for filename in files if checkMotif(filename,motifs,exclure)]
		listfile_all.extend(listfile)
	return listfile_all
	

def makeTxtPACSman(fitsfile):
  
	fits = pyfits.open(fitsfile)

	try:
		mainHeader = fits[0].header

	finally:
		fits.close()

	filename = fitsfile[:len(fitsfile)-5]+".txt"
	fileToWrite = open(filename,"w")
	toWrite = "Main Header\n\n"+str(mainHeader)
	fileToWrite.write(toWrite)


motifs = [".fits"]
exclure = ["RasterCubeCorrPACSman","mask","smooth","spectra"]
listfile = getFilesMotif(directory,motifs,exclure)
listfile = list(sorted(listfile))

for i in listfile: print i

tmp = open(template, 'r')
htmlString = tmp.read()
tmp.close()

for i in listfile:
	
	champ = i.split("/")[-3]
	line = i.split("/")[-2]
	filenameFits = i.split("/")[-1]
	matchObj = re.match( r'.*(\d{10}).*', filenameFits)
	obsid = str(matchObj.group(1))
	filenameHtml = i.replace(".fits",".html")
	
	fichier = open(filenameHtml, "w")
	htmlString_i = htmlString.replace("CHAMP",champ)
	htmlString_i = htmlString_i.replace("OBSID",obsid)
	htmlString_i = htmlString_i.replace("LINE",line)
	fichier.write(htmlString_i)
	fichier.close()
	
	makeTxtPACSman(i)
	

