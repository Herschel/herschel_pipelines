#!/usr/bin/env python
# -*-coding:utf-8 -*

from Database_Tools import getFilesMotif
from multiprocessing import Pool
from random import shuffle
import pyfits

####################################################################################################################
####################################################################################################################

# SANEPIC_FITS
#directory = "/data/glx-herschel/data1/herschel/Release/Attic/SANEPIC_Fits/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/SANEPIC_Fits/L1_Sanepic_SPIRE/"
#directory = "/data/glx-herschel/data1/herschel/Release/R4_spire_photo/"

# HIPE_FITS
directory = "/data/glx-herschel/data1/herschel/Release/R5_spire_photo/HIPE_Fits/MAPS_SPIRE/SAG-4/n2023"
#directory = "/data/glx-herschel/data1/herschel/Release/R3_spire_fts/HIPE_Fits/FTS_SPIRE/OT1_atielens/"
#directory = "/data/glx-herschel/data1/herschel/Release/R2_pacs_spectro/HIPE_Fits/SPECTRO_PACS/SAG-4"

#motifs = ["UrsaMajor_combined_PLW.fits"]
motifs = [".fits"]
#motifs = ["cube.fits","1342197489_Ced201-3_SPIRE-FTS_10.0.2747_HR_SLW_aNB_15"]
#exclure = ["ATLAS","old","test"]
#exclure = ["RebinnedCube","SlicedCube","Frame","Raster","OT2_ehabart"]
exclure = []

####################################################################################################################
####################################################################################################################

listfile = getFilesMotif(directory,motifs,exclure)[:]
print len(listfile)
#for i in listfile: print i

metadata = ["ORIGIN","OBSID001"]

def checkMetadata(i):
	fits = pyfits.open(i)
	print i,
	for meta in metadata: 
		try: 
			print fits[0].header[meta],
		except: print "NO "+meta,
	print ""

Pool().map(checkMetadata,listfile)

