#execfile('/data/glx-herschel/data1/herschel/scriptsIAS/IAS_tools.py')

# Import all needed classes
from herschel.spire.all import *
from herschel.spire import all
from herschel.ia.all import *
from herschel.ia.task.mode import *
from herschel.ia.pg import ProductSink
from java.lang import *
from java.util import *
from herschel.ia.obs.util import ObsParameter
from herschel.ia.dataset.image.wcs import *
import random
import os, shutil

# Import the script tasks.py that contains the task definitions
from herschel.spire.ia.pipeline.scripts.POF5.POF5_tasks import *

# Input definition
from herschel.spire.ia.pipeline.scripts.POF5.POF5_input import *
from herschel.spire.ia.scripts.tools.obsLoader import *
#from herschel.ia.pal.browser.v2.table.model import StorageResult
from herschel.ia.pal.util import StorageResult
from herschel.spire.ia.pipeline.phot.fluxconv import * 
from herschel.spire.ia.pipeline.phot.baseline import *
from herschel.spire.ia.pipeline.scripts.twopass.twopass_tasks import *
from herschel.spire.ia.pipeline.phot.zeropoint import ZeroPointCorrectionTask
from herschel.ia.gui.apps.modifier import JOptionModifier, JFilePathModifier
from herschel.ia.gui.kernel.util.field import FileSelectionMode
from javax.swing.filechooser import FileFilter
from java.util import List, Arrays, LinkedHashMap, ArrayList, HashMap
from os import path 
import glob
import string
import time



def getCustomModifiers(self): 
		map = LinkedHashMap()
		map.put("hfi857Map", JFilePathModifier())
		map.put("hfi545Map", JFilePathModifier())
		return map

def destriping( level, l2DeglitchRepeat=None, diagnostic=None, step=None,wcsMap={},SPECIAL_MAPPING_ONLY=False):
	"""
	Function to call the destriper on specified level1 context.
	Input:
	     level1 context: the level1 context to process
	     l2DeglitchRepeat: number of repetition before to perform the 2nd level deglitch;
	             if set to zero, the 2nd level deglitching is not performed
	     diagnostic: the diagnostic product to use as start parameter or None
	Return:
	      level1 context (baseline removed)
	      list of level2 maps
	      list of diagnostic product
	"""
	SPECIAL_MAPPING_ONLY = SPECIAL_MAPPING_ONLY
	maps = {}
	diags = {}
	arrays = ['PSW', 'PMW', 'PLW']
	jumpThresh = -1
	if (l2DeglitchRepeat==None):
		l2DeglitchRepeat = 0
	if l2DeglitchRepeat!=0:
		jumpThresh = 0
	else:
		jumpThresh = jumpThresh
	pass
	
	minVel = 5
	maxVel = None
	polyDegree = 0
	brightSourceThresh = 1.5
	roi = None
	kappa = 5.0
	kappa2 = 5.0
	jumpIter = 100
	#jumpThresh=-1
	offsetFunction = "perScan"
	tempStorage = True
	if step==1:
		minVel=0.0
	size={"PSW":6.0, "PMW":10.0, "PLW":14.0}
	wcs={"PSW":None, "PMW":None, "PLW":None}

	useOnlyMasks = getMasks(step)
	for array in arrays:
		if SPECIAL_MAPPING_ONLY:
			maps[array] = naiveScanMapper(input = level, array = array)
			diags = None
		else:
			if diagnostic!=None:
				dg = diagnostic[array]
			else:
				dg = None
			print "Processing destriping", array
			(level, map, diag, p4, p5) = destriper(level1=level, \
				array=array, nThreads=8, kappa=kappa,kappa2=kappa2, l2DeglitchRepeat=l2DeglitchRepeat, jumpThresh=jumpThresh, jumpIter=jumpIter, l2IterMax=3, \
				withMedianCorrected=True, useSink=tempStorage, startParameters=dg, offsetFunction=offsetFunction, useOnlyMasks=useOnlyMasks, \
				polyDegree=polyDegree, brightSourceThresh=brightSourceThresh,  pixelSize=size[array],wcs=wcs[array], minVel=minVel, maxVel=maxVel)
			maps[array] = map
			diags[array] = diag
	return (level, maps, diags)

def getMasks(step):
	"""
	Return the masks value to use during destriping phase
	Input: step if step =1 the turnarournd data are not excluded
	"""
	if (step==1):
		return [SpireMask.MASTER, SpireMask.ISDEAD, SpireMask.ISNOISY, SpireMask.ISSLOW, SpireMask.ISNOCHOPSKY, SpireMask.GLITCH_FIRST_LEVEL_UNCORR, \
		SpireMask.GLITCH_SECOND_LEVEL_UNCORR, SpireMask.TRUNCATED_UNCORR, SpireMask.GLITCH_FIRST_LEVEL, SpireMask.GLITCH_SECOND_LEVEL, SpireMask.NO_RESP_DATA]
	if (step==2 or step==None):
		return [SpireMask.MASTER, SpireMask.ISDEAD, SpireMask.ISNOISY, SpireMask.ISSLOW, SpireMask.ISNOCHOPSKY, SpireMask.GLITCH_FIRST_LEVEL_UNCORR, \
		SpireMask.GLITCH_SECOND_LEVEL_UNCORR, SpireMask.TRUNCATED_UNCORR, SpireMask.GLITCH_FIRST_LEVEL, SpireMask.GLITCH_SECOND_LEVEL, SpireMask.NO_RESP_DATA, SpireMask.TURNAROUND]


def exportMetadata_fromFits(filelist):
	p = Product()
	oneLine = FitsArchive().load(filelist[0])
	#print filelist[0]
	metaLevel1 = oneLine.getMeta()
	p.meta = metaLevel1
	if (metaLevel1.containsKey("temperatureDriftCorrectionDone")==0):
		p.meta["temperatureDriftCorrection"] = StringParameter("False","temperatureDriftCorrection")
	else:
		p.meta["temperatureDriftCorrectionDone"] = metaLevel1["temperatureDriftCorrectionDone"]
	if (metaLevel1.containsKey("electricalCrosstalkCorrectionDone")==0):
		p.meta["electricalCrosstalkCorrectionDone"] = StringParameter("False","electricalCrosstalkCorrectionDone")
	else:
		p.meta["electricalCrosstalkCorrectionDone"] = metaLevel1["electricalCrosstalkCorrectionDone"]
	if (metaLevel1.containsKey("opticalCrosstalkCorrectionDone")==0):
		p.meta["opticalCrosstalkCorrectionDone"] = StringParameter("False","opticalCrosstalkCorrectionDone")
	else:
		p.meta["opticalCrosstalkCorrectionDone"] = metaLevel1["opticalCrosstalkCorrectionDone"]
	p.meta["creationDate"] = StringParameter(time.strftime('%d/%m/%y %H:%M',time.localtime()),"creationDate")
	p.meta["scriptID"] = StringParameter("$Rev: 1453 $","$Id: IAS_tools.py 1453 2016-02-19 13:46:56Z kdassas $")
	p.meta["FromFits"] = StringParameter("1","FromFits")
	History = oneLine.getHistory()
	p["HistoryTask"] = History
	return p

def exportMetadata(obs,indexLeg):
# return a product with chosen L1 and L2 metadata + L1 History Task
	p = Product()
	metaLevel2 = obs.meta
	metaLevel1 = obs.level1.getProduct(0).meta
	#ListMetaL1=["elecSide","biasMode","timeOffset","timeDrift","invalidOffsetFlag","adcErrFlag","plwBiasAmpl","pmwBiasAmpl","pswBiasAmpl","ptcBiasAmpl","rcRollApp", \
	#      "fluxConversionDone","startNominalScanLine","endNominalScanLine"]
	ListMetaL1 = ["elecSide","biasMode","timeOffset","timeDrift","invalidOffsetFlag","adcErrFlag","plwBiasAmpl","pmwBiasAmpl","pswBiasAmpl","ptcBiasAmpl","rcRollApp", \
	              "fluxConversionDone"]
	ListMetaL2 = ["type","description","instrument","startDate","endDate","obsid","odNumber","cusMode","instMode","obsMode","origin", \
	              "aorLabel","aot","equinox","object","raDeSys","raNominal","decNominal","ra","dec","posAngle","telescope","observer","proposal","scanSpeed","mapLength","mapHeight",\
	             "numScanLinesNom","numScanLinesOrth"]
	for i in indexLeg[1:]:
		metaLevel1 = obs.level1.getProduct(i).meta
		metaLevel1_before = obs.level1.getProduct(i-1).meta
		# insert METADATA LEVEL1
		for j in range(len(ListMetaL1)):
		#       #test if keywords are equal for all legs: if not, exit
			if (metaLevel1[ListMetaL1[j]]!=metaLevel1_before[ListMetaL1[j]]):
				print "metalLevel1",metaLevel1[ListMetaL1[j]]
				print "metalLevel1before",metaLevel1_before[ListMetaL1[j]]
				#exit()
			p.meta[ListMetaL1[j]] = metaLevel1[ListMetaL1[j]]
	# insert METADATA LEVEL2
	for i in range(len(ListMetaL2)):
		try:
			p.meta[ListMetaL2[i]] = metaLevel2[ListMetaL2[i]]
		except:
			print  ListMetaL2[i]," not in obs metadata" 
	# TODO: Also retrieve the SpireCal product version 
	if (metaLevel1.containsKey("temperatureDriftCorrectionDone")==0):
		p.meta["temperatureDriftCorrection"] = StringParameter("False","temperatureDriftCorrection")
	else:
		p.meta["temperatureDriftCorrectionDone"] = metaLevel1["temperatureDriftCorrectionDone"]
	if (metaLevel1.containsKey("electricalCrosstalkCorrectionDone")==0):
		p.meta["electricalCrosstalkCorrectionDone"] = StringParameter("False","electricalCrosstalkCorrectionDone")
	else:
		p.meta["electricalCrosstalkCorrectionDone"] = metaLevel1["electricalCrosstalkCorrectionDone"]
	if (metaLevel1.containsKey("opticalCrosstalkCorrectionDone")==0):
		p.meta["opticalCrosstalkCorrectionDone"] = StringParameter("False","opticalCrosstalkCorrectionDone")
	else:
		p.meta["opticalCrosstalkCorrectionDone"] = metaLevel1["opticalCrosstalkCorrectionDone"]
	p.meta["creationDate"] = StringParameter(time.strftime('%d/%m/%y %H:%M',time.localtime()),"creationDate")
	p.meta["scriptID"] = StringParameter("$Rev: 1453 $","$Id: IAS_tools.py 1453 2016-02-19 13:46:56Z kdassas $")
	# History Tas and Build Version
	HistoryTask = obs.level1.getProduct(0).getHistory()
	HistoryTask_forVersion = obs.refs["level1"].product.refs[0].product["History"]["HistoryTasks"]
	Version = HistoryTask_forVersion["BuildVersion"].getData()
	p.meta["BuildVersion"] = StringParameter(str(Version[0]), "BuildVersion")
	p["HistoryTask"] = HistoryTask
	# get Calibration Tree 
	CalibVersion = obs.refs["calibration"].product.meta["version"].getString()
	p.meta["CalibrationVersion"] = StringParameter(str(CalibVersion),"CalibrationVersion")
	return p

#'_' + string.join(meta['object'].value.split()) + \
def makeName(dir,meta,suffix=''):
	filename = dir+ \
		'OD'+ str(meta['odNumber'].value)+ \
		'_' + hex(meta['obsid'].value)+ \
		'_' + str(meta['cusMode'].value) + \
		'_' + meta['object'].value.replace(" ","").replace("+","plus") + \
		suffix
	return filename

#### add missing metadata to level1Frames from PACS pipelines
### listfile= glob.glob('/home/kdassas/Glx_herschel/HIPE_Fits/L1_PACS/SAG-4/*/*blue_level1Frames.fits')
### ListMetaL1 = ["observer","obsid","object","odNumber","instMode","cusMode","obsMode","pointingMode","ra","dec","raDeSys","raNominal","decNominal",\
#"equinox","camName","blue","repFactor"]
def exportL1PACSMetadata(listfile,listMetaL1):
	for fileL1 in listfile:
		# get Level1 Frame to update
		framesLevel1 = FitsArchive().load(fileL1)
		L1split = fileL1.split("/")
		# get object name from File
		object_name = L1split[len(L1split)-2]
		print object_name
		filename = L1split[len(L1split)-1]
		print filename
		filenameSplit = filename.split("_")
		obsid = filenameSplit[0]
		print obsid
		poolName = object_name+"_PACS_orig"
		# mettre la bonne obsid
		obs = getObs(poolName,obsid)
		metaObs = obs.meta
		print metaObs
		for j in range(len(ListMetaL1)):
			try:
				framesLevel1.meta[ListMetaL1[j]] = metaObs[ListMetaL1[j]]
				print metaObs[ListMetaL1[j]]
			except:
				print "metaL1 pas dans level1"
				#framesLevel1.meta[ListMetaL1[j]]=StringParameter("None",ListMetaL1[j])
		FitsArchive().save(fileL1,framesLevel1)


def exportL2PACSMetadata(listfile):
	for fileL2 in listfile:
		# get Level1 Frame to update
		framesLevel2 = FitsArchive().load(fileL2)
		L2split = fileL2.split("/")
		# get object name from File
		object_name = L2split[len(L2split)-2]
		print object_name
		filename = L2split[len(L2split)-1]
		print filename
		poolName = object_name+"_PACS_orig"
		if object_name=="ursamajors":
			listpool = ["ursamajors1_PACS_orig", "ursamajors2_PACS_orig"]
		else:
			listpool = [poolName]
		#obs = getObs(poolName,obsids[0])
		#metaObs=obs.meta
		#print metaObs
		obsids_data = obsIdTable(listpool)
		framesLevel2["obsids"] = obsids_data
		framesLevel2.meta["object"] = StringParameter(object_name, "object")
		FitsArchive().save(fileL2,framesLevel2)


def makeCombinedName(dir, suffix='', listpool=[]):
	listpool_string = listpool[0]
	for poolname in listpool[1:]:
		listpool_string = listpool_string+'_'+poolname
	filename = dir+ \
		listpool_string + \
		suffix
	return filename

def obsIdTable(listpool):
	array_obsIds = Long1d()
	for poolname in listpool:
		obsIds = getObsIds(poolname)
		for obsid in obsIds:
			array_obsIds.append(obsid)
	#
	obsids = TableDataset()
	obsids["obsids"] = Column(array_obsIds,description="obsids")
	return obsids

def obsIdTable_fromObsids(obsIds):
	array_obsIds = Long1d()
	for obsid in obsIds:
		array_obsIds.append(obsid)
	#
	obsids = TableDataset()
	obsids["obsids"] = Column(array_obsIds,description="obsids")
	return obsids

def obsIdTable_fromList(obsIds):
	array_obsIds = Long1d()
	for obsid in obsIds:
		array_obsIds.append(obsid)
	obsids = TableDataset()
	obsids["obsids"] = Column(array_obsIds,description="obsids")
	return obsids

def makeName_simple(dir,meta,suffix=''):
	filename = dir+ \
		+ string.join(meta['object'].value.split()) + \
		suffix
	return filename

def getRandomChannel(obs):
	listChan_uniq = []
	for array in ["PSW","PMW","PLW"]:
		listChan_array = []
		LW_tab = obs.refs["calibration"].product.refs["Phot"].product.refs["ChanNum"].product[array]
		LW_Tab = LW_tab["names"].getData()
		while len(uniq(listChan_array)) < int(len(LW_Tab)/2) :
			position = random.randrange(len(LW_Tab))
			listChan_array.append(str(LW_Tab[position]))
			listChan_uniq.append(str(LW_Tab[position]))
	#print listChan_uniq
	return uniq(listChan_uniq)


def listObsIds(poolName):
	obsIds = getObsIds(poolName)
	for obsid in obsIds:
		print hex(obsid)

def getObsIds(poolName):
	storage = ProductStorage([poolName])
	queryResult = StorageResult(storage, storage.select(MetaQuery(ObservationContext, "p", "1")))
	obsIds = []
	for refs in queryResult.listIterator():
		meta = refs.getMeta()
		obsIds.append(meta['obsid'].value)
	del(storage)
	return obsIds

def newPoolList(poollist,suffix=''):
	newPoolList = []
	for poolName in poollist:
		storage = ProductStorage([poolName])
		queryResult = StorageResult(storage, storage.select(MetaQuery(ObservationContext, "p", "1")))
		for refs in queryResult.listIterator():
			newPoolList.append(makeName('',refs.getMeta(),suffix))
		del(storage)
	return newPoolList



def getObs(poolName, obsid):
	# Register the pool...
	storage = ProductStorage([poolName])
	# ... find the obsid ...
	query = MetaQuery(ObservationContext,"p","p.meta['obsid'].value==%iL"%obsid)
	refs = storage.select(query)
	# TODO : What if several refs ? 
	# TODO : What about versions ?
	# ... and finally retrieve the first ref
	obs = refs[0].product
	return obs

def getObsIAS(poolName, obsid):
	# Register the pool...
	storage = ProductStorage([poolName])
	# ... find the obsid ...
	query = MetaQuery(ObservationContext,"p","p.meta['obsid'].value==%iL"%obsid)
	refs = storage.select(query)
	# TODO : What if several refs ? 
	# TODO : What about versions ?
	# ... and finally retrieve the first ref
	obs = refs[0].product
	return obs

def createWcs(resolution,naxis1,naxis2,crpix1,crpix2,meta):
	wcs = Wcs()
	print naxis1
	wcs.setNaxis1(naxis1)
	wcs.setNaxis2(naxis2)
	wcs.setCrpix1(crpix1)
	wcs.setCrpix2(crpix2)
	wcs.setCrval1(meta['raNominal'].value)
	wcs.setCrval2(meta['decNominal'].value)
	wcs.setCdelt1(-resolution / 3600.0)
	wcs.setCdelt2(resolution / 3600.0)
	wcs.setCtype1("RA--")
	wcs.setCtype2("DEC-")
	wcs.setProjection("-TAN")
	wcs.setEquinox(2000.)
	wcs.setCrota2(0.)
	return wcs

def cutLevel1Obsids(scansAllObsids,obsid):
	scansOneObsid = SpireListContext()
	#TODO, add NbBurps as output
	NbBurps = 0
	for i in range(scansAllObsids.getCount()):
		if scansAllObsids.refs[i].product.meta["obsid"].value == obsid:
			ref = ProductSink.getInstance().save(scansAllObsids.getProduct(i))
			scansOneObsid.addRef(ref)
			try:
				if psp.meta["coolerBurpFound"].value=="True":
					NbBurps = NbBurps+1
					if doCoolerBurpCorr==2:
						continue
			except:
				NbBurps = 9999

	return (scansOneObsid, NbBurps)

def getAllLegs(scans, obs, listChan_given=None,doRmBaselinePerLeg=True,doRmBaselinePerScan=False,doPolynomial=False,doApplyExtendedEmissionGains=False,doCoolerBurpCorr=0):
	# Will load obsid in poolname and add ref from all legs to scans
	# performing a baseline per leg (median) if asked 
	scansOneObsid = SpireListContext()
	chanNum = obs.calibration.phot.chanNum
	chanRelGains = obs.calibration.phot.chanRelGain
	obsid = obs.meta["obsid"].value
	print "Adding",hex(obsid)
	#### CoolerBurps
	NbBurps = 0
	for i in range(obs.level1.count):
		psp = obs.level1.getProduct(i)
		
		# Skip the current id if serendipity
		if psp.meta['bbid'].value>>16 != 0xa103:
			continue
		
		print "adding scan ",i
		try:
			if psp.meta["coolerBurpFound"].value=="True":
				NbBurps += 1
				if doCoolerBurpCorr==2:
					continue
		except:
			NbBurps = 9999
		if listChan_given!=None :
			for chan in psp.channelNames:
				if chan in listChan_given:
						psp.removeColumn(chan)
		if doApplyExtendedEmissionGains:
			print "Apply relative gains for bolometers for better extended maps"
			psp.meta['type'].value = "PSP"  # Necessary for pre-HCSS 8.0 processed observations
			applyRelativeGains = ApplyRelativeGainsTask()
			psp = applyRelativeGains(psp, chanRelGains)
		ref = ProductSink.getInstance().save(psp)
		scansOneObsid.addRef(ref)
	if doRmBaselinePerLeg:
		if doPolynomial:
			print "removeBaseLinePolynomial  leg by leg"
			baselineRemovalPolynomial = BaselineRemovalPolynomialTask()
			scansOneObsid = baselineRemovalPolynomial(input=scansOneObsid, polyDegree=2, wholeTimeline=False)
		else: 
			print "removeBaseLine median  leg by leg"
			scansOneObsid = baselineRemovalMedian(input=scansOneObsid)
	if doRmBaselinePerScan:
		if doPolynomial:
			print "removeBaseLinePolynomial on all legs"
			baselineRemovalPolynomial = BaselineRemovalPolynomialTask()
			scansOneObsid = baselineRemovalPolynomial(input=scansOneObsid, wholeTimeline=True,polyDegree=2 )
		else:
			print "removeBaseLineMedian on all legs"
			scansOneObsid = baselineRemovalMedian(input=scansOneObsid,wholeTimeline=Boolean.TRUE)
			print "after removeBaseLineMedian on all legs"

	# Add all ref to the input SpireListContext
	for i in range(scansOneObsid.getCount()):
		ref = ProductSink.getInstance().save(scansOneObsid.getProduct(i))
		scans.addRef(ref)
	
	return scans,NbBurps


def createRGB(obs,doDestriper=False):
	level2 = obs.level2
	if doDestriper:
		mapPlw = level2.refs["PLW_destriped"].product
		mapPmw = level2.refs["PMW_destriped"].product
		mapPsw = level2.refs["PSW_destriped"].product
	else:
		mapPlw = level2.refs["PLW"].product
		mapPmw = level2.refs["PMW"].product
		mapPsw = level2.refs["PSW"].product	
	#
	# promote to LEVEL2_PROCESSED
	obs.obsState = ObservationContext.OBS_STATE_LEVEL2_PROCESSED
	print "Creating rgb Image..."
	# Create browse product and image
	createRgbImage = CreateRgbImageTask()
	browseProduct = createRgbImage(red=mapPlw,green=mapPmw,blue=mapPsw,percent=98.0,redFactor=1.0,\
		greenFactor=1.0,blueFactor=1.0)
	#
	# Populate metadata of the browse product
	for par in ObsParameter.values():
		if obs.meta.containsKey(par.key) and par.key != "fileName":
			browseProduct.meta[par.key] = obs.meta[par.key].copy()
	pass
	browseProduct.startDate = obs.startDate
	browseProduct.endDate = obs.endDate
	browseProduct.instrument = obs.instrument
	browseProduct.modelName = obs.modelName
	browseProduct.description = "Browse Product"
	browseProduct.type = "BROWSE"
	#
	# Attach the browse product to the ObservationContext
	obs.browseProduct = browseProduct
	#
	# Generate the browse image
	from herschel.ia.gui.image import ImageUtil
	imageUtil = ImageUtil()
	browseProductImage = imageUtil.getRgbTiledImage(\
	                     browseProduct["red"].data, browseProduct["green"].data, browseProduct["blue"].data)
	obs.browseProductImage = browseProductImage.asBufferedImage
	
	return obs


def reMap(obsid, poolname,useHsa=False, doRmBaselinePerLeg=True,doRmBaselinePerScan=False,doDestriper=False,doPolynomial=False,listChan_given=None, wcsPSW=None, wcsPMW=None, wcsPLW=None,resolution='',doApplyExtendedEmissionGains=False,doJumpThresh=False,doCoolerBurpCorr=0,doNoise=False,doExtdMaps=False):
	scans = SpireListContext()
	if doDestriper:
		doRmBaselinePerScan = False
		doRmBaselinePerLeg = False
	else:
		doRmBaselinePerScan = doRmBaselinePerScan
		doRmBaselinePerLeg = doRmBaselinePerLeg
	doRmBaselinePerScan = False
	doRmBaselinePerLeg = False
	if doExtdMaps:
		doApplyExtendedEmissionGains = True
	if useHsa:
		obs = getObservation(obsid,useHsa=True,instrument="SPIRE")
	else:
		obs = getObs(poolname, obsid)
	(scans, NbBurps) = getAllLegs(scans, obs, listChan_given=listChan_given, doRmBaselinePerLeg=doRmBaselinePerLeg, doRmBaselinePerScan=doRmBaselinePerScan,doPolynomial=doPolynomial,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains,doCoolerBurpCorr=doCoolerBurpCorr)
	print "NbBurps",NbBurps
	print "Creating Single Maps with correct WCS..."
	
	(level2, scans, diags) = doMaps(scans,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,resolution=resolution,doDestriper=doDestriper,doJumpThresh=doJumpThresh,doNoise=doNoise)
	if doExtdMaps:
		colorCorrHfi = obs.calibration.phot.colorCorrHfi
		fluxConvList = obs.calibration.phot.fluxConvList
		level2 = CreateExtdMaps(scans,level2,diags,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,resolution=resolution,colorCorrHfi=colorCorrHfi,fluxConvList=fluxConvList)
	level2.meta["NbBurps"] = LongParameter(NbBurps,"NbCoolerBurps")
	return level2

def combineMaps(listpool,useHsa=False,obsIds=None,resolution='',listChan_given=None,wcsPSW=None,wcsPMW=None,wcsPLW=None,doRmBaselinePerLeg=True,doRmBaselinePerScan=False,doDestriper=False,doPolynomial=False,doApplyExtendedEmissionGains=False,doJumpThresh=False,doCoolerBurpCorr=0,doExtdMaps=False):
	scans = SpireListContext()
	NbBurpsAll = 0
	if doExtdMaps:
		doApplyExtendedEmissionGains = True
	if useHsa:
		for obsid in obsIds:
			obs = getObservation(obsid,useHsa=True,instrument="SPIRE")
			(scans, NbBurps) = getAllLegs(scans, obs, listChan_given=listChan_given, doRmBaselinePerLeg=doRmBaselinePerLeg, doRmBaselinePerScan=doRmBaselinePerScan,doPolynomial=doPolynomial,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains,doCoolerBurpCorr=doCoolerBurpCorr)
	#
	else:
		for poolname in listpool:
			#print poolname
			if  not obsIds:
				list_obsids = getObsIds(poolname)
			else:	
				list_obsids = obsIds
			for obsid in list_obsids:
				obs = getObs(poolname, obsid)
				(scans, NbBurps) = getAllLegs(scans, obs,listChan_given=listChan_given, doRmBaselinePerLeg=doRmBaselinePerLeg, doRmBaselinePerScan=doRmBaselinePerScan,doPolynomial=doPolynomial,doApplyExtendedEmissionGains=doApplyExtendedEmissionGains,doCoolerBurpCorr=doCoolerBurpCorr)
	#
			NbBurpsAll = NbBurpsAll + NbBurps
	print "Combining Maps..."
	(level2, scans, diags) = doMaps(scans,resolution=resolution,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,doDestriper=doDestriper,doJumpThresh=doJumpThresh)
	if doExtdMaps:
		print "Create Extd Maps"
		colorCorrHfi = obs.calibration.phot.colorCorrHfi
		fluxConvList = obs.calibration.phot.fluxConvList
		level2 = CreateExtdMaps(scans,level2,diags,resolution=resolution,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW,colorCorrHfi=colorCorrHfi,fluxConvList=fluxConvList)
	if  not obsIds:
		obsids_product = obsIdTable(listpool)
	else:
		obsids_product = obsIdTable_fromList(obsIds)
	level2["obsids"] = obsids_product
	if (listChan_given!=None):
		channels = TableDataset()
		channels["names"] = Column(String1d(listChan_given),description="channels used for processing")
		level2["channels"] = channels
	level2.meta["NbBurps"] = LongParameter(NbBurpsAll,"NbCoolerBurps")
	return level2,scans

def saveMaps(level2Context,doExtdMaps=False,suffix='',listpool=[],dir='/data/glx-herschel/data1/herschel/HIPE_Fits/MAPS_SPIRE/'):
	print "saving ..."
	if doExtdMaps:
		(PSW, PMW, PLW) = ("extdPSW","extdPMW","extdPLW")
	else : 
		(PSW, PMW, PLW) = ("PSW", "PMW", "PLW")
	for map in [PSW,PMW,PLW]:
		print "       ..."+map
		product = level2Context.refs[map].product
		try:
			product.meta["NbBurps"] = level2Context.meta["NbBurps"]
		except:
			product.meta["NbBurps"] = LongParameter(9999, "NbCoolerBurps")
		if suffix.count("combined")==1:
			filename = makeCombinedName(dir, suffix, listpool)
			product["obsids"] = level2Context["obsids"]
			if suffix.count("chooseBolo")==1:
				product["channels"] = level2Context["channels"]
		else:
			filename = makeName(dir,product.meta,suffix)
		filename += '_'+map+'.fits'
		FitsArchive().save(filename,product)
		#simpleFitsWriter(product=product,file=filename)
		del(product)

def uniq(seq):
	keys = {}
	for e in seq:
		keys[e] = 1
	return keys.keys()


def copyPool(fromPool, toPool):
	params = java.util.HashMap()
	print "Creating pool "+toPool
	params.put('id', toPool)
	pool = herschel.ia.pal.managers.PoolCreatorFactory.createPool( 'lstore', params)
	del(params)
	
	for obsid in getObsIds(fromPool):
		print " saving "+hex(obsid)+"..."
		obs = getObs(fromPool,obsid)
		saveObs(obs,toPool)

def savePool(obs,suffix=''):
	# TODO: Check if better ways 
	# TODO: delete the previous pool if exist 
	# TODO: What about versioning ?
	#poolName = makeName('',obs.meta,suffix)
	poolName = makeName_simple('',obs.meta,suffix)
	store = ProductStorage()
	myPool = PoolManager.getPool(poolName)
	store.register(myPool)
	store.save(obs)


def saveObs(product,poolName):
#	myPool = LocalStoreFactory.getStore(poolName)
	poolholder = ProductStorage()  # "poolholder" can be any word
	myPool = PoolManager.getPool(poolName)
	# Now link the directory on disc to the "poolholder" in HIPE
	print "save product in ",poolName
	poolholder.register(myPool)
	poolholder.save(product)
	del(poolholder)

def Find_holes(obs):
	count = 0
	level0_5 = obs.level0_5
	# Open the input dialog to enter inputs
	level   = 'level0_5'
	# Set this to FALSE if you don't want to use the ProductSink
	# and do all the processing in memory
	tempStorage = Boolean.TRUE
	# set the progress
	progress = 20
	# counter for computing progress
	count = 0
	holes_box = []
	# From Level 0.5 to Level 1
	if level=="level0" or level=="level0_5":
		bbids = level0_5.getBbids(0xa103)
		# bbids=level0_5.getBbids()
		nlines = len(bbids)
		#print "number of scan lines:",nlines
		#
		# Loop over scan lines
		for bbid in bbids:
			block = level0_5.get(bbid)
			#print "processing BBID="+hex(bbid)
			# Now move to engineering data products
			pdt  = block.pdt
			nhkt = block.nhkt
			if pdt == None:
				logger.severe("Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block.")
				print "Building block "+hex(bbid)+" doesn't contain a PDT. Cannot process this building block."
				continue
			if nhkt == None:
				logger.severe("Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block.")
				print "Building block "+hex(bbid)+" doesn't contain a NHKT. Cannot process this building block."
				continue
			#
			bbCount = bbid & 0xFFFF
			pdtFollow = None
			nhktFollow = None
			pdtTrail = None
			nhktTrail = None
			if bbid < MAX(Long1d(bbids)):
			    # 0xaf000000+bbCount = bbid for turnaround 2
				blockFollow = level0_5.get(0xaf000000L+bbCount)
				pdtFollow = blockFollow.pdt
				nhktFollow = blockFollow.nhkt
				#print " pdtFollow.sampleTime(0) "+str(pdtFollow.sampleTime[0])+" pdt.sampleTime[-1]  "+str(pdt.sampleTime[-1])+" bbid "+hex(bbid)
				if pdtFollow != None and pdtFollow.sampleTime[0] > pdt.sampleTime[-1]+3.0:
					pdtFollow = None
					nhktFollow = None
					print "pdtFollow.sampleTime(0) > pdt.sampleTime[-1]+3.0  bbcount "+str(bbCount)+" bbid "+hex(bbid)
					print "bbid "+hex(bbid)
					print "bbCount "+str(bbCount)
			if bbCount >1:
			    # 0xaf000000+bbCount=bbid for turnaround 1
				blockTrail = level0_5.get(0xaf000000L+bbCount-1)
				pdtTrail = blockTrail.pdt
				nhktTrail = blockTrail.nhkt
				if pdtTrail != None and pdtTrail.sampleTime[-1] < pdt.sampleTime[0]-3.0:
				 	# print " pdtTrail.sampleTime(-1) "+str(pdtTrail.sampleTime[0])+ " pdt.sampleTime[0]  "+str(pdt.sampleTime[0])
					print "pdtTrail.sampleTime < pdt.sampleTime(0)-3  bbcount "+str(bbCount)+" bbid "+hex(bbid)
					pdtTrail = None
					nhktTrail = None
					# store indexes where discontinuity
					holes_box.append(int(bbCount-2))
					print holes_box
			#
			#print "Completed BBID="+hex(bbid)+" bbid "+str(bbid)
			# set the progress
			count = count+1
			progress = 20+(60*count)/nlines
	return  holes_box


def doMapNoise(scans,array,wcs):
	tempStorage = Boolean.TRUE
	level1FirstHalf = Level1Context()
	# Cut the level scans in two parts
	for i in range((scans.getCount())/2):
		psp = scans.getProduct(i)
		level1FirstHalf.addProduct(psp)
	level1SecondHalf = Level1Context()
	for i in range(scans.getCount()/2,scans.getCount()):
		psp = scans.getProduct(i)
		level1SecondHalf.addProduct(psp)
	# remove baseline on each part (useful?)
	#level1FirstHalf=baselineRemovalMedian(level1FirstHalf,tempStorage=tempStorage)
	#level1SecondHalf=baselineRemovalMedian(level1SecondHalf,tempStorage=tempStorage)
	mapsFirstHalf = naiveScanMapper(level1FirstHalf, array=array, wcs=wcs)
	mapsSecondHalf = naiveScanMapper(level1SecondHalf, array=array, wcs=wcs)
	#images = ArrayList()
	#mapsSecondHalf.image=-1*mapsSecondHalf.image
	#images.add(mapsSecondHalf)
	#images.add(mapsFirstHalf)
	DiffNoiseMap = imageSubtract(image1 = mapsFirstHalf, image2 = mapsSecondHalf, ref = 0)
	DiffNoiseMapDiv2 = imageDivide(image1 = DiffNoiseMap, scalar = 2)
	#DiffNoiseMaps = mosaic(images = images,wcs=wcs,oversample=0)
	return DiffNoiseMapDiv2 


def doMaps(scans, resolution='',wcsPSW=None,wcsPMW=None,wcsPLW=None,doDestriper=False,doJumpThresh=False,doNoise=False):
	print "mapping..."
	level2 = MapContext()
	level2.type = "level2context"
	level2.description = "Context for SPIRE Level 2 products"
	level2.meta["level"] = StringParameter("2.0", "The level of the product")
	arrays = ['PSW', 'PMW', 'PLW']
	wcsMap = {"PSW":wcsPSW, "PMW":wcsPMW, "PLW":wcsPLW}
	mapProgress = {"PSW":85, "PMW":90, "PLW":95}
	psize = {"PSW":6.0, "PMW":10.0, "PLW":14.0}
	maps = {}
	diags = {}
	if doDestriper:
		SPECIAL_MAPPING_ONLY = False
	else:
		SPECIAL_MAPPING_ONLY = True
	(scans, maps, diags) = destriping(level=scans, l2DeglitchRepeat=0, diagnostic=None, step=2,wcsMap=wcsMap,SPECIAL_MAPPING_ONLY=SPECIAL_MAPPING_ONLY)
	for array in arrays:
		map = maps[array]
		level2.refs.put(array,ProductRef(map))
	return (level2, scans, diags)

def CreateExtdMaps(scans,level2,diags,resolution='',wcsPSW='',wcsPMW='',wcsPLW='',colorCorrHfi=None,fluxConvList=None):

	# Deglitching Parameters
	l2DeglitchRepeat = 100
	kappa = 5.0
	kappa2 = 5.0

	# Destriping Parameters
	offsetFunction = "perScan"
	polyDegree = 0
	withMedianCorrected = True
	nThreads = 2
	jumpThresh = -1.0
	jumpIter = 100
	brightSourceThresh = 1.5
	roi = 0 
	
	# Mapmaking Parameters
	pswSize = 6.0      # Recommended map pixel size for PSW
	pmwSize = 10.0     # Recommended map pixel size for PMW
	plwSize = 14.0     # Recommended map pixel size for PLW
	minVel  = 5.0      # Recommended min scan velocity to be included in map
	level2ZeroPoint = makeContext(scans)
	arrays = ['PSW', 'PMW', 'PLW']
	wcsMap = {"PSW":wcsPSW, "PMW":wcsPMW, "PLW":wcsPLW}
	mapProgress = {"PSW":85, "PMW":90, "PLW":95}
	psize = {"PSW":6.0, "PMW":10.0, "PLW":14.0}
	
	hfi545Map  = "/data/glx-herschel/data1/herschel/data/PlanckZeroPoint/DX9_map_545_smooth_8arcmin.fits"
	hfi857Map  = "/data/glx-herschel/data1/herschel/data/PlanckZeroPoint/DX9_map_857_smooth_8arcmin.fits"

	hfi545Gain = zeroPointCorrection["hfi545Gain"]  # Recommended gain for Planck HFI 545GHz channel
	hfi857Gain = zeroPointCorrection["hfi857Gain"]  # Recommended gain for Planck HFI 857Hz channel
	hfiFwhm    = zeroPointCorrection["hfiFwhm"]     # Recommended Planck HFI FWHM
	#for i in range(scans.getCount()):
	#		psp = scans.getProduct(i)
	#		if psp.type=="PPT":  psp.setType("PSP")   #for old Level 1 contexts
	#		psp = applyRelativeGains(psp, chanRelGains)
	#		scansZeroPoint.addProduct(psp)
	fluxConv = fluxConvList.getProduct(scans.refs[-1].product.meta["biasMode"].value,scans.refs[-1].product.startDate)
	print fluxConv,colorCorrHfi
	
	(level1zp, mapsZero, diagsZero) = destriping(scans, 0, diags, step=2)
	for array in arrays: 
		print "mapsZero "+array
		level2ZeroPoint.refs.put(array.upper(), ProductRef(mapsZero[array]))
	updatingLevel2(level2, "extd", None, diagsZero)

	(zeroPointMaps, zeroPointParam) = zeroPointCorrection(level2=level2ZeroPoint, hfi545Map=hfi545Map, hfi857Map=hfi857Map, hfi545Gain=hfi545Gain, hfi857Gain=hfi857Gain,hfiFwhm=hfiFwhm, colorCorrHfi=colorCorrHfi, fluxConv=fluxConv)
	for array in arrays:
		level2.refs.put("extd"+array.upper(),ProductRef(zeroPointMaps.getProduct("extd"+array.upper())))
	return level2

def makeContext(level):
		"""
		Method to create a level context.
		Input: 
		    level: a level context (not a string) from where the 
		    relevant metadata are copied in the new one;
		    if the input is a level0_5 context, a new level1 context is generated; 
		    if the input is a level1 context, a new level2 (MapContext) context is generated.
		Return a context (MapContext or Level1Context) with metadata set
		    """
		print "Creating a Level2 Context"
		level2 = MapContext()
		for key in level.meta.keySet():
			if key != "creator" and key != "creationDate":
				level2.meta[key] = level.meta[key].copy()
				print level2.meta[key]
		
		level2.creator = "IDOC"
		level2.type = "level2context"
		level2.description = "Context for SPIRE Level 2 products"
		level2.meta["level"] = StringParameter("20", "The level of the product")
		level2.meta["instMode"] = StringParameter("POF5", "instMode")
		return level2

def updatingLevel2(level2, type, maps, diags=None):
		"""
		Function to update the Level2 Context
		Input: 
			level2 context to update
			type (sso, psrc, extd)
			maps to add to the context
			diagnostic products to add to the context
		Return:
		    level2 context updated
		"""
		arrays = ['PSW', 'PMW', 'PLW']
		prefix = type
		for array in arrays:
			print "Updating level2 ", array
			if (maps is not None):
				if (type=='sso'):
					map = maps[array]
					map.meta['type'] = StringParameter('ssoPMP')
				else:
					map = maps[array]
				level2.refs.put(prefix+array.upper(),  ProductRef(map))
			if (diags != None):
				level2.refs.put(prefix+array.upper()+'diag', ProductRef(diags[array]))

	
def multiL1fitstoL2(toProcess,resolution='',wcsPSW='',wcsPMW='',wcsPLW=''):
	scans = SpireListContext()
	useRemoveBaseline = True
	tempStorage = Boolean.TRUE
	chanNum = FitsArchive().load("/data/glx-herschel/data1/herschel/data/SPIRE/ChanNum.fits")
	for index in range(len(toProcess)):
		print "Adding "+toProcess[index]['obsid']+"..."
		filelist=glob.glob(path.join(toProcess[index]['dir'],"*"+toProcess[index]['obsid']+"*"))
		for file in filelist[0:]:
			psp = FitsArchive().load(file)
			if useRemoveBaseline:
				psp = removeBaseline(psp,chanNum=chanNum)
				if tempStorage:
					ref = ProductSink.getInstance().save(psp)
					scans.addRef(ref)
				else:
					scans.addProduct(psp)
			else:
				scans.addRef(psp)
			pass
	
	(level2, scans, diags) = doMaps(scans,resolution=resolution,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW)
	
	return level2

# L1fitstoL2 take L1 fits as input and create PSW PMW PLW maps (used for H-ATLAS)
def L1fitstoL2(dir,string,resolution='',wcsPSW='',wcsPMW='',wcsPLW=''):
	filelist = glob.glob(path.join(dir,"*"+string+"*"))
	# Use the first let to get the list of good channels
	# TODO : Will it be the same over the AOR ?
	scans = SpireListContext()
	useRemoveBaseline = True
	tempStorage = Boolean.TRUE
	chanNum = FitsArchive().load("/data/glx-herschel/data1/herschel/data/SPIRE/ChanNum.fits")
	for file in filelist[0:]:
		psp = FitsArchive().load(file)
		if useRemoveBaseline:
			psp = removeBaseline(psp,chanNum=chanNum)
			if tempStorage:
				print file
				ref = ProductSink.getInstance().save(psp)
				scans.addRef(ref)
			else:
				scans.addProduct(psp)
		else:
			scans.addRef(psp)
		pass
	
	(level2, scans, diags) = doMaps(scans,wcsPSW=wcsPSW,wcsPMW=wcsPMW,wcsPLW=wcsPLW)
	
	return level2

def fixBaseLine(scansIn, chanMask, ra0, dec0, dx, scansOut):
	# TODO: Not finished/tested/do not use...
	ss = Double1d()
	tt = Double1d()
	# Start a loop on all scans
	for i in range(scansIn.count):
		# get the scan line timeline
		timeline = scansIn.getProduct(i)
		# get the time
		time = timeline.sampleTime
		# now loop on all detectors that are contained in the product
		for detector in timeline.channelNames:
			# exclude dead detectors and use only PSW ones
			if chanMask.isDead(detector) == False and detector[0:3]=="PSW":
				# get RA, Dec and signal
				ra = timeline.getRa(detector)
				dec = timeline.getDec(detector)
				signal = timeline.getSignal(detector)
				mask = timeline.getMask(detector)
				# find out for which indexes values the RA and Dec
				# are near the reference point
				jjj = (ra > ra0-dx) and (ra < ra0+dx)
				jjj = jjj and (dec > dec0-dx) and (dec < dec0+dx)
				jjj = jjj and (mask == 0)
				# select from the signal and time the data that respect
				# the above limits
				selSignal = signal[signal.where(jjj)]
				selTime = time[time.where(jjj)]
				# if any value is found, put it in the temporary arrays
				if selTime.length() > 0:
					tt.append(Double1d(selTime))
					ss.append(Double1d(selSignal))
	
	# sort the arrays according to the time
	time = tt[Selection(SORT.BY_INDEX(tt))]
	signal = ss[Selection(SORT.BY_INDEX(tt))]
	
	# Plot the result
	plot = PlotXY(time,signal,line=0,symbol=14,xtitle="Time [TAI]", \
	              ytitle="Flux [Jy]", titleText="Flux at the reference position")
	plot.legend.visible = 1
	plot[0].name = "Measured flux"
	
	# We can note from the plot that the trend seems linear in time
	# so we will do a linear fit to the above data.
	
	# Create a polynomial model of degree 1 (i.e. linear)
	poly = PolynomialModel(1)
	
	# Rescale the time otherwise the fit procedure
	# can go mad
	meant = (time[0]+time[-1])/2.0
	deltat = (time[0]-time[-1])
	x = (time-meant)/deltat
	
	# Create a fitter object
	fitter = Fitter(x,poly)
	
	# Perform the fit and extract the resulting parameters
	param  =  fitter.fit(signal)
	
	# Create the fitting linear relation
	trend = param[0]+param[1]*x
	
	# Overplot the fitted linear relation to measured values
	plot.addLayer(LayerXY(time,trend,color=Color.red,stroke=2,name="Trend fit"))
	
	# The result is encouraging! Now we will remove this linear trend
	# from all timelines and run the mapmaking on these modified timelines.
	
	# Create a new SpireListContext
	scansOut = SpireListContext()
	
	# Loop on all timelines contained in the "scans" list
	for i in range(scans.count):
		timeline = scans.getProduct(i)
		# get the sampling time
		sampleTime = timeline.sampleTime
		# compute the trend
		x = (sampleTime-meant)/deltat
		trend = param[0]+param[1]*x
		# loop on all detectors
		for detector in timeline.channelNames:
			# select only PSW detectors
			if detector[0:3]=="PSW":
				# overwrite the signal by substracting the trend
				timeline.setSignal(detector,\
					timeline.getSignal(detector)-trend)
		# save the timeline product in the ProductSink (a scratch area),
		# so that we do not run out of memory, and attach it to the list
		scansOut.addRef(ProductSink.getInstance().save(timeline))



def getFilesMotif(basePath,motifs,exclure):
	"""Get all the files from a directory, subdirectories included, that have some patterns and have not others"""
	listfile_all = []
	listroot = []
	for root, dirs, files in os.walk(basePath):
		##### WARNING KD added root in checkMotif call
		listfile = [root+"/"+filename for filename in files if ajoutRoot(root,exclure) and checkMotif(root+"/"+filename,motifs,exclure)]
		listfile_all.extend(listfile)

	listfile_all = sorted(listfile_all)

	return listfile_all

def checkMotif(chaine,Motifs, Exclure):
	for motif in Motifs:
		if str(chaine).count(motif)==0:
			return False

	for exclu in Exclure:
		if str(chaine).count(exclu)>0:
			return False

	return True

def ajoutRoot(root,exclure):
	for exclu in exclure:
		if str(root).count(exclu)>0:
			return False
	return True
