from herschel.ia.toolbox.util import SimpleFitsWriterTask
from herschel.ia.toolbox.image import ImageSubtractTask
from herschel.ia.toolbox.util import FitsReaderTask

import sys
import os
import pdb

def check_path(filename):
	"""check if the path is absolute. If not, return an error"""
	
	if (not os.path.isabs(filename)):
		print("Error: Path must be absolute")
		exit()
	
	return 0

DEFAULT_OUTPUT = "difference.fits"


isProblem = False
problem_message = """AIM : Given 2 inputs .fits files, generate a combined
 .fits file that is the substraction of the two, allowing to see
 the differences between the two original files

The script can take various arguments :
(no spaces between the key and the values, only separated by '=')

 * 2 input files
 * [optional] name for the output file

EXAMPLE:
To see the difference between file1.fits and file2.fits. Default name will be OUTPUTNAME:
> SCRIPTNAME file1.fits file2.fits

To specify the name of the output file
> SCRIPTNAME file1.fits file2.fits difference.fits
"""

problem_message = problem_message.replace("SCRIPTNAME", sys.argv[0])
problem_message = problem_message.replace("OUTPUTNAME", DEFAULT_OUTPUT)

nb_args = len(sys.argv) - 1

if ((nb_args<2) or (nb_args>3)):
	isProblem = True

if isProblem:
	print(problem_message)
	os._exit(1)
	
filename1 = sys.argv[1]
filename2 = sys.argv[2]

if (nb_args == 3):
	output_filename = sys.argv[3]
else:
	output_filename = DEFAULT_OUTPUT

########################################################################
##                                                                    ##
##                    BEGINNING OF THE CODE                           ##
##                                                                    ##
########################################################################
#~ HIPE> output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_extdPSW = fitsReader(file = '/home/ccossou/output_test/output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_extdPSW.fits')
#~ HIPE> output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_psrcPSW = fitsReader(file = '/home/ccossou/output_test/output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_psrcPSW.fits')
#~ HIPE> difference = imageSubtract(image1=output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_psrcPSW, ref=1, image2=output_testOD301_0x500039e5L_SpirePhotoSmallScan_HD37041_extdPSW)
#~ HIPE> simpleFitsWriter(product=difference, file='/home/ccossou/output_test/difference_zero_point_correction.fits')
#~ print os.getcwd()

fits1 = fitsReader(file=filename1)
fits2 = fitsReader(file=filename2)

difference = imageSubtract(image1=fits1, ref=1, image2=fits2)

simpleFitsWriter(product=difference, file=output_filename)
